FROM node:6.11.1

# Create app directory
WORKDIR /usr/src/app

# Install global node dependencies
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install gcc make build-essential g++ -y
RUN npm install -g node-gyp
RUN npm install -g pm2


# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

#RUN yarn install
RUN yarn --pure-lockfile
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

EXPOSE 8080

CMD [ "pm2", "start", "--no-daemon", "app.js" ]
