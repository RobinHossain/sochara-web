module.exports.crontab = {

  /*

  * The asterisks in the key are equivalent to the

  * schedule setting in crons, i.e.

  * minute hour day month day-of-week year

  * so in the example below it will run every minute

  */


  crons: function () {
    var jsonArray = [];
    jsonArray.push({ interval: '*/39 * * * * *', method: 'mail' });
    jsonArray.push({ interval: '*/20 * * * * *', method: 'social' });
    jsonArray.push({ interval: '* * *', method: 'social_token' });
    // jsonArray.push({ interval: '*/30 * * * * *', method: 'social_token' });
    jsonArray.push({ interval: '*/37 * * * * *', method: 'mail_newAccount' });

    // add more cronjobs if you want like below
    // but dont forget to add a new method.
    //jsonArray.push({interval:'*/1 * * * * * ',method:'anothertest'});

    return jsonArray;
  },

  // declare the method mytest
  // and add it in the crons function
  mail: function () {
    CronService.cronMails(function ( returnData ) {
      /*---------------- Formatting Mail With Global Mail ------------------------------*/
      // console.log(returnData);
      /*---------------- Formatting Mail With Global Mail ------------------------------*/
    });
  },

  mail_newAccount: function () {
    CronService.cronNewAccountMails(function ( returnData ) {
      /*---------------- Formatting Mail With Global Mail ------------------------------*/
      // console.log(returnData);
      /*---------------- Formatting Mail With Global Mail ------------------------------*/
    });
  },

  social: function () {
    SocialCron.cronSocialFeed(function ( returnData ) {
      /*---------------- Formatting Mail With Global Mail ------------------------------*/
      // console.log(returnData);
      /*---------------- Formatting Mail With Global Mail ------------------------------*/
    });
  },

  social_token: function () {
    SocialCron.refreshToken(function ( returnData ) {
      /*---------------- Formatting Mail With Global Mail ------------------------------*/
      // console.log(returnData);
      /*---------------- Formatting Mail With Global Mail ------------------------------*/
    });
  }

  /*

  anothertest:function(){
  require('../crons/anothertest.js').run();
  }
  */


};
