/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  // '/': {
  //   // skipAssets: true,
  //   view: 'main',
  //   locals: {
  //     layout: 'layout'
  //   }
  // }

  // 'GET /admin/outlook/oauth2callback': $admin + '/OutlookController.oauth2callback',

  'get /gmail/oauth2callback': 'GmailController.oauth2callback',
  'get /outlook/oauth2callback': 'OutlookController.oauth2callback',
  'get /auth/gmailAuthCallback': 'AuthController.gmailAuthCallback',
  'get /oAuthInstagramApi': 'SocialAuthController.oAuthInstagramApi',
  // 'get /oAuthInstagramApiToken': 'SocialAuthController.oAuthInstagramApiToken',
  'get /outlook/importContact': 'ThirdPartyTokenController.importContact',
  'get /page/facebook-privacy-policy': 'PageController.facebookPrivacyPolicy',

  'get /setAtsToken' : function (req, res, next) {
    let ats = req.query.ats ;
    let redirect_uri;
    if( req.headers.host === 'www.sochara.com' || req.headers.host === 'sochara.com' ){
      redirect_uri = 'https://' + req.headers.host + '/dashboard';
    }else{
      redirect_uri = 'http://' + req.headers.host + '/dashboard';
    }
    return res.view('auth_layout', {layout: 'auth_layout', data: {ats: ats, url: redirect_uri}});
  },

  // app.get('/auth/facebook/callback',
  // passport.authenticate('facebook', {
  //   successRedirect : '/profile',
  //   failureRedirect : '/'
  // }));

  // 'get /auth/facebook/callback': function(req, res) {
  //   passport.authenticate('facebook', {
  //     successRedirect : '/profile',
  //     failureRedirect : '/'
  //   });
  // },

  'get /*': function(req, res, next) {
    if (req.path.match(/\..*/g)) {
      return next();
    } else {
      // let url = req.originalUrl;
      // let module = url.substring(1).split('/', 1)[0];

      return res.view('main');
      // return res.view('main', {
      //   url: module
      // });
    }
  }

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
