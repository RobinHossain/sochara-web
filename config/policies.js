/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#!/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
 */


module.exports.policies = {

  /***************************************************************************
   *                                                                          *
   * Default policy for all controllers and actions (`true` allows public     *
   * access)                                                                  *
   *                                                                          *
   ***************************************************************************/

  // '*': true,
  // '*': {
  //   '*': ['oauth2']
  // },

  // For Dashboard
  // 'dashboard/*': ['sessionAuth', 'onlineUsers', 'tokenAuth'],
  // 'auth': {
  //   '*': ['sessionAuth', 'onlineUsers', 'tokenAuth']
  // },

  // 'gmail/oauth2callback': {
  //   '*': ['sessionAuth', 'onlineUsers', 'oauth2']
  // },


  'GmailController': {
    '*': ['sessionAuth', 'onlineUsers', 'oauth2', 'tokenAuth'], 'oauth2callback': ['sessionAuth', 'onlineUsers', 'oauth2']
  },

  'OutlookController': {
    '*': ['sessionAuth', 'onlineUsers', 'authOutlook', 'tokenAuth'], 'oauth2callback': ['sessionAuth', 'onlineUsers', 'authOutlook']
  },

  'DashboardController': {
    '*': ['sessionAuth', 'onlineUsers', 'authOutlook', 'tokenAuth', 'oauth2'], 'oauth2callback': ['sessionAuth', 'onlineUsers', 'authOutlook']
  },

  'SocketController' :{
    '*' : ['sessionAuth', 'oauth2', 'onlineUsers'], 'oauth2callback': ['sessionAuth', 'onlineUsers', 'authOutlook']
  },

  'CardsController': {
    '*': ['sessionAuth', 'onlineUsers','tokenAuth']
  },

  'ChatController': {
    '*': ['sessionAuth', 'onlineUsers','tokenAuth']
  },

  'ContactsController': {
    '*': ['sessionAuth', 'onlineUsers','tokenAuth']
  },

  'FilesController': {
    '*': ['sessionAuth', 'onlineUsers','tokenAuth']
  },

  'MailController': {
    '*': ['sessionAuth', 'onlineUsers', 'oauth2', 'authOutlook','tokenAuth']
  },

  'SocialAuthController': {
    '*': ['sessionAuth', 'onlineUsers', 'oauth2', 'authOutlook','tokenAuth']
  },

  'MediaController': {
    '*': ['sessionAuth', 'onlineUsers','tokenAuth']
  },

  'CalendarController': {
    '*': ['oauth2', 'authOutlook', 'onlineUsers', 'sessionAuth', 'tokenAuth']
  },

  'UserController': {
    '*': ['sessionAuth', 'onlineUsers', 'tokenAuth']
  },

  'ThirdPartyTokenController': {
    '*': ['authOutlook', 'sessionAuth', 'oauth2']
  },

  'auth': {
    '*': ['oauth2']
  },



  // For Gmail Module
  // 'mail/*': ['sessionAuth', 'onlineUsers', 'oauth2', 'gmail', 'gmail_watch', 'tokenAuth'],

  // For Profile
  // 'profile/*': ['sessionAuth', 'onlineUsers', 'tokenAuth'],


  /***************************************************************************
   *                                                                          *
   * Here's an example of mapping some policies to run before a controller    *
   * and its actions                                                          *
   *                                                                          *
   ***************************************************************************/
  // RabbitController: {

  // Apply the `false` policy as the default for all of RabbitController's actions
  // (`false` prevents all access, which ensures that nothing bad happens to our rabbits)
  // '*': false,

  // For the action `nurture`, apply the 'isRabbitMother' policy
  // (this overrides `false` above)
  // nurture	: 'isRabbitMother',

  // Apply the `isNiceToAnimals` AND `hasRabbitFood` policies
  // before letting any users feed our rabbits
  // feed : ['isNiceToAnimals', 'hasRabbitFood']
  // }
};
