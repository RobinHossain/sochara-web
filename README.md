# sochara-new

a [Sochara](https://www.sochara.com) application

## Instruction to use sails on your low performance pc.

1. Pull the lastest files from git

  ``git pull origin master``
2. Run Your app
 
  ``sails lift`` or ``node app.js``
 
3. Copy the ``importer.css`` file from ``.tmp/public/sytles`` to ``assets/styles`` directory.
4. Now insert following code to ``.sailsrc`` file
 
```
"hooks": {
        "grunt": false
     },
     "paths": {
         "public": "assets"
     }
 ```
5. Now finaly ``.sailsrc`` file would look like
  
 ``` 
  {"generators": {
      "modules": {}
    },
    "hooks": {
       "grunt": false
    },
    "paths": {
        "public": "assets"
    }
  }
```
6. Now Run you app again

 ``sails lift`` or ``node app.js``

#### Hope this trick will works fine on your pc
