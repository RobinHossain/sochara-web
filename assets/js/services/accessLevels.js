angular.module('app.auth.login')
  .constant('AccessLevels', {
    anon: 0,
    user: 1
  });
