(function () {
  'use strict';

  /**
   * Main module of the Fuse
   */
  angular
    .module('sochara', [

      // Common 3rd Party Dependencies
      // 'textAngular',
      // 'xeditable',

      // Core
      'app.core',

      // Toolbar
      'app.toolbar',

      // Quick Panel
      // 'app.quick-panel',

      // Auth - Login Page
      'app.auth.login',

      // Auth - Register Page
      'app.auth.register',

      //Forgot Password
      'app.auth.forgot-password',

      //Reset Password
      'app.auth.reset-password',

      //Mail Module
      'app.mail',

      //Facebook privacy policy
      'app.pages',
      // Chat Module
      'app.chat',

      // Media Module
      'app.media',

      // Files Module
      'app.files',


      // Contact
      'app.contacts',
      // Dashboard
      'app.dashboard',

      // Contact-demo
      // 'app.contactsec',

      // Label
      // 'app.labelsec',
      'app.labels',

      // Calendar
      'app.calendar',

      // Cards
      'app.cards',

      // Poll
      'app.poll',

      //profile
      'app.profile',

      //profile
      'app.global-setting',

      'ngImgCrop',
      //ng-emoticons
      'ngFileUpload',
      'ngFileUpload',

    ]);
})();
