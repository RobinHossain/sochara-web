(function () {
  'use strict';

  angular
    .module('sochara')
    .controller('IndexController', IndexController);

  /** @ngInject */
  function IndexController(socharaTheming) {
    var vm = this;

    // Data
    vm.themes = socharaTheming.themes;

    //////////
  }
})();
