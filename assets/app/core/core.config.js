(function () {
  'use strict';

  angular
    .module('app.core')
    .config(config);

  /** @ngInject */
  function config($ariaProvider, $logProvider, msScrollConfigProvider, socharaConfigProvider) {
    // Enable debug logging
    $logProvider.debugEnabled(true);

    /*eslint-disable */

    // ng-aria configuration
    $ariaProvider.config({
      tabindex: false
    });

    // Fuse theme configurations
    socharaConfigProvider.config({
      'disableCustomScrollbars': false,
      'disableCustomScrollbarsOnMobile': true,
      'disableMdInkRippleOnMobile': true
    });

    // msScroll configuration
    msScrollConfigProvider.config({
      wheelPropagation: true
    });

    /*eslint-enable */
  }
})();
