(function () {
  'use strict';
  // Color Picker Directive
  angular.module('app.core')
    .directive('spectrumColorpicker', function () {
      return {
        restrict: 'EA',
        require: 'ngModel',
        scope: {
          fallbackValue: '=',
          disabled: '=?',
          format: '=?',
          options: '=?',
          triggerId: '@?',
          palette: '=?',
          onChange: '&?',
          onShow: '&?',
          onHide: '&?',
          onMove: '&?',

          onBeforeShow: '&?',

          onChangeOptions: '=?',
          onShowOptions: '=?',
          onHideOptions: '=?',
          onMoveOptions: '=?'
        },
        replace: true,
        templateUrl: 'directive.html',
        link: function ($scope, $element, attrs, $ngModel) {

          var $input = $element.find('input');

          function formatColor(tiny) {
            var formatted = tiny;
            if (formatted) {
              formatted = tiny.toString($scope.format);
            }
            return formatted;
          }

          function callOnChange(color) {
            if (angular.isFunction($scope.onChange)) {
              $scope.onChange({color: color});
            }
          }

          function setViewValue(color) {
            var value = $scope.fallbackValue;

            if (color) {
              value = formatColor(color);
            } else if (angular.isUndefined($scope.fallbackValue)) {
              value = color;
            }

            $ngModel.$setViewValue(value);
            callOnChange(value);
          }

          var onChange = function (color) {
            $scope.$evalAsync(function () {
              setViewValue(color);
            });
          };
          var onToggle = function () {
            $input.spectrum('toggle');
            return false;
          };


          var baseOpts = {
            color: $ngModel.$viewValue
          };

          var localOpts = {};

          angular.forEach({
              'change': 'onChange',
              'move': 'onMove',
              'hide': 'onHide',
              'show': 'onShow',
            },
            function (eventHandlerName, eventName) {
              var spectrumEventHandlerOptions = $scope[eventHandlerName + 'Options'];
              localOpts[eventName] = function (color) {
                if (!spectrumEventHandlerOptions || spectrumEventHandlerOptions.update) {
                  onChange(color);
                }
                // we don't do this for change, because we expose the current
                // value actively through the model
                if (eventHandlerName !== 'change' && angular.isFunction($scope[eventHandlerName])) {
                  return $scope[eventHandlerName]({color: formatColor(color)});
                } else {
                  return null;
                }
              };
            });

          if (angular.isFunction($scope.onBeforeShow)) {
            localOpts.beforeShow = function (color) {
              return $scope.onBeforeShow({color: formatColor(color)});
            };
          }

          if ($scope.palette) {
            localOpts.palette = $scope.palette;
          }

          localOpts.replacerClassName = 'icon-format-color-fill';
          localOpts.showButtons = false;
          localOpts.showPaletteOnly = true;
          localOpts.showPalette = true;
          localOpts.hideAfterPaletteSelect = true;
          localOpts.palette = [
            ["#000", "#444", "#666", "#999", "#ccc", "#eee", "#f3f3f3", "#fff"],
            ["#f00", "#f90", "#ff0", "#0f0", "#0ff", "#00f", "#90f", "#f0f"],
            ["#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#cfe2f3", "#d9d2e9", "#ead1dc"],
            ["#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
            ["#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6fa8dc", "#8e7cc3", "#c27ba0"],
            ["#c00", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3d85c6", "#674ea7", "#a64d79"],
            ["#900", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#0b5394", "#351c75", "#741b47"],
            ["#600", "#783f04", "#7f6000", "#274e13", "#0c343d", "#073763", "#20124d", "#4c1130"]
          ];

          var options = angular.extend({}, baseOpts, $scope.options, localOpts);

          if ($scope.triggerId) {
            angular.element(document.body).on('click', '#' + $scope.triggerId, onToggle);
          }

          $ngModel.$render = function () {
            $input.spectrum('set', $ngModel.$viewValue || '');
            callOnChange($ngModel.$viewValue);
          };

          if (options.color) {
            $input.spectrum('set', options.color || '');
            setViewValue(options.color);
          }

          $input.spectrum(options);

          $scope.$on('$destroy', function () {
            if ($scope.triggerId) {
              angular.element(document.body).off('click', '#' + $scope.triggerId, onToggle);
            }
          });
          $element.on('$destroy', function () {
            $input.spectrum('destroy');
          });

          if (angular.isDefined(options.disabled)) {
            $scope.disabled = !!options.disabled;
          }

          $scope.$watch('disabled', function (newDisabled) {
            $input.spectrum(newDisabled ? 'disable' : 'enable');
          });

          $scope.$watch('palette', function (palette) {
            $input.spectrum('option', 'palette', palette);
          }, true);
        }
      };
    }).run(['$templateCache', function ($templateCache) {
    'use strict';
    $templateCache.put('directive.html',
      "<span><input class=\"input-small\"></span>"
    );
  }]);

})();
