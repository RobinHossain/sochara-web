(function () {
  'use strict';
  angular.module('app.core')
    .directive('errSrc', function () {
      return {
        link: function (scope, element, attrs) {

          var watcher = scope.$watch(function () {
            return attrs['ngSrc'];
          }, function (value) {
            if (!value) {
              element.attr('src', attrs.errSrc);
            }
          });

          element.bind('error', function () {
            element.attr('src', attrs.errSrc);
          });

          //unsubscribe on success
          element.bind('load', watcher);

        }
      };
    });


  angular.module('app.core')
    .directive('fdFontDropdown', function () {
      var fonts = ["Aclonica", "Allan", "Annie+Use+Your+Telescope", "Anonymous+Pro", "Allerta", "Amaranth", "Anton", "Architects+Daughter", "Artifika", "Arvo", "Asset", "Astloch", "Bangers", "Bentham", "Bevan", "Bigshot+One", "Bowlby+One", "Bowlby+One+SC", "Brawler", "Cabin", "Calligraffitti", "Candal", "Cantarell", "Cardo", "Carter One", "Caudex", "Cedarville+Cursive", "Cherry+Cream+Soda", "Chewy", "Coda", "Coming+Soon", "Copse", "Cousine", "Covered+By+Your+Grace", "Crafty+Girls", "Crimson+Text", "Crushed", "Cuprum", "Damion", "Dancing+Script", "Dawning+of+a+New+Day", "Didact+Gothic", "Droid+Sans", "Droid+Sans+Mono", "Droid+Serif", "Expletus+Sans", "Fontdiner+Swanky", "Forum", "Francois+One", "Geo", "Give+You+Glory", "Goblin+One", "Goudy+Bookletter+1911", "Gravitas+One", "Gruppo", "Hammersmith+One", "Holtwood+One+SC", "Homemade+Apple", "Inconsolata", "Indie+Flower", "IM+Fell+DW+Pica", "Irish+Grover","Istok+Web", "Josefin+Sans", "Josefin+Slab", "Judson", "Jura", "Just+Another+Hand", "Just+Me+Again+Down+Here", "Kameron", "Kenia", "Kranky", "Kreon", "Kristi", "La+Belle+Aurore", "Lato", "League+Script", "Lekton", "Limelight", "Lobster", "Lobster Two","Love+Ya+Like+A+Sister", "Loved+by+the+King", "Luckiest+Guy", "Maiden+Orange", "Mako", "Maven+Pro", "Meddon", "MedievalSharp", "Megrim", "Merriweather", "Metrophobic", "Michroma", "Miltonian Tattoo", "Miltonian", "Modern Antiqua", "Monofett", "Molengo", "Mountains of Christmas", "Muli", "Neucha", "Neuton", "News+Cycle", "Nixie+One", "Nobile", "Nova+Cut", "Nunito", "OFL+Sorts+Mill+Goudy+TT", "Old+Standard+TT", "Open+Sans", "Orbitron", "Oswald", "Over+the+Rainbow", "Reenie+Beanie", "Pacifico", "Patrick+Hand", "Paytone+One", "Permanent+Marker", "Philosopher", "Play", "Playfair+Display", "Podkova", "PT+Sans", "PT+Sans+Narrow", "PT+Serif", "PT+Serif Caption", "Puritan", "Quattrocento", "Quattrocento+Sans", "Radley", "Redressed", "Rock+Salt", "Rokkitt", "Ruslan+Display", "Schoolbell", "Shadows+Into+Light", "Shanti", "Sigmar+One", "Six+Caps", "Slackey", "Smythe", "Special+Elite", "Stardos+Stencil", "Sue+Ellen+Francisco", "Sunshiney", "Swanky+and+Moo+Moo", "Syncopate", "Tangerine", "Tenor+Sans", "Terminal+Dosis+Light", "The+Girl+Next+Door", "Tinos", "Ubuntu", "Ultra", "Unkempt", "UnifrakturMaguntia", "Varela", "Varela Round", "Vibur", "Vollkorn", "VT323", "Waiting+for+the+Sunrise", "Wallpoet", "Walter+Turncoat", "Wire+One", "Yanone+Kaffeesatz", "Yeseva+One", "Zeyada"];
      return {
        link: function (scope, element, attr) {
          window.WebFontConfig = {
            google: {
              families: []
            }
          }
          scope.fontslist = fonts.map(function(font){
            var nFont = font.replace(/\+/g, ' ').split(':')[0];
            return {'name': nFont, 'face': font, 'style': {'fontFamily': nFont, 'fontWeight': font.split(':')[1] || 400}};
          });

          var WEBFONTAPI = "//ajax.googleapis.com/ajax/libs/webfont/1/webfont.js";

          var loadFonts = function () {
            var font, i, len, ref, s, wf;
            ref = scope.fontslist;
            for (i = 0, len = ref.length; i < len; i++) {
              font = ref[i];
              WebFontConfig.google.families.push(font.face);
            }

            wf = document.createElement('script');
            wf.src = ('https:' === document.location.protocol ? 'https:' : 'http:') + WEBFONTAPI;
            wf.type = 'text/javascript';
            wf.async = 'true';
            s = document.getElementsByTagName('script')[0];
            return s.parentNode.insertBefore(wf, s);
          };
          loadFonts();
          scope.selectedIdx = 0;
          scope.changeFont = function (idx) {
            scope.selectedIdx = idx;
          };

          // return angular.element(document).bind('click', function (event) {
          //   console.log(event);
          //   console.log(element);
          //   // return element.toggleClass("active");
          // });
          return element.bind('click', function () {
            return element.toggleClass("active");
          });
        }
      };
    });

  angular.module('app.core')
    .directive('socharaFontSize', function () {
      return {
        link: function (scope, element, attr) {
          scope.selectedFontIdx = 0;
          scope.fontSizeList = [5,6,7,8,9,10,11,12,13,14,15,16,18,20,22,24,25,26,28,30,32,34,36,38,40,45,50,55];
          scope.changeFontSize = function (idx) {
            scope.selectedFontIdx = idx;
          };
          return element.bind('click', function () {
            return element.toggleClass("active");
          });
        }
      };
    });
})();

