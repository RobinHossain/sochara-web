(function ()
{
    'use strict';

    angular
        .module('app.core')
        .provider('socharaConfig', socharaConfigProvider);

    /** @ngInject */
    function socharaConfigProvider()
    {
        // Default configuration
        var socharaConfiguration = {
            'disableCustomScrollbars'        : false,
            'disableMdInkRippleOnMobile'     : true,
            'disableCustomScrollbarsOnMobile': true
        };

        // Methods
        this.config = config;

        //////////

        /**
         * Extend default configuration with the given one
         *
         * @param configuration
         */
        function config(configuration)
        {
            socharaConfiguration = angular.extend({}, socharaConfiguration, configuration);
        }

        /**
         * Service
         */
        this.$get = function ()
        {
            var service = {
                getConfig: getConfig,
                setConfig: setConfig
            };

            return service;

            //////////

            /**
             * Returns a config value
             */
            function getConfig(configName)
            {
                if ( angular.isUndefined(socharaConfiguration[configName]) )
                {
                    return false;
                }

                return socharaConfiguration[configName];
            }

            /**
             * Creates or updates config object
             *
             * @param configName
             * @param configValue
             */
            function setConfig(configName, configValue)
            {
                socharaConfiguration[configName] = configValue;
            }
        };
    }

})();
