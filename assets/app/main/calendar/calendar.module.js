(function () {
  'use strict';

  angular
    .module('app.calendar',
      [
        // 3rd Party Dependencies
        'ui.calendar',
        'mdPickers',
        'angularMoment',
        'angular-tiny-calendar',
      ]
    )
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    // State
    $stateProvider.state('app.calendar', {
      url: '/calendar',
      views: {
        'content@app': {
          templateUrl: 'app/main/calendar/calendar.html',
          controller: 'CalendarController as vm'
        }
      },
      bodyClass: 'calendar'
    });

  }
})();
