'use strict';

//This will be overridden later in the file after a build in order to load the templates, this is just a placeholder
//to keep the module happy during development
angular.module("weather-templates", []);

var weatherModule = angular.module('weatherModule', ["weather-templates"]);


weatherModule.service('weatherService', function ($http, $q) {


  var service = {
    curWeather: {},
    forecast: {},
    userLocation: function () {
    },
    getWeather: function (location, units) {
      location = window.userProfileLocation;
      if (service.curWeather[location]) {
        return service.curWeather[location];
      }
      service.curWeather[location] = {temp: {}, clouds: null};
      $http.get("https://query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where woeid in (select woeid from geo.places(1) where text='" + location + "')&format=json").then(function successCallback(resp) {
          var data = resp.data;
          console.log(data);
          if (data) {
            service.curWeather[location].temp.current = data.query.results.channel.item.condition.temp;
            service.curWeather[location].temp.min = data.query.results.channel.item.forecast[0].low;
            service.curWeather[location].temp.max = data.query.results.channel.item.forecast[0].high;
            service.curWeather[location].icon = data.query.results.channel.item.condition.code;
            service.curWeather[location].id = data.query.results.channel.item.condition.code;
            service.curWeather[location].condition = data.query.results.channel.item.condition.text;

            var userCity = data.query.results.channel.location.city;
            var userCountry = data.query.results.channel.location.country;
            service.curWeather[location].userlocation = userCity + ', ' + userCountry;
            // console.log(data)
          }
        },
        function errorCallback(err) {
          service.curWeather[location];
        });

      return service.curWeather[location];
    },
    getForecast: function (location, units) {
      location = window.userProfileLocation;
      if (service.forecast[location])
        return service.forecast[location];

      service.forecast[location] = {}

      $http.get("https://query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where woeid in (select woeid from geo.places(1) where text='" + location + "')&format=json&callback=callbackFunction").then(function successCallback(resp) {
          var data = resp.data;
          if (data) {
            angular.copy(data, service.forecast[location]);
          }
        },
        function errorCallback(err) {
          defer.reject(err);
        });

      return service.forecast[location];
    }
  };
  return service;
});

weatherModule.filter('temp', function ($filter) {
  return function (input, precision, units) {
    if (!precision) {
      precision = 1;
    }

    var unitDisplay;

    switch (units) {
      case "imperial":
        unitDisplay = "F"
        break;
      case "metric":
        unitDisplay = "C"
        break;
      default:
        unitDisplay = "F"
        break;
    }

    var numberFilter = $filter('number');
    return numberFilter(input, precision) + '&deg;' + unitDisplay;
  };
});

weatherModule.filter('daysInTheFuture', function () {
  return function (input) {
    return new moment().add(input, 'days').format('dddd<br/>MMM Do YYYY');
  };
});

weatherModule.directive('todaysWeather', function (weatherService) {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      location: '@',
      useGoogleImages: '=',
      customSize: '=?',
      units: '@?',
      clickevent: '&'
    },
    templateUrl: '../../../../templates/currentWeatherDisplay.tpl.html',
    link: function (scope, iElem, iAttr) {
      scope.clickevent = function () {
      },
        scope.customSize = scope.customSize || 75;
      scope.units = scope.units || "imperial";
      scope.weather = weatherService.getWeather(scope.location, scope.units);
    }
  };
});


weatherModule.directive('weatherForecast', function (weatherService) {
  return {
    scope: {
      location: '@',
      useGoogleImages: '=',
      customSize: '=?',
      units: '@?'
    },
    restrict: 'E',
    replace: true,
    templateUrl: '../../../../templates/forecastDisplay.tpl.html',
    link: function (scope) {
      scope.customSize = scope.customSize || 50;
      scope.units = scope.units || "imperial";

      scope.findIndex = function (weatherObj) {
        return scope.forecast.list.indexOf(weatherObj);
      };
      scope.forecast = weatherService.getForecast(scope.location, scope.units);
      // console.log('loaded forecast:')
      // console.log(scope.forecast)
    }
  };
});


weatherModule.directive('weatherDisplay', function () {
  return {
    scope: {
      weather: '=',
      customSize: '=',
      useGoogleImages: '=',
      units: '=',
      clickevent: '&'
    },
    link: function (scope) {
      scope.clickevent = function () {

      }
    },
    restrict: 'E',
    replace: true,
    templateUrl: '../../../../templates/basicWeatherDisplay.tpl.html'
  };
});


weatherModule.directive('weatherIcon', function () {
  return {
    restrict: 'E', replace: true,
    scope: {
      cloudiness: '@',
      icon: '@',
      conditionid: '@',
      customSize: '=',
      useGoogleImages: '='
    },
    link: function (scope) {
      scope.getIconClass = function () {
        var baseUrl = 'http://l.yimg.com/a/i/us/we/52/';
        var showingIcon = baseUrl + scope.icon + '.gif'
        return showingIcon;
      };
    },
    template: '<img style=\'height:{{customSize}}px;width:{{customSize}}px\' ng-src=\'{{ getIconClass() }}\'>'
  };
});


weatherModule.directive('weatherIconGoogle', function () {
  return {
    restrict: 'E', replace: true,
    scope: {
      cloudiness: '@',
      icon: '@',
      customSize: '=',
      useGoogleImages: '='
    },
    link: function (scope) {
      scope.imgurl = function () {
        var baseUrl = 'http://l.yimg.com/a/i/us/we/52/';
        var showingIcon = baseUrl + scope.icon + '.gif'
        return showingIcon;
      };
    },
    template: '<img style=\'height:{{customSize}}px;width:{{customSize}}px\' ng-src=\'{{ imgurl() }}\'>'
  };
});
