(function () {
  'use strict';

  angular
    .module('sochara')
    .factory('CalendarService', CalendarService);

  /** @ngInject */
  function CalendarService($http, $q) {

    return {

      // createNewCalendarEvent: function (data) {
      //   var createNewCalendarEventCall = $http.post('/calendar/createNewCalendarEvent', data);
      //   createNewCalendarEventCall.success(function (result) {
      //     return result;
      //   });
      //   createNewCalendarEventCall.error(function (resp) {
      //     return resp;
      //   });
      //   return createNewCalendarEventCall;
      // },

      addNewContact: function ( data ) {
        var defer = $q.defer();
        $http.post('/contacts/addContact', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      createNewCalendarEvent: function ( data ) {
        var defer = $q.defer();
        $http.post('/calendar/createNewCalendarEvent', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getCalendarEventCount: function ( ) {
        var defer = $q.defer();
        $http.post('/calendar/getCalendarEventCount').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      updateSignupEvent: function ( data ) {
        var defer = $q.defer();
        $http.post('/calendar/updateSignupEvent', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      updateEvent: function ( data ) {
        var defer = $q.defer();
        $http.post('/calendar/updateEvent', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      updateRecurringEvent: function ( data ) {
        var defer = $q.defer();
        $http.post('/calendar/updateRecurringEvent', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      updateEventOptions: function ( data ) {
        var defer = $q.defer();
        $http.post('/calendar/updateEventOptions', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      deleteEvent: function ( data ) {
        var defer = $q.defer();
        $http.post('/calendar/deleteEvent', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getEvent: function ( data ) {
        var defer = $q.defer();
        $http.post('/calendar/getEvent', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getBirthdayContacts: function () {
        var defer = $q.defer();
        $http.post('/calendar/getBirthdayContacts').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getHolidays: function () {
        var defer = $q.defer();
        $http.post('/calendar/getHolidays').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getCalendarInitData: function ( ) {
        var defer = $q.defer();
        $http.post('/calendar/getCalendarInitData').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      readCalendarEvents: function ( ) {
        var defer = $q.defer();
        $http.post('/calendar/readCalendarEvents').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      // readCalendarEvents: function () {
      //   var readCalendarEventsCall = $http.post('/calendar/readCalendarEvents');
      //   readCalendarEventsCall.success(function (result) {
      //     return result.msg;
      //   });
      //   readCalendarEventsCall.error(function (resp) {
      //     return resp;
      //   });
      //   return readCalendarEventsCall;
      // },
      update: function (updatedEvent, attendeesData, nAndPdata, signupsData) {
        var eventData = {};
        eventData.event = updatedEvent;
        eventData.attendees = attendeesData;
        eventData.nAndP = nAndPdata;
        eventData.signup = signupsData;

        var updateCalendarEventsCall = $http.post('/calendar/updateCalendarEvent', eventData);
        updateCalendarEventsCall.success(function (result) {
          return result.msg;
        });
        updateCalendarEventsCall.error(function (resp) {
          return resp;
        });
        return updateCalendarEventsCall;

      },
      responseOrganizerChange: function (id, response) {
        var responseOrganizerChangeCall = $http.post('/calendar/responseOrganizerChange', {id: id, res: response});
        responseOrganizerChangeCall.success(function (result) {
          return result.msg;
        });
        responseOrganizerChangeCall.error(function (resp) {
          return resp;
        });
        return responseOrganizerChangeCall;
      },
      saveAttendeeCmntColor: function (eventId, userId, response) {
        var saveAttendeeCmntColorCall = $http.post('/calendar/saveAttendeeCmntColor', {
          eventId: eventId,
          userId: userId,
          res: response
        });
        saveAttendeeCmntColorCall.success(function (result) {
          return result.msg;
        });
        saveAttendeeCmntColorCall.error(function (resp) {
          return resp;
        });
        return saveAttendeeCmntColorCall;
      },
      responseContributorChange: function (eventId, userId, response) {
        var responseContributorChangeCall = $http.post('/calendar/responseContributorChange', {
          eventId: eventId,
          userId: userId,
          res: response
        });
        responseContributorChangeCall.success(function (result) {
          return result.msg;
        });
        responseContributorChangeCall.error(function (resp) {
          return resp;
        });
        return responseContributorChangeCall;
      },
      saveContribution: function (eventId, userId, contribution, eventScope, eventScopeDate) {
        var saveContributionCall = $http.post('/calendar/saveContribution', {
          eventId: eventId,
          userId: userId,
          contribution: contribution,
          eventScope: eventScope,
          eventScopeDate: eventScopeDate
        });
        saveContributionCall.success(function (result) {
          return result.msg;
        });
        saveContributionCall.error(function (resp) {
          return resp;
        });
        return saveContributionCall;
      },
      swapRequest: function (data) {
        var swapRequestCall = $http.post('/calendar/swapRequest', data);
        swapRequestCall.success(function (result) {
          return result.msg;
        });
        swapRequestCall.error(function (resp) {
          return resp;
        });
        return swapRequestCall;
      },
      acceptSwapRequest: function (id) {
        var acceptSwapRequestCall = $http.post('/calendar/acceptSwapRequest', {id: id});
        acceptSwapRequestCall.success(function (result) {
          return result.msg;
        });
        acceptSwapRequestCall.error(function (resp) {
          return resp;
        });
        return acceptSwapRequestCall;
      },
      getSwapRequests: function (data) {
        var getSwapRequestsCall = $http.post('/calendar/getSwapRequests');
        getSwapRequestsCall.success(function (result) {
          return result.msg;
        });
        getSwapRequestsCall.error(function (resp) {
          return resp;
        });
        return getSwapRequestsCall;
      },
      // setWeatherLocation: function (data,lat,lng) {
      //   var setWeatherLocationCall = $http.post('/calendar/setWeatherLocation', {data: data,lat:lat,lng:lng});
      //   setWeatherLocationCall.success(function (result) {
      //     return result.msg;
      //   });
      //   setWeatherLocationCall.error(function (resp) {
      //     return resp;
      //   });
      //   return setWeatherLocationCall;
      // },

      setWeatherLocation: function ( data ) {
        var defer = $q.defer();
        $http.post('/calendar/setWeatherLocation', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getWeatherLocation: function () {
        var defer = $q.defer();
        $http.post('/calendar/getWeatherLocation').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getLocationByLatLon: function ( data ) {
        var defer = $q.defer();
        $http.post('/calendar/getLocationByLatLon', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getSettings: function ( ) {
        var defer = $q.defer();
        $http.post('/calendar/getSettings').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getApiEvents: function ( ) {
        var defer = $q.defer();
        $http.post('/calendar/getApiEvents').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      setSettings: function ( data ) {
        var defer = $q.defer();
        $http.post('/calendar/setSettings', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      holidayEvents: function () {
        var holidayEventsCall = $.get('https://www.googleapis.com/calendar/v3/calendars/en.usa%23holiday@group.v.calendar.google.com/events?key=AIzaSyAMuw6cJXavTBNIJk6x4D3SCJxiDI7bl5U');
        holidayEventsCall.done(function (result) {
          return result;
        });
        holidayEventsCall.fail(function (resp) {
          return resp;
        });
        return holidayEventsCall;
      },

      weatherInfo: function ( locationData ) {
        var defer = $q.defer();
        $http.post('/calendar/getWeatherInfo', locationData).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      // weatherInfo: function ( locationData ) {
      //   var defer = $q.defer();
      //   $http.get('https://api.openweathermap.org/data/2.5/forecast/daily?lon=' + locationData.lon + '&lat=' + locationData.lat + '&cnt=10&mode=json&units=metric&appid=0702621e6f9a5bc65b5be33ddd6c5e66', { 'Access-Control-Allow-Origin': '*', headers: { 'Content-Type': 'application/json; charset=utf-8' }}).then(function successCallback(resp) {
      //     console.log(resp.data);
      //     defer.resolve(resp.data);
      //   }, function errorCallback(err) {
      //     defer.reject(err);
      //   });
      //   return defer.promise;
      // },

      // weatherInfo: function ( locationData ) {
      //   var location = locationData;
      //   var holidayEventsCall = $.get('https://api.openweathermap.org/data/2.5/forecast/daily?lon=' + location.lon + '&lat=' + location.lat + '&cnt=10&mode=json&units=metric&appid=0702621e6f9a5bc65b5be33ddd6c5e66');
      //   holidayEventsCall.done(function (result) {
      //     console.log(result)
      //     return result;
      //   });
      //   holidayEventsCall.fail(function (resp) {
      //     return resp;
      //   });
      //   return holidayEventsCall;
      //
      // },
      removeEvent: function (id) {
        var removeEventCall = $http.post('/calendar/removeEvent',{id:id});
        removeEventCall.success(function (result) {
          return result.msg;
        });
        removeEventCall.error(function (resp) {
          return resp;
        });
        return removeEventCall;
      },
      getVolunteerInThisOccurrence:function(id, date){

        var getVolunteerInThisOccurrenceCall = $http.post('/calendar/getVolunteerInThisOccurrence',{id:id, date:date});
        getVolunteerInThisOccurrenceCall.success(function (result) {
          return result.msg;
        });
        getVolunteerInThisOccurrenceCall.error(function (resp) {
          return resp;
        });
        return getVolunteerInThisOccurrenceCall;

      },
      cancelSwapRequest:function(id){
        var cancelSwapRequestCall = $http.post('/calendar/cancelSwapRequest',{id:id});
        cancelSwapRequestCall.success(function (result) {
          return result.msg;
        });
        cancelSwapRequestCall.error(function (resp) {
          return resp;
        });
        return cancelSwapRequestCall;

      }
    };
  }


})();
