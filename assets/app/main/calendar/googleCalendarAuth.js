(function () {


  var CLIENT_ID = '574074947238-56isrrhlrl49vqbkpnocl3hkgnadqit1.apps.googleusercontent.com';

  // Array of API discovery doc URLs for APIs used by the quickstart
  var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

  // Authorization scopes required by the API; multiple scopes can be
  // included, separated by spaces.
  var SCOPES = "https://www.googleapis.com/auth/calendar.readonly";


  /**
   *  On load, called to load the auth2 library and API client library.
   */
  function handleClientLoad() {
    gapi.load('client:auth2', initClient, errHandle, timeoutError);
  }

  function initClient() {
    gapi.client.init({
      discoveryDocs: DISCOVERY_DOCS,
      clientId: CLIENT_ID,
      scope: SCOPES
    }).then(function () {
      // Listen for sign-in state changes.
      // gapi.auth2.getAuthInstance().isSignedIn.listen(location.reload());
      // Handle the initial sign-in state.
      googleClaAuthStatus = gapi.auth2.getAuthInstance().isSignedIn.get();

    }).then(function (response) {
      console.log(response);
      return true;
    }, function (err) {
      console.log(err);
      return true;
    });
  }
  //error handler for any error in client initiation
  function errHandle(err) {
    console.log(err);
  }

  function timeoutError(timeoutErr) {
    console.log(timeoutErr);
  }

  handleClientLoad();

})();
