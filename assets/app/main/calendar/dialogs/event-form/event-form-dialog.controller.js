(function () {
  'use strict';

  angular.module('app.calendar')
    .controller('EventFormDialogController', EventFormDialogController);

  /** @ngInject */
  function EventFormDialogController($mdDialog, dialogData) {
    var vm = this;

    // Data
    vm.dialogData = dialogData;
    vm.notifications = ['15 minutes before', '30 minutes before', '1 hour before'];

    // Methods
    vm.saveEvent = saveEvent;
    vm.removeEvent = removeEvent;
    vm.closeDialog = closeDialog;
    vm.hourVal = 1;
    vm.minVal = 25;

    vm.hourInDr = hourInDr;
    vm.minuteInDr = minuteInDr;


    function hourInDr(inc){
      if( inc === 'yes' && vm.hourVal <12 ){
        vm.hourVal  +=1;
      }else if( inc === 'no' && vm.hourVal >1 ){
        vm.hourVal -=1;
      }
    }
    function minuteInDr(inc){
      if( inc === 'yes' && vm.minVal <60 ){
        vm.minVal  +=1;
      }else if( inc === 'no' && vm.minVal >=1 ){
        vm.minVal -=1;
      }
    }

    init();

    //////////

    /**
     * Initialize
     */
    function init() {
      // Figure out the title
      switch (vm.dialogData.type) {
        case 'add' :
          vm.dialogTitle = 'Add Event';
          break;

        case 'edit' :
          vm.dialogTitle = 'Add Event';
          break;

        default:
          break;
      }

      // Edit
      if (vm.dialogData.calendarsecEvent) {
        // Clone the calendarsecEvent object before doing anything
        // to make sure we are not going to brake FullCalendar
        vm.calendarsecEvent = angular.copy(vm.dialogData.calendarsecEvent);

        // Convert moment.js dates to javascript date object
        if (moment.isMoment(vm.calendarsecEvent.start)) {
          vm.calendarsecEvent.start = vm.calendarsecEvent.start.toDate();
        }

        if (moment.isMoment(vm.calendarsecEvent.end)) {
          vm.calendarsecEvent.end = vm.calendarsecEvent.end.toDate();
        }
      }
      // Add
      else {
        // Convert moment.js dates to javascript date object
        if (moment.isMoment(vm.dialogData.start)) {
          vm.dialogData.start = vm.dialogData.start.toDate();
        }

        if (moment.isMoment(vm.dialogData.end)) {
          vm.dialogData.end = vm.dialogData.end.toDate();
        }

        vm.calendarsecEvent = {
          start: vm.dialogData.start,
          end: vm.dialogData.end,
          notifications: []
        };
      }
    }

    /**
     * Save the event
     */
    function saveEvent() {
      // Convert the javascript date objects back to the moment.js dates
      var dates = {
        start: moment.utc(vm.calendarsecEvent.start),
        end: moment.utc(vm.calendarsecEvent.end)
      };

      var response = {
        type: vm.dialogData.type,
        calendarsecEvent: angular.extend({}, vm.calendarsecEvent, dates)
      };

      $mdDialog.hide(response);
    }

    /**
     * Remove the event
     */
    function removeEvent() {
      var response = {
        type: 'remove',
        calendarsecEvent: vm.calendarsecEvent
      };

      $mdDialog.hide(response);
    }

    /**
     * Close the dialog
     */
    function closeDialog() {
      $mdDialog.cancel();
    }
  }
})();
