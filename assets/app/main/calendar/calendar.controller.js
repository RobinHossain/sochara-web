(function () {
  'use strict';

  angular
    .module('app.calendar')
    .controller('CalendarController', CalendarController);

  /**
   * Description
   * @ngInject
   * @method CalendarController
   * @param {} $mdDialog
   * @param {} $document
   * @param {} $http
   * @param {} $rootScope
   * @param {} ContactService
   * @param {} MailService
   * @param {} $filter
   * @param {} $scope
   * @param {} $compile
   * @param {} moment
   * @param {} CalendarService
   * @param {} Auth
   * @param {} $q
   * @param {} $timeout
   * @param {} uiCalendarConfig
   * @param {} $mdToast
   * @param {} $state
   * @return
   */
  function CalendarController($mdDialog, $document, $http, $rootScope, ContactService, MailService, $filter, $scope, $compile, moment, CalendarService, Auth, $q, $timeout, uiCalendarConfig, $mdToast, $state) {

    /*****************************************VM and initial scopes area******************************************************/
    var vm = this;
    vm.startTimeNow = moment().format('hh:mm A');
    vm.endTimeNow = moment().add(30, 'minutes').format('hh:mm A');
    vm.newEventEndDate = new Date();
    vm.eventEditData = {}
    vm.eventEditData.endDate = new Date();
    vm.newEventEndType = 'occuranceLimit';
    vm.newBirthdayEventData = {};
    vm.newBirthdayEventData.newBirthdayEventData = new Date();
    vm.calendearViewEvents = true;
    vm.calendearViewHolidays = true;
    vm.calendearViewGoogle = false;
    vm.calendearViewOutlook = false;
    vm.calendearViewBirthday = true;
    vm.discard = vm.closeDialog = closeDialog;
    vm.settings = {};
    vm.removeMailAccount = removeMailAccount;
    vm.getMailAuthUrl = getMailAuthUrl;
    vm.loadAndShowTinyCalendar = false;
    vm.isAllEventsFiltered = true;
    vm.closeContactPopup = closeContactPopup;
    vm.setWeekDaysOnly = setWeekDaysOnly;
    vm.setSignupWeekDaysOnly = setSignupWeekDaysOnly;
    vm.setEventSignupRepeatEnds = setEventSignupRepeatEnds;
    vm.setEventRepeatEnds = setEventRepeatEnds;
    vm.closePopoverHtml = closePopoverHtml;
    vm.showEventDetailsPopover = false;
    vm.changeSomeColumnForChangeView = changeSomeColumnForChangeView;
    vm.goToNextMonth = goToNextMonth;
    vm.goToPreviousMonth = goToPreviousMonth;
    vm.changeColorOfEvent = changeColorOfEvent;
    vm.getSignupCalculatedVal = getSignupCalculatedVal;
    vm.getContributorBringCount = getContributorBringCount;
    $rootScope.goToItem = goToItem;

    /**
     * Description
     * @method closeDialog
     * @return
     */
    function closeDialog() {
      $mdDialog.hide();
    }

    /**
     * Description
     * @method closeContactPopup
     * @return
     */
    function closeContactPopup() {
      vm.searchText = '';
      closeDialog();
    }

    vm.sidebarCalendarMain = [];
    vm.agendaViewClass = 'month';

    vm.saveNewEvent = saveNewEvent;
    vm.editEvent = editEvent;
    vm.deleteEvent = deleteEvent;
    vm.deleteEventConfirmation = deleteEventConfirmation;
    vm.calendarChangeView = calendarChangeView;
    vm.updateNewEvent = updateNewEvent;
    vm.addNewContacts = addNewContacts;
    vm.saveNewContact = saveNewContact;
    vm.updateEventSignup = updateEventSignup;
    vm.colorItemArr = ['c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10'];
    vm.initCalendarSocialSetting = initCalendarSocialSetting;
    vm.changeAccountSetting = changeAccountSetting;
    vm.addNewAccount = addNewAccount;
    vm.updateEvent = updateEvent;
    vm.recurringEventUpdateConfirm = recurringEventUpdateConfirm;
    vm.updateCurrentEvent = updateCurrentEvent;
    vm.showBirthdayEvent = true;
    vm.showHolidayEvent = true;
    vm.showEventsOnly = true;
    vm.filterAllEvents = filterAllEvents;
    vm.toogleRepeatDaySelection = toogleRepeatDaySelection;
    vm.gotoDay = gotoDay;
    vm.goToToday = goToToday;
    $rootScope.searchable = true;

    vm.timeSlot = [
      {name: '12:00am', value: 'T000000'}, {name: '12:30am', value: 'T003000'}, {name: '1:00am', value: 'T010000'},{name: '1:30am', value: 'T013000'},
      {name: '2:00am', value: 'T020000'}, {name: '2:30am', value: 'T023000'}, {name: '3:00am', value: 'T030000'},{name: '3:30am', value: 'T033000'},
      {name: '4:00am', value: 'T040000'}, {name: '4:30am', value: 'T043000'}, {name: '5:00am', value: 'T050000'},{name: '5:30am', value: 'T053000'},
      {name: '6:00am', value: 'T060000'}, {name: '6:30am', value: 'T063000'}, {name: '7:00am', value: 'T070000'},{name: '7:30am', value: 'T073000'},
      {name: '8:00am', value: 'T080000'}, {name: '8:30am', value: 'T083000'}, {name: '9:00am', value: 'T090000'},{name: '9:30am', value: 'T093000'},
      {name: '10:00am', value: 'T100000'}, {name: '10:30am', value: 'T103000'}, {name: '11:00am', value: 'T110000'},{name: '11:30am', value: 'T113000'},
      {name: '12:00pm', value: 'T120000'}, {name: '12:30pm', value: 'T123000'}, {name: '1:00pm', value: 'T130000'},{name: '1:30pm', value: 'T133000'},
      {name: '2:00pm', value: 'T140000'}, {name: '2:30pm', value: 'T143000'}, {name: '3:00pm', value: 'T150000'},{name: '3:30pm', value: 'T153000'},
      {name: '4:00pm', value: 'T160000'}, {name: '4:30pm', value: 'T163000'}, {name: '5:00pm', value: 'T170000'},{name: '5:30pm', value: 'T173000'},
      {name: '6:00pm', value: 'T180000'}, {name: '6:30pm', value: 'T183000'}, {name: '7:00pm', value: 'T190000'},{name: '7:30pm', value: 'T193000'},
      {name: '8:00pm', value: 'T200000'}, {name: '8:30pm', value: 'T203000'}, {name: '9:00pm', value: 'T210000'},{name: '9:30pm', value: 'T213000'},
      {name: '10:00pm', value: 'T220000'}, {name: '10:30pm', value: 'T223000'}, {name: '11:00pm', value: 'T230000'},{name: '11:30pm', value: 'T233000'},
    ];

    /**
     * ***************************************Adding New Event Area**********************************************
     * @method initCalendarSocialSetting
     * @return
     */
    function initCalendarSocialSetting() {
      vm.mailAccounts = [];
      vm.checkedAccounts = {};
      vm.loadingMailAccount = true;
      MailService.getMailAddresses().then(function (response) {
        if (response && response.length) {
          response.forEach(function (item) {
            // item.selected = Boolean(vm.settings.showEventsFrom) && vm.settings.showEventsFrom.includes(item.id);
            vm.mailAccounts.push(item);
            vm.checkedAccounts[item.id] = true;
            vm.loadingMailAccount = false;
          });
        } else {
          vm.loadingMailAccount = false;
        }
      });
    }

    /**
     * Description
     * @method removeMailAccount
     * @param {} account
     * @return
     */
    function removeMailAccount(account) {
      if (account.email) {
        MailService.deleteCurrentAccount({email: account.email}).then(function (response) {
          if (response) {
            $state.reload();
            showMessage('The Account has been removed', 'success');
            closeDialog();
          }
        });
      }
    }

    /**
     * Description
     * @method goToNextMonth
     * @return
     */
    function goToNextMonth(){
      vm.calendar.next();
      if(vm.agendaViewClass!=='month'){
        vm.changeSomeColumnForChangeView();
      }
    }

    /**
     * Description
     * @method goToPreviousMonth
     * @return
     */
    function goToPreviousMonth(){
      vm.calendar.prev();
      if(vm.agendaViewClass!=='month'){
        vm.changeSomeColumnForChangeView();
      }
    }

    /**
     * Description
     * @method changeColorOfEvent
     * @return
     */
    function changeColorOfEvent(){
      var updateEvent = {}; updateEvent.color = vm.eventDetails.color; updateEvent.id = vm.eventDetails.id;
      CalendarService.updateEventOptions(updateEvent).then(function (resp) {
        if (resp && typeof resp === 'object') {
          var eventIndex = getCurrentElementIndexById(vm.events[0], resp.id);
          vm.events[0][eventIndex] = resp;
          showMessage('Event Color Updated.');
        }
      });
    }

    vm.confirmShowEventForEmail = confirmShowEventForEmail;

    /**
     * Description
     * @method confirmShowEventForEmail
     * @param {} email
     * @return
     */
    function confirmShowEventForEmail(email) {
      // console.log(email);
      console.log(email);
    }

    /**
     * Description
     * @method changeAccountSetting
     * @return
     */
    function changeAccountSetting() {
      var calendarAccounts = [];
      vm.mailAccounts.forEach(function (item) {
        if (item.selected === true) {
          calendarAccounts.push(item.id);
        }
      });
      var calendarSettnigs = {};
      calendarSettnigs.showApiEvents = true;
      calendarSettnigs.showEventsFrom = calendarAccounts;

      CalendarService.setSettings(calendarSettnigs).then(function (resp) {
        if (resp && typeof resp === 'object') {
          vm.settings = resp;
          getApiEvents();
        }
      });
    }

    /**
     * Description
     * @method getMailAuthUrl
     * @return
     */
    function getMailAuthUrl() {
      if (vm.selectedAddAccount === 'gmail') {
        MailService.GMailAuthUrl().then(function (response) {
          if (response) {
            vm.MailAuthUrl = response;
          }
        });
      } else if (vm.selectedAddAccount === 'outlook') {
        MailService.OutlookAuthUrl().then(function (response) {
          if (response) {
            vm.MailAuthUrl = response;
          }
        });
      }
    }

    /**
     * Description
     * @method addNewAccount
     * @return
     */
    function addNewAccount() {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/mail/dialogs/add/account.html',
        clickOutsideToClose: false
      });
      Auth.lastUrl({module: 'calendar'}).then(function (respUrl) {
        console.log(respUrl);
      });
    }

    /**
     * Description
     * @method getApiEvents
     * @return
     */
    function getApiEvents() {
      CalendarService.getApiEvents().then(function (resp) {
        console.log(resp);
      });
    }

    vm.changeEventTime = changeEventTime;

    /**
     * Description
     * @method changeEventTime
     * @return
     */
    function changeEventTime() {
      // if( vm.eventDetails.start.toDateString() ===  vm.eventDetails.end.toDateString()){
      //   vm.eventDetails.end.setHours ( vm.eventDetails.end.getHours() + 1 );
      //   console.log(vm.eventDetails.end);
      // }
    }

    vm.changeViewEvent = changeViewEvent;

    /**
     * Description
     * @method changeViewEvent
     * @param {} type
     * @return
     */
    function changeViewEvent( type ) {
      $rootScope.loadingProgress = true;
      removeOrAddElement(vm.eventTypeArr, type, function ( eventArr ) {
        loadEventForTypes( eventArr, function ( resp ) {
          uiCalendarConfig.calendars['main'].fullCalendar( 'removeEvents' );
          vm.events[0] = resp;
          $rootScope.loadingProgress = false;
        });
      });
    }

    /**
     * Description
     * @method removeOrAddElement
     * @param {} array
     * @param {} element
     * @param {} callback
     * @return
     */
    function removeOrAddElement(array, element, callback) {
      var index = array.indexOf(element);
      if (index !== -1) {
        array.splice(index, 1);
      } else {
        array.push(element);
      }
      callback(array);
    }

    /**
     * Description
     * @method loadEventForTypes
     * @param {} eventArr
     * @param {} callback
     * @return
     */
    function loadEventForTypes(eventArr, callback) {
      var currentEvents = vm.allEvents.filter(function (event) {
        return eventArr.indexOf(event.type) > -1;
      });
      callback(currentEvents);
    }

    vm.showEventsForAccounts = showEventsForAccounts;
    /**
     * Description
     * @method showEventsForAccounts
     * @param {} account
     * @return
     */
    function showEventsForAccounts( account ){
      var currentEvents;
      $rootScope.loadingProgress = true;
      if(vm.checkedAccounts[account.id]){
        currentEvents = vm.allEvents.filter(function (event) {
          return event.email !== account.email;
        });
      }else{
        currentEvents = vm.allEvents;
      }
      uiCalendarConfig.calendars['main'].fullCalendar( 'removeEvents' );
      vm.events[0] = currentEvents;
      $rootScope.loadingProgress = false;
    }


    /**
     * Description
     * @method initNewEvent
     * @return
     */
    function initNewEvent() {
      vm.eventDetails.signup = [{
        job: '',
        needed: 1,
        available: '',
        start: vm.eventDetails.start,
        end: vm.eventDetails.end,
        color: 'c1'
      }];
      vm.eventDetails.signupRepeat = {
        type: 'daily',
        interval: 1,
        days: [],
        end: {never: false, on: true, after: false, afterValue: 5, onDate: vm.eventDetails.end}
      };
      vm.eventDetails.signupReminder = [{name: 'First', type: 'hours', yes: false, value: 1}, {
        name: 'First',
        type: 'days',
        yes: false,
        value: 1
      }, {name: 'First', type: 'years', yes: false, value: 1}];
      vm.eventDetails.reminder = [{type: 'notification', duration: 30, format: 'minutes', user: vm.user.id}];
      vm.eventDetails.showMeAs = 'available';
      vm.eventDetails.repeat = {
        type: 'daily',
        interval: 1,
        days: [],
        end: {never: false, on: true, after: false, afterValue: 5, onDate: vm.eventDetails.end}
      };
      vm.newEventSignupJobs = [];

      vm.eventDetails = vm.eventDetails;
      vm.eventDetails.dialogTitle = 'Create A New Event';
      initDefaultFieldsForDialog();
      addDefaultAttendees();
    }

    /**
     * Description
     * @method initDefaultFieldsForDialog
     * @return
     */
    function initDefaultFieldsForDialog() {
      vm.signupReminderTypes = ['hours', 'days', 'years'];
      vm.eventWeekDays = [{id:6, name: 'Sat'},{id:0, name: 'Sun'},{id:1, name: 'Mon'},{id:2, name: 'Tue'},{id:3, name: 'Wed'},{id:4, name: 'Thu'},{id:5, name: 'Fri'}];
    }

    /**
     * Description
     * @method initUpdateEvent
     * @return
     */
    function initUpdateEvent() {
      vm.eventStartTime = {};
      vm.eventEndTime = {};
      if(vm.eventDetails.repeat){
        vm.eventDetails.repeat.end.onDate = new Date(vm.eventDetails.repeat.end.onDate);
      }
      if(vm.eventDetails.signupRepeat){
        vm.eventDetails.signupRepeat.end.onDate = new Date(vm.eventDetails.signupRepeat.end.onDate);
      }
      vm.eventDetails.dialogTitle = 'Update Event';
      initDefaultFieldsForDialog();
    }

    /**
     * Description
     * @method addDefaultAttendees
     * @return
     */
    function addDefaultAttendees() {
      vm.eventDetails.attendees = vm.eventDetails.attendees ? vm.eventDetails.attendees.length = 0 : [];
      if (vm.user && typeof vm.user === 'object') {
        vm.eventDetails.attendees.push({
          name: vm.user.first_name + ' ' + vm.user.last_name,
          email: vm.user.email,
          role: 'organizer',
          photo_url: vm.user.photo_url ? vm.user.photo_url : 'images/avatars/profile.jpg',
          id: vm.user.id,
          acceptance: 'yes'
        });
      }
    }


    /**
     * Description
     * @method setWeekDaysOnly
     * @return
     */
    function setWeekDaysOnly() {
      vm.eventDetails.repeat.days = vm.eventDetails.repeat.weekOnly ? [1, 2, 3, 4, 5] : [];
    }

    /**
     * Description
     * @method setSignupWeekDaysOnly
     * @return
     */
    function setSignupWeekDaysOnly() {
      vm.eventDetails.signupRepeat.days = vm.eventDetails.signupRepeat.weekOnly ? [1, 2, 3, 4, 5] : [];
    }

    /**
     * Description
     * @method setEventSignupRepeatEnds
     * @param {} value
     * @return
     */
    function setEventSignupRepeatEnds(value) {
      vm.eventDetails.signupRepeat.end.never = false;
      vm.eventDetails.signupRepeat.end.on = false;
      vm.eventDetails.signupRepeat.end.after = false;
      vm.eventDetails.signupRepeat.end[value] = true;
    }

    /**
     * Description
     * @method setEventRepeatEnds
     * @param {} value
     * @return
     */
    function setEventRepeatEnds(value) {
      vm.eventDetails.repeat.end.never = false;
      vm.eventDetails.repeat.end.on = false;
      vm.eventDetails.repeat.end.after = false;
      vm.eventDetails.repeat.end[value] = true;
    }

    /**
     * Description
     * @method saveNewEvent
     * @return
     */
    function saveNewEvent() {
      if (!vm.eventDetails.title) {
        showMessage('Please input title for event');
        return;
      }

      if(!(vm.eventDetails.start && vm.eventDetails.end)){
        showMessage('Please select correct date!');
        return;
      }

      if(!vm.eventDetails.allDay){
        if(!(vm.eventStartTime && vm.eventStartTime.value && vm.eventEndTime && vm.eventEndTime.value)){
          showMessage('Please select correct time!');
          return;
        }else{
          vm.eventDetails.start = new Date(moment(vm.eventDetails.start).format('YYYY-MM-DD') + ' ' + vm.eventStartTime.value.replace('T', '').replace(/.{2}/g, "$&" + ":"));
          vm.eventDetails.end = new Date(moment(vm.eventDetails.end).format('YYYY-MM-DD') + ' ' + vm.eventEndTime.value.replace('T', '').replace(/.{2}/g, "$&" + ":"));
        }
      }



      vm.eventDetails.place = angular.element('#getLocationByGoogle').val();

      CalendarService.createNewCalendarEvent(vm.eventDetails).then(function (resp) {
        if (resp && typeof resp === 'object') {
          showMessage('Event saved successfully');
          $mdDialog.hide();
          resp.stick = true;
          vm.allEvents.push(resp);
          // uiCalendarConfig.calendars['main'].fullCalendar( 'renderEvent', resp );
          vm.events[0] = vm.allEvents;
        }
      });

    }



    io.socket.on('event_notification', function (resp) {
      showMessage(resp.data, 'success');
      // showMessage()
    });

    /**
     * Description
     * @method init
     * @return
     */
    function init() {
      $scope.eventSources = [];
      vm.eventTypeArr = ['holiday', 'birthday', 'event'];

      initCalendarData();

      $timeout(function () {
        vm.loadingCalendar = false;
        loadCalendarConfig();
        setCalendarViewPosition();
        // loadExtarnalJavascript();
      });
    }

    /**
     * Description
     * @method initCalendarData
     * @return
     */
    function initCalendarData(){
      CalendarService.getCalendarInitData().then(function (resp) {
        vm.settings = resp.settings;
        vm.user = resp.user;
        vm.contacts = resp.contacts;
        vm.allEvents = resp.events;
        vm.events[0] = vm.allEvents.slice();
        vm.sidebarCalendarMain = resp.events.map(function (event) {
          return {start: event.start, title: event.title, endDate: null};
        });
        fullCalendarExtendDayView(4);
        vm.filteredContacts = resp.contacts.map(function (c) {
          var contact = {
            name: c.first_name + ' ' + c.last_name,
            email: c.sochara_user_id && typeof c.sochara_user_id === 'object' ? c.sochara_user_id.email : c.email,
            photo_url: c.sochara_user_id && typeof c.sochara_user_id === 'object' ? c.sochara_user_id.photo_url.replace('assets', '').replace(/\\/, '/') : '../images/avatars/blank.jpg',
            id: c.sochara_user_id && typeof c.sochara_user_id ? c.sochara_user_id.id : undefined,
            acceptance: 'pending',
          };
          // contact._lowername = contact.name.toLowerCase();
          return {
            value: contact.name.toLowerCase(),
            display: contact.name,
            image: contact.photo_url,
            isContact: true,
            id: contact.id,
            attendeeData: contact
          };
        });
        $rootScope.items = resp.events.map(function (event) {
          return {name: event.title, id: event.id, module: 'calendar', date: event.start};
        });
      });
    }


    /**
     * Description
     * @method fullCalendarExtendDayView
     * @param {} days
     * @return
     */
    function fullCalendarExtendDayView(days) {
      $.fullCalendar.views.agendaFourDay = {
        type: 'agenda',
        duration: {days: days},
        columnFormat: 'ddd D'
      };
      $.fullCalendar.views.agendaTenDay = {
        type: 'agenda',
        duration: {days: 10},
        columnFormat: 'ddd D'
      };
      $.fullCalendar.views.agendaDay = {
        type: 'agenda',
        columnFormat: 'ddd D'
      };
    }

    init();

    /**
     * Description
     * @method getSettings
     * @return
     */
    function getSettings() {
      CalendarService.getSettings().then(function (resp) {
        if (resp && typeof resp === 'object') {
          vm.settings = resp;
        }
      });
    }



    /**
     * Description
     * @method showMessage
     * @param {} message
     * @param {} type
     * @param {} position
     * @return
     */
    function showMessage(message, type, position) {
      type = type || 'success';
      position = position || 'bottom';
      $mdToast.show({
        template: '<md-toast class="md-toast ' + type + ' ">' + message + '</md-toast>',
        hideDelay: 3000,
        position: 'bottom left'
      });
    }

    vm.newEvent = {};
    vm.newEvent.showAs = 'available';
    vm.newEvent.repeat = false;
    vm.showEventDuration = true;
    vm.newEventStartTime = 3;

    vm.showIncludeFrom1 = true;
    vm.showIncludeTo1 = true;

    /**
     * New Event Signup array
     * @method addContributionBringing
     * @param {} item
     * @return
     */
    vm.addContributionBringing = function (item) {
      if (item.needed - item.preAvailable + item.preBring - 1 < item.bringing) {
        alert('You have reached to maximum amount');
        item.bringing = item.needed - item.preAvailable + item.preBring;
      } else {
        item.bringing = item.bringing + 1;
        item.available = item.available + 1;
      }
    };
    /**
     * Description
     * @method reduceContributionBringing
     * @param {} item
     * @return
     */
    vm.reduceContributionBringing = function (item) {
      if (item.bringing < 1) {
        alert('You have reached to minimum amount');
        item.bringing = 0;
      } else {
        item.bringing = item.bringing - 1;
        item.available = item.available - 1;
      }
    };

    /**
     * New Event Signup array
     * @method jobColorSelector
     * @param {} item
     * @return
     */
    vm.jobColorSelector = function (item) {
      if (item.color === 'c1') {
        return '#54edeb !important;';
      }
      if (item.color === 'c2') {
        return '#a4fafc !important;';
      }
      if (item.color === 'c3') {
        return '#46db78 !important;';
      }
      if (item.color === 'c4') {
        return '#81e77a !important;';
      }
      if (item.color === 'c5') {
        return '#9eb749 !important;';
      }
      if (item.color === 'c6') {
        return '#fb675b !important;';
      }
      if (item.color === 'c7') {
        return '#ff7896 !important;';
      }
      if (item.color === 'c8') {
        return '#ff7ccb !important;';
      }
      if (item.color === 'c9') {
        return '#dc21a9 !important;';
      }
      if (item.color === 'c10') {
        return '#adb8ff !important;';
      }
      if (item.color === 'c11') {
        return '#e1e1e1 !important;';
      }
    };
    //Loading Event Constants will be available in all views in calendar
    vm.eventConst = {};
    vm.eventConst.today = new Date();

    //New Event constants
    vm.newEventReminder = [{type: '', duration: '', format: ''}];


    vm.newEventAttendeeRole = ['organizer', 'coordinator', 'volunteer'];


    vm.showAccountSettingDialogue = showAccountSettingDialogue;

    /**
     * Description
     * @method showAccountSettingDialogue
     * @return
     */
    function showAccountSettingDialogue() {
      showDialogue('app/main/calendar/dialogs/settings/account.html', false);
    }


    /**
     * Description
     * @method getUserInfo
     * @return
     */
    function getUserInfo() {
      Auth.info().then(function (resp) {
        vm.user = resp;
      });
    }


    /**
     * Description
     * @method itemAvailable
     * @param {} all
     * @param {} job
     * @return ConditionalExpression
     */
    vm.itemAvailable = function (all, job) {
      var returnAll = all;
      vm.volunteerContributionArray.forEach(function (t) {
        var newBringing = t.bringing;
        vm.volunteerContributionArrayOld.forEach(function (t2) {
          var oldBringing = t2.bringing;
          if (t2.job === t.job === job) {
            returnAll = returnAll - oldBringing + newBringing;
          }
        });
      });
      return returnAll ? returnAll : 0;
    };
    /**
     * Description
     * @method saveNewBirthdayEvent
     * @return
     */
    vm.saveNewBirthdayEvent = function () {
      if (!vm.newBirthdayEventData.first_name || !vm.newBirthdayEventData.last_name) {
        showMessage('Please fill the name fields!');
        return;
      }
      vm.newBirthdayEventData.photo_url = '';
      Auth.addNewContact(vm.newBirthdayEventData).success(function () {
        $mdDialog.hide();
        showMessage('Contact has been saved successfully!');
        $state.reload();
      }).error(function () {
        $mdDialog.hide();
        showMessage('Please fill the name fields!');
      });
    }
    /**
     * Description
     * @method addBirthdayEvent
     * @return
     */
    vm.addBirthdayEvent = function () {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: '/app/main/calendar/dialogs/event-detail/so-event-add-dialog-new-birthday.html',
        clickOutsideToClose: true,
        // targetEvent: ev,
      });
    };

    /**
     * ***************************************VM and initial scopes area*****************************************************
     * @method RepeatIntervalList
     * @return list
     */
    vm.RepeatIntervalList = function () {
      var list = [];
      for (var i = 1; i < 31; i++) {
        list.push(i);
      }
      return list;
    }
    /********************************************Initial Calendar Variables declaration***************************************/
    vm.today = new Date(); //today data
    vm.events = [[]]; //initializing events array for ui-calendar !important
    vm.newEventRepeatWeekDay = {};
    /**
     * Description
     * @method dateParser
     * @param {} item
     * @return NewExpression
     */
    vm.dateParser = function (item) {
      return new Date(item);
    };
    /**
     * Description
     * @method timeParser
     * @param {} item
     * @return CallExpression
     */
    vm.timeParser = function (item) {
      return moment(item).format('h:mm A');
    };
    /**
     * Updateing response
     * @method responseOrganizerChange
     * @return
     */
    vm.responseOrganizerChange = function () {
      CalendarService.responseOrganizerChange(vm.eventDetails.id, vm.eventDetails.showMeAs).success(function () {
        showMessage('Response updated successfully');
      }).error(function () {

      });
    };

    /**
     * Description
     * @method contributionToSignup
     * @param {} item
     * @param {} volunteer
     * @return contribution
     */
    vm.contributionToSignup = function (item, volunteer) {
      var contribution = 0;
      volunteer.forEach(function (i) {
        if (i.id === vm.user.id) {
          i.id.contribution.forEach(function (t) {
            if (item.job === t.job) {
              contribution = t.bringing;
            }
          });
        }
      });
      return contribution;
    };

    /**
     * Description
     * @method saveContribution
     * @return
     */
    vm.saveContribution = function () {
      vm.eventEditData.attendees.forEach(function (item) {
        if (item.id === vm.user.id && item.role === vm.eventEditData.contributionType) {
          CalendarService.saveAttendeeCmntColor(vm.eventEditData.id, vm.user.id, item).success(function () {
            if (vm.checkIfVolunteer() === true) {
              CalendarService.saveContribution(vm.eventEditData.id, vm.user.id, vm.volunteerContributionArray, vm.volunteerEventScope, vm.eventEditData.start).success(function () {
                showMessage('Event updated successfully!');
                $mdDialog.hide();
                $state.reload();
              }).error(function () {
                showMessage('Oops! Something went wrong!');
              });
            } else {
              //success as color updated
              showMessage('Event updated successfully!');
              $mdDialog.hide();
              $state.reload();
            }
          }).error(function () {

          });
        }
      });
    };
    /**
     * Description
     * @method responseContributorChange
     * @param {} res
     * @return
     */
    vm.responseContributorChange = function (res) {
      CalendarService.responseContributorChange(vm.eventEditData.id, vm.user.id, res).success(function () {
        showMessage('Response updated successfully!');
      }).error(function () {

      });
    };

    /**
     * Description
     * @method checkIfCoordinate
     * @return flag
     */
    vm.checkIfCoordinate = function () {
      var flag = false;
      vm.eventEditData.attendees.forEach(function (item) {
        if (item.acceptance === 'yes' && item.id === vm.user.id && vm.eventEditData.contributionType === 'volunteer' && vm.eventEditData.nAndP[0].nAndP.newEventPermissionCoEdit) {
          flag = true;
        }
      });
      return flag;
    };

    /**
     * Description
     * @method swapEvent
     * @return
     */
    vm.swapEvent = function () {
      vm.volunteerInThisOccurrenceFunction();
      vm.swapWithThisUer = {};
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/calendar/dialogs/event-form/so-create-signup-swap.html',
        clickOutsideToClose: true,
        // targetEvent: ev,
      });
    };
    /**
     * vm.ifAlreadySwapRequested = function (curUserId) {
     * var flag = false;
     * vm.swapRequested.forEach(function (m) {
     * console.log(moment(m.eventScopeDate).format('DD/MM/YYYY'));
     * console.log(moment(vm.eventEditData.start).format('DD/MM/YYYY'));
     * if (m.swapRequestFrom.userId === vm.user.id && moment(vm.eventEditData.start).format('DD/MM/YYYY') === moment(m.eventScopeDate).format('DD/MM/YYYY') && m.eventId === vm.eventEditData.id && m.swapRequestTo.userId === curUserId) {
     * flag = true;
     * vm.swapWithThisUer.userId = curUserId;
     * }
     * });
     * return flag;
     * };
     * @method swapRequest
     * @param {} item
     * @param {} swapRequestToDate
     * @return
     */
    vm.swapRequest = function (item, swapRequestToDate) {
      console.log(item)
      console.log(swapRequestToDate);
      var data = {};
      data.eventId = item.eventId;
      data.eventScopeDate = item.eventScopeDate;
      data.swapRequestFrom = {
        name: vm.user.first_name + ' ' + vm.user.last_name, userId: vm.user.id,
        photo_url: vm.user.photo_url ? vm.user.photo_url : 'images/avatars/profile.jpg',
        email: vm.user.email
      };
      data.swapRequestTo = item;
      data.swapRequestToDate = swapRequestToDate;
      CalendarService.swapRequest(data).success(function () {
        $mdDialog.hide();
        $state.reload();
        showMessage('Swap Requested successfully');
      }).error(function () {
        $mdDialog.hide();
        showMessage('Something went wrong');
      });
    };
    /**
     * Description
     * @method checkIfVolunteerHas
     * @return Literal
     */
    vm.checkIfVolunteerHas = function () {
      if (vm.filteredAttendeeContribution.length > 1) {
        return true;
      }
      if (vm.filteredAttendeeContribution.length === 1 && vm.filteredAttendeeContribution[0].userId === vm.user.id) {
        return false;
      }
      return false;
    };

    /**
     * Description
     * @method checkIfVolunteer
     * @return flag
     */
    vm.checkIfVolunteer = function () {
      var flag = false;
      vm.eventEditData.attendees.forEach(function (item) {
        if (item.acceptance === 'yes' && item.id === vm.user.id && vm.eventEditData.contributionType === 'volunteer' && vm.eventEditData.isSignupChecked) {
          flag = true;
        }
      });
      return flag;
    };
    /**
      * Description
      * @description going to specific date and changing view of calendar
      * @function gotoDay
      * @method gotoDay
      * @param date utc date format
      * @return
      */
     function gotoDay(date) {
      compareDateWithCurrent(date, function (respResult) {
        if( respResult && respResult.data ){
          uiCalendarConfig.calendars['main'].fullCalendar('gotoDate', date);
          vm.calendar.changeView('agendaDay');
          vm.agendaViewClass='day';
          vm.changeSomeColumnForChangeView();
          continiousDayCss();
        }
      });
    }

    /*CSS for Continious Day Event*/

    /**
     * Add Css For continous days
     * @method continiousDayCss
     * @return
     */
    function continiousDayCss() {
      $timeout(function () {
        angular.element('.fc-agendaDay-view .fc-not-end').parent().css('padding-right', '24px');
        angular.element('.fc-agendaDay-view .fc-not-end').parent().addClass('addEndingIcon');
        angular.element('.fc-agendaDay-view .fc-not-start').parent().css('padding-left', '24px');
        angular.element('.fc-agendaDay-view .fc-not-start').parent().addClass('addStartIcon');
      }, 50);
    }

    /**
     * Description
     * @method compareDateWithCurrent
     * @param {} date
     * @param {} callback
     * @return
     */
    function compareDateWithCurrent( date, callback ){
      var target = moment(date).format('DD-MM-YYYY');
      var current = uiCalendarConfig.calendars['main'].fullCalendar('getDate').format('DD-MM-YYYY');
      callback({data: target !== current});
    }

    /**
     * Description
     * @method goToToday
     * @return
     */
    function goToToday(){
      var currentView = vm.calendar.getView().name;
      vm.calendarView.calendar.today();
      if( currentView !== 'month'){
        changeSomeColumnForChangeView();
      }
    }

    // vm.gotoFourDayView = function (date) {
    //   uiCalendarConfig.calendars['main'].fullCalendar('gotoDate', date);
    //   vm.calendar.changeView('agendaThreeDay');
    //   vm.continiousDayCss();
    // };


    /*****************************************VM area******************************************************/

    /**
     * ***************************************Adding Fetched Events to Calendar**********************************************
     * @method calendarChangeView
     * @param {} type
     * @return
     */
    function calendarChangeView( type ){
      var currentView = vm.calendar.getView().name;
      // console.log(vm.calendar.getView());
      if( type === 'agendaDay' && currentView !== 'agendaDay' ){
        vm.calendar.changeView('agendaDay');
        vm.agendaViewClass='day';
        $timeout(function () { changeSomeColumnForChangeView(); });
        removeCalendarStrictHeightPosition();
      }else if( type === 'agendaWeek' && currentView !== 'agendaWeek' ){
        vm.calendar.changeView('agendaWeek');
        vm.agendaViewClass='agendaWeek';
        $timeout(function () { changeSomeColumnForChangeView(); });
        removeCalendarStrictHeightPosition();
      }else if( type === 'month' && currentView !== 'month' ){
        vm.calendar.changeView('month');
        vm.agendaViewClass='month';
        setCalendarViewPosition();
      }else if( type === 'agendaFourDay' && currentView !== 'agendaFourDay'){
        vm.calendar.changeView('agendaFourDay');
        vm.agendaViewClass='agendaFourDay';
        $timeout(function () { changeSomeColumnForChangeView(); });
        removeCalendarStrictHeightPosition();
      }else if( type === 'agendaTenDay' && currentView !== 'agendaTenDay'){
        vm.calendar.changeView('agendaTenDay');
        vm.agendaViewClass='agendaWeek';
        $timeout(function () { changeSomeColumnForChangeView(); });
        removeCalendarStrictHeightPosition();
      }else if( type === 'today'){
        vm.calendarView.calendar.today();
        if( currentView !=='month' ){
          $timeout(function () { changeSomeColumnForChangeView(); });
        }
      }
    }

    /**
     * Description
     * @method changeSomeColumnForChangeView
     * @return
     */
    function changeSomeColumnForChangeView(){
      var fcDayHeader = angular.element('.fc-day-header');
      var dayOfToday = moment().format('ddd');
      if( !angular.element('span.cal_w_num').length ){
        fcDayHeader.each(function() {
          var spictedWord = angular.element( this ).text().split(/\s+/);
          var toDayClass = spictedWord[0] === dayOfToday ? 'cal_w_today':'';
          var generateHtml = spictedWord[0] + '<span class="cal_w_num '+toDayClass+'">' + spictedWord[1] + '</span>';
          angular.element( this ).html(generateHtml);
        });
        var findIndex = angular.element(".fc-today.fc-state-highlight");
        var getTdIndex = angular.element(".fc-week.fc-widget-content tbody tr td").index(findIndex);
        if( getTdIndex > -1){
          angular.element("thead.fc-head .fc-widget-header tr th").eq(getTdIndex).addClass('today');
        }
      }
    }

    /**
     * Description
     * @method deleteEvent
     * @param {} id
     * @return
     */
    function deleteEvent( id ) {
      closePopoverHtml();
      showDialogue('/app/main/calendar/dialogs/event/delete-confirmation.html');
    }

    /**
     * Description
     * @method deleteEventConfirmation
     * @param {} id
     * @return
     */
    function deleteEventConfirmation( id ){
      id = id || vm.eventDetails.id;
      CalendarService.deleteEvent({id: id}).then(function ( respDeletedEvents ) {
        // vm.allEvents = vm.events[0];

        var allDeletedEvents = respDeletedEvents.map(function (event) {
          return event.id;
        });
        if( allDeletedEvents ){
          uiCalendarConfig.calendars['main'].fullCalendar( 'removeEvents' );
          vm.events[0] = vm.allEvents.filter(function ( item ) {
            // return item.id !== respDeletedEvent.id;
            return allDeletedEvents.indexOf(item.id) < 0;
          });
          loadCalendarConfig();
          showMessage('The Event has been deleted !');
          vm.allEvents = vm.events[0].slice();
        }
      });
    }

    /**
     * Description
     * @method removeElementsById
     * @param {} elements
     * @param {} id
     * @return CallExpression
     */
    function removeElementsById(elements, id) {
      // return elements.filter(item => item.id !== id);
      return elements.filter(function ( item ) {
        return item.id !== id;
      });
    }

    /**
     * All Holiday Events
     * @method editEvent
     * @return
     */
    function editEvent() {
      vm.showEventDetailsPopover = false;
      /*Changing date and time format*/
      // console.log(vm.eventEditData);
      initUpdateEvent();

      // vm.eventStartTime.name = moment(vm.eventDetails.start).format('h:mma');
      // vm.eventEndTime.name = moment(vm.eventDetails.end).format('h:mma');

      vm.eventStartTime = vm.timeSlot.getBy('name', moment(vm.eventDetails.start).format('h:mma'));
      vm.eventEndTime = vm.timeSlot.getBy('name', moment(vm.eventDetails.end).format('h:mma'));

      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: '/app/main/calendar/dialogs/event-form/so-create-event-dialog.html',
        clickOutsideToClose: false,
        // targetEvent: ev,
      });

    }

    /**
     * Description
     * @method updateCurrentEvent
     * @return
     */
    function updateCurrentEvent(){

    }

    /**
     * Description
     * @method getCurrentElementIndexById
     * @param {} elements
     * @param {} eId
     * @return returnIndex
     */
    function getCurrentElementIndexById( elements, eId ){
      var returnIndex = -1;
      elements.forEach(function (val, key) {
        if( val.id === eId ) returnIndex = key;
      });
      return returnIndex;
    }

    /**
     * Description
     * @method updateEvent
     * @return
     */
    function updateEvent() {

      if(!(vm.eventDetails.start && vm.eventDetails.end && vm.eventStartTime && vm.eventEndTime)){
        showMessage('Please select correct date and time');
        return;
      }

      if(!vm.eventDetails.allDay){
        if(vm.eventStartTime && vm.eventStartTime.value && vm.eventEndTime && vm.eventEndTime.value){
          vm.eventDetails.start = new Date(moment(vm.eventDetails.start).format('YYYY-MM-DD') + ' ' + vm.eventStartTime.value.replace('T', '').replace(/.{2}/g, "$&" + ":"));
          vm.eventDetails.end = new Date(moment(vm.eventDetails.end).format('YYYY-MM-DD') + ' ' + vm.eventEndTime.value.replace('T', '').replace(/.{2}/g, "$&" + ":"));
        }
      }

      if( vm.eventBerofeModify.isRepeated ){
        vm.saveEventFor = 'this';
        vm.eventDetails.place = angular.element('#getLocationByGoogle').val();
        showDialogue('/app/main/calendar/dialogs/event/update-type.html');
      }else{
        CalendarService.updateEvent(vm.eventDetails).then(function (updatedEvent) {
          closeDialog();
          if(Object.prototype.toString.call(updatedEvent) === '[object Object]'){
            updateEventFor(updatedEvent);
          }else{
            showMessage(updatedEvent, 'error');
          }
        });
      }
    }

    /**
     * Description
     * @method recurringEventUpdateConfirm
     * @return
     */
    function recurringEventUpdateConfirm(){
      var eventDetails = vm.eventDetails;
      eventDetails.updateFor = vm.saveEventFor;
      CalendarService.updateRecurringEvent(eventDetails).then(function (updatedEvents) {
        closeDialog();
        if(Object.prototype.toString.call(updatedEvents) === '[object Object]'){
          updateEventFor(updatedEvents);
        }else if(Object.prototype.toString.call(updatedEvents) === '[object Array]'){
          var eventIndex;
          updatedEvents.forEach(function (event) {
            eventIndex = getCurrentElementIndexById(vm.allEvents, event.id);
            if(eventIndex>-1){
              vm.allEvents[eventIndex] = event;
            }else{
              vm.allEvents.push(event);
            }
          });
          uiCalendarConfig.calendars['main'].fullCalendar( 'removeEvents' );
          vm.events[0] = [];
          $timeout(function () {
            vm.events[0] = vm.allEvents.slice();
          });
          showMessage('Recurring event updated');
        }
      });
    }

    /**
     * Description
     * @method updateEventFor
     * @param {} updatedEvent
     * @return
     */
    function updateEventFor( updatedEvent ){
      var eventIndex = getCurrentElementIndexById(vm.allEvents, updatedEvent.id);
      vm.allEvents[eventIndex] = updatedEvent;
      uiCalendarConfig.calendars['main'].fullCalendar( 'removeEvents' );
      vm.events[0] = vm.allEvents.slice();
      showMessage('Event updated successfully', 'success');
    }

    /**
     * Description
     * @method updateEventSignup
     * @return
     */
    function updateEventSignup(){
      // console.log(vm.eventDetails.reminder);
      CalendarService.updateSignupEvent({signup: vm.eventDetails.signup, id: vm.eventDetails.id, comment: vm.eventDetails.comment, reminder: vm.eventDetails.reminder}).then(function (resp) {
        closeDialog();
        showMessage('Saved')
      });
    }

    /**
     * Description
     * @method getContacts
     * @return
     */
    function getContacts() {
      ContactService.getContacts().then(function (resp) {
        if (resp) {
          vm.contacts = resp;
          vm.filteredContacts = resp.map(function (c) {
            var contact = {
              name: c.first_name + ' ' + c.last_name,
              email: c.email,
              photo_url: c.photo_url ? c.photo_url.replace('assets', '').replace(/\\/, '/') : '../images/avatars/blank.jpg',
              id: c.sochara_user_id || undefined,
              acceptance: 'pending',
            };
            // contact._lowername = contact.name.toLowerCase();
            return {
              value: contact.name.toLowerCase(),
              display: contact.name,
              isContact: true,
              image: contact.photo_url,
              attendeeData: contact
            };
          });
        }
      });
    }

    /**
     * Description
     * @method getBirthdayContacts
     * @return
     */
    function getBirthdayContacts() {
      CalendarService.getBirthdayContacts().then(function (resp) {
        resp.forEach(function (respObj) {
          var forEventForSidebar = {start: respObj.start, end: respObj.start, title: respObj.title}
          vm.sidebarCalendarMain.push(forEventForSidebar);
          respObj.stick  = true;
          vm.events[0].push(respObj);
        });
      });
    }


    vm.newSignupEntry = newSignupEntry;
    vm.showSignupColorSelector = showSignupColorSelector;


    /**
     * Description
     * @method newSignupEntry
     * @return
     */
    function newSignupEntry() {
      var signupObj = {};
      signupObj.job = "";
      signupObj.needed = 1;
      signupObj.start = vm.eventDetails.start;
      signupObj.end = vm.eventDetails.end;
      signupObj.color = 'c1';
      vm.eventDetails.signup.push(signupObj);
      var timePickerDom = angular.element('mdp-time-picker.calendar_style .md-button.md-icon-button');
      if (timePickerDom) {
        timePickerDom.remove();
      }
    }


    /**
     * Description
     * @method showSignupColorSelector
     * @param {} $index
     * @return
     */
    function showSignupColorSelector($index) {
      // vm.hideColorSelector = !vm.hideColorSelector;
      if ($index) {
        vm.showColorSelector[$index] = !vm.showColorSelector[$index];
      }
    }

    vm.addEvent = addEvent;
    vm.next = next;
    vm.prev = prev;

    /**
     * Go to next on current view (week, month etc.)
     * @method next
     * @return
     */
    function next() {
      // vm.events[0] = vm.allEvents.slice();
      vm.calendarView.calendar.next();
      vm.agendaViewClass!=='month'?vm.changeSomeColumnForChangeView():'';
    }

    /**
     * Go to previous on current view (week, month etc.)
     * @method prev
     * @return
     */
    function prev() {
      // vm.events[0] = vm.allEvents.slice();
      vm.calendarView.calendar.prev();
      vm.agendaViewClass!=='month'?vm.changeSomeColumnForChangeView():'';
    }

    /**
     * Add event
     * @method addEvent
     * @param e
     * @return
     */
    function addEvent(e) {
      vm.showEventDetailsPopover = false;
      vm.eventDetails = {};
      vm.eventDetails.start = new Date();
      vm.eventDetails.end = new Date();
      initNewEvent();
      showDialogue('app/main/calendar/dialogs/event-form/so-create-event-dialog.html');
      removeTimePickerDefaultDesign();
    }


    /**
     * Description
     * @method removeTimePickerDefaultDesign
     * @return
     */
    function removeTimePickerDefaultDesign() {
      $timeout(function () {
        var calTpickerButton = angular.element('mdp-time-picker.calendar_style .md-button.md-icon-button');
        if (calTpickerButton) {
          calTpickerButton.remove();
        }
      }, 1000);
    }

    /**
     * Description
     * @method setCalendarViewPosition
     * @return
     */
    function setCalendarViewPosition(){
      var calendarTopAndHeaderHeight = parseInt(angular.element("#calendar .header").css('height'), 10) + parseInt(angular.element("#toolbar").css('height'), 10);
      var calendarViewHeight  = angular.element(window).height() - calendarTopAndHeaderHeight;
      angular.element("#calendarView").height(calendarViewHeight - 18);
    }

    /**
     * Description
     * @method removeCalendarStrictHeightPosition
     * @return
     */
    function removeCalendarStrictHeightPosition(){
      angular.element("#calendarView").height('inherit');
    }

    /**
     * Add new event in between selected dates
     * @method select
     * @param start
     * @param end
     * @return
     */
    function select(start, end) {
      vm.showEventDetailsPopover = false;
      vm.eventDetails = {};

      if(!(start && start.hasTime())){
        if(start.isSame(new Date(), 'day')){
          vm.eventDetails.start = new Date();
        }else{
          vm.eventDetails.start = moment(start.format()).add(8, 'hours')._d;
        }
      }else{
        vm.eventDetails.start = start._d;
      }
      if(!(end && end.hasTime())){
        vm.eventDetails.end = moment(end.format()).subtract(1, 'day').add(20, 'hours')._d;
      }else{
        vm.eventDetails.end = end._d;
      }

      initNewEvent();
      showDialogue('app/main/calendar/dialogs/event-form/so-create-event-dialog.html');
      removeTimePickerDefaultDesign();
    }

    vm.searchTime = searchTime;
    vm.searchTimeEnd = searchTimeEnd;
    vm.selectedStartTimeChange = selectedStartTimeChange;
    vm.selectedEndTimeChange = selectedEndTimeChange;
    // vm.timeSlotEnd = [];

    /**
     * Description
     * @method searchTime
     * @param {} query
     * @return ConditionalExpression
     */
    function searchTime (query) {
      query = query || '';
      var lowercaseQuery = angular.lowercase(query);
      return query ? vm.timeSlot.filter( function( time ){
        return time.name.indexOf(lowercaseQuery) === 0;
      }) : vm.timeSlot;
    }

    /**
     * Description
     * @method searchTimeEnd
     * @param {} query
     * @return ConditionalExpression
     */
    function searchTimeEnd (query) {
      query = query || '';
      var lowercaseQuery = angular.lowercase(query);
      if(!vm.timeSlotEnd){
        var timeVal = parseInt(vm.eventEndTime.value.replace('T', ''));
        vm.timeSlotEnd = vm.timeSlot.filter(function (slot) {
          return parseInt(slot.value.replace('T', '')) > timeVal;
        });
      }
      return query ? vm.timeSlotEnd.filter( function( time ){
        return time.name.indexOf(lowercaseQuery) === 0;
      }) : vm.timeSlotEnd;
    }

    /**
     * Description
     * @method selectedStartTimeChange
     * @param {} item
     * @return
     */
    function selectedStartTimeChange(item){
      // T220000
      var timeVal;
      if(item && item.value){
        timeVal = parseInt(item.value.replace('T', ''));
        vm.timeSlotEnd = vm.timeSlot.filter(function (slot) {
          return parseInt(slot.value.replace('T', '')) > timeVal;
        });
        vm.eventEndTime = vm.timeSlotEnd[0];
      }

    }

    /**
     * Description
     * @method selectedEndTimeChange
     * @param {} item
     * @return
     */
    function selectedEndTimeChange(item){
      var timeVal, startTimeVal;
      if(item && item.value && vm.eventStartTime && vm.eventStartTime.value){
        timeVal = parseInt(item.value.replace('T', ''));
        startTimeVal = parseInt(vm.eventStartTime.value.replace('T', ''));
        if(timeVal<startTimeVal){
          showMessage('Can not end time before start time !')
        }
      }
    }

    /**
     * Description
     * @method updateNewEvent
     * @return
     */
    function updateNewEvent() {

      var updatedEvent = {};
      updatedEvent.id = vm.eventEditData.id;
      updatedEvent.title = vm.eventEditData.title;
      updatedEvent.place = vm.eventEditData.place;
      updatedEvent.description = vm.eventEditData.description;
      updatedEvent.organizer = vm.eventEditData.organizer;
      updatedEvent.isRepeated = vm.eventEditData.isRepeated;
      updatedEvent.repeat = vm.eventEditData.repeat;
      if (updatedEvent.isRepeated) {
        updatedEvent.repeat.endsAtDate = vm.eventEditData.endDate;
      }
      updatedEvent.reminder = vm.eventEditData.reminder;
      updatedEvent.start = moment(moment(vm.eventEditData.startDate).format('DD/MM/YYYY') + ' ' + moment(vm.eventEditData.startTime).format('h:mm A'), 'DD/MM/YYYY h:mm A').format();
      updatedEvent.end = moment(moment(vm.eventEditData.endDate).format('DD/MM/YYYY') + ' ' + moment(vm.eventEditData.endTime).format('h:mm A'), 'DD/MM/YYYY h:mm A').format();
      updatedEvent.color = vm.eventEditData.color;
      updatedEvent.allDay = vm.eventEditData.allDay;
      updatedEvent.notifcationStatus = vm.eventEditData.notifcationStatus;
      updatedEvent.showMeAs = vm.eventEditData.showMeAs;
      updatedEvent.isSignupChecked = vm.eventEditData.isSignupChecked;

      updatedEvent.attendees = vm.eventEditData.attendees;
      updatedEvent.nAnd = vm.eventEditData.nAndP;

      updatedEvent.signup = vm.updateEventSignupArray;


      CalendarService.updateEvent(updatedEvent).then(function (resp) {
        $mdDialog.hide();
        $state.reload();
        if (resp && typeof resp === 'object') {
          showMessage('Event updated successfully', 'success');
        } else {
          showMessage(resp, 'error');
        }
      });

    }

    /**
     * Description
     * @method addNewContacts
     * @param {} name
     * @return
     */
    function addNewContacts(name) {
      vm.newUserName = name;
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: '/app/main/calendar/dialogs/event-detail/add-new-contact.html',
        clickOutsideToClose: true,
        multiple: true
        // targetEvent: ev,
      });
    }


    /**
     * Description
     * @method getSignupCalculatedVal
     * @param {} have
     * @param {} need
     * @return CallExpression
     */
    function getSignupCalculatedVal( have, need){
      have = have || 0;
      need = need || 0;
      return Math.max(0, need - have);
    }

    /**
     * Description
     * @method getContributorBringCount
     * @param {} signup
     * @return bringReturn
     */
    function getContributorBringCount( signup ){
      var bringReturn = 0;
      if( signup.contributors && signup.contributors.length ){
        signup.contributors.forEach(function ( signupElement, key ) {
          if( signupElement.user === vm.user.id ){
            bringReturn = signup.contributors[key].bring;
          }
        });
      }
      return bringReturn;
    }


    /**
     * Description
     * @method goToItem
     * @param {} id
     * @param {} module
     * @param {} $event
     * @return
     */
    function goToItem( id, module, $event ){
      if( module === 'calendar'){
        var event = vm.allEvents.find(function(element) {
          return element.id === id;
        });
        selectedEventDetails(event, $event);
      }
    }

    /**
     * Description
     * @method saveNewContact
     * @return
     */
    function saveNewContact() {
      if (!vm.newContact.email) {
        return false;
      }
      vm.newContact.photo_url = '';
      CalendarService.addNewContact(vm.newContact).then(function (resp) {
        var newContact = {};
        newContact.name = resp.first_name + ' ' + resp.last_name;
        newContact.email = resp.email;
        newContact.role = 'volunteer';
        newContact.photo_url = resp.photo_url || 'images/avatars/profile.jpg';
        newContact.id = resp.id;
        newContact.acceptance = 'pending';
        vm.eventDetails.attendees.push(newContact);
        showMessage('Contact added successfully');
        $mdDialog.hide();
        ContactService.getContacts().then(function (data) {
          vm.contacts = data;
          getContacts();
        });
      });
    }

    /**
     * Description
     * @method closePopoverHtml
     * @return
     */
    function closePopoverHtml() {
      vm.showEventDetailsPopover = false;
    }
    //
    // $scope.$watch('isOpenEventDatePicker', function(newValue, oldValue) {
    //   console.log('hello...');
    //   if (!newValue && oldValue) {
    //     document.querySelector('.md-datepicker-input').blur();
    //   }
    // });

    $scope.$watch(angular.bind(this, function () {
      return this.isOpenEventDatePicker;
    }), function (newValue, oldValue) {
      if (!newValue && oldValue) {
        // document.querySelector('.md-datepicker-input').blur();
        angular.element('.start_event .md-datepicker-input').blur()
      }
    });

    $scope.$watch(angular.bind(this, function () {
      return this.isOpenEventDatePickerEnd;
    }), function (newValue, oldValue) {
      if (!newValue && oldValue) {
        angular.element('.end_event .md-datepicker-input').blur()
        // document.querySelector('.md-datepicker-input').blur();
      }
    });

    vm.changeOnEventStartDate = changeOnEventStartDate;
    /**
     * Description
     * @method changeOnEventStartDate
     * @param {} event
     * @return
     */
    function changeOnEventStartDate(event) {
      vm.eventDetails.end = vm.eventDetails.start;
    }



    /**
     * Description
     * @method selectedEventDetails
     * @param {} data
     * @param {} jsEvent
     * @return
     */
    function selectedEventDetails(data, jsEvent) {
      initDefaultFieldsForDialog();
      CalendarService.getEvent({id: data.id, type: data.type}).then(function (eventData) {
        vm.eventDetails = eventData;
        vm.eventBerofeModify = angular.copy(eventData);

        vm.eventDetails.showMeAs = eventData.availability ? eventData.availability[vm.user.id] : '';

        if(vm.eventDetails.isRepeated){
          vm.eventDetails.start = data._start._d;
          vm.eventDetails.end = data.end._d;
        }else{
          vm.eventDetails.start = new Date(vm.eventDetails.start);
          vm.eventDetails.end = new Date(vm.eventDetails.end);
        }

        vm.eventDetails.attendee = data.attendee;
        vm.eventDetails.reminder = eventData.reminder && eventData.reminder.length ? eventData.reminder : [{type: 'notification', duration: 30, format: 'minutes', user: vm.user.id}];
        checkAttendees();
        if (data.type === 'birthday') {
          showDialogue('/app/main/calendar/dialogs/event-detail/so-event-detail-dialog-new-birthday.html', true);
        } else if (data.type === 'event') {
          var tempStart = eventData.origin ? eventData.origin.start : eventData.start;
          var tempEnd = eventData.origin ? eventData.origin.end : eventData.end;
          if(!moment(new Date(tempStart)).diff(moment(new Date(tempEnd)), 'days')){
            vm.eventDetails.date = moment(new Date(data.start)).format('dddd, MMMM D, YYYY');
            if(!vm.eventDetails.allDay){
              vm.eventDetails.time = moment(new Date(data.start)).format('h:mma') + ' - ' + moment(new Date(data.end)).format('h:mma');
            }
          }else{
            if(!vm.eventDetails.allDay){
              vm.eventDetails.date = moment(new Date(data.start)).format('ddd, MMMM D, YYYY, h:mma') + ' - ' + moment(new Date(data.end)).format('ddd, MMMM D, YYYY, h:mma');
            }else{
              vm.eventDetails.date = moment(new Date(data.start)).format('ddd, MMMM D, YYYY') + ' - ' + moment(new Date(data.end)).format('ddd, MMMM D, YYYY');
            }
          }
          if( data.attendee.role === 'organizer' ) {
            vm.showEventDetailsPopover = true;
            $timeout(function () {
              setPopoverPosition( jsEvent.pageY, jsEvent.pageX );
            }, 10);
          } else{
            showDialogue('/app/main/calendar/dialogs/event-detail/so-event-detail-dialog-new.html', true);
          }
          //showDialogue('/app/main/calendar/dialogs/event-detail/so-event-detail-dialog-new.html', true);
        } else if (data.type === 'holiday') {
          showDialogue('/app/main/calendar/dialogs/event-detail/so-event-detail-dialog-new-holiday.html', true);
        } else {
          showDialogue('/app/main/calendar/dialogs/event-detail/so-event-detail-dialog-new.html', true);
        }
      });
    }

    /**
     * Description
     * @method setPopoverPosition
     * @param {} x
     * @param {} y
     * @return
     */
    function setPopoverPosition( x, y ){
      var getWindoWidth =angular.element(window).width();
      var setPositionValue;
      if( getWindoWidth/2>y ){
        setPositionValue = y + 130;
        angular.element('#eventDetailsPopover').css({ right: 'inherit', left: setPositionValue, top: 100});
      }else{
        setPositionValue = (getWindoWidth-y) + 130;
        angular.element('#eventDetailsPopover').css({ left: 'inherit', right: setPositionValue, top: 100});
      }
    }

    /**
     * Description
     * @method checkAttendees
     * @return
     */
    function checkAttendees() {
      if (vm.eventDetails.attendees) {
        vm.eventAttendee = vm.eventDetails.attendees.filter(function (attendee) {
          return attendee.id === vm.user.id;
        })[0];
      }
    }

    vm.querySearch = querySearch;
    vm.selectedItemChange = selectedItemChange;
    vm.selectedItemChangeUpdate = selectedItemChangeUpdate;


    /**
     * Description
     * @method querySearch
     * @param {} query
     * @return MemberExpression
     */
    function querySearch(query) {
      vm.previousSearchText = query;
      var selectedAttendeeIds = vm.eventDetails.attendees.map(function (attendee) {
        return attendee.id;
      });
      var results = query ? ContactService.getAllSocharaUsers({name: query}).then(function (response) {
        var totalResponse = response.map(function (c) {
          var contact = {
            name: c.first_name + ' ' + c.last_name,
            email: c.email,
            photo_url: c.photo_url ? c.photo_url.replace('assets', '').replace(/\\/, '/') : '../images/avatars/blank.jpg',
            id: c.id || undefined,
            acceptance: 'pending',
          };
          // contact._lowername = contact.name.toLowerCase();
          return {
            value: contact.name.toLowerCase(),
            display: contact.name,
            image: contact.photo_url,
            isContact: false,
            attendeeData: contact
          };
        });


        var searchQuery = angular.lowercase(query);
        // concate 2 arrays
        totalResponse = totalResponse.concat(vm.filteredContacts);
        return totalResponse.filter(function ( state ) {
          return state.value.indexOf(searchQuery) === 0 && selectedAttendeeIds.indexOf(state.id) < 0;
        });
        // return totalResponse;
      }) : vm.filteredContacts.filter(function (cont) {
        return !cont.id || selectedAttendeeIds.indexOf(cont.id) < 0;
      });

      var deferred = $q.defer();
      $timeout(function () {
        deferred.resolve(results);
      }, Math.random() * 1000, false);
      return deferred.promise;
    }

    /**
     * Description
     * @method selectedItemChange
     * @param {} resp
     * @return
     */
    function selectedItemChange(resp) {
      console.log(resp);
      if (!resp) {
        return;
      }
      var attendeeLength = vm.eventDetails.attendees.filter(function (e) {
        return e.id === resp.attendeeData.id;
      }).length;
      if (attendeeLength > 0) {
        showMessage('This Contact already added', 'error');
      } else {
        vm.eventDetails.attendees.push({
          name: resp.attendeeData.name,
          email: resp.attendeeData.email,
          role: 'volunteer',
          photo_url: resp.attendeeData.photo_url ? resp.attendeeData.photo_url : 'images/avatars/profile.jpg',
          id: resp.attendeeData.id,
          acceptance: 'pending',
          isContact: resp.isContact
        });
        showMessage('Contact added', 'success');
      }
      vm.searchText = vm.previousSearchText;
      vm.selectedItem = undefined;
      // document.querySelector('#input-11').blur();
      var peopleSearchInput = angular.element("input#searchPeople");
      if (peopleSearchInput) {
        peopleSearchInput.blur();
        peopleSearchInput.focus();
      }
    }

    /**
     * Description
     * @method selectedItemChangeUpdate
     * @param {} resp
     * @return
     */
    function selectedItemChangeUpdate(resp) {
      var attendeeLength = vm.eventDetails.attendees.filter(function (e) {
        return e.id === resp.attendeeData.id;
      }).length;
      if (attendeeLength > 0) {
        showMessage('The Contact already added', 'error');
      } else {
        vm.eventDetails.attendees.push({
          name: resp.attendeeData.name,
          email: resp.attendeeData.email,
          role: 'volunteer',
          photo_url: resp.attendeeData.photo_url ? resp.attendeeData.photo_url : 'images/avatars/profile.jpg',
          id: resp.attendeeData.id,
          acceptance: 'pending',
        });
        showMessage('Added the contact', 'success');
      }
    }



    /**
     * Description
     * @method loadCalendarConfig
     * @return
     */
    function loadCalendarConfig() {
      vm.calendarUiConfig = {
        calendar: {
          editable: true,
          eventLimit: true,
          header: '',
          handleWindowResize: false,
          displayEventTime: false,
          minTime: "08:00:00",
          maxTime: "23:00:00",
          // nextDayThreshold: "08:00:00",
          timezone: false,
          aspectRatio: 1,
          dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
          dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
          /**
           * Description
           * @method viewRender
           * @param {} view
           * @return
           */
          viewRender: function (view) {
            vm.calendarView = view;
            vm.calendar = vm.calendarView.calendar;
            vm.currentMonthShort = vm.calendar.getDate().format('MMM');
          },

          columnFormat: {
            month: 'ddd',
            week: 'ddd D',
            day: 'ddd M'
          },
          eventClick: selectedEventDetails,
          selectable: true,
          selectHelper: true,
          select: select,
          textEscape: false,
          /**
           * Description
           * @method eventRender
           * @param {} event
           * @param {} element
           * @return
           */
          eventRender: function (event, element) {
            if (event.color && event.color.eventColor) element.addClass('evnt-' + event.color.eventColor);
            if (event.color && event.color.textColor) element.addClass('evntText-' + event.color.textColor);
            if( event.ranges ){
              return (event.ranges.filter(function (range) {
                return (event.start.isBefore(range.end) && event.end.isAfter(range.start));
              }).length)>0;
            }
          }
        }
      };
      /*running funtions when page loaded*/

      $timeout(function () {
        loadTinyCalendar();
      });
    }

    /**
     * Description
     * @method loadTinyCalendar
     * @return
     */
    function loadTinyCalendar() {
      vm.loadAndShowTinyCalendar = true;
      angular.element(document).ready(function () {
        $timeout(function () {
          angular.element('.fixTC button').trigger('click');
        }); // 3 seconds
      });
    }

    // Load Sochara Events




    /**
     * Description
     * @method showDialogue
     * @param {} templateUrl
     * @param {} clickOutsideToClose
     * @return
     */
    function showDialogue(templateUrl, clickOutsideToClose) {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: templateUrl,
        clickOutsideToClose: clickOutsideToClose || false
      });
    }

    /**
     * Description
     * @method toogleRepeatDaySelection
     * @param {} id
     * @return
     */
    function toogleRepeatDaySelection( id ){
      var findIndexPos = vm.eventDetails.repeat.days.indexOf(id);
      if( findIndexPos > -1 ){
        vm.eventDetails.repeat.days.splice(findIndexPos, 1);
      }else{
        vm.eventDetails.repeat.days.push(id);
      }
    }

    /**
     * Description
     * @method filterAllEvents
     * @return
     */
    function filterAllEvents() {
      $rootScope.loadingProgress = true;
      if (vm.isAllEventsFiltered) {
        angular.element('.fc-month-view td.fc-event-container').hide();
        uiCalendarConfig.calendars['main'].fullCalendar( 'removeEvents' );
        vm.events[0] = [];
        vm.showEventsOnly = false;
        vm.showHolidayEvent = false;
        vm.showBirthdayEvent = false;
        $rootScope.loadingProgress = false;
      } else {
        vm.events[0] = vm.allEvents.slice();
        vm.showEventsOnly = true;
        vm.showHolidayEvent = true;
        vm.showBirthdayEvent = true;
        $rootScope.loadingProgress = false;
      }
    }

    /**
     * Description
     * @method getBy
     * @param {} id
     * @param {} value
     * @return MemberExpression
     */
    Array.prototype.getBy = function (id, value) {
      return this.filter(function (x) {
        return x[id] === value;
      })[0];
    };

  }

})();
