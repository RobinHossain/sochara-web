(function () {
  'use strict';

  angular.module('app.dashboard').controller('DashboardController', DashboardController).directive('bufferedScroll', function ($parse) {
    return function ($scope, element, attrs) {
      var handler = $parse(attrs.bufferedScroll);
      element.scroll(function (evt) {
        var scrollTop    = element[0].scrollTop,
          scrollHeight = element[0].scrollHeight,
          offsetHeight = element[0].offsetHeight;
        if (scrollTop === (scrollHeight - offsetHeight)) {
          $scope.$apply(function () {
            handler($scope);
          });
        }
      });
    };
  }).filter('selectedTags', function () {
    //custom filter for dashboard. The functions omits the content from array matching unchecked area using tags
    return function (currentDashboardData, tags) {
      return currentDashboardData.filter(function (fbFeed) {
        return tags.indexOf(fbFeed.fbApiType) !== -1;
      });
    };
  });


  function DashboardController(ChatsService, MailService, DashboardService, CalendarService, $scope, $state, Auth, $mdToast, $mdDialog, $rootScope, $interval, $anchorScroll, $location, $timeout, $window, socharaLightbox, Upload) {
    var vm = this;
    //define locals
    vm.tags = [];
    //vm.filter =['music','feed','Sochara Posts'];
    vm.isPost = true;
    // vm.contentLoading = false; //content loading animation
    vm.isPostClicked = false; //hide posting box area
    vm.editId = ""; //id of the editing post
    vm.OGAvailable = false; //meta data fetching popover box
    vm.progressBarLinearOGData = false; //line loading animation for meta data fetching
    vm.availableOG = []; //meta data fetching sites' url array
    vm.postData = ""; //string of post
    vm.singleStickerCategory = false; //sticker category
    vm.singleStickerCategoryArray = [];
    vm.singleStickerCategoryName = "";
    vm.gifCategory = "Trendy"; //default GIF category
    vm.gifImages = []; //GIF images array
    vm.isFilterAvailable = false; //setting filter scopes false by default
    vm.searchGIFTerm = ""; //searching GIF terms
    vm.currentSocharPostId = ""; //ID used when to edit
    vm.socharaPost = "";
    vm.fbApiType = "Sochara Posts";
    vm.progressBarLinear = true;
    vm.progressBarLinearSticker = true;
    $rootScope.unreadCountTotal = 0;
    vm.userPhoto = "";
    $scope.currentDashboardData = [];
    vm.uploadAttachment = uploadAttachment;
    vm.deleteCurrentAttachment = deleteCurrentAttachment;
    vm.attachments = [];

    //Filtering scopes
    $scope.filterSocharaPosts = false;
    $scope.filterPosts = false;
    $scope.filterMusic = false;
    $scope.filterTwitterVisible = false;
    $scope.filterTwitter = false;
    $scope.filterMovies = false;
    $scope.filterBooks = false;
    $scope.filterInstagramVisible = false;
    $scope.filterInstagram = false;
    $scope.filterSocharaPostsVisible = false;
    $scope.filterPostsVisible = false;
    $scope.filterMusicVisible = false;
    $scope.filterMoviesVisible = false;
    $scope.filterBooksVisible = false;
    $scope.webcamPermissionNote = true;
    $scope.webCamAreaAvailable = false;
    $scope.postPhotos = [];
    $scope.loadMoreArea = false; //load more area is false until data is loaded

    vm.availableSocialAPIPost = true;
    vm.openEmail = openEmail;
    vm.closeVideoPopup = closeVideoPopup;
    vm.videoOrImagePreview = videoOrImagePreview;
    vm.showMessage = showMessage;
    vm.submitSocialPost = submitSocialPost;
    vm.changeDashboardDataLimit = changeDashboardDataLimit;
    vm.statusBarOpen = statusBarOpen;
    vm.cancelPostBox = cancelPostBox;
    vm.removePost = removePost;
    vm.removePostConfirm = removePostConfirm;
    vm.makeFavorite = makeFavorite;
    vm.editPost = editPost;
    vm.closeDialog = closeDialog;
    vm.editPostConfirm = editPostConfirm;

    function closeVideoPopup() {
      $mdDialog.hide();
      angular.element('#videoPlayerPopup').removeClass('is-visible');
      vm.videoPlayers.forEach(function (player) {
        player.destroy();
      });
      angular.element('#videoUrl').attr('src', '');
      angular.element('#posterUrl').attr('poster', '');
    }

    vm.goToGlobalSetting = function () {
      $state.go('app.global-setting', {});
    };

    function statusBarOpen(){
      // angular.element('.dashboard-item .md-whiteframe-2dp').addClass('status_bar_open');
    }

    function cancelPostBox() {
      vm.files = [];
      vm.isPostClicked = false;
      vm.isPost = true;
      vm.dashboardStatusOpen = false;
      // angular.element('.dashboard-item .md-whiteframe-2dp').removeClass('status_bar_open');
    }

    vm.dynamicHelloText = [{"busy": "looks like you’ve got a pretty busy day today!"}];


    function videoOrImagePreview(content) {
      vm.showVideoPlayer = false;
      $rootScope.loadingProgress = true;
      if ( content.provider === 'facebook' ) {
        if( content.attachment_details.type === 'album'){
          showImagePreview(content.attachments);
        }else if( content.type === 'video' ){
          showVideoPreview(content.source, content.attachment_details.type);
        }else if( content.type === 'photo' ){
          showImagePreview(content.attachment_details.media);
        }else if( content.type === 'link'){
          goToLink(content.link);
        }else if( content.attachment_details.type === 'share'){
          goToLink(content.attachment_details.url);
        }
      }else if(content.provider === 'twitter'){
        showImagePreview(content.media);
      }else if(content.provider === 'instagram'){
        showImagePreview(content.media);
      }
    }

    function removePost( post ){
      vm.currentPost = post;
      showDialogue('app/main/dashboard/dialogs/delete.html');
    }

    function removePostConfirm(){
      closeDialog();
      DashboardService.removePost({id: vm.currentPost.id || ''}).then(function (respPost) {
        fetchDashboardDataCall();
        if(respPost.error){
          showMessage('The post has been deleted from Sochara!');
        }else if(respPost.success){
          showMessage('The post has been Deleted !');
        }
      });
    }

    function makeFavorite( id ){
      DashboardService.makeFavorite({id: id}).then(function (respPost) {
        console.log(respPost);
        if(respPost.error){
          showMessage(respPost.error.message);
        }else if(respPost.success){
          showMessage('Favorite post updated !');
          fetchDashboardDataCall();
        }
      });
    }

    function closeDialog() {
      $mdDialog.hide();
    }

    function editPost( post ){
      vm.currentPost = post;
      showDialogue('app/main/dashboard/dialogs/edit-post.html');
    }

    function editPostConfirm(){
      closeDialog();
      DashboardService.editPost({id: vm.currentPost.id, message: vm.currentPost.post}).then(function (respPost) {
        if(respPost.error){
          showMessage(respPost.error.message);
        }else if(respPost.success){
          showMessage('The post has been Updated !');
          vm.dashboardAllData.getById(vm.currentPost.id).post = vm.currentPost.post;
        }
      });
    }

    function showDialogue(templateUrl, clickOutsideToClose) {
      $mdDialog.show({
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: templateUrl,
        clickOutsideToClose: clickOutsideToClose || false
      });
    }

    function showVideoPreview( media, media_type ){
      $rootScope.loadingProgress = false;
      $mdDialog.show({
        controller: function () {
          return vm;
        },
        panelClass: 'sochara_video_player',
        controllerAs: 'vm',
        templateUrl: 'app/main/dashboard/dialogs/video-preview.html',
        clickOutsideToClose: false,
      });
      $timeout(function () {
        vm.videoPlayers = plyr.setup(document.getElementById('sochara_video_player'), [{
          autoplay: false,
          hideControls: true
        }]);
        vm.showVideoPlayer = true;
        vm.videoPlayers[0].source({
          type: 'video',
          sources: [{
            src: media,
            type: media_type === 'video_inline' ? 'video/mp4' : 'youtube'
          }]
        });
      }, 1500);
    }

    function showImagePreview( media ){
      $rootScope.loadingProgress = false;
      var albumImages = [];
      if(typeof media === 'object'){
        albumImages = media.map(function (mObject) {
          return mObject.media;
        });
      }else{
        albumImages.push(media);
      }
      socharaLightbox.show(albumImages, { initialIndex: 0 });
    }

    function goToLink(link){
      $rootScope.loadingProgress = false;
      $window.open(link, '_blank');
    }

    // Data Sorting by Specific Key Value
    if (typeof Object.defineProperty === 'function') {
      try {
        Object.defineProperty(Array.prototype, 'sortBy', {value: srtby, configurable: true});
      } catch (e) {
        console.log(e);
      }
    }
    if (!Array.prototype.sortBy) Array.prototype.sortBy = srtby;

    /**
     * @example srtby - Sort By.
     * @param {string} f - The Sorting Key value.
     * @returns {Array|Object} - Return as Array/Object.
     */
    function srtby(f) {
      var i;
      for (i = this.length; i;) {
        var o = this[--i];
        this[i] = [].concat(f.call(o, o, i), o);
      }
      this.sort(function (a, b) {
        for (var i = 0, len = a.length; i < len; ++i) {
          if (a[i] !== b[i]) return a[i] < b[i] ? 1 : -1;
        }
        return 0;
      });
      for (i = this.length; i;) {
        this[--i] = this[i][this[i].length - 1];
      }
      return this;
    }


    function getUserInfo(){
      Auth.info().then(function (response) {
        vm.user = response;
      });
    }

    //Starts Loading dashboard data from getDashboardData() functions from Auth service

    //declaring the function then we will load the function
    function fetchDashboardDataCall() {
      vm.loadingDashboardData = true; //loading animation

      DashboardService.getDashboardData().then(function (result) {
        // console.log(result);
        if( result && result.length ){
          vm.dashboardAllData = result.sortBy(function (o) {
            return o.date_sort;
          });
          // console.log(vm.dashboardAllData);
          loadMasonryForPosts();
          vm.loadingDashboardData = false;
        }else{
          vm.loadingDashboardData = false;
          vm.dashboardAllData = [];
        }
      });
    }


    /*Ends laoding dashboard data*/
    function showMessage(message) {
      $mdToast.show({
        template: '<md-toast class="md-toast success">' + message + '</md-toast>',
        hideDelay: 3000,
        position: 'bottom left'
      });
    }

    function postStatus() {
      if (!vm.socharaPost.length) {
        showMessage("Please add something to your post.");
        return;
      }else if( !vm.selection.length ){
        showMessage("Choice a social account to submit post.");
        return false;
      }
      var data = {};
      data.post = vm.socharaPost;
      data.photos = vm.uploadedMedia;
      data.media = vm.uploadedMedia;
      data.apiTypes = vm.selection;
      data.og = vm.ogData;
      vm.postData = "";
      vm.socharaPost = "";
      DashboardService.submitSocialPost(data).then(function ( respPostBack ) {
        if( respPostBack ){
          vm.isPostClicked = false;
          $rootScope.loadingProgress = false;
          showMessage('That post looks great! Once it is published you can find it here on your dashboard.');
          if( respPostBack.media ){
            $timeout(function () {
              DashboardService.deletePostedMedia({files: respPostBack.media}).then(function (respDeletedMedia) {
                // console.log(respDeletedMedia);
              });
            }, 5000);
          }
          if( respPostBack.socialBack ){
            $rootScope.loadingProgress = false;
            vm.files = [];
            respPostBack.socialBack.forEach(function ( post ) {
              // vm.dashboardAllData.push(post);
            });
            vm.selection = [];
          }
        }
      });
    }

    function loadMasonryForPosts(){
      $timeout(function () {
        var elem = document.querySelector('.grid');
        var pckry = new Packery( elem, {
          itemSelector: '.grid-item',
          columnWidth: '.grid-sizer',
          percentPosition: true,
          getSortData: {
            date: '[data-date_sort]'
          },
          sortBy: 'date',
          sortAscending: false
        });
      }, 2000);
    }

    function changeDashboardDataLimit(){
      // console.log('working here');
    }

    /*Starts submiting sochara own posts*/
    function submitSocialPost() {
      vm.uploadedMedia = [];
      if (vm.toUploadFiles && vm.toUploadFiles.length) {
        uploadMedia(vm.toUploadFiles);
      } else {
        postStatus();
      }
    }

    function uploadMedia(photos) {
      photos = photos.filter(function (photo) {
        return photo.type && photo.type.length;
      });
      $rootScope.loadingProgress = true;
      $scope.files = photos;
      // closeDialog();
      if (photos && photos.length) {
        // PHotos Here
        photos.forEach(function ( file, index ) {
          file.upload = Upload.upload({
            url: '/dashboard/uploadMedia',
            headers: {
              'Content-Type': 'multipart/form-data'
            },
            data: {filePath: file.path, file: file}
          });

          file.upload.then(function (response) {
            if( response && response.data.id ){
              vm.uploadedMedia.push(response.data.id);
            }
            $timeout(function () {
              file.result = response.data;
              if ( photos.length === index + 1) {
                vm.showUploadedNotification = true;
                $scope.files = null;
                $timeout(function () {
                  // console.log('done');
                  postStatus();
                }, 500);
              }
            });
          });
        });
      }
    }


    /**
     * Description
     * @method uploadAttachment
     * @param {} files
     * @param {} errFiles
     * @return
     */
    function uploadAttachment(files, errFiles) {
      vm.isPostClicked = true;
      vm.toUploadFiles = files;
    }

    /**
     * ------------------------------------- Delete Attachment by ID -----------------------------------------------
     * @method deleteCurrentAttachment
     * @param {} id
     * @return
     */
    function deleteCurrentAttachment(id) {

      for (var i = 0; i < vm.files.length; ++i) {
        if (vm.files[i]['$$hashKey'] === id) {
          vm.files.splice([i], 1);
        }
      }

      for (i = 0; i < vm.attachments.length; ++i) {
        if (vm.attachments[i]['id'] === id) {
          vm.attachments.splice([i], 1);
        }
      }
    }

    /*------------------------------------- end Delete Attachment by ID -----------------------------------------------*/


    init();

    /**
     * @example init - Call to Initiate All Provider Mails With Mail Count.
     */
    function init() {
      loadAllProvider();
      userCalendarDataCount();
      getSocialAccount();
      $timeout(function () {
        fetchDashboardDataCall();
        if( $rootScope.user ){
          vm.user = $rootScope.user;
        }else{
          getUserInfo();
        }
        getCalendarEvent();
      });
    }

    io.socket.on('refresh_social', function (data) {
      $timeout(function () {
        fetchDashboardDataCall();
      }, 3000);
    });

    function getSocialAccount(){
      DashboardService.getSocialAccount().then(function ( respAcc ) {
        respAcc = respAcc.map(function (val) {
          return val.provider;
        });
        vm.socialList = respAcc.filter(function (val) {
          return val.provider !== 'instagram';
        });
      });
    }

    function getCalendarEvent(){
      DashboardService.getCalendarEvents().then(function ( respEvent ) {
        // console.log(respEvent);
        vm.events = respEvent;
      });
    }

    vm.goToCalendarModule = function () {
      $state.go('app.calendar', {});
    };
    vm.goToMailModule = function () {
      $state.go('app.mail.account.threads', {filter: 'inbox', accountId: 0});
    };


    function onResize() {
      var gridFooterDom = angular.element('.card-grid-footer');
      if (gridFooterDom) {
        var gridFooter = gridFooterDom.height() + 24;
        angular.element('.card-grid-header').height(gridFooter);
      }

    }

    function userCalendarDataCount(){
      CalendarService.getCalendarEventCount().then(function (respEventCount) {
        vm.eventCount = respEventCount.calendarCount;
      });
    }

    $timeout(onResize, 1000);
    angular.element($window).on('resize', onResize);

    /**
     * @example loadAllProvider - Load All Provider Mails.
     * @returns {Array|Object} - Return Object/Array Data.
     */
    function loadAllProvider() {

      vm.folderMailCount = 0;
      vm.activeProvider = "all";
      vm.newAccountInstruction = false;
      vm.currentThread = null;
      vm.allMailPaginationSkip = 0;

      vm.threads = null;
      vm.allMailPagination = null;
      vm.initMessagePositionCount = 1;
      vm.previousPaginationCount = 1;

      //vm.currentFilter.type = "folder";
      //vm.currentFilter.filter = "inbox";

      vm.loadingThreads = true;


      MailService.getMailAddresses().then(function (response) {
        if (response && response.length) {

          vm.folders = [
            {"id": "inbox", "name": "Inbox", "icon": "icon-inbox"},
            {"id": "sent", "name": "Sent", "icon": "icon-send"},
            {"id": "draft", "name": "Draft", "icon": "icon-email-open"},
            {"id": "spam", "name": "Spam", "icon": "icon-alert-octagon"},
            {"id": "trash", "name": "Trash", "icon": "icon-delete"}
          ];

          MailService.getMessages({limit: 50}).then(function (messageData) {
            var threadlist;
            if (vm.threads && vm.threads.length) {
              vm.threads = vm.threads.concat(messageData.messages);
              vm.threads = threadlist.sortBy(function (o) {
                return o.date;
              });
            } else {
              threadlist = messageData.messages;
              vm.threads = threadlist.sortBy(function (o) {
                return o.date;
              });
            }

            if (vm.allMailPagination && vm.allMailPagination.length) {
              vm.allMailPagination = vm.allMailPagination.concat(response.pagination);
            } else {
              vm.allMailPagination = response.pagination;
            }
            vm.loadingThreads = false;
            $rootScope.loadingProgress = false;
          });

          $rootScope.loadingProgress = true;

          vm.ThreadCountGMail = 0;
          vm.AllThreadCount = 0;
          vm.ThreadCountOutlook = 0;
          if (response && response.length) {
            vm.accounts = $rootScope.mailAccounts = response;
            response.forEach(function (val) {
              if (val.provider === "gmail") {
                MailService.GMailThreadCount({id: val.id}).then(function (response) {
                  vm.ThreadCountGMail = vm.ThreadCountGMail + response.threadsUnread;
                  vm.AllThreadCount = vm.AllThreadCount + response.threadsUnread;
                });
                inboxMailCount("gmail", val.id, "inbox");
              } else if (val.provider === "outlook") {
                MailService.getOutlookUnreadMailCount({id: val.id}).then(function (response) {
                  var mailCount = response.UnreadItemCount;
                  vm.ThreadCountOutlook = vm.ThreadCountOutlook + mailCount;
                  vm.AllThreadCount = vm.AllThreadCount + mailCount;
                });
                inboxMailCount("outlook", val.id, "inbox");
              }
            });
            vm.selectedAccount = response[0];
            vm.selectedAccountID = response[0].id;
          }
        } else {
          vm.loadingThreads = false;
          vm.newAccountInstruction = true;
          vm.activeProvider = "";
          vm.folders = null;
        }
      });

    }

    function inboxMailCount(provider, mailid, folder) {
      if (provider === "gmail") {
        MailService.GMailThreadFolderCount({id: mailid, folder: folder.toUpperCase()}).then(function (response) {
          vm.folderMailCount = vm.folderMailCount + response.threadsTotal;
        });
      } else if (provider === "outlook") {
        MailService.getOutlookUnreadMailCount({
          id: mailid,
          Mailbox: capitalizeFirstLetter(folder)
        }).then(function (response) {
          if (response) {
            vm.folderMailCount = vm.folderMailCount + parseInt(response.TotalItemCount);
          }
        });
      }
    }

    function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }


    function openEmail(thread) {
      $rootScope.loadingProgress = true;
      $state.go('app.mail.account.threads.thread', {
        filter: 'inbox',
        threadId: thread.mail_id,
        accountId: thread.email_id
      });
    }

    // vm.socialList = ['Facebook', 'Twitter'];
    vm.selection = [];
    vm.toggleSelection = function toggleSelection(site) {
      var idx = vm.selection.indexOf(site);removePost
      // is currently selected
      if (idx > -1) {
        vm.selection.splice(idx, 1);
      }
      // is newly selected
      else {
        vm.selection.push(site);
      }
    };

    Array.prototype.getById = function (value) {
      return this.filter(function (x) {
        return x.id === value;
      })[0];
    };
  }
})();
