(function () {
  'use strict';

  angular
    .module('sochara')
    .factory('DashboardService', dashboardService);

  /** @ngInject */
  function dashboardService($http, $q) {
    return {
      getDashboardData: function () {
        var defer = $q.defer();
        $http.post('/dashboard/fetchAll').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },


      socketTestOne: function () {
        var defer = $q.defer();
        $http.post('/dashboard/socketTestOne').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getCalendarEvents: function () {
        var defer = $q.defer();
        $http.post('/dashboard/getCalendarEvents').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      deletePostedMedia: function ( data ) {
        var defer = $q.defer();
        $http.post('/dashboard/deletePostedMedia', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      removePost: function ( data ) {
        var defer = $q.defer();
        $http.post('/dashboard/removePost', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      makeFavorite: function ( data ) {
        var defer = $q.defer();
        $http.post('/dashboard/makeFavorite', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      editPost: function ( data ) {
        var defer = $q.defer();
        $http.post('/dashboard/editPost', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getSocialAccount: function () {
        var defer = $q.defer();
        $http.post('/dashboard/getSocialAccount').then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      fetchFbVideoDetails: function (data) {
        var defer = $q.defer();
        $http.post('/dashboard/fetchFbVideoDetails', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      submitSocialPost:function (data) {
        var defer = $q.defer();
        $http.post('/dashboard/submitSocialPost',data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getNotificationCount:function () {
        var defer = $q.defer();
        $http.post('/dashboard/getNotificationCount').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
    };
  }

})();
