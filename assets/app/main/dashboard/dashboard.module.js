(function () {
  'use strict';

  angular
    .module('app.dashboard', ['app.auth.login', 'sochara.lightbox'])
    .config(config);

  /**
   * @example Dashboar Controller Config
   * @param {string} $stateProvider -State provider.
   * @returns {void} Return nothing.
   */
  function config($stateProvider) {

    // var token = localStorage.getItem('auth_token');
    // if( !token ){
    //   return ;
    // }

    // State
    $stateProvider
      .state('app.dashboard', {
        url: '/dashboard',
        views: {
          'content@app': {
            templateUrl: 'app/main/dashboard/dashboard.html',
            controller: 'DashboardController as vm'
          }
        }
      });


  }
})();
