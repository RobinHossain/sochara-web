(function () {
  'use strict';

  angular
    .module('app.privacypolicy-instagram', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {

    // var token = localStorage.getItem('ats');
    // if ( token && token.length ) {
    //   return;
    // }

    // State
    $stateProvider.state('app.privacypolicy-instagram', {
      url: '/privacypolicy/instagram',
      views: {
        'main@': {
          templateUrl: 'app/core/layouts/content-only.html',
          controller: 'MainController as vm'
        },
        'content@app.privacypolicy-instagram': {
          templateUrl: 'app/main/auth/privacypolicy-instagram/privacypolicy.html',
          controller: 'PrivacyPolicyInstagramController as vm'
        }
      },
      bodyClass: 'login'
    });

  }

})();
