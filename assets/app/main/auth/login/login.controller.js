(function () {
  'use strict';

  angular
    .module('app.auth.login')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController($scope, $state, Auth, $mdToast) {

    var token = localStorage.getItem('ats');
    if (token && token.length) {
      Auth.isAuthenticated().then(function (response) {
        if (response && response.user_id) {
          $state.go('app.dashboard');
        }
      });
    }


    var vm = this;
    vm.facebookLoginUrl = facebookLoginUrl;

    init();

    function init() {
      gmailLoginUrl();
    }

    vm.submitLogin = submitlogin;
    vm.getTrial = getTrial;

    function submitlogin() {

      Auth.login(vm.form).success(function (result) {
        $state.go('app.dashboard', {});
      }).error(function (err) {
        $mdToast.show({
          template: '<md-toast class="md-toast error">' + err.err + '</md-toast>',
          hideDelay: 3000,
          position: 'bottom left'
        });
      });
    }


    function gmailLoginUrl() {
      Auth.getGoogleAuthUri().then(function (response) {
        if (response) {
          vm.gmailLoginUri = response;
        }else{
          console.log('not found any login uri');
        }
      });
    }

    //checking Facebook Application Auth for the current user

    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    window.fbAsyncInit = function () {
      FB.init({
        appId: '1589011794492434',
        cookie: true, // This is important, it's not enabled by default
        version: 'v2.2'
      });
    };


    function facebookLoginUrl() {
      // var accessScops = ['email', 'picture'];
      // var scope = accessScops.join(', ');
      FB.login(function (response) {
        console.log(response);
        var previousResp = response.authResponse;
        if ( previousResp.accessToken ) {
          FB.api('/me', 'get', { access_token: previousResp.accessToken, fields: 'name,email,last_name,first_name,middle_name,picture.width(250).height(250)' }, function(response) {
            var resFound = response;
            resFound.token = previousResp.accessToken;
            console.log(resFound);
            Auth.loginWithFacebook(resFound).then(function (resp) {
              console.log(resp);
              $state.go('app.dashboard', {});
            });
          });
        } else {
          console.log('User cancelled login or did not fully authorize.');
        }
      }, {scope: 'public_profile,email'});
    }

    function getTrial() {
      $state.go('app.auth_register');
    }
  }

})();
