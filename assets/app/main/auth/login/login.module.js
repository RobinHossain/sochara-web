(function () {
  'use strict';

  angular
    .module('app.auth.login', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {

    // var token = localStorage.getItem('ats');
    // if ( token && token.length ) {
    //   return;
    // }

    // State
    $stateProvider.state('app.auth_login', {
      url: '/auth/login',
      views: {
        'main@': {
          templateUrl: 'app/core/layouts/content-only.html',
          controller: 'MainController as vm'
        },
        'content@app.auth_login': {
          templateUrl: 'app/main/auth/login/login.html',
          controller: 'LoginController as vm'
        }
      },
      bodyClass: 'login'
    });

  }

})();
