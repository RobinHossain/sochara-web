(function ()
{
    'use strict';

    angular
        .module('app.auth.register')
        .controller('RegisterController', RegisterController);

    /** @ngInject */
    function RegisterController($scope, $state, Auth, $mdToast)
    {
      var vm = this;

      vm.submitRegister = submitRegister;

      function submitRegister() {

        // console.log(vm.form);

        Auth.register(vm.form).success(function(result) {
          $state.go('app.auth_login', {});
          $mdToast.show({
            template: '<md-toast class="md-toast success">' + "Your account has been created successfully !" + '</md-toast>',
            hideDelay: 3000,
            position: 'bottom left'
          });
        }).error(function(err) {
          $state.go('app.auth_register', {});
          $mdToast.show({
            template: '<md-toast class="md-toast error">' + err.err + '</md-toast>',
            hideDelay: 3000,
            position: 'bottom left'
          });
        });

      }

    }
})();
