(function () {
  'use strict';

  angular
    .module('app.privacypolicy-facebook', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {

    // var token = localStorage.getItem('ats');
    // if ( token && token.length ) {
    //   return;
    // }

    // State
    $stateProvider.state('app.privacypolicy-facebook', {
      url: '/privacypolicy/facebook',
      views: {
        'main@': {
          templateUrl: 'app/core/layouts/content-only.html',
          controller: 'MainController as vm'
        },
        'content@app.privacypolicy-facebook': {
          templateUrl: 'app/main/auth/privacypolicy-facebook/privacypolicy.html',
          controller: 'PrivacyPolicyControllerFacebook as vm'
        }
      },
      bodyClass: 'login'
    });

  }

})();
