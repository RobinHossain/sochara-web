(function () {
  'use strict';

  angular
    .module('sochara')
    .factory('Auth', authService)
    .factory('AuthInterceptor', authInterCeptorService)
    .config(function ($httpProvider) {
      $httpProvider.interceptors.push('AuthInterceptor');
    });

  /** @ngInject */
  function authService($http, $q) {

    var AccessLevels = {};
    AccessLevels.user = 1;
    AccessLevels.anon = 0;

    return {
      authorize: function (access) {
        if (access === AccessLevels.user) {
          return this.isAuthenticated();
        } else {
          return true;
        }
      },

      isAuthenticated: function () {
        // return getLocationService('ats');

        var defer = $q.defer();
        $http.post('/auth/logincheck').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      login: function (credentials) {
        var login = $http.post('/auth/authenticate', credentials);
        login.success(function (result) {
          setLocationService('ats', JSON.stringify(result));
        });
        login.error(function (resp) {
          return resp.err;
        });
        return login;
      },

      loginWithFacebook: function (credentials) {
        var loginWithFacebook = $http.post('/auth/authenticateFacebook', credentials);
        loginWithFacebook.success(function (result) {
          setLocationService('ats', JSON.stringify(result));
        });
        loginWithFacebook.error(function (resp) {
          return resp.err;
        });
        return loginWithFacebook;
      },

      logout: function () {
        var defer = $q.defer();
        $http.post('/auth/logout').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getGoogleAuthUri: function () {
        var defer = $q.defer();
        $http.post('/auth/googleLoginUri').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getFacebookAuthUri: function () {
        var defer = $q.defer();
        $http.post('/auth/facebookLoginUri').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      register: function (formData) {
        var login = $http.post('/auth/register', formData);
        login.success(function (result) {
          return result.msg;
        });
        login.error(function (resp) {
          return resp.err;
        });
        return login;

      },

      lastUrl: function ( data ) {
        var defer = $q.defer();
        $http.post('/auth/lastUrl', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      saveProfileData: function (formData) {
        var profileSave = $http.post('/user/updateUser', formData);
        profileSave.success(function (result) {
          return result.msg;
        });
        profileSave.error(function (resp) {
          return resp.err;
        });
        return profileSave;

      },

      addContact: function (formData) {
        var addContactPost = $http.post('/contacts/addContact', formData);
        addContactPost.success(function (result) {
          return result.msg;
        });
        addContactPost.error(function (resp) {
          return resp.err;
        });
        return addContactPost;

      },

      saveAvatar: function (url) {
        var saveAvatarUrl = $http.post('/user/updateAvatar', {url: url});
        saveAvatarUrl.success(function (result) {
          return result.msg;
        });
        saveAvatarUrl.error(function (resp) {
          return resp.err;
        });
        return saveAvatarUrl;

      },
      saveBannerImage: function (url) {
        var updateBanner = $http.post('/user/updateBanner', {url: url});
        updateBanner.success(function (result) {
          return result.msg;
        });
        updateBanner.error(function (resp) {
          return resp.err;
        });
        return updateBanner;

      },
      privacyAttribute: function (attribute, privacy) {

        var defer = $q.defer();
        $http.post('/user/updatePrivacyAttribute', {attribute:attribute, privacy: privacy}).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      submitAccountSetting: function (formData) {

        var accountSave = $http.post('/user/updateUserAccount', formData);
        accountSave.success(function (result) {
          return result.msg;
        });
        accountSave.error(function (resp) {
          return resp.err;
        });
        return accountSave;

      },

      getProfiledata: function () {

        var getdata = $http.post('/user/getProfileData');
        getdata.success(function (result) {
          // console.log(result);
          return result.msg;
        });
        getdata.error(function (resp) {
          // console.log(resp);
          return resp.err;
        });
        return getdata;
      },
      fbAuthorize: function (auth) {
        var submitFbAuth = $http.post('/socialAuth/facebook', auth);
        submitFbAuth.success(function (result) {
          console.log(result);
          return result.msg;
        });
        submitFbAuth.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return submitFbAuth;
      },
      savePhoto: function (data) {
        var submitURL = $http.post('/contacts/convertImg', {data: data});
        submitURL.success(function (result) {
          console.log(result);
          return result.msg;
        });
        submitURL.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return submitURL;
      },
      uploadCSV: function (file) {
        var submitURL = $http.post('/contacts/uploadCSV', file, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        });
        submitURL.success(function (result) {
          console.log(result);
          return result.msg;
        });
        submitURL.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return submitURL;
      },

      socialAccountAuthCheck: function (auth) {
        var checkAuth = $http.post('/socialAuth/socialAccountAuthCheck');
        checkAuth.success(function (result) {
          return result.msg;
        });
        checkAuth.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return checkAuth;
      },
      InstagramOauthCheck: function () {
        var checkAuth = $http.post('/socialAuth/InstagramOauthCheck');
        checkAuth.success(function (result) {
          return result.msg;
        });
        checkAuth.error(function (resp) {
          return resp;
        });
        return checkAuth;
      },
      rmInstagramAuth: function () {
        var checkAuth = $http.post('/socialAuth/rmInstagramAuth');
        checkAuth.success(function (result) {
          return result.msg;
        });
        checkAuth.error(function (resp) {
          return resp;
        });
        return checkAuth;
      },
      rmPermission: function (permissionScope) {
        var rmPermissionCall = $http.post('/socialAuth/rmFbPermission', {permissionScope: permissionScope});
        rmPermissionCall.success(function (result) {
          console.log(result);
          return result.msg;
        });
        rmPermissionCall.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return rmPermissionCall;
      },
      setFbPermission: function (permissionScope) {
        var setFbPermissionCall = $http.post('/socialAuth/setFbPermission', {permissionScope: permissionScope});
        setFbPermissionCall.success(function (result) {
          console.log(result);
          return result.msg;
        });
        setFbPermissionCall.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return setFbPermissionCall;
      },
      removeAuthorization: function () {
        var removeAuthorizationCall = $http.post('/socialAuth/removeAuthorization');
        removeAuthorizationCall.success(function (result) {
          console.log(result);
          return result.msg;
        });
        removeAuthorizationCall.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return removeAuthorizationCall;
      },
      getDashboardData: function () {
        var getDashboardDataCall = $http.post('/dashboard/fetchAll');
        getDashboardDataCall.success(function (result) {
          // console.log(result);
          return result;
        });
        getDashboardDataCall.error(function (resp) {
          // console.log(resp);
          return resp.err;
        });
        return getDashboardDataCall;
      },
      submitSocharaOwnPost: function (data) {
        var submitSocharaOwnPostCall = $http.post('/dashboard/submitSocharaOwnPost', data);
        submitSocharaOwnPostCall.success(function (result) {
          console.log(result);
          return result.msg;
        });
        submitSocharaOwnPostCall.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return submitSocharaOwnPostCall;
      },
      rmSocharaPost: function (id) {
        var rmSocharaPostCall = $http.post('/dashboard/rmSocharaPost', {id: id});
        rmSocharaPostCall.success(function (result) {
          console.log(result);
          return result.msg;
        });
        rmSocharaPostCall.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return rmSocharaPostCall;
      },

      /************************Label CRUD **************************/
      createLabel: function (data) {
        var createLabelCall = $http.post('/label/create', {data:data});
        createLabelCall.success(function (result) {
          // console.log(result);
          return result.msg;
        });
        createLabelCall.error(function (resp) {
          // console.log(resp);
          return resp.err;
        });
        return createLabelCall;
      },
      readLabel: function () {
        var readLabelCall = $http.post('/label/read');
        readLabelCall.success(function (result) {
          // console.log(result);
          return result.msg;
        });
        readLabelCall.error(function (resp) {
          // console.log(resp);
          return resp.err;
        });
        return readLabelCall;
      },
      changeMode: function ( status ) {
        var defer = $q.defer();
        $http.post('/user/changeMode', {status:status}).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      updateLabel: function (data) {
        var updateLabelCall = $http.post('/label/update',{data:data});
        updateLabelCall.success(function (result) {
          // console.log(result);
          return result.msg;
        });
        updateLabelCall.error(function (resp) {
          // console.log(resp);
          return resp.err;
        });
        return updateLabelCall;
      },
      /****************End Label CRUD*************************/

      dashbaordGIFImages: function () {
        var dashbaordGIFImagesCall = $http.post('/dashboard/gifImages');
        dashbaordGIFImagesCall.success(function (result) {
          return result;
        });
        dashbaordGIFImagesCall.error(function (resp) {
          console.log(resp);
          return resp;
        });
        return dashbaordGIFImagesCall;
      },
      searchGIFCategory: function (term) {
        var searchGIFCategoryCall = $http.post('/dashboard/searchGIFCategory', {term: term});
        searchGIFCategoryCall.success(function (result) {
          return result;
        });
        searchGIFCategoryCall.error(function (resp) {
          // console.log(resp);
          return resp;
        });
        return searchGIFCategoryCall;
      },
      getSingleStickerCategroyImages: function (category) {
        var getSingleStickerCategroyImagesCall = $http.post('/dashboard/getSingleStickerCategroyImages', {category: category});
        getSingleStickerCategroyImagesCall.success(function (result) {
          return result;
        });
        getSingleStickerCategroyImagesCall.error(function (resp) {
          // console.log(resp);
          return resp;
        });
        return getSingleStickerCategroyImagesCall;
      },
      getOG: function (url) {
        var getOGCall = $http.post('/dashboard/og', {url: url});
        getOGCall.success(function (result) {
          return result;
        });
        getOGCall.error(function (resp) {
          // console.log(resp);
          return resp;
        });
        return getOGCall;
      },
      updateSocharaOwnPost: function (data, id) {
        var updateSocharaOwnPostCall = $http.post('/dashboard/updateSocharaOwnPost', {data: data, id: id});
        updateSocharaOwnPostCall.success(function (result) {
          return result;
        });
        updateSocharaOwnPostCall.error(function (resp) {
          // console.log(resp);
          return resp;
        });
        return updateSocharaOwnPostCall;
      },
      allUserList: function () {
        var allUserListCall = $http.post('/user/allUserList');
        allUserListCall.success(function (result) {
          return result;
        });
        allUserListCall.error(function (resp) {
          // console.log(resp);
          return resp;
        });
        return allUserListCall;
      },
      twitterOauth: function () {
        console.log('mow')
        var twitterOauthCall = $http.post('/socialAuth/oAuthTwitter');
        twitterOauthCall.success(function (result) {
          console.log(result);

          return result;
        });
        twitterOauthCall.error(function (resp) {
          console.log(resp);
          return resp;
        });
        return twitterOauthCall;
      },
      instagramOauth: function (accessToken) {
        var instagramOauthCall = $http.post('/socialAuth/oAuthInstagram', {accessToken: accessToken});
        instagramOauthCall.success(function (result) {
          return result;
        });
        instagramOauthCall.error(function (resp) {
          return resp;
        });
        return instagramOauthCall;
      },
      newTwitterOuth: function (oauthToken, oauthTokenVerifier) {
        var newTwitterOuthCall = $http.post('/socialAuth/newTwitterOuth', {
          oauthToken: oauthToken,
          oauthTokenVerifier: oauthTokenVerifier
        });
        newTwitterOuthCall.success(function (result) {
          return result;
        });
        newTwitterOuthCall.error(function (resp) {
          // console.log(resp);
          return resp;
        });
        return newTwitterOuthCall;
      },
      deAuthorizeTwitterOauth: function () {
        var deAuthorizeTwitterOauthCall = $http.post('/socialAuth/deAuthorizeTwitterOauth');
        deAuthorizeTwitterOauthCall.success(function (result) {
          return result;
        });
        deAuthorizeTwitterOauthCall.error(function (resp) {
          // console.log(resp);
          return resp;
        });
        return deAuthorizeTwitterOauthCall;
      },
      info: function () {
        var defer = $q.defer();
        $http.post('/user/info').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
    };
  }

  function authInterCeptorService($q, $injector) {

    return {
      request: function (config) {
        var token;
        if (getLocationService('ats')) {
          token = angular.fromJson(getLocationService('ats')).token;
        }
        if (token) {
          config.headers.Authorization = 'Bearer ' + token;
        }
        return config;
      },
      responseError: function (response) {
        if (response.status === 401 || response.status === 403) {
          unSetLocationService('ats');
          $injector.get('$state').go('app.auth_login');
        }
        return $q.reject(response);
      }
    }
  }

  function getLocationService(key) {
    return localStorage.getItem(key);
  }

  function setLocationService(key, value) {
    return localStorage.setItem(key, value);
  }

  function unSetLocationService(key) {
    return localStorage.removeItem(key);
  }

})();
