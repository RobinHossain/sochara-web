(function () {
  'use strict';

  angular
    .module('app.global-setting', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    $stateProvider.state('app.global-setting', {
      url: '/global-setting',
      views: {
        'content@app': {
          templateUrl: 'app/main/setting/global.html',
          controller: 'globalSettingController as vm'
        }
      },
      resolve: {},
      bodyClass: 'global-setting'
    });

  }

})();

