(function () {
  'use strict';

  angular
    .module('app.global-setting')
    .controller('globalSettingController', GlobalSettingController);

  /**
   * Description
   * @ngInject 
   * @method GlobalSettingController
   * @param {} $scope
   * @param {} $state
   * @param {} $timeout
   * @param {} $location
   * @param {} $mdToast
   * @param {} $mdDialog
   * @param {} GlobalService
   * @param {} MailService
   * @return 
   */
  function GlobalSettingController($scope, $state, $timeout, $location, $mdToast, $mdDialog, GlobalService, MailService) {

    /*HSN*/

    var vm = this;
    vm.site_url = $location.protocol() + '://' + $location.host() + ':' + $location.port();
    vm.socharaAccountSettings = socharaAccountSettings;
    vm.socharaPasswordAccountSettings = socharaPasswordAccountSettings;
    vm.closeDialog = closeDialog;
    vm.contact = {};
    vm.getProfiledata = getProfiledata;
    vm.getMailAuthUrl = getMailAuthUrl;
    vm.removeAccountConfirm = removeAccountConfirm;
    vm.removeAccount = removeAccount;
    vm.deAuthorizeTwitterOauth = deAuthorizeTwitterOauth;

    init();


    /**
     * Description
     * @method init
     * @return 
     */
    function init() {
      getProfiledata();
      $timeout(function () {
        getEmailAddress();
        socialAccountAuthCheck();
        instagramOauthChecking();
        // getIntegratedSocialAccounts();
      }, 2000);
    }

    /**
     * Fetching Contact Data
     * @method getProfiledata
     * @return 
     */
    function getProfiledata() {
      GlobalService.getProfiledata().then(function (response) {
        vm.contact = response;
        // console.log(vm.contact);
        // angular.copy(result, vm.resetContact);
        if (response == null) {
          showMessage('Something went wrong. Please try again.');
        }
      });
    }

    /**
     * Description
     * @method getIntegratedSocialAccounts
     * @return 
     */
    function getIntegratedSocialAccounts(){
      GlobalService.getIntegratedSocialAccounts().then(function (respSocialAccount) {
        vm.socialAccounts = respSocialAccount;
      });
    }


    /**
     * Sochara Account Email Setting area
     * @method socharaAccountSettings
     * @return 
     */
    function socharaAccountSettings() {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/setting/popup/sochara_account_settings.html',
        clickOutsideToClose: true
      });
    }

    /*Saving Contact Data*/
    vm.saveProfile = submitProfileData;

    /**
     * Description
     * @method submitProfileData
     * @return 
     */
    function submitProfileData() {
      GlobalService.saveProfileData(vm.contact).success(function (result) {
        closeDialog();
        getProfiledata();
        showMessage('Profile has been updated successfully!');
      }).error(function (err) {
        closeDialog();
        showMessage(err.err);
      });
    }

    /**
     * Sochara Account Password Setting area
     * @method socharaPasswordAccountSettings
     * @return 
     */
    function socharaPasswordAccountSettings() {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/setting/popup/sochara_account_password_settings.html',
        clickOutsideToClose: true
      });
    }

    /**
     * Description
     * @method closeDialog
     * @return 
     */
    function closeDialog() {
      $mdDialog.hide();
    }

    /*Social Auth Settings Checking*/
    $scope.fbAuthorized = false;
    $scope.fbAuthUserPhoto = '';
    $scope.fbAccountName = '';
    vm.fbAuthpupostsRtrnSpin = false;
    vm.fbAuthpupostsPublishSpin = false;
    vm.fbAuthmusicRtrnSpin = false;
    vm.fbAuthmoviesRtrnSpin = false;
    vm.fbAuthbookRtrnSpin = false;
    vm.fbAuthRemoveSpin = false;
    var fbGraphApiAccessArea = {};

    /**
     * Description
     * @method socialAccountAuthCheck
     * @return 
     */
    var socialAccountAuthCheck = function socialAccountAuthCheckFun() {
      GlobalService.socialAccountAuthCheck().success(function (resultAccounts) {
        resultAccounts.forEach(function (account) {
          if( account.provider === 'facebook' ){
            $scope.fbAuthorized = true;
            $scope.fbAccountName = account.accountName;
            $scope.accessArea = account.accessArea;
            $scope.fbAuthUserPhoto = '//graph.facebook.com/' + account.providerId + '/picture';
            vm.fbAuthpupostsRtrn = account.accessArea.feed;
            vm.fbAuthpupostsPublish = account.accessArea.feedPostPermission;
            vm.fbAuthmusicRtrn = account.accessArea.music;
            vm.fbAuthmoviesRtrn = account.accessArea.books;
            vm.fbAuthbookRtrn = account.accessArea.movies;
            fbGraphApiAccessArea = account.accessArea;
          }else if( account.provider === 'twitter' ){
            vm.twitterAuth = true;
            $scope.twitterUserScreenName = account.screenName;
            $scope.twitterUserImageUrl = 'https://twitter.com/' + account.screenName + '/profile_image?size=mini';
          }
        });
      }).error(function (err) {
        ////console.log(err);
      });
    };


    //Instagram Oauth checking
    /**
     * Description
     * @method instagramOauthChecking
     * @return 
     */
    var instagramOauthChecking = function () {
      GlobalService.InstagramOauthCheck().success(function (response) {
        if (response) {
          $scope.instagramAuth = true;
          $scope.instagramUserImageUrl = response.msg.profilePicture;
          $scope.instagramUserScreenName = response.msg.name;
        }
      });
    };

    /*End Social Oauth Settings*/
    /**
     * Facebook Add authorization 
     * @method fbAuthModal
     * @return 
     */
    $scope.fbAuthModal = function () {
      // vm.fbAuthposts = false;
      vm.fbAuthmusic = false;
      vm.fbAuthmovies = false;
      vm.fbAuthbook = false;
      $scope.fbAuthposts = false;
      $('#fbAuthorizeModal').appendTo('body').modal();
      // $scope.fbAuthposts = false;
    };

    //Authorizing
    /**
     * Description
     * @method fbAuthorize
     * @return 
     */
    $scope.fbAuthorize = function () {
      ////console.log(vm.fbAuthposts);
      var scope = [];
      if (vm.fbAuthPostsPermission) {
        scope.push('publish_actions');
      }
      if (vm.fbAuthposts) {
        scope.push('user_posts');
        scope.push('user_photos');
      }
      if (vm.fbAuthmusic) {
        scope.push('user_actions.music');
      }
      if (vm.fbAuthmovies) {
        scope.push('user_actions.video');
      }
      if (vm.fbAuthbook) {
        scope.push('user_actions.books');
      }
      if (vm.fbPublishpost) {
        scope.push('publish_actions');
      }
      var accessScope = scope.join(', ');
      if (scope.length > 0) {
        var accessArea = {};
        accessArea.feedPostPermission = vm.fbAuthPostsPermission;
        accessArea.feed = vm.fbAuthposts;
        // accessArea.music = vm.fbAuthmusic;
        // accessArea.movies = vm.fbAuthmovies;
        // accessArea.books = vm.fbAuthbook;
        FB.login(function (response) {
          if (response.authResponse) {
            //console.log(response.authResponse);
            FB.api(
              "/me",
              function (data) {
                if (data && !data.error) {
                  //console.log(data);
                  /* handle the result */
                  response.authResponse.name = data.name;
                  response.authResponse.accessArea = accessArea;
                  GlobalService.fbAuthorize(response.authResponse).success(function (result) {
                    $.modal.close();
                    socialAccountAuthCheck();
                    showMessage('Authorizaton has been completed successfully!');
                    vm.currentTabIndex = 1;
                  }).error(function (err) {
                    //console.log(err);
                  });
                }
              }
            );
          } else {
            alert('User cancelled login or did not fully authorize.');
          }
        }, {scope: accessScope});
        return false;

      } else {
        showMessage("Please Select any choice!");
      }
    };

    //Removing Facebook Auth
    /**
     * Description
     * @method removeAuthorization
     * @return 
     */
    $scope.removeAuthorization = function () {
      GlobalService.removeAuthorization().success(function (result) {
        $.modal.close();
        showMessage("Authorization has been successfully removed!");
        $scope.fbAuthorized = false;
        vm.currentTabIndex = 1;
      }).error(function (err) {
        //console.log(err);
        showMessage(err.err);
      }).finally(function () {
        vm.removeFBAuth = false;
        vm.fbAuthRemoveSpin = false;
      });
    };

    //Updating Facebook Auth
    /**
     * Description
     * @method changePermission
     * @param {} status
     * @param {} permissionScope
     * @param {} switchId
     * @return 
     */
    $scope.changePermission = function (status, permissionScope, switchId) {
      if (status === false) {
        // //console.log(vm.fbAuthpupostsRtrn);
        GlobalService.rmPermission(permissionScope).success(function (result) {
          showMessage("Authorization has been successfully updated!");
          vm.currentTabIndex = 1;
        }).error(function (err) {
          //console.log(err);
          showMessage(err.err);
        }).finally(function () {
          if (switchId === 'fbAuthpupostsRtrnSpin') {
            vm.fbAuthpupostsRtrnSpin = false;
          }
          if (switchId === 'fbAuthpupostsPublishSpin') {
            vm.fbAuthpupostsPublishSpin = false;
          }
          if (switchId === 'fbAuthmusicRtrnSpin') {
            vm.fbAuthmusicRtrnSpin = false;
          }
          if (switchId === 'fbAuthmoviesRtrnSpin') {
            vm.fbAuthmoviesRtrnSpin = false;
          }
          if (switchId === 'fbAuthbookRtrnSpin') {
            vm.fbAuthbookRtrnSpin = false;
          }
        });
      }
      else {
        GlobalService.setFbPermission(permissionScope).success(function (result) {
          showMessage("Authorization has been successfully updated!");
          vm.currentTabIndex = 1;
        }).error(function (err) {
          //console.log(err);
          showMessage(err.err);
        }).finally(function () {
          if (switchId === 'fbAuthpupostsRtrnSpin') {
            vm.fbAuthpupostsRtrnSpin = false;
          }
          if (switchId === 'fbAuthpupostsPublishSpin') {
            vm.fbAuthpupostsPublishSpin = false;
          }
          if (switchId === 'fbAuthmusicRtrnSpin') {
            vm.fbAuthmusicRtrnSpin = false;
          }
          if (switchId === 'fbAuthmoviesRtrnSpin') {
            vm.fbAuthmoviesRtrnSpin = false;
          }
          if (switchId === 'fbAuthbookRtrnSpin') {
            vm.fbAuthbookRtrnSpin = false;
          }
        });
      }
    };

    //checking Facebook Application Auth for the current user
    /**
     * Description
     * @method fbAsyncInit
     * @return 
     */
    window.fbAsyncInit = function () {
      FB.init({
        //appId: '294059841030709',
        appId: '1589011794492434',
        cookie: true, // This is important, it's not enabled by default
        version: 'v2.2'
      });
    };

    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    /*Twitter Oauth Settings*/

    //Adding Oauth Permission
    /**
     * Description
     * @method twitterOauth
     * @return 
     */
    $scope.twitterOauth = function () {
      GlobalService.twitterOauth().success(function (response) {
        var token = response.msg;
        window.location.href = 'https://twitter.com/oauth/authenticate?oauth_token=' + token;
      }).error(function (err) {
        showMessage("Please try again!");
      });
    };



    //Removing Oauth Permission
    /**
     * Description
     * @method deAuthorizeTwitterOauth
     * @return 
     */
    function deAuthorizeTwitterOauth() {
      GlobalService.deAuthorizeTwitterOauth().success(function (response) {
        vm.twitterAuth = false;
        showMessage('Removed authorize from twitter.');
      });
    }

    /**
     * Description
     * @method showMessage
     * @param {} message
     * @param {} type
     * @param {} position
     * @return 
     */
    function showMessage(message, type, position) {
      type = type || 'success';
      position = position || 'bottom';
      $mdToast.show({
        template: '<md-toast class="md-toast ' + type + ' ">' + message + '</md-toast>',
        hideDelay: 3000,
        position: 'bottom left'
      });
    }

    //  Checking Twitter callback url for Oauth
    var accessTokenUrl = window.location.href;
    var splitUrl = accessTokenUrl.split('?');
    if (splitUrl.length > 1) {
      var tokenString = splitUrl[1];
      var splitTokenString = tokenString.split('&');
      if (splitTokenString.length > 1) {
        var access_tokenString = splitTokenString[0];
        var oauth_verifierString = splitTokenString[1];
        var splitAccess_tokenString = access_tokenString.split('oauth_token=');
        var splitOauth_verifierString = oauth_verifierString.split('oauth_verifier=');
        if (splitAccess_tokenString.length > 1) {
          var oauthToken = splitAccess_tokenString[1];
          if (splitOauth_verifierString.length > 1) {
            var oauthTokenVerifier = splitOauth_verifierString[1];
            if (oauthToken && oauthTokenVerifier) {
              GlobalService.newTwitterOuth(oauthToken, oauthTokenVerifier).success(function (response) {
                window.history.replaceState(null, null, splitUrl[0]);
                showMessage('Twitter Authorization Completed Successfully!');
                vm.twitterAuth = true;
                $scope.twitterUserScreenName = response.msg.screen_name;
                $scope.twitterUserImageUrl = 'https://twitter.com/' + response.msg.screen_name + '/profile_image?size=mini';
                vm.currentTabIndex = 1;
              });
            }
          }
        }
      }
    }


    /**
     * Adding Instagram Oauth
     * @method instagramOauth
     * @return 
     */
    $scope.instagramOauth = function () {
      // GlobalService.getInstagramAuth().then(function (respInstagram) {
      //   console.log(respInstagram);
      // });
      window.location.href = 'https://api.instagram.com/oauth/authorize/?client_id=0ab9e25c89494bc4ae8b8743e34b0ca7&redirect_uri=https://www.sochara.com/global-setting&response_type=token';
    };

    /**
     * Removing Instagram Oauth
     * @method deAuthorizeInstagramOauth
     * @return 
     */
    $scope.deAuthorizeInstagramOauth = function () {
      GlobalService.rmInstagramAuth().success(function () {
        $scope.instagramAuth = false;
        showMessage('Instagram Authorization has been removed!');
      }).error(function () {
        showMessage('Sorry! Something went wrong!');
      });
    };

    /*Checking Instagram Oauth URL Callback*/
    /*Checking Instagram*/
    var instagramSplitUrl = accessTokenUrl.split('#');
    if (instagramSplitUrl.length > 1) {
      var accessTokenString = instagramSplitUrl[1];
      var instagramAccessToken = accessTokenString.split('access_token=');
      if (instagramAccessToken.length > 1) {
        var hash = instagramAccessToken[1];
        GlobalService.instagramOauth(hash).success(function (response) {
          $scope.instagramAuth = true;
          $scope.instagramUserImageUrl = response.msg.profilePicture;
          $scope.instagramUserScreenName = response.msg.name;
          showMessage('Authorization successfully completed!');
          vm.currentTabIndex = 1;
        }).error(function (err) {
          c('Sorry! Try Again');
        });
      }
    }

    /**
     * Description
     * @method getEmailAddress
     * @return 
     */
    function getEmailAddress() {
      MailService.getMailAddresses().then(function (response) {
        if (response && response.length) {
          vm.gmailAccounts = response.filter(function(email){
            return email.provider === 'gmail';
          });
          vm.outlookAccounts = response.filter(function (email) {
            return email.provider === 'outlook';
          });
          console.log(vm.gmailAccounts);
          console.log(vm.outlookAccounts);
        }
      });
    }

    /**
     * Description
     * @method removeAccount
     * @param {} account
     * @return 
     */
    function removeAccount(account) {
      vm.selectedRemoveAccount = account.email;
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: "vm",
        templateUrl: "app/main/mail/dialogs/remove/confirm.html",
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method removeAccountConfirm
     * @return 
     */
    function removeAccountConfirm() {
      MailService.deleteCurrentAccount({email: vm.selectedRemoveAccount}).then(function (response) {
        if (response) {
          // console.log(response);
          var message = "The Account has been removed";
          $mdDialog.hide();
          getEmailAddress();
          showMessage(message);
        }
      });
    }

    /**
     * Description
     * @method getMailAuthUrl
     * @return 
     */
    function getMailAuthUrl() {
      MailService.GMailAuthUrl().then(function (response) {
        if (response) {
          vm.gmailAuthUrl = response;
        }
      });

      MailService.OutlookAuthUrl().then(function (response) {
        if (response) {
          vm.outlookAuthUrl = response;
        }
      });
    }

  }

})();
