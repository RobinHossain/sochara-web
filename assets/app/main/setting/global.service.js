(function () {
  'use strict';

  angular
    .module('sochara')
    .factory('GlobalService', globalService);

  /** @ngInject */
  function globalService($http, $q) {
    return {
      getProfiledata: function () {
        var defer = $q.defer();
        $http.post('/user/getProfileData').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getIntegratedSocialAccounts: function () {
        var defer = $q.defer();
        $http.post('/socialAuth/socialAccounts').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getInstagramAuth: function () {
        var defer = $q.defer();
        $http.post('/socialAuth/oAuthForInstagram').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      saveProfileData: function (formData) {
        var profileSave = $http.post('/user/updateUser', formData);
        profileSave.success(function (result) {
          return result.msg;
        });
        profileSave.error(function (resp) {
          return resp.err;
        });
        return profileSave;

      },
      fbAuthorize: function (auth) {
        var submitFbAuth = $http.post('/socialAuth/facebook', auth);
        submitFbAuth.success(function (result) {
          return result.msg;
        });
        submitFbAuth.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return submitFbAuth;
      },

      twitterOauth: function () {
        console.log('mow')
        var twitterOauthCall = $http.post('/socialAuth/oAuthTwitter');
        twitterOauthCall.success(function (result) {
          console.log(result);

          return result;
        });
        twitterOauthCall.error(function (resp) {
          console.log(resp);
          return resp;
        });
        return twitterOauthCall;
      },
      instagramOauth: function (accessToken) {
        var instagramOauthCall = $http.post('/socialAuth/oAuthInstagram', {accessToken: accessToken});
        instagramOauthCall.success(function (result) {
          return result;
        });
        instagramOauthCall.error(function (resp) {
          return resp;
        });
        return instagramOauthCall;
      },
      newTwitterOuth: function (oauthToken, oauthTokenVerifier) {
        var newTwitterOuthCall = $http.post('/socialAuth/newTwitterOuth', {
          oauthToken: oauthToken,
          oauthTokenVerifier: oauthTokenVerifier
        });
        newTwitterOuthCall.success(function (result) {
          return result;
        });
        newTwitterOuthCall.error(function (resp) {
          // console.log(resp);
          return resp;
        });
        return newTwitterOuthCall;
      },
      deAuthorizeTwitterOauth: function () {
        var deAuthorizeTwitterOauthCall = $http.post('/socialAuth/deAuthorizeTwitterOauth');
        deAuthorizeTwitterOauthCall.success(function (result) {
          return result;
        });
        deAuthorizeTwitterOauthCall.error(function (resp) {
          // console.log(resp);
          return resp;
        });
        return deAuthorizeTwitterOauthCall;
      },

      socialAccountAuthCheck: function (auth) {
        var checkAuth = $http.post('/socialAuth/socialAccountAuthCheck');
        checkAuth.success(function (result) {
          return result.msg;
        });
        checkAuth.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return checkAuth;
      },
      InstagramOauthCheck: function () {
        var checkAuth = $http.post('/socialAuth/InstagramOauthCheck');
        checkAuth.success(function (result) {
          return result.msg;
        });
        checkAuth.error(function (resp) {
          return resp;
        });
        return checkAuth;
      },
      rmInstagramAuth: function () {
        var checkAuth = $http.post('/socialAuth/rmInstagramAuth');
        checkAuth.success(function (result) {
          return result.msg;
        });
        checkAuth.error(function (resp) {
          return resp;
        });
        return checkAuth;
      },
      rmPermission: function (permissionScope) {
        var rmPermissionCall = $http.post('/socialAuth/rmFbPermission', {permissionScope: permissionScope});
        rmPermissionCall.success(function (result) {
          console.log(result);
          return result.msg;
        });
        rmPermissionCall.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return rmPermissionCall;
      },
      setFbPermission: function (permissionScope) {
        var setFbPermissionCall = $http.post('/socialAuth/setFbPermission', {permissionScope: permissionScope});
        setFbPermissionCall.success(function (result) {
          console.log(result);
          return result.msg;
        });
        setFbPermissionCall.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return setFbPermissionCall;
      },
      removeAuthorization: function () {
        var removeAuthorizationCall = $http.post('/socialAuth/removeAuthorization');
        removeAuthorizationCall.success(function (result) {
          console.log(result);
          return result.msg;
        });
        removeAuthorizationCall.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return removeAuthorizationCall;
      },

    };
  }

})();
