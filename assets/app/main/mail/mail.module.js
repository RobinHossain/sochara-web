(function () {
  'use strict';

  angular
    .module('app.mail', ['app.auth.login', 'textAngular', 'ngFileUpload'])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {


    // State
    $stateProvider
      .state('app.mail', {
        abstract: true,
        url: '/mail'
      })
      .state('app.mail.account', {
        url: '/:accountId',
        views: {
          'content@app': {
            templateUrl: 'app/main/mail/mail.html',
            controller: 'MailController as vm'
          }
        }
      })
      .state('app.mail.account.threads', {
        url: '/:filter',
        bodyClass: 'mail'
      })
      .state('app.mail.account.threads.thread', {
        url: '/:threadId',
        bodyClass: 'mail'
      });


  }
})();
