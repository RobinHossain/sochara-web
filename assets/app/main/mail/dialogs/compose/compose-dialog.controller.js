

(function ()
{
  'use strict';

  angular
    .module('app.mail')
    .controller('ComposeDialogController', ComposeDialogController);

  /**
   * Description
   * @ngInject
   * @method ComposeDialogController
   * @param {} $mdDialog
   * @param {} selectedMail
   * @param {} emailAccounts
   * @param {} MailService
   * @param {} $mdToast
   * @param {} $scope
   * @param {} $location
   * @param {} selectedAccount
   * @param {} $mdConstant
   * @param {} $timeout
   * @param {} $q
   * @param {} Upload
   * @return
   */
  function ComposeDialogController($mdDialog, selectedMail , emailAccounts, MailService, ContactService, $mdToast, $scope, $location, selectedAccount, $mdConstant, $timeout, $q, Upload)
  {
    var vm = this;
    vm.form = {};
    vm.files = [];
    vm.activeButtonToolbar = '';
    vm.isActiveButton = isActiveButton;
    vm.showEditorBar = showEditorBar;
    vm.getGMailAuthUrl = getGMailAuthUrl;
    vm.formattingToMail = formattingToMail;
    vm.deleteCurrentAttachment = deleteCurrentAttachment;
    vm.attachments = [];
    vm.form.to = [];
    vm.form.cc = [];
    vm.form.bcc = [];

    if( selectedAccount ){
      vm.form.from = {};
      vm.form.from.id = selectedAccount.id;
      vm.form.from.email = selectedAccount.email;
    }


    vm.emoticonList = [
      'angel', 'angry', 'bandit', 'bartlett', 'beer', 'bigsmile', 'bike', 'blackwidow', 'blushing', 'bow', 'brokenheart',
      'bucky', 'bug', 'cake', 'call', 'captain', 'cash', 'cat', 'clapping', 'coffee', 'cool', 'crying',
      'dancing', 'devil', 'dog', 'doh', 'drink', 'drunk', 'dull', 'emo', 'envy', 'evilgrin', 'facepalm', 'finger', 'fingerscrossed',
      'flower', 'fubar', 'giggle', 'handshake', 'happy', 'headbang', 'heart', 'heidy',
      'hi', 'highfive', 'hollest', 'hug', 'idea', 'inlove', 'itwasntme', 'kiss', 'lalala', 'lipssealed', 'mail', 'makeup', 'malthe', 'mmm',
      'mooning', 'movie', 'muscle', 'music', 'nerd', 'nickfury', 'ninja', 'no', 'nod',
      'oliver', 'party', 'phone', 'pizza', 'poolparty', 'priidu', 'puking', 'punch', 'rain', 'rock', 'rofl', 'sadsmile', 'shake', 'sheep',
      'shielddeflect', 'skype', 'sleepy', 'smile', 'smirk', 'smoking', 'speechless',
      'star', 'sunshine', 'surprised', 'swear', 'sweating', 'talking', 'talktothehand', 'tauri', 'thinking', 'time', 'tmi', 'toivo',
      'tongueout', 'tumbleweed', 'wait', 'waiting', 'wfh', 'whew', 'wink', 'wondering', 'worried', 'wtf', 'yawning', 'yes'
    ];

    vm.emojiList = [{"class":"e_1","name":"bowtie"},{"class":"e_2","name":"smile"},{"class":"e_3","name":"simple_smile"},{"class":"e_4","name":"laughing"},{"class":"e_5","name":"blush"},{"class":"e_6","name":"smiley"},{"class":"e_7","name":"relaxed"},{"class":"e_8","name":"smirk"},{"class":"e_9","name":"heart_eyes"},{"class":"e_10","name":"kissing_heart"},{"class":"e_11","name":"kissing_closed_eyes"},{"class":"e_12","name":"flushed"},{"class":"e_13","name":"relieved"},{"class":"e_14","name":"satisfied"},{"class":"e_15","name":"grin"},{"class":"e_16","name":"wink"},{"class":"e_17","name":"stuck_out_tongue_winking_eye"},{"class":"e_18","name":"stuck_out_tongue_closed_eyes"},{"class":"e_19","name":"grinning"},{"class":"e_20","name":"kissing"},{"class":"e_21","name":"kissing_smiling_eyes"},{"class":"e_22","name":"stuck_out_tongue"},{"class":"e_23","name":"sleeping"},{"class":"e_24","name":"worried"},{"class":"e_25","name":"frowning"},{"class":"e_26","name":"anguished"},{"class":"e_27","name":"open_mouth"},{"class":"e_28","name":"grimacing"},{"class":"e_29","name":"confused"},{"class":"e_30","name":"hushed"},{"class":"e_31","name":"expressionless"},{"class":"e_32","name":"unamused"},{"class":"e_33","name":"sweat_smile"},{"class":"e_34","name":"sweat"},{"class":"e_35","name":"disappointed_relieved"},{"class":"e_36","name":"weary"},{"class":"e_37","name":"pensive"},{"class":"e_38","name":"disappointed"},{"class":"e_39","name":"confounded"},{"class":"e_40","name":"fearful"},{"class":"e_41","name":"cold_sweat"},{"class":"e_42","name":"persevere"},{"class":"e_43","name":"cry"},{"class":"e_44","name":"sob"},{"class":"e_45","name":"joy"},{"class":"e_46","name":"astonished"},{"class":"e_47","name":"scream"},{"class":"e_48","name":"neckbeard"},{"class":"e_49","name":"tired_face"},{"class":"e_50","name":"angry"},{"class":"e_51","name":"rage"},{"class":"e_52","name":"triumph"},{"class":"e_53","name":"sleepy"},{"class":"e_54","name":"yum"},{"class":"e_55","name":"mask"},{"class":"e_56","name":"sunglasses"},{"class":"e_57","name":"dizzy_face"},{"class":"e_58","name":"imp"},{"class":"e_59","name":"smiling_imp"},{"class":"e_60","name":"neutral_face"},{"class":"e_61","name":"no_mouth"},{"class":"e_62","name":"innocent"},{"class":"e_63","name":"alien"},{"class":"e_64","name":"yellow_heart"},{"class":"e_65","name":"blue_heart"},{"class":"e_66","name":"purple_heart"},{"class":"e_67","name":"heart"},{"class":"e_68","name":"green_heart"},{"class":"e_69","name":"broken_heart"},{"class":"e_70","name":"heartbeat"},{"class":"e_71","name":"heartpulse"},{"class":"e_72","name":"two_hearts"},{"class":"e_73","name":"revolving_hearts"},{"class":"e_74","name":"cupid"},{"class":"e_75","name":"sparkling_heart"},{"class":"e_76","name":"sparkles"},{"class":"e_77","name":"star"},{"class":"e_78","name":"star2"},{"class":"e_79","name":"dizzy"},{"class":"e_80","name":"boom"},{"class":"e_81","name":"collision"},{"class":"e_82","name":"anger"},{"class":"e_83","name":"exclamation"},{"class":"e_84","name":"question"},{"class":"e_85","name":"grey_exclamation"},{"class":"e_86","name":"grey_question"},{"class":"e_87","name":"zzz"},{"class":"e_88","name":"dash"},{"class":"e_89","name":"sweat_drops"},{"class":"e_90","name":"notes"},{"class":"e_91","name":"musical_note"},{"class":"e_92","name":"fire"},{"class":"e_93","name":"hankey"},{"class":"e_94","name":"poop"},{"class":"e_95","name":"shit"},{"class":"e_96","name":"plus1"},{"class":"e_97","name":"thumbsup"},{"class":"e_98","name":"-1"},{"class":"e_99","name":"thumbsdown"},{"class":"e_100","name":"ok_hand"},{"class":"e_101","name":"punch"},{"class":"e_102","name":"facepunch"},{"class":"e_103","name":"fist"},{"class":"e_104","name":"v"},{"class":"e_105","name":"wave"},{"class":"e_106","name":"hand"},{"class":"e_107","name":"raised_hand"},{"class":"e_108","name":"open_hands"},{"class":"e_109","name":"point_up"},{"class":"e_110","name":"point_down"},{"class":"e_111","name":"point_left"},{"class":"e_112","name":"point_right"},{"class":"e_113","name":"raised_hands"},{"class":"e_114","name":"pray"},{"class":"e_115","name":"point_up_2"},{"class":"e_116","name":"clap"},{"class":"e_117","name":"muscle"},{"class":"e_118","name":"metal"},{"class":"e_119","name":"fu"},{"class":"e_120","name":"runner"},{"class":"e_121","name":"running"},{"class":"e_122","name":"couple"},{"class":"e_123","name":"family"},{"class":"e_124","name":"two_men_holding_hands"},{"class":"e_125","name":"two_women_holding_hands"},{"class":"e_126","name":"dancer"},{"class":"e_127","name":"dancers"},{"class":"e_128","name":"ok_woman"},{"class":"e_129","name":"no_good"},{"class":"e_130","name":"information_desk_person"},{"class":"e_131","name":"raising_hand"},{"class":"e_132","name":"bride_with_veil"},{"class":"e_133","name":"person_with_pouting_face"},{"class":"e_134","name":"person_frowning"},{"class":"e_135","name":"bow"},{"class":"e_136","name":"couplekiss"},{"class":"e_137","name":"couple_with_heart"},{"class":"e_138","name":"massage"},{"class":"e_139","name":"haircut"},{"class":"e_140","name":"nail_care"},{"class":"e_141","name":"boy"},{"class":"e_142","name":"girl"},{"class":"e_143","name":"woman"},{"class":"e_144","name":"man"},{"class":"e_145","name":"baby"},{"class":"e_146","name":"older_woman"},{"class":"e_147","name":"older_man"},{"class":"e_148","name":"person_with_blond_hair"},{"class":"e_149","name":"man_with_gua_pi_mao"},{"class":"e_150","name":"man_with_turban"},{"class":"e_151","name":"construction_worker"},{"class":"e_152","name":"cop"},{"class":"e_153","name":"angel"},{"class":"e_154","name":"princess"},{"class":"e_155","name":"smiley_cat"},{"class":"e_156","name":"smile_cat"},{"class":"e_157","name":"heart_eyes_cat"},{"class":"e_158","name":"kissing_cat"},{"class":"e_159","name":"smirk_cat"},{"class":"e_160","name":"scream_cat"},{"class":"e_161","name":"crying_cat_face"},{"class":"e_162","name":"joy_cat"},{"class":"e_163","name":"pouting_cat"},{"class":"e_164","name":"japanese_ogre"},{"class":"e_165","name":"japanese_goblin"},{"class":"e_166","name":"see_no_evil"},{"class":"e_167","name":"hear_no_evil"},{"class":"e_168","name":"speak_no_evil"},{"class":"e_169","name":"guardsman"},{"class":"e_170","name":"skull"},{"class":"e_171","name":"feet"},{"class":"e_172","name":"lips"},{"class":"e_173","name":"kiss"},{"class":"e_174","name":"droplet"},{"class":"e_175","name":"ear"},{"class":"e_176","name":"eyes"},{"class":"e_177","name":"nose"},{"class":"e_178","name":"tongue"},{"class":"e_179","name":"love_letter"},{"class":"e_180","name":"bust_in_silhouette"},{"class":"e_181","name":"busts_in_silhouette"},{"class":"e_182","name":"speech_balloon"},{"class":"e_183","name":"thought_balloon"},{"class":"e_184","name":"feelsgood"},{"class":"e_185","name":"finnadie"},{"class":"e_186","name":"goberserk"},{"class":"e_187","name":"godmode"},{"class":"e_188","name":"hurtrealbad"},{"class":"e_189","name":"rage1"},{"class":"e_190","name":"rage2"},{"class":"e_191","name":"rage3"},{"class":"e_192","name":"rage4"},{"class":"e_193","name":"suspect"},{"class":"e_194","name":"trollface"},{"class":"e_195","name":"sunny"},{"class":"e_196","name":"umbrella"},{"class":"e_197","name":"cloud"},{"class":"e_198","name":"snowflake"},{"class":"e_199","name":"snowman"},{"class":"e_200","name":"zap"},{"class":"e_201","name":"cyclone"},{"class":"e_202","name":"foggy"},{"class":"e_203","name":"ocean"},{"class":"e_204","name":"cat"},{"class":"e_205","name":"dog"},{"class":"e_206","name":"mouse"},{"class":"e_207","name":"hamster"},{"class":"e_208","name":"rabbit"},{"class":"e_209","name":"wolf"},{"class":"e_210","name":"frog"},{"class":"e_211","name":"tiger"},{"class":"e_212","name":"koala"},{"class":"e_213","name":"bear"},{"class":"e_214","name":"pig"},{"class":"e_215","name":"pig_nose"},{"class":"e_216","name":"cow"},{"class":"e_217","name":"boar"},{"class":"e_218","name":"monkey_face"},{"class":"e_219","name":"monkey"},{"class":"e_220","name":"horse"},{"class":"e_221","name":"racehorse"},{"class":"e_222","name":"camel"},{"class":"e_223","name":"sheep"},{"class":"e_224","name":"elephant"},{"class":"e_225","name":"panda_face"},{"class":"e_226","name":"snake"},{"class":"e_227","name":"bird"},{"class":"e_228","name":"baby_chick"},{"class":"e_229","name":"hatched_chick"},{"class":"e_230","name":"hatching_chick"},{"class":"e_231","name":"chicken"},{"class":"e_232","name":"penguin"},{"class":"e_233","name":"turtle"},{"class":"e_234","name":"bug"},{"class":"e_235","name":"honeybee"},{"class":"e_236","name":"ant"},{"class":"e_237","name":"beetle"},{"class":"e_238","name":"snail"},{"class":"e_239","name":"octopus"},{"class":"e_240","name":"tropical_fish"},{"class":"e_241","name":"fish"},{"class":"e_242","name":"whale"},{"class":"e_243","name":"whale2"},{"class":"e_244","name":"dolphin"},{"class":"e_245","name":"cow2"},{"class":"e_246","name":"ram"},{"class":"e_247","name":"rat"},{"class":"e_248","name":"water_buffalo"},{"class":"e_249","name":"tiger2"},{"class":"e_250","name":"rabbit2"},{"class":"e_251","name":"dragon"},{"class":"e_252","name":"goat"},{"class":"e_253","name":"rooster"},{"class":"e_254","name":"dog2"},{"class":"e_255","name":"pig2"},{"class":"e_256","name":"mouse2"},{"class":"e_257","name":"ox"},{"class":"e_258","name":"dragon_face"},{"class":"e_259","name":"blowfish"},{"class":"e_260","name":"crocodile"},{"class":"e_261","name":"dromedary_camel"},{"class":"e_262","name":"leopard"},{"class":"e_263","name":"cat2"},{"class":"e_264","name":"poodle"},{"class":"e_265","name":"paw_prints"},{"class":"e_266","name":"bouquet"},{"class":"e_267","name":"cherry_blossom"},{"class":"e_268","name":"tulip"},{"class":"e_269","name":"four_leaf_clover"},{"class":"e_270","name":"rose"},{"class":"e_271","name":"sunflower"},{"class":"e_272","name":"hibiscus"},{"class":"e_273","name":"maple_leaf"},{"class":"e_274","name":"leaves"},{"class":"e_275","name":"fallen_leaf"},{"class":"e_276","name":"herb"},{"class":"e_277","name":"mushroom"},{"class":"e_278","name":"cactus"},{"class":"e_279","name":"palm_tree"},{"class":"e_280","name":"evergreen_tree"},{"class":"e_281","name":"deciduous_tree"},{"class":"e_282","name":"chestnut"},{"class":"e_283","name":"seedling"},{"class":"e_284","name":"blossom"},{"class":"e_285","name":"ear_of_rice"},{"class":"e_286","name":"shell"},{"class":"e_287","name":"globe_with_meridians"},{"class":"e_288","name":"sun_with_face"},{"class":"e_289","name":"full_moon_with_face"},{"class":"e_290","name":"new_moon_with_face"},{"class":"e_291","name":"new_moon"},{"class":"e_292","name":"waxing_crescent_moon"},{"class":"e_293","name":"first_quarter_moon"},{"class":"e_294","name":"waxing_gibbous_moon"},{"class":"e_295","name":"full_moon"},{"class":"e_296","name":"waning_gibbous_moon"},{"class":"e_297","name":"last_quarter_moon"},{"class":"e_298","name":"waning_crescent_moon"},{"class":"e_299","name":"last_quarter_moon_with_face"},{"class":"e_300","name":"first_quarter_moon_with_face"},{"class":"e_301","name":"crescent_moon"},{"class":"e_302","name":"earth_africa"},{"class":"e_303","name":"earth_americas"},{"class":"e_304","name":"earth_asia"},{"class":"e_305","name":"volcano"},{"class":"e_306","name":"milky_way"},{"class":"e_307","name":"partly_sunny"},{"class":"e_308","name":"octocat"},{"class":"e_309","name":"squirrel"}];
    vm.emojiList_2 = [{"class":"e_310","name":"bamboo"},{"class":"e_311","name":"gift_heart"},{"class":"e_312","name":"dolls"},{"class":"e_313","name":"school_satchel"},{"class":"e_314","name":"mortar_board"},{"class":"e_315","name":"flags"},{"class":"e_316","name":"fireworks"},{"class":"e_317","name":"sparkler"},{"class":"e_318","name":"wind_chime"},{"class":"e_319","name":"rice_scene"},{"class":"e_320","name":"jack_o_lantern"},{"class":"e_321","name":"ghost"},{"class":"e_322","name":"santa"},{"class":"e_323","name":"christmas_tree"},{"class":"e_324","name":"gift"},{"class":"e_325","name":"bell"},{"class":"e_326","name":"no_bell"},{"class":"e_327","name":"tanabata_tree"},{"class":"e_328","name":"tada"},{"class":"e_329","name":"confetti_ball"},{"class":"e_330","name":"balloon"},{"class":"e_331","name":"crystal_ball"},{"class":"e_332","name":"cd"},{"class":"e_333","name":"dvd"},{"class":"e_334","name":"floppy_disk"},{"class":"e_335","name":"camera"},{"class":"e_336","name":"video_camera"},{"class":"e_337","name":"movie_camera"},{"class":"e_338","name":"computer"},{"class":"e_339","name":"tv"},{"class":"e_340","name":"iphone"},{"class":"e_341","name":"phone"},{"class":"e_342","name":"telephone"},{"class":"e_343","name":"telephone_receiver"},{"class":"e_344","name":"pager"},{"class":"e_345","name":"fax"},{"class":"e_346","name":"minidisc"},{"class":"e_347","name":"vhs"},{"class":"e_348","name":"sound"},{"class":"e_349","name":"speaker"},{"class":"e_350","name":"mute"},{"class":"e_351","name":"loudspeaker"},{"class":"e_352","name":"mega"},{"class":"e_353","name":"hourglass"},{"class":"e_354","name":"hourglass_flowing_sand"},{"class":"e_355","name":"alarm_clock"},{"class":"e_356","name":"watch"},{"class":"e_357","name":"radio"},{"class":"e_358","name":"satellite"},{"class":"e_359","name":"loop"},{"class":"e_360","name":"mag"},{"class":"e_361","name":"mag_right"},{"class":"e_362","name":"unlock"},{"class":"e_363","name":"lock"},{"class":"e_364","name":"lock_with_ink_pen"},{"class":"e_365","name":"closed_lock_with_key"},{"class":"e_366","name":"key"},{"class":"e_367","name":"bulb"},{"class":"e_368","name":"flashlight"},{"class":"e_369","name":"high_brightness"},{"class":"e_370","name":"low_brightness"},{"class":"e_371","name":"electric_plug"},{"class":"e_372","name":"battery"},{"class":"e_373","name":"calling"},{"class":"e_374","name":"email"},{"class":"e_375","name":"mailbox"},{"class":"e_376","name":"postbox"},{"class":"e_377","name":"bath"},{"class":"e_378","name":"bathtub"},{"class":"e_379","name":"shower"},{"class":"e_380","name":"toilet"},{"class":"e_381","name":"wrench"},{"class":"e_382","name":"nut_and_bolt"},{"class":"e_383","name":"hammer"},{"class":"e_384","name":"seat"},{"class":"e_385","name":"moneybag"},{"class":"e_386","name":"yen"},{"class":"e_387","name":"dollar"},{"class":"e_388","name":"pound"},{"class":"e_389","name":"euro"},{"class":"e_390","name":"credit_card"},{"class":"e_391","name":"money_with_wings"},{"class":"e_392","name":"e-mail"},{"class":"e_393","name":"inbox_tray"},{"class":"e_394","name":"outbox_tray"},{"class":"e_395","name":"envelope"},{"class":"e_396","name":"incoming_envelope"},{"class":"e_397","name":"postal_horn"},{"class":"e_398","name":"mailbox_closed"},{"class":"e_399","name":"mailbox_with_mail"},{"class":"e_400","name":"mailbox_with_no_mail"},{"class":"e_401","name":"package"},{"class":"e_402","name":"door"},{"class":"e_403","name":"smoking"},{"class":"e_404","name":"bomb"},{"class":"e_405","name":"gun"},{"class":"e_406","name":"hocho"},{"class":"e_407","name":"pill"},{"class":"e_408","name":"syringe"},{"class":"e_409","name":"page_facing_up"},{"class":"e_410","name":"page_with_curl"},{"class":"e_411","name":"bookmark_tabs"},{"class":"e_412","name":"bar_chart"},{"class":"e_413","name":"chart_with_upwards_trend"},{"class":"e_414","name":"chart_with_downwards_trend"},{"class":"e_415","name":"scroll"},{"class":"e_416","name":"clipboard"},{"class":"e_417","name":"calendar"},{"class":"e_418","name":"date"},{"class":"e_419","name":"card_index"},{"class":"e_420","name":"file_folder"},{"class":"e_421","name":"open_file_folder"},{"class":"e_422","name":"scissors"},{"class":"e_423","name":"pushpin"},{"class":"e_424","name":"paperclip"},{"class":"e_425","name":"black_nib"},{"class":"e_426","name":"pencil2"},{"class":"e_427","name":"straight_ruler"},{"class":"e_428","name":"triangular_ruler"},{"class":"e_429","name":"closed_book"},{"class":"e_430","name":"green_book"},{"class":"e_431","name":"blue_book"},{"class":"e_432","name":"orange_book"},{"class":"e_433","name":"notebook"},{"class":"e_434","name":"notebook_with_decorative_cover"},{"class":"e_435","name":"ledger"},{"class":"e_436","name":"books"},{"class":"e_437","name":"bookmark"},{"class":"e_438","name":"name_badge"},{"class":"e_439","name":"microscope"},{"class":"e_440","name":"telescope"},{"class":"e_441","name":"newspaper"},{"class":"e_442","name":"football"},{"class":"e_443","name":"basketball"},{"class":"e_444","name":"soccer"},{"class":"e_445","name":"baseball"},{"class":"e_446","name":"tennis"},{"class":"e_447","name":"8ball"},{"class":"e_448","name":"rugby_football"},{"class":"e_449","name":"bowling"},{"class":"e_450","name":"golf"},{"class":"e_451","name":"mountain_bicyclist"},{"class":"e_452","name":"bicyclist"},{"class":"e_453","name":"horse_racing"},{"class":"e_454","name":"snowboarder"},{"class":"e_455","name":"swimmer"},{"class":"e_456","name":"surfer"},{"class":"e_457","name":"ski"},{"class":"e_458","name":"spades"},{"class":"e_459","name":"hearts"},{"class":"e_460","name":"clubs"},{"class":"e_461","name":"diamonds"},{"class":"e_462","name":"gem"},{"class":"e_463","name":"ring"},{"class":"e_464","name":"trophy"},{"class":"e_465","name":"musical_score"},{"class":"e_466","name":"musical_keyboard"},{"class":"e_467","name":"violin"},{"class":"e_468","name":"space_invader"},{"class":"e_469","name":"video_game"},{"class":"e_470","name":"black_joker"},{"class":"e_471","name":"flower_playing_cards"},{"class":"e_472","name":"game_die"},{"class":"e_473","name":"dart"},{"class":"e_474","name":"mahjong"},{"class":"e_475","name":"clapper"},{"class":"e_476","name":"memo"},{"class":"e_477","name":"pencil"},{"class":"e_478","name":"book"},{"class":"e_479","name":"art"},{"class":"e_480","name":"microphone"},{"class":"e_481","name":"headphones"},{"class":"e_482","name":"trumpet"},{"class":"e_483","name":"saxophone"},{"class":"e_484","name":"guitar"},{"class":"e_485","name":"shoe"},{"class":"e_486","name":"sandal"},{"class":"e_487","name":"high_heel"},{"class":"e_488","name":"lipstick"},{"class":"e_489","name":"boot"},{"class":"e_490","name":"shirt"},{"class":"e_491","name":"tshirt"},{"class":"e_492","name":"necktie"},{"class":"e_493","name":"womans_clothes"},{"class":"e_494","name":"dress"},{"class":"e_495","name":"running_shirt_with_sash"},{"class":"e_496","name":"jeans"},{"class":"e_497","name":"kimono"},{"class":"e_498","name":"bikini"},{"class":"e_499","name":"ribbon"},{"class":"e_500","name":"tophat"},{"class":"e_501","name":"crown"},{"class":"e_502","name":"womans_hat"},{"class":"e_503","name":"mans_shoe"},{"class":"e_504","name":"closed_umbrella"},{"class":"e_505","name":"briefcase"},{"class":"e_506","name":"handbag"},{"class":"e_507","name":"pouch"},{"class":"e_508","name":"purse"},{"class":"e_509","name":"eyeglasses"},{"class":"e_510","name":"fishing_pole_and_fish"},{"class":"e_511","name":"coffee"},{"class":"e_512","name":"tea"},{"class":"e_513","name":"sake"},{"class":"e_514","name":"baby_bottle"},{"class":"e_515","name":"beer"},{"class":"e_516","name":"beers"},{"class":"e_517","name":"cocktail"},{"class":"e_518","name":"tropical_drink"},{"class":"e_519","name":"wine_glass"},{"class":"e_520","name":"fork_and_knife"},{"class":"e_521","name":"pizza"},{"class":"e_522","name":"hamburger"},{"class":"e_523","name":"fries"},{"class":"e_524","name":"poultry_leg"},{"class":"e_525","name":"meat_on_bone"},{"class":"e_526","name":"spaghetti"},{"class":"e_527","name":"curry"},{"class":"e_528","name":"fried_shrimp"},{"class":"e_529","name":"bento"},{"class":"e_530","name":"sushi"},{"class":"e_531","name":"fish_cake"},{"class":"e_532","name":"rice_ball"},{"class":"e_533","name":"rice_cracker"},{"class":"e_534","name":"rice"},{"class":"e_535","name":"ramen"},{"class":"e_536","name":"stew"},{"class":"e_537","name":"oden"},{"class":"e_538","name":"dango"},{"class":"e_539","name":"egg"},{"class":"e_540","name":"bread"},{"class":"e_541","name":"doughnut"},{"class":"e_542","name":"custard"},{"class":"e_543","name":"icecream"},{"class":"e_544","name":"ice_cream"},{"class":"e_545","name":"shaved_ice"},{"class":"e_546","name":"birthday"},{"class":"e_547","name":"cake"},{"class":"e_548","name":"cookie"},{"class":"e_549","name":"chocolate_bar"},{"class":"e_550","name":"candy"},{"class":"e_551","name":"lollipop"},{"class":"e_552","name":"honey_pot"},{"class":"e_553","name":"apple"},{"class":"e_554","name":"green_apple"},{"class":"e_555","name":"tangerine"},{"class":"e_556","name":"lemon"},{"class":"e_557","name":"cherries"},{"class":"e_558","name":"grapes"},{"class":"e_559","name":"watermelon"},{"class":"e_560","name":"strawberry"},{"class":"e_561","name":"peach"},{"class":"e_562","name":"melon"},{"class":"e_563","name":"banana"},{"class":"e_564","name":"pear"},{"class":"e_565","name":"pineapple"},{"class":"e_566","name":"sweet_potato"},{"class":"e_567","name":"eggplant"},{"class":"e_568","name":"tomato"},{"class":"e_569","name":"corn"}];
    vm.emojiList_3 = [{"class":"e_570","name":"house"},{"class":"e_571","name":"house_with_garden"},{"class":"e_572","name":"school"},{"class":"e_573","name":"office"},{"class":"e_574","name":"post_office"},{"class":"e_575","name":"hospital"},{"class":"e_576","name":"bank"},{"class":"e_577","name":"convenience_store"},{"class":"e_578","name":"love_hotel"},{"class":"e_579","name":"hotel"},{"class":"e_580","name":"wedding"},{"class":"e_581","name":"church"},{"class":"e_582","name":"department_store"},{"class":"e_583","name":"european_post_office"},{"class":"e_584","name":"city_sunrise"},{"class":"e_585","name":"city_sunset"},{"class":"e_586","name":"japanese_castle"},{"class":"e_587","name":"european_castle"},{"class":"e_588","name":"tent"},{"class":"e_589","name":"factory"},{"class":"e_590","name":"tokyo_tower"},{"class":"e_591","name":"japan"},{"class":"e_592","name":"mount_fuji"},{"class":"e_593","name":"sunrise_over_mountains"},{"class":"e_594","name":"sunrise"},{"class":"e_595","name":"stars"},{"class":"e_596","name":"statue_of_liberty"},{"class":"e_597","name":"bridge_at_night"},{"class":"e_598","name":"carousel_horse"},{"class":"e_599","name":"rainbow"},{"class":"e_600","name":"ferris_wheel"},{"class":"e_601","name":"fountain"},{"class":"e_602","name":"roller_coaster"},{"class":"e_603","name":"ship"},{"class":"e_604","name":"speedboat"},{"class":"e_605","name":"boat"},{"class":"e_606","name":"sailboat"},{"class":"e_607","name":"rowboat"},{"class":"e_608","name":"anchor"},{"class":"e_609","name":"rocket"},{"class":"e_610","name":"airplane"},{"class":"e_611","name":"helicopter"},{"class":"e_612","name":"steam_locomotive"},{"class":"e_613","name":"tram"},{"class":"e_614","name":"mountain_railway"},{"class":"e_615","name":"bike"},{"class":"e_616","name":"aerial_tramway"},{"class":"e_617","name":"suspension_railway"},{"class":"e_618","name":"mountain_cableway"},{"class":"e_619","name":"tractor"},{"class":"e_620","name":"blue_car"},{"class":"e_621","name":"oncoming_automobile"},{"class":"e_622","name":"car"},{"class":"e_623","name":"red_car"},{"class":"e_624","name":"taxi"},{"class":"e_625","name":"oncoming_taxi"},{"class":"e_626","name":"articulated_lorry"},{"class":"e_627","name":"bus"},{"class":"e_628","name":"oncoming_bus"},{"class":"e_629","name":"rotating_light"},{"class":"e_630","name":"police_car"},{"class":"e_631","name":"oncoming_police_car"},{"class":"e_632","name":"fire_engine"},{"class":"e_633","name":"ambulance"},{"class":"e_634","name":"minibus"},{"class":"e_635","name":"truck"},{"class":"e_636","name":"train"},{"class":"e_637","name":"station"},{"class":"e_638","name":"train2"},{"class":"e_639","name":"bullettrain_front"},{"class":"e_640","name":"bullettrain_side"},{"class":"e_641","name":"light_rail"},{"class":"e_642","name":"monorail"},{"class":"e_643","name":"railway_car"},{"class":"e_644","name":"trolleybus"},{"class":"e_645","name":"ticket"},{"class":"e_646","name":"fuelpump"},{"class":"e_647","name":"vertical_traffic_light"},{"class":"e_648","name":"traffic_light"},{"class":"e_649","name":"warning"},{"class":"e_650","name":"construction"},{"class":"e_651","name":"beginner"},{"class":"e_652","name":"atm"},{"class":"e_653","name":"slot_machine"},{"class":"e_654","name":"busstop"},{"class":"e_655","name":"barber"},{"class":"e_656","name":"hotsprings"},{"class":"e_657","name":"checkered_flag"},{"class":"e_658","name":"crossed_flags"},{"class":"e_659","name":"izakaya_lantern"},{"class":"e_660","name":"moyai"},{"class":"e_661","name":"circus_tent"},{"class":"e_662","name":"performing_arts"},{"class":"e_663","name":"round_pushpin"},{"class":"e_664","name":"triangular_flag_on_post"},{"class":"e_665","name":"jp"},{"class":"e_666","name":"kr"},{"class":"e_667","name":"cn"},{"class":"e_668","name":"us"},{"class":"e_669","name":"fr"},{"class":"e_670","name":"es"},{"class":"e_671","name":"it"},{"class":"e_672","name":"ru"},{"class":"e_673","name":"gb"},{"class":"e_674","name":"uk"},{"class":"e_675","name":"de"},{"class":"e_676","name":"one"},{"class":"e_677","name":"two"},{"class":"e_678","name":"three"},{"class":"e_679","name":"four"},{"class":"e_680","name":"five"},{"class":"e_681","name":"six"},{"class":"e_682","name":"seven"},{"class":"e_683","name":"eight"},{"class":"e_684","name":"nine"},{"class":"e_685","name":"keycap_ten"},{"class":"e_686","name":"1234"},{"class":"e_687","name":"zero"},{"class":"e_688","name":"hash"},{"class":"e_689","name":"symbols"},{"class":"e_690","name":"arrow_backward"},{"class":"e_691","name":"arrow_down"},{"class":"e_692","name":"arrow_forward"},{"class":"e_693","name":"arrow_left"},{"class":"e_694","name":"capital_abcd"},{"class":"e_695","name":"abcd"},{"class":"e_696","name":"abc"},{"class":"e_697","name":"arrow_lower_left"},{"class":"e_698","name":"arrow_lower_right"},{"class":"e_699","name":"arrow_right"},{"class":"e_700","name":"arrow_up"},{"class":"e_701","name":"arrow_upper_left"},{"class":"e_702","name":"arrow_upper_right"},{"class":"e_703","name":"arrow_double_down"},{"class":"e_704","name":"arrow_double_up"},{"class":"e_705","name":"arrow_down_small"},{"class":"e_706","name":"arrow_heading_down"},{"class":"e_707","name":"arrow_heading_up"},{"class":"e_708","name":"leftwards_arrow_with_hook"},{"class":"e_709","name":"arrow_right_hook"},{"class":"e_710","name":"left_right_arrow"},{"class":"e_711","name":"arrow_up_down"},{"class":"e_712","name":"arrow_up_small"},{"class":"e_713","name":"arrows_clockwise"},{"class":"e_714","name":"arrows_counterclockwise"},{"class":"e_715","name":"rewind"},{"class":"e_716","name":"fast_forward"},{"class":"e_717","name":"information_source"},{"class":"e_718","name":"ok"},{"class":"e_719","name":"twisted_rightwards_arrows"},{"class":"e_720","name":"repeat"},{"class":"e_721","name":"repeat_one"},{"class":"e_722","name":"new"},{"class":"e_723","name":"top"},{"class":"e_724","name":"up"},{"class":"e_725","name":"cool"},{"class":"e_726","name":"free"},{"class":"e_727","name":"ng"},{"class":"e_728","name":"cinema"},{"class":"e_729","name":"koko"},{"class":"e_730","name":"signal_strength"},{"class":"e_731","name":"u5272"},{"class":"e_732","name":"u5408"},{"class":"e_733","name":"u55b6"},{"class":"e_734","name":"u6307"},{"class":"e_735","name":"u6708"},{"class":"e_736","name":"u6709"},{"class":"e_737","name":"u6e80"},{"class":"e_738","name":"u7121"},{"class":"e_739","name":"u7533"},{"class":"e_740","name":"u7a7a"},{"class":"e_741","name":"u7981"},{"class":"e_742","name":"sa"},{"class":"e_743","name":"restroom"},{"class":"e_744","name":"mens"},{"class":"e_745","name":"womens"},{"class":"e_746","name":"baby_symbol"},{"class":"e_747","name":"no_smoking"},{"class":"e_748","name":"parking"},{"class":"e_749","name":"wheelchair"},{"class":"e_750","name":"metro"},{"class":"e_751","name":"baggage_claim"},{"class":"e_752","name":"accept"},{"class":"e_753","name":"wc"},{"class":"e_754","name":"potable_water"},{"class":"e_755","name":"put_litter_in_its_place"},{"class":"e_756","name":"secret"},{"class":"e_757","name":"congratulations"},{"class":"e_758","name":"m"},{"class":"e_759","name":"passport_control"},{"class":"e_760","name":"left_luggage"},{"class":"e_761","name":"customs"},{"class":"e_762","name":"ideograph_advantage"},{"class":"e_763","name":"cl"},{"class":"e_764","name":"sos"},{"class":"e_765","name":"id"},{"class":"e_766","name":"no_entry_sign"},{"class":"e_767","name":"underage"},{"class":"e_768","name":"no_mobile_phones"},{"class":"e_769","name":"do_not_litter"},{"class":"e_770","name":"non-potable_water"},{"class":"e_771","name":"no_bicycles"},{"class":"e_772","name":"no_pedestrians"},{"class":"e_773","name":"children_crossing"},{"class":"e_774","name":"no_entry"},{"class":"e_775","name":"eight_spoked_asterisk"},{"class":"e_776","name":"sparkle"},{"class":"e_777","name":"eight_pointed_black_star"},{"class":"e_778","name":"heart_decoration"},{"class":"e_779","name":"vs"},{"class":"e_780","name":"vibration_mode"},{"class":"e_781","name":"mobile_phone_off"},{"class":"e_782","name":"chart"},{"class":"e_783","name":"currency_exchange"},{"class":"e_784","name":"aries"},{"class":"e_785","name":"taurus"},{"class":"e_786","name":"gemini"},{"class":"e_787","name":"cancer"},{"class":"e_788","name":"leo"},{"class":"e_789","name":"virgo"},{"class":"e_790","name":"libra"},{"class":"e_791","name":"scorpius"},{"class":"e_792","name":"sagittarius"},{"class":"e_793","name":"capricorn"},{"class":"e_794","name":"aquarius"},{"class":"e_795","name":"pisces"},{"class":"e_796","name":"ophiuchus"},{"class":"e_797","name":"six_pointed_star"},{"class":"e_798","name":"negative_squared_cross_mark"},{"class":"e_799","name":"a"},{"class":"e_800","name":"b"},{"class":"e_801","name":"ab"},{"class":"e_802","name":"o2"},{"class":"e_803","name":"diamond_shape_with_a_dot_inside"},{"class":"e_804","name":"recycle"},{"class":"e_805","name":"end"},{"class":"e_806","name":"back"},{"class":"e_807","name":"on"},{"class":"e_808","name":"soon"},{"class":"e_809","name":"clock1"},{"class":"e_810","name":"clock130"},{"class":"e_811","name":"clock10"},{"class":"e_812","name":"clock1030"},{"class":"e_813","name":"clock11"},{"class":"e_814","name":"clock1130"},{"class":"e_815","name":"clock12"},{"class":"e_816","name":"clock1230"},{"class":"e_817","name":"clock2"},{"class":"e_818","name":"clock230"},{"class":"e_819","name":"clock3"},{"class":"e_820","name":"clock330"},{"class":"e_821","name":"clock4"},{"class":"e_822","name":"clock430"},{"class":"e_823","name":"clock5"},{"class":"e_824","name":"clock530"},{"class":"e_825","name":"clock6"},{"class":"e_826","name":"clock630"},{"class":"e_827","name":"clock7"},{"class":"e_828","name":"clock730"},{"class":"e_829","name":"clock8"},{"class":"e_830","name":"clock830"},{"class":"e_831","name":"clock9"},{"class":"e_832","name":"clock930"},{"class":"e_833","name":"heavy_dollar_sign"},{"class":"e_834","name":"copyright"},{"class":"e_835","name":"registered"},{"class":"e_836","name":"tm"},{"class":"e_837","name":"x"},{"class":"e_838","name":"heavy_exclamation_mark"},{"class":"e_839","name":"bangbang"},{"class":"e_840","name":"interrobang"},{"class":"e_841","name":"o"},{"class":"e_842","name":"heavy_multiplication_x"},{"class":"e_843","name":"heavy_plus_sign"},{"class":"e_844","name":"heavy_minus_sign"},{"class":"e_845","name":"heavy_division_sign"},{"class":"e_846","name":"white_flower"},{"class":"e_847","name":"100"},{"class":"e_848","name":"heavy_check_mark"},{"class":"e_849","name":"ballot_box_with_check"},{"class":"e_850","name":"radio_button"},{"class":"e_851","name":"link"},{"class":"e_852","name":"curly_loop"},{"class":"e_853","name":"wavy_dash"},{"class":"e_854","name":"part_alternation_mark"},{"class":"e_855","name":"trident"},{"class":"e_856","name":"black_small_square"},{"class":"e_857","name":"white_small_square"},{"class":"e_858","name":"black_medium_small_square"},{"class":"e_859","name":"white_medium_small_square"},{"class":"e_860","name":"black_medium_square"},{"class":"e_861","name":"white_medium_square"},{"class":"e_862","name":"black_square"},{"class":"e_863","name":"white_large_square"},{"class":"e_864","name":"white_check_mark"},{"class":"e_865","name":"black_square_button"},{"class":"e_866","name":"white_square_button"},{"class":"e_867","name":"black_circle"},{"class":"e_868","name":"white_circle"},{"class":"e_869","name":"red_circle"},{"class":"e_870","name":"large_blue_circle"},{"class":"e_871","name":"large_blue_diamond"},{"class":"e_872","name":"large_orange_diamond"},{"class":"e_873","name":"small_blue_diamond"},{"class":"e_874","name":"small_orange_diamond"},{"class":"e_875","name":"small_red_triangle"},{"class":"e_876","name":"small_red_triangle_down"},{"class":"e_877","name":"shipit"}];



    vm.insertEmoticonToMessageBody = insertEmoticonToMessageBody;
    vm.insertEmojiToMessageBody = insertEmojiToMessageBody;


    // Data
    // vm.form = {
    //   from: 'johndoe@creapond.com'
    // };

    vm.hiddenCC = true;
    vm.hiddenBCC = true;
    vm.emoAreaVisible = false;





    // If replying
    if ( angular.isDefined(selectedMail) )
    {
      // console.log(selectedMail);
      // console.log(emailAccounts);
      if( selectedMail.from ){
        vm.form.from = {};
        vm.form.from.id = selectedMail.from.id;
        vm.form.from.email = selectedMail.from.email;
      }

      if( selectedMail.type === 'reply' ){
        vm.form.to = selectedMail.to;
        vm.form.subject = 'RE: ' + selectedMail.subject;
        vm.form.message = '<br>' +'<blockquote>' +  selectedMail.message + '</blockquote>';
      }
      else if( selectedMail.type === 'forward' ){
        vm.form.subject = 'FWD: ' + selectedMail.subject;
        var fwdHtml = '<br>---------- Forwarded message ----------';
        fwdHtml = fwdHtml + '<br>From: ' + selectedMail.to;
        fwdHtml = fwdHtml + '<br>Date: ' + selectedMail.time;
        fwdHtml = fwdHtml + '<br>Subject: ' + selectedMail.subject;
        fwdHtml = fwdHtml + '<br>To: ' + selectedMail.from.email;
        fwdHtml = fwdHtml + '<br>' + selectedMail.message;
        vm.form.message = fwdHtml;
      }
      else if( selectedMail.type === 'global' ){
        vm.form.to = selectedMail.to;
        // vm.selectedToItem = {email: 'robin@lskdf.com', name: 'hey'};
        // vm.querySearch = 'robin@lskdf.com';
      }
    }

    // Methods
    vm.closeDialog = closeDialog;
    vm.sendMail = sendMail;

    // Upload Attachment
    vm.uploadAttachment = uploadAttachment;
    vm.attachmentSize = attachmentSize;
    vm.showEmoArea = showEmoArea;
    vm.showEmoticonArea = showEmoticonArea;
    vm.showEmojiArea = showEmojiArea;
    vm.showEmojiArea_2 = showEmojiArea_2;
    vm.showEmojiArea_3 = showEmojiArea_3;
    //////////

    /**
     * Description
     * @method closeDialog
     * @return
     */
    function closeDialog()
    {
      $mdDialog.hide();
    }

    init();

    function init(){
      loadContacts();
    }

    /**
     * Description
     * @method isActiveButton
     * @param {} button
     * @return
     */
    function isActiveButton( button ) {
      if( vm.activeButtonToolbar === button ){
        return 'active';
      }else{
        return '';
      }
    }

    /**
     * Description
     * @method showEmoArea
     * @return
     */
    function showEmoArea() {
      if( vm.emoAreaVisible === true ){
        vm.emoAreaVisible = false;
      }else{
        vm.emoAreaVisible = true;
        vm.showEmojiList = false;
        vm.showEmojiList_2 = false;
        vm.showEmojiList_3 = false;
        vm.showEmoticonList = true;
      }
    }

    /**
     * Description
     * @method showEmoticonArea
     * @return
     */
    function showEmoticonArea(){
      vm.showEmojiList = false;
      vm.showEmojiList_2 = false;
      vm.showEmojiList_3 = false;
      vm.showEmoticonList = true;
    }

    /**
     * Description
     * @method showEmojiArea
     * @return
     */
    function showEmojiArea(){
      vm.showEmoticonList = false;
      vm.showEmojiList_2 = false;
      vm.showEmojiList_3 = false;
      vm.showEmojiList = true;
    }

    /**
     * Description
     * @method showEmojiArea_2
     * @return
     */
    function showEmojiArea_2(){
      vm.showEmoticonList = false;
      vm.showEmojiList = false;
      vm.showEmojiList_3 = false;
      vm.showEmojiList_2 = true;
    }

    /**
     * Description
     * @method showEmojiArea_3
     * @return
     */
    function showEmojiArea_3(){
      vm.showEmoticonList = false;
      vm.showEmojiList = false;
      vm.showEmojiList_2 = false;
      vm.showEmojiList_3 = true;
    }


    /**
     * Description
     * @method showEditorBar
     * @return
     */
    function showEditorBar() {
      if ( vm.activeButtonToolbar === 'editor' ){
        vm.activeButtonToolbar = null;
      }else{
        vm.activeButtonToolbar = 'editor';
      }
    }

    /**
     * insertAtCaret
     * @method insertAtCaret
     * @param text
     * @param areaId
     * @return
     */
    function insertAtCaret(text, areaId) {
      var txtarea = document.getElementById(areaId);
      if (!txtarea) { return; }
      var scrollPos = txtarea.scrollTop;
      var strPos = 0;
      var br = ((txtarea.selectionStart || txtarea.selectionStart === '0') ?
        "ff" : (document.selection ? "ie" : false ) );
      if (br === "ie") {
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        strPos = range.text.length;
      } else if (br === "ff") {
        strPos = txtarea.selectionStart;
      }

      var front = (txtarea.value).substring(0, strPos);
      var back = (txtarea.value).substring(strPos, txtarea.value.length);
      txtarea.value = front + text + back;
      strPos = strPos + text.length;
      if (br === "ie") {
        txtarea.focus();
        var ieRange = document.selection.createRange();
        ieRange.moveStart ('character', -txtarea.value.length);
        ieRange.moveStart ('character', strPos);
        ieRange.moveEnd ('character', 0);
        ieRange.select();
      } else if (br === "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
      }

      txtarea.scrollTop = scrollPos;
      txtarea.focus();

      vm.form.message = txtarea.value;

    }

    vm.getCursorPosition = getCursorPosition;

    /**
     * Description
     * @method getCursorPosition
     * @return
     */
    function getCursorPosition() {
      var pos = window.getSelection().getRangeAt(0);
      if( pos ) vm.cursorPositionRange = pos;
    }

    /**
     * Description
     * @method insertContentToTexAngularCursorPosition
     * @param {} content
     * @return
     */
    function insertContentToTexAngularCursorPosition( content ){
      if( vm.cursorPositionRange ){
        var el = document.createElement("div");
        el.innerHTML = content;
        var frag = document.createDocumentFragment(), node, lastNode;
        while ( (node = el.firstChild) ) {
          lastNode = frag.appendChild(node);
        }
        var firstNode = frag.firstChild;
        vm.cursorPositionRange.insertNode(frag);
      }
    }


    /**
     * Description
     * @method getGMailAuthUrl
     * @return
     */
    function getGMailAuthUrl() {
      MailService.GMailAuthUrl().then(function (response) {
        if( response ){
          vm.GMailAuthUrl = response;
        }
      })
    }

    /**
     * Description
     * @method insertEmoticonToMessageBody
     * @param {} icon
     * @return
     */
    function insertEmoticonToMessageBody( icon ) {
      var baseUrl = $location.protocol() + '://'+ $location.host();
      var iconUrl = '<img src="'+ baseUrl + '/vendors/skypecon/src/emoticons/still@2x/' + icon + '.png' +'" alt="'+ icon +'">';
      var areaID = angular.element("#messageDetails").find('textarea').attr("id");
      // insertAtCaret( iconUrl, areaID );
      insertContentToTexAngularCursorPosition( iconUrl );
      vm.emoAreaVisible = false;
    }


    /**
     * Description
     * @method insertEmojiToMessageBody
     * @param {} icon
     * @return
     */
    function insertEmojiToMessageBody( icon ) {
      var baseUrl = $location.protocol() + '://'+ $location.host();
      var iconUrl = '<img src="'+ baseUrl + '/vendors/emoji-cheat-sheet.com/public/graphics/emojis/' + icon + '.png' +'" alt="'+ icon +'">';
      var areaID = angular.element("#messageDetails").find('textarea').attr("id");
      // insertAtCaret( iconUrl, areaID );
      insertContentToTexAngularCursorPosition( iconUrl );
      vm.emoAreaVisible = false;
    }


    /**
     * Description
     * @method sendMail
     * @return
     */
    function sendMail() {
      var data = {};

      if( !vm.form.from || !vm.form.from.id || !vm.form.to || !vm.form.to.length || !vm.form.subject || !vm.form.message ){
        var message;
        if( !vm.form.from || !vm.form.from.id ){
          message = "The email address you entered for From recipient is not listed or Invalid, please select a correct email from dropdown.";
        }else if( !vm.form.to || !vm.form.to.length ) {
          message = "'To' recipient is empty. Please input 'To' recipient email to send mail.";
        }else if( !vm.form.subject ) {
          message = "Please input subject to send mail.";
        }else if( !vm.form.message ) {
          message = "Please input message body to send mail.";
        }
        $mdToast.show({
          template: '<md-toast class="md-toast success">'+ message + '</md-toast>',
          hideDelay: 3000,
          position: 'bottom left'
        });
        return;
      }
      data.id = vm.form.from.id;
      data.from = vm.form.from.email;
      data.subject = vm.form.subject;
      data.body = vm.form.message;

      if( vm.attachments && vm.attachments.length ){
        data.attachments = vm.attachments;
      }

      // console.log(data.from);

      // console.log(vm.mailAccounts);

      // var filteredArray = data.record.filter(function(itm){
      //   return empIds.indexOf(itm.empid) > -1;
      // });

      var tData = [];
      if( typeof vm.form.to === 'object' ){
        vm.form.to.forEach(function (val) {
          tData.push(val.email);
        });
        data.to = tData.join(",");
      }else{
        data.to = vm.form.to;
      }



      var cData = [];
      vm.form.cc.forEach(function (val) {
        cData.push(val.email);
      });
      data.cc = cData.join(",");

      var bData = [];
      vm.form.bcc.forEach(function (val) {
        bData.push(val.email);
      });
      data.bcc = bData.join(",");

      $mdDialog.hide();


      // console.log(vm.attachments.length);

      MailService.sendEmail(data).then(function (response) {
        var message = "Mail has been sent.";
        if( response ){
          $mdToast.show({
            template: '<md-toast class="md-toast success">' + message + '</md-toast>',
            hideDelay: 3000,
            position: 'bottom left'
          });
        }else {
          $mdDialog.hide();
        }

      });

    }




    /**
     * Description
     * @method fileUploadFiles
     * @param {} file
     * @param {} i
     * @return
     */
    function fileUploadFiles ( file, i ) {
      var postUrl;
      postUrl = '/admin/files/upload/files';

      console.log(file);
      // console.log(i);
    }

    /**
     * Description
     * @method uploadAttachment
     * @param {} files
     * @param {} errFiles
     * @return
     */
    function uploadAttachment(files, errFiles) {
      vm.uploadNotification = true;
      vm.errFiles = errFiles;
      vm.attachments = [];

      if (files && files.length) {

        files.forEach(function ( file ) {
          if (!file.$error) {
            var attachment = {};
            attachment.id = file.$$hashKey;
            attachment.name = file.name;
            attachment.size = file.size;
            attachment.type = file.type;
            Upload.base64DataUrl(file).then(function( url ){
              attachment.url =  url.replace(/^data:image\/(png|jpg|jpeg|gif|bmp|svg|webp);base64,/, "");
              vm.attachments.push(attachment);
            });
          }
        });
      }

    }

    /**
     * Description
     * @method attachmentSize
     * @param {} size
     * @return rData
     */
    function attachmentSize( size ){
      var fsize = parseFloat(parseInt(size)/1024).toFixed(2);
      var rData = fsize + ' KB';
      return rData;
    }



    // For Search Email
    vm.simulateQuery = false;
    vm.isDisabledFrom    = false;

    // list of `state` value/display objects
    var mailAccounts        = loadAllMail();
    vm.querySearchMail   = querySearchMail;

    /**
     * Description
     * @method querySearchMail
     * @param {} query
     * @return
     */
    function querySearchMail (query) {
      var results = query ? mailAccounts.filter( createFilterForMail(query) ) : mailAccounts,
        deferred;
      if (vm.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }

    /**
     * Description
     * @method loadAllMail
     * @return CallExpression
     */
    function loadAllMail() {
      vm.mailAccounts = emailAccounts;
      return vm.mailAccounts.map( function ( account ) {
        return {
          id: account.id,
          email: account.email
        };
      });
      // return true;
    }

    /**
     * Description
     * @method createFilterForMail
     * @param {} query
     * @return FunctionExpression
     */
    function createFilterForMail(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(mail) {
        return (mail.email.indexOf(lowercaseQuery) === 0);
      };

    }
    // For Search Email

    /**
     * Description
     * @method formattingToMail
     * @return
     */
    function formattingToMail(){
      var toMails = vm.form.to;
      var toMailsOutput = "";
      if( toMails.indexOf(',') > -1 || toMails.indexOf(' ') > -1 ){
        toMails = toMails.split(/[ ,]+/);
        // toMails.forEach(function (val) {
        //   toMailsOutput = toMailsOutput + "<span class='to_email_selected'>" + val + "<span class=''>&Cross;</span></span>";
        // });
      }
    }

    /**
     * ------------------------------------- Delete Attachment by ID -----------------------------------------------
     * @method deleteCurrentAttachment
     * @param {} id
     * @return
     */
    function deleteCurrentAttachment( id ){

      for (var i = 0; i < vm.files.length; ++i) {
        if ( vm.files[i]['$$hashKey'] === id ) {
          vm.files.splice([i],1);
        }
      }

      for (i = 0; i < vm.attachments.length; ++i) {
        if ( vm.attachments[i]['id'] === id ) {
          vm.attachments.splice([i],1);
        }
      }
    }
    /*------------------------------------- end Delete Attachment by ID -----------------------------------------------*/

    /* --------------------------------- For Custom Chips With Enter And Comma ------------------------------------ */

    // Any key code can be used to create a custom separator
    var semicolon = 186;
    vm.customKeys = [$mdConstant.KEY_CODE.ENTER, $mdConstant.KEY_CODE.SPACE, $mdConstant.KEY_CODE.COMMA, semicolon, $mdConstant.KEY_CODE.TAB];


    // New With Autocomplate
    vm.selectedToItem = null;
    vm.selectedCcItem = null;
    vm.selectedBccItem = null;

    vm.searchToEmail = null;
    vm.searchCcEmail = null;
    vm.searchBccEmail = null;


    // vm.selectedCcEmails = [];
    // vm.selectedBccEmails = [];

    vm.querySearch = querySearch;
    // vm.contactsData = loadContacts();
    vm.autocompleteDemoRequireMatch = false;
    vm.transformChip = transformChip;
    vm.ifValidateToEmail = ifValidateToEmail;
    vm.ifValidateCcEmail = ifValidateCcEmail;
    vm.ifValidateBccEmail = ifValidateBccEmail;
    /**
     * Return the proper object when the append is called.
     * @method transformChip
     * @param {} chip
     * @return ObjectExpression
     */
    function transformChip(chip) {
      // If it is an object, it's already a known chip
      if (angular.isObject(chip)) {
        return chip;
      }

      // Otherwise, create a new one
      return { email: chip, name: 'new' }
    }


    /**
     * Create filter function for a query string
     * @method createFilterFor
     * @param {} query
     * @return FunctionExpression
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(vegetable) {
        return (vegetable._lowername.indexOf(lowercaseQuery) === 0) ||
          (vegetable._lowertype.indexOf(lowercaseQuery) === 0);
      };
    }

    /**
     * Description
     * @method ifValidateToEmail
     * @return
     */
    function ifValidateToEmail(){
      if( vm.searchToEmail && vm.searchToEmail.length ){
        if ( validateEmail( vm.searchToEmail ) ){
          return false;
        }else{
          return true;
        }
      }else{
        return true;
      }
    }

    /**
     * Description
     * @method ifValidateCcEmail
     * @return
     */
    function ifValidateCcEmail(){
      if( vm.searchCcEmail && vm.searchCcEmail.length ){
        if ( validateEmail( vm.searchCcEmail ) ){
          return false;
        }else{
          return true;
        }
      }else{
        return true;
      }
    }

    /**
     * Description
     * @method ifValidateBccEmail
     * @return
     */
    function ifValidateBccEmail(){
      if( vm.searchBccEmail && vm.searchBccEmail.length ){
        if ( validateEmail( vm.searchBccEmail ) ){
          return false;
        }else{
          return true;
        }
      }else{
        return true;
      }
    }

    /**
     * Search for vegetables.
     * @method querySearch
     * @param {} query
     * @return results
     */
    function querySearch (query) {
      // query = query.length > 1 ? query : '';
      var results = query ? ContactService.getAllSocharaUsers({name: query}).then(function (response) {
        var totalResponse = response.map(function (c) {
          var contact = {
            name: c.first_name + ' ' + c.last_name,
            email: c.email,
            photo_url: c.photo_url ? c.photo_url.replace('assets', '').replace(/\\/, '/') : '../images/avatars/blank.jpg',
            id: c.id || undefined,
          };
          // contact._lowername = contact.name.toLowerCase();
          return {
            value: contact.name.toLowerCase(),
            name: contact.name,
            email: contact.email,
            image: contact.photo_url,
            isContact: false,
            id: contact.id
          };
        });
        var searchQuery = angular.lowercase(query);
        totalResponse = totalResponse.concat(vm.filteredContacts);
        return totalResponse.filter(function ( state ) {
          return state.value.indexOf(searchQuery) === 0;
        });
      }) : vm.filteredContacts;
      var deferred = $q.defer();
      $timeout(function () {
        deferred.resolve(results);
      }, Math.random() * 1000, false);
      return deferred.promise;
      // new functions

      // var results = query ? vm.contactsData.filter(createFilterFor(query)) : [];
      // return results;
    }

    /**
     * Description
     * @method loadContacts
     * @return CallExpression
     */
    function loadContacts() {

      // new methods
      ContactService.getContacts().then(function (resp) {
        if (resp) {
          vm.filteredContacts = resp.map(function (c) {
            var contact = {
              name: c.first_name + ' ' + c.last_name,
              email: c.email,
              photo_url: c.photo_url ? c.photo_url.replace('assets', '').replace(/\\/, '/') : '../images/avatars/blank.jpg',
              id: c.sochara_user_id || undefined,
            };
            // contact._lowername = contact.name.toLowerCase();
            return {
              value: contact.name.toLowerCase(),
              name: contact.name,
              email: contact.email,
              isContact: true,
              image: contact.photo_url,
              id: contact.id
            };
          });
        }
      });
      // new methods

      // var veggies = [
      //   {
      //     'email': 'new@email.com',
      //     'name': 'Brassica'
      //   },
      //   {
      //     'email': 'other@gmail.com',
      //     'name': 'Brassica'
      //   },
      //   {
      //     'email': 'option@gmail.com',
      //     'name': 'Umbelliferous'
      //   }
      // ];
      //
      // return veggies.map(function (veg) {
      //   veg._lowername = veg.email.toLowerCase();
      //   veg._lowertype = veg.name.toLowerCase();
      //   return veg;
      // });
    }

    /**
     * Description
     * @method validateEmail
     * @param {} email
     * @return CallExpression
     */
    function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

    /* --------------------------------- For Custom Chips With Enter And Comma ------------------------------------ */

  }
})();


(function ()
{
  'use strict';
  angular
    .module('app.core')
    .directive('mdChips', function () {
      return {
        restrict: 'E',
        require: 'mdChips',
        /**
         * Description
         * @method link
         * @param {} scope
         * @param {} element
         * @param {} attributes
         * @param {} mdChipsCtrl
         * @return
         */
        link: function (scope, element, attributes, mdChipsCtrl) {
          /**
           * Description
           * @method onInputBlur
           * @return
           */
          mdChipsCtrl.onInputBlur = function () {
            this.inputHasFocus = false;
            var chipBuffer = this.getChipBuffer();
            if ( chipBuffer !== "" && validateEmail( chipBuffer ) ) {
              this.appendChip(chipBuffer);
              this.resetChipBuffer();
            }
          };
        }
      };
    });
})();

/**
 * Description
 * @method validateEmail
 * @param {} email
 * @return CallExpression
 */
function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
