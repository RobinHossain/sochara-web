(function ()
{
  'use strict';

  angular
    .module('sochara')
    .factory('MailService', mailService);

  /** @ngInject */
  function mailService( $http, $q ){
    return {

      getGMailAddresses: function() {
        var defer = $q.defer();
        $http.post('/gmail/getGMailAddresses').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getMailAddresses: function ( provider ) {
        var defer = $q.defer(); var data = {}; if( provider ) { data.provider = provider; }
        $http.post('/mail/getMailAddresses', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getMailLabels: function () {
        var defer = $q.defer();
        $http.post('/mail/getMailLabels').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      checkEmailStates: function () {
        var defer = $q.defer();
        $http.post('/mail/checkEmailStates').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      loadSubFolders: function ( data ) {
        var defer = $q.defer();
        $http.post('/mail/loadSubFolders', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getUserInfoById: function ( id ) {
        var defer = $q.defer(); var data = {}; data.id = id;
        $http.post('/user/getUserById', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      loadGMailBoxes: function(id) {
        var defer = $q.defer();
        $http.post('/gmail/getListsOfMailboxes/'+id).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      loadGMailThreads: function( data ) {
        var defer = $q.defer();
        $http.post('/gmail/getMailThreads/', data ).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      loadGMailThread: function (GMailID, threadID) {
        var defer = $q.defer();
        var data = {}; data.id = GMailID; data.threadID = threadID;
        $http.post('/gmail/getMailThreadDetails/', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getGMailAttachment: function(gmailId, threadID, attachmentID) {
        var defer = $q.defer();
        var data = {}; data.id = gmailId; data.threadID = threadID; data.attachmentID = attachmentID;
        $http.post('/gmail/getMailAttachment/', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      modifyThreadFromSochara: function ( data ) {
        var defer = $q.defer();
        $http.post('/mail/modifyThreadFromSochara/', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      countAllUnreadMail: function ( data ) {
        var defer = $q.defer();
        $http.post('/mail/countAllUnreadMail/', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      loadAccountMailCounting: function ( data ) {
        var defer = $q.defer();
        $http.post('/mail/loadAccountMailCounting/', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      modifyThreadGMail: function(gmailId, threadID, addLabelIds, removeLabelIds) {
        var defer = $q.defer();
        var data = {}; data.id = gmailId; data.threadID = threadID; data.addLabelIds = addLabelIds; data.removeLabelIds = removeLabelIds;
        $http.post('/gmail/modifyThread/', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      deleteThreadParmanentGMail: function( threads ) {
        var defer = $q.defer();
        var data = {}; data.threads = threads;
        $http.post('/gmail/deleteParmanentThreadGMail', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getThreadCountGMail: function(gmailId, mailbox) {
        var defer = $q.defer();
        var data = {}; data.id = gmailId; data.mailbox = mailbox;
        $http.post('/gmail/getThreadCountGMail/', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      sendEmail: function(data) {
        var defer = $q.defer();
        $http.post('/mail/sendEmail', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getMessage: function(data) {
        var defer = $q.defer();
        $http.post('/mail/getMail', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      GMailAuthUrl: function() {
        var defer = $q.defer();
        $http.post('/gmail/getAuthUrl').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      info: function () {
        var defer = $q.defer();
        $http.post('/user/info').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getGmailMessages: function ( data ) {
        var defer = $q.defer();
        $http.post('/mail/getGmailMessages', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getOutlookMessages: function ( data ) {
        var defer = $q.defer();
        $http.post('/mail/getOutlookMessages', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      send: function ( data ) {
        var defer = $q.defer();
        $http.post('/calendar/sendMail', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getMessages: function ( data ) {
        var defer = $q.defer();
        $http.post('/mail/getMessages', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      findSearchedMail: function ( data ) {
        var defer = $q.defer();
        $http.post('/mail/findSearchedMail', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getNextMessages: function ( data ) {
        var defer = $q.defer();
        $http.post('/mail/getNextMessages', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },


      GMailThreadCount: function ( data ) {
        var defer = $q.defer();
        $http.post('/mail/GMailThreadCount', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      GMailThreadFolderCount: function ( data ) {
        var defer = $q.defer();
        $http.post('/mail/GMailThreadFolderCount', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      deleteCurrentAccount: function ( data ) {
        var defer = $q.defer();
        $http.post('/mail/RemoveCurrentAccount', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      /* ---------------------------------------------------------------------------------------------------------------- */
      /* --------------------------------------------- Outlook Mail Operation ------------------------------------------- */
      /* ---------------------------------------------------------------------------------------------------------------- */
      OutlookAuthUrl: function( data ) {
        var defer = $q.defer();
        $http.post('/outlook/authUrl', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getOutlookUnreadMailCount: function( data ) {
        var defer = $q.defer();
        $http.post('/outlook/getUnreadMailCount', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getOutlookAddresses: function() {
        var defer = $q.defer();
        $http.post('/outlook/getOutlookAddresses').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getOutlookMailThreads: function( data ) {
        var defer = $q.defer();
        $http.post('/outlook/getMailThreads', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getOutlookThreadDetails: function( data ) {
        var defer = $q.defer();
        $http.post('/outlook/getMailThreadDetails', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getOutlookMailAttachments: function( data ) {
        var defer = $q.defer();
        $http.post('/outlook/getMailAttachment', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      modifyOutlookThread: function( data ) {
        var defer = $q.defer();
        $http.post('/outlook/modifyThread', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      ourlookMailMoveToFolder: function( data ) {
        var defer = $q.defer();
        $http.post('/outlook/getMailMoveToFolder', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      loadOutlookBoxes: function(id) {
        var defer = $q.defer();
        $http.post('/outlook/getMailFolders/'+id).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

    }
  }

})();
