(function () {
  "use strict";

  angular
    .module("app.mail")
    .controller("MailController", MailController);

  /**
   * @example MailController - Mail Controller to Controll all Mail Data.
   * @param {Object|string} $scope - Store String/Object/Array Data.
   * @param {Object|string} $rootScope - Store String/Object/Array Data Globally.
   * @param {Object|string} $document - To work with browser Dom.
   * @param {Object|string} $mdDialog - To Call Angular Module Dialogue from Anywhere.
   * @param {Object|string} $mdMedia - Angular Material Service to Use Media Queries.
   * @param {Object|string} $mdSidenav - Angular Material Service to Control Side Navigation.
   * @param {Object|string} $state - AngularJS Service to Specified Location/Route Etc.
   * @param {Object|string} $filter - AngularJS Filter Service to Filter an Array/Object Data.
   * @param {Object|string} $sce - AngularJS Service to Make Trusted Html.
   * @param {Object|string} $timeout - AngularJS Service to Make Trusted Html.
   * @param {Object|Array|string} MailService - Mail Service to get data from backend.
   * @param {Object|string} $mdToast - Angular Material Service For Notification Alert.
   * @param {Object|string} Upload - File Upload Service.
   * @param {Object|string} $q - Qeery to Use deffer/resolve request.
   * @returns {Object|Array} - Can Return Object/Array/String/Integer/Void and other Mixed Data For specific function.
   */
  function MailController($scope, $rootScope, $document, $mdDialog, DashboardService, $mdMedia, Auth, $mdSidenav, $state, $filter, $sce, $timeout, MailService, $mdToast, Upload, $q) {
    var vm = this;

    // Array to Store all provider mail accounts
    vm.accounts = [];

    // Label Color Thread
    vm.colors = ["blue-bg", "blue-grey-bg", "orange-bg", "pink-bg", "purple-bg"];

    // To Store Selected Account only
    vm.selectedAccount = {};

    // To Store Selected Account ID
    vm.selectedAccountID = 0;

    // Loader Start
    vm.loadingThreads = true;

    // Load Gmail Function to Fetch all Gmail Mail on the fly
    vm.loadGmail = loadGmail;

    // Load Outlook Function to Fetch all Outlook Mail on the fly
    vm.loadOutlook = loadOutlook;

    // To store current folder/label item - First One for Indicate Type(Folder/Label), Second One For Name(Folder/Label)
    vm.currentFilter = {
      type: $state.params.type,
      filter: $state.params.filter
    };

    // Make Current Mail Item to Empty
    vm.currentThread = null;

    // Make Selected Mails to Empty
    vm.selectedThreads = [];

    // Mail Layout view - 1 for Classic view - 2 for Outlook view
    vm.views = {
      classic: "app/main/mail/views/classic/classic-view.html",
      outlook: "app/main/mail/views/outlook/outlook-view.html"
    };

    // Make Default to Outlook View
    vm.defaultView = "outlook";

    // Make Current View to Outlook
    vm.currentView = "outlook";

    // Set Null Authentication URL Initially
    vm.MailAuthUrl = null;

    // Function to Load Folder
    vm.loadFolder = loadFolder;
    vm.loadAccountFolder = loadAccountFolder;
    vm.isFolderActive = isFolderActive;
    vm.isLabelActive = isLabelActive;

    vm.openThread = openThread;
    vm.closeThread = closeThread;

    vm.isSelected = isSelected;
    vm.toggleSelectThread = toggleSelectThread;
    vm.selectThreads = selectThreads;
    vm.deselectThreads = deselectThreads;
    vm.toggleSelectThreads = toggleSelectThreads;

    vm.setThreadStatus = setThreadStatus;
    vm.toggleThreadStatus = toggleThreadStatus;
    vm.removeAccount = removeAccount;
    vm.removeAccountConfirm = removeAccountConfirm;

    vm.getLabel = getLabel;
    // vm.getLabelColor = getLabelColor;
    // vm.getLabelTitle = getLabelTitle;
    // vm.toggleLabel = toggleLabel;
    vm.isLabelExist = isLabelExist;

    vm.changeView = changeView;

    vm.composeDialog = composeDialog;
    vm.replyDialog = replyDialog;
    vm.forwardDialog = forwardDialog;

    vm.toggleSidenav = toggleSidenav;

    /*-----------------------------------
    * Gmail Functionality Init Here
    */
    vm.loadMailLabels = loadMailLabels;
    vm.refreshMessages = refreshMessages;
    vm.attachmentSize = attachmentSize;
    vm.downloadAttachment = downloadAttachment;
    vm.getGmailAttachmentData = getGmailAttachmentData;
    vm.totalAchmentCount = totalAchmentCount;
    vm.isStarredMail = isStarredMail;
    vm.isImportantMail = isImportantMail;
    vm.isReadMail = isReadMail;
    vm.changeThreadLabel = changeThreadLabel;
    vm.deleteThreadForever = deleteThreadForeverGmail;
    vm.totalThreadLabelsGMail = [];
    vm.mailCountByFolder = mailCountByFolder;
    vm.getLabelCountListGMail = [];
    vm.assignAsLabel = assignAsLabel;
    vm.loadAllProvider = loadAllProvider;
    vm.totalMailCount = totalMailCount;
    vm.currentMailCount = currentMailCount;
    vm.folderMailCount = 0;
    vm.allPreviousPagination = [];
    vm.previousPaginationCount = 1;
    vm.mailSearchTimoutInterval = mailSearchTimoutInterval;
    vm.mailSearchClearTimoutInterval = mailSearchClearTimoutInterval;
    vm.loadSubfolders = loadSubfolders;


    // For Pagination GMail
    vm.initMessagePositionCount = 0;
    vm.currentPageMessagesGMail = 1;
    vm.currentPageTokensGMail = [];
    vm.currentPageTokensGMail[vm.currentPageMessagesGMail] = "";
    vm.prevPagePaginationGMail = false;
    vm.loadNextMessage = loadNextMessage;
    vm.loadPreviousMessage = loadPreviousMessage;
    vm.addNewAccount = addNewAccount;
    vm.getMailAuthUrl = getMailAuthUrl;
    vm.getGMailAuthUrl = getGMailAuthUrl;
    vm.closeDialog = closeDialog;
    vm.printMail = printMail;
    vm.selectedRemoveAccount = null;



    init();

    /**
     * @example init - Call to Initiate All Provider Mails With Mail Count.
     */
    function init() {
      // Load All Provider Mails
      var accountId = $state.params.accountId && $state.params.accountId !=='0' ? $state.params.accountId : '';
      var threadId = vm.threadParamId = $state.params.threadId || '';
      loadAllProvider( accountId, threadId );
      $rootScope.searchable = false;
      $rootScope.getGlobalNotificationCount();

      $timeout(function () {
        checkEmailStates();
      });
    }

    // function loadExtarnalJavascript(){
    //   var jsTag;
    //   if(!isMyScriptLoaded('/app/core/directives/ng-draggable.js')){
    //     console.log('Not Include');
    //     jsTag = document.createElement("script");
    //     jsTag.src = "/app/core/directives/ng-draggable.js";
    //     document.getElementsByTagName("head")[0].appendChild(jsTag);
    //   }else{
    //     console.log('Included Already');
    //   }
    // }

    function isMyScriptLoaded(url) {
      var scripts = document.getElementsByTagName('script');
      for (var i = scripts.length; i--;) {
        if (scripts[i].src == url){
          return true;
        }
      }
      return false;
    }

    // Data Sorting by Specific Key Value
    if (typeof Object.defineProperty === 'function') {
      try {
        Object.defineProperty(Array.prototype, 'sortBy', {value: srtby, configurable: true});
      } catch (e) {
        console.log(e);
      }
    }
    if (!Array.prototype.sortBy) Array.prototype.sortBy = srtby;

    /**
     * @example srtby - Sort By.
     * @param {string} f - The Sorting Key value.
     * @returns {Array|Object} - Return as Array/Object.
     */
    function srtby(f) {
      var i;
      for (i = this.length; i;) {
        var o = this[--i];
        this[i] = [].concat(f.call(o, o, i), o);
      }
      this.sort(function (a, b) {
        for (var i = 0, len = a.length; i < len; ++i) {
          if (a[i] !== b[i]) return a[i] < b[i] ? 1 : -1;
        }
        return 0;
      });
      for (i = this.length; i;) {
        this[--i] = this[i][this[i].length - 1];
      }
      return this;
    }

    function checkEmailStates(){
      if(vm.accounts && vm.accounts.length){
        MailService.checkEmailStates().then(function (respEmails) {
          $rootScope.loadingProgress = false;
          // vm.currentThread = null;
          // vm.loadingThreads = true;
          // vm.loadingText = 'Loading emails..';
          if (vm.activeProvider === 'all') {
            loadAllProvider();
          } else if (vm.activeProvider === 'gmail') {
            loadGmail();
          } else if (vm.activeProvider === 'outlook') {
            loadOutlook();
          }
        });
      }
    }

    /**
     * @example loadAllProvider - Load All Provider Mails.
     * @returns {Array|Object} - Return Object/Array Data.
     */
    function loadAllProvider( accountId, threadId ) {

      vm.folderMailCount = 0;
      vm.activeProvider = accountId ? 'account' : "all";
      vm.newAccountInstruction = false;
      vm.currentThread = null;
      vm.allMailPaginationSkip = 0;

      vm.threads = null;
      vm.allMailPagination = null;
      vm.initMessagePositionCount = 0;
      vm.previousPaginationCount = 1;

      vm.currentFilter.type = "folder";
      vm.currentFilter.filter = "inbox";

      vm.loadingThreads = true;
      vm.loadingText = 'Loading emails..';

      // $state.go('app.mail.account.threads', {
      //   filter: vm.currentFilter.filter || 'inbox',
      //   accountId: 0
      // }, {notify: false});


      MailService.getMailAddresses().then(function (response) {
        if (response && response.length) {

          vm.folders = [
            {"id": "inbox", "name": "Inbox", "icon": "icon-inbox"},
            {"id": "sent", "name": "Sent", "icon": "icon-send"},
            {"id": "draft", "name": "Draft", "icon": "icon-email-open"},
            {"id": "spam", "name": "Spam", "icon": "icon-alert-octagon"},
            {"id": "trash", "name": "Trash", "icon": "icon-delete"}
          ];

          MailService.getMessages({}).then(function (messageData) {
            var threadlist;
            if (vm.threads && vm.threads.length) {
              threadlist = vm.threads.concat(messageData.messages);
              vm.threads = threadlist.sortBy(function (o) {
                return o.date;
              });
            } else {
              threadlist = messageData.messages;
              vm.threads = threadlist.sortBy(function (o) {
                return o.date;
              });
            }

            if (vm.allMailPagination && vm.allMailPagination.length) {
              vm.allMailPagination = vm.allMailPagination.concat(response.pagination);
            } else {
              vm.allMailPagination = response.pagination;
            }
            vm.loadingThreads = false;
            if( threadId ){
              MailService.getMessage({id: threadId}).then(function (response) {
                $rootScope.loadingProgress = false;
                openThread(response);
              });
            }else{
              $rootScope.loadingProgress = false;
            }

          });

          $rootScope.loadingProgress = true;

          vm.ThreadCountGMail = 0;
          vm.AllThreadCount = 0;
          vm.ThreadCountOutlook = 0;
          vm.accounts = $rootScope.mailAccounts = response;
          vm.selectedAccountID = accountId || 0;
          vm.selectedAccount = vm.accounts.find(function (acc){
            return acc.id === vm.selectedAccountID;
          });
          if(accountId){
            loadMailLabels(accountId, threadId);
          }
          refreshMailCounting();
        } else {
          vm.loadingThreads = false;
          vm.newAccountInstruction = true;
          vm.activeProvider = "";
          vm.folders = null;
        }
      });

    }

    function refreshMailCounting( data ){
      MailService.countAllUnreadMail( data || {} ).then(function (respCount) {
        vm.ThreadCountGMail = respCount.gmail;
        vm.ThreadCountOutlook = respCount.outlook;
        vm.AllThreadCount = respCount.gmail + respCount.outlook;
        if($rootScope.notificationCount){
          $rootScope.notificationCount.mail = vm.AllThreadCount;
        }
        if(vm.selectedAccountID && vm.selectedAccountID!= 0){
          loadAccountMailCounting(vm.selectedAccountID);
        }else{
          vm.folderMailCount = respCount.all;
        }
      });
    }

    function loadAccountMailCounting(id, provider){
      MailService.loadAccountMailCounting({id: id || '', provider: provider || ''}).then(function (respCount) {
        if( respCount && respCount.all ){
          vm.folderMailCount = respCount.all;
        }
      });
    }

    // response = vm.accounts || $rootScope.mailAccounts || response;
    // var gCount = 0, oCount = 0, aCount = 0;
    // response.forEach(function (val) {
    //   console.log(val);
    //   if (val && val.provider === "gmail") {
    //     MailService.GMailThreadCount({id: val.id}).then(function (respThreads) {
    //       gCount = gCount + respThreads.threadsUnread;
    //       aCount = aCount + respThreads.threadsUnread;
    //     });
    //     inboxMailCount("gmail", val.id, "inbox");
    //   } else if (val && val.provider === "outlook") {
    //     MailService.getOutlookUnreadMailCount({id: val.id}).then(function (respThreads) {
    //       oCount = oCount + respThreads.UnreadItemCount;
    //       aCount = aCount + respThreads.UnreadItemCount;
    //     });
    //     inboxMailCount("outlook", val.id, "inbox");
    //   }
    // });



    //setup before functions
    var mailSearchTypingTimer = null;                //timer identifier



    function mailSearchTimoutInterval() {
      $timeout.cancel( mailSearchTypingTimer );
      if ( !!vm.searchMail ) {
        mailSearchTypingTimer = $timeout(getSearchedMail, 1000);
      }
    }

    function mailSearchClearTimoutInterval() {
      $timeout.cancel( mailSearchTypingTimer );
    }

    vm.skipSearchItem = 0;
    function getSearchedMail() {
      console.log(vm.searchMail);
      MailService.findSearchedMail({text: vm.searchMail, skip: vm.skipSearchItem}).then(function (resp) {
        vm.threads = resp;
      });
    }

    // function loadAllMailCount() {
    //   MailService.getMailAddresses().then(function (response) {
    //     vm.ThreadCountGMail = 0;
    //     vm.AllThreadCount = 0;
    //     vm.ThreadCountOutlook = 0;
    //     if (response && response.length) {
    //       response.forEach(function (val) {
    //         if (val.provider === "gmail") {
    //           MailService.GMailThreadCount({id: val.id}).then(function (response) {
    //             vm.ThreadCountGMail = vm.ThreadCountGMail + response.threadsUnread;
    //             vm.AllThreadCount = vm.AllThreadCount + response.threadsUnread;
    //           });
    //         } else if (val.provider === "outlook") {
    //           MailService.getOutlookUnreadMailCount({id: val.id}).then(function (response) {
    //             var mailCount = response.UnreadItemCount;
    //             vm.ThreadCountOutlook = vm.ThreadCountOutlook + mailCount;
    //             vm.AllThreadCount = vm.AllThreadCount + mailCount;
    //           });
    //         }
    //       });
    //     }
    //   });
    // }

    function inboxMailCount(provider, mailid, folder) {
      if (provider === "gmail") {
        MailService.GMailThreadFolderCount({id: mailid, folder: folder.toUpperCase()}).then(function (response) {
          vm.folderMailCount = vm.folderMailCount + response.threadsTotal;
        });
      } else if (provider === "outlook") {
        MailService.getOutlookUnreadMailCount({
          id: mailid,
          Mailbox: capitalizeFirstLetter(folder)
        }).then(function (response) {
          if (response) {
            vm.folderMailCount = vm.folderMailCount + parseInt(response.TotalItemCount);
          }
        });
      }
    }


    function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function loadGmail() {

      vm.activeProvider = "gmail";
      vm.newAccountInstruction = false;
      vm.folderMailCount = 0;
      vm.currentThread = null;
      vm.allMailPaginationSkip = 0;

      vm.threads = null;
      vm.allMailPagination = null;
      vm.initMessagePositionCount = 0;
      vm.previousPaginationCount = 1;

      vm.currentFilter.type = "folder";
      vm.currentFilter.filter = "inbox";


      vm.folders = [
        {"id": "inbox", "name": "Inbox", "icon": "icon-inbox"},
        {"id": "sent", "name": "Sent", "icon": "icon-send"},
        {"id": "draft", "name": "Draft", "icon": "icon-email-open"},
        {"id": "spam", "name": "Spam", "icon": "icon-alert-octagon"},
        {"id": "trash", "name": "Trash", "icon": "icon-delete"}
      ];
      MailService.getMessages({provider: 'gmail'}).then(function (messageData) {
        if (messageData) {
          vm.threads = messageData.messages;
          if (vm.allMailPagination && vm.allMailPagination.length) {
            vm.allMailPagination = vm.allMailPagination.concat(messageData.pagination);
          } else {
            vm.allMailPagination = messageData.pagination;
          }
          vm.loadingThreads = false;
          $rootScope.loadingProgress = false;


        } else {
          vm.loadingThreads = false;
          vm.activeProvider = "";
          vm.folders = null;
        }
      });

      // MailService.getMailAddresses('gmail').then(function (response) {
      //
      // });
      loadAccountMailCounting('', 'gmail');
    }

    function loadOutlook() {
      vm.activeProvider = "outlook";
      vm.newAccountInstruction = false;
      vm.folderMailCount = 0;
      vm.allMailPaginationSkip = 0;

      vm.threads = null;
      vm.allMailPagination = null;
      vm.initMessagePositionCount = 0;
      vm.previousPaginationCount = 1;

      vm.currentFilter.type = "folder";
      vm.currentFilter.filter = "inbox";


      vm.folders = [
        {"id": "inbox", "name": "Inbox", "icon": "icon-inbox"},
        {"id": "sent", "name": "Sent", "icon": "icon-send"},
        {"id": "draft", "name": "Draft", "icon": "icon-email-open"},
        {"id": "spam", "name": "Spam", "icon": "icon-alert-octagon"},
        {"id": "trash", "name": "Trash", "icon": "icon-delete"}
      ];
      MailService.getMessages({provider: 'outlook'}).then(function (messageData) {
        if (messageData) {

          vm.threads = messageData.messages;

          if (vm.allMailPagination && vm.allMailPagination.length) {
            vm.allMailPagination = vm.allMailPagination.concat(messageData.pagination);
          } else {
            vm.allMailPagination = messageData.pagination;
          }
          vm.loadingThreads = false;
          $rootScope.loadingProgress = false;
        } else {
          vm.loadingThreads = false;
          vm.activeProvider = "";
          vm.folders = null;
        }
      });
      loadAccountMailCounting('', 'outlook');
      // MailService.getMailAddresses('outlook').then(function (response) {
      //   if (response && response.length) {
      //     response.forEach(function (val) {
      //       MailService.getOutlookUnreadMailCount({
      //         id: val.id,
      //         Mailbox: 'Inbox'
      //       }).then(function (response) {
      //         if (response) {
      //           vm.folderMailCount = vm.folderMailCount + parseInt(response.TotalItemCount);
      //         }
      //       });
      //     });
      //   }
      //
      // });
    }

    /**
     * @example totalMailCount - Count total Mail.
     * @returns {number} - Return count value.
     */
    function totalMailCount() {
      if (vm.activeProvider === "all") {
        return vm.AllThreadCount;
      } else if (vm.activeProvider === "gmail") {
        return vm.ThreadCountGMail;
      } else if (vm.activeProvider === "outlook") {
        return vm.ThreadCountOutlook;
      } else if (vm.activeProvider === "account") {
        return 1;
      }
    }

    /**
     * @example currentMailCount - Count Current Mail.
     * @returns {number} - Return count value.
     */
    function currentMailCount() {
      var mailCount = 0;
      if (vm.activeProvider === "all") {
        mailCount = 30;
      } else if (vm.activeProvider === "gmail") {
        vm.accounts.forEach(function (value) {
          if (value.provider === "gmail") {
            mailCount = mailCount + 30;
          }
        });
      } else if (vm.activeProvider === "outlook") {
        vm.accounts.forEach(function (value) {
          if (value.provider === "outlook") {
            mailCount = mailCount + 30;
          }
        });
      } else if (vm.activeProvider === "account") {
        mailCount = mailCount + 30;
      }
      return mailCount;
    }




    $scope.$watch(function () {
      return $mdMedia("xs");
    }, function (current, old) {
      if (angular.equals(current, old)) {
        return;
      }

      if (current) {
        vm.currentView = "classic";
      }
    });

    $scope.$watch(function () {
      return $mdMedia("gt-xs");
    }, function (current, old) {
      if (angular.equals(current, old)) {
        return;
      }

      if (current) {
        if (vm.defaultView === "outlook") {
          vm.currentView = "outlook";
        }
      }
    });

    /**
     * Load folder
     *
     * @param name
     * @param labelType
     */
    function loadFolder(name, labelType) {

      var provider = vm.activeProvider || '';
      var vendor = vm.selectedAccount ? vm.selectedAccount.provider : '';

      // console.log(vm.selectedAccount.provider);
      // console.log(vm.activeProvider);

      vm.currentFilter.type = labelType ? "label":"folder";
      vm.folderMailCount = 0;
      vm.initMessagePositionCount = 0;
      vm.currentFilter.filter = name;


      $rootScope.loadingProgress = true;

      vm.threads = null;

      vm.allMailPagination = null;

      vm.loadingThreads = true;


      if (provider === "all") {
        if (typeof(name) === "undefined") {
          name = "inbox";
        }

        $state.go('app.mail.account.threads', {
          filter: vm.currentFilter.filter,
          accountId: 0
        }, {notify: false});

        if ( name === 'inbox' ) {
          loadAllProvider();
          return;
        }

        MailService.getMailAddresses().then(function (response) {
          response.forEach(function (val) {
            var data;
            if (val.provider === "gmail") {
              data = {};
              data.id = val.id;
              if( !labelType ){
                data.folder = name.toUpperCase();
              }else{
                data.folder = name;
              }
              MailService.getGmailMessages(data).then(function (response) {
                if (vm.threads && vm.threads.length) {
                  var threadlist = vm.threads.concat(response.messages);
                  vm.threads = threadlist.sortBy(function (o) {
                    return o.date;
                  });
                } else {
                  vm.threads = response.messages;
                }
                if (vm.allMailPagination && vm.allMailPagination.length) {
                  vm.allMailPagination = vm.allMailPagination.concat(response.pagination);
                } else {
                  vm.allMailPagination = response.pagination;
                }
                vm.loadingThreads = false;
                $rootScope.loadingProgress = false;
              });
              inboxMailCount("gmail", val.id, name);


            } else if (val.provider === "outlook") {
              data = {};
              data.id = val.id;
              data.Mailbox = toCapitalizeOutlook(name);
              MailService.getOutlookMailThreads(data).then(function (response) {
                if (vm.threads && vm.threads.length) {
                  var threadlist = vm.threads.concat(response.messages);
                  vm.threads = threadlist.sortBy(function (o) {
                    return o.date;
                  });
                } else {
                  vm.threads = response.messages;
                }
                if (vm.allMailPagination && vm.allMailPagination.length) {
                  vm.allMailPagination = vm.allMailPagination.concat(response.pagination);
                } else {
                  vm.allMailPagination = response.pagination;
                }
                vm.loadingThreads = false;
                $rootScope.loadingProgress = false;
              });
              inboxMailCount("outlook", val.id, name);
            }
          });
        });

      } else if (vendor === 'gmail' || provider === "gmail") {

        $state.go('app.mail.account.threads', {
          filter: vm.currentFilter.filter || 'inbox',
          accountId: vm.selectedAccountID || vm.selectedAccount.id || 0
        }, {notify: false});

        if (typeof(name) === "undefined" || name === "inbox" || name === "INBOX") {
          loadGmail();
        } else {
          MailService.getMailAddresses().then(function (response) {
            response.forEach(function (val) {
              if (val.provider === "gmail") {
                var data = {};
                data.id = val.id;
                if( !labelType ){
                  data.folder = name.toUpperCase();
                }else{
                  data.folder = name;
                }
                console.log(data);
                MailService.getGmailMessages(data).then(function (response) {
                  if (vm.threads && vm.threads.length) {
                    vm.threads = vm.threads.concat(response.messages);
                  } else {
                    vm.threads = response.messages;
                  }
                  if (vm.allMailPagination && vm.allMailPagination.length) {
                    vm.allMailPagination = vm.allMailPagination.concat(response.pagination);
                  } else {
                    vm.allMailPagination = response.pagination;
                  }
                  vm.loadingThreads = false;
                  $rootScope.loadingProgress = false;
                  inboxMailCount("gmail", val.id, name);
                });
              }
            });
          });
        }

      } else if ( vendor === 'outlook' || provider === "outlook" ) {

        $state.go('app.mail.account.threads', {
          filter: vm.currentFilter.filter || 'Inbox',
          accountId: vm.selectedAccountID || vm.selectedAccount.id || 0
        }, {notify: false});

        if (typeof(name) === "undefined" || name === "inbox" || name === "Inbox") {
          loadOutlook();
        } else {
          MailService.getMailAddresses().then(function (response) {
            response.forEach(function (val) {
              var data = {};
              data.id = val.id;
              data.Mailbox = toCapitalizeOutlook(name);
              if (val.provider === "outlook") {
                MailService.getOutlookMailThreads(data).then(function (response) {
                  if (vm.threads && vm.threads.length) {
                    vm.threads = vm.threads.concat(response.messages);
                  } else {
                    vm.threads = response.messages;
                  }
                  if (vm.allMailPagination && vm.allMailPagination.length) {
                    vm.allMailPagination = vm.allMailPagination.concat(response.pagination);
                  } else {
                    vm.allMailPagination = response.pagination;
                  }
                  vm.loadingThreads = false;
                  $rootScope.loadingProgress = false;
                  inboxMailCount("outlook", val.id, name);
                });
              }

            });
          });
        }

      }
    }

    function removeAccount() {
      vm.selectedRemoveAccount = null;
      $mdDialog.show({
        controller: function () {
          return vm;
        },
        controllerAs: "vm",
        templateUrl: "app/main/mail/dialogs/remove/confirm.html",
        clickOutsideToClose: false
      });
    }

    function removeAccountConfirm() {
      MailService.deleteCurrentAccount({email: vm.selectedRemoveAccount}).then(function (response) {
        $mdDialog.hide();
        $timeout(function () {
          vm.accounts = $filter('filter')(vm.accounts, {email: !vm.selectedRemoveAccount});
          refreshMailCounting();
          loadAllProvider();
          showMessage("The Account has been removed");
          checkEmailStates();
        }, 10);
      });
    }


    function pushToThreadData(data) {
      vm.threads.push(data);
      vm.threads = vm.threads.sortBy(function (o) {
        return o.date;
      });
    }


    io.socket.on('new_email', function (data) {
      showMessage("You have a new message.");
      $rootScope.getGlobalNotificationCount();
      pushToThreadData(data);
    });


    // io.socket.on('refresh_for_new_mail', function onServerSentEvent(data) {
    //   // pushToThreadData(data);
    //   loadAllProvider();
    //   showMessage("You have a new message.");
    // });

    function showMessage(message) {
      $mdToast.show({
        template: '<md-toast class="md-toast success">' + message + '</md-toast>',
        hideDelay: 3000,
        position: 'bottom left'
      });
    }


    function loadAccountFolder(name) {
      vm.currentFilter.type = 'folder';

      vm.currentFilter.filter = name;
      $rootScope.loadingProgress = true;

      vm.folderMailCount = 0;
      vm.initMessagePositionCount = 0;

      vm.threads = null;
      vm.allMailPagination = null;

      vm.loadingThreads = true;
      vm.loadingText = 'Loading emails..';

      if (name === "inbox" || name === "INBOX" || name === "Inbox") {
        MailService.getMessages({email_id: vm.selectedAccount.id, maxResults: 30}).then(function (response) {
          vm.threads = response.messages;
          vm.loadingThreads = false;
          vm.allMailPagination = response.pagination;
          $rootScope.loadingProgress = false;
          // inboxMailCount('gmail', id, 'inbox');
        });
      } else {
        var data = {};
        if (vm.selectedAccount.provider === 'gmail') {
          data.id = vm.selectedAccount.id;
          data.folder = name.toUpperCase();
          data.maxResults = 30;
          MailService.getGmailMessages(data).then(function (response) {
            vm.threads = response.messages;
            vm.allMailPagination = response.pagination;
            vm.loadingThreads = false;
            $rootScope.loadingProgress = false;
            inboxMailCount('gmail', vm.selectedAccount.id, name);
          });
        }else if (vm.selectedAccount.provider === 'outlook') {
          data.id = vm.selectedAccount.id;
          data.folder = name;
          data.max = 30;
          MailService.getOutlookMessages(data).then(function (response) {
            vm.threads = response.messages;
            vm.allMailPagination = response.pagination;
            vm.loadingThreads = false;
            $rootScope.loadingProgress = false;
            inboxMailCount('outlook', vm.selectedAccount.id, name);
          });
        }
      }
    }

    vm.getChangedAccount = function () {
      console.log(vm.selectedRemoveAccount);
    }


    function addNewAccount() {
      vm.selectedAddAccount = null;
      $mdDialog.show({
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/mail/dialogs/add/account.html',
        clickOutsideToClose: false
      });
      Auth.lastUrl({module: 'mail'}).then(function (respUrl) {
        console.log(respUrl);
      });
    }

    function getMailAuthUrl() {
      if (vm.selectedAddAccount === 'gmail') {
        MailService.GMailAuthUrl().then(function (response) {
          if (response) {
            vm.MailAuthUrl = response;
          }
        });
      } else if (vm.selectedAddAccount === 'outlook') {
        MailService.OutlookAuthUrl().then(function (response) {
          if (response) {
            vm.MailAuthUrl = response;
          }
        });
      }
    }

    function getGMailAuthUrl() {
      MailService.GMailAuthUrl().then(function (response) {
        if (response) {
          vm.GMailAuthUrl = response;
        }
      });
    }


    function closeDialog() {
      $mdDialog.hide();
    }


    function loadMailLabels( id, threadId ) {

      vm.loadingThreads = true;
      vm.loadingText = 'Loading emails..';
      vm.currentThread = null;

      vm.threads = null;
      vm.allMailPagination = null;
      vm.currentFilter.type = 'folder';
      vm.currentFilter.filter = 'inbox';
      vm.folderMailCount = 0;


      if (typeof id === "undefined") {
        vm.selectedAccount = vm.accounts.find(function (acc){
          return acc.id === vm.selectedAccountID;
        });
        if( typeof vm.selectedAccount === "undefined" ){
          vm.activeProvider = 'all';
          loadAllProvider( null, threadId );
          goToState();
          return;
        }
        id = vm.selectedAccount.id;
        // console.log(id);
      }

      goToState(id, vm.currentFilter.filter, threadId);

      vm.activeProvider = 'account';


      loadMailProvider( id, vm.selectedAccount.provider );

      if( threadId ){
        $rootScope.loadingProgress = true;
        MailService.getMessage({id: threadId}).then(function (response) {
          $rootScope.loadingProgress = false;
          openThread(response);
        });
      }

    }

    function loadSubfolders( id ){
      if(typeof vm.expendSubfolder === 'object' && vm.expendSubfolder[id]){
        vm.expendSubfolder[id] = false;
      }else {
        vm.expendSubfolder = {};
        MailService.loadSubFolders({folder: id, account: vm.selectedAccount}).then(function (response) {
          vm.expendSubfolder[id] = true;
          vm.subFolders = response;
          console.log(response);
        });
      }
    }

    function loadMailProvider( id, provider ){
      $rootScope.loadingProgress = true;
      var data = {};
      if( provider === 'gmail' ){
        loadGmailLabels(id);
        data.email_id = id;
        data.provider = 'gmail';
      }else if( provider === 'outlook' ){
        loadOutlookLabels(id);
        data.email_id = id;
        data.top = 30;
        data.provider = 'outlook';
      }
      loadAccountMailCounting(id);
      MailService.getMessages(data).then(function (response) {
        vm.threads = response.messages;
        vm.loadingThreads = false;
        vm.allMailPagination = response.pagination;
        $rootScope.loadingProgress = false;
      });
    }

    function goToState( accountId, name, threadId ) {
      var states = threadId ? 'app.mail.account.threads.thread' : 'app.mail.account.threads';
      $state.go(states, {
        type: vm.currentFilter.type || 'label',
        filter: name || 'inbox',
        accountId: accountId || 0,
        threadId: threadId
      }, {notify: false});
    }

    function refreshMessages() {
      vm.threads = null;
      $rootScope.loadingProgress = true;
      vm.currentThread = null;
      vm.loadingThreads = true;
      vm.loadingText = 'Loading emails..';
      checkEmailStates();
      // if (vm.activeProvider === 'all') {
      //   loadAllProvider();
      // } else if (vm.activeProvider === 'gmail') {
      //   loadGmail();
      // } else if (vm.activeProvider === 'outlook') {
      //   loadOutlook();
      // }
    }


    /**
     * Load Gmail message by folder/label id
     */
    function loadGmailMessages(folder, id, useCache, nextPageToken, maxResults) {

      console.log('Got message from line 965.');
      if (typeof folder === 'undefined') {
        folder = vm.currentFilter.filter;
      }
      if (typeof id === 'undefined') {
        id = vm.selectedAccount.id;
      }
      vm.currentThread = null;
      var data = {};
      data.folder = folder;
      data.id = id;
      if (useCache) {
        data.withOutCache = useCache;
      }
      if (nextPageToken) {
        data.nextPageToken = nextPageToken;
      }
      if (maxResults) {
        data.maxResults = maxResults;
      }
      MailService.loadGMailThreads(data).then(function (response) {
        var emails = [];
        if (response && response.threads) {
          response.threads.forEach(function (val) {
            // getGMailHeaderValueByName('Subject', val.metadata[0]);
            var email = {};
            email.from = {};
            email.to = {};
            email.id = val.id;
            var fromName = getGMailHeaderValueByName('From', val.metadata[0]);
            if (fromName && fromName.indexOf('<') > -1) {
              email.from.name = fromName.split(" <")[0].replace(/[^a-zA-Z ]/g, "");
              email.from.email = fromName.split(" <")[1].replace(">", "");
            } else {
              email.from.email = fromName;
            }

            email.to.email = getGMailHeaderValueByName('To', val.metadata[0]);
            email.to.name = "me";
            email.message = val.snippet;
            email.subject = getGMailHeaderValueByName('Subject', val.metadata[0]);
            var getDate = getGMailHeaderValueByName('Date', val.metadata[0]);
            email.time = moment(new Date(getDate)).format('ddd, MMMM Do YYYY [at] HH:mm a');
            email.hasAttachments = isHasAttachmentsGmail(val);
            email.read = isReadGMail(val);
            email.starred = isStarredGMail(val);
            email.important = isImportantGMail(val);
            email.labels = val.metadata[0].labelIds;
            emails.push(email);
          });
          vm.threads = emails;
          vm.currentPageTokensGMail[vm.currentPageMessagesGMail] = response.nextPageToken;
        } else {
          vm.threads = [];
        }
        $rootScope.loadingProgress = false;
        vm.selectedThreads = [];
        getThreadLabelListCountGMail();
      });
    }

    function getNextMessages(skip, provider) {
      var data = {};
      if (skip) {
        data.skip = skip;
      }
      if (provider) {
        data.provider = provider;
      }
      MailService.getNextMessages(data).then(function (response) {
        if (response) {
          vm.threads = response.messages;
          console.log(response);
          vm.loadingThreads = false;
          $rootScope.loadingProgress = false;
        }
      });
    }

    function loadNextMessage() {
      vm.allMailPaginationSkip = vm.allMailPaginationSkip + 30;
      if (vm.activeProvider === 'all') {
        vm.threads = null;
        vm.currentThread = null;
        vm.loadingThreads = true;
        $rootScope.loadingProgress = true;

        getNextMessages(vm.allMailPaginationSkip);
        vm.initMessagePositionCount = vm.initMessagePositionCount + vm.currentMailCount();
        vm.previousPaginationCount++;

      } else if (vm.activeProvider === 'gmail') {
        vm.threads = null;
        vm.loadingThreads = true;
        $rootScope.loadingProgress = true;

        getNextMessages(vm.allMailPaginationSkip, 'gmail');
        vm.initMessagePositionCount = vm.initMessagePositionCount + vm.currentMailCount();
        vm.previousPaginationCount++;
      } else if (vm.activeProvider === 'outlook') {
        vm.threads = null;
        vm.loadingThreads = true;
        $rootScope.loadingProgress = true;

        getNextMessages(vm.allMailPaginationSkip, 'outlook');
        vm.initMessagePositionCount = vm.initMessagePositionCount + vm.currentMailCount();
        vm.previousPaginationCount++;
      } else if (vm.activeProvider === 'account') {

        getNextMessages(vm.allMailPaginationSkip, vm.selectedAccount.provider);
        vm.initMessagePositionCount = vm.initMessagePositionCount + vm.currentMailCount();
        vm.previousPaginationCount++;

      }
    }

    function loadPreviousMessage() {
      vm.threads = null;
      vm.loadingThreads = true;
      $rootScope.loadingProgress = true;
      vm.allMailPaginationSkip = vm.allMailPaginationSkip - 30;
      if (vm.activeProvider === 'all') {

        getNextMessages(vm.allMailPaginationSkip);
        vm.initMessagePositionCount = vm.initMessagePositionCount - vm.currentMailCount();
        vm.previousPaginationCount--;
      } else if (vm.activeProvider === 'gmail') {

        getNextMessages(vm.allMailPaginationSkip, 'gmail');
        vm.initMessagePositionCount = vm.initMessagePositionCount - vm.currentMailCount();
        vm.previousPaginationCount--;
      } else if (vm.activeProvider === 'outlook') {

        getNextMessages(vm.allMailPaginationSkip, 'outlook');
        vm.initMessagePositionCount = vm.initMessagePositionCount - vm.currentMailCount();
        vm.previousPaginationCount--;
      } else if (vm.activeProvider === "account") {

        getNextMessages(vm.allMailPaginationSkip, vm.selectedAccount.provider);
        vm.initMessagePositionCount = vm.initMessagePositionCount - vm.currentMailCount();
        vm.previousPaginationCount++;
      }
    }

    /**
     * Load Gmail Labels
     * @param id
     */
    function loadGmailLabels(id) {
      if (typeof id === "undefined") {
        id = vm.selectedAccount.id;
      }
      vm.folders = [];
      vm.labels = [];
      MailService.loadGMailBoxes(id).then(function (response) {
        response.forEach(function (val) {
          vm.totalThreadLabelsGMail.push(val.id);
          if (val.type === 'system') {
            var folder = val;
            if (val.name === 'INBOX') {
              folder.icon = 'icon-inbox';
              vm.currentFilter.filter = val.name;
            }
            else if (val.name === 'STARRED') {
              folder.icon = 'icon-star';
            }
            else if (val.name === 'SENT') {
              folder.icon = 'icon-send';
            }
            else if (val.name === 'DRAFT') {
              folder.icon = 'icon-email-open';
            }
            else if (val.name === 'SPAM') {
              folder.icon = 'icon-alert-octagon';
            }
            else if (val.name === 'TRASH') {
              folder.icon = 'icon-delete';
            }
            else if (val.name === 'IMPORTANT') {
              folder.icon = 'icon-label';
            }
            else {
              folder.icon = 'icon-label';
            }
            vm.folders.push(folder);
          } else if (val.type === 'user') {
            var label = val;
            vm.labels.push(label);
          }
        });
      });
    }

    function loadOutlookLabels(id) {
      if (typeof id == "undefined") {
        id = vm.selectedAccount.id;
      }
      MailService.loadOutlookBoxes(id).then(function (response) {
        vm.folders = [];
        vm.labels = [];
        response.value.forEach(function (val) {
          vm.totalThreadLabelsGMail.push(val.id);
          var folder = {};
          folder.id = val.Id;
          folder.name = val.DisplayName;
          if (val.DisplayName === 'Inbox') {
            folder.icon = 'icon-inbox';
            vm.currentFilter.filter = val.Id;
          }
          else if (val.DisplayName === 'Sent Items') {
            folder.icon = 'icon-send';
          }
          else if (val.DisplayName === 'Drafts') {
            folder.icon = 'icon-email-open';
          }
          else if (val.DisplayName === 'Junk Email') {
            folder.icon = 'icon-alert-octagon';
          }
          else if (val.DisplayName === 'Deleted Items') {
            folder.icon = 'icon-delete';
          }
          else {
            folder.icon = 'icon-label';
          }
          folder.unread = val.UnreadItemCount;
          folder.total = val.TotalItemCount;
          folder.child = val.ChildFolderCount;
          folder.type = 'system';
          vm.folders.push(folder);
        });
      });
    }

    function assignAsLabel(label) {
      if (vm.selectedThreads && vm.selectedThreads.length) {
        changeThreadLabel(label, 'add');
      }
      else if (vm.currentThread) {
        if (isFolderExistByID(label)) {
          changeThreadLabel(label, 'remove');
        } else {
          changeThreadLabel(label, 'remove');
        }
      }
      return true;
    }


    function getThreadLabelListCountGMail() {
      MailService.getThreadCountGMail(vm.selectedAccount.id, vm.totalThreadLabelsGMail).then(function (response) {
        vm.getLabelCountListGMail = response;
      });
    }

    function findAttachment( response ){
      var result = {mailAttachmentImages: [], mailAttachmentDocs: [], mailAttachmentPDFs: [], mailAttachmentZIPs: []};
      var parts = [response.payload];

      while (parts.length) {
        var part = parts.shift();
        if (part.parts) {
          parts = parts.concat(part.parts);
        }
        if (part.mimeType === "image/jpeg" || part.mimeType === "image/png" || part.mimeType === "image/gif") {
          vm.currentThread.attachments.images.push({id: part.body.attachmentId, size: part.body.size || ''});
        } else if (part.mimeType === "application/zip" || part.mimeType === "application/x-zip-compressed") {
          result.mailAttachmentZIPs.push({id: part.body.attachmentId, size: part.body.size || ''});
        } else if (part.mimeType === "application/pdf") {
          result.mailAttachmentPDFs.push({id: part.body.attachmentId, size: part.body.size || ''});
        } else if (part.mimeType === "application/msword" || part.mimeType === "plication/vnd.openxmlformats-officedocument.wordprocessingml.document" || part.mimeType === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
          result.mailAttachmentDocs.push({id: part.body.attachmentId, size: part.body.size || ''});
        }
      }
      return result;
    }

    function loadGMailThread(GMailID, thread) {
      vm.currentThread = thread;
      vm.currentThread.messages = [];
      MailService.loadGMailThread(GMailID, thread.mail_id).then(function (response) {
        response.messages.forEach(function (message) {
          vm.currentThread.messages.push($sce.trustAsHtml(message));
        });

        if( response.attachments ){
          vm.currentThread.attachments = response.attachments;
        }
        $rootScope.loadingProgress = false;
          if(!response.read || !thread.read){
            vm.currentThread.read = true;
            MailService.modifyThreadFromSochara({
              mail_id: vm.currentThread.mail_id || thread.mail_id,
              remove: 'unread'
            }).then(function (resp) {
              //{mail_id: value.mail_id, add: add, remove: remove}
              MailService.modifyThreadGMail(vm.currentThread.email_id || thread.email_id, vm.currentThread.mail_id || thread.mail_id, 'no', 'UNREAD').then(function (respModify) {
                if (!response.read) {
                  vm.ThreadCountGMail = vm.ThreadCountGMail ? vm.ThreadCountGMail - 1 : vm.ThreadCountGMail;
                  vm.AllThreadCount = vm.AllThreadCount ? vm.AllThreadCount - 1 : vm.AllThreadCount;
                  $rootScope.notificationCount.mail = vm.AllThreadCount;
                  $rootScope.getGlobalNotificationCount();
                }
              });
            });
          }

      });
    }

    function loadOutlookThread(MailID, thread) {
      vm.currentThread = thread;
      var threadID = thread.mail_id;
      vm.currentThread.messages = [];

      // For Attachment Data
      vm.currentThread.attachments = {};
      var mailAttachmentImages = [];
      var mailAttachmentZIPs = [];
      var mailAttachmentPDFs = [];
      var mailAttachmentDocs = [];

      MailService.getOutlookThreadDetails({id: MailID, threadID: threadID}).then(function (response) {
        if (response.Body.Content && response.Body.Content) {
          var messageData = getOutlookBodyData(response.Body.Content);
          vm.currentThread.messages.push(messageData);

          if (thread.hasAttachments) {
            MailService.getOutlookMailAttachments({id: MailID, threadID: threadID}).then(function (response) {
              if (response && response.value) {
                if (response.value.length) {
                  response.value.forEach(function (val) {
                    var attachment = {};
                    attachment.id = val.Id;
                    attachment.filename = val.Name;
                    attachment.mimeType = val.ContentType;
                    attachment.size = val.Size;
                    attachment.preview = "data:" + val.ContentType + ";base64," + val.ContentBytes;
                    if (val.ContentType === "image/jpeg" || val.ContentType === "image/png" || val.ContentType === "image/gif") {
                      mailAttachmentImages.push(attachment);
                    } else if (val.ContentType === "application/x-zip-compressed") {
                      mailAttachmentZIPs.push(attachment);
                    } else if (val.ContentType === "application/pdf") {
                      mailAttachmentPDFs.push(attachment);
                    } else if (val.ContentType === "application/msword" || val.ContentType === "plication/vnd.openxmlformats-officedocument.wordprocessingml.document" || val.ContentType === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                      mailAttachmentDocs.push(attachment);
                    }
                  });
                }
              }
            });

            vm.currentThread.attachments.images = mailAttachmentImages;
            vm.currentThread.attachments.pdfs = mailAttachmentPDFs;
            vm.currentThread.attachments.docs = mailAttachmentDocs;
            vm.currentThread.attachments.zips = mailAttachmentZIPs;

          }
          $rootScope.loadingProgress = false;

          // refreshMailCounting();


          if(threadID && vm.threads.getById(threadID)){
            vm.threads.getById(threadID).read = true;
          }

          if(!response.IsRead ||  !thread.read){
            if( !thread.read ){
              vm.currentThread.read = true;
              $rootScope.getGlobalNotificationCount();
            }
            if(!response.IsRead){
              MailService.modifyOutlookThread({
                id: MailID,
                idStr: threadID,
                updateData: {"IsRead": true}
              }).then(function (response) {
                if (response) {
                  $rootScope.getGlobalNotificationCount();
                }
              });
            }
          }


        }
      });
    }

    // /**------------------------------------ Used For Gmail Message Header Data ----------------------------------------*/

    function attachmentSize(size) {
      var fsize = parseFloat(parseInt(size) / 1024).toFixed(2);
      var rData = fsize + ' KB';
      return rData;
    }


    function changeThreadLabel(label, type, label_2) {
      // console.log(vm.selectedThreads);
      if (type === 'add') {
        changeThreadLabelFun(label, '');
      } else if (type === 'remove') {
        changeThreadLabelFun('no', label);
      } else if (type === 'add_remove') {
        changeThreadLabelFun(label, label_2);
      }
    }


    function changeThreadLabelFun(add, remove, thread) {
      thread = thread || vm.currentThread;
      var message = "";
      if (add === "important" || add === "spam" || add === "starred" || add === "unread") {
        message = "The message has been marked as " + add + ".";
      } else if (remove === "important" || remove === "spam" || remove === "starred" || remove === "unread" || remove === "inbox" || remove === "trash") {
        if (remove === "inbox" || remove === "important" || remove === "unread" || remove === "starred") {
          message = "The message has been removed from " + remove + ".";
        } else if (remove === "trash") {
          message = "The message has been moved to " + remove + ".";
        } else {
          message = "The message has been marked as not " + remove + ".";
        }
      } else {
        message = "The message has been assigned as " + add + ".";
      }
      $rootScope.loadingProgress = true;

      if (vm.selectedThreads && vm.selectedThreads.length) {
        var doneModifyData = 0;
        vm.selectedThreads.forEach(function (value) {
          if (value.provider === 'gmail') {
            if (add && add.length) {
              add = add.toUpperCase();
            }
            if (remove && remove.length) {
              remove = remove.toUpperCase();
            }
            MailService.modifyThreadGMail(value.email_id, value.mail_id, add, remove).then(function (response) {
              if (response) {
                MailService.modifyThreadFromSochara({
                  mail_id: value.mail_id,
                  add: add,
                  remove: remove
                }).then(function (response) {
                  if (response) {
                    if (add === "UNREAD") {
                      vm.currentThread = null;
                    }
                    if(add === "UNREAD" || remove === "UNREAD"){
                      $rootScope.getGlobalNotificationCount();
                    }
                  }
                });
              }

            });
          } else if (value.provider === 'outlook') {
            if (add && add.length) {
              add = toCapitalizeOutlook(add);
            }
            if (remove && remove.length) {
              remove = toCapitalizeOutlook(remove);
            }
            var data = {};
            data.id = value.email_id;
            data.threadID = value.mail_id;
            data.add = add;
            data.remove = remove;
            MailService.ourlookMailMoveToFolder(data).then(function (response) {
              if (response) {
                deselectThreads();
                MailService.modifyThreadFromSochara({
                  mail_id: value.mail_id,
                  add: add,
                  remove: remove
                }).then(function (response) {
                  $rootScope.loadingProgress = true;
                  if (add === "Unread") {
                    vm.currentThread = null;
                  }
                  if(add === "Unread" || remove === "Unread"){
                    $rootScope.getGlobalNotificationCount();
                  }
                });
              }
            });
          }
          doneModifyData += 1;
        });
        message = message.replace("message has", "messages have");
        $mdToast.show({
          template: '<md-toast class="md-toast success">' + message + '</md-toast>',
          hideDelay: 3000,
          position: 'bottom left'
        });
        vm.currentThread = null;
        if (doneModifyData === vm.selectedThreads.length) {
          refreshMailCounting();
          if (vm.activeProvider === 'all') {
            // loadAllProvider();
            $timeout(loadAllProvider, 3000);
          } else if (vm.activeProvider === 'gmail') {
            // loadGmail();
            $timeout(loadGmail, 3000);
          } else if (vm.activeProvider === 'outlook') {
            // loadOutlook();
            $timeout(loadOutlook, 3000);
          }
          deselectThreads();
        }
        // vm.selectedThreads = [];

      } else if (thread) {
        if (thread.provider === 'gmail') {
          if (add && add.length) {
            add = add.toUpperCase();
          }
          if (remove && remove.length) {
            remove = remove.toUpperCase();
          }
          MailService.modifyThreadGMail(thread.email_id, thread.mail_id, add, remove).then(function (response) {
            // if (add === "UNREAD") {

            // }
            if (response) {
              deselectThreads();
              MailService.modifyThreadFromSochara({
                mail_id: thread.mail_id,
                add: add,
                remove: remove
              }).then(function (response) {
                if (response) {
                  if (vm.activeProvider === 'all') {
                    loadAllProvider();
                  } else if (vm.activeProvider === 'gmail') {
                    loadGmail();
                  }
                  vm.currentThread = null;
                  if(add === "UNREAD" || remove === "UNREAD"){
                    $rootScope.getGlobalNotificationCount();
                  }
                }
              });

              $mdToast.show({
                template: '<md-toast class="md-toast success">' + message + '</md-toast>',
                hideDelay: 3000,
                position: 'bottom left'
              });
            }


          });
        } else if (thread.provider === 'outlook') {
          if (add && add.length) {
            add = toCapitalizeOutlook(add);
          }
          if (remove && remove.length) {
            remove = toCapitalizeOutlook(remove);
          }
          var data = {};
          data.id = thread.email_id;
          data.threadID = thread.mail_id;
          data.add = add;
          data.remove = remove;
          MailService.ourlookMailMoveToFolder(data).then(function (response) {
            if (response) {
              MailService.modifyThreadFromSochara({
                mail_id: thread.mail_id,
                add: add,
                remove: remove
              }).then(function (response) {
                if (response) {
                  if (vm.activeProvider === 'all') {
                    loadAllProvider();
                  } else if (vm.activeProvider === 'outlook') {
                    loadOutlook();
                  }
                  if (add === 'Unread') {
                    vm.currentThread = null;
                  }
                  if(add === "Unread" || remove === "Unread"){
                    $rootScope.getGlobalNotificationCount();
                  }
                }
              });

              $mdToast.show({
                template: '<md-toast class="md-toast success">' + message + '</md-toast>',
                hideDelay: 3000,
                position: 'bottom left'
              });
            }


          });
        }
      }
    }

    function toCapitalizeOutlook(str) {
      var capStr = str.charAt(0).toUpperCase() + str.substr(1).toLowerCase();
      var returnStr;
      if (capStr === 'Sent') {
        returnStr = 'SentItems';
      } else if (capStr === 'Trash') {
        returnStr = 'DeletedItems';
      } else if (capStr === 'Draft') {
        returnStr = 'Drafts';
      } else if (capStr === 'Spam') {
        returnStr = 'JunkEmail';
      } else {
        returnStr = capStr;
      }
      return returnStr;
    }

    function mailCountByFolder(label) {
      if (vm.activeProvider === 'gmail') {
        mailCountByFolderGMail(label);
      }
    }

    function mailCountByFolderGMail(label) {
      if (typeof label === 'undefined') {
        label = vm.currentFilter.filter;
      }
      var total = 0;
      if (vm.getLabelCountListGMail && vm.getLabelCountListGMail.length) {
        vm.getLabelCountListGMail.forEach(function (value) {
          if (value.id === label) {
            total = value.total;
          }
        });
      }
      return total;
    }

    // function deleteThreadForever() {
    //   if (vm.activeProvider === 'gmail' || vm.selectedAccount.provider === 'gmail') {
    //     deleteThreadForeverGmail();
    //   }
    // }

    function deleteThreadForeverGmail() {
      var message = "";
      message = "The message has been deleted Forever";
      vm.loadingText = "Please wait while we delete your selected emails.";

      var selectedGmailThreads = [];
      if (vm.selectedThreads.length) {
        message = message.replace("message has", "messages have");
        vm.selectedThreads.forEach(function (value) {
          var mailData = {};
          mailData.id = value.email_id;
          mailData.mail_id = value.mail_id;
          if( value.provider === 'gmail'){
            selectedGmailThreads.push(mailData);
          }
        });
      }else if(vm.currentThread){
        var mailData = {};
        mailData.id = vm.currentThread.email_id;
        mailData.mail_id = vm.currentThread.mail_id;
        if( vm.currentThread.provider === 'gmail'){
          selectedGmailThreads.push(mailData);
        }
      }

      // console.log(selectedGmailThreads);

      if( selectedGmailThreads.length ){
        MailService.deleteThreadParmanentGMail( selectedGmailThreads ).then(function (response) {
          // loadMailProvider( null, 'gmail');
          loadAllProvider();
          deselectThreads();
          $mdToast.show({
            template: '<md-toast class="md-toast success">' + message + '</md-toast>',
            hideDelay: 3000,
            position: 'bottom left'
          });
        });
      }

    }


    function isStarredMail() {
      if (vm.currentThread.labels.indexOf("STARRED") > -1) {
        return true;
      } else {
        return false;
      }
    }

    function isImportantMail() {
      if (vm.currentThread.labels.indexOf("IMPORTANT") > -1) {
        return true;
      } else {
        return false;
      }
    }

    function isReadMail() {
      return !(vm.currentThread.labels.indexOf("UNREAD") > -1);
    }


    function totalAchmentCount() {
      if(vm.currentThread.hasAttachments){
        return vm.currentThread.attachments.images.length + vm.currentThread.attachments.pdfs.length +
          vm.currentThread.attachments.docs.length + vm.currentThread.attachments.zips.length;
      }
    }

    function totalGMailAchmentCount() {
      var totalGMailAttachment = 0;
      if (vm.currentThread.attachments.images.length) {
        totalGMailAttachment = totalGMailAttachment + vm.currentThread.attachments.images.length
      }
      if (vm.currentThread.attachments.pdfs.length) {
        totalGMailAttachment = totalGMailAttachment + vm.currentThread.attachments.pdfs.length
      }
      if (vm.currentThread.attachments.docs.length) {
        totalGMailAttachment = totalGMailAttachment + vm.currentThread.attachments.docs.length
      }
      if (vm.currentThread.attachments.zips.length) {
        totalGMailAttachment = totalGMailAttachment + vm.currentThread.attachments.zips.length
      }
      return totalGMailAttachment;
    }

    function downloadAttachment(mailId, attachmentId, type, name, attachData) {
      if (vm.currentThread.provider === 'gmail') {
        MailService.getGMailAttachment(vm.currentThread.email_id, mailId, attachmentId).then(function (response) {
          var file = 'data:' + type + ';base64,' + response.data.replace(/-/g, '+').replace(/_/g, '/');
          var link = document.createElement('a');
          if (typeof link.download === 'string') {
            link.href = file;
            link.download = name;
            //Firefox requires the link to be in the body
            document.body.appendChild(link);
            //simulate click
            link.click();
            //remove the link when done
            document.body.removeChild(link);
          } else {
            window.open(uri);
          }
        });
      } else if (vm.currentThread.provider === 'outlook') {
        if (attachData) {
          var link = document.createElement('a');
          if (typeof link.download === 'string') {
            link.href = attachData;
            link.download = name;
            //Firefox requires the link to be in the body
            document.body.appendChild(link);
            //simulate click
            link.click();
            //remove the link when done
            document.body.removeChild(link);
          } else {
            window.open(uri);
          }
        }
      }

    }

    function getGmailAttachmentData(attachmentId, type, attachData) {
      console.log(vm.currentThread);
      if (vm.currentThread.provider === 'gmail') {
        MailService.getGMailAttachment(vm.currentThread.email_id, vm.currentThread.mail_id, attachmentId).then(function (response) {
          var file = 'data:' + type + ';base64,' + response.data.replace(/-/g, '+').replace(/_/g, '/');
          return file;
        });
      }
      if (vm.currentThread.provider === 'outlook') {
        return attachData;
      }

    }


    function getGMailThreadData(data) {
      var flag = null;
      vm.currentThread.attachments = {images: [], zips: [], pdfs: [], docs: []};
      var mailBodyContent;
      function decode(string) {
        return decodeURIComponent(encodeURI(atob(string.replace(/\-/g, '+').replace(/\_/g, '/'))));
      }

      function getText(response) {
        var result = '';
        // In e.g. a plain text message, the payload is the only part.
        var parts = [response.payload];

        while (parts.length) {
          var part = parts.shift();
          if (part.parts) {
            parts = parts.concat(part.parts);
          }

          if (part.mimeType === 'text/plain') {
            result = decode(part.body.data);
          } else if (part.mimeType === 'text/html') {
            result = decode(part.body.data);
            result = removeStyle(result);
          }else if (part.mimeType === "image/jpeg" || part.mimeType === "image/png" || part.mimeType === "image/gif") {
            vm.currentThread.attachments.images.push({id: part.body.attachmentId, size: part.body.size || '', name: part.filename});
          } else if (part.mimeType === "application/zip" || part.mimeType === "application/x-zip-compressed") {
            vm.currentThread.attachments.zips.push({id: part.body.attachmentId, size: part.body.size || '', name: part.filename});
          } else if (part.mimeType === "application/pdf") {
            vm.currentThread.attachments.pdfs.push({id: part.body.attachmentId, size: part.body.size || '', name: part.filename});
          } else if (part.mimeType === "application/msword" || part.mimeType === "plication/vnd.openxmlformats-officedocument.wordprocessingml.document" || part.mimeType === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
            vm.currentThread.attachments.docs.push({id: part.body.attachmentId, size: part.body.size || '', name: part.filename});
          }
        }
        return result;

      }

      mailBodyContent = getText(data);
      return $sce.trustAsHtml(mailBodyContent);
    }

    function getOutlookBodyData(message) {
      return $sce.trustAsHtml(message);
    }

    function removeStyle(str) {
      var basic = str.replace(/\r?\n|\r/g, "").replace(/<style(.*?)<\/style>/gm, "");
      return basic.replace(/<!--[\s\S]*?-->/g, "");
    }

    function getGMailHeaderValueByName(headerField, emailItem) {
      // console.log(emailItem.payload.headers);
      if (typeof emailItem === "undefined" || typeof emailItem.payload.headers === "undefined") {
        return '';
      }
      // console.log(emailItem.payload.headers);
      var val = '';
      emailItem.payload.headers.forEach(function (value) {
        if (value.name === headerField) {
          val = value.value;
        }
      });
      return val;
    }






    /**
     * If Has Attachment on Gmail
     * @param email
     * @returns {boolean}
     */
    function isHasAttachmentsGmail(email) {
      if (email.metadata.length) {
        var val = false;
        email.metadata.forEach(function (value) {
          if (value.payload.mimeType == "multipart/mixed") {
            val = true;
            return val;
          }
        });
        return val;
      } else {
        return false;
      }
    }

    /**
     * IF Read already for gmail
     * @param email
     * @returns {boolean}
     */

    function isReadGMail(email) {
      if (email.metadata[0].labelIds.indexOf("UNREAD") > -1) {
        return false;
      } else {
        return true;
      }
    }

    /**
     * Is Starred For Gmail
     * @param email
     * @returns {boolean}
     */
    function isStarredGMail(email) {
      if (email.metadata[0].labelIds.indexOf("STARRED") > -1) {
        return true;
      } else {
        return false;
      }
    }

    /**
     * Is Important For Gmail
     * @param email
     * @returns {boolean}
     */

    function isImportantGMail(email) {
      if (email.metadata[0].labelIds.indexOf("IMPORTANT") > -1) {
        return true;
      } else {
        return false;
      }
    }



    /**
     * Is the folder with the given name active?
     *
     * @param name
     * @returns {boolean}
     */
    function isFolderActive(name) {
      return (vm.currentFilter.type === 'folder' && vm.currentFilter.filter === name);
    }

    /**
     * Is the label with the given name active?
     *
     * @param name
     * @returns {boolean}
     */
    function isLabelActive(name) {
      return (vm.currentFilter.type === 'label' && vm.currentFilter.filter === name);
    }

    /**
     * Open thread
     *
     * @param thread
     */
    function openThread(thread) {
      if (thread.provider === 'gmail') {
        loadGMailThread(thread.email_id, thread);
        $rootScope.loadingProgress = true;
      } else if (thread.provider === 'outlook') {
        loadOutlookThread(thread.email_id, thread);
        $rootScope.loadingProgress = true;
      } else {
        vm.currentThread = thread;
      }
      // Update the state without reloading the controller
      $state.go('app.mail.account.threads.thread', {threadId: thread.mail_id, accountId: thread.email_id}, {notify: false});
    }

    /**
     * Close thread
     */
    function closeThread() {
      vm.currentThread = null;

      // Update the state without reloading the controller
      $state.go('app.mail.account.threads', {
        type: vm.currentFilter.type,
        filter: vm.currentFilter.filter,
        accountId: 0,
      }, {notify: false});
    }

    /**
     * Return selected status of the thread
     *
     * @param thread
     * @returns {boolean}
     */
    function isSelected(thread) {
      return vm.selectedThreads.indexOf(thread) > -1;
    }

    /**
     * Toggle selected status of the thread
     *
     * @param thread
     * @param event
     */
    function toggleSelectThread(thread, event) {
      if (event) {
        event.stopPropagation();
      }

      if (vm.selectedThreads.indexOf(thread) > -1) {
        vm.selectedThreads.splice(vm.selectedThreads.indexOf(thread), 1);
      }
      else {
        vm.selectedThreads.push(thread);
      }
    }

    /**
     * Select threads. If key/value pair given,
     * threads will be tested against them.
     *
     * @param [key]
     * @param [value]
     */
    function selectThreads(key, value) {
      // Make sure the current selection is cleared
      // before trying to select new threads
      vm.selectedThreads = [];

      for (var i = 0; i < vm.threads.length; i++) {
        if (angular.isUndefined(key) && angular.isUndefined(value)) {
          vm.selectedThreads.push(vm.threads[i]);
          continue;
        }

        if (angular.isDefined(key) && angular.isDefined(value) && vm.threads[i][key] === value) {
          vm.selectedThreads.push(vm.threads[i]);
        }
      }
    }

    /**
     * Deselect threads
     */
    function deselectThreads() {
      vm.selectedThreads = [];
    }

    /**
     * Toggle select threads
     */
    function toggleSelectThreads() {
      if (vm.selectedThreads.length > 0) {
        vm.deselectThreads();
      }
      else {
        vm.selectThreads();
      }
    }

    /**
     * Set the status on given thread, current thread or selected threads
     *
     * @param key
     * @param value
     * @param [thread]
     * @param [event]
     */
    function setThreadStatus(key, value, thread, event) {
      // Stop the propagation if event provided
      // This will stop unwanted actions on button clicks
      if (event) {
        event.stopPropagation();
      }

      // If the thread provided, do the changes on that
      // particular thread
      if (thread) {
        thread[key] = value;
        return;
      }

      // If the current thread is available, do the
      // changes on that one
      if (vm.currentThread) {
        vm.currentThread[key] = value;
        return;
      }

      // Otherwise do the status update on selected threads
      for (var x = 0; x < vm.selectedThreads.length; x++) {
        vm.selectedThreads[x][key] = value;
      }
    }

    /**
     * Toggle the value of the given key on given thread, current
     * thread or selected threads. Given key value must be boolean.
     *
     * @param key
     * @param thread
     * @param event
     */
    function toggleThreadStatus(key, thread, event) {
      // Stop the propagation if event provided
      // This will stop unwanted actions on button clicks
      if (event) {
        event.stopPropagation();
      }

      // If the thread provided, do the changes on that
      // particular thread

      thread = thread || vm.currentThread;
      if (thread) {

        if (typeof(thread[key]) !== 'boolean') {
          return;
        }



        if (thread.provider === 'gmail') {
          if (thread[key]) {
            thread[key] = !thread[key];
            changeThreadLabelFun('no', key, thread);
          }
          else {
            thread[key] = !thread[key];
            changeThreadLabelFun(key, '', thread);
          }
        }
        return;
      }

      // If the current thread is available, do the
      // changes on that one
      // if (vm.currentThread) {
      //   if (typeof(vm.currentThread[key]) !== 'boolean') {
      //     return;
      //   }
      //
      //   vm.currentThread[key] = !vm.currentThread[key];
      //   if (vm.currentThread.provider === 'gmail') {
      //     if (vm.currentThread[key] === true) {
      //       changeThreadLabelFun(key, '');
      //     }
      //     else if (vm.currentThread[key] === false) {
      //       changeThreadLabelFun('no', key);
      //     }
      //   }
      //   return;
      // }

      // Otherwise do the status update on selected threads
      for (var x = 0; x < vm.selectedThreads.length; x++) {
        if (typeof(vm.selectedThreads[x][key]) !== 'boolean') {
          continue;
        }

        vm.selectedThreads[x][key] = !vm.selectedThreads[x][key];
      }
    }

    /**
     * Get label object content
     *
     * @param id
     * @returns {*}
     */
    function getLabel(id) {
      for (var i = 0; i < vm.labels.length; i++) {
        if (vm.labels[i].id === id) {
          return vm.labels[i];
        }
      }
    }

    /**
     * Get label color from label object
     *
     * @param id
     * @returns {*}
     */
    function getLabelColor(id) {
      return vm.getLabel(id).color;
    }

    /**
     * Get label title from label object
     *
     * @param id
     * @returns {*}
     */
    function getLabelTitle(id) {
      return vm.getLabel(id).title;
    }

    /**
     * Toggle label
     *
     * @param label
     */
    function toggleLabel(label) {
      // Toggle label on the currently open thread
      if (vm.currentThread) {
        if (vm.currentThread.labels.indexOf(label.id) > -1) {
          vm.currentThread.labels.splice(vm.currentThread.labels.indexOf(label.id), 1);
        }
        else {
          vm.currentThread.labels.push(label.id);
        }

        return;
      }

      // Toggle labels on selected threads
      // Toggle action will be determined by the first thread from the selection.
      if (vm.selectedThreads.length > 0) {
        // Decide if we are going to remove or add labels
        var removeLabels = (vm.selectedThreads[0].labels.indexOf(label.id) > -1);

        for (var i = 0; i < vm.selectedThreads.length; i++) {
          if (!removeLabels) {
            vm.selectedThreads[i].labels.push(label.id);
            continue;
          }

          if (vm.selectedThreads[i].labels.indexOf(label.id) > -1) {
            vm.selectedThreads[i].labels.splice(vm.selectedThreads[i].labels.indexOf(label.id), 1);
          }
        }
      }
    }

    /**
     * @example isLabelExist - If folder Exist By Folder ID.
     * @param {string} label - Label Object ID.
     * @returns {boolean} - Return True/False Value.
     */
    function isLabelExist(label) {
      if (vm.currentThread && vm.currentThread.labels) {
        return (vm.currentThread.labels.indexOf(label.id) > -1);
      }
    }

    /**
     * @example isFolderExistByID - If folder Exist By Folder ID.
     * @param {string} folder - Folder Object ID.
     * @returns {boolean} - Return True/False Value.
     */
    function isFolderExistByID(folder) {
      if (vm.currentThread && vm.currentThread.labels) {
        return (vm.currentThread.labels.indexOf(folder) > -1);
      }
    }

    /**
     * @example changeView - To change Mail layout view.
     * @param {string} view - Define which view.
     */
    function changeView(view) {
      if (vm.views[view]) {
        vm.defaultView = view;
        vm.currentView = view;
      }
    }

    /**
     * @example composeDialog - To Compose A Mail.
     * @param {Object} ev - Javascript Event.
     */
    function composeDialog(ev) {
      $mdDialog.show({
        controller: 'ComposeDialogController',
        controllerAs: 'vm',
        locals: {
          selectedMail: undefined,
          emailAccounts: vm.accounts,
          selectedAccount: vm.selectedAccount || vm.accounts[0]
        },
        templateUrl: 'app/main/mail/dialogs/compose/compose-dialog.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: false
      });
    }

    /**
     * @example replyDialog - To Show A popup to Reply Mail.
     * @param {Object} ev - Javascript Event.
     */
    function replyDialog(ev) {
      var selectedAccount = vm.selectedAccount || vm.accounts[0];
      var replyMailData = {};
      replyMailData.type = 'reply';
      replyMailData.from = {};
      replyMailData.from.id = selectedAccount.id;
      replyMailData.from.name = selectedAccount.name;
      replyMailData.from.email = selectedAccount.email;
      replyMailData.to = vm.currentThread.from.email;
      replyMailData.subject = vm.currentThread.subject;
      replyMailData.message = angular.element('#message_contents').html();

      $mdDialog.show({
        controller: 'ComposeDialogController',
        controllerAs: 'vm',
        locals: {
          selectedMail: replyMailData,
          emailAccounts: vm.accounts,
          selectedAccount: undefined
        },
        templateUrl: 'app/main/mail/dialogs/compose/reply-dialog.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: false
      });
    }


    /**
     * @example forwardDialog - To Show A popup to Forward Mail.
     * @param {Object} ev - Javascript Event.
     */
    function forwardDialog(ev) {
      var selectedAccount = vm.selectedAccount || vm.accounts[0];
      var forwardMailData = {};
      forwardMailData.type = 'forward';
      forwardMailData.from = {};
      forwardMailData.from.id = selectedAccount;
      forwardMailData.from.name = selectedAccount.name;
      forwardMailData.from.email = selectedAccount.email;
      forwardMailData.to = vm.currentThread.from.email;
      forwardMailData.subject = vm.currentThread.subject;
      forwardMailData.time = vm.currentThread.time;
      forwardMailData.message = angular.element('#message_contents').html();

      $mdDialog.show({
        controller: 'ComposeDialogController',
        controllerAs: 'vm',
        locals: {
          selectedMail: forwardMailData,
          emailAccounts: vm.accounts,
          selectedAccount: undefined
        },
        templateUrl: 'app/main/mail/dialogs/compose/forward-dialog.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: false
      });
    }

    /**
     * @example toggleSidenav - Toggle Side Navigation Menu On Click.
     * @param {string} sidenavId - Contain Div ID.
     */
    function toggleSidenav(sidenavId) {
      $mdSidenav(sidenavId).toggle();
    }

    /**
     * @example PrintMail - Another.
     * @param {string} divName - Div ID Name.
     * @returns {number} Void - Return Void.
     */
    function printMail(divName) {
      var selectedAccount = vm.selectedAccount || vm.accounts[0];
      var printContents = document.getElementById(divName).innerHTML;
      var printTopContent = '<table border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr> <td width="143"><img class="logo" src="https://www.gmail.com/mail/help/images/logonew.gif" alt="Gmail" width="143" height="59" /></td>' +
        '<td align="right"><span style="color: #777777;"><strong>' + selectedAccount.name + '&lt;' + selectedAccount.email + '&gt;</strong></span></td> </tr> </tbody> </table>' +
        '<hr /> <div class="maincontent"> <table border="0" width="100%" cellspacing="0" cellpadding="0"> <tbody> <tr> <td><span style="font-size: xx-small;"><h1>' + vm.currentThread.subject + '</h1></span></td> </tr>' +
        '</tbody> </table> <hr /> <table class="message" border="0" width="100%" cellspacing="0" cellpadding="0"> <tbody> <tr> <td><strong>' + vm.currentThread.from.name + '</strong>&lt;' + vm.currentThread.from.email + '&gt;</td> <td align="right">' + vm.currentThread.time + '</td> </tr>' +
        '<tr><td colspan="2"><div class="replyto">Reply-To: ' + vm.currentThread.from.email + '</div> <div>To: ' + vm.currentThread.to.email + '</div> </td> </tr> </tbody> </table> </div><br>';
      var popupWin = window.open('', '_blank');
      popupWin.document.open();
      popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="/styles/print_style.css" /></head><body onload="window.print()">' + printTopContent + printContents + '</body></html>');
      popupWin.document.close();
    }

    Array.prototype.getById = function (value) {
      return this.filter(function (x) {
        return x.id === value;
      })[0];
    };
  }
})();

