(function () {
  'use strict';

  angular
    .module('app.chat')
    .factory('ChatsService', ChatsService);

  /**
   * @method Service For Chat
   * @param {string} $http
   * @param {object} $q
   * @returns {Object|Array}
   */
  // The Service to get All requested data by Controller.

  function ChatsService($http, $q) {

    return {

      /**
       * @method findGiphyStickerSearch
       * @param {number} limit
       * @param {number} offset
       * @param {string} tags
       * @return {Object|Array}
       */
      // Get Giphy Sticker by Searching String
      findGiphyStickerSearch: function (limit, offset, tags) {
        var defer = $q.defer();
        var data = {};
        if (limit) {
          data.limit = limit;
        }
        if (offset) {
          data.offset = offset;
        }
        if (tags) {
          data.q = tags;
        }
        $http.post('/chat/getGiphyStickerByTag', data).then(function successCallback(resp) {
          defer.resolve(resp.data.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      /**
       * @method getConversations
       * @return {Object|Array}
       */
      // Get All Conversation By Current User
      getConversations: function () {
        var defer = $q.defer();
        $http.post('/chat/conversations').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getChatInitData: function () {
        var defer = $q.defer();
        $http.post('/chat/getChatInitData').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      createNewGroup: function ( data ) {
        var defer = $q.defer();
        $http.post('/chat/createNewGroup', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      /**
       * @method getConversations
       * @return {Object|Array}
       */
      // Get All Conversation By Current User
      deleteConversation: function ( id ) {
        var defer = $q.defer(); var data = {}; data.id = id;
        $http.post('/chat/deleteConversation', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      /**
       * @method getConversationById
       * @param {string|ObjectID} id
       * @return {Object|Array}
       */
      // Get Specific Conversation by Conversation ID
      getConversationById: function (id) {
        var defer = $q.defer();
        $http.post('/chat/conversationById', {id: id}).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      addMessage: function (data) {
        var defer = $q.defer();
        $http.post('/chat/addMessage', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      /**
       * @method getConversationById
       * @param {ObjectID|string} id
       * @return {Object|Array}
       */
      // Make Specific Conversation unread count empty for Current user
      makeUnreadEmpty: function (id) {
        var defer = $q.defer();
        var data = {};
        data.id = id;
        $http.post('/chat/makeUnreadEmpty', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      /**
       * @method getConversationById
       * @param {ObjectID|string} id
       * @return {Object|Array}
       */
      // Make Specific Conversation unread count empty for Current user
      unreadCountTotal: function () {
        var defer = $q.defer();
        var data = {};
        $http.post('/chat/unreadCountTotal', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      /**
       * Description
       * @method getConversationByContact
       * @param {string|ObjectId} contactId
       * @return {Object|Array}
       */
      // Get Conversation by Specific Contact
      getConversationByContact: function (data) {
        var defer = $q.defer();
        $http.post('/chat/conversationByContactId', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      changeUserStatus: function (data) {
        var defer = $q.defer();
        $http.post('/chat/changeUserStatus', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      sendContactRequest: function (data) {
        var defer = $q.defer();
        $http.post('/chat/sendContactRequest', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      acceptRequest: function (data) {
        var defer = $q.defer();
        $http.post('/chat/acceptRequest', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      /**
       * @method leaveGroup
       * @param {ObjectId|string} conversation - Chat Conversation ID
       * @returns Mixed
       */
      // When User Want to leave a particular group
      leaveGroup: function (conversation) {
        var defer = $q.defer();
        var data = {};
        data.conversation = conversation;
        $http.post('/chat/leaveGroup', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      /**
       * @method createGroupChat
       * @param {ObjectId|string} conversation
       * @param {Array} contacts
       * @return {Object|Array}
       */
      // Create a group Conversation for some contacts
      createGroupChat: function (conversation, contacts) {
        var defer = $q.defer();
        var data = {};
        data.conversation = conversation;
        data.contacts = contacts;
        $http.post('/chat/createGroupChat', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },


      /**
       * @method getContacts
       * @return {Object|Array}
       */
      // Get All Contacts With this Request
      getSocharaContacts: function () {
        var defer = $q.defer();
        $http.post('/contacts/getSocharaContacts').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      sendMail: function ( data ) {
        var defer = $q.defer();
        $http.post('/chat/sendMail', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      /**
       * @method getContactDetails
       * @return {Object|Array}
       */
      // Get Contact Details For Specific Contact
      getContactDetails: function (data) {
        var defer = $q.defer();
        $http.post('/contacts/getContactById', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      /**
       * @method getUser
       * @return {Object|Array}
       */
      // Get Current/Logged IN User Data
      getUser: function () {
        var defer = $q.defer();
        $http.post('/json/user').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      /***
       * @method muteConversation
       * @param {ObjectId|string} id
       * @param {string} mswitch
       * @returns {Object}
       */
      // Mute/Unmute Specific Conversation with ID
      muteConversation: function (id, mswitch) {
        var defer = $q.defer();
        var data = {};
        data.id = id;
        data.mswitch = mswitch;
        $http.post('/chat/muteConversation', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      /**
       * @method findGiphyStickerTrending
       * @param {number} limit
       * @param {number} offset
       * @return {Object|Array}
       */
      // Get Giphy Sticker Data From Giphy API via backend Controller
      findGiphyStickerTrending: function (limit, offset) {
        var defer = $q.defer();
        var data = {};
        if (limit) {
          data.limit = limit;
        }
        if (offset) {
          data.offset = offset;
        }
        $http.post('/chat/getGiphySticker', data).then(function successCallback(resp) {
          defer.resolve(resp.data.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      }

    };
  }

})();
