(function () {
  'use strict';

  angular
    .module('app.chat')
    .controller('ChatController', ChatController);

  /**
   * Description
   * @function Controller.
   * @example Controller For Chat
   * @method ChatController
   * @param {} ChatsService
   * @param {string} $state - Used for custom keyboard key as Aschii Code.
   * @param {string} $state - Used for custom keyboard key as Aschii Code.
   * @param {string} $state - Used for custom keyboard key as Aschii Code.
   * @param {string} $state - Used for custom keyboard key as Aschii Code.
   * @param {string} $state - Used for custom keyboard key as Aschii Code.
   * @param {string} $state - Used for custom keyboard key as Aschii Code.
   * @param {string} $state - Used for custom keyboard key as Aschii Code.
   * @param {string} $state - Used for custom keyboard key as Aschii Code.
   * @param {} ContactService
   * @param {string} $state - Used for custom keyboard key as Aschii Code.
   * @param {string} $state - Used for custom keyboard key as Aschii Code.
   * @param {string} $state - Used for custom keyboard key as Aschii Code.
   * @return 
   */
  function ChatController(ChatsService, $mdSidenav, $timeout, $document, $location, $q, $mdConstant, $rootScope, $scope, ContactService, $mdDialog, $state, $mdToast) {
    var vm = this;

    // Data
    // vm.contacts = ChatsService.contacts = Contacts.data;
    // vm.contacts = null;
    // vm.chats = ChatsService.chats;
    // vm.user = User.data;
    vm.leftSidenavView = false;
    vm.chat = undefined;

    // Methods
    vm.getChatByConversationContact = getChatByConversationContact;
    vm.inviteSocharaUserToJoin = inviteSocharaUserToJoin;
    vm.getChatByConversationID = getChatByConversationID;
    vm.toggleSidenav = toggleSidenav;
    vm.toggleLeftSidenavView = toggleLeftSidenavView;
    vm.reply = reply;
    vm.setUserStatus = setUserStatus;
    vm.clearMessages = clearMessages;
    vm.viewChatTab = viewChatTab;
    vm.tabCurrentTab = "chat";
    vm.emoAreaVisible = false;
    vm.showEmoArea = showEmoArea;
    vm.showEmojiArea = showEmojiArea;
    vm.showEmoticonArea = showEmoticonArea;
    vm.showEmoStickerArea = showEmoStickerArea;
    vm.giphyStickerTimoutInterval = giphyStickerTimoutInterval;
    vm.giphyClearTimoutInterval = giphyClearTimoutInterval;

    vm.insertEmoticonToMessageBody = insertEmoticonToMessageBody;
    vm.insertEmojiToMessageBody = insertEmojiToMessageBody;
    vm.closeDialog = closeDialog;
    vm.insertEmojiStickerToChat = insertEmojiStickerToChat;
    vm.getRecipientId = getRecipientId;
    // vm.getRecipientNames = getRecipientNames;
    vm.getContactDetails = getContactDetails;
    vm.querySearch = querySearch;
    vm.createNewGroup = createNewGroup;
    vm.newGroupContactSearch = newGroupContactSearch;
    vm.addContactToGroup = addContactToGroup;
    vm.addContactToNewGroup = addContactToNewGroup;

    vm.selectedContact = [];
    vm.loadContacts = loadContacts;
    vm.selectedContactForGroup = null;
    vm.createGroupInit = createGroupInit;
    vm.createGroup = createGroup;
    vm.leaveGroup = leaveGroup;
    vm.chatUserPhoto = chatUserPhoto;
    vm.getChatUserStatus = getChatUserStatus;
    vm.muteConversation = muteConversation;
    vm.deleteConversation = deleteConversation;
    vm.deleteConversationConfirmation = deleteConversationConfirmation;
    vm.createNewConversation = createNewConversation;
    vm.createNewContact = createNewContact;
    $rootScope.goToItem = goToItem;
    vm.toggleFavoriteContact = toggleFavoriteContact;

    vm.isObject = isObject;
    vm.createGroupNew = createGroupNew;
    vm.chatRecipientName = chatRecipientName;
    vm.sendRequestDialog = sendRequestDialog;
    vm.sendRequestConfirm = sendRequestConfirm;
    vm.sendRequestConfirmCancel = sendRequestConfirmCancel;
    vm.acceptRequest = acceptRequest;

    // vm.conversations = conversations;
    // vm.contacts = contacts;


    $scope.$watchCollection(function () {
      return $state.params;
    }, function () {
      var type = $state.params.type || '';
      var typeId = $state.params.typeId || '';
      if (type && type === 'u' && typeId) {
        getChatByConversationContact(typeId);
      } else if (type && type === 'c' && typeId) {
        getChatByConversationID(typeId);
      }
    });


    /**
     * @method conversations
     * @return {Object|Array}
     */
    // Get All Conversation For Current User
    /**
     * Description
     * @method conversations
     * @return 
     */
    function conversations() {
      ChatsService.getConversations().then(function (resp) {
        vm.loadingConversations = false;
        vm.conversations = resp;
        $rootScope.loadingProgress = false;
      });
    }


    /**
     * Description
     * @method goToItem
     * @param {} id
     * @param {} module
     * @param {} $event
     * @return 
     */
    function goToItem( id, module, $event ){
      if( module === 'chat'){
        var contact = vm.contacts.find(function(element) {
          return element.id === id;
        });
        getChatByConversationContact(contact);
      }
    }




    /**
     * Description
     * @method init
     * @return 
     */
    function init() {
      $rootScope.searchable = true;
      $rootScope.loadingProgress = true;
      vm.loadingConversations = true;
      // getUserData();
      vm.tabCurrentTab = 'chat';

      if( $rootScope.user ){
        vm.user = $rootScope.user;
      }else{
        getUserData();
      }

      ChatsService.getChatInitData().then(function (resp) {
        vm.conversations = resp.conversations;
        vm.contacts = resp.contacts;
        vm.loadingConversations = false;
        $rootScope.loadingProgress = false;
        $rootScope.items = resp.contacts.map(function (contact) {
          return {name: contact.first_name + ' ' + contact.last_name, id: contact.id, module: 'chat', image: contact.photo_url || 'images/avatars/profile.jpg'};
        });
      });
    }

    init();

    /***
     * @example Find chat by Conversation ID
     * @param {number} conversationId
     * @return {Object|Array}
     */
    // Find Specific Chat/Conversation with Conversation ID
    /**
     * Description
     * @method getChatByConversationID
     * @param {} conversationId
     * @return 
     */
    function getChatByConversationID(conversationId) {
      vm.chat = null;
      vm.loadingChat = true;
      vm.showRecipientContact = false;
      $state.go('app.chat.type.id', {type: 'c', typeId: conversationId});
      $rootScope.loadingProgress = true;
      getConversationById(conversationId);
    }


    /**
     * Description
     * @method getConversationById
     * @param {} conversationId
     * @return 
     */
    function getConversationById(conversationId) {
      ChatsService.getConversationById(conversationId).then(function (resp) {
        vm.chat = resp;
        vm.chatConversationId = resp.id;
        scrollToBottomOfChat();
        makeUnreadEmpty(conversationId);
        $rootScope.loadingProgress = false;
        if(resp.unread && resp.unread[$rootScope.user.id]){
          $rootScope.getGlobalNotificationCount();
        }
      });
    }

    //setup before functions
    var contactSearchTypingTimer;                //timer identifier
    var contactSearchDoneTypingInterval = 1000;  //time in ms, 5 second for example
    vm.contactSearchTimoutInterval = contactSearchTimoutInterval;
    vm.contactSearchClearTimoutInterval = contactSearchClearTimoutInterval;

    /**
     * Description
     * @return
     * @method contactSearchTimoutInterval
     * @return 
     */
    function contactSearchTimoutInterval() {
      $timeout.cancel(contactSearchTypingTimer);
      contactSearchTypingTimer = $timeout(contactSearch, contactSearchDoneTypingInterval);
    }

    /**
     * Description
     * @return
     * @method contactSearch
     * @return 
     */
    function contactSearch() {
      if( vm.chatSearch.length && vm.tabCurrentTab !== 'group' ){
        ContactService.getAllSocharaUsers({name: vm.chatSearch}).then(function (response) {
          vm.socharaContacts = response;
          vm.showSocharaSearchResult = true;
        });
      }else{
        vm.socharaContacts = [];
      }
    }

    /**
     * Description
     * @method contactSearchClearTimoutInterval
     * @return 
     */
    function contactSearchClearTimoutInterval() {
      $timeout.cancel(contactSearchTypingTimer);
    }

    /**
     * @method getChatByConversationContact
     * @param {Object|string} contactId
     * @return {Object|Array}
     */
    // Get Specific Conversation by Contact ID
    /**
     * Description
     * @method getChatByConversationContact
     * @param {} contact
     * @return 
     */
    function getChatByConversationContact(contact) {
      if( contact && contact.user_id ){
        vm.chat = null;
        vm.loadingChat = true;
        vm.showRecipientContact = false;
        getConversationByUser(contact.user_id);
      }
    }

    /**
     * Description
     * @method inviteSocharaUserToJoin
     * @param {} contact
     * @return 
     */
    function inviteSocharaUserToJoin(contact) {
      if( contact && contact.id ){
        vm.chatSearch = '';
        vm.chat = null;
        vm.loadingChat = true;
        vm.showRecipientContact = false;
        getConversationByUser(contact.id, 'invite');
      }
    }

    /**
     * Description
     * @method getConversationByUser
     * @param {} id
     * @param {} type
     * @return 
     */
    function getConversationByUser(id, type) {
      ChatsService.getConversationByContact({contact: id, type: type || ''}).then(function (resp) {
        if (resp && resp.id) {
          addToConversation(resp);
          $state.go('app.chat.type.id', {type: 'c', typeId: resp.id});
          vm.chat = resp;
          vm.chat.messages = resp.messages || [];
          vm.chatConversationId = resp.id;
          vm.tabCurrentTab = "chat";
          resetReplyTextarea();
          scrollToBottomOfChat();
          if( resp.unread[$rootScope.user.id] ){
            $rootScope.notificationCount['chat'] --;
          }
        } else {
          vm.chat = {};
          vm.chat.recipients = [];
          vm.chat.messages = [];
          vm.chatConversationId = '';
          resetReplyTextarea();
        }

      });
    }

    /**
     * Description
     * @method addToConversation
     * @param {} resp
     * @return 
     */
    function addToConversation(resp) {
      if (!vm.conversations.getById(resp.id)) {
        vm.conversations.push(resp);
      }
    }

    /**
     * Description
     * @method createGroupNew
     * @return 
     */
    function createGroupNew() {

    }

    vm.changeUserStatus = changeUserStatus;

    /**
     * Description
     * @method changeUserStatus
     * @return 
     */
    function changeUserStatus(){
      if( $rootScope.user.status && $rootScope.user.status.length ){
        vm.user.status = $rootScope.user.status;
        ChatsService.changeUserStatus({status: $rootScope.user.status}).then(function (respStatus) {})
      }
    }


    /***
     * @example makeUnreadEmpty
     * @param conversationId
     */
    // Make All Unread message to Read
    /**
     * Description
     * @method makeUnreadEmpty
     * @param {} conversationId
     * @return 
     */
    function makeUnreadEmpty(conversationId) {
      if (conversationId) {
        ChatsService.makeUnreadEmpty(conversationId).then(function (respUnread) {
          if (respUnread && vm.conversations && vm.conversations.getById(conversationId)) {
            vm.conversations.getById(conversationId).unread[vm.user.id] = 0;
          }
        });
      }
    }

    /**
     * Description
     * @method deleteConversation
     * @return 
     */
    function deleteConversation() {
      if (vm.chat && vm.chat.id){
        $mdDialog.show({
          /**
           * Description
           * @method controller
           * @return vm
           */
          controller: function () {
            return vm;
          },
          controllerAs: 'vm',
          templateUrl: '/app/main/chat/dialogs/delete-conversation.html',
          clickOutsideToClose: false,
          multiple: false
        });
      }
    }

    /**
     * Description
     * @method deleteConversationConfirmation
     * @return 
     */
    function deleteConversationConfirmation(){
      ChatsService.deleteConversation(vm.chat.id).then(function (resp) {
        closeDialog();
        if( resp ){
          showMessage('The Conversation has been deleted !');
          vm.chat = null;
          vm.loadingChat = false;
          vm.showRecipientContact = false;
          conversations();
        }
      });
    }


    var semicolon = 186;
    vm.customKeys = [$mdConstant.KEY_CODE.ENTER, $mdConstant.KEY_CODE.SPACE, $mdConstant.KEY_CODE.COMMA, semicolon, $mdConstant.KEY_CODE.TAB];


    /**
     * @method createFilterFor
     * @param {Object|Array} query
     * @return {Object|Array}
     */
    //Create filter function for a query string
    /**
     * Description
     * @method createFilterFor
     * @param {} query
     * @return FunctionExpression
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(item) {
        return (item.value.indexOf(lowercaseQuery) === 0);
      };
    }


    /**
     * Description
     * @method leaveGroup
     * @param {} conversationId
     * @return 
     */
    function leaveGroup(conversationId) {
      ChatsService.leaveGroup(conversationId).then(function (resp) {
        vm.chat = {};
        vm.conversations = vm.conversations.filterExceptId(resp.id);
      });
    }

    /**
     * Description
     * @method chatUserPhoto
     * @param {} recipients
     * @return LogicalExpression
     */
    function chatUserPhoto(recipients) {
      return recipients.filter(function (x) {
        return x.id !== vm.user.id;
      })[0].photo_url || 'images/avatars/profile.jpg';
    }

    /**
     * Description
     * @method getChatUserStatus
     * @param {} recipients
     * @return LogicalExpression
     */
    function getChatUserStatus(recipients) {
      return recipients.filter(function (x) {
        return x.id !== vm.user.id;
      })[0].mode || 'offline';
    }

    /**
     * Description
     * @method muteConversation
     * @param {} mswitch
     * @return 
     */
    function muteConversation(mswitch) {
      ChatsService.muteConversation(vm.chat.id, mswitch).then(function (resp) {
        vm.chat.mute = resp.mute;
        vm.conversations.getById(resp.id).mute = resp.mute;
      });
    }

    /**
     * Search for vegetables.
     * @method querySearch
     * @param {} query
     * @return ArrayExpression
     */
    function querySearch(query) {
      var contactsData = vm.contactsData.filter(function (contact) {
        return vm.chat.users.indexOf(contact.id) === -1;
      });
      vm.groupContactsData = query ? contactsData.filter(createFilterFor(query)) : contactsData;
      return [];
    }

    /**
     * Description
     * @method createNewGroup
     * @return 
     */
    function createNewGroup(){
      var groupData = {name: vm.newGroup.name || undefined};
      groupData.contacts = vm.selectedGroupContacts.map(function (contact) {
        return contact.id;
      });
      ChatsService.createNewGroup(groupData).then(function (respGroup) {
        conversations();
        vm.showAddGroupPopup = false;
        vm.selectedContact = [];
        vm.chat = respGroup;
        vm.tabCurrentTab = 'group';
        closeDialog();
        return true;
      });
    }

    /**
     * Description
     * @method initializeNewGroup
     * @return 
     */
    function initializeNewGroup(){
      vm.selectedGroupContacts = [];
      var groupContacts = vm.contacts.map(function (c, index) {
        var contact = {
          name: c.first_name + ' ' + c.last_name,
          // email: c.email,
          status: c.status,
          id: c.id,
          image: c.photo_url || 'images/avatars/profile.jpg',
          mode: c.mode
        };
        contact.value = contact.name.toLowerCase();
        return contact;
      });
      vm.newGroupContacts = groupContacts.slice();
      vm.mappedContacts = groupContacts.slice();
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: '/app/main/chat/dialogs/create-group.html',
        clickOutsideToClose: true,
        multiple: true
        // targetEvent: ev,
      });
    }


    /**
     * Description
     * @method newGroupContactSearch
     * @param {} query
     * @return ArrayExpression
     */
    function newGroupContactSearch(query) {
      vm.newGroupContacts = query ? vm.mappedContacts.filter(createFilterFor(query)) : vm.mappedContacts;
      return [];
    }

    /**
     * Description
     * @method addContactToGroup
     * @param {} contact
     * @return 
     */
    function addContactToGroup(contact) {
      vm.selectedContact.push(contact);
    }

    /**
     * Description
     * @method addContactToNewGroup
     * @param {} contact
     * @return 
     */
    function addContactToNewGroup(contact) {
      vm.selectedGroupContacts.push(contact);
    }

    /**
     * Description
     * @method loadContacts
     * @param {} callback
     * @return 
     */
    function loadContacts(callback) {
      vm.chatRecipientIds = [];
      vm.chat.recipients.forEach(function (val) {
        if (val.id !== vm.user.id) {
          vm.chatRecipientIds.push(val.id);
        }
      });
      var returnData = vm.contacts.map(function (c, index) {
        var contact = {
          name: c.first_name + ' ' + c.last_name,
          email: c.email,
          id: c.id,
          image: c.photo_url || 'images/avatars/profile.jpg',
          mode: c.mode
        };
        contact._lowername = contact.name.toLowerCase();
        return contact;
      });


      $rootScope.loadingProgress = false;
      callback(returnData);

    }

    /**
     * Description
     * @method createNewContact
     * @return 
     */
    function createNewContact() {
      if (!vm.newContact.email) {
        return false;
      }
      var contactData = vm.newContact;
      contactData.photo_url = contactData.photo_url || 'images/avatars/profile.jpg';
      ContactService.addNewContact(contactData).then(function (resp) {
        closeDialog();
        if( resp && !resp.message ){
          showMessage('An invitation has been sent to your newly created contact !');
          ChatsService.sendMail({id: resp.id}).then(function (respMail) {
            console.log(respMail);
          });
        }else{
          showMessage(resp.message);
        }
      });
    }

    /**
     * Description
     * @method createNewConversation
     * @return 
     */
    function createNewConversation(){
      if(vm.tabCurrentTab === 'contact'){
        vm.newContact = {};
        $mdDialog.show({
          /**
           * Description
           * @method controller
           * @return vm
           */
          controller: function () {
            return vm;
          },
          controllerAs: 'vm',
          templateUrl: '/app/main/chat/dialogs/add-new-contact.html',
          clickOutsideToClose: true,
          multiple: true
        });
      }else if(vm.tabCurrentTab === 'group'){
        initializeNewGroup();
      }
    }


    /**
     * Description
     * @method createGroup
     * @return 
     */
    function createGroup() {
      ChatsService.createGroupChat(vm.chatConversationId, vm.selectedContact).then(function (resp) {
        conversations();
        vm.showAddGroupPopup = false;
        vm.selectedContact = [];
        vm.chat = resp;
        vm.tabCurrentTab = 'group';
        return true;
      });
    }

    /**
     * Description
     * @method createGroupInit
     * @return 
     */
    function createGroupInit() {
      $rootScope.loadingProgress = true;
      loadContacts(function (contactsData) {
        vm.groupContactsData = contactsData.filter(function (contact) {
          return vm.chat.users.indexOf(contact.id) === -1;
        });
        vm.showAddGroupPopup = true;
      });
    }

    vm.getRecepientInfo = getRecepientInfo;

    /**
     * Description
     * @method getRecepientInfo
     * @return 
     */
    function getRecepientInfo() {
      $timeout(function () {
        vm.chatReceipient = {};
        if (vm.chat && vm.chat.recipients && vm.chat.recipients.length === 2) {
          vm.chatReceipient = vm.chat.recipients.filter(function (recipient) {
            return recipient.id !== vm.user.id;
          })[0];
        }
      });
    }

    /**
     * Description
     * @method chatRecipientName
     * @param {} recipients
     * @return BinaryExpression
     */
    function chatRecipientName(recipients){
      var recipient = recipients.filter(function (recip) {
        return recip.id !== $rootScope.user.id;
      })[0];
      return recipient.first_name + 's ' + recipient.last_name;
    }

    /**
     * Description
     * @method sendRequestDialog
     * @param {} event
     * @return 
     */
    function sendRequestDialog(event){
      event.stopPropagation();
      if(vm.chat && !vm.chat.messages.length ){
        var recipient = vm.chat.recipients.filter(function (recip) {
          return recip.id !== $rootScope.user.id;
        })[0];
        vm.chat.request_message = 'Hi ' + recipient.first_name + ' ' + recipient.last_name + ', I\'d like to add you as a contact.'
        vm.showContactRequestMessage = true;
      }
    }

    /**
     * Description
     * @method sendRequestConfirm
     * @return 
     */
    function sendRequestConfirm(){
      vm.showContactRequestMessage = false;
      var id = vm.chat.id, message = vm.chat.request_message || '';
      if( !id && !message ){
        showMessage('Message empty, can not send request !');
      }else{
        ChatsService.sendContactRequest({ id: id, message: message }).then(function (respRequest) {
          if(typeof respRequest === 'object'){
            vm.chatConversationId = vm.chatConversationId ? vm.chatConversationId : respRequest.id;
            vm.conversations.push(respRequest);
            getChatByConversationID(vm.chatConversationId);
          }
        });
      }
    }

    /**
     * Description
     * @method sendRequestConfirmCancel
     * @return 
     */
    function sendRequestConfirmCancel(){
      vm.showContactRequestMessage = false;
    }


    /**
     * Description
     * @method acceptRequest
     * @return 
     */
    function acceptRequest(){
      if(vm.chat && vm.chat.pending ){
        ChatsService.acceptRequest({id: vm.chat.id}).then(function (respRequest) {
          if(respRequest && typeof respRequest === 'object'){
            $rootScope.getGlobalNotificationCount();
            getChatByConversationID(vm.chat.id);
          }
        });
      }
    }


    /**
     * Description
     * @method getRecipientId
     * @return 
     */
    function getRecipientId() {
      if (vm.chat && vm.chat.recipients && vm.chat.recipients.length === 2) {
        var recepients = vm.chat.recipients.filter(function (x) {
          return x.id !== vm.user.id;
        });
        return recepients[0].id;
      } else {
        return '';
      }
    }


    /**
     * Description
     * @method getContactDetails
     * @param {} id
     * @return 
     */
    function getContactDetails(id) {
      if (vm.showRecipientContact) {
        vm.showRecipientContact = false;
      } else {
        console.log(id);
        // $rootScope.loadingProgress = true;
        // ChatsService.getContactDetails({id: id}).then(function (resp) {
        //   vm.recipientDetails = resp;
        //   if( resp.sochara_user_id ){
        //     vm.recipientDetails.isContact = true;
        //   }
        //   vm.showRecipientContact = true;
        //   $rootScope.loadingProgress = false;
        //   return true;
        // });
      }
    }

    /**
     * Description
     * @method isObject
     * @param {} param
     * @return LogicalExpression
     */
    function isObject(param) {
      return typeof param === 'object' && Object.keys(param).length;
    }


    /**
     * Description
     * @return
     * @method getUserData
     * @return 
     */
    function getUserData() {
      ChatsService.getUser().then(function (resp) {
        vm.user = resp;
      });
    }


    vm.emoticonList = [
      'angel', 'angry', 'bandit', 'bartlett', 'beer', 'bigsmile', 'bike', 'blackwidow', 'blushing', 'bow', 'brokenheart',
      'bucky', 'bug', 'cake', 'call', 'captain', 'cash', 'cat', 'clapping', 'coffee', 'cool', 'crying',
      'dancing', 'devil', 'dog', 'doh', 'drink', 'drunk', 'dull', 'emo', 'envy', 'evilgrin', 'facepalm', 'finger', 'fingerscrossed',
      'flower', 'fubar', 'giggle', 'handshake', 'happy', 'headbang', 'heart', 'heidy',
      'hi', 'highfive', 'hollest', 'hug', 'idea', 'inlove', 'itwasntme', 'kiss', 'lalala', 'lipssealed', 'mail', 'makeup', 'malthe', 'mmm',
      'mooning', 'movie', 'muscle', 'music', 'nerd', 'nickfury', 'ninja', 'no', 'nod',
      'oliver', 'party', 'phone', 'pizza', 'poolparty', 'priidu', 'puking', 'punch', 'rain', 'rock', 'rofl', 'sadsmile', 'shake', 'sheep',
      'shielddeflect', 'skype', 'sleepy', 'smile', 'smirk', 'smoking', 'speechless',
      'star', 'sunshine', 'surprised', 'swear', 'sweating', 'talking', 'talktothehand', 'tauri', 'thinking', 'time', 'tmi', 'toivo',
      'tongueout', 'tumbleweed', 'wait', 'waiting', 'wfh', 'whew', 'wink', 'wondering', 'worried', 'wtf', 'yawning', 'yes'
    ];

    vm.emojiList = [{"class": "e_1", "name": "bowtie"}, {"class": "e_2", "name": "smile"}, {
      "class": "e_3",
      "name": "simple_smile"
    }, {"class": "e_4", "name": "laughing"}, {"class": "e_5", "name": "blush"}, {
      "class": "e_6",
      "name": "smiley"
    }, {"class": "e_7", "name": "relaxed"}, {"class": "e_8", "name": "smirk"}, {
      "class": "e_9",
      "name": "heart_eyes"
    }, {"class": "e_10", "name": "kissing_heart"}, {"class": "e_11", "name": "kissing_closed_eyes"}, {
      "class": "e_12",
      "name": "flushed"
    }, {"class": "e_13", "name": "relieved"}, {"class": "e_14", "name": "satisfied"}, {
      "class": "e_15",
      "name": "grin"
    }, {"class": "e_16", "name": "wink"}, {"class": "e_17", "name": "stuck_out_tongue_winking_eye"}, {
      "class": "e_18",
      "name": "stuck_out_tongue_closed_eyes"
    }, {"class": "e_19", "name": "grinning"}, {"class": "e_20", "name": "kissing"}, {
      "class": "e_21",
      "name": "kissing_smiling_eyes"
    }, {"class": "e_22", "name": "stuck_out_tongue"}, {"class": "e_23", "name": "sleeping"}, {
      "class": "e_24",
      "name": "worried"
    }, {"class": "e_25", "name": "frowning"}, {"class": "e_26", "name": "anguished"}, {
      "class": "e_27",
      "name": "open_mouth"
    }, {"class": "e_28", "name": "grimacing"}, {"class": "e_29", "name": "confused"}, {
      "class": "e_30",
      "name": "hushed"
    }, {"class": "e_31", "name": "expressionless"}, {"class": "e_32", "name": "unamused"}, {
      "class": "e_33",
      "name": "sweat_smile"
    }, {"class": "e_34", "name": "sweat"}, {"class": "e_35", "name": "disappointed_relieved"}, {
      "class": "e_36",
      "name": "weary"
    }, {"class": "e_37", "name": "pensive"}, {"class": "e_38", "name": "disappointed"}, {
      "class": "e_39",
      "name": "confounded"
    }, {"class": "e_40", "name": "fearful"}, {"class": "e_41", "name": "cold_sweat"}, {
      "class": "e_42",
      "name": "persevere"
    }, {"class": "e_43", "name": "cry"}, {"class": "e_44", "name": "sob"}, {
      "class": "e_45",
      "name": "joy"
    }, {"class": "e_46", "name": "astonished"}, {"class": "e_47", "name": "scream"}, {
      "class": "e_48",
      "name": "neckbeard"
    }, {"class": "e_49", "name": "tired_face"}, {"class": "e_50", "name": "angry"}, {
      "class": "e_51",
      "name": "rage"
    }, {"class": "e_52", "name": "triumph"}, {"class": "e_53", "name": "sleepy"}, {
      "class": "e_54",
      "name": "yum"
    }, {"class": "e_55", "name": "mask"}, {"class": "e_56", "name": "sunglasses"}, {
      "class": "e_57",
      "name": "dizzy_face"
    }, {"class": "e_58", "name": "imp"}, {"class": "e_59", "name": "smiling_imp"}, {
      "class": "e_60",
      "name": "neutral_face"
    }, {"class": "e_61", "name": "no_mouth"}, {"class": "e_62", "name": "innocent"}, {
      "class": "e_63",
      "name": "alien"
    }, {"class": "e_64", "name": "yellow_heart"}, {"class": "e_65", "name": "blue_heart"}, {
      "class": "e_66",
      "name": "purple_heart"
    }, {"class": "e_67", "name": "heart"}, {"class": "e_68", "name": "green_heart"}, {
      "class": "e_69",
      "name": "broken_heart"
    }, {"class": "e_70", "name": "heartbeat"}, {"class": "e_71", "name": "heartpulse"}, {
      "class": "e_72",
      "name": "two_hearts"
    }, {"class": "e_73", "name": "revolving_hearts"}, {"class": "e_74", "name": "cupid"}, {
      "class": "e_75",
      "name": "sparkling_heart"
    }, {"class": "e_76", "name": "sparkles"}, {"class": "e_77", "name": "star"}, {
      "class": "e_78",
      "name": "star2"
    }, {"class": "e_79", "name": "dizzy"}, {"class": "e_80", "name": "boom"}, {
      "class": "e_81",
      "name": "collision"
    }, {"class": "e_82", "name": "anger"}, {"class": "e_83", "name": "exclamation"}, {
      "class": "e_84",
      "name": "question"
    }, {"class": "e_85", "name": "grey_exclamation"}, {"class": "e_86", "name": "grey_question"}, {
      "class": "e_87",
      "name": "zzz"
    }, {"class": "e_88", "name": "dash"}, {"class": "e_89", "name": "sweat_drops"}, {
      "class": "e_90",
      "name": "notes"
    }, {"class": "e_91", "name": "musical_note"}, {"class": "e_92", "name": "fire"}, {
      "class": "e_93",
      "name": "hankey"
    }, {"class": "e_94", "name": "poop"}, {"class": "e_95", "name": "shit"}, {
      "class": "e_96",
      "name": "plus1"
    }, {"class": "e_97", "name": "thumbsup"}, {"class": "e_98", "name": "-1"}, {
      "class": "e_99",
      "name": "thumbsdown"
    }, {"class": "e_100", "name": "ok_hand"}, {"class": "e_101", "name": "punch"}, {
      "class": "e_102",
      "name": "facepunch"
    }, {"class": "e_103", "name": "fist"}, {"class": "e_104", "name": "v"}, {
      "class": "e_105",
      "name": "wave"
    }, {"class": "e_106", "name": "hand"}, {"class": "e_107", "name": "raised_hand"}, {
      "class": "e_108",
      "name": "open_hands"
    }, {"class": "e_109", "name": "point_up"}, {"class": "e_110", "name": "point_down"}, {
      "class": "e_111",
      "name": "point_left"
    }, {"class": "e_112", "name": "point_right"}, {"class": "e_113", "name": "raised_hands"}, {
      "class": "e_114",
      "name": "pray"
    }, {"class": "e_115", "name": "point_up_2"}, {"class": "e_116", "name": "clap"}, {
      "class": "e_117",
      "name": "muscle"
    }, {"class": "e_118", "name": "metal"}, {"class": "e_119", "name": "fu"}, {
      "class": "e_120",
      "name": "runner"
    }, {"class": "e_121", "name": "running"}, {"class": "e_122", "name": "couple"}, {
      "class": "e_123",
      "name": "family"
    }, {"class": "e_124", "name": "two_men_holding_hands"}, {
      "class": "e_125",
      "name": "two_women_holding_hands"
    }, {"class": "e_126", "name": "dancer"}, {"class": "e_127", "name": "dancers"}, {
      "class": "e_128",
      "name": "ok_woman"
    }, {"class": "e_129", "name": "no_good"}, {"class": "e_130", "name": "information_desk_person"}, {
      "class": "e_131",
      "name": "raising_hand"
    }, {"class": "e_132", "name": "bride_with_veil"}, {
      "class": "e_133",
      "name": "person_with_pouting_face"
    }, {"class": "e_134", "name": "person_frowning"}, {"class": "e_135", "name": "bow"}, {
      "class": "e_136",
      "name": "couplekiss"
    }, {"class": "e_137", "name": "couple_with_heart"}, {"class": "e_138", "name": "massage"}, {
      "class": "e_139",
      "name": "haircut"
    }, {"class": "e_140", "name": "nail_care"}, {"class": "e_141", "name": "boy"}, {
      "class": "e_142",
      "name": "girl"
    }, {"class": "e_143", "name": "woman"}, {"class": "e_144", "name": "man"}, {
      "class": "e_145",
      "name": "baby"
    }, {"class": "e_146", "name": "older_woman"}, {"class": "e_147", "name": "older_man"}, {
      "class": "e_148",
      "name": "person_with_blond_hair"
    }, {"class": "e_149", "name": "man_with_gua_pi_mao"}, {
      "class": "e_150",
      "name": "man_with_turban"
    }, {"class": "e_151", "name": "construction_worker"}, {"class": "e_152", "name": "cop"}, {
      "class": "e_153",
      "name": "angel"
    }, {"class": "e_154", "name": "princess"}, {"class": "e_155", "name": "smiley_cat"}, {
      "class": "e_156",
      "name": "smile_cat"
    }, {"class": "e_157", "name": "heart_eyes_cat"}, {"class": "e_158", "name": "kissing_cat"}, {
      "class": "e_159",
      "name": "smirk_cat"
    }, {"class": "e_160", "name": "scream_cat"}, {"class": "e_161", "name": "crying_cat_face"}, {
      "class": "e_162",
      "name": "joy_cat"
    }, {"class": "e_163", "name": "pouting_cat"}, {"class": "e_164", "name": "japanese_ogre"}, {
      "class": "e_165",
      "name": "japanese_goblin"
    }, {"class": "e_166", "name": "see_no_evil"}, {"class": "e_167", "name": "hear_no_evil"}, {
      "class": "e_168",
      "name": "speak_no_evil"
    }, {"class": "e_169", "name": "guardsman"}, {"class": "e_170", "name": "skull"}, {
      "class": "e_171",
      "name": "feet"
    }, {"class": "e_172", "name": "lips"}, {"class": "e_173", "name": "kiss"}, {
      "class": "e_174",
      "name": "droplet"
    }, {"class": "e_175", "name": "ear"}, {"class": "e_176", "name": "eyes"}, {
      "class": "e_177",
      "name": "nose"
    }, {"class": "e_178", "name": "tongue"}, {"class": "e_179", "name": "love_letter"}, {
      "class": "e_180",
      "name": "bust_in_silhouette"
    }, {"class": "e_181", "name": "busts_in_silhouette"}, {
      "class": "e_182",
      "name": "speech_balloon"
    }, {"class": "e_183", "name": "thought_balloon"}, {"class": "e_184", "name": "feelsgood"}, {
      "class": "e_185",
      "name": "finnadie"
    }, {"class": "e_186", "name": "goberserk"}, {"class": "e_187", "name": "godmode"}, {
      "class": "e_188",
      "name": "hurtrealbad"
    }, {"class": "e_189", "name": "rage1"}, {"class": "e_190", "name": "rage2"}, {
      "class": "e_191",
      "name": "rage3"
    }, {"class": "e_192", "name": "rage4"}, {"class": "e_193", "name": "suspect"}, {
      "class": "e_194",
      "name": "trollface"
    }, {"class": "e_195", "name": "sunny"}, {"class": "e_196", "name": "umbrella"}, {
      "class": "e_197",
      "name": "cloud"
    }, {"class": "e_198", "name": "snowflake"}, {"class": "e_199", "name": "snowman"}, {
      "class": "e_200",
      "name": "zap"
    }, {"class": "e_201", "name": "cyclone"}, {"class": "e_202", "name": "foggy"}, {
      "class": "e_203",
      "name": "ocean"
    }, {"class": "e_204", "name": "cat"}, {"class": "e_205", "name": "dog"}, {
      "class": "e_206",
      "name": "mouse"
    }, {"class": "e_207", "name": "hamster"}, {"class": "e_208", "name": "rabbit"}, {
      "class": "e_209",
      "name": "wolf"
    }, {"class": "e_210", "name": "frog"}, {"class": "e_211", "name": "tiger"}, {
      "class": "e_212",
      "name": "koala"
    }, {"class": "e_213", "name": "bear"}, {"class": "e_214", "name": "pig"}, {
      "class": "e_215",
      "name": "pig_nose"
    }, {"class": "e_216", "name": "cow"}, {"class": "e_217", "name": "boar"}, {
      "class": "e_218",
      "name": "monkey_face"
    }, {"class": "e_219", "name": "monkey"}, {"class": "e_220", "name": "horse"}, {
      "class": "e_221",
      "name": "racehorse"
    }, {"class": "e_222", "name": "camel"}, {"class": "e_223", "name": "sheep"}, {
      "class": "e_224",
      "name": "elephant"
    }, {"class": "e_225", "name": "panda_face"}, {"class": "e_226", "name": "snake"}, {
      "class": "e_227",
      "name": "bird"
    }, {"class": "e_228", "name": "baby_chick"}, {"class": "e_229", "name": "hatched_chick"}, {
      "class": "e_230",
      "name": "hatching_chick"
    }, {"class": "e_231", "name": "chicken"}, {"class": "e_232", "name": "penguin"}, {
      "class": "e_233",
      "name": "turtle"
    }, {"class": "e_234", "name": "bug"}, {"class": "e_235", "name": "honeybee"}, {
      "class": "e_236",
      "name": "ant"
    }, {"class": "e_237", "name": "beetle"}, {"class": "e_238", "name": "snail"}, {
      "class": "e_239",
      "name": "octopus"
    }, {"class": "e_240", "name": "tropical_fish"}, {"class": "e_241", "name": "fish"}, {
      "class": "e_242",
      "name": "whale"
    }, {"class": "e_243", "name": "whale2"}, {"class": "e_244", "name": "dolphin"}, {
      "class": "e_245",
      "name": "cow2"
    }, {"class": "e_246", "name": "ram"}, {"class": "e_247", "name": "rat"}, {
      "class": "e_248",
      "name": "water_buffalo"
    }, {"class": "e_249", "name": "tiger2"}, {"class": "e_250", "name": "rabbit2"}, {
      "class": "e_251",
      "name": "dragon"
    }, {"class": "e_252", "name": "goat"}, {"class": "e_253", "name": "rooster"}, {
      "class": "e_254",
      "name": "dog2"
    }, {"class": "e_255", "name": "pig2"}, {"class": "e_256", "name": "mouse2"}, {
      "class": "e_257",
      "name": "ox"
    }, {"class": "e_258", "name": "dragon_face"}, {"class": "e_259", "name": "blowfish"}, {
      "class": "e_260",
      "name": "crocodile"
    }, {"class": "e_261", "name": "dromedary_camel"}, {"class": "e_262", "name": "leopard"}, {
      "class": "e_263",
      "name": "cat2"
    }, {"class": "e_264", "name": "poodle"}, {"class": "e_265", "name": "paw_prints"}, {
      "class": "e_266",
      "name": "bouquet"
    }, {"class": "e_267", "name": "cherry_blossom"}, {"class": "e_268", "name": "tulip"}, {
      "class": "e_269",
      "name": "four_leaf_clover"
    }, {"class": "e_270", "name": "rose"}, {"class": "e_271", "name": "sunflower"}, {
      "class": "e_272",
      "name": "hibiscus"
    }, {"class": "e_273", "name": "maple_leaf"}, {"class": "e_274", "name": "leaves"}, {
      "class": "e_275",
      "name": "fallen_leaf"
    }, {"class": "e_276", "name": "herb"}, {"class": "e_277", "name": "mushroom"}, {
      "class": "e_278",
      "name": "cactus"
    }, {"class": "e_279", "name": "palm_tree"}, {"class": "e_280", "name": "evergreen_tree"}, {
      "class": "e_281",
      "name": "deciduous_tree"
    }, {"class": "e_282", "name": "chestnut"}, {"class": "e_283", "name": "seedling"}, {
      "class": "e_284",
      "name": "blossom"
    }, {"class": "e_285", "name": "ear_of_rice"}, {"class": "e_286", "name": "shell"}, {
      "class": "e_287",
      "name": "globe_with_meridians"
    }, {"class": "e_288", "name": "sun_with_face"}, {
      "class": "e_289",
      "name": "full_moon_with_face"
    }, {"class": "e_290", "name": "new_moon_with_face"}, {"class": "e_291", "name": "new_moon"}, {
      "class": "e_292",
      "name": "waxing_crescent_moon"
    }, {"class": "e_293", "name": "first_quarter_moon"}, {
      "class": "e_294",
      "name": "waxing_gibbous_moon"
    }, {"class": "e_295", "name": "full_moon"}, {"class": "e_296", "name": "waning_gibbous_moon"}, {
      "class": "e_297",
      "name": "last_quarter_moon"
    }, {"class": "e_298", "name": "waning_crescent_moon"}, {
      "class": "e_299",
      "name": "last_quarter_moon_with_face"
    }, {"class": "e_300", "name": "first_quarter_moon_with_face"}, {
      "class": "e_301",
      "name": "crescent_moon"
    }, {"class": "e_302", "name": "earth_africa"}, {"class": "e_303", "name": "earth_americas"}, {
      "class": "e_304",
      "name": "earth_asia"
    }, {"class": "e_305", "name": "volcano"}, {"class": "e_306", "name": "milky_way"}, {
      "class": "e_307",
      "name": "partly_sunny"
    }, {"class": "e_308", "name": "octocat"}, {"class": "e_309", "name": "squirrel"}];


    /**
     * insertAtCaret
     * @return
     * @method insertAtCaret
     * @param text
     * @param areaId
     * @return 
     */
    function insertAtCaret(text, areaId) {
      var txtarea = document.getElementById(areaId);
      if (!txtarea) {
        return;
      }
      var scrollPos = txtarea.scrollTop;
      var strPos = 0;
      var br = ((txtarea.selectionStart || txtarea.selectionStart === '0') ?
        "ff" : (document.selection ? "ie" : false ) );
      if (br === "ie") {
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart('character', -txtarea.value.length);
        strPos = range.text.length;
      } else if (br === "ff") {
        strPos = txtarea.selectionStart;
      }

      var front = (txtarea.value).substring(0, strPos);
      var back = (txtarea.value).substring(strPos, txtarea.value.length);
      txtarea.value = front + text + back;
      strPos = strPos + text.length;
      if (br === "ie") {
        txtarea.focus();
        var ieRange = document.selection.createRange();
        ieRange.moveStart('character', -txtarea.value.length);
        ieRange.moveStart('character', strPos);
        ieRange.moveEnd('character', 0);
        ieRange.select();
      } else if (br === "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
      }

      txtarea.scrollTop = scrollPos;
      txtarea.focus();

      vm.replyMessage = txtarea.value;

    }


    /**
     * Reply
     * @return
     * @method reply
     * @param {} $event
     * @return 
     */
    function reply($event) {
      // If "shift + enter" pressed, grow the reply textarea
      if ($event && $event.keyCode === 13 && $event.shiftKey) {
        vm.textareaGrow = true;
        return;
      }

      // Prevent the reply() for key presses rather than the"enter" key.
      if ($event && $event.keyCode !== 13) {
        return;
      }

      // Check for empty messages
      if (vm.replyMessage === '') {
        resetReplyTextarea();
        return;
      }

      var replyMessage = checkEmoticonMessage(vm.replyMessage);

      updateChatData(replyMessage, 'chat');

      // Reset the reply textarea


    }

    /**
     * Description
     * @return
     * @method updateChatData
     * @param {} data
     * @param {} type
     * @return 
     */
    function updateChatData(data, type) {
      // Message Data
      var messageData = {};
      messageData.sender = vm.user.id;
      messageData.conversation = vm.chatConversationId;
      messageData.message_type = type || 'chat';
      messageData.body = data;
      var last_message;

      if (type === 'sticker') {
        last_message = 'Send a Sticker';
      } else if (type === 'giphy') {
        last_message = 'Send a Giphy';
      } else if (type === 'attachment') {
        last_message = 'Send an Attachment';
      } else {
        last_message = data.replace('1026' +
          '', '');
      }


      // Message
      var message = {
        sender: vm.user,
        body: data,
        conversation: vm.chatConversationId,
        message_type: type || 'chat',
        createdAt: new Date().toISOString(),
        updatedAt: new Date().toISOString()
      };


      // Add the message to the chat
      vm.chat.messages.push(message);


      if (vm.chatConversationId) {

        ChatsService.addMessage(messageData).then(function (messageResp) {});

        vm.conversations.getById(vm.chatConversationId).last_message = last_message;
        vm.conversations.getById(vm.chatConversationId).unread[vm.user.id] = 0;
        vm.conversations.getById(vm.chatConversationId).last_message_time = new Date().toISOString();

      } else {
        vm.tabCurrentTab = vm.chat.group ? "group" : "chat";
        messageData.recipients = vm.chat.recipients;
        ChatsService.addMessage(messageData).then(function (messageResp) {
          vm.conversations.push(messageResp);
          vm.chatConversationId = messageResp.id;
        });
      }

      makeUnreadEmpty(vm.chatConversationId);



      resetReplyTextarea();

      // Scroll to the new message
      scrollToBottomOfChat();
    }

    /**
     * Clear Chat Messages
     * @return
     * @method clearMessages
     * @return 
     */
    function clearMessages() {
      vm.chats[vm.chatContactId] = vm.chat = [];
      vm.contacts.getById(vm.chatContactId).lastMessage = null;
    }

    /**
     * Description
     * @return
     * @method showEmoArea
     * @return 
     */
    function showEmoArea() {
      if (vm.emoAreaVisible === true) {
        vm.emoAreaVisible = false;
      } else {
        vm.emoAreaVisible = true;
        vm.showEmojiList = false;
        vm.showEmoStickerList = false;
        vm.showEmoticonList = true;
      }
    }

    /**
     * Description
     * @return
     * @method showEmojiArea
     * @return 
     */
    function showEmojiArea() {
      vm.showEmoticonList = false;
      vm.showEmoStickerList = false;
      vm.showEmojiList = true;
    }

    /**
     * Description
     * @return
     * @method showEmoticonArea
     * @return 
     */
    function showEmoticonArea() {
      vm.showEmojiList = false;
      vm.showEmoStickerList = false;
      vm.showEmoticonList = true;
    }


    /**
     * Description
     * @return
     * @method showEmoStickerArea
     * @return 
     */
    function showEmoStickerArea() {
      vm.showEmojiList = false;
      vm.showEmoticonList = false;
      vm.showEmoStickerList = true;
      ChatsService.findGiphyStickerTrending(48, 0).then(function (resp) {
        vm.emoStickerList = resp;
      });
    }

    // for user offline
    io.socket.on('new_message', function onServerSentEvent( message ) {
      $scope.$apply(function(){
        vm.conversations.getById(message.data.conversation).last_message = message.data.body;
        vm.conversations.getById(message.data.conversation).last_message_time = new Date().toISOString();
        vm.conversations.getById(message.data.conversation).unread[vm.user.id] = vm.conversations.getById(message.data.conversation).unread[vm.user.id] + 1;
        // $rootScope.unreadCountTotal = $rootScope.unreadCountTotal + 1;
        if (vm.chatConversationId && message.data.conversation === vm.chatConversationId) {
          $rootScope.getGlobalNotificationCount();
          vm.chat.messages.push( message.data );
          scrollToBottomOfChat();
        }
      });
    });

    // for user offline
    io.socket.on('new_conversation', function onServerSentEvent( conversation ) {
      if (conversation.created_by !== vm.user.id) {
        conversations();
        if (conversation.group === true) {
          vm.tabCurrentTab = 'group';
        }
      }
    });


    io.socket.on('user_mode', function onServerSentEvent(user) {
      if( user && user.id !== vm.user.id ){
        $scope.$apply(function(){ vm.contacts.getById(user.id).mode = user.mode;});
        if( vm.chatReceipient &&  vm.chatReceipient.id && vm.chatReceipient.id === user.id ){
          vm.chatReceipient.mode = user.mode || '';
        }
        changeConversationUserMode(user);
      }
    });

    /**
     * Description
     * @method changeConversationUserMode
     * @param {} user
     * @return 
     */
    function changeConversationUserMode( user ){
      var getUserChats = vm.conversations.filter(function (chat) {
        return !chat.group && chat.users.indexOf(user.id) > -1;
      });

      getUserChats.forEach(function ( chat ) {
        var userIndex;
        chat.recipients.forEach(function (val, key) {
          if( val.id === user.id ) userIndex = key;
        });
        $scope.$apply(function(){
          vm.conversations.getById(chat.id).recipients[userIndex].mode = user.mode;
        });
      });
    }



    /*---------------------------------------------------- Giphy Search Functionality ---------------------------------*/
    /************************************** Giphy Search Functinoality ******************************************/
    //setup before functions
    var giphyTypingTimer;                //timer identifier
    var giphyDoneTypingInterval = 2000;  //time in ms, 5 second for example

    /**
     * Description
     * @return
     * @method giphyStickerSearch
     * @return 
     */
    function giphyStickerSearch() {
      ChatsService.findGiphyStickerSearch(45, 0, [vm.emo_search]).then(function (resp) {
        vm.emoStickerList = resp;
      });
    }

    /**
     * Description
     * @return
     * @method giphyStickerTimoutInterval
     * @return 
     */
    function giphyStickerTimoutInterval() {
      $timeout.cancel(giphyTypingTimer);
      if (vm.emo_search.length) {
        giphyTypingTimer = $timeout(giphyStickerSearch, giphyDoneTypingInterval);
      }
    }

    /**
     * Description
     * @return
     * @method giphyClearTimoutInterval
     * @return 
     */
    function giphyClearTimoutInterval() {
      $timeout.cancel(giphyTypingTimer);
    }


    /************************************** # End * Giphy Search Functinoality ***********************************/
    /**
     * ---------------------------------------------------- Giphy Search Functionality ---------------------------------
     * @method checkEmoticonMessage
     * @param {} message
     * @return message
     */
    function checkEmoticonMessage(message) {

      var regex = /\(([^)]+)\)/g;
      var regex_emoji = /\{([^)]+)\}/g;

      var match = regex.exec(message);
      while (match !== null) {

        if (vm.emoticonList.indexOf(match[1]) !== -1) {
          message = message.replace(match[0], '<i class="sc sc-' + match[1] + ' sc-2x"></i>');
        }
        match = regex.exec(message);

      }

      var match_emoji = regex_emoji.exec(message);
      while (match_emoji !== null) {

        vm.emojiList.forEach(function (val) {
          if (val.name === match_emoji[1]) {
            var baseUrl = $location.protocol() + '://' + $location.host();
            var iconUrl = '<img class="imoji_2x" src="' + baseUrl + '/vendors/emoji-cheat-sheet.com/public/graphics/emojis/' + match_emoji[1] + '.png' + '" alt="' + match_emoji[1] + '">';
            message = message.replace(match_emoji[0], iconUrl);
          }
        });

        match_emoji = regex_emoji.exec(message);
      }
      return message;

    }


    /**
     * Description
     * @method insertEmoticonToMessageBody
     * @param {} icon
     * @return 
     */
    function insertEmoticonToMessageBody(icon) {
      var formatIcon = '(' + icon + ')';
      insertAtCaret(formatIcon, "reply");
      vm.emoAreaVisible = false;
      return;
    }


    /**
     * Description
     * @return
     * @method insertEmojiToMessageBody
     * @param {} emoji
     * @return 
     */
    function insertEmojiToMessageBody(emoji) {
      insertAtCaret('{' + emoji + '}', 'reply');
      vm.emoAreaVisible = false;
    }

    /**
     * Description
     * @return
     * @method insertEmojiStickerToChat
     * @param {} giphy
     * @return 
     */
    function insertEmojiStickerToChat(giphy) {
      vm.emoAreaVisible = false;
      vm.showEmoticonList = false;
      vm.showEmoStickerList = false;
      vm.showEmojiList = false;
      var url = '<div class="giphy_sticker"><img src="' + giphy + '" alt="" /><img alt="Powered by Giphy" src="images/chat/powered_by_giphy_white_bg_27.png" style="width: 51px;margin-top: 9px;margin-left: 5px;"></div>';
      updateChatData(url, 'sticker');
    }

    /**
     * Reset reply textarea
     * @return
     * @method resetReplyTextarea
     * @return 
     */
    function resetReplyTextarea() {
      vm.replyMessage = '';
      vm.textareaGrow = false;
    }

    /**
     * Scroll Chat Content to the bottom
     * @return
     * @method scrollToBottomOfChat
     * @return 
     */
    function scrollToBottomOfChat() {
      $timeout(function () {
        var chatContent = angular.element($document.find('#chat-content, #chat-dialog'));
        if( chatContent && chatContent.length ){
          chatContent.animate({
            scrollTop: chatContent[0].scrollHeight
          }, 400);
        }

      }, 0);

    }

    /**
     * Description
     * @method showMessage
     * @param {} message
     * @return 
     */
    function showMessage(message) {
      $mdToast.show({
        template: '<md-toast class="md-toast success">' + message + '</md-toast>',
        hideDelay: 3000,
        position: 'bottom left'
      });
    }

    /**
     * Description
     * @method toggleFavoriteContact
     * @param {} item
     * @param {} status
     * @return 
     */
    function toggleFavoriteContact(item, status) {
      if (status === false) {
        ContactService.updateFavorites(item).then(function (resp) {
          if (resp) {
            var message = 'The selected contact set as your favorite contact.';
            showMessage(message);
            vm.loadContact();
          }
        });
      } else {
        ContactService.removeFavorites(item).then(function (resp) {
          if (resp) {
            var message = 'The selected contact remove from your favorite contact.';
            showMessage(message);
            vm.loadContact();
          }
        });
      }
    }

    /**
     * Description
     * @method closeDialog
     * @return 
     */
    function closeDialog() {
      $mdDialog.hide();
    }

    /**
     * Set User Status
     * @return
     * @method setUserStatus
     * @param {} status
     * @return 
     */
    function setUserStatus(status) {
      vm.user.status = status;
    }

    /**
     * Toggle sidenav
     * @return
     * @method toggleSidenav
     * @param sidenavId
     * @return 
     */
    function toggleSidenav(sidenavId) {
      $mdSidenav(sidenavId).toggle();
    }

    /**
     * Toggle Left Sidenav View
     * @return
     * @method toggleLeftSidenavView
     * @param {} id
     * @return 
     */
    function toggleLeftSidenavView(id) {
      vm.leftSidenavView = id;
    }

    /**
     * Description
     * @return
     * @method viewChatTab
     * @param {} tab
     * @return 
     */
    function viewChatTab(tab) {
      vm.tabCurrentTab = tab;
      vm.chatSearch = '';
    }

    /**
     * Array prototype
     * Except by ID
     * @method filterExceptId
     * @param value
     * @return CallExpression
     */
    Array.prototype.filterExceptId = function (value) {
      return this.map(function (x) {
        if (x.id !== value) {
          return x;
        }
      });
    };

    /**
     * Array prototype
     * Get by id
     * @method getById
     * @param value
     * @return MemberExpression
     */
    Array.prototype.getById = function (value) {
      return this.filter(function (x) {
        return x.id === value;
      })[0];
    };


    /**
     * Description
     * @method getConversatinoByRespId
     * @param {} chats
     * @param {} userId
     * @return MemberExpression
     */
    Array.prototype.getConversatinoByRespId = function (chats, userId) {
      return chats.filter(function (c) {
        return c.recipients.filter(function (r) {
          return r.id = userId;
        });
      })[0];
    };

    // Methods
    vm.toggleChat = toggleChat;
    vm.reply = reply;


    /**
     * Description
     * @method toggleChat
     * @param {} conversation
     * @return 
     */
    function toggleChat(conversation) {
      vm.replyMessage = '';
      if (conversation.created_by !== undefined) {
        vm.chat = getChatByConversationID(conversation.id);
      } else {
        vm.chat = getChatByConversationContact(conversation.id);
      }
      scrollToBottomOfChat(0);
    }

  }
})();



