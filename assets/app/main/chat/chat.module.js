(function () {
  'use strict';

  angular
    .module('app.chat', [])
    .config(config);

  /**
   * Description
   * @ngInject
   * @method config
   * @param {} $stateProvider
   * @return
   */
  function config($stateProvider) {

    // State
    $stateProvider.state('app.chat', {
      url: '/chat',
      views: {
        'content@app': {
          templateUrl: 'app/main/chat/chat.html',
          controller: 'ChatController as vm'
        }
      }
    }).state('app.chat.type', {
      url: '/:type'
    }).state('app.chat.type.id', {
      url: '/:typeId'
    });


    // Api
    // Contacts data must be alphabatically ordered.


  }

})();
