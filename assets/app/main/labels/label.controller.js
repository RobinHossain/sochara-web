(function () {
  'use strict';

  angular
    .module('app.labels')
    .controller('LabelsController', LabelsController).directive('andyDraggable', function () {
    return {
      restrict: 'A',
      link: function (scope, elm, attrs) {
        var options = scope.$eval(attrs.andyDraggable); //allow options to be passed in
        elm.draggable(options);
      }
    };
  });

  /* @ngInject */
  function LabelsController($scope, $mdSidenav, $http, $rootScope, $sce, $location,  Upload, $filter, LabelService, $state, $timeout, $mdToast, Auth, msUtils, $mdDialog, $document, ContactService) {

    var vm = this;
    vm.site_url = $location.protocol() + '://' + $location.host() + ':' + $location.port();
    /////////////
    vm.contentOfReturn = '';
    vm.contentOfShipping = '';
    // vm.contactsec = Contactsec.data;
    // vm.user = User.data;
    vm.user = $rootScope.user;
    vm.filterIds = null;
    vm.listType = 'all';
    vm.listOrder = 'name';
    vm.listOrderAsc = false;
    vm.selectedContacts = [];
    vm.newGroupName = '';
    vm.labelCorRadiusClr = '#0098eb';
    vm.labelGradientFirst = '#0098eb';
    vm.labelGradientSecond = '#02c9ce';
    vm.labelBGImage = '';
    vm.labelTextAlignLeft = 'left';
    // Methods
    vm.filterChange = filterChange;
    vm.openContactDialog = openContactDialog;
    vm.openAddContactDialog = openAddContactDialog;
    vm.openImportExportContactDialog = openImportExportContactDialog;
    vm.openNewGroupContactDialog = openNewGroupContactDialog;
    vm.openManageGroupContactDialog = openManageGroupContactDialog;
    vm.deleteContactConfirm = deleteContactConfirm;
    vm.deleteContact = deleteContact;
    vm.deleteSelectedContacts = deleteSelectedContacts;
    vm.toggleSelectContact = toggleSelectContact;
    vm.deselectContacts = deselectContacts;
    vm.selectAllContacts = selectAllContacts;
    vm.deleteContact = deleteContact;
    vm.intToArray = intToArray;
    vm.addNewGroup = addNewGroup;
    vm.deleteGroup = deleteGroup;
    vm.toggleSidenav = toggleSidenav;
    vm.loadGroupsNameforExport = loadGroupsNameforExport;
    vm.toggleInArray = msUtils.toggleInArray;
    vm.exists = msUtils.exists;
    vm.isInsertImage = false;
    vm.isInsertextText = false;
    vm.loadContact = loadContact;
    vm.labelBGColorChange = labelBGColorChange;
    vm.changeGradientColor = changeGradientColor;
    vm.clearOtherBackground = clearOtherBackground;
    vm.removeInsertedText = removeInsertedText;
    vm.removeInsertedImage = removeInsertedImage;
    vm.addlayerImage = addlayerImage;
    vm.clickLayerImage = clickLayerImage;
    vm.insertImageLayer = insertImageLayer;
    vm.getBackgroundImageAcceptRatio = getBackgroundImageAcceptRatio;
    vm.saveCurrentDesign = saveCurrentDesign;
    vm.saveLabel = saveCurrentDesign;
    vm.removeLabel = removeLabel;
    vm.changeLabelFontStyle = changeLabelFontStyle;

    // angular.element("#labelfontstyle").change(function () {
    //   var font = angular.element(this).val().replace(/\+/g, ' ');
    //   var selectedInputId = checkSelectedInput();
    //   if(selectedInputId){
    //     angular.element('#textInput'+selectedInputId).css('font-family', font);
    //     pushOrSliceObject('font', selectedInputId, font);
    //   }else{
    //     angular.element('#labelEditorArea').css('font-family', font);
    //     vm.label.styles.main['font-family'] = font;
    //   }
    // });

    function changeLabelFontStyle(font){
      var selectedInputId = checkSelectedInput();
      font = font.name;
      if(selectedInputId){
        angular.element('#textInput'+selectedInputId).css('font-family', font);
        pushOrSliceObject('font', selectedInputId, font);
      }else{
        angular.element('#labelEditorArea').css('font-family', font);
        vm.label.styles.main['font-family'] = font;
      }
    }


    //fav contacts
    var favoriteContactsIds = [];
    ContactService.getFavorites().success(function (resp) {
      if (typeof resp.msg.contactIds !== 'undefined') {
        favoriteContactsIds = resp.msg.contactIds;
      }
      else {
        favoriteContactsIds = [];
      }
    }).error(function () {
      favoriteContactsIds = [];
    });

    function intToArray(item) {
      var array = [];
      for (var i = 0; i < item; i++) {
        array.push(i);
      }
      return array;
    }

    vm.gradientOptions = [
      {values: "top", field: "Lighter from the top"},
      {values: "right", field: "Lighter from the right"},
      {values: "left", field: "Lighter from the left"},
      {values: "bottom", field: "Lighter from the bottom"},
    ];
    vm.isPreviewActive = false;
    //////////
    /*
     * Change Contactsec List Filter
     * @param type
     */
    // Data
    function loadContact() {
      ContactService.getContacts().then(function (data) {
        vm.loadingContacts = false;
        vm.contacts = data;
        vm.contactShow = data[0];
      });
    }
    vm.loadContact();//hit the function

    function loadGroupsNameforExport() {
      // Use timeout to simulate a 650ms request.
      return $timeout(function () {
        Auth.getGroups().success(function (resp) {
          vm.groupsNameforExport = resp.msg;
        }).error(function () {
        });

        // console.log( $scope.groupsNameforExport)
      }, 650);
    }

    vm.closeDialog = closeDialog;

    function closeDialog() {
      $mdDialog.hide();
    }

    function filterChange(type) {

      vm.listType = type;

      if (type === 'all') {
        vm.filterIds = null;
      }
      else if (type === 'frequent') {
        vm.filterIds = vm.user.frequentContacts;
      }
      else if (type === 'starred') {
        vm.filterIds = vm.user.starred;
      }
      else if (angular.isObject(type)) {
        vm.filterIds = type.contactIds;
      }

      vm.selectedContacts = [];

    }

    /*
     * Open new contact dialog
     *
     * @param ev
     * @param contact
     */
    function openAddContactDialog(ev, contact) {
      $mdDialog.show({
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/contactsec/dialogs/contact/contact-add-dialog.html',
        clickOutsideToClose: true,
        targetEvent: ev,
      });
    }

    /*
     * Open new contact dialog
     *
     * @param ev
     * @param contact
     */
    function openImportExportContactDialog(ev, contact) {
      $mdDialog.show({
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/contactsec/dialogs/contact/contact-import-export-dialog.html',
        clickOutsideToClose: true,
        targetEvent: ev,
      });
    }

    /*
     * Open new contact dialog
     *
     * @param ev
     * @param contact
     */
    function openNewGroupContactDialog(ev, contact) {
      $mdDialog.show({
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/contactsec/dialogs/contact/contact-new-group-dialog.html',
        clickOutsideToClose: true,
        targetEvent: ev,
      });
    }

    function changeGradientColor(){
      vm.label.styles.main['background-image'] = 'inherit';
      vm.label.styles.main['background-color'] = 'inherit';
      vm.label.styles.main['background'] = '-webkit-linear-gradient(' + vm.label.styles.gradient.type + ',' +
        vm.label.styles.gradient.c1 + ',' + vm.label.styles.gradient.c2 + ')';
      // angular.element('#labelEditorArea').css({'background': vm.label.styles.main['background-color']});
    }


    function clearOtherBackground(){
      vm.label.styles.isBgColor = false;
      vm.label.styles.isGradient = false;
      vm.label.styles.isBgImage = true;
    }

    /*
     * Open new contact dialog
     *
     * @param ev
     * @param contact
     */
    function openManageGroupContactDialog(ev, contact) {
      $mdDialog.show({
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/contactsec/dialogs/contact/contact-manage-group-dialog.html',
        clickOutsideToClose: true,
        targetEvent: ev,
      });
    }

    /*
     * Open new contact dialog
     *
     * @param ev
     * @param contact
     */
    function openContactDialog(ev, contact) {
      $mdDialog.show({
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/contactsec/dialogs/contact/contact-dialog.html',
        clickOutsideToClose: true,
        targetEvent: ev,
      });
    }

    /*
     * Delete Contact Confirm Dialog
     */
    function deleteContactConfirm(contact, ev) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure want to delete the contact?')
        .htmlContent('<b>' + contact.name + ' ' + contact.lastName + '</b>' + ' will be deleted.')
        .ariaLabel('delete contact')
        .targetEvent(ev)
        .ok('OK')
        .cancel('CANCEL');

      $mdDialog.show(confirm).then(function () {

        deleteContact(contact);
        vm.selectedContacts = [];

      }, function () {

      });
    }

    function init(){
      initLabelObjects();
      getCurrentUser();
      loadExistingLabels();
      // loadFontSelector();
    }

    init();

    function initLabelObjects() {
      vm.labelFontSize = {small: 12, normal: 18, large: 20, extra_large: 22};
      vm.label = {color: '#000000', layers: [], user: {}};
      vm.label.styles = {
        main: {'background-color': '#ffffff', color: '#000000', 'font-size': '16px', 'font-weight': '400',
          'background-image': 'inherit', 'font-style': 'normal', 'font-family': 'arial', 'text-align': 'left',
          'text-decoration': 'none', 'border-radius': 0, 'background': 'inherit'},
        bgColor: 'transparent',
        radius: 0,
        gradient: {type: 'top', c1: '#e73333', c2:'#d96d6d'},
        isGradient: false,
        isBgColor: false,
        isBgImage: false,
        bold: [],
        italic: [],
        underline: [],
        color: {},
        align: {},
        font: {},
        fontSize: {}
      };
    }

    function loadFontSelector(){
      angular.element("#labelfontstyle").fontselect({lookahead: 20});
    }

    vm.changeLabelRadius = changeLabelRadius;
    function changeLabelRadius(bRadius){
      vm.label.styles.main['border-radius'] = bRadius + 'px';
      // angular.element('#labelEditorArea').css({'border-radius': vm.label.styles.radius});
      // angular.element('#labelEditorArea').css(vm.label.styles.main);
    }

    /*
     * Delete Contact
     */
    function deleteContact(contact) {
      // vm.contactsec.splice(vm.contactsec.indexOf(contact), 1);
    }

    /*
     * Delete Selected Contactsec
     */
    function deleteSelectedContacts(ev) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure want to delete the selected contactsec?')
        .htmlContent('<b>' + vm.selectedContacts.length + ' selected</b>' + ' will be deleted.')
        .ariaLabel('delete contactsec')
        .targetEvent(ev)
        .ok('OK')
        .cancel('CANCEL');

      $mdDialog.show(confirm).then(function () {

        vm.selectedContacts.forEach(function (contact) {
          deleteContact(contact);
        });

        vm.selectedContacts = [];

      });

    }

    /*
     * Toggle selected status of the contact
     *
     * @param contact
     * @param event
     */
    function toggleSelectContact(contact, event) {
      if (event) {
        event.stopPropagation();
      }

      if (vm.selectedContacts.indexOf(contact) > -1) {
        vm.selectedContacts.splice(vm.selectedContacts.indexOf(contact), 1);
      }
      else {
        vm.selectedContacts.push(contact);
      }
    }

    /*
     * Deselect contactsec
     */
    function deselectContacts() {
      vm.selectedContacts = [];
    }

    /*
     * Sselect all contactsec
     */
    function selectAllContacts() {
      if (vm.selectAllContact) {
        vm.selectedContacts = [];
      } else {
        vm.selectedContacts = $scope.filteredContacts;
        vm.selectAllContact = true;
      }

    }

    /*
     *
     */
    function addNewGroup() {
      if (vm.newGroupName === '') {
        return;
      }

      var newGroup = {
        'id': msUtils.guidGenerator(),
        'name': vm.newGroupName,
        'contactIds': []
      };

      vm.user.groups.push(newGroup);
      vm.newGroupName = '';
    }

    /*
     * Delete Group
     */
    function deleteGroup(ev) {
      var group = vm.listType;

      var confirm = $mdDialog.confirm()
        .title('Are you sure want to delete the group?')
        .htmlContent('<b>' + group.name + '</b>' + ' will be deleted.')
        .ariaLabel('delete group')
        .targetEvent(ev)
        .ok('OK')
        .cancel('CANCEL');

      $mdDialog.show(confirm).then(function () {

        vm.user.groups.splice(vm.user.groups.indexOf(group), 1);

        filterChange('all');
      });

    }

    /*
     * Toggle sidenav
     *
     * @param sidenavId
     */
    function toggleSidenav(sidenavId) {
      $mdSidenav(sidenavId).toggle();
    }

    /********************************Mahdi Stars here****************************************/

    //Defining Avery Label Template sizes
    //Link : http://www.avery.com/weprint/landing-labels.shtml
    vm.labelLayoutList = [
      {id: "tpl-1", value: "2\" x 4\" Label A4", printCol: 2, printRow: 5},
      {id: "tpl-2", value: "⅔\" x 1¾\" Label A4", printCol: 4, printRow: 15},
      {id: "tpl-3", value: "1\" x 2⅝\" Label A4", printCol: 3, printRow: 10},
      {id: "tpl-4", value: "3 ⅓\" x 4\" Label A4", printCol: 2, printRow: 3},
      {id: "tpl-5", value: "4\" x 6\" Label A4", printCol: 1, printRow: 1}
    ];
    vm.amountOfReturnAddress = 1;
    //Set first Template at load
    vm.labelLayout = "tpl-1";


    function getCurrentUser(){
      Auth.info().then(function (resp) {
        // console.log(resp);
        vm.labelAddressType = 'return';
        vm.label.user.name = resp.first_name + (resp.middle_name ? ' ' + resp.middle_name : '') + (resp.last_name ? ' ' + resp.last_name : '');
        vm.label.user.address = resp.address[0].address.city + ' ' + resp.address[0].address.state + ' ' + resp.address[0].address.zip;
        vm.label.styles.main['background-color'] = 'rgb(255, 255, 255)';
        angular.element('#labelEditorArea').addClass('tpl-1');
        angular.element('#labelEditorArea').html('<span class="labelUserName" style="font-size:18px;">' + vm.label.user.name + '</span><br>' + '<span class="labelUserAddress" style="font-size:16px;">' + vm.label.user.address + '</span>');
      });
    }


    function loadExistingLabels() {
      LabelService.loadExistingLabel().then(function (resp) {
        vm.savedLabels = resp;
      });
    }

    vm.changedLabelLayout = function () {
      angular.element('#labelEditorArea').removeClass('tpl-1');
      angular.element('#labelEditorArea').removeClass('tpl-2');
      angular.element('#labelEditorArea').removeClass('tpl-3');
      angular.element('#labelEditorArea').removeClass('tpl-4');
      angular.element('#labelEditorArea').removeClass('tpl-5');
      angular.element('#labelEditorArea').addClass(vm.labelLayout);
    };


    function labelBGColorChange(color) {
      vm.label.styles.isBgImage = false;
      vm.changeBgImage = null;
      vm.label.styles.isGradient = false;
      vm.label.styles.isBgColor = true;
      vm.label.styles.main['background-image'] = 'inherit';
      if(color){
        vm.label.styles.main['background'] = color;
      }

    }

    /*Cropped Image*/
    vm.myImage = '';
    vm.myCroppedImage = '';

    var handleFileSelect = function (evt) {
      // console.log(evt);
      // vm.myImage = '';
      // vm.myCroppedImage = '';
      var file = angular.element(document.querySelector('#fileInput'))[0].files[0];
      if (file.type === 'image/jpeg' || file.type === 'image/png') {
        vm.uploadButton = true;
        var reader = new FileReader();
        reader.onload = function (evt) {
          $scope.$apply(function ($scope) {
            vm.myImage = evt.target.result;
          });
        };
        reader.readAsDataURL(file);
      }
      else {
        vm.myCroppedImage = '';
      }
    };
    vm.tmpAvatar = '';
    vm.showAvatarImage = handleFileSelect;
    vm.saveAvatar = function () {
      // console.log(vm.myCroppedImage)
      angular.element('#labelEditorArea').css({"background-image": "url('" + vm.myCroppedImage + "')"});
      // vm.labelBGImage = '';
      vm.labelBGImage = vm.myCroppedImage;
      // vm.myImageInsertImage = true;
      $.modal.close();
    };

    vm.removeLabelBGImage = removeLabelBGImage;

    function removeLabelBGImage() {
      vm.label.styles.isBgImage = false;
      vm.changeBgImage = null;
      vm.label.styles.bgImage = null;
    }

    /*CRUD*/
    // vm.saveLabel = function () {
    //   if (!vm.labelSavingName) {
    //     return false;
    //   }
    //   html2canvas(angular.element('#labelEditorArea'),
    //     {
    //       onrendered: function (canvas) {
    //         // toDataURL defaults to png, so we need to request a jpeg, then convert for file download.
    //         var data = {};
    //         data.name = vm.labelSavingName;
    //         if (vm.labelAddressType === 'return') {
    //           data.addressType = 'return';
    //         } else {
    //           data.addressType = 'shipping';
    //           data.shippingAddress = vm.contacts;
    //         }
    //         data.labelPreview = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
    //         data.layout = vm.labelLayout;
    //         data.html = angular.element("#labelEditorArea").html();
    //         data.labelRadius = vm.labelRadius;
    //         data.labelBGColor = vm.label.styles.bgColor;
    //         data.labelBGImage = vm.labelBGImage;
    //         if (vm.isInsertImage) {
    //           data.isInsertImage = true;
    //         } else {
    //           data.isInsertImage = false;
    //         }
    //         if (vm.isInsertextText) {
    //           data.isInsertextText = true;
    //         }
    //         else {
    //           data.isInsertextText = false;
    //         }
    //         data.editorCSS = angular.element("#labelEditorArea").attr('style');
    //         Auth.createLabel(data).success(function (resp) {
    //           $mdToast.show({
    //             template: '<md-toast class="md-toast success">' + "Label saved successfully" + '</md-toast>',
    //             hideDelay: 3000,
    //             position: 'bottom left'
    //           });
    //           $state.reload();
    //         }).error(function (err) {
    //           $mdToast.show({
    //             template: '<md-toast class="md-toast error">' + "Something went wrong" + '</md-toast>',
    //             hideDelay: 3000,
    //             position: 'bottom left'
    //           });
    //         });
    //       }, background: '#fff'
    //     });
    //
    //
    // };
    vm.newLabel = function (ev) {
      if (vm.currentLabelUpdateId) {
        var confirm = $mdDialog.confirm()
          .title('Do you want to save label "' + vm.currentLabelName + '"')
          .htmlContent('<b>' + vm.currentLabelName + '</b>' + ' will be saved.')
          .ariaLabel('Save Label')
          .targetEvent()
          .ok('Save')
          .cancel('Discard');
        $mdDialog.show(confirm).then(function () {
          $mdDialog.show({
            controller: function () {
              return vm;
            },
            controllerAs: 'vm',
            templateUrl: 'app/main/labels/dialogs/contact/label_save.html',
            clickOutsideToClose: true,
            // targetEvent: ev,
          });
        }, function () {
          freshLabel();
        });
      } else {
        var confirm = $mdDialog.confirm()
          .title('Do you want to save unsaved label?')
          .htmlContent('Current Label' + ' will be saved.')
          .ariaLabel('Save Label')
          .targetEvent()
          .ok('Save')
          .cancel('Discard');
        $mdDialog.show(confirm).then(function () {
          showDialogue('app/main/labels/dialogs/contact/label_save_new.html');
        }, function () {
          freshLabel();
        });
      }
    };
    // angular.element("#draggable").draggable();
    function freshLabel() {
      initLabelObjects();
      getCurrentUser();
      vm.currentLabelName = '';
      vm.currentLabelUpdateId = '';
      vm.labelLayout = 'tpl-1';
      vm.labelRadius = 0;
      vm.labelAddressType = 'return';
      vm.isInsertImage = false;
      vm.changedLabelLayout();
      vm.removeLabelBGImage();
      changeLabelRadius()
      labelBGColorChange();

      angular.element('#labelEditorArea').html('<span class="labelUserName" style="font-size:18px;">' + vm.label.user.name + '</span><br>'
        + '<span class="labelUserAddress" style="font-size:16px;">' + vm.label.user.address + '</span>');
      angular.element('#labelEditorArea').attr('style', 'border:1px solid gainsboro;');

    }

    vm.labelZoomPreview = function (layout) {
      if (layout === 'tpl-1') {
        return 'zoom:.5;';
      }
      if (layout === 'tpl-4') {
        return 'zoom:.4;';
      }
      if (layout === 'tpl-5') {
        return 'zoom:.3;';
      }
    };

    function setLayerImage (layer){
      angular.element('#labelEditorArea').append('<div id="resizableImage' + layer.id + '" class="resizable_image" style="background-image:url(' + layer.src + ');">' +
        '</div>');
      angular.element('#resizableImage' + layer.id).css({left: layer.pos.left, top: layer.pos.top});
      angular.element('#resizableImage' + layer.id).draggable({
        containment: angular.element('#labelEditorArea'),
        drag: function(){
          vm.label.layers.getBy('id', layer.id).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
        }
      }).resizable({
        aspectRatio: true
      });
    }

    function setLayerText(layer){
      angular.element('#labelEditorArea').append(' <div class="draggable resizable_text" id="insertedText' + layer.id + '" style="text-align:left;">\n' +
        '            <input type="text" id="textInput'+ layer.id +'" class="tool txt_input" style="font-size:14px;text-align:left;background: transparent" value="' + layer.value + '" />\n' +
        '          </div>');
      angular.element("#insertedText"+layer.id).css({top: layer.pos.top, left: layer.pos.left});
      angular.element('#insertedText'+layer.id).draggable({
        containment: angular.element('#labelEditorArea'),
        drag: function(){
          vm.label.layers.getBy('id', layer.id).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
        }
      }).resizable();
    }

    function removeExistingLayer(selector){
      if(selector.length){
        selector.remove();
      }
    }


    vm.useSavedLabel = useSavedLabel;

    function useSavedLabel(item) {

      // new changeLabelTextStyle

      vm.label = item;
      vm.currentLabelUpdateId = item.id;
      vm.currentLabelName = item.name;
      vm.labelAddressType = item.addressType;

      removeExistingLayer(angular.element('#labelEditorArea .resizable_text'));
      removeExistingLayer(angular.element('#labelEditorArea .resizable_image'));

      angular.element('#labelEditorArea').css(item.styles.main);

      item.layers.forEach(function (layer) {
        if(layer.type === 'image'){
          setLayerImage(layer);
        }else if(layer.type === 'text'){
          setLayerText(layer);
        }
      });

      for(var style in item.styles){
        if(style === 'isGradient' && item.styles.isGradient){
          changeGradientColor();
        }else if(style === 'radius' && item.styles.radius){
          changeLabelRadius();
        }else if(style === 'align'){
          for(var align in item.styles.align){
            var layerSel = angular.element('#textInput'+align);
            if(layerSel.length){
              layerSel.css({'text-align': item.styles.align[align]});
            }
          }
        }else if(style === 'color'){
          for(var color in item.styles.color){
            setCssColor(color, item.styles.color[color]);
          }
        }else if(style === 'font'){
          for(var font in item.styles.font){
            setFontFamily(font, item.styles.font[font]);
          }
        }else if(style === 'fontSize'){
          for(var fsize in item.styles.fontSize){
            setFontSize(fsize, item.styles.fontSize[fsize]);
          }
        }else if( style === 'italic'){
          for(var italic in item.styles.italic){
            setFontStyle(italic, item.styles.italic[italic]);
          }
        }else if(style === 'underline'){
          for(var underline in item.styles.underline){
            setTextDecoration(underline, item.styles.underline[underline]);
          }
        }else if(style === 'bold'){
          item.styles.bold.forEach(function (bold) {
            setFontWeight(bold, 'bold');
          });
        }
      }

      $scope.bRadius = vm.label.styles.main['border-radius'];

    }

    vm.updateLabel = function () {
      if (!vm.currentLabelName) {
        return false;
      }
      html2canvas(angular.element('#labelEditorArea'),
        {
          onrendered: function (canvas) {
            // toDataURL defaults to png, so we need to request a jpeg, then convert for file download.
            var data = {};
            data.id = vm.currentLabelUpdateId;
            data.name = vm.currentLabelName;
            if (vm.labelAddressType === 'return') {
              data.addressType = 'return';
            } else {
              data.addressType = 'shipping';
              data.shippingAddress = vm.contacts;
            }
            data.labelPreview = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
            data.layout = vm.labelLayout;
            data.html = angular.element("#labelEditorArea").html();
            data.editorCSS = angular.element("#labelEditorArea").attr('style');
            data.labelRadius = vm.labelRadius;
            data.labelBGColor = vm.label.styles.bgColor;
            data.labelBGImage = vm.labelBGImage;
            if (vm.isInsertImage) {
              data.isInsertImage = true;
            } else {
              data.isInsertImage = false;
            }
            if (vm.isInsertextText) {
              data.isInsertextText = true;
            }
            else {
              data.isInsertextText = false;
            }
            Auth.updateLabel(data).success(function () {
              $mdToast.show({
                template: '<md-toast class="md-toast success">' + "Label updated successfully" + '</md-toast>',
                hideDelay: 3000,
                position: 'bottom left'
              });
              $state.reload();
            }).error(function () {
              $mdToast.show({
                template: '<md-toast class="md-toast error">' + "Something went wrong!" + '</md-toast>',
                hideDelay: 3000,
                position: 'bottom left'
              });
            });
          }, background: '#fff'
        });
    };
    vm.changeLabelAddressType = function () {

    };
    vm.myCroppedImageInsertImage = '';
    vm.myImageInsertImage = '';
    var handleFileSelectInsertImage = function (evt) {
      // console.log(evt);
      // vm.myImage = '';
      // vm.myCroppedImage = '';
      var file = angular.element(document.querySelector('#fileInputInsertImage'))[0].files[0];
      if (file.type === 'image/jpeg' || file.type === 'image/png') {
        vm.uploadButtonInsertedImage = true;
        var reader = new FileReader();
        reader.onload = function (evt) {
          $scope.$apply(function ($scope) {
            vm.myImageInsertImage = evt.target.result;
          });
        };
        reader.readAsDataURL(file);
      }
      else {
        vm.myCroppedImageInsertImage = '';
      }
    };
    // vm.tmpAvatar = '';
    vm.showAvatarImageInsertImage = handleFileSelectInsertImage;


    function checkSelectedInput(){
      var input = vm.selectedInputTxtId || '';
      var withNoDigits = input.replace(/[0-9]/g, '');
      return withNoDigits === 'textInput' || withNoDigits === 'insertedText' ? parseInt(input.replace(/\D/g,'')) : 0;
    }


    angular.element(document).on('change', '#labelFontSize', function () {

      var currentFontSize = angular.element('#labelFontSize').val();
      var selectedInputId = checkSelectedInput();
      if(selectedInputId){
        angular.element('#textInput'+selectedInputId).css('font-size', vm.labelFontSize[currentFontSize]+'px');
        pushOrSliceObject('fontSize', selectedInputId, currentFontSize);
      }else{
        angular.element('.card-preview .labelUserName').css({'font-size': vm.labelFontSize[currentFontSize]+'px'});
        angular.element('.card-preview .labelUserAddress').css({'font-size': (vm.labelFontSize[currentFontSize])-2+'px'});
        vm.label.styles.main['font-size'] = currentFontSize;
      }
    });

    angular.element(document).on('change', '#labelInsertedTextFontSize', function () {
      if (angular.element('#labelInsertedTextFontSize').val() === 'small') {
        angular.element('.card-preview #insertedText input').css({'font-size': '12px'});
      }
      if (angular.element('#labelInsertedTextFontSize').val() === 'normal') {
        angular.element('.card-preview #insertedText input').css({'font-size': '16px'});
      }
      if (angular.element('#labelInsertedTextFontSize').val() === 'large') {
        angular.element('.card-preview #insertedText input').css({'font-size': '18px'});
      }
      if (angular.element('#labelInsertedTextFontSize').val() === 'extra_large') {
        angular.element('.card-preview #insertedText input').css({'font-size': '20px'});
      }
    });

    function pushOrSliceObject(param, selector, val){
      vm.label.styles[param][selector] = val;
    }

    function setCssColor(selector, val){
      selector = selector === 'all' ? '#labelEditorArea' : '#'+'textInput'+selector;
      angular.element(selector).css({'color': val});
    }

    function setFontFamily(selector, val){
      selector = selector === 'all' ? '#labelEditorArea' : '#'+'textInput'+selector;
      angular.element(selector).css({'font-family': val});
    }

    function setFontSize(selector, val){
      selector = selector === 'all' ? '#labelEditorArea' : '#'+'textInput'+selector;
      angular.element(selector).css({'font-size': vm.labelFontSize[val]+'px'});
    }

    vm.changeSelectedTextColor = changeSelectedTextColor;

    function changeSelectedTextColor() {
      var color = vm.label.color || 'transparent', attributeId;
      var selectedInputId = checkSelectedInput();
      if(selectedInputId){
        setCssColor(selectedInputId, color);
        pushOrSliceObject('color', selectedInputId, color);
      }else{
        attributeId = 'all';
        setCssColor(attributeId, color);
        vm.label.styles.main['color'] = color;
      }
    }

    vm.labelInsertedTextcolorChange = function () {
      angular.element('.card-preview #insertedText input').css({'color': vm.labelInsertedTextcolor.bgColorValue});
    };
    vm.trustAsHtml = function (string) {
      return $sce.trustAsHtml(string);
    };
    vm.insertTextLayer = insertTextLayer;
    function insertTextLayer() {
      vm.insertedTxtId = vm.insertedTxtId ? ++vm.insertedTxtId : 1;
      vm.label.layers.push({type: 'text', id: vm.insertedTxtId, value: vm.insertedText, pos: {}});
      angular.element('#labelEditorArea').append(' <div class="draggable resizable_text" id="insertedText'+vm.insertedTxtId+'" style="text-align:left;">\n' +
        '            <input type="text" id="textInput'+ vm.insertedTxtId +'" class="tool txt_input" style="font-size:14px;text-align:left;background: transparent" value="' + vm.insertedText + '" />\n' +
        '          </div>');
      angular.element('.card-preview #insertedText'+vm.insertedTxtId).draggable({
        containment: angular.element('#labelEditorArea'),
        drag: function(){
          vm.label.layers.getBy('id', vm.insertedTxtId).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
        }
      }).resizable();
      vm.insertedText = '';
      vm.isInsertextText = true;
    }

    angular.element('#labelEditorArea').on('click change mouseup', '.resizable_image, .resizable_text, .labelUserName, .labelUserAddress', function (event) {
      vm.selectedInputTxtId = event.target.id;
      resetToolbarItem();
    });

    angular.element('#labelEditorArea').on('change', '.resizable_text input.txt_input', function () {
      var parseDigit = parseInt(vm.selectedInputTxtId.replace(/\D/g,''));
      if(vm.label.layers.getBy('id', parseDigit) && vm.label.layers.getBy('id', parseDigit).value){
        vm.label.layers.getBy('id', parseDigit).value = angular.element(this).val();
      }
    });

    function resetToolbarItem(){
      vm.label.color = 'transparent';
      vm.labelTextAlignRight = false;
      vm.labelTextAlignCenter = false;
      vm.labelTextAlignLeft = false;
      vm.isTextItalic = false;
      vm.isTextBold = false;
      vm.isTextUnderlined = false;
    }

    // angular.element(document).on('click', '#insertedText', function () {
    //   // vm.editingInsertedText = true;
    //   angular.element("#editingInsertedText").show();
    //   angular.element("#editingLabelText").hide();
    // });

    // angular.element(document).on('focus', '#labelEditorArea', function () {
    //   // vm.editingInsertedText = true;
    //   angular.element("#editingInsertedText").hide();
    //   angular.element("#editingLabelText").show();
    // });

    vm.labelInsertedTextAlign = 'left';
    vm.changeInsertedTextAlign = function () {
      angular.element('.card-preview #insertedText input').css({'text-align': vm.labelInsertedTextAlign});

    };

    function setFontWeight(selector, val){
      selector = selector === 'all' ? '#labelEditorArea' : '#'+'textInput'+selector;
      angular.element(selector).css({'font-weight': val});
    }

    function setFontStyle(selector, val){
      selector = selector === 'all' ? '#labelEditorArea' : '#'+'textInput'+selector;
      angular.element(selector).css({'font-style': val});
    }

    function setTextDecoration(selector, val){
      selector = selector === 'all' ? '#labelEditorArea' : '#'+'textInput'+selector;
      angular.element(selector).css({'text-decoration': val});
    }


    function pushOrSliceStyleAttributes(param, selector){
      var getIndex = vm.label.styles[param].indexOf(selector);
      if(getIndex>-1){
        vm.label.styles[param].splice(getIndex, 1);
      }else{
        vm.label.styles[param].push(selector);
      }
    }
    vm.changeLabelTextStyle = changeLabelTextStyle;
    function changeLabelTextStyle(param) {
      var value, attribute;
      var selectedInputId = checkSelectedInput();
      if(selectedInputId){
        if( param === 'bold'){
          value = vm.label.styles.bold.indexOf(selectedInputId)>-1 ? 'normal' : 'bold';
          setFontWeight(selectedInputId, value);
        }else if( param === 'italic'){
          value = vm.label.styles.italic.indexOf(selectedInputId)>-1 ? 'normal' : 'italic';
          setFontStyle(parseInt(selectedInputId), value);
        }else if( param === 'underline'){
          value = vm.label.styles.underline.indexOf(selectedInputId)>-1 ? 'none' : 'underline';
          setTextDecoration(parseInt(selectedInputId), value);
        }
        pushOrSliceStyleAttributes(param, selectedInputId);
      }else{
        attribute = 'all';
        if( param === 'bold'){
          value = vm.label.styles.bold.indexOf(attribute)>-1 ? 'normal' : 'bold';
          setFontWeight(attribute, value);
          vm.label.styles.main['font-weight'] = param;
        }else if( param === 'italic'){
          value = vm.label.styles.italic.indexOf(attribute)>-1 ? 'normal' : 'italic';
          setFontStyle(attribute, value);
          vm.label.styles.main['font-style'] = param;
        }else if( param === 'underline'){
          value = vm.label.styles.underline.indexOf(attribute)>-1 ? 'none' : 'underline';
          setTextDecoration(attribute, value);
          vm.label.styles.main['text-decoration'] = param;
        }
      }
    }
    vm.changeInsertedTextStyle = function (param) {
      if (param === 'bold') {
        if (vm.isInsertedTextBold) {
          angular.element('.card-preview #insertedText input').css({'font-weight': 'bold'});
        }
        else {
          angular.element('.card-preview #insertedText input').css({'font-weight': 'normal'});
        }
      }
      if (param === 'italic') {
        if (vm.isInsertedTextItalic === true) {
          angular.element('.card-preview #insertedText input').css({'font-style': 'italic'});
        }
        if (vm.isInsertedTextItalic === false) {
          angular.element('.card-preview #insertedText input').css({'font-style': 'normal'});
        }
      }
      if (param === 'underline') {
        if (vm.isInsertedTextUnderlined) {
          angular.element('.card-preview #insertedText input').css({'text-decoration': 'underline'});
        }
        else {
          angular.element('.card-preview #insertedText input').css({'text-decoration': 'none'});
        }
      }
    };
    vm.changeLabelTextAlign = changeLabelTextAlign;
    function changeLabelTextAlign(align) {
      var selectedInputId = checkSelectedInput();
      if(selectedInputId){
        angular.element('#textInput'+selectedInputId).css({'text-align': align});
        pushOrSliceObject('align', selectedInputId, align);
      }else{
        angular.element('#labelEditorArea').css({'text-align': align});
        vm.label.styles.main['text-align'] = align;
      }
    }

    function clickLayerImage(){

    }

    // angular.element(".card-preview #resizableImage").draggable({
    //   containment: angular.element('#labelEditorArea')
    // }).resizable({
    //   aspectRatio: true,
    //   handles: 'ne, se, sw, nw'
    // });

    function insertImageLayer(){
      vm.insertedTxtId = vm.insertedTxtId ? ++vm.insertedTxtId : 1;
      vm.label.layers.push({id: vm.insertedTxtId, type: 'image', src: vm.layerImageOutput, pos: {}});
      angular.element('#labelEditorArea').append('<div id="resizableImage' + vm.insertedTxtId + '" class="resizable_image" style="background-image:url(' + vm.layerImageOutput + ');">' +
        '</div>');
      angular.element(".card-preview #resizableImage"+vm.insertedTxtId).draggable({
        containment: angular.element('#labelEditorArea'),
        drag: function(){
          vm.label.layers.getBy('id', vm.insertedTxtId).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
        }
      }).resizable({
        aspectRatio: true
      });
      vm.isInsertImage = true;
      closeDialog();
    }

    function getBackgroundImageAcceptRatio() {
      var cardWidth = angular.element('#labelEditorArea').width();
      var cardHeigth = angular.element('#labelEditorArea').height();
      if (cardWidth && cardHeigth) {
        vm.cardBackgroundImageWidth = cardWidth;
        vm.cardBackgroundImageHeight = cardHeigth;
        vm.cardBackgroundRatio = cardWidth / cardHeigth;
      }
    }

    function saveCurrentDesign(){
      LabelService.saveCurrentDesign(vm.label).then(function (savedLabel) {
        var canvasArea = angular.element('#labelEditorArea');
        makeCanvasToDataUrl(canvasArea, function (dataUrl) {
          saveDataImageToServer(dataUrl, savedLabel.id, function (file) {
            showMessage('The label design has been saved.');
            // loadSavedLabel(savedLabel.id);
            loadExistingLabels();
          });
        });
      });
    }

    function loadSavedLabel( id ) {
      LabelService.loadExistingLabel({id: id}).then(function (resp) {
        vm.savedLabels.push(resp);
      });
    }

    function saveDataImageToServer(data_url, labelId, callback) {
      if (data_url) {
        Upload.upload({
          url: '/label/saveLabelPreview',
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          data: {
            file: Upload.dataUrltoBlob(data_url, 'preview.png'), label: labelId || ''
          }
        }).then(function (resp) {
          callback(resp.data);
        }, function (resp) {
          //console.log('Error status: ' + resp.status);
        });
      } else {
        callback('');
      }
    }

    function makeCanvasToDataUrl(selector, callback){
      html2canvas(selector, {
        onrendered: function(canvas) {
          // angular.element('.card-preview').append(canvas);
          callback(canvas.toDataURL('image/png', 1.0));
        },
        useCORS:true
      });
    }

    function showMessage(message) {
      $mdToast.show({
        template: '<md-toast class="md-toast success">' + message + '</md-toast>',
        hideDelay: 3000,
        position: 'bottom left'
      });
    }

    function addlayerImage(){
      showDialogue('app/main/labels/dialogs/insert-image.html');
    }

    function showDialogue(templateUrl, clickOutsideToClose) {
      $mdDialog.show({
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: templateUrl,
        clickOutsideToClose: clickOutsideToClose || false
      });
    }

    function removeInsertedImage(){
      if(vm.selectedInputTxtId && vm.selectedInputTxtId.length){
        var withNoDigits = vm.selectedInputTxtId.replace(/[0-9]/g, '');
        var targetInputId = angular.element('#'+vm.selectedInputTxtId);
        if(withNoDigits === 'resizableImage' && targetInputId.length){
          targetInputId.remove();
        }
      }
    }

    function removeInsertedText() {
      if(vm.selectedInputTxtId && vm.selectedInputTxtId.length){
        var withNoDigits = vm.selectedInputTxtId.replace(/[0-9]/g, '');
        if(withNoDigits === 'textInput' || withNoDigits === 'insertedText'){
          if(withNoDigits === 'textInput'){
            angular.element('#'+vm.selectedInputTxtId).parent().remove();
          }else if(withNoDigits === 'insertedText'){
            angular.element('#'+vm.selectedInputTxtId).remove();
          }
        }
      }
    }

    function removeLabel( item ) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure want to delete the label?')
        .htmlContent('<b>Label will be removed permanently.')
        .ariaLabel('delete label')
        .targetEvent()
        .ok('OK')
        .cancel('CANCEL');
      $mdDialog.show(confirm).then(function () {
        LabelService.deleteLabel({id: item.id}).then(function (respLabel) {
          loadExistingLabels();
          showMessage('Label removed successfully');
          closeDialog();
        });
      }, function () {
        closeDialog();
      });
    }

    function shippingAddressContentRender() {
      var totalContactsForShipping = [];
      if (vm.exportContacts === 'all') {
        totalContactsForShipping = vm.contacts;
      }
      else if (vm.exportContacts === 'favorites') {
        favoriteContactsIds.forEach(function (item) {
          vm.contacts.forEach(function (contact) {
            if (contact.id === item) {
              totalContactsForShipping.push(contact);
            }
          });
        });
      } else if (vm.exportContacts === 'group') {
        vm.selectedGroupForExport.contactIds.forEach(function (item) {
          vm.contacts.forEach(function (contact) {
            if (contact.id === item) {
              totalContactsForShipping.push(contact);
            }
          });
        });
      } else if (vm.exportContacts === 'selected') {
        vm.selectedContacts.forEach(function (item) {
          vm.contacts.forEach(function (contact) {
            if (contact.id === item.id) {
              totalContactsForShipping.push(contact);
            }
          });
        });
      }
      vm.amountOfReturnAddressforPrintPreview = '';
      var printPreviewData = $filter('filter')(vm.labelLayoutList, {id: vm.labelLayout}, true)[0];
      // angular.copy(vm.amountOfReturnAddress, vm.amountOfReturnAddressforPrintPreview);
      vm.singlePageRow = printPreviewData.printRow;
      vm.singlePageColumn = printPreviewData.printCol;

      var tmpContents = [];
      angular.element("#pHWpShipping").remove();
      angular.element('body').append('<div id="pHWpShipping" style="display:none;visibility:hidden;"></div>');
      totalContactsForShipping.forEach(function (item) {
        angular.element("#pHWpShipping").html(angular.element("#labelEditorArea").html());

        if (!item.first_name && !item.last_name) {
          angular.element("#pHWpShipping .labelUserName").html('No Name');

        } else {
          angular.element("#pHWpShipping .labelUserName").html(item.first_name + ' ' + item.last_name);
        }
        if (!item.address[0].address.city && !item.address[0].address.state && !item.address[0].address.zip) {
          angular.element("#pHWpShipping .labelUserAddress").html('No Address');
        } else {
          angular.element("#pHWpShipping .labelUserAddress").html(item.address[0].address.city + ' ' + item.address[0].address.state + ' ' + item.address[0].address.zip);
        }

        tmpContents.push(angular.element("#pHWpShipping").html());
      });

      function pageItemChunk(chunk, content) {
        var i, j, temparray;
        var fi = [];
        for (i = 0, j = content.length; i < j; i += chunk) {
          temparray = content.slice(i, i + chunk);
          // do whatever
          fi.push(temparray);
        }
        return fi;
      }

      vm.pagesContent = pageItemChunk(Math.ceil(vm.singlePageColumn * vm.singlePageRow), tmpContents);
      var finalPageContents = [];
      vm.pagesContent.forEach(function (item) {
        finalPageContents.push(pageItemChunk(vm.singlePageColumn, item));
      });
      return finalPageContents;
    }

    vm.printPreview = printPreview;

    function printPreview() {
      if (vm.labelAddressType === 'return') {
        vm.amountOfReturnAddressforPrintPreview = '';
        var printPreviewData = $filter('filter')(vm.labelLayoutList, {id: vm.labelLayout}, true)[0];
        // angular.copy(vm.amountOfReturnAddress, vm.amountOfReturnAddressforPrintPreview);
        vm.amountOfReturnAddressforPrintPreview = vm.amountOfReturnAddress;
        // console.log(vm.amountOfReturnAddressforPrintPreview);
        // console.log(vm.amountOfReturnAddress);
        vm.singlePageRow = printPreviewData.printRow;
        vm.singlePageColumn = printPreviewData.printCol;
        vm.printLabelContent = angular.element("#labelEditorArea").html();

        if (vm.labelBGImage) {
          function dataURLtoBlob(dataurl) {
            var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
              bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
            while (n--) {
              u8arr[n] = bstr.charCodeAt(n);
            }
            return new Blob([u8arr], {type: mime});
          }

          var blob = URL.createObjectURL(dataURLtoBlob(vm.labelBGImage));
          angular.element("#pHWp").remove();
          angular.element('body').append('<div id="pHWp" style="display:none;visibility:hidden;"><div id="pHWpc"></div></div>');
          angular.element('#pHWpc').attr('style', angular.element("#labelEditorArea").attr('style'));
          angular.element('#pHWpc').css('background-image', '');
          angular.element('#pHWpc').css('background-image', 'url(' + blob + ')');
          vm.printLabelContentCSS = angular.element('#pHWpc').attr('style');
        } else {
          vm.printLabelContentCSS = angular.element("#labelEditorArea").attr('style');
        }
      }
      if (vm.labelAddressType === 'shipping') {
        vm.finalPageContents = shippingAddressContentRender();
        // console.log(vm.finalPageContents);
        if (vm.labelBGImage) {
          function dataURLtoBlob(dataurl) {
            var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
              bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
            while (n--) {
              u8arr[n] = bstr.charCodeAt(n);
            }
            return new Blob([u8arr], {type: mime});
          }

          var blob = URL.createObjectURL(dataURLtoBlob(vm.labelBGImage));
          angular.element("#pHWp").remove();
          angular.element('body').append('<div id="pHWp" style="display:none;visibility:hidden;"><div id="pHWpc"></div></div>');
          angular.element('#pHWpc').attr('style', angular.element("#labelEditorArea").attr('style'));
          angular.element('#pHWpc').css('background-image', '');
          angular.element('#pHWpc').css('background-image', 'url(' + blob + ')');
          vm.printLabelContentCSS = angular.element('#pHWpc').attr('style');
        } else {
          vm.printLabelContentCSS = angular.element("#labelEditorArea").attr('style');
        }
      }
    }
    vm.applyCSSOnPrintPreview = function () {
      angular.element(".printContent #insertedText input").prop('disabled', true);
      angular.element(".printContent #insertedText").resizable();
      angular.element(".printContent #resizableImage").resizable();
      angular.element(".printContent #insertedText").resizable('destroy');
      angular.element(".printContent #resizableImage").resizable('destroy');
    };
    vm.print = printLabel;
    function printLabel() {
      window.print();
    }
    vm.toPdf = convertLabelToPdf;
    function convertLabelToPdf() {
      showMessage('Preparing PDF...');
      var labelData = {
        html: JSON.stringify(vm.labelAddressType === 'shipping' ? shippingAddressContentRender() : angular.element("#labelEditorArea").html()),
        editorCSS: JSON.stringify(angular.element('#labelEditorArea').attr('style')),
        editorClass: JSON.stringify(angular.element("#labelEditorArea").attr('class')),
        layout: $filter('filter')(vm.labelLayoutList, {id: vm.labelLayout}, true)[0],
        addressType: vm.labelAddressType,
        amountOfReturnAddress: vm.amountOfReturnAddress,
        backgroundImageData: vm.labelBGImage
      };
      // console.log(labelData);
      LabelService.convertLabelToPdf(labelData).then(function (respPdf) {
        if(respPdf){
          vm.pdfLink = vm.site_url + '/' + respPdf;
          showMessage('The pdf file is ready to download');
        }
        // console.log(respPdf);
      });

      vm.downloadPdf = downloadPdf;

      function downloadPdf(){
        window.open(vm.pdfLink);
      }


      //
      // $mdToast.show({
      //   template: '<md-toast class="md-toast success">' + 'Preparing your PDF...' + '</md-toast>',
      //   hideDelay: 3000,
      //   position: 'bottom left'
      // });
      // angular.element("#pHW").remove();
      // angular.element('body').append('<div id="pHW" style="display:none;visibility:hidden;"><div id="pHWc"></div></div>');
      // angular.element('#pHWc').attr('style', angular.element("#labelEditorArea").attr('style'));
      // angular.element('#pHWc').css('background-image', '');
      //
      //
      //
      // var convertPDFURL = $http.post('/label/toPdf', {
      //   data: {
      //     html: JSON.stringify(vm.labelAddressType === 'shipping' ? shippingAddressContentRender() : angular.element("#labelEditorArea").html()),
      //     editorCSS: JSON.stringify(angular.element('#pHWc').attr('style')),
      //     editorClass: JSON.stringify(angular.element("#labelEditorArea").attr('class')),
      //     layout: $filter('filter')(vm.labelLayoutList, {id: vm.labelLayout}, true)[0],
      //     addressType: vm.labelAddressType,
      //     amountOfReturnAddress: vm.amountOfReturnAddress,
      //     backgroundImageData: vm.labelBGImage
      //   }
      // });
      // convertPDFURL.success(function (result) {
      //   $mdToast.hide();
      //   vm.pdfDownlodLink = result;
      //   $mdDialog.show({
      //     controller: function () {
      //       return vm;
      //     },
      //     controllerAs: 'vm',
      //     templateUrl: 'app/main/labels/dialogs/contact/pdfDownload.html',
      //     clickOutsideToClose: true,
      //     // targetEvent: ev,
      //   });
      // });
      // convertPDFURL.error(function (resp) {
      //   $mdToast.show({
      //     template: '<md-toast class="md-toaLabelServiceProviderst error">' + 'Something went wrong' + '</md-toast>',
      //     hideDelay: 3000,
      //     position: 'bottom left'
      //   });
      //   // return resp;
      // });
    }


    vm.fontSizeSelect = ['10px', '11px', '12px', '13px', '14px', '15px', '16px', '17px', '18px', '19px', '20px', '21px', '22px', '23px', '24px', '25px', '26px'];

    Array.prototype.getBy = function (id, value) {
      return this.filter(function (x) {
        return x[id] === value;
      })[0];
    };
  }

})();
