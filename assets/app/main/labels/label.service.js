(function () {
  'use strict';

  angular
    .module('app.labels')
    .factory('LabelService', LabelService);

  /**
   * @method Service For Chat
   * @param {string} $http
   * @param {object} $q
   * @returns {Object|Array}
   */
  // The Service to get All requested data by Controller.

  function LabelService($http, $q) {

    return {

      /**
       * @method findGiphyStickerSearch
       * @param {number} limit
       * @param {number} offset
       * @param {string} tags
       * @return {Object|Array}
       */
      // Get Giphy Sticker by Searching String
      saveCurrentDesign: function ( data ) {
        var defer = $q.defer();
        $http.post('/label/saveCurrentDesign', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      loadExistingLabel: function () {
        var defer = $q.defer();
        $http.post('/label/loadExistingLabel').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      loadSavedLabel: function (data) {
        var defer = $q.defer();
        $http.post('/label/loadExistingLabel', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      deleteLabel: function (data) {
        var defer = $q.defer();
        $http.post('/label/delete', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      convertLabelToPdf: function (data) {
        var defer = $q.defer();
        $http.post('/label/toPdf', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

    };
  }

})();
