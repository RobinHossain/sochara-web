(function () {
  'use strict';

  angular
    .module('app.labels',
      [
        // 3rd Party Dependencies
        // 'xeditable',
        'ngDraggable',
        'ngFileUpload',
        'ngImgCrop',
        'mdPickers',
        'ngMaterial'
      ]
    )
    .config(config);

  /** @ngInject */
  function config($stateProvider) {

    $stateProvider.state('app.labels', {
      url: '/labels',
      views: {
        'content@app': {
          templateUrl: 'app/main/labels/label.html',
          controller: 'LabelsController as vm'
        }
      }
    });


  }

})();
