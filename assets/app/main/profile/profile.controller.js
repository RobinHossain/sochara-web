(function () {
  'use strict';

  angular
    .module('app.profile')
    .controller('ProfileController', ProfileController);

  /**
   * Description
   * @ngInject 
   * @method ProfileController
   * @param {} $scope
   * @param {} $state
   * @param {} Upload
   * @param {} Auth
   * @param {} $document
   * @param {} $mdToast
   * @param {} $mdDialog
   * @param {} $location
   * @param {} MailService
   * @param {} $timeout
   * @param {} ProfileService
   * @return 
   */
  function ProfileController($scope, $state, Upload, Auth, $document, $mdToast, $mdDialog, $location, MailService, $timeout, ProfileService) {

    /*HSN*/

    var vm = this;
    vm.site_url = $location.protocol() + '://' + $location.host() + ':' + $location.port();
    vm.readonly = false;
    // Lists of fruit names and Vegetable objects
    vm.fruitNames = [];
    vm.roFruitNames = angular.copy(vm.fruitNames);
    vm.editableFruitNames = angular.copy(vm.fruitNames);
    vm.tags = [];
    vm.contact = {};
    vm.resetContact = {};
    vm.profileGeneralSettings = profileGeneralSettings;
    vm.profileContactSettings = profileContactSettings;
    vm.profileWorkSettings = profileWorkSettings;
    vm.profileRelativesSettings = profileRelativesSettings;
    vm.socharaAccountSettings = socharaAccountSettings;
    vm.socharaPasswordAccountSettings = socharaPasswordAccountSettings;
    vm.setAttributePrivacyIcon = setAttributePrivacyIcon;
    vm.closeDialog = closeDialog;
    vm.setProfilePhoto = setProfilePhoto;
    vm.uploadProfilePhotoPopup = uploadProfilePhotoPopup;
    vm.getMailAuthUrl = getMailAuthUrl;
    vm.removeAccountConfirm = removeAccountConfirm;
    vm.removeAccount = removeAccount;
    vm.saveProfile = submitProfileData;
    vm.dateParser = dateParser;
    vm.attributePrivacy = attributePrivacy;
    vm.getData = getData;
    vm.relativeFromContactPopup = relativeFromContactPopup;

    init();


    /**
     * Description
     * @method init
     * @return 
     */
    function init() {
      getData();
      getContacts();
      getAllRelativeInfo();

      $timeout(function () {
        getEmailAddress();
        socialAccountAuthCheck();
        instagramOauthChecking();
      }, 2000);
    }

    /**
     * Description
     * @method dateParser
     * @param {} item
     * @return 
     */
    function dateParser(item) {
      if (item) {
        return new Date(item);
      }
      else {
        return new Date();
      }
    }

    /**
     * Description
     * @method uploadProfilePhotoPopup
     * @return 
     */
    function uploadProfilePhotoPopup() {
      if (vm.contact.photo_url) {
        vm.changeProfilePhoto = vm.contact.photo_url;
      }

      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/profile/popup/uploadProfilePhoto.html',
        clickOutsideToClose: false
      });
    }

    /**
     * Fetching Contact Data
     * @method getData
     * @return 
     */
    function getData() {

      Auth.getProfiledata().success(function (result) {
        vm.contact = result;
        angular.copy(result, vm.resetContact);
      }).error(function (err) {
        showMessage('Something went wrong. Please try again.');
      });
    }


    /**
     * Saving Contact Data
     * @method submitProfileData
     * @return 
     */
    function submitProfileData() {
      vm.contact.date_birth = vm.date_birth;
      vm.contact.date_anniversary = vm.date_anniv;
      Auth.saveProfileData(vm.contact).success(function (result) {
        closeDialog();
        getData();
        showMessage('Profile has been successfully updated!');
      }).error(function (err) {
        closeDialog();
        showMessage(err.err);
      });
    }

    /**
     * Returning Date string for Birthday and Annniversary in md-datepicker
     * @method StrToDate
     * @param {} str
     * @return NewExpression
     */
    vm.StrToDate = function (str) {
      return new Date(str);
    };


    /**
     * Privacy attribute
     * @method setAttributePrivacy
     * @param {} attribute
     * @param {} privacy
     * @return 
     */
    function setAttributePrivacy(attribute, privacy) {
      var friendsIndex = vm.contact.privacyAttribute.friends.indexOf(attribute);
      var onlyMeIndex = vm.contact.privacyAttribute.onlyMe.indexOf(attribute);
      if (friendsIndex !== -1) {
        vm.contact.privacyAttribute.friends.splice(friendsIndex, 1);
      }
      if (onlyMeIndex !== -1) {
        vm.contact.privacyAttribute.onlyMe.splice(friendsIndex, 1);
      }
      if (privacy === 'friends') {
        vm.contact.privacyAttribute.friends.push(attribute);
      }
      if (privacy === 'onlyMe') {
        vm.contact.privacyAttribute.onlyMe.push(attribute);
      }
    }

    /**
     * Description
     * @method setAttributePrivacyIcon
     * @param {} attribute
     * @return 
     */
    function setAttributePrivacyIcon(attribute) {
      var privacy = '';
      var friendsIndex = '';
      var onlyMeIndex = '';
      // console.log(vm.contact);
      if (vm.contact.privacyAttribute && vm.contact.privacyAttribute.friends && vm.contact.privacyAttribute.friends.length) {
        friendsIndex = vm.contact.privacyAttribute.friends.indexOf(attribute);
        onlyMeIndex = vm.contact.privacyAttribute.onlyMe.indexOf(attribute);
      }
      if (friendsIndex !== -1) {
        privacy = 'friends';
      }

      if (onlyMeIndex !== -1) {
        privacy = 'onlyMe';
      }
      if (privacy === 'friends') {
        return 'icon-account-multiple';
      } else if (privacy === 'onlyMe') {
        return 'icon-eye-off';
      } else {
        return 'icon-earth';
      }
    }

    /**
     * Description
     * @method attributePrivacy
     * @param {} attribute
     * @param {} privacy
     * @return 
     */
    function attributePrivacy(attribute, privacy) {

      Auth.privacyAttribute(attribute, privacy).then(function (response) {
        setAttributePrivacy(attribute, privacy);
      });

    }

    /**
     * Description
     * @method getEmailAddress
     * @return 
     */
    function getEmailAddress() {
      MailService.getMailAddresses().then(function (response) {
        vm.gmailAccounts = [];
        vm.outlookAccounts = [];
        if (response && response.length) {
          response.forEach(function (val) {
            if (val.provider === 'gmail') {
              vm.gmailAccounts.push(val);
            }
          });
          response.forEach(function (val) {
            if (val.provider === 'outlook') {
              vm.outlookAccounts.push(val);
            }
          });
        }

      });
    }

    /**
     * Description
     * @method removeAccount
     * @param {} account
     * @return 
     */
    function removeAccount(account) {
      vm.selectedRemoveAccount = account.email;
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: "vm",
        templateUrl: "app/main/mail/dialogs/remove/confirm.html",
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method removeAccountConfirm
     * @return 
     */
    function removeAccountConfirm() {
      MailService.deleteCurrentAccount({email: vm.selectedRemoveAccount}).then(function (response) {
        if (response) {
          // console.log(response);
          var message = "The Account has been removed";
          $mdDialog.hide();
          getEmailAddress();
          $mdToast.show({
            template: '<md-toast class="md-toast success">' + message + '</md-toast>',
            hideDelay: 3000,
            position: 'bottom left'
          });
        }
      });
    }

    /**
     * Description
     * @method getMailAuthUrl
     * @return 
     */
    function getMailAuthUrl() {
      MailService.GMailAuthUrl().then(function (response) {
        if (response) {
          vm.gmailAuthUrl = response;
        }
      });

      MailService.OutlookAuthUrl().then(function (response) {
        if (response) {
          vm.outlookAuthUrl = response;
        }
      });
    }


    //End privacy Attribute

    /**
     * Description
     * @method closeDialog
     * @return 
     */
    function closeDialog() {
      //console.log(vm.resetContact);
      angular.copy(vm.resetContact, vm.contact);
      $mdDialog.hide();
    }

    /**
     * Description
     * @method setProfilePhoto
     * @return 
     */
    function setProfilePhoto() {
      savePhotoDataUrl(vm.changeProfileImageCropped, function (photoId) {
        if (photoId) {
          showMessage('The profile photo has been updated !');
        } else {
          showMessage('Can not update profile photo !');
        }
        closeDialog();
        vm.contact.photo_url = vm.changeProfileImageCropped;
      });
    }

    /**
     * Description
     * @method savePhotoDataUrl
     * @param {} dataUrl
     * @param {} callback
     * @return 
     */
    function savePhotoDataUrl(dataUrl, callback) {
      if (dataUrl) {
        Upload.upload({
          url: '/profile/saveProfilePhoto',
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          data: {
            file: Upload.dataUrltoBlob(vm.changeProfileImageCropped, 'profile_photo.png')
          }
        }).then(function (resp) {
          callback(resp.data);
        }, function (resp) {
          //console.log('Error status: ' + resp.status);
        });
      } else {
        callback('');
      }
    }

    /**
     * Description
     * @method showMessage
     * @param {} message
     * @return 
     */
    function showMessage(message) {
      $mdToast.show({
        template: '<md-toast class="md-toast success">' + message + '</md-toast>',
        hideDelay: 3000,
        position: 'bottom left'
      });
    }


    /**
     * Description
     * @method profileGeneralSettings
     * @return 
     */
    function profileGeneralSettings() {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/profile/popup/profile_general_settings.html',
        clickOutsideToClose: true
      });
    }

    /**
     * Contact area
     * @method profileContactSettings
     * @return 
     */
    function profileContactSettings() {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/profile/popup/profile_contact_settings.html',
        clickOutsideToClose: true
      });
    }

    /**
     * Work area
     * @method profileWorkSettings
     * @return 
     */
    function profileWorkSettings() {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/profile/popup/profile_work_settings.html',
        clickOutsideToClose: true
      });
    }

    /**
     * Sochara Account Email Setting area
     * @method socharaAccountSettings
     * @return 
     */
    function socharaAccountSettings() {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/profile/popup/sochara_account_settings.html',
        clickOutsideToClose: true
      });
    }

    /**
     * Sochara Account Password Setting area
     * @method socharaPasswordAccountSettings
     * @return 
     */
    function socharaPasswordAccountSettings() {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/profile/popup/sochara_account_password_settings.html',
        clickOutsideToClose: true
      });
    }

    /*Relatives area*/

    vm.relativeContactType = '';

    /**
     * Description
     * @method profileRelativesSettings
     * @return 
     */
    function profileRelativesSettings() {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/profile/popup/profile_relatives_settings.html',
        clickOutsideToClose: true
      });
    }

    /**
     * Description
     * @method relativeFromContactPopup
     * @return 
     */
    function relativeFromContactPopup() {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/profile/popup/profile_relatives_contact_settings.html',
        clickOutsideToClose: true
      });
    }

    /*HSN*/


    //Avatar upload
    $scope.myImage = '';
    $scope.myCroppedImage = '';

    /**
     * Description
     * @method handleFileSelect
     * @param {} evt
     * @return 
     */
    var handleFileSelect = function (evt) {
      var file = evt.currentTarget.files[0];
      if (file.type === 'image/jpeg' || file.type === 'image/png') {
        vm.uploadButton = true;
        var reader = new FileReader();
        /**
         * Description
         * @method onload
         * @param {} evt
         * @return 
         */
        reader.onload = function (evt) {
          $scope.$apply(function ($scope) {
            $scope.myImage = evt.target.result;
          });
        };
        reader.readAsDataURL(file);
      }
      else {
        $scope.myCroppedImage = '';
      }
    };
    angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);
    /**
     * Description
     * @method saveAvatar
     * @return 
     */
    $scope.saveAvatar = function () {
      //console.log($scope.myCroppedImage);
      if ($scope.myCroppedImage === '') {
        $mdToast.show({
          template: '<md-toast class="md-toast error">' + "No Image found" + '</md-toast>',
          hideDelay: 3000,
          position: 'bottom left'
        });
      }
      else if ($scope.myCroppedImage !== '') {
        Auth.saveAvatar($scope.myCroppedImage).success(function (result) {
          $.modal.close();
          getData();
          $mdToast.show({
            template: '<md-toast class="md-toast success">' + "Photo has been successfully updated!" + '</md-toast>',
            hideDelay: 3000,
            position: 'bottom left'
          });
        }).error(function (err) {
          //console.log(err);
          $mdToast.show({
            template: '<md-toast class="md-toast error">' + err.err + '</md-toast>',
            hideDelay: 3000,
            position: 'bottom left'
          });
        });
      }
    };

    // Banner Image

    $scope.myImageBanner = '';
    $scope.myCroppedImageBanner = '';

    /**
     * Description
     * @method handleFileSelect
     * @param {} evt
     * @return 
     */
    var handleFileSelect = function (evt) {
      var file = evt.currentTarget.files[0];
      if (file.type === 'image/jpeg' || file.type === 'image/png') {
        vm.uploadButton = true;
        var reader = new FileReader();
        /**
         * Description
         * @method onload
         * @param {} evt
         * @return 
         */
        reader.onload = function (evt) {
          $scope.$apply(function ($scope) {
            $scope.bannerImgSrc = evt.target.result;
            $scope.myCroppedImageBanner = evt.target.result;
          });
        };
        reader.readAsDataURL(file);
      }
      else {
        $scope.myCroppedImageBanner = '';
        alert("Sorry! No Image file has been selected");
      }
    };
    angular.element(document.querySelector('#fileInputBanner')).on('change', handleFileSelect);
    /**
     * Description
     * @method saveBannerImage
     * @return 
     */
    $scope.saveBannerImage = function () {

      if ($scope.myCroppedImageBanner === '') {
        $mdToast.show({
          template: '<md-toast class="md-toast error">' + "No Image found" + '</md-toast>',
          hideDelay: 3000,
          position: 'bottom left'
        });
      }
      else if ($scope.myCroppedImageBanner !== '') {
        Auth.saveBannerImage($scope.myCroppedImageBanner).success(function (result) {
          $.modal.close();
          getData();
          $mdToast.show({
            template: '<md-toast class="md-toast success">' + "Photo has been successfully updated!" + '</md-toast>',
            hideDelay: 3000,
            position: 'bottom left'
          });
        }).error(function (err) {
          ////console.log(err);
          $mdToast.show({
            template: '<md-toast class="md-toast error">' + err.err + '</md-toast>',
            hideDelay: 3000,
            position: 'bottom left'
          });
        });
      }
    };
    /*Social Auth Settings Checking*/
    $scope.fbAuthorized = false;
    $scope.fbAuthUserPhoto = '';
    $scope.fbAccountName = '';
    vm.fbAuthpupostsRtrnSpin = false;
    vm.fbAuthpupostsPublishSpin = false;
    vm.fbAuthmusicRtrnSpin = false;
    vm.fbAuthmoviesRtrnSpin = false;
    vm.fbAuthbookRtrnSpin = false;
    vm.fbAuthRemoveSpin = false;
    var fbGraphApiAccessArea = {};

    /**
     * Description
     * @method socialAccountAuthCheck
     * @return 
     */
    var socialAccountAuthCheck = function socialAccountAuthCheckFun() {
      Auth.socialAccountAuthCheck().success(function (result) {
        Object.keys(result).forEach(function (item) {
          if (item === 'fb') {
            $scope.fbAuthorized = true;
            $scope.fbAccountName = result.fb.accountName;
            $scope.accessArea = result.fb.accessArea;
            $scope.fbAuthUserPhoto = '//graph.facebook.com/' + result.fb.socialUserId + '/picture';
            vm.fbAuthpupostsRtrn = result.fb.accessArea.feed;
            vm.fbAuthpupostsPublish = result.fb.accessArea.feedPostPermission;
            vm.fbAuthmusicRtrn = result.fb.accessArea.music;
            vm.fbAuthmoviesRtrn = result.fb.accessArea.books;
            vm.fbAuthbookRtrn = result.fb.accessArea.movies;
            fbGraphApiAccessArea = result.fb.accessArea;
          }
          if (item === 'twitter') {
            vm.twitterAuth = true;
            $scope.twitterUserScreenName = result.twitter.screen_name;
            $scope.twitterUserImageUrl = 'https://twitter.com/' + result.twitter.screen_name + '/profile_image?size=mini';
          }
        });
      }).error(function (err) {
        ////console.log(err);
      });
    };


    //Instagram Oauth checking
    /**
     * Description
     * @method instagramOauthChecking
     * @return 
     */
    var instagramOauthChecking = function () {
      Auth.InstagramOauthCheck().success(function (response) {
        if (response) {
          $scope.instagramAuth = true;
          $scope.instagramUserImageUrl = response.msg.profilePicture;
          $scope.instagramUserScreenName = response.msg.name;
        }
      });
    };

    /*End Social Oauth Settings*/

    /**
     * Facebook Add authorization 
     * @method fbAuthModal
     * @return 
     */
    $scope.fbAuthModal = function () {
      vm.fbAuthposts = false;
      vm.fbAuthmusic = false;
      vm.fbAuthmovies = false;
      vm.fbAuthbook = false;
      $('#fbAuthorizeModal').appendTo('body').modal();
      $scope.fbAuthposts = false;
    };

    //Authorizing
    /**
     * Description
     * @method fbAuthorize
     * @return 
     */
    $scope.fbAuthorize = function () {
      ////console.log(vm.fbAuthposts);
      var scope = [];
      if (vm.fbAuthPostsPermission) {
        scope.push('publish_actions');
      }
      if (vm.fbAuthposts) {
        scope.push('user_posts');
      }
      if (vm.fbAuthmusic) {
        scope.push('user_actions.music');
      }
      if (vm.fbAuthmovies) {
        scope.push('user_actions.video');
      }
      if (vm.fbAuthbook) {
        scope.push('user_actions.books');
      }
      if (vm.fbPublishpost) {
        scope.push('publish_actions');
      }
      if (scope.length > 0) {
        scope.push('user_likes');
      }
      var accessScope = scope.join(', ');
      if (scope.length > 0) {
        var accessArea = {};
        accessArea.feedPostPermission = vm.fbAuthPostsPermission;
        accessArea.feed = vm.fbAuthposts;
        accessArea.music = vm.fbAuthmusic;
        accessArea.movies = vm.fbAuthmovies;
        accessArea.books = vm.fbAuthbook;
        FB.login(function (response) {
          if (response.authResponse) {
            //console.log(response.authResponse);
            FB.api(
              "/me",
              function (data) {
                if (data && !data.error) {
                  //console.log(data);
                  /* handle the result */
                  response.authResponse.name = data.name;
                  response.authResponse.accessArea = accessArea;
                  Auth.fbAuthorize(response.authResponse).success(function (result) {
                    $.modal.close();
                    socialAccountAuthCheck();
                    $mdToast.show({
                      template: '<md-toast class="md-toast success">' + "Authorizaton has been completed successfully!" + '</md-toast>',
                      hideDelay: 3000,
                      position: 'bottom left'
                    });
                  }).error(function (err) {
                    //console.log(err);
                  });
                }
              }
            );
          } else {
            alert('User cancelled login or did not fully authorize.');
          }
        }, {scope: accessScope});
        return false;

      } else {
        $mdToast.show({
          template: '<md-toast class="md-toast error">' + "Please Select any choice!" + '</md-toast>',
          hideDelay: 3000,
          position: 'bottom left'
        });
      }
    };

    //Removing Facebook Auth
    /**
     * Description
     * @method removeAuthorization
     * @return 
     */
    $scope.removeAuthorization = function () {
      Auth.removeAuthorization().success(function (result) {
        $.modal.close();
        $mdToast.show({
          template: '<md-toast class="md-toast success">' + "Authorization has been successfully removed!" + '</md-toast>',
          hideDelay: 3000,
          position: 'bottom left'
        });
        $scope.fbAuthorized = false;
      }).error(function (err) {
        //console.log(err);
        $mdToast.show({
          template: '<md-toast class="md-toast error">' + err.err + '</md-toast>',
          hideDelay: 3000,
          position: 'bottom left'
        });
      }).finally(function () {
        vm.removeFBAuth = false;
        vm.fbAuthRemoveSpin = false;
      });
    };

    //Updating Facebook Auth
    /**
     * Description
     * @method changePermission
     * @param {} status
     * @param {} permissionScope
     * @param {} switchId
     * @return 
     */
    $scope.changePermission = function (status, permissionScope, switchId) {
      if (status === false) {
        // //console.log(vm.fbAuthpupostsRtrn);
        Auth.rmPermission(permissionScope).success(function (result) {
          $mdToast.show({
            template: '<md-toast class="md-toast success">' + "Authorization has been successfully updated!" + '</md-toast>',
            hideDelay: 3000,
            position: 'bottom left'
          });
        }).error(function (err) {
          //console.log(err);
          $mdToast.show({
            template: '<md-toast class="md-toast error">' + err.err + '</md-toast>',
            hideDelay: 3000,
            position: 'bottom left'
          });
        }).finally(function () {
          if (switchId === 'fbAuthpupostsRtrnSpin') {
            vm.fbAuthpupostsRtrnSpin = false;
          }
          if (switchId === 'fbAuthpupostsPublishSpin') {
            vm.fbAuthpupostsPublishSpin = false;
          }
          if (switchId === 'fbAuthmusicRtrnSpin') {
            vm.fbAuthmusicRtrnSpin = false;
          }
          if (switchId === 'fbAuthmoviesRtrnSpin') {
            vm.fbAuthmoviesRtrnSpin = false;
          }
          if (switchId === 'fbAuthbookRtrnSpin') {
            vm.fbAuthbookRtrnSpin = false;
          }
        });
      }
      else {
        Auth.setFbPermission(permissionScope).success(function (result) {
          $mdToast.show({
            template: '<md-toast class="md-toast success">' + "Authorization has been successfully updated!" + '</md-toast>',
            hideDelay: 3000,
            position: 'bottom left'
          });
        }).error(function (err) {
          //console.log(err);
          $mdToast.show({
            template: '<md-toast class="md-toast error">' + err.err + '</md-toast>',
            hideDelay: 6000,
            position: 'top right'
          });
        }).finally(function () {
          if (switchId === 'fbAuthpupostsRtrnSpin') {
            vm.fbAuthpupostsRtrnSpin = false;
          }
          if (switchId === 'fbAuthpupostsPublishSpin') {
            vm.fbAuthpupostsPublishSpin = false;
          }
          if (switchId === 'fbAuthmusicRtrnSpin') {
            vm.fbAuthmusicRtrnSpin = false;
          }
          if (switchId === 'fbAuthmoviesRtrnSpin') {
            vm.fbAuthmoviesRtrnSpin = false;
          }
          if (switchId === 'fbAuthbookRtrnSpin') {
            vm.fbAuthbookRtrnSpin = false;
          }
        });
      }
    };

    //checking Facebook Application Auth for the current user
    /**
     * Description
     * @method fbAsyncInit
     * @return 
     */
    window.fbAsyncInit = function () {
      FB.init({
        //appId: '294059841030709',
        appId: '1589011794492434',
        cookie: true, // This is important, it's not enabled by default
        version: 'v2.2'
      });
    };

    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    /*Twitter Oauth Settings*/

    //Adding Oauth Permission
    /**
     * Description
     * @method twitterOauth
     * @return 
     */
    $scope.twitterOauth = function () {
      Auth.twitterOauth().success(function (response) {
        var token = response.msg;
        window.location.href = 'https://twitter.com/oauth/authenticate?oauth_token=' + token;
      }).error(function (err) {
        $mdToast.show({
          template: '<md-toast class="md-toast error">' + "Please try again!" + '</md-toast>',
          hideDelay: 6000,
          position: 'top right'
        });
      });
    };

    //Removing Oauth Permission
    /**
     * Description
     * @method deAuthorizeTwitterOauth
     * @return 
     */
    $scope.deAuthorizeTwitterOauth = function () {
      Auth.deAuthorizeTwitterOauth().success(function (response) {
        vm.twitterAuth = false;
        $mdToast.show({
          template: '<md-toast class="md-toast success">' + "De authorization completed successfully!" + '</md-toast>',
          hideDelay: 6000,
          position: 'top right'
        });
      }).error(function (err) {
        $mdToast.show({
          template: '<md-toast class="md-toast error">' + "Please try again!" + '</md-toast>',
          hideDelay: 6000,
          position: 'top right'
        });
      });
    };

    //  Checking Twitter callback url for Oauth
    var accessTokenUrl = window.location.href;
    var splitUrl = accessTokenUrl.split('?');
    if (splitUrl.length > 1) {
      var tokenString = splitUrl[1];
      var splitTokenString = tokenString.split('&');
      if (splitTokenString.length > 1) {
        var access_tokenString = splitTokenString[0];
        var oauth_verifierString = splitTokenString[1];
        var splitAccess_tokenString = access_tokenString.split('oauth_token=');
        var splitOauth_verifierString = oauth_verifierString.split('oauth_verifier=');
        if (splitAccess_tokenString.length > 1) {
          var oauthToken = splitAccess_tokenString[1];
          if (splitOauth_verifierString.length > 1) {
            var oauthTokenVerifier = splitOauth_verifierString[1];
            if (oauthToken && oauthTokenVerifier) {
              Auth.newTwitterOuth(oauthToken, oauthTokenVerifier).success(function (response) {
                window.history.replaceState(null, null, splitUrl[0]);
                $mdToast.show({
                  template: '<md-toast class="md-toast success">' + 'Twitter Authorization Completed Successfully!' + '</md-toast>',
                  hideDelay: 6000,
                  position: 'top right'
                });
                vm.twitterAuth = true;
                $scope.twitterUserScreenName = response.msg.screen_name;
                $scope.twitterUserImageUrl = 'https://twitter.com/' + response.msg.screen_name + '/profile_image?size=mini';
              }).error(function (err) {
                $mdToast.show({
                  template: '<md-toast class="md-toast error">' + 'Sorry try again!' + '</md-toast>',
                  hideDelay: 6000,
                  position: 'top right'
                });
                window.history.replaceState(null, null, splitUrl[0]);
              });
            }
          }
        }
      }
    }


    /**
     * Adding Instagram Oauth
     * @method instagramOauth
     * @return 
     */
    $scope.instagramOauth = function () {
      window.location.href = 'https://api.instagram.com/oauth/authorize/?client_id=0a08497c207146c4acad6fd457807b42&redirect_uri=https://www.sochara.com/global-setting&response_type=token';
    };

    /**
     * Removing Instagram Oauth
     * @method deAuthorizeInstagramOauth
     * @return 
     */
    $scope.deAuthorizeInstagramOauth = function () {
      Auth.rmInstagramAuth().success(function () {
        $scope.instagramAuth = false;
        $mdToast.show({
          template: '<md-toast class="md-toast success">' + "Instagram Authorization has been removed!" + '</md-toast>',
          hideDelay: 6000,
          position: 'top right'
        });
      }).error(function () {
        $mdToast.show({
          template: '<md-toast class="md-toast error">' + "Sorry! Something went wrong." + '</md-toast>',
          hideDelay: 6000,
          position: 'top right'
        });
      });
    };

    /*Checking Instagram Oauth URL Callback*/
    /*Checking Instagram*/
    var instagramSplitUrl = accessTokenUrl.split('#');
    if (instagramSplitUrl.length > 1) {
      var accessTokenString = instagramSplitUrl[1];
      var instagramAccessToken = accessTokenString.split('access_token=');
      if (instagramAccessToken.length > 1) {
        var hash = instagramAccessToken[1];
        Auth.instagramOauth(hash).success(function (response) {
          $scope.instagramAuth = true;
          $scope.instagramUserImageUrl = response.msg.profilePicture;
          $scope.instagramUserScreenName = response.msg.name;
          showMessage('Authorization successfully completed!');

        }).error(function (err) {
          c('Sorry! Try Again');
        });
      }
    }

    /*Custom Item Area*/

    //Declaring 1st blank item
    vm.contact.profGenSetAddItemAmount = [{label: '', value: ''}];

    //Adding sibling item
    /**
     * Description
     * @method addSiblingItem
     * @return 
     */
    vm.addSiblingItem = function () {
      vm.contact.profGenSetAddItemAmount.push({label: '', value: ''});
      //console.log(vm.contact.profGenSetAddItemAmount);
    };

    //Removing sibling item
    /**
     * Description
     * @method removeSiblingItem
     * @param {} index
     * @return 
     */
    vm.removeSiblingItem = function (index) {
      vm.contact.profGenSetAddItemAmount.splice(index, 1);
      if (vm.contact.profGenSetAddItemAmount.length < 1) {
        vm.contact.profGenSetAddItemAmount.push({label: '', value: ''});
      }
    };

    //Add Item Button Label
    /**
     * Description
     * @method addItemButtonValue
     * @return 
     */
    vm.addItemButtonValue = function () {
      //console.log(vm.contact.profGenSetAddItemAmount);
      if (vm.contact.profGenSetAddItemAmount.length === 1) {
        if (!vm.contact.profGenSetAddItemAmount[0].value && !vm.contact.profGenSetAddItemAmount[0].label) {
          return "Add Item";
        }
        else {
          return "Edit Added Item";
        }
      }
      else {
        return "Edit Added Item";
      }

    };
    /*Adding Custom Item*/
    /**
     * @description adding custom item not available in default mode
     * @return undefined
     */
    vm.tempAddedItem = [];
    /**
     * Description
     * @method addGeneralItem
     * @return 
     */
    vm.addGeneralItem = function () {
      vm.tempAddedItem = [];
      angular.copy(vm.contact.profGenSetAddItemAmount, vm.tempAddedItem);
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/profile/popup/profile_general_settings_add_item.html',
        clickOutsideToClose: true
      });
    };

    //Work Add Item

    //Declaring 1st blank item
    vm.contact.profWorkSetAddItemAmount = [{label: '', value: ''}];

    //Adding sibling item
    /**
     * Description
     * @method addWorkSiblingItem
     * @return 
     */
    vm.addWorkSiblingItem = function () {
      vm.contact.profWorkSetAddItemAmount.push({label: '', value: ''});
    };

    //Removing sibling item
    /**
     * Description
     * @method removeWorkSiblingItem
     * @param {} index
     * @return 
     */
    vm.removeWorkSiblingItem = function (index) {
      vm.contact.profWorkSetAddItemAmount.splice(index, 1);
      if (vm.contact.profWorkSetAddItemAmount.length < 1) {
        vm.contact.profWorkSetAddItemAmount.push({label: '', value: ''});
      }
    };

    //Add Item Button Label
    /**
     * Description
     * @method addWorkItemButtonValue
     * @return 
     */
    vm.addWorkItemButtonValue = function () {
      if (vm.contact.profWorkSetAddItemAmount.length === 1) {
        if (!vm.contact.profWorkSetAddItemAmount[0].value && !vm.contact.profWorkSetAddItemAmount[0].label) {
          return "Add Item";
        }
        else {
          return "Edit Added Item";
        }
      }
      else {
        return "Edit Added Item";
      }
    };
    /*Adding Custom Item*/
    /**
     * @description adding custom item not available in default mode
     * @return undefined
     */
    vm.tempWorkAddedItem = [];
    /**
     * Description
     * @method addWorkItem
     * @return 
     */
    vm.addWorkItem = function () {
      vm.tempWorkAddedItem = [];
      angular.copy(vm.contact.profWorkSetAddItemAmount, vm.tempWorkAddedItem);
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/profile/popup/profile_work_settings_add_item.html',
        clickOutsideToClose: true
      });
    };

    //Contact Add item

    //Declaring 1st blank item
    vm.contact.profcontactSetAddItemAmount = [{label: '', value: ''}];

    //Adding sibling item
    /**
     * Description
     * @method addcontactSiblingItem
     * @return 
     */
    vm.addcontactSiblingItem = function () {
      vm.contact.profcontactSetAddItemAmount.push({label: '', value: ''});
    };

    //Removing sibling item
    /**
     * Description
     * @method removecontactSiblingItem
     * @param {} index
     * @return 
     */
    vm.removecontactSiblingItem = function (index) {
      vm.contact.profcontactSetAddItemAmount.splice(index, 1);
      if (vm.contact.profcontactSetAddItemAmount.length < 1) {
        vm.contact.profcontactSetAddItemAmount.push({label: '', value: ''});
      }
    };

    //Add Item Button Label
    /**
     * Description
     * @method addcontactItemButtonValue
     * @return 
     */
    vm.addcontactItemButtonValue = function () {
      if (vm.contact.profcontactSetAddItemAmount.length === 1) {
        if (!vm.contact.profcontactSetAddItemAmount[0].value && !vm.contact.profcontactSetAddItemAmount[0].label) {
          return "Add Item";
        }
        else {
          return "Edit Added Item";
        }
      }
      else {
        return "Edit Added Item";
      }
    };
    /*Adding Custom Item*/
    /**
     * @description adding custom item not available in default mode
     * @return undefined
     */
    vm.tempcontactAddedItem = [];
    /**
     * Description
     * @method addcontactItem
     * @return 
     */
    vm.addcontactItem = function () {
      vm.tempcontactAddedItem = [];
      angular.copy(vm.contact.profcontactSetAddItemAmount, vm.tempcontactAddedItem);
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/profile/popup/profile_contact_settings_add_item.html',
        clickOutsideToClose: true
      });
    };

    //Realtive Item add
    vm.contact.profrelativeSetAddItemAmount = [{label: '', value: ''}];

    //Adding sibling item
    /**
     * Description
     * @method addrelativeSiblingItem
     * @return 
     */
    vm.addrelativeSiblingItem = function () {
      vm.contact.profrelativeSetAddItemAmount.push({label: '', value: ''});
    };

    //Removing sibling item
    /**
     * Description
     * @method removerelativeSiblingItem
     * @param {} index
     * @return 
     */
    vm.removerelativeSiblingItem = function (index) {
      vm.contact.profrelativeSetAddItemAmount.splice(index, 1);
      if (vm.contact.profrelativeSetAddItemAmount.length < 1) {
        vm.contact.profrelativeSetAddItemAmount.push({label: '', value: ''});
      }
    };

    //Add Item Button Label
    /**
     * Description
     * @method addrelativeItemButtonValue
     * @return 
     */
    vm.addrelativeItemButtonValue = function () {
      if (vm.contact.profrelativeSetAddItemAmount.length === 1) {
        if (!vm.contact.profrelativeSetAddItemAmount[0].value && !vm.contact.profrelativeSetAddItemAmount[0].label) {
          return "Add Item";
        }
        else {
          return "Edit Added Item";
        }
      }
      else {
        return "Edit Added Item";
      }
    };
    /*Adding Custom Item*/
    /**
     * @description adding custom item not available in default mode
     * @return undefined
     */
    vm.temprelativeAddedItem = [];
    /**
     * Description
     * @method addrelativeItem
     * @return 
     */
    vm.addrelativeItem = function () {
      vm.temprelativeAddedItem = [];
      angular.copy(vm.contact.profrelativeSetAddItemAmount, vm.temprelativeAddedItem);
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/profile/popup/profile_relative_settings_add_item.html',
        clickOutsideToClose: true
      });
    };

    /**
     * Checking Relative from Contact
     * @method relativeFromContact
     * @param {} relative
     * @return 
     */
    vm.relativeFromContact = function (relative) {
    };

    vm.contacts = [];

    /**
     * Description
     * @method getContacts
     * @return 
     */
    function getContacts() {
      ProfileService.loadContact().then(function (response) {
        vm.contacts = response;
      });
    }

    vm.selectContactForAdd = selectContactForAdd;
    vm.relativesUserArray = [];

    /**
     * Description
     * @method selectContactForAdd
     * @param {} contact
     * @return 
     */
    function selectContactForAdd(contact) {
      vm.relativesUserInfo = contact;
      console.log(vm.relativesUserInfo);
      /* serializeRelativesUser(vm.relativesUserArray, function (returnUsers) {
         vm.selectedContactIdForAdd = returnUsers;
         if (vm.selectedContactIdForAdd.indexOf(contact.id) === -1) {
           vm.relativesUserArray.push(contact);
           vm.selectedContactIdForAdd.push(contact.id);
         }
       });*/
      vm.searchedContact = null;
    }

    vm.relativeType = ['Mother', 'Father', 'Parents', 'Brother', 'Sister', 'Son', 'Daughter', 'Wife', 'Husband', 'Spouse', 'Partner', 'Assistant', 'Manager'];
    vm.saveRelative = saveRelative;

    /**
     * Description
     * @method saveRelative
     * @return 
     */
    function saveRelative() {
      var relativeInfo = {};
      relativeInfo.type = vm.selectedRelativeType;
      relativeInfo.relative = vm.relativesUserInfo;
      relativeInfo.visibility = vm.relativesUserInfo.visibility;
      ProfileService.saveRelative(relativeInfo).then(function (response) {
        if (response) {
          vm.relativesUserInfo = null;
          closeDialog();
          getAllRelativeInfo();
          showMessage("Relative information added successfully!");
        }
      });
    }

    vm.relativePrivecySettings = relativePrivecySettings;
    vm.setRelativePrivecySettingsIcon = setRelativePrivecySettingsIcon;

    /**
     * Description
     * @method relativePrivecySettings
     * @param {} type
     * @return 
     */
    function relativePrivecySettings(type) {
      // vm.visibility = type;
      vm.relativesUserInfo.visibility = type;
    }

    /**
     * Description
     * @method setRelativePrivecySettingsIcon
     * @param {} privacy
     * @return 
     */
    function setRelativePrivecySettingsIcon(privacy) {
      if (privacy === 'friends') {
        return 'icon-account-multiple';
      } else if (privacy === 'onlyMe') {
        return 'icon-eye-off';
      } else {
        return 'icon-earth';
      }
    }

    // vm.getAllRelativeInfo = getAllRelativeInfo;
    vm.allRelativesInfo = [];

    /**
     * Description
     * @method getAllRelativeInfo
     * @return 
     */
    function getAllRelativeInfo() {
      ProfileService.getAllRelativeInfo().then(function (response) {
        vm.allRelativesInfo = response;
        console.log(response);
      });
    }

    vm.deleteRelative = deleteRelative;
    vm.deleteItemsConfirm = deleteItemsConfirm;

    /**
     * Description
     * @method deleteRelative
     * @param {} id
     * @return 
     */
    function deleteRelative(id) {
      vm.deleteRelativeItemId = id;
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/profile/dialogs/delete.html',
        parent: angular.element($document.body),
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method deleteItemsConfirm
     * @return 
     */
    function deleteItemsConfirm() {
      if (vm.deleteRelativeItemId) {
        ProfileService.deleteRelative(vm.deleteRelativeItemId).then(function (response) {
          vm.deleteRelativeItemId = null;
          closeDialog();
          showMessage('The Reletive information has been deleted.');
          getAllRelativeInfo();
        });
      } else {
        showMessage('The Reletive information you want do delete is not found!');
      }
    }

    vm.updateRelative = updateRelative;

    /**
     * Description
     * @method updateRelative
     * @param {} id
     * @return 
     */
    function updateRelative(id) {
      vm.updateRelativeItemId = id;
      ProfileService.getRelativeInfo(id).then(function (response) {
        vm.relativesUserInfo = {};
        vm.selectedRelativeType = response.type;
        vm.relativesUserInfo = response.relative;
        vm.relativesUserInfo.visibility = response.visibility;
        console.log(vm.relativesUserInfo);
      });
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/profile/dialogs/update_profile_relatives_settings.html',
        parent: angular.element($document.body),
        clickOutsideToClose: false
      });
    }

    vm.updateRelativeSubmit = updateRelativeSubmit;

    /**
     * Description
     * @method updateRelativeSubmit
     * @return 
     */
    function updateRelativeSubmit() {
      var relativeData = {};
      relativeData.id = vm.updateRelativeItemId;
      relativeData.type = vm.selectedRelativeType;
      relativeData.relative = vm.relativesUserInfo.id;
      relativeData.visibility = vm.relativesUserInfo.visibility;
      console.log(relativeData);
      ProfileService.updateRelative(relativeData).then(function (returnData) {
        if (returnData) {
          vm.updateRelativeItemId = null;
          vm.relativesUserInfo = null;
          closeDialog();
          showMessage('Relative has been updated successfully!');
          getAllRelativeInfo();
        }
      });
    }
  }

})();
