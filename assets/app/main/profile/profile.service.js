(function () {
  'use strict';

  angular
    .module('sochara')
    .factory('ProfileService', profileService);

  /** @ngInject */
  function profileService($http, $q) {
    return {
      saveRelative: function (data) {
        var defer = $q.defer();
        $http.post('/profile/saveRelative', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      loadContact: function () {
        var defer = $q.defer();
        $http.post('/json/users').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getAllRelativeInfo: function () {
        var defer = $q.defer();
        $http.post('/profile/getAllRelativeInfo').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getRelativeInfo: function ( id ) {
        var defer = $q.defer(); var data = {}; data.id = id;
        $http.post('/profile/getRelativeInfo', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      updateRelative: function ( data ) {
        var defer = $q.defer();
        $http.post('/profile/updateRelative', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      deleteRelative: function ( id ) {
        var defer = $q.defer(); var data = {}; data.id = id;
        $http.post('/profile/deleteRelative', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      }

    };
  }

})();
