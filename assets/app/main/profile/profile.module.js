(function () {
  'use strict';

  angular
    .module('app.profile', ['ngImgCrop'])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    $stateProvider.state('app.profile', {
      url: '/profile',
      views: {
        'content@app': {
          templateUrl: 'app/main/profile/profile.html',
          controller: 'ProfileController as vm'
        }
      },
      resolve: {},
      bodyClass: 'profile'
    });


  }

})();

