(function ()
{
    'use strict';

    angular
        .module('app.profileDemo')
        .controller('SettingPopupController', SettingPopupController);

    /** @ngInject */
    function SettingPopupController($mdDialog, $mdToast, $scope, $location, data1, data2) {
        var vm = this;
        vm.closeDialog = closeDialog;
        function closeDialog() {
            $mdDialog.hide();
        }
    }
});