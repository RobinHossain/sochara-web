(function () {
  'use strict';

  angular
    .module('sochara')
    .factory('PollService', pollService);

  /** @ngInject */
  function pollService($http, $q) {
    return {
      save: function (data) {
        var defer = $q.defer();
        $http.post('/poll/save', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      saveCustomDesign: function (data) {
        var defer = $q.defer();
        $http.post('/poll/saveCustomDesign', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getPolls: function () {
        var defer = $q.defer();
        $http.post('/poll/getPolls').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getCustomDesigns: function () {
        var defer = $q.defer();
        $http.post('/poll/getCustomDesigns').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      loadContact: function () {
        var defer = $q.defer();
        $http.post('/json/users').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      pollCount: function () {
        var defer = $q.defer();
        $http.post('/poll/pollCount').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getDraft: function (id) {
        var defer = $q.defer();
        var data = {};
        data.id = id;
        $http.post('/poll/getDraft', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      loadCustomDesign: function (id) {
        var defer = $q.defer();
        var data = {};
        data.id = id;
        $http.post('/poll/loadCustomDesign', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      loadPublicPollPreview: function (id) {
        var defer = $q.defer();
        var data = {};
        data.id = id;
        $http.post('/poll/loadPublicPollPreview', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      deletePoll: function (id) {
        var defer = $q.defer();
        var data = {};
        data.id = id;
        $http.post('/poll/deletePoll', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      deleteDesginItemsConfirm: function (id) {
        var defer = $q.defer();
        var data = {};
        data.id = id;
        $http.post('/poll/deleteDesginItemsConfirm', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      savePollVote: function (data) {
        var defer = $q.defer();
        $http.post('/poll/saveVote', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      checkIfAlreadyVoted: function (id) {
        var defer = $q.defer();
        var data = {};
        data.id = id;
        $http.post('/poll/checkIfAlreadyVoted', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getPollAnswer: function (id) {
        var defer = $q.defer();
        var data = {};
        data.id = id;
        $http.post('/poll/getPollById', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
    };
  }

})();
