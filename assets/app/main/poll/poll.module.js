(function () {
  'use strict';

  angular
    .module('app.poll',
      [
        // 3rd Party Dependencies
        // 'xeditable',
        'ngDraggable',
        'ngFileUpload',
        'ngImgCrop',
        'mdPickers',
        // 'md.time.picker',
        'ngMaterial'
      ]
    )
    .config(config);

  /** @ngInject */
  function config($stateProvider) {

    $stateProvider
      .state('app.poll', {
        url: '/poll',
        views: {
          'content@app': {
            templateUrl: 'app/main/poll/poll.html',
            controller: 'pollController as vm'
          }
        }
      })
      .state('app.poll.preview', {
        url: '/:pollId',
        views: {
          'content@app': {
            templateUrl: 'app/main/poll/preview.html',
            controller: 'pollController as vm'
          }
        }
      });


  }

})();
