(function () {
  'use strict';


  angular
    .module('app.poll')
    .controller('pollController', PollController);

  /**
   * Description
   * @ngInject 
   * @method PollController
   * @param {} $scope
   * @param {} $mdSidenav
   * @param {} $state
   * @param {} $mdDialog
   * @param {} PollService
   * @param {} $rootScope
   * @param {} ContactService
   * @param {} $document
   * @param {} $q
   * @param {} $mdToast
   * @param {} Upload
   * @param {} $timeout
   * @param {} $location
   * @return 
   */
  function PollController($scope, $mdSidenav, $state, $mdDialog, PollService, $rootScope, ContactService, $document, $q, $mdToast, Upload, $timeout, $location) {

    var vm = this;
    /////////////

    vm.filterIds = null;
    vm.listType = 'all';
    vm.listOrder = 'name';
    vm.listOrderAsc = false;
    vm.selectedContacts = [];
    vm.newGroupName = '';
    var currentDate = new Date();

    vm.inputAnswers = [0];

    vm.answers = {fields: [], fieldImage: []};
    vm.currentTabIndex = 0;


    // Methods
    vm.increaseAnswerInput = increaseAnswerInput;
    vm.gotoNextQuestion = gotoNextQuestion;
    vm.deleteAnswer = deleteAnswer;
    vm.textFeatureOnly = true;
    vm.textAndMediaFeature = true;
    vm.mediaClass = null;
    vm.savePoll = savePoll;
    vm.getBackgroundImageAcceptRatio = getBackgroundImageAcceptRatio;
    vm.setCurrentFont = setCurrentFont;
    vm.setCurrentFontSize = setCurrentFontSize;
    vm.changeFontStyleBold = changeFontStyleBold;
    vm.changeFontStyleItalic = changeFontStyleItalic;
    vm.changeFontStyleUnderline = changeFontStyleUnderline;
    vm.changeTextAlign = changeTextAlign;
    vm.changeGradientColor = changeGradientColor;
    vm.changePollStyle = changePollStyle;
    vm.clearOtherBackground = clearOtherBackground;
    vm.site_url = $location.protocol() + '://' + $location.host() + ':' + $location.port();
    vm.getThumbImage = getThumbImage;
    vm.loadDraftPoll = loadDraftPoll;
    vm.initPollObjects = initPollObjects;
    vm.deletePoll = deletePoll;
    vm.deleteItemsConfirm = deleteItemsConfirm;
    vm.getAndInputFiles = getAndInputFiles;
    vm.closeDialog = closeDialog;
    vm.savePollVote = savePollVote;
    vm.selectContactForShare = selectContactForShare;
    vm.deleteContactFromShare = deleteContactFromShare;
    vm.pollDisplayVoting = false;
    vm.pollDisplayVotingChart = false;
    vm.initPollResult = initPollResult;
    vm.customizeDesign = false;
    // vm.initPollResultChart = initPollResultChart;
    vm.initPollVoteGreeting = initPollVoteGreeting;
    vm.voteGreetingShow = false;
    vm.cancelCustomDesign = cancelCustomDesign;
    vm.saveCustomDesign = saveCustomDesign;
    vm.allDraftPoll = allDraftPoll;
    vm.getCustomDesigns = getCustomDesigns;
    vm.loadCustomDesign = loadCustomDesign;
    vm.deleteCustomDesign = deleteCustomDesign;
    vm.deleteDesginItemsConfirm = deleteDesginItemsConfirm;
    vm.addEmailToSharedPoll = addEmailToSharedPoll;
    vm.uploadFile = uploadFile;
    vm.putFilesByExt = putFilesByExt;
    vm.changeBackgroundOption = changeBackgroundOption;
    vm.backToPreviousLink = backToPreviousLink;
    vm.peopleSearch = peopleSearch;
    vm.selectedPeopleChange = selectedPeopleChange;
    vm.removePeopleFromShare = removePeopleFromShare;
    vm.changePollFontStyle = changePollFontStyle;

    vm.fontSizeSelect = ['10px', '12px', '14px', '16px', '18px', '20px', '22px', '24px', '26px','30px'];

    init();

    /**
     * Description
     * @method init
     * @return 
     */
    function init() {
      $rootScope.searchable = false;
      $rootScope.loadingProgress = true;
      initPollObjects();
      vm.loadingPoll = true;
      vm.pollId = $state.params.pollId || '';
      $timeout(function () {
        if (vm.pollId) {
          PollService.checkIfAlreadyVoted(vm.pollId).then(function (response) {
            if (response.voted) {
              vm.initPollResult();
            } else {
              vm.pollDisplayVoting = true;
              loadPublicPollPreview(vm.pollId);
            }
          });
        } else {
          getCustomDesigns();
          getPolls();
          getContacts();
          getPollCount();
        }
      }, 2000);

    }

    // ********************** Store Default Styles Value For Poll *****************************************************************

    /**
     * Description
     * @method initPollObjects
     * @param {} init
     * @return 
     */
    function initPollObjects(init) {
      vm.poll = {};
      vm.poll.show_vote_result = false;
      vm.poll.show_vote_result_when_closed = false;
      vm.poll.allow_vote_again = 'never';
      vm.poll.ip_restriction = false;
      vm.poll.styles = {
        class: 'default',
        color: '#ffffff',
        bgColor: '#0195C0',
        bgImage: null,
        font: '',
        fontSize: '15px',
        align: 'left',
        borderRadius: '',
        bold: false,
        fontStyle: '',
        underline: ''
      };
      vm.poll.bgOption = 'color';
      vm.bgOptionColor = true;
      vm.setRadioCheckboxCurr = 'radio';
      vm.conCheckboxActiveClass = conCheckboxActiveClass;
      vm.conRadioActiveClass = conRadioActiveClass;

      vm.poll.styles.gradient = {};
      vm.poll.styles.gradient.gradientType = 'top';
      vm.poll.styles.gradient.colorOne = '#e73333';
      vm.poll.styles.gradient.colorTwo = '#d96d6d';

      vm.poll.expireDate = currentDate;
      vm.poll.expireTime = currentDate;
      vm.poll.eventDate = currentDate;
      vm.poll.eventTime = currentDate;
      vm.poll.sendNextTime = false;

      vm.pollSharedUser = [];

      vm.poll.answers = [];

      vm.inputAnswers = [0];

      if (init) {
        vm.changeBgImage = null;
        vm.currentTabIndex = 1;
      }

      vm.selectedContactIdForShare = [];
    }

    /**
     * Description
     * @method changePollFontStyle
     * @param {} font
     * @return 
     */
    function changePollFontStyle(font){
      font = font.name;
      $scope.$apply(function(){ vm.poll.styles.font = font;});
    }

    /**
     * Description
     * @method closeDialog
     * @return 
     */
    function closeDialog() {
      $mdDialog.hide();
    }

    /**
     * Description
     * @method loadPublicPollPreview
     * @param {} id
     * @return 
     */
    function loadPublicPollPreview(id) {
      PollService.loadPublicPollPreview(id).then(function (response) {
        // console.log(response);
        vm.poll = response;
        vm.pollStyle = response.styles;
        if (response.polltype && response.polltype === 'checkbox') {
          vm.setRadioCheckboxCurr = 'checkbox';
        }

        if (!response.gradient && response.background) {
          vm.changeBgImage = vm.site_url + response.background.file_dir;
          vm.changeBgImageCropped = vm.site_url + response.background.file_dir;
        } else {
          vm.changeBgImageCropped = '';
        }
      });
    }


    // ********** End # Store Default Styles Value For Poll *************************************************************************

    /**
     * Description
     * @method getContacts
     * @return 
     */
    function getContacts() {
      PollService.loadContact().then(function (response) {
        vm.contacts = response;
        vm.filteredContacts = response.map(function (c) {
          var name = c.first_name + ' ' + c.last_name;
          return {
            value: name.toLowerCase(),
            name: name,
            email: c.email,
            photo_url: c.photo_url,
            id: c.id || undefined,
          };
        });
      });
    }

    /**
     * Description
     * @method loadDraftPoll
     * @param {} draft
     * @return 
     */
    function loadDraftPoll(draft) {
      PollService.getDraft(draft.id).then(function (response) {
        vm.inputAnswers = new Array(response.answers.length + 1);
        vm.currentTabIndex = 1;
        vm.poll = response;
        vm.pollSharedUser = vm.poll.users;
        vm.poll.expireDate = new Date(vm.poll.expire_on);
        vm.poll.expireTime = new Date(vm.poll.expire_on);
        vm.poll.eventDate = vm.poll.event_date ? new Date(vm.poll.event_date) : new Date();
        vm.poll.eventTime = vm.poll.event_date ? new Date(vm.poll.event_date) : new Date();

        if (!response.gradient && response.background) {
          vm.changeBgImage = vm.site_url + response.background.file_dir;
        } else {
          vm.changeBgImageCropped = '';
        }

        setTimeout(function () {
          for (var x = 1; x <= response.answers.length; x++) {
            getAndInputFiles(null, x);
          }
        }, 1000);

      });
    }

    /**
     * Description
     * @method deletePoll
     * @param {} id
     * @return 
     */
    function deletePoll(id) {
      vm.deletePollItemId = id;
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/poll/dialogs/delete.html',
        parent: angular.element($document.body),
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method backToPreviousLink
     * @return 
     */
    function backToPreviousLink(){
      window.history.back();
    }

    /**
     * Description
     * @method peopleSearch
     * @param {} query
     * @return MemberExpression
     */
    function peopleSearch(query) {
      vm.previousSearchText = query;
      var results = query ? ContactService.getAllSocharaUsers({name: query}).then(function (response) {
        var totalResponse = response.map(function (c) {
          var name = c.first_name + ' ' + c.last_name;
          return{
            name: name,
            email: c.email,
            photo_url: c.photo_url ? c.photo_url.replace('assets', '').replace(/\\/, '/') : '../images/avatars/blank.jpg',
            id: c.id || undefined,
            value: name.toLowerCase()
          };
        });

        var searchQuery = angular.lowercase(query);

        totalResponse = totalResponse.concat(vm.filteredContacts);
        return totalResponse.filter(function ( state ) {
          return state.value.indexOf(searchQuery) === 0 && vm.selectedContactIdForShare.indexOf(state.id) < 0;
        });
        // return totalResponse;
      }) : vm.filteredContacts.filter(function (cont) {
        return !cont.id || vm.selectedContactIdForShare.indexOf(cont.id) < 0;
      });

      var deferred = $q.defer();
      $timeout(function () {
        deferred.resolve(results);
      }, Math.random() * 1000, false);
      return deferred.promise;
    }

    /**
     * Description
     * @method selectedPeopleChange
     * @param {} resp
     * @return 
     */
    function selectedPeopleChange(resp) {
      if (!resp) {
        return;
      }
      vm.selectedContactIdForShare = vm.selectedContactIdForShare || [];
      var findIndex = vm.selectedContactIdForShare.indexOf(resp.id);
      if (findIndex > -1) {
        showMessage('This Contact already added', 'error');
      } else {
        vm.pollSharedUser.push(resp);
        vm.selectedContactIdForShare.push(resp.id);
      }
      vm.searchText = vm.previousSearchText;
      vm.selectedItem = undefined;
      // document.querySelector('#input-11').blur();
      var peopleSearchInput = angular.element("input#searchPeople");
      if (peopleSearchInput) {
        peopleSearchInput.blur();
        peopleSearchInput.focus();
      }
    }

    /**
     * Description
     * @method removePeopleFromShare
     * @param {} id
     * @return 
     */
    function removePeopleFromShare( id ){
      var findIndex = vm.selectedContactIdForShare.indexOf(id);
      vm.selectedContactIdForShare.splice(findIndex,1);
    }

    /**
     * Description
     * @method changeBackgroundOption
     * @param {} type
     * @param {} val
     * @return 
     */
    function changeBackgroundOption(type, val){
      if(type === 'color' && val){
        vm.bgOptionGradient = vm.bgOptionImage = false;
        vm.poll.bgOption = 'color';
        vm.poll.styles.bgColor = '#0195C0';
      }else if(type === 'gradient' && val){
        vm.bgOptionColor = vm.bgOptionImage = false;
        vm.poll.bgOption = 'gradient';
        vm.poll.styles.bgColor = '-webkit-linear-gradient(' + vm.poll.styles.gradient.gradientType + ',' +
          vm.poll.styles.gradient.colorOne + ',' + vm.poll.styles.gradient.colorTwo + ')';
      }else if(type === 'image' && val){
        vm.bgOptionGradient = vm.bgOptionColor = false;
        vm.poll.bgOption = 'image';
      }else{
        vm.poll.bgOption = 'color';
        vm.bgOptionColor = true;
      }
    }

    /**
     * Description
     * @method deleteItemsConfirm
     * @return 
     */
    function deleteItemsConfirm() {
      if (vm.deletePollItemId) {
        PollService.deletePoll(vm.deletePollItemId).then(function (response) {
          vm.deletePollItemId = null;
          closeDialog();
          showMessage('The poll has been deleted.');
          getPolls();
          getPollCount();
        });
      } else {
        showMessage('The Poll you want do delete is not found !');
      }
    }


    /**
     * Description
     * @return
     * @method getAndInputFiles
     * @param {} e
     * @param {} index
     * @return 
     */
    function getAndInputFiles(e, index) {
      var flavel = e + 1;
      var gtTmpUrl = "#answers > div:nth-child(" + flavel + ") > label > div.tmpImgvid";
      var elink = angular.element(gtTmpUrl).text() || '';
      putFilesByExt(elink, e);
    }

    /**
     * Description
     * @method putFilesByExt
     * @param {} elink
     * @param {} e
     * @return 
     */
    function putFilesByExt(elink, e){
      var flavel = e + 1;
      if(!elink) elink = '';
      var gtCurrentUrl = "#answers > div:nth-child(" + flavel + ") > label > div.imgVid";
      var fileExt = elink.split('.').pop();
      if (fileExt === 'png' || fileExt === 'jpeg' || fileExt === 'gif' || fileExt === 'jpg') {
        var imgLink = "<img src='" + elink + "' alt='image' width='200px' />";
        angular.element(gtCurrentUrl).html(imgLink);
      } else if (fileExt === 'pdf' || fileExt === 'doc' || fileExt === 'docs') {
        var docLink, imgvidT;
        if (fileExt === 'pdf') {
          docLink = "<a href='" + elink + "' target='_blank'><img src='http://res.cloudinary.com/aan-nahl/image/upload/pdf.png' alt='pdf' /></a>";
          imgvidT = 'pdf';
        } else {
          docLink = "<a href='" + elink + "' target='_blank'><img src='http://res.cloudinary.com/aan-nahl/image/upload/doc.png' alt='pdf' /></a>";
          imgvidT = 'doc';
        }
        angular.element(gtCurrentUrl).html(docLink);
      }
      else if (elink.indexOf("youtube") > -1 || elink.indexOf("youtu") > -1) {
        var yId = youtube_parser(elink);
        var vidLink = '<iframe width="200" height="160" src="https://www.youtube.com/embed/' + yId + '" frameborder="0" allowfullscreen></iframe>';
        angular.element(gtCurrentUrl).html(vidLink);
      } else {
        angular.element(gtCurrentUrl).html(elink);
      }
    }

    /**
     * Description
     * @return
     * @method youtube_parser
     * @param {} url
     * @return 
     */
    function youtube_parser(url) {
      var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
      var match = url.match(regExp);
      if (match && match[2].length === 11) {
        return match[2];
      }
    }

    /**
     * Description
     * @return
     * @method increaseAnswerInput
     * @return 
     */
    function increaseAnswerInput() {
      var iLength = angular.element("#choiceAnswer > div.inputAns").length;

      var length;
      if (iLength === 1) {
        length = angular.element("#choiceAnswer > div:first-child > input").val().length;
      } else {
        length = angular.element("#choiceAnswer > div:nth-child(" + iLength + ") > input").val().length;
      }

      if (length > 0) {
        vm.inputAnswers.push(0);
        if (vm.textFeatureOnly === true) {
          addTextFeatureOnly();
        } else {
          addTextAndMediaFeature();
        }
      }
    }

    /**
     * Description
     * @method savePoll
     * @param {} param
     * @return 
     */
    function savePoll(param) {
      $rootScope.loadingProgress = true;
      uploadImage(vm.changeBgImage, function (imageData) {
        generateImage(function (imageDataUrl) {
          if (!vm.poll.question) {
            showMessage('Please Input poll question!');
          } else {
            uploadCanvas(imageDataUrl, function (canvasData) {
              vm.poll.draft = Boolean(param);
              if (vm.selectedContactIdForShare) {
                vm.poll.users = vm.selectedContactIdForShare;
                vm.poll.shared = vm.selectedContactIdForShare;
              }
              vm.poll.file = canvasData.id || '';
              if (imageData) {
                vm.poll.background = vm.site_url + imageData.file_dir;
              }
              vm.poll.polltype = vm.setRadioCheckboxCurr;
              PollService.save(vm.poll).then(function (response) {
                $rootScope.loadingProgress = false;
                if (response) {
                  initPollObjects();
                  $timeout(function () {
                    vm.currentTabIndex = 0;
                    showMessage('The poll has been saved');
                    getPolls();
                    getPollCount();
                  });
                }
              });
            });
          }
        });

      });
    }

    /**
     * Description
     * @method getPolls
     * @return 
     */
    function getPolls() {
      PollService.getPolls().then(function (response) {
        vm.loadingPoll = false;
        $rootScope.loadingProgress = false;
        if (response) {
          vm.allPolls = response;
        }
      });
    }

    /**
     * Description
     * @method getPollCount
     * @return 
     */
    function getPollCount() {
      PollService.pollCount().then(function (response) {
        if (response) {
          vm.pollCount = response;
        }
      });
    }

    /**
     * Description
     * @method getThumbImage
     * @param {} file
     * @return url
     */
    function getThumbImage(file) {
      var url = '';
      if (file && file.thumb_dir) {
        url = vm.site_url + file.thumb_dir;
      } else {
        url = vm.site_url + '/images/icons/album_cover.png';
      }
      return url;
    }

    /**
     * Description
     * @return
     * @method addTextFeatureOnly
     * @return 
     */
    function addTextFeatureOnly() {
      var cCls = angular.element("#addTextMediaFeature").find("i.fa-text-width");
      var aCls = angular.element("#addTextMediaFeature").find("i.fa-image");
      if (!angular.element(cCls).hasClass("active")) {
        angular.element(aCls).removeClass("active");
        angular.element(cCls).addClass("active");
        vm.textAndMediaFeature = false;
        vm.textFeatureOnly = true;
        vm.mediaClass = null;
      }
    }


    /**
     * Description
     * @return
     * @method addTextAndMediaFeature
     * @return 
     */
    function addTextAndMediaFeature() {
      var iCls = angular.element("#addTextMediaFeature").find("i.fa-text-width");
      var aCls = angular.element("#addTextMediaFeature").find("i.fa-image");
      if (!angular.element(aCls).hasClass("active")) {
        angular.element(iCls).removeClass("active");
        angular.element(aCls).addClass("active");
        vm.textFeatureOnly = false;
        vm.textAndMediaFeature = true;
        vm.mediaClass = 'halfAns';
      }

      var iLength = angular.element("#choiceAnswer > div.inputAns").length;

      var length;
      if (iLength === 1) {
        length = angular.element("#choiceAnswer > div:first-child > input").val().length;
      } else {
        length = angular.element("#choiceAnswer > div:nth-child(" + iLength + ") > input").val().length;
      }
    }


    /**
     * Description
     * @return
     * @method gotoNextQuestion
     * @param {} e
     * @return 
     */
    function gotoNextQuestion(e) {
      angular.element('.inputAns input.ansTxt,.inputAns input.ansImg').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 13) {
          e.preventDefault();
          // return false;
          // Get all focusable elements on the page
          var $canfocus = angular.element(':focusable');
          var index = $canfocus.index(this) + 1;
          if (index >= $canfocus.length) index = 0;
          $canfocus.eq(index).focus();
        }
      });
    }

    /**
     * Description
     * @method selectContactForShare
     * @param {} contact
     * @return 
     */
    function selectContactForShare(contact) {
      serializeSharedUser(vm.pollSharedUser, function (returnUsers) {
        vm.selectedContactIdForShare = returnUsers;
        if (vm.selectedContactIdForShare.indexOf(contact.id) === -1) {
          vm.pollSharedUser.push(contact);
          vm.selectedContactIdForShare.push(contact.id);
        }
      });
      vm.searchedContact = null;
    }

    /**
     * Description
     * @method deleteContactFromShare
     * @param {} contact
     * @return 
     */
    function deleteContactFromShare(contact) {
      serializeSharedUser(vm.pollSharedUser, function (returnUsers) {
        vm.selectedContactIdForShare = returnUsers;
        var userPosition = vm.selectedContactIdForShare.indexOf(contact.id);
        var userPositionIndex = vm.pollSharedUser.indexOf(contact);
        if (userPosition > -1) {
          vm.selectedContactIdForShare.splice(userPosition, 1);
          vm.pollSharedUser.splice(userPositionIndex, 1);
        }
      });
    }


    /**
     * Description
     * @method serializeSharedUser
     * @param {} users
     * @param {} callback
     * @return 
     */
    function serializeSharedUser(users, callback) {
      var userData = [];
      if (users && users.length) {
        users.forEach(function (user) {
          userData.push(user.id);
        });
      }
      callback(userData);
    }


    /**
     * Description
     * @return
     * @method deleteAnswer
     * @param {} index
     * @return 
     */
    function deleteAnswer(index) {

      var iLength = angular.element("#choiceAnswer > div > input").length;

      var iLengthTxt = angular.element("#choiceAnswer > div > input.ansTxt").length;

      if (iLengthTxt > 1) {

      }


      vm.poll.answers.splice(index, 1);
      if (iLengthTxt > 1) {
        vm.inputAnswers.pop();
      }
    }

    /**
     * Description
     * @method getBackgroundImageAcceptRatio
     * @return 
     */
    function getBackgroundImageAcceptRatio() {
      var cardWidth = angular.element('#poll-preview-content').width();
      var cardHeigth = angular.element('#poll-preview-content').height();
      if (cardWidth && cardHeigth) {
        vm.cardBackgroundImageWidth = cardWidth;
        vm.cardBackgroundImageHeight = cardHeigth;
        vm.cardBackgroundRatio = cardWidth / cardHeigth;
      }
    }

    /**
     * Description
     * @method clearOtherBackground
     * @return 
     */
    function clearOtherBackground() {
      vm.poll.gradient = false;
      vm.poll.bgOption = 'image';
    }

    /**
     * Description
     * @method setCurrentFont
     * @param {} font
     * @return 
     */
    function setCurrentFont(font) {
      vm.poll.styles.font = font;
    }

    /**
     * Description
     * @method setCurrentFontSize
     * @param {} fontSize
     * @return 
     */
    function setCurrentFontSize(fontSize) {
      vm.poll.styles.fontSize = fontSize;
    }

    /**
     * Change font style
     * @return
     * @method changeFontStyleBold
     * @return 
     */
    function changeFontStyleBold() {
      angular.element(".preview-toolbar-bold").toggleClass('preview-toolbar-active');

      if (vm.poll.styles.bold == true) {
        vm.poll.styles.bold = '';
      } else {
        vm.poll.styles.bold = true;
      }
    }

    /**
     * Description
     * @return
     * @method changeFontStyleItalic
     * @return 
     */
    function changeFontStyleItalic() {
      angular.element(".preview-toolbar-italic").toggleClass('preview-toolbar-active');
      if (vm.poll.styles.fontStyle == 'italic') {
        vm.poll.styles.fontStyle = '';
      } else {
        vm.poll.styles.fontStyle = 'italic';
      }
    }

    /**
     * Description
     * @return
     * @method changeFontStyleUnderline
     * @return 
     */
    function changeFontStyleUnderline() {
      angular.element(".preview-toolbar-underline").toggleClass('preview-toolbar-active');
      if (vm.poll.styles.underline == 'underline') {
        vm.poll.styles.underline = '';
      } else {
        vm.poll.styles.underline = 'underline';
      }
    }

    /**
     * Change answer text alignment
     * @return
     * @method changeTextAlign
     * @param {} align
     * @return 
     */
    function changeTextAlign(align) {
      if (align == 1) {
        angular.element(".preview-toolbar-left").addClass('preview-toolbar-active');
        angular.element(".preview-toolbar-center").removeClass('preview-toolbar-active');
        angular.element(".preview-toolbar-right").removeClass('preview-toolbar-active');
        angular.element(".preview-toolbar-justify").removeClass('preview-toolbar-active');
        vm.poll.styles.align = 'left';
      } else if (align == 2) {
        angular.element(".preview-toolbar-center").addClass('preview-toolbar-active');
        angular.element(".preview-toolbar-right").removeClass('preview-toolbar-active');
        angular.element(".preview-toolbar-justify").removeClass('preview-toolbar-active');
        angular.element(".preview-toolbar-left").removeClass('preview-toolbar-active');

        vm.poll.styles.align = 'center';
      } else if (align == 3) {
        angular.element(".preview-toolbar-right").addClass('preview-toolbar-active');
        angular.element(".preview-toolbar-left").removeClass('preview-toolbar-active');
        angular.element(".preview-toolbar-center").removeClass('preview-toolbar-active');
        angular.element(".preview-toolbar-justify").removeClass('preview-toolbar-active');

        vm.poll.styles.align = 'right';
      } else if (align == 4) {
        angular.element(".preview-toolbar-justify").addClass('preview-toolbar-active');
        angular.element(".preview-toolbar-right").removeClass('preview-toolbar-active');
        angular.element(".preview-toolbar-center").removeClass('preview-toolbar-active');
        angular.element(".preview-toolbar-left").removeClass('preview-toolbar-active');

        vm.poll.styles.align = 'justify';
      }
    }

    /**
     * Description
     * @method changeBaakgroundColor
     * @return 
     */
    vm.changeBaakgroundColor = function () {
      vm.bgOptionGradient = vm.bgOptionImage = false;
      vm.bgOptionColor = true;
      vm.poll.bgOption = 'color';
    };
    /**
     * Description
     * @method changeTextColor
     * @return 
     */
    vm.changeTextColor = function () {
    };

    vm.gradientOptions = [
      {values: "top", field: "Lighter from the top"},
      {values: "right", field: "Lighter from the right"},
      {values: "left", field: "Lighter from the left"},
      {values: "bottom", field: "Lighter from the bottom"},
    ];


    /**
     * Change gradient Color
     * @return
     * @method changeGradientColor
     * @return 
     */
    function changeGradientColor() {
      vm.poll.gradient = true;
      vm.poll.bgOption = 'gradient';
      vm.changeBgImageCropped = '';
      vm.changeBgImage = null;
      vm.poll.styles.bgColor = '-webkit-linear-gradient(' + vm.poll.styles.gradient.gradientType + ',' +
        vm.poll.styles.gradient.colorOne + ',' + vm.poll.styles.gradient.colorTwo + ')';
    }

    /**
     * Description
     * @method changePollStyle
     * @param {} style
     * @return 
     */
    function changePollStyle(style) {
      vm.changeBgImageCropped = '';
      vm.changeBgImage = null;
      vm.customizeDesign = false;
      if (style === 'default') {
        vm.customizeDesign = true;
      } else if (style === 'style1') {
        vm.poll.gradient = true;
        vm.poll.bgOption = 'gradient';
        vm.poll.styles.gradient.colorOne = '#5B6DC4';
        vm.poll.styles.gradient.colorTwo = '#6BA1C8';
        vm.poll.styles.bgColor = '-webkit-linear-gradient(top, #5B6DC4 0%, #6BA1C8 100%)';
        vm.poll.styles.color = '#ffffff';
      } else if (style === 'style2') {
        vm.poll.gradient = true;
        vm.poll.bgOption = 'gradient';
        vm.poll.styles.gradient.colorOne = '#fc8ca8';
        vm.poll.styles.gradient.colorTwo = '#E4A8D8';
        vm.poll.styles.bgColor = '-webkit-linear-gradient(top, #fc8ca8, #E4A8D8)';
        vm.poll.styles.color = '#ffffff';
      } else if (style === 'style3') {
        vm.poll.gradient = true;
        vm.poll.bgOption = 'gradient';
        vm.poll.styles.gradient.colorOne = '#E276AB';
        vm.poll.styles.gradient.colorTwo = '#E4BD8F';
        vm.poll.styles.bgColor = '-webkit-linear-gradient(top, #E276AB, #E4BD8F)';
        vm.poll.styles.color = '#ffffff';
      } else if (style === 'style4') {
        vm.poll.gradient = false;
        vm.poll.bgOption = 'color';
        vm.poll.styles.bgColor = '#0195C0';
        vm.poll.styles.color = '#ffffff';
      } else if (style === 'style5') {
        vm.poll.gradient = false;
        vm.poll.bgOption = 'color';
        vm.poll.styles.bgColor = '#38CF6A';
        vm.poll.styles.color = '#ffffff';
      } else if (style === 'style6') {
        vm.poll.gradient = false;
        vm.poll.bgOption = 'color';
        vm.poll.styles.bgColor = '#9FB749';
        vm.poll.styles.color = '#ffffff';
      } else if (style === 'style7') {
        vm.poll.gradient = false;
        vm.poll.bgOption = 'color';
        vm.poll.styles.bgColor = '#FB675B';
        vm.poll.styles.color = '#4c1130';
      } else {
        loadCustomDesign(style);
      }
    }

    /**
     * Description
     * @method uploadImage
     * @param {} image
     * @param {} callback
     * @return 
     */
    function uploadImage(image, callback) {
      var image_return = '';
      if (image && image.type) {
        Upload.upload({
          url: '/poll/saveImage',
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          data: {
            file: image
          }
        }).then(function (resp) {
          callback(resp.data);
        }, function (resp) {
          // console.log('Error status: ' + resp.status);
        });
      } else {
        callback(image_return);
      }
    }

    /**
     * Description
     * @method uploadCanvas
     * @param {} canvas
     * @param {} callback
     * @return 
     */
    function uploadCanvas(canvas, callback) {
      if (canvas) {
        vm.poll.question = vm.poll.question ? vm.poll.question : '';
        var canvas_title = vm.poll.question.replace(/[^a-zA-Z ]/g, "");
        Upload.upload({
          url: '/poll/saveImage',
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          data: {
            file: Upload.dataUrltoBlob(canvas, (canvas_title || 'untitled') + '.png')
          }
        }).then(function (resp) {
          callback(resp.data);
        }, function (resp) {
          // console.log('Error status: ' + resp.status);
        });
      } else {
        callback('');
      }
    }

    /**
     * Description
     * @method generateImage
     * @param {} callback
     * @return 
     */
    function generateImage(callback) {
      var pollContentArea = angular.element('#poll-preview-content');
      html2canvas(pollContentArea, {
        /**
         * Description
         * @method onrendered
         * @param {} canvas
         * @return 
         */
        onrendered: function (canvas) {
          var dataUrl = canvas.toDataURL();
          if (dataUrl) {
            callback(dataUrl);
          }
        }
      });
    }

    /**
     * Description
     * @method showMessage
     * @param {} message
     * @return 
     */
    function showMessage(message) {
      $mdToast.show({
        template: '<md-toast class="md-toast success">' + message + '</md-toast>',
        hideDelay: 3000,
        position: 'bottom left'
      });
    }

    /**
     * Description
     * @method savePollVote
     * @return 
     */
    function savePollVote() {
      var answers = [];
      // var aitem = {};
      // aitem.answer_id = vm.selectedAnswer;
      // answers.push(aitem);
      angular.element("#answers input.vote-answer").each(function (index) {
        if (angular.element(this).is(':checked')) {
          answers.push({answer_id: parseInt(angular.element(this).val())});
        }
      });

      if (answers.length > 0) {
        var data = {
          'id': vm.pollId,
          'answers': answers
        };
        PollService.savePollVote(data).then(function (response) {
          if (response.id) {
            showMessage('Your poll vote has been saved successfully.');
            vm.pollDisplayVoting = false;
            initPollResult();
          }
        });
      } else {
        showMessage("Error! Please select atleast one option.");
      }
    }

    /**
     * Description
     * @return
     * @method initPollResult
     * @return 
     */
    function initPollResult() {
      loadExtarnalJavascript();
      PollService.getPollAnswer(vm.pollId).then(function (response) {
        if (response.tvote > 0) {
          vm.poll = response;
          vm.Question = response.question;
          vm.chartData = [['Answer', 'Total Vote']];
          response.answers.forEach(function (val) {
            vm.chartData.push([val.txt, val.vote]);
          });


          if ( response.show_vote_result ) {
            vm.pollDisplayVotingChart = true;
            $timeout(function () {
              initPollResultChart(response);
            }, 1000);
          } else {
            vm.initPollVoteGreeting();
          }
        } else {
          showMessage('There is no vote to show voting result.');
        }
      });
    }

    /**
     * Description
     * @method initPollVoteGreeting
     * @return 
     */
    function initPollVoteGreeting() {
      vm.voteGreetingShow = true;
    }

    /**
     * Description
     * @method loadExtarnalJavascript
     * @return 
     */
    function loadExtarnalJavascript(){
      var tag = document.createElement("script");
      tag.src = "https://www.gstatic.com/charts/loader.js";
      document.getElementsByTagName("head")[0].appendChild(tag);
    }

    /**
     * Description
     * @method initPollResultChart
     * @param {} pollResultData
     * @return 
     */
    function initPollResultChart( pollResultData ) {
      google.charts.load('current', {packages: ['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      // var answeredItem = pollResultData.answers.filter(function (answer) {
      //   return answer.vote > 0;
      // });

      var answerRaws = pollResultData.answers.map(function (ai) {
        return [ai.txt, ai.vote];
      });

      /**
       * Description
       * @method drawChart
       * @return 
       */
      function drawChart() {
        // Define the chart to be drawn.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Element');
        data.addColumn('number', 'Percentage');
        data.addRows(answerRaws);

        // Instantiate and draw the chart.
        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, null);
      }
    }

    /**
     * Description
     * @return
     * @method conCheckboxActiveClass
     * @return 
     */
    function conCheckboxActiveClass() {
      var cCls = angular.element(".qc-controll-btn").find("i.fa-dot-circle-o");
      var aCls = angular.element(".qc-controll-btn").find("i.fa-check-square-o");
      if (!angular.element(cCls).hasClass("toggle-active")) {
        angular.element(aCls).removeClass("toggle-active");
        angular.element(cCls).addClass("toggle-active");
        vm.setRadioCheckboxCurr = 'radio';
      }
    }

    /**
     * Description
     * @return
     * @method conRadioActiveClass
     * @return 
     */
    function conRadioActiveClass() {
      var aCls = angular.element(".qc-controll-btn").find("i.fa-dot-circle-o");
      var cCls = angular.element(".qc-controll-btn").find("i.fa-check-square-o");
      if (!angular.element(cCls).hasClass("toggle-active")) {
        angular.element(aCls).removeClass("toggle-active");
        angular.element(cCls).addClass("toggle-active");
        vm.setRadioCheckboxCurr = 'checkbox';
      }
    }

    /**
     * Description
     * @method cancelCustomDesign
     * @return 
     */
    function cancelCustomDesign() {
      vm.customizeDesign = false;
    }

    /**
     * Description
     * @method saveCustomDesign
     * @return 
     */
    function saveCustomDesign() {
      $rootScope.loadingProgress = true;
      uploadImage(vm.changeBgImage, function (imageData) {
        generateImage(function (imageDataUrl) {
          uploadCanvas(imageDataUrl, function (canvasData) {
            vm.poll.file = canvasData.id || '';
            var imageId = imageData.id || '';
            if (imageId) {
              vm.poll.background = imageId;
            }
            PollService.saveCustomDesign(vm.poll).then(function (response) {
              $rootScope.loadingProgress = false;
              if (response) {
                $timeout(function () {
                  vm.customizeDesign = false;
                  getCustomDesigns();
                  vm.currentTabIndex = 2;
                  showMessage('The poll design has been saved');
                });
              }
            });
          });
        });

      });
    }

    /**
     * Description
     * @method allDraftPoll
     * @param {} param
     * @return 
     */
    function allDraftPoll(param) {
      vm.currentTabIndex = 0;
    }

    /**
     * Description
     * @method getCustomDesigns
     * @return 
     */
    function getCustomDesigns() {
      PollService.getCustomDesigns().then(function (response) {
        $rootScope.loadingProgress = false;
        if (response) {
          vm.allCustomDesigns = response;
        }
      });
    }

    /**
     * Description
     * @method loadCustomDesign
     * @param {} id
     * @return 
     */
    function loadCustomDesign(id) {
      PollService.loadCustomDesign(id).then(function (response) {
        vm.currentTabIndex = 2;
        // console.log(response);
        vm.poll.gradient = response.gradient;
        vm.poll.styles = response.styles;
        if (!response.gradient && response.background) {
          vm.changeBgImage = vm.site_url + response.background.file_dir;
        } else {
          vm.changeBgImageCropped = '';
        }
      });
    }

    /**
     * Description
     * @method deleteCustomDesign
     * @param {} id
     * @return 
     */
    function deleteCustomDesign(id) {
      vm.deleteCustomDesignId = id;
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/poll/dialogs/deleteDesign.html',
        parent: angular.element($document.body),
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method addEmailToSharedPoll
     * @return 
     */
    function addEmailToSharedPoll(){

    }


    /**
     * Description
     * @method uploadFile
     * @param {} files
     * @param {} $index
     * @return 
     */
    function uploadFile(files, $index){
      if(typeof vm.poll.answers[$index] === 'undefined'){
        $index = vm.poll.answers.push({img: 'generating image link....'}) - 1;
      }
      files = files.filter(function (file) {
        return file.type && file.type.length;
      });
      $rootScope.loadingProgress = true;
      if (files && files.length) {
        var file = files[0];
        file.upload = Upload.upload({
          url: '/poll/saveImage',
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          data: {file: file, output: 'file'}
        });
        file.upload.then(function (response) {
          // console.log(response);
          $timeout(function () {
            // console.log(vm.poll.answers);
            // console.log($index);
            vm.poll.answers[$index].img = vm.site_url + response.data.file_dir;
            // file.result = response.data;
            $rootScope.loadingProgress = false;
            $scope.files = null;
          });
        });
      }
    }

    /**
     * Description
     * @method deleteDesginItemsConfirm
     * @return 
     */
    function deleteDesginItemsConfirm() {
      if (vm.deleteCustomDesignId) {
        PollService.deleteDesginItemsConfirm(vm.deleteCustomDesignId).then(function (response) {
          vm.deleteCustomDesignId = null;
          closeDialog();
          showMessage('The tamplate has been deleted.');
          getCustomDesigns();
          vm.currentTabIndex = 2;
        });
      } else {
        showMessage('The desgin you want do delete is not found !');
      }
    }

  }

})();
