(function () {
  'use strict';


  angular
    .module("app.files")
    .controller('FilesController', FilesController);

  /**
   * Description
   * @ngInject 
   * @method FilesController
   * @param {} $mdSidenav
   * @param {} $q
   * @param {} FilesService
   * @param {} $rootScope
   * @param {} ContactService
   * @param {} $mdDialog
   * @param {} $window
   * @param {} ChatsService
   * @param {} $document
   * @param {} Upload
   * @param {} $scope
   * @param {} $location
   * @param {} $timeout
   * @param {} $state
   * @param {} $mdToast
   * @param {} socharaLightbox
   * @param {} $mdConstant
   * @return 
   */
  function FilesController($mdSidenav, $q, FilesService, $rootScope, ContactService, $mdDialog, $window, ChatsService, $document, Upload, $scope, $location, $timeout, $state, $mdToast, socharaLightbox, $mdConstant) {
    var vm = this;
    vm.selectedAccount = 'creapond';
    vm.currentView = 'list';
    vm.showDetails = true;

    vm.path = [{name: "Files", uri: "app.files"}];

    // Methods
    vm.siteUrl = $location.protocol() + '://' + $location.host() + ':' + $location.port();
    vm.fileAdded = fileAdded;
    vm.fileSuccess = fileSuccess;

    vm.select = select;
    vm.toggleDetails = toggleDetails;
    vm.toggleSidenav = toggleSidenav;
    vm.toggleView = toggleView;
    vm.getAllFileList = getAllFileList;
    vm.getAllFolderList = getAllFolderList;
    vm.uploadFilesFolder = uploadFilesFolder;
    vm.createNewFolder = createNewFolder;
    vm.closeDialog = closeDialog;
    vm.viewChatTab = viewChatTab;
    vm.tabCurrentTab = 'files';
    vm.uploadFiles = uploadFiles;
    vm.newFoderSubmission = newFoderSubmission;
    vm.checkNewFolderSubmission = checkNewFolderSubmission;
    vm.goToFolder = goToFolder;
    vm.selectedItem = selectedItem;
    vm.selectedFolderClass = selectedFolderClass;
    vm.downloadItems = downloadItems;
    vm.deleteItems = deleteItems;
    vm.deleteItemsConfirm = deleteItemsConfirm;
    vm.copySelectedItems = copySelectedItems;
    vm.cutSelectedItems = cutSelectedItems;
    vm.pasteSelectedItems = pasteSelectedItems;
    vm.renameItem = renameItem;
    vm.renameItemConfirm = renameItemConfirm;
    vm.downloadFileFromUrl = downloadFileFromUrl;
    vm.myFileFolderList = myFileFolderList;
    vm.myRecentFileFolderList = myRecentFileFolderList;
    vm.starredFileFolderList = starredFileFolderList;
    vm.isUnknownFileSystem = isUnknownFileSystem;
    vm.addToStar = addToStar;
    vm.back = back;
    vm.filePreview = filePreview;
    vm.sharePopup = sharePopup;
    vm.contacts = [];
    // vm.querySearch = querySearch;
    vm.transformChip = transformChip;
    vm.shareSubmission = shareSubmission;
    vm.querySearch = querySearch;
    vm.addToSharedUserArray = addToSharedUserArray;
    vm.sharedFileFolderList = sharedFileFolderList;
    vm.selectMultipleFiles = selectMultipleFiles;
    vm.resetKeyPress = resetKeyPress;
    vm.setTabIndex = setTabIndex;
    vm.getTabIndex = getTabIndex;
    vm.ifCheckedAll = ifCheckedAll;
    vm.toggleAllFilesFolders = toggleAllFilesFolders;
    vm.isSelIndeterminate = isSelIndeterminate;
    vm.ifHaveAnyFilesFolders = ifHaveAnyFilesFolders;

    //////////


    init();

    /**
     * Description
     * @method init
     * @return 
     */
    function init() {
      vm.selectedMultipleItem = [];
      vm.selectedMultipleItemAttr = [];
      vm.folders = [];
      vm.files = [];
      $rootScope.searchable = false;
      vm.navMenu = 'files';
      reloadFilesFolderItems();
      // getEssentialData();
      loadContacts();
    }


    vm.getIconImage = getIconImage;
    /**
     * Description
     * @method getIconImage
     * @param {} ext
     * @return iconName
     */
    function getIconImage(ext){
      var iconName = 'icon-file';
      if(['.png', '.jpg', '.gif', '.jpeg', '.bmp'].indexOf(ext) > -1 ){
        iconName = 'icon-file-image-box';
      }else if(['.doc', '.docs'].indexOf(ext) > -1 ){
        iconName = 'icon-file-document';
      }else if(['.pdf'].indexOf(ext) > -1 ){
        iconName = 'icon-file-pdf';
      }else if(['.xls', '.xlsc'].indexOf(ext) > -1 ){
        iconName = 'icon-grid';
      }else if(['.mp4'].indexOf(ext) > -1 ){
        iconName = 'icon-file-image-box';
      }
      return iconName;
    }




    /**
     * File added callback
     * Triggers when files added to the uploader
     * @method fileAdded
     * @param file
     * @return 
     */
    function fileAdded(file) {
      // Prepare the temp file data for file list
      var uploadingFile = {
        id: file.uniqueIdentifier,
        file: file,
        type: '',
        owner: 'Emily Bennett',
        size: '',
        modified: moment().format('MMMM D, YYYY'),
        opened: '',
        created: moment().format('MMMM D, YYYY'),
        extention: '',
        location: 'My Files > Documents',
        offline: false,
        preview: 'images/etc/sample-file-preview.jpg'
      };

      // Append it to the file list
      vm.files.push(uploadingFile);
    }

    /**
     * Description
     * @method closeDialog
     * @return 
     */
    function closeDialog() {
      $mdDialog.hide();
    }

    /**
     * Description
     * @method viewChatTab
     * @param {} tab
     * @return 
     */
    function viewChatTab(tab) {
      vm.tabCurrentTab = tab;
    }

    /**
     * Description
     * @method reloadFilesFolderItems
     * @return 
     */
    function reloadFilesFolderItems() {
      $rootScope.loadingProgress = true;
      var type = $state.params.type || '';
      if(type){
        vm.currentFolderId = $state.params.folderId || '';
        if (type === 'folder' && vm.currentFolderId) {
          getAllFileList($state.params.folderId || null);
          getAllFolderList($state.params.folderId || null);
          // vm.path[1] = {name: vm.folders.getById(vm.currentFolderId).name, uri: "app.files.type.folder"};
          vm.path[1] = {name: '', uri: "app.files.type.folder"};
        }else if(type === 'starred'){
          starredFileFolderList();
          vm.path[1] = null;
        }else if(type === 'recent'){
          myRecentFileFolderList();
          vm.path[1] = null;
        }else if(type === 'shared'){
          sharedFileFolderList();
          vm.path[1] = null;
        }
      } else {
        getAllFileList();
        getAllFolderList();
      }
    }

    $scope.$watchCollection(function () {
      return $state.params;
    }, function () {
      var type = $state.params.type;
      vm.currentFolderId = $state.params.folderId || '';
      if (type === 'folder' && vm.currentFolderId) {
        getAllFileList($state.params.folderId || null);
        getAllFolderList($state.params.folderId || null);
        if(vm.currentFolderId){
          vm.path[1] = {uri: "app.files.type.folder"};
          if(vm.folders.getById(vm.currentFolderId)){
            vm.path[1].name = vm.folders.getById(vm.currentFolderId).name;
          }
        }
      }else if(type === 'starred'){
        starredFileFolderList();
        vm.path[1] = null;
      }else if(type === 'recent'){
        myRecentFileFolderList();
        vm.path[1] = null;
      }else if(type === 'shared'){
        sharedFileFolderList();
        vm.path[1] = null;
      } else {
        getAllFileList($state.params.folderId || null);
        getAllFolderList($state.params.folderId || null);
        vm.path[1] = null;
      }
    });


    /**
     * Description
     * @method goToFolder
     * @param {} folder
     * @return 
     */
    function goToFolder(folder) {
      if (folder) {
        $rootScope.loadingProgress = true;
        var folderId = folder.id;
        $state.go('app.files.type.folder', {type: 'folder', folderId: folderId});
      }
    }

    /**
     * Description
     * @method addToSharedUserArray
     * @return 
     */
    function addToSharedUserArray() {
      // console.log(vm.selectedContact);
    }


    /**
     * Description
     * @method sharedFileFolderList
     * @return 
     */
    function sharedFileFolderList(){
      $state.go('app.files.type', {type: 'shared'});
      $rootScope.loadingProgress = true;
      getSharedItemList();
    }

    vm.keyMap = {};
    /**
     * Description
     * @method selectMultipleFiles
     * @param {} e
     * @return 
     */
    function selectMultipleFiles(e){
      e.preventDefault();
      e = e || event; // to deal with IE
      vm.keyMap[e.keyCode] = e.type === 'keydown';
      if(vm.keyMap[16] && vm.keyMap[38]){
        // console.log('Shift Up Arrow');
        removeSelecttMediaItem();
        vm.keyMap[38] = false;
      }else if(vm.keyMap[16] && vm.keyMap[40]){
        // console.log('Shift Down Arrow');
        selectNextMediaItem();
        vm.keyMap[40] = false;
      }else if(vm.keyMap[17] && vm.keyMap[65]){
        selectAllMediaItem();
        vm.keyMap[65] = false;
      }
    }

    /**
     * Description
     * @method selectAllMediaItem
     * @return 
     */
    function selectAllMediaItem(){
      var fileItems = vm.files.map(function (file) { return file.id; });
      var fileItemsAttr = vm.files.map(function (file) { return {type: 'file', id: file.id }; });
      var folderItems = vm.folders.map(function (folder) { return folder.id; });
      var folderItemsAttr = vm.folders.map(function (folder) { return {type: 'folder', id: folder.id}; });
      vm.selectedMultipleItem = fileItems.concat(folderItems);
      vm.selectedMultipleItemAttr = fileItemsAttr.concat(folderItemsAttr);
    }

    /**
     * Description
     * @method selectNextMediaItem
     * @return 
     */
    function selectNextMediaItem(){
      var nextItem, pushAlready = false;
      nextItem = vm.folders.find(function (folder) {
        return vm.selectedMultipleItem.indexOf(folder.id) === -1;
      });
      if(nextItem){
        pushMediaItem(nextItem.id, nextItem.type);
        pushAlready = true;
      }
      if(!pushAlready){
        nextItem = vm.files.find(function (file) {
          // console.log(vm.selectedMultipleItem.indexOf(file.id));
          return vm.selectedMultipleItem.indexOf(file.id) === -1;
        });
        if(nextItem){
          pushMediaItem(nextItem.id, nextItem.type);
        }
      }
    }

    /**
     * Description
     * @method removeSelecttMediaItem
     * @return 
     */
    function removeSelecttMediaItem(){
      vm.selectedMultipleItem.splice(-1,1);
      vm.selectedMultipleItemAttr.splice(-1,1);
    }

    /**
     * Description
     * @method pushMediaItem
     * @param {} id
     * @param {} type
     * @return 
     */
    function pushMediaItem( id, type ){
      if(vm.selectedMultipleItem.indexOf(id)<0){
        vm.selectedMultipleItem.push(id);
        vm.selectedMultipleItemAttr.push({type: type, id: id});
      }
    }

    /**
     * Description
     * @method resetKeyPress
     * @return 
     */
    function resetKeyPress(){
      if(vm.keyMap && !vm.keyMap[16] ){
        vm.keyMap = {};
      }
    }

    vm.tabIndex = 1;
    /**
     * Description
     * @method setTabIndex
     * @return 
     */
    function setTabIndex(){
      if(vm.folders && vm.folders.length){
        vm.tabIndex = vm.folders[0].id;
      }else if(vm.files && vm.files.length){
        vm.tabIndex = vm.files[0].id;
      }
    }

    /**
     * Description
     * @method getTabIndex
     * @param {} id
     * @return 
     */
    function getTabIndex( id ){
      if( id && id === vm.tabIndex ){
        return 1;
      }else{
        return 9283498237498;
      }
    }

    /**
     * Description
     * @method ifCheckedAll
     * @return BinaryExpression
     */
    function ifCheckedAll(){
      return vm.selectedMultipleItem.length === vm.folders.length + vm.files.length;
    }

    /**
     * Description
     * @method ifHaveAnyFilesFolders
     * @return UnaryExpression
     */
    function ifHaveAnyFilesFolders(){
      return !(vm.files.length || vm.folders.length);
    }


    /**
     * Description
     * @method shareSubmission
     * @return 
     */
    function shareSubmission() {
      $rootScope.loadingProgress = true;
      var data = {};
      data.users = vm.selectedContacts.map(function (contact) {
        return contact.id;
      });
      data.items = vm.selectedMultipleItemAttr;
      FilesService.itemsShareWithUsers(data).then(function (response) {
        if (response) {
          showMessage("Items are shared");
          $rootScope.loadingProgress = false;
        }
        closeDialog();
      });
    }

    /**
     * Description
     * @method getPreviousSharedUsers
     * @param {} item
     * @return 
     */
    function getPreviousSharedUsers( item ) {
      FilesService.getPreviousSharedUsers(item).then(function (response) {
        vm.selectedContacts = response;
      });
    }

    /**
     * Description
     * @method transformChip
     * @param {} chip
     * @return ObjectExpression
     */
    function transformChip(chip) {
      // If it is an object, it's already a known chip
      if (angular.isObject(chip)) {
        return chip;
      }

      // Otherwise, create a new one
      return {first_name: chip};
    }

    /**
     * Description
     * @method loadContacts
     * @return 
     */
    function loadContacts() {
      ContactService.getContacts().then(function (resp) {
        if (resp) {
          vm.filteredContacts = resp.map(function (c) {
            var contact = {
              name: c.first_name + ' ' + c.last_name,
              email: c.email,
              photo_url: c.photo_url ? c.photo_url.replace('assets', '').replace(/\\/, '/') : '../images/avatars/blank.jpg',
              id: c.sochara_user_id || undefined,
            };
            return {
              value: contact.name.toLowerCase(),
              name: contact.name,
              email: contact.email,
              isContact: true,
              image: contact.photo_url,
              id: contact.id
            };
          });
        }
      });
    }


    /**
     * Description
     * @method toggleAllFilesFolders
     * @return 
     */
    function toggleAllFilesFolders() {
      var totalItems = vm.files.length + vm.folders.length;
      if (vm.selectedMultipleItemAttr.length === totalItems) {
        vm.selectedMultipleItem = [];
        vm.selectedMultipleItemAttr = [];
      } else {
        var selectedItem = vm.files.map(function (file) {return file.id;}) || [];
        vm.selectedMultipleItem = selectedItem.concat(vm.folders.map(function (folder) {return folder.id;}));
        var selectedItemArr = vm.files.map(function (file) {return {id: file.id, type: 'file'};});
        vm.selectedMultipleItemAttr = selectedItemArr.concat(vm.folders.map(function (folder) {return {id: folder.id, type: 'folder'};}));
      }
    }

    /**
     * Description
     * @method isSelIndeterminate
     * @return LogicalExpression
     */
    function isSelIndeterminate() {
      var totalItemLength = vm.files.length + vm.folders.length;
      return (vm.selectedMultipleItem.length && vm.selectedMultipleItem.length !== totalItemLength);
    }

    /***************************************** Autocomplete For Share FIles ****************************************/
    vm.simulateQuery = true;
    vm.isDisabled = false;
    vm.selectedContact = null;
    vm.searchText = null;
    vm.selectedContacts = [];

    vm.querySearch = querySearch;


    /**
     * Search for vegetables.
     * @method querySearch
     * @param {} query
     * @return MemberExpression
     */
    function querySearch (query) {
      // query = query.length > 1 ? query : '';
      var results = query ? ContactService.getAllSocharaUsers({name: query}).then(function (response) {
        var totalResponse = response.map(function (c) {
          var contact = {
            name: c.first_name + ' ' + c.last_name,
            email: c.email,
            photo_url: c.photo_url ? c.photo_url.replace('assets', '').replace(/\\/, '/') : '../images/avatars/blank.jpg',
            id: c.id || undefined,
          };
          // contact._lowername = contact.name.toLowerCase();
          return {
            value: contact.name.toLowerCase(),
            name: contact.name,
            email: contact.email,
            image: contact.photo_url,
            isContact: false,
            id: contact.id
          };
        });
        var searchQuery = angular.lowercase(query);
        totalResponse = totalResponse.concat(vm.filteredContacts);
        return totalResponse.filter(function ( state ) {
          return state.value.indexOf(searchQuery) === 0;
        });
      }) : vm.filteredContacts;
      var deferred = $q.defer();
      $timeout(function () {
        deferred.resolve(results);
      }, Math.random() * 1000, false);
      return deferred.promise;
      // new functions

      // var results = query ? vm.contactsData.filter(createFilterFor(query)) : [];
      // return results;
    }


    /***************************************** Autocomplete For Share FIles ****************************************/


    /**
     * File upload success callback
     * Triggers when single upload completed
     * @method fileSuccess
     * @param file
     * @param message
     * @return 
     */
    function fileSuccess(file, message) {
      // Iterate through the file list, find the one we
      // are added as a temp and replace its data
      // Normally you would parse the message and extract
      // the uploaded file data from it
      angular.forEach(vm.files, function (item, index) {
        if (item.id && item.id === file.uniqueIdentifier) {
          // Normally you would update the file from
          // database but we are cheating here!

          // Update the file info
          item.name = file.file.name;
          item.type = 'document';

          // Figure out & upddate the size
          if (file.file.size < 1024) {
            item.size = parseFloat(file.file.size).toFixed(2) + ' B';
          }
          else if (file.file.size >= 1024 && file.file.size < 1048576) {
            item.size = parseFloat(file.file.size / 1024).toFixed(2) + ' Kb';
          }
          else if (file.file.size >= 1048576 && file.file.size < 1073741824) {
            item.size = parseFloat(file.file.size / (1024 * 1024)).toFixed(2) + ' Mb';
          }
          else {
            item.size = parseFloat(file.file.size / (1024 * 1024 * 1024)).toFixed(2) + ' Gb';
          }
        }
      });
    }

    /**
     * Description
     * @method addToStar
     * @return 
     */
    function addToStar() {
      if (vm.selectedMultipleItemAttr && vm.selectedMultipleItemAttr.length) {
        FilesService.AddToStar(vm.selectedMultipleItemAttr).then(function (response) {
          if (response) {
            showMessage('Favorite items updated !');
          }
        });
      }
    }

    /**
     * Description
     * @method back
     * @return 
     */
    function back(){
      $window.history.back();
    }

    /**
     * Description
     * @method filePreview
     * @param {} file
     * @return 
     */
    function filePreview(file) {
      $rootScope.loadingProgress = true;
      if (file) {
        var imageList = vm.files;
        var fileUrl = vm.siteUrl + file.file_dir;
        var lightBoxOption = {};
        makeImageLinkArray(imageList, function (listData) {
          $rootScope.loadingProgress = false;
          if (listData && listData.length) {
            var fileUrlPosition = listData.indexOf(fileUrl);
            if (fileUrlPosition > -1) {
              lightBoxOption.initialIndex = fileUrlPosition;
              socharaLightbox.show(listData, lightBoxOption);
            } else {
              showMessage('Preview Not Available For This File');
            }
          } else {
            showMessage('No Image File Found');
          }
        });
      } else {
        showMessage('Invalid File');
      }
    }

    /**
     * Description
     * @method copySelectedItems
     * @return 
     */
    function copySelectedItems() {
      vm.selectedFilesForMove = null;
      vm.selectedFilesForCopy = vm.selectedMultipleItemAttr;
      vm.selectedFilesFrom = $state.params.folderId || '0';
      showMessage("Items are coppied.");
    }

    /**
     * Description
     * @method cutSelectedItems
     * @return 
     */
    function cutSelectedItems() {
      vm.selectedFilesForCopy = null;
      vm.selectedFilesForMove = vm.selectedMultipleItemAttr;
      vm.selectedFilesFrom = $state.params.folderId || '0';
      showMessage("Items are ready to move.");
    }

    /**
     * Description
     * @method pasteSelectedItems
     * @return 
     */
    function pasteSelectedItems() {
      var pasteData = vm.selectedFilesForCopy || vm.selectedFilesForMove;
      var pasteTo = $state.params.folderId || null;
      var pasteFrom = vm.selectedFilesFrom;
      var pasteType = vm.selectedFilesForCopy && vm.selectedFilesForCopy.length ? 'copy' : 'cut';
      vm.selectedFilesForCopy = vm.selectedFilesForMove = null;
      FilesService.pasteSelectedItems(pasteData, pasteTo, pasteFrom, pasteType).then(function (response) {
        if (response) {
          reloadFilesFolderItems();
        }
      });
    }

    /**
     * Description
     * @method renameItem
     * @param {} ev
     * @return 
     */
    function renameItem(ev) {
      if (vm.selectedMultipleItemAttr.length === 1) {
        if (vm.selectedMultipleItemAttr[0].type === 'folder') {
          vm.itemPreviousName = vm.folders.getById(vm.selectedMultipleItemAttr[0].id).name;
        } else if (vm.selectedMultipleItemAttr[0].type === 'file') {
          vm.itemPreviousName = vm.files.getById(vm.selectedMultipleItemAttr[0].id).name;
        }
        $mdDialog.show({
          // controller: 'UploadDialogController',
          /**
           * Description
           * @method controller
           * @return vm
           */
          controller: function () {
            return vm;
          },
          controllerAs: 'vm',
          templateUrl: 'app/main/files/dialogs/rename.html',
          parent: angular.element($document.body),
          targetEvent: ev,
          clickOutsideToClose: false
        });
      }
    }

    /**
     * Description
     * @method sharePopup
     * @param {} ev
     * @return 
     */
    function sharePopup(ev) {
      vm.sharaData = {id: vm.selectedMultipleItemAttr[0].id || '', type: vm.selectedMultipleItemAttr[0].type || ''};
      getPreviousSharedUsers( vm.selectedMultipleItemAttr[0]);
      var semicolon = 186;
      // vm.customKeys = [$mdConstant.KEY_CODE.ENTER, $mdConstant.KEY_CODE.SPACE, $mdConstant.KEY_CODE.COMMA, semicolon, $mdConstant.KEY_CODE.TAB];
      vm.customKeys = [];
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/files/dialogs/share.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: false
      });

    }


    /**
     * Description
     * @method renameItemConfirm
     * @return 
     */
    function renameItemConfirm() {
      var data = {};
      data.type = vm.selectedMultipleItemAttr[0].type;
      data.id = vm.selectedMultipleItemAttr[0].id;
      data.name = vm.itemPreviousName;
      // console.log(data);
      closeDialog();
      FilesService.renameSelectedItem(data).then(function (response) {
        if (response) {
          showMessage('The item is renamed');
          reloadFilesFolderItems();
        }
      });
    }

    /**
     * Description
     * @method downloadFileFromUrl
     * @return 
     */
    function downloadFileFromUrl() {
      if (vm.fileUrl && vm.fileUrl.length) {
        var folder = $state.params.folderId || '';
        closeDialog();
        FilesService.downloadFileFromUrl(vm.fileUrl, folder).then(function (response) {
          if (response) {
            showMessage('The file uploaded successfully.');
            reloadFilesFolderItems();
          }
        });
      }
    }

    /**
     * Description
     * @method myFileFolderList
     * @return 
     */
    function myFileFolderList() {
      // Reload Again For files and folders
      vm.navMenu = 'files';
      $state.go('app.files');
      $state.params.folderId = null;
      getAllFileList();
      getAllFolderList();
      // reloadFilesFolderItems();
    }

    /**
     * Description
     * @method myRecentFileFolderList
     * @return 
     */
    function myRecentFileFolderList() {
      vm.navMenu = 'recent';
      vm.files = null;
      vm.folders = null;
      $state.go('app.files.type', {type: 'recent'});
      FilesService.getAllFileList({recent: true}).then(function (response) {
        vm.files = response;
        $rootScope.loadingProgress = false;
        vm.selected = vm.files[0];
      });
      FilesService.getAllFolderList({recent: true}).then(function (response) {
        vm.folders = response;
      });
    }

    /**
     * Description
     * @method makeImageLinkArray
     * @param {} files
     * @param {} callback
     * @return 
     */
    function makeImageLinkArray(files, callback) {

      var fileDataUrls = [];
      files.forEach(function (val) {
        var filExt = val.ext.toLowerCase();
        if (filExt === '.png' || filExt === '.jpeg' || filExt === '.jpg' || filExt === '.gif' || filExt === '.bmp') {
          fileDataUrls.push(vm.siteUrl + val.file_dir);
        }
      });
      callback(fileDataUrls);
    }

    /**
     * Description
     * @method isUnknownFileSystem
     * @param {} ext
     * @return BinaryExpression
     */
    function isUnknownFileSystem( ext ){
      return  ['.png', '.jpg', '.gif', '.jpeg', '.bmp', '.doc', '.docs', '.pdf', '.xls', '.xlsc', '.mp4'].indexOf(ext) > -1 ;
    }

    /**
     * Description
     * @method starredFileFolderList
     * @return 
     */
    function starredFileFolderList() {
      vm.navMenu = 'starred';
      vm.files = [];
      vm.folders = [];
      $state.go('app.files.type', {type: 'starred'});
      $rootScope.loadingProgress = true;
      FilesService.getAllFileList({starred: true}).then(function (response) {
        vm.files = response;
        $rootScope.loadingProgress = false;
        vm.selected = vm.files[0];
      });
      FilesService.getAllFolderList({starred: true}).then(function (response) {
        vm.folders = response;
      });
    }


    /**
     * Description
     * @method createNewFolder
     * @param {} ev
     * @return 
     */
    function createNewFolder(ev) {
      vm.newFolderName = null;
      $mdDialog.show({
        // controller: 'UploadDialogController',
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        locals: {
          selectedAccount: undefined
        },
        templateUrl: 'app/main/files/dialogs/new-folder.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: false
      });
    }


    /**
     * Description
     * @method uploadFilesFolder
     * @param {} ev
     * @return 
     */
    function uploadFilesFolder(ev) {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        // controller: 'UploadDialogController',
        controllerAs: 'vm',
        locals: {
          selectedAccount: undefined
        },
        templateUrl: 'app/main/files/dialogs/upload.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method downloadItems
     * @return 
     */
    function downloadItems() {
      var fileIds = [];
      vm.selectedMultipleItemAttr.forEach(function (val) {
        if (val.type === 'file') {
          fileIds.push(val.id);
        }
      });
      if (fileIds && fileIds.length) {
        FilesService.getFiles(fileIds).then(function (response) {
          if (response) {
            // console.log(response);
            window.location.href = response;

          }
        });
      }
    }

    /**
     * Description
     * @method deleteItems
     * @param {} ev
     * @return 
     */
    function deleteItems(ev) {
      $mdDialog.show({
        // controller: 'UploadDialogController',
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/files/dialogs/delete.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method deleteItemsConfirm
     * @return 
     */
    function deleteItemsConfirm() {
      var fileIds = [];
      var folderIds = [];
      vm.selectedMultipleItemAttr.forEach(function (val) {
        if (val.type === 'file') {
          fileIds.push(val.id);
        }
      });
      vm.selectedMultipleItemAttr.forEach(function (val) {
        if (val.type === 'folder') {
          folderIds.push(val.id);
        }
      });
      closeDialog();
      if (fileIds.length) {
        FilesService.deletFiles(fileIds).then(function (response) {
          reloadFilesFolderItems();
        });
      }
      if (folderIds.length) {
        FilesService.deleteEntireFolder(folderIds).then(function (response) {
          reloadFilesFolderItems();
        });
      }
    }

    /**
     * Description
     * @method selectedFolderClass
     * @param {} file
     * @return LogicalExpression
     */
    function selectedFolderClass(file) {
      return vm.selectedMultipleItem && vm.selectedMultipleItem.indexOf(file) > -1;
    }



    /**
     * Selected Files or Folder
     * @method selectedItem
     * @param current
     * @param event
     * @param type
     * @return 
     */
    function selectedItem(current, event, type) {
      vm.keyMap = {};
      var itemArr = {};
      var checkIndexForExistId = vm.selectedMultipleItem.indexOf(current);
      if (event.ctrlKey) {
        if (checkIndexForExistId === -1) {
          vm.selectedMultipleItem.push(current);
          itemArr.id = current;
          if (type) {
            itemArr.type = type;
          }
          vm.selectedMultipleItemAttr.push(itemArr);
        } else if (checkIndexForExistId > -1) {
          vm.selectedMultipleItem.splice(checkIndexForExistId, 1);
          vm.selectedMultipleItemAttr.splice(checkIndexForExistId, 1);
        }
      } else if(event.shiftKey){
        var prevId = vm.selectedMultipleItem[vm.selectedMultipleItem.length - 1];
        vm.selectedMultipleItem.splice(vm.selectedMultipleItem.indexOf(prevId),1);
        vm.selectedMultipleItemAttr = vm.selectedMultipleItemAttr.filter(function (file) { return prevId !== file.id; })
        var getSelectedItems = getFromToItems(prevId, current);
        vm.selectedMultipleItem = vm.selectedMultipleItem.concat(getSelectedItems.ids);
        vm.selectedMultipleItemAttr = vm.selectedMultipleItemAttr.concat(getSelectedItems.files);
      } else {
        vm.selectedMultipleItem = [];
        vm.selectedMultipleItem.push(current);
        vm.selectedMultipleItemAttr = [];
        itemArr.id = current;
        if (type) {
          itemArr.type = type;
          vm.selectedMultipleItemAttr.push(itemArr);
        }
      }
    }

    /**
     * Description
     * @method getFromToItemsFor
     * @param {} prevFile
     * @param {} currentFile
     * @param {} prev
     * @param {} current
     * @return CallExpression
     */
    function getFromToItemsFor( prevFile, currentFile, prev, current ){
      var foundItems = [], prevIndex, currentIndex, foundItems2 = [];
      var findArr = prevFile.type === 'folder' ? vm.folders : vm.files;

      prevIndex = findArr.findIndex(function (file) {
        return file.id === prev;
      });
      if( prevFile.type === 'folder' && currentFile.type === 'file' ){
        foundItems = findArr.slice(prevIndex, findArr.length -1 );
        if(!foundItems.length){
          foundItems.push(prevFile);
        }
        prevIndex = 0;
      }else if( prevFile.type === 'file' && currentFile.type === 'folder' ){
        foundItems = findArr.slice( 0, prevIndex+1 );
        if(!foundItems.length){
          foundItems.push(prevFile);
        }
        prevIndex = findArr.length;
      }

      findArr = currentFile.type === 'folder' ? vm.folders : vm.files;

      currentIndex = findArr.findIndex(function (file) {
        return file.id === current;
      });

      if(prevIndex>currentIndex){
        foundItems2 = findArr.slice(currentIndex, prevIndex+1 );
      }else if(currentIndex>prevIndex){
        foundItems2 = findArr.slice(prevIndex, currentIndex+1 );
      }
      return foundItems.concat(foundItems2);
    }

    /**
     * Description
     * @method getFromToItems
     * @param {} prev
     * @param {} current
     * @return ObjectExpression
     */
    function getFromToItems( prev, current ){
      var getFiles = [], getfileIds = [];
      var prevFile = vm.folders.getById(prev) || vm.files.getById(prev);
      var currentFile = vm.folders.getById(current) || vm.files.getById(current);

      if( prevFile && currentFile ){
        getFiles = getFromToItemsFor(prevFile, currentFile, prev, current);
        getfileIds = getFiles.map(function (file) {
          return file.id;
        });
      }
      return {ids: getfileIds, files: getFiles || []};
    }

    /**
     * Select an item
     * @method select
     * @param item
     * @return 
     */
    function select(item) {
      vm.selected = item;
    }


    /**
     * Description
     * @method showMessage
     * @param {} message
     * @return 
     */
    function showMessage(message) {
      $mdToast.show({
        template: '<md-toast><span class="md-toast-text" flex>' + message + '</span><md-button ng-click="$mdToast.hide();">Close</md-button></md-toast>',
        hideDelay: 3000,
        position: 'bottom left'
      });
    }


    /**
     * Toggle details
     * @method toggleDetails
     * @param item
     * @return 
     */
    function toggleDetails(item) {
      vm.selected = item;
      toggleSidenav('details-sidenav');
    }

    /**
     * Description
     * @method getAllFileList
     * @param {} folder
     * @return 
     */
    function getAllFileList(folder) {
      folder = folder || $state.params.folderId || null;
      FilesService.getAllFileList({folder: folder}).then(function (response) {
        vm.files = response;
        $rootScope.loadingProgress = false;
        vm.selected = vm.files[0];
      });
    }

    /**
     * Description
     * @method getAllFolderList
     * @param {} folder
     * @return 
     */
    function getAllFolderList(folder) {
      folder = folder || $state.params.folderId || null;
      FilesService.getAllFolderList({folder: folder}).then(function (response) {
        vm.folders = response;
      });
    }

    /**
     * Description
     * @method getSharedItemList
     * @param {} folder
     * @return 
     */
    function getSharedItemList(folder) {
      vm.navMenu = 'shared';
      FilesService.getSharedItemList({folder: folder}).then(function (response) {
        vm.files = [];
        vm.folders = [];
        $rootScope.loadingProgress = false;
        if(response){
          vm.files = response.files || [];
          vm.folders = response.folders || [];
        }
      });
    }

    /**
     * Toggle sidenav
     * @method toggleSidenav
     * @param sidenavId
     * @return 
     */
    function toggleSidenav(sidenavId) {
      $mdSidenav(sidenavId).toggle();
    }


    /**
     * Toggle view
     * @method toggleView
     * @return 
     */
    function toggleView() {
      vm.currentView = vm.currentView === 'list' ? 'grid' : 'list';
    }

    /**
     * Description
     * @method checkNewFolderSubmission
     * @param {} e
     * @return 
     */
    function checkNewFolderSubmission(e){
      if(e.which && e.which === 13){
        newFoderSubmission();
      }
    }


    /**
     * Description
     * @method newFoderSubmission
     * @return 
     */
    function newFoderSubmission() {
      var name = vm.newFolderName || '';
      var folderID = $state.params.folderId || '';
      closeDialog();
      if (!name) {
        return;
      }
      $rootScope.loadingProgress = true;
      FilesService.createNewFolder(name, folderID).then(function (response) {
        if (response) {
          // console.log(response);
          vm.folders.push(response);
        }
        $rootScope.loadingProgress = false;
      });
    }

    /******************************************* Upload files *********************************************/

    /**
     * Function: for upload files
     * @return
     * @Return Type: Json(Uploaded File Url)
     * @method uploadFiles
     * @param files, ,
     * @param errFiles,
     * @param folder,
     * @param from,
     * @return 
     */
    function uploadFiles(files, errFiles, folder, from) {
      from = from || '';
      if(!($rootScope.loadingProgress && from === 'parent')){
        files = files.filter(function (file) {
          return file.type && file.type.length;
        });
        // console.log(files);
        $rootScope.loadingProgress = true;
        vm.uploadNotification = true;
        $scope.files = files;
        vm.errFiles = errFiles;
        closeDialog();
        if (files && files.length) {
          angular.forEach(files, function(file, i) {
            if (!file.$error && file.type && file.type.length) {
              folder = folder || '';
              file.upload = Upload.upload({
                url: '/files/uploadFilesToFolder',
                headers: {
                  'Content-Type': 'multipart/form-data'
                },
                data: {folder: folder || '', filePath: file.path, file: file}
              });

              file.upload.then(function (response) {
                $timeout(function () {
                  file.result = response.data;

                  // if(vm.currentTab === 'recent'){ $scope.getAllRecentFiles(); } else if($scope.currentTab === 'folder'){ $scope.folderFiles($scope.selectedFolderUrlId); }
                  // else if($scope.currentTab === 'share'){ $scope.getAllSharedFiles(); } else { $scope.getAllFileList(); }

                  if ($scope.files && $scope.files.length && $scope.files.length === i + 1) {
                    vm.showUploadedNotification = true;
                    $rootScope.loadingProgress = false;
                    $scope.files = null;
                    getAllFileList();
                    getAllFolderList();
                  }
                });
              }, function (response) {
                if (response.status > 0) {
                  vm.errorMsg = response.status + ': ' + response.data;
                }
              }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 *
                  evt.loaded / evt.total));
              });
            }
          });
        }
      }
    }



    /******************************************* End # Upload files *********************************************/

    /**
     * Array prototype
     * Get by id
     * @method getById
     * @param value
     * @return MemberExpression
     */
    Array.prototype.getById = function (value) {
      return this.filter(function (x) {
        return x.id === value;
      })[0];
    };
  }
})();
