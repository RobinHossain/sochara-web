(function () {
  'use strict';

  angular
    .module('app.files',
      [
        'app.auth.login',
        'hmTouchevents',
        'sochara.lightbox'
        // 3rd Party Dependencies
        // 'flow'
      ]
    )
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    // State
    $stateProvider
      .state('app.files', {
        url: '/files',
        views: {
          'content@app': {
            templateUrl: 'app/main/files/files.html',
            controller: 'FilesController as vm'
          }
        },
        bodyClass: 'file-manager'
      })
      .state('app.files.type', {
        url: '/:type',
        bodyClass: 'file-manager'
      })
      .state('app.files.type.folder', {
        url: '/:folderId',
        bodyClass: 'file-manager'
      });
      // .state('app.files.folder', {
      //   url: '/:folderId',
      //   bodyClass: 'file-manager'
      // });


    // Api
    // msApiProvider.register('fileManager.documents', ['app/data/file-manager/documents.json']);

  }

})();
