(function () {
  'use strict';

  angular
    .module('sochara')
    .factory('FilesService', filesService);

  /** @ngInject */
  function filesService($http, $q) {
    return {

      getAllFileList: function ( data ) {
        var defer = $q.defer();
        $http.post('/files/getAllFileList', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getContacts: function () {
        var defer = $q.defer();
        $http.post('/json/users').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },


      getAllFolderList: function ( data ) {
        var defer = $q.defer();
        $http.post('/files/getAllFolderList', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getSharedItemList: function (data) {
        var defer = $q.defer();
        $http.post('/files/getSharedItemList', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      itemsShareWithUsers: function ( data ) {
        var defer = $q.defer();
        $http.post('/files/itemsShareWithUsers', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getPreviousSharedUsers: function ( data ) {
        var defer = $q.defer();
        $http.post('/files/getPreviousSharedUsers', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },


      getFiles: function (files) {
        var defer = $q.defer();
        var data = {};
        if (files) {
          data.files = files;
        }
        $http.post('/files/getFiles', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      deletFiles: function (files) {
        var defer = $q.defer();
        var data = {};
        if (files) {
          data.files = files;
        }
        $http.post('/files/deletFiles', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      deleteEntireFolder: function (folders) {
        var defer = $q.defer();
        var data = {};
        if (folders) {
          data.folders = folders;
        }
        $http.post('/files/deleteEntireFolder', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      createNewFolder: function ( name, folder ) {
        var defer = $q.defer();
        var data = {};
        if (name) {
          data.name = name;
        }
        if (folder) {
          data.root = folder;
        }
        $http.post('/files/createNewFolder', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      pasteSelectedItems: function (pasteItems, pasteTo, pasteFrom, pasteType) {
        var defer = $q.defer();
        var data = {};
        if (pasteItems) {
          data.pasteItems = pasteItems;
        }
        if (pasteTo) {
          data.folder = pasteTo;
        }
        if (pasteFrom) {
          data.pasteFrom = pasteFrom;
        }
        if (pasteType) {
          data.pasteType = pasteType;
        }
        $http.post('/files/pasteSelectedItems', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      renameSelectedItem: function (data) {
        var defer = $q.defer();
        $http.post('/files/renameSelectedItem', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      AddToStar: function (data) {
        var defer = $q.defer(); var iData = {}; if( data ) { iData.items = data; }
        $http.post('/files/AddToStar', iData ).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      downloadFileFromUrl: function (fileUrl, folder) {
        var defer = $q.defer();
        var data = {};
        if (fileUrl) {
          data.fileUrl = fileUrl;
        }
        if (folder) {
          data.folder = folder;
        }
        $http.post('/files/downloadFileFromUrl', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

    };
  }

})();
