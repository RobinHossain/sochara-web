(function () {
  'use strict';

  angular
    .module('sochara')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope, $rootScope, $mdDialog, mainService) {
    // Data

    //////////

    $rootScope.closeDialog = closeDialog;

    init();

    function init(){
      getUserInfo();
    }

    function closeDialog() {
      $mdDialog.hide();
    }

    // Remove the splash screen
    $scope.$on('$viewContentAnimationEnded', function (event) {
      if (event.targetScope.$id === $scope.$id) {
        $rootScope.$broadcast('msSplashScreen::remove');
      }
    });

    function getUserInfo() {
      mainService.userInfo().then(function (resp) {
        if( typeof (resp) === 'object' ){
          $rootScope.user = resp;
        }
      });
    }
  }
})();
