(function () {
  'use strict';

  angular
    .module('sochara')
    .factory('CardService', cardService);

  /** @ngInject */
  function cardService($http, $q) {
    return {
      saveCard: function (data) {
        var defer = $q.defer();
        $http.post('/cards/saveCard', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      saveTemplate: function (data) {
        var defer = $q.defer();
        $http.post('/cards/saveTemplate', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getCardBackground: function () {
        var defer = $q.defer();
        $http.post('/cards/getCardBackground').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getCardImages: function () {
        var defer = $q.defer();
        $http.post('/cards/getCardImages').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      saveFileFor: function (data) {
        var defer = $q.defer();
        $http.post('/cards/saveFileFor', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getCardElements: function () {
        var defer = $q.defer();
        $http.post('/cards/getCardElements').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      saveShapeSvg: function (data) {
        var defer = $q.defer();
        $http.post('/cards/saveShapeSvg', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getCardData: function () {
        var defer = $q.defer();
        $http.post('/cards/getCardData').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getTemplates: function () {
        var defer = $q.defer();
        $http.post('/cards/getTemplates').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getDummyCards: function () {
        var defer = $q.defer();
        $http.post('/cards/getDummyCards').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      deleteCard: function ( id ) {
        var defer = $q.defer(); var data = {}; data.id = id;
        $http.post('/cards/deleteCard', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      favoriteCard: function ( data ) {
        var defer = $q.defer();
        $http.post('/cards/favoriteCard', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getSharedCardData: function () {
        var defer = $q.defer();
        $http.post('/cards/getSharedCardData').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      loadCard: function ( id ) {
        var defer = $q.defer(); var data = {}; data.id = id;
        $http.post('/cards/getCard', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      loadDummyCard: function ( id ) {
        var defer = $q.defer(); var data = {}; data.id = id;
        $http.post('/cards/getDummyCard', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },


      loadContact: function () {
        var defer = $q.defer();
        $http.post('/json/users').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      }

    };
  }

})();
