(function () {
  'use strict';


  angular
    .module('app.cards')
    .controller('cardsController', CardsController).directive('andyDraggable', function () {
      return {
        restrict: 'A',
        /**
         * Description
         * @method link
         * @param {} scope
         * @param {} elm
         * @param {} attrs
         * @return 
         */
        link: function (scope, elm, attrs) {
          var options = scope.$eval(attrs.andyDraggable); //allow options to be passed in
          elm.draggable(options);
        }
      };
    });

  /**
   * Description
   * @ngInject 
   * @method CardsController
   * @param {} $scope
   * @param {} $mdSidenav
   * @param {} $rootScope
   * @param {} CardService
   * @param {} ContactService
   * @param {} $mdDialog
   * @param {} $window
   * @param {} $document
   * @param {} moment
   * @param {} $mdToast
   * @param {} Upload
   * @param {} $timeout
   * @param {} $location
   * @param {} $q
   * @return 
   */
  function CardsController($scope, $mdSidenav, $rootScope, CardService, ContactService, $mdDialog, $window, $document, moment, $mdToast, Upload, $timeout, $location, $q) {

    var vm = this;
    /////////////

    vm.filterIds = null;
    vm.listType = 'all';
    vm.listOrder = 'name';
    vm.listOrderAsc = false;
    vm.selectedContacts = [];
    vm.newGroupName = '';
    vm.labelCorRadiusClr = '#0098eb';
    vm.labelGradientFirst = '#0098eb';
    vm.labelGradientSecond = '#02c9ce';

    vm.saveCardPopup = saveCardPopup;
    vm.saveCard = saveCard;
    vm.changeBackGroundImage = changeBackGroundImage;
    vm.currentDate = new Date();
    vm.setCurrentFont = setCurrentFont;
    vm.setCurrentFontSize = setCurrentFontSize;
    vm.changeFontStyleBold = changeFontStyleBold;
    vm.changeFontStyleItalic = changeFontStyleItalic;
    vm.changeFontStyleUnderline = changeFontStyleUnderline;
    vm.clearOtherBackground = clearOtherBackground;
    vm.changeFontLayer = changeFontLayer;
    vm.changeFontSizeLayer = changeFontSizeLayer;

    vm.getBackgroundImageAcceptRatio = getBackgroundImageAcceptRatio;
    vm.cardBackgroundRatio = 1;
    vm.site_url = $location.protocol() + '://' + $location.host() + ':' + $location.port();
    vm.getThumbImage = getThumbImage;
    vm.loadExistingCard = loadExistingCard;
    vm.cardSharedUser = [];
    vm.initCardObjects = initCardObjects;
    vm.deleteCard = deleteCard;
    vm.deleteItemsConfirm = deleteItemsConfirm;
    vm.favoriteCard = favoriteCard;
    vm.findFavoriteCardItem = findFavoriteCardItem;
    vm.changeCardBackground = changeCardBackground;
    vm.viewMyCards = viewMyCards;
    vm.viewTemplates = viewTemplates;
    vm.showElements = showElements;
    vm.showTexFormats = showTexFormats;
    vm.showBackgroundOptions = showBackgroundOptions;
    vm.uploadFile = uploadFile;
    vm.peopleSearch = peopleSearch;
    vm.selectedPeopleChange = selectedPeopleChange;


    // Methods
    vm.toggleSelectContact = toggleSelectContact;
    vm.toggleSidenav = toggleSidenav;
    vm.generateImage = generateImage;
    vm.changeGradientColor = changeGradientColor;
    vm.printCard = printCard;
    vm.closeDialog = closeDialog;
    vm.insertText = insertText;
    vm.insertImage = insertImage;
    vm.removeLayer = removeLayer;
    vm.changeCardFontSize = changeCardFontSize;
    vm.changeCardTextStyleBold = changeCardTextStyleBold;
    vm.changeCardTextStyleItalic = changeCardTextStyleItalic;
    vm.changeLayerArea = changeLayerArea;
    vm.changeCardTextAlign = changeCardTextAlign;
    vm.changeCapitalize = changeCapitalize;
    vm.changeSelectedTextColor = changeSelectedTextColor;
    vm.cardArrays = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40];
    vm.changeCardBackgroundImage = changeCardBackgroundImage;
    vm.increaseCardArea = increaseCardArea;
    vm.decreaseCardArea = decreaseCardArea;
    vm.changeCardViewArea = changeCardViewArea;
    vm.selectLayer = selectLayer;
    vm.saveCurrentTemplate = saveCurrentTemplate;
    vm.changeCardTemplate = changeCardTemplate;
    vm.changeLetterSpacing = changeLetterSpacing;
    vm.moveToForwardLayer = moveToForwardLayer;
    vm.moveToBackwardLayer = moveToBackwardLayer;
    vm.copyCurrentLayer = copyCurrentLayer;
    vm.changeLineHeight = changeLineHeight;
    vm.changeLayerTransparency = changeLayerTransparency;
    vm.setBackgroundGrid = setBackgroundGrid;
    vm.setupLayerToFitSize = setupLayerToFitSize;
    vm.setCardBackgroundPalette = setCardBackgroundPalette;
    vm.changeBgColor = changeBgColor;
    vm.getGradientOutput = getGradientOutput;
    vm.addLayerToGradient = addLayerToGradient;
    vm.deleteCurrentGCL = deleteCurrentGCL;
    vm.addToGradientColors = addToGradientColors;
    vm.showGradientPicker = showGradientPicker;
    vm.addTextLayer = addTextLayer;
    vm.changeEventVenue = changeEventVenue;
    vm.insertImageToCard = insertImageToCard;
    vm.applyImageShape = applyImageShape;
    vm.resetCardDataToDefault = resetCardDataToDefault;
    vm.viewCard = viewCard;
    vm.shareOnline = shareOnline;
    vm.removePeopleFromShare = removePeopleFromShare;
    vm.shareCard = shareCard;
    vm.downloadPdf = downloadPdf;
    vm.saveSvgPopup = saveSvgPopup;
    vm.saveSvgItem = saveSvgItem;
    vm.addSvgLayer = addSvgLayer;
    vm.changeSelectedSvgColor = changeSelectedSvgColor;


    vm.contacts = [];

    vm.demoCard = {percent: 33, users: [], userArr: [], styles: {width: '559.35px', height: '372.9px', 'background': 'url(/images/cards/bg-grid/3.jpg)', 'background-size': 'cover'}, temp: {styles: {top: '0px', left: '0px'}},
      layers: [{id: 'layer1', venue: 'name', type: 'text', value: 'Alex Wolf', class: 'name', index: 1, styles: {'top': '47.5625px', 'left': '141.188px', 'width': '287.25px', 'height': '55.25px', 'font-family': 'Bevan', 'font-size': '34px', 'text-transform':'uppercase', 'letter-spacing': '5px', 'font-weight': 'normal'}},
        {id: 'layer2', venue: 'day', type: 'text', value: 'Saturday', class: 'name', index: 2, styles: {'top': '107.923px', 'left': '115.085px', 'width': '113.25px', 'height': '40.25px', 'font-family': 'Bentham', 'font-size': '25px', 'text-align':'center', 'color': 'rgb(120, 63, 4)'}},
        {id: 'layer3', venue: 'date', type: 'text', value: '10/12/2018', class: 'date', index: 3, styles: {'top': '145.519px', 'left': '94.776px', 'width': '147.25px', 'height': '38.25px', 'font-family': 'Bentham', 'font-size': '25px', 'text-align':'center', 'color': 'rgb(127, 96, 0)'}},
        {id: 'layer4', type: 'text', value: '@', class: 'name', index: 4, styles: {'top': '109.689px', 'left': '243.354px', 'width': '78.25px', 'height': '73.25px', 'font-size': '50px', 'text-align':'center', 'color': 'rgb(194, 123, 160)', 'font-style': 'normal'}},
        {id: 'layer5', venue: 'time_from', type: 'text', value: '5:30pm', class: 'time', index: 5, styles: {'top': '121.532px', 'left': '328.241px', 'width': '131.25px', 'height': '49.25px', 'font-family': 'Brawler', 'font-size': '32px', 'text-align':'center', 'color': 'rgb(142, 124, 195)'}},
        {id: 'layer6', venue: 'address', type: 'text', value: '103 Linrow Lane', class: 'address', index: 6, styles: {'top': '200.257px', 'left': '146.681px', 'width': '250.25px', 'height': '39.25px', 'font-family': 'Anonymous Pro', 'font-size': '25px', 'text-align':'center', 'color': 'rgb(7, 55, 99)', 'text-transform': 'uppercase'}},
        {id: 'layer7', venue: 'address_2', type: 'text', value: 'Avila, SOCHARA', class: 'address', index: 7, styles: {'top': '244.867px', 'left': '156.603px', 'width': '232.25px', 'height': '39.25px', 'font-family': 'Architects Daughter', 'font-size': '25px', 'text-align':'center', 'color': 'rgb(153, 0, 0)', 'text-transform': 'uppercase'}},
        {id: 'layer8', venue: 'rsvp', type: 'text', value: 'RSVP(000)xxx-xxxx BY 10/10/2015', class: 'info', index: 8, styles: {'top': '297.016px', 'left': '82.6875px', 'width': '377.25px', 'height': '32.25px', 'font-family': 'Anonymous Pro', 'font-size': '18px', 'text-transform':'uppercase', 'letter-spacing': '1px', 'text-align': 'center', 'color': 'rgb(76, 17, 48)'}},
      ]};

    /**
     * Description
     * @method init
     * @return 
     */
    function init() {
      vm.sideCurrentTab = 'templates';
      $rootScope.searchable = false;
      initCardObjects();
      vm.loadingCards = true;
      $rootScope.loadingProgress = true;
      $timeout( function(){
        getContacts();
        getCardTemplates();
      }, 1000 );
    }

    init();

    /**
     * Description
     * @method getCardTemplates
     * @return 
     */
    function getCardTemplates(){
      CardService.getTemplates().then(function (respTemplate) {
        vm.cardTemplates = respTemplate;
        $rootScope.loadingProgress = false;
        // vm.cardData = respTemplate[1];
      });
    }

    /**
     * Description
     * @method changeCardBackground
     * @param {} file
     * @return 
     */
    function changeCardBackground(file){
      vm.cardBgImage = file;
      if(file){
        Upload.imageDimensions(file).then(function(dimensions){
          vm.cardData.styles.width = dimensions.width;
          vm.cardData.styles.height = dimensions.height;
        });
      }
    }

    vm.cardGradients = ['linear-gradient(to bottom, rgba(0,183,234,1) 0%,rgba(0,158,195,1) 100%)', 'linear-gradient(to bottom, rgba(169,3,41,1) 0%,rgba(143,2,34,1) 44%,rgba(109,0,25,1) 100%)'
      ,'linear-gradient(to bottom, rgba(254,252,234,1) 0%,rgba(241,218,54,1) 100%)', 'linear-gradient(to bottom, rgba(205,235,142,1) 0%,rgba(165,201,86,1) 100%)'
      ,'linear-gradient(to bottom, rgba(96,108,136,1) 0%,rgba(63,76,107,1) 100%)', 'linear-gradient(to bottom, rgba(245,246,246,1) 0%,rgba(219,220,226,1) 21%,rgba(184,186,198,1) 49%,rgba(221,223,227,1) 80%,rgba(245,246,246,1) 100%)'
      ,'linear-gradient(to bottom, rgba(254,187,187,1) 0%,rgba(254,144,144,1) 45%,rgba(255,92,92,1) 100%)', 'linear-gradient(to bottom, rgba(254,204,177,1) 0%,rgba(241,116,50,1) 50%,rgba(234,85,7,1) 51%,rgba(251,149,94,1) 100%)'
      ,'linear-gradient(to bottom, rgba(167,207,223,1) 0%,rgba(35,83,138,1) 100%)', 'linear-gradient(to bottom, rgba(254,191,1,1) 0%,rgba(254,191,1,1) 100%)'
      ,'linear-gradient(to bottom, rgba(246,248,249,1) 0%,rgba(229,235,238,1) 50%,rgba(215,222,227,1) 51%,rgba(245,247,249,1) 100%)'];

    vm.cardColors = ['#54edeb', '#a4fafc', '#46db78','#81e77a', '#9eb749','#fb675b','#ff7896','#ff7ccb','#dc21a9','#adb8ff','#e1e1e1'];

    /**
     * Description
     * @method viewMyCards
     * @return 
     */
    function viewMyCards(){
      getCards();
      vm.sideCurrentTab = 'cards';
      vm.selectedTxtTab = 'my_cards';
    }

    /**
     * Description
     * @method viewTemplates
     * @return 
     */
    function viewTemplates(){
      vm.sideCurrentTab = 'templates';
    }

    /**
     * Description
     * @method showElements
     * @return 
     */
    function showElements(){
      vm.sideCurrentTab = 'elements';
      vm.selectedTxtTab = 'basic';
      var shapeElements = ['polygon(50% 0%, 0% 100%, 100% 100%)', 'polygon(20% 0%, 80% 0%, 100% 100%, 0% 100%)', 'polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%)',
        'polygon(25% 0%, 100% 0%, 75% 100%, 0% 100%)', 'polygon(50% 0%, 100% 38%, 82% 100%, 18% 100%, 0% 38%)', 'polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%)',
        'polygon(50% 0%, 90% 20%, 100% 60%, 75% 100%, 25% 100%, 0% 60%, 10% 20%)', 'polygon(30% 0%, 70% 0%, 100% 30%, 100% 70%, 70% 100%, 30% 100%, 0% 70%, 0% 30%)',
        'polygon(50% 0%, 83% 12%, 100% 43%, 94% 78%, 68% 100%, 32% 100%, 6% 78%, 0% 43%, 17% 12%)', 'polygon(50% 0%, 80% 10%, 100% 35%, 100% 70%, 80% 90%, 50% 100%, 20% 90%, 0% 70%, 0% 35%, 20% 10%)',
        'polygon(20% 0%, 80% 0%, 100% 20%, 100% 80%, 80% 100%, 20% 100%, 0% 80%, 0% 20%)','polygon(0% 15%, 15% 15%, 15% 0%, 85% 0%, 85% 15%, 100% 15%, 100% 85%, 85% 85%, 85% 100%, 15% 100%, 15% 85%, 0% 85%)',
        'polygon(40% 0%, 40% 20%, 100% 20%, 100% 80%, 40% 80%, 40% 100%, 0% 50%)', 'polygon(0% 20%, 60% 20%, 60% 0%, 100% 50%, 60% 100%, 60% 80%, 0% 80%)',
        'polygon(25% 0%, 100% 1%, 100% 100%, 25% 100%, 0% 50%)','polygon(0% 0%, 75% 0%, 100% 50%, 75% 100%, 0% 100%)','polygon(100% 0%, 75% 50%, 100% 100%, 25% 100%, 0% 50%, 25% 0%)',
        'polygon(75% 0%, 100% 50%, 75% 100%, 0% 100%, 25% 50%, 0% 0%)','polygon(50% 0%, 61% 35%, 98% 35%, 68% 57%, 79% 91%, 50% 70%, 21% 91%, 32% 57%, 2% 35%, 39% 35%)',
        'polygon(10% 25%, 35% 25%, 35% 0%, 65% 0%, 65% 25%, 90% 25%, 90% 50%, 65% 50%, 65% 100%, 35% 100%, 35% 50%, 10% 50%)','polygon(0% 0%, 100% 0%, 100% 75%, 75% 75%, 75% 100%, 50% 75%, 0% 75%)',
        'polygon(20% 0%, 0% 20%, 30% 50%, 0% 80%, 20% 100%, 50% 70%, 80% 100%, 100% 80%, 70% 50%, 100% 20%, 80% 0%, 50% 30%)','polygon(0% 0%, 0% 100%, 25% 100%, 25% 25%, 75% 25%, 75% 75%, 25% 75%, 25% 100%, 100% 100%, 100% 0%)',
        'inset(5% 20% 15% 10%)','circle(50% at 50% 50%)','ellipse(25% 40% at 50% 50%)'
      ];
      vm.shapeElements = shapeElements.map(function(shap){return {'-webkit-clip-path': shap, 'clip-path': shap};});
      CardService.getCardElements().then(function (respEl) {
        vm.cardShapeBasic = respEl.filter(function (sh) {
          return sh.type === 'basic';
        });

        vm.cardShapeLine = respEl.filter(function (sh) {
          return sh.type === 'line';
        });
      });
      loadCardImages();
    }

    /**
     * Description
     * @method saveSvgPopup
     * @return 
     */
    function saveSvgPopup(){
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/cards/dialogs/save-svg.html',
        parent: angular.element($document.body),
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method saveSvgItem
     * @return 
     */
    function saveSvgItem(){
      CardService.saveShapeSvg({element: vm.svgElement}).then(function (respCard) {
        showMessage('Save Done with ID: ' + respCard.id);
      });
    }

    /**
     * Description
     * @method showTexFormats
     * @return 
     */
    function showTexFormats(){
      vm.sideCurrentTab = 'text';
      vm.selectedTxtTab = 'event';
      vm.textStyles = [
        {'font-weight': 400, 'font-family': 'Aclonica'},
        {'font-weight': 400, 'font-family': 'Limelight'},
        {'font-weight': 400, 'font-family': 'Pacifico'},
        {'font-weight': 400, 'font-family': 'Reenie Beanie'},
        {'font-weight': 400, 'font-family': 'Megrim'},
        {'font-weight': 400, 'font-family': 'MedievalSharp'},
        {'font-weight': 400, 'font-family': 'Over the Rainbow'},
        {'font-weight': 400, 'font-family': 'Orbitron'},
        {'font-weight': 400, 'font-family': 'Annie Use Your Telescope'},
        {'font-weight': 400, 'font-family': 'Asset', 'font-size': '17px'},
        {'font-weight': 400, 'font-family': 'Astloch'},
        {'font-weight': 400, 'font-family': 'Nixie One'},
        {'font-weight': 400, 'font-family': 'Mountains of Christmas'},
        {'font-weight': 400, 'font-family': 'Monofett'},
        {'font-weight': 400, 'font-family': 'Miltonian'},
        {'font-weight': 400, 'font-family': 'Gruppo'},
        {'font-weight': 400, 'font-family': 'Give You Glory'},
        {'font-weight': 400, 'font-family': 'Josefin Slab'},
        {'font-weight': 400, 'font-family': 'Indie Flower'},
        {'font-weight': 400, 'font-family': 'La Belle Aurore'},
        {'font-weight': 400, 'font-family': 'Miltonian Tattoo'},
        {'font-weight': 400, 'font-family': 'Michroma'},
        {'font-weight': 400, 'font-family': 'Kranky'},
        {'font-weight': 400, 'font-family': 'Crafty Girls'},
        {'font-weight': 400, 'font-family': 'Lobster Two'},
        {'font-weight': 400, 'font-family': 'Bevan'},
        {'font-weight': 400, 'font-family': 'Bangers'},
        {'font-weight': 400, 'font-family': 'Crushed', 'letter-spacing': '5px', 'text-shadow': '0px 0px 16px #f9fdff'},
        {'font-weight': 400, 'font-family': 'Nova Cut', 'letter-spacing': '5px'},
        {'font-weight': 600, 'font-family': 'Zeyada', 'letter-spacing': '3px'},
        {'font-weight': 400, 'font-family': 'Yeseva One', 'letter-spacing': '1px'},
        {'font-weight': 400, 'font-family': 'Yanone Kaffeesatz', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Wire One', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Walter Turncoat', 'letter-spacing': '3px'},
        {'font-weight': 400, 'font-family': 'Wallpoet', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Waiting for the Sunrise', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'VT323', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Calligraffitti', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Vibur', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Varela', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'UnifrakturMaguntia', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Unkempt', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Ultra', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Ubuntu', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'The Girl Next Door', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Terminal Dosis Light', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Tenor Sans', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Tangerine', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Syncopate', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Swanky and Moo Moo', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Sunshiney', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Sue Ellen Francisco', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Stardos Stencil', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Special Elite', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Smythe', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Slackey', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Six Caps', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Sigmar One', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Shanti', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Shadows Into Light', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Schoolbell', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Ruslan Display', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Rock Salt', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Redressed', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Radley', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Quattrocento Sans', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'PT Sans', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Playfair Display', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Philosopher', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Permanent Marker', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Paytone One', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Patrick Hand', 'letter-spacing': '0px'},
        {'font-weight': 400, 'font-family': 'Meddon', 'letter-spacing': '0px'},
        {'font-weight': 600, 'font-family': 'League Script', 'letter-spacing': '0px'},
      ];
    }

    /**
     * Description
     * @method showBackgroundOptions
     * @return 
     */
    function showBackgroundOptions(){
      vm.sideCurrentTab = 'background_options';
      loadCardBackground();
    }

    /**
     * Description
     * @method uploadFile
     * @param {} files
     * @param {} type
     * @return 
     */
    function uploadFile(files, type){
      files = files.filter(function (file) {
        return file.type && file.type.length;
      });
      $rootScope.loadingProgress = true;
      if (files && files.length) {
        var file = files[0];
        file.upload = Upload.upload({
          url: '/cards/saveImageBackground',
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          data: {file: file, output: 'file'}
        });
        file.upload.then(function (response) {
          $timeout(function () {

            var uploadedFileData = response.data;
            $scope.files = null;

            saveFileFor( uploadedFileData, type);
          });
        });
      }
    }

    /**
     * Description
     * @method saveFileFor
     * @param {} data
     * @param {} type
     * @return 
     */
    function saveFileFor(data, type){
      data.type = type;
      CardService.saveFileFor(data).then(function (resp) {
        $rootScope.loadingProgress = false;
        if(type ==='image'){
          loadCardImages();
          insertImageToCard(resp);
        }else{
          loadCardBackground();
          setBackgroundGrid(resp);
        }
      });
    }

    /**
     * Description
     * @method loadCardBackground
     * @return 
     */
    function loadCardBackground(){
      $timeout(function () {
        CardService.getCardBackground().then(function (resp) {
          vm.cardBackgrounds = resp;
        });
      });
    }

    /**
     * Description
     * @method loadCardImages
     * @return 
     */
    function loadCardImages(){
      $timeout(function () {
        CardService.getCardImages().then(function (resp) {
          vm.cardImages = resp;
        });
      });
    }

    /**
     * Description
     * @method initCardObjects
     * @param {} init
     * @return 
     */
    function initCardObjects(init) {
      vm.selectedContactIdForShare = [];
      vm.cardData = vm.demoCard;
      vm.cardData.layerPosition = {};
      vm.cardData.eventDate = new Date();
      vm.cardData.eventStartTime = new Date();
      vm.cardData.eventEndTime = new Date();
      vm.sideCurrentTab = 'templates';


      if (init) {
        vm.changeBgImage = null;
        vm.currentTabIndex = 1;
      }

      vm.selectedCardBgImage = 1;
      // vm.cardData.styles.background = vm.site_url + '/images/cards/main/1.png';
      vm.cardViewPercent = 20;

      setupLayerToFitSize();
      loadExtarnalJavascript();
      $timeout(function () {
        makeDraggable();
      }, 2000);
    }

    /**
     * Description
     * @method loadExtarnalJavascript
     * @return 
     */
    function loadExtarnalJavascript(){
      var jsTag = document.createElement("script");
      jsTag.src = "/js/dependencies/card/sochara_rotate.js";
      document.getElementsByTagName("head")[0].appendChild(jsTag);
    }

    /**
     * Description
     * @method makeDraggable
     * @param {} id
     * @param {} aRatio
     * @return 
     */
    function makeDraggable( id , aRatio ){
      id = id || '#cardContent .card___content_wrap .card___content_layer';
      angular.element(id).draggable({
        containment: angular.element('#cardContent .card___content_wrap'),
        /**
         * Description
         * @method drag
         * @return 
         */
        drag:function () {
          var layerId = angular.element(this).attr('id');
          if(layerId){
            vm.cardData.layers.getBy('id', layerId).styles.left = angular.element(this).css('left');
            vm.cardData.layers.getBy('id', layerId).styles.top = angular.element(this).css('top');
          }
        }
      }).on('click', function(e) {
        var eSel = angular.element(e.target);
        if(eSel.hasClass('draggable') || eSel.hasClass('ui-resizable-handle') || eSel.hasClass('ui-rotatable-handle')){
          angular.element(this).draggable( {disabled: false});
        }
      }).dblclick(function() {
        angular.element(this).draggable({ disabled: true });
      }).resizable({
        aspectRatio: aRatio || false,
        /**
         * Description
         * @method resize
         * @return 
         */
        resize: function(){
          vm.cardData.layers.getBy('id', angular.element(this).attr('id')).styles.width = angular.element(this).css('width');
          vm.cardData.layers.getBy('id', angular.element(this).attr('id')).styles.height = angular.element(this).css('height');
        }
      }).rotatable({
        /**
         * Description
         * @method rotate
         * @return 
         */
        rotate: function () {
          if(angular.element(this).attr('id')){
            vm.cardData.layers.getBy('id', angular.element(this).attr('id')).styles.transform = angular.element(this).css('transform');
          }
        }
      });
    }



    /**
     * Description
     * @method setupLayerToFitSize
     * @return 
     */
    function setupLayerToFitSize(){
      $timeout(function () {
        changeCardViewArea(33);
      });
    }

    /**
     * Description
     * @method setCardBackgroundPalette
     * @param {} palette
     * @return 
     */
    function setCardBackgroundPalette(palette){
      if(palette){
        vm.cardData.styles['background'] = palette;
      }
    }

    /**
     * Description
     * @method changeBgColor
     * @param {} color
     * @return 
     */
    function changeBgColor(color){
      if(color){
        vm.cardData.styles['background'] = color;
      }
    }

    /**
     * Description
     * @method addLayerToGradient
     * @return 
     */
    function addLayerToGradient(){
      vm.gradientLayers.push({color: '#ffffff', loc: 50});
    }

    /**
     * Description
     * @method deleteCurrentGCL
     * @param {} layer
     * @return 
     */
    function deleteCurrentGCL(layer){
      var c__index = vm.gradientLayers.indexOf(layer);
      if(c__index>-1){
        vm.gradientLayers.splice(c__index, 1);
      }
    }

    /**
     * Description
     * @method addToGradientColors
     * @return 
     */
    function addToGradientColors(){
      vm.cardGradients.push(vm.gradientOutput.background);
      vm.showGradientColorPicker = false;
    }

    /**
     * Description
     * @method changeEventVenue
     * @param {} val
     * @param {} type
     * @return 
     */
    function changeEventVenue(val, type){
      var randNum, newLayer, city, state, zip;
      if(vm.cardData.layers.getBy('venue', type)){
        vm.cardData.layers.getBy('venue', type)['value'] = val;
      }else if(['city', 'state', 'zip'].indexOf(type)>-1){
        city = vm.currentEventCity || '';
        state = vm.currentEventState ? ', ' + vm.currentEventState : '';
        zip = vm.currentEventZip ? ', ' + vm.currentEventZip : '';
        var address_2 = city + state + zip;
        if(vm.cardData.layers.getBy('venue', 'address_2')){
          vm.cardData.layers.getBy('venue', 'address_2')['value'] = address_2;
        }else{
          randNum = parseInt(Math.random()* (1111 - 111) + 111);
          newLayer = {id: 'layer' + 11 + randNum, type: 'text', venue: 'address_2', class: 'address_2', index: 11 + randNum, value: val, styles: {'font-size': '20px', 'top': '50px', 'left': '50px', 'width': '200px'}};
          vm.cardData.layers.push(newLayer);
          $timeout(function () {
            makeDraggable('div#'+newLayer.id);
          }, 1000);
        }
      }else{
        randNum = parseInt(Math.random()* (1111 - 111) + 111);
        newLayer = {id: 'layer' + 11 + randNum, type: 'text', venue: type, class: type, index: 11 + randNum, value: val, styles: {'font-size': '20px', 'top': '50px', 'left': '50px', 'width': '200px'}};
        vm.cardData.layers.push(newLayer);
        $timeout(function () {
          makeDraggable('div#'+newLayer.id);
        }, 1000);
      }
    }

    /**
     * Description
     * @method addTextLayer
     * @param {} style
     * @param {} text
     * @return 
     */
    function addTextLayer( style, text ){
      style = angular.copy(style);
      var randNum = parseInt(Math.random()* (1111 - 111) + 111);
      var lastLayer = vm.cardData.layers[vm.cardData.layers.length - 1] || {id: 'layer' + 11 + randNum, index: 11 + randNum, styles: {'font-size': '20px'}};
      var newLayer = {id: lastLayer.id + 111, type: 'text', class: 'name', index: lastLayer.index + 111, value: text || 'Sample', styles: style};
      newLayer.styles.top = '50px';
      newLayer.styles.left = '50px';
      newLayer.styles.width = '200px';
      newLayer.styles['font-size'] = style['font-size'] || lastLayer.styles['font-size'];
      vm.cardData.layers.push(newLayer);
      $timeout(function () {
        makeDraggable('div#'+newLayer.id);
      }, 1000);
    }

    /**
     * Description
     * @method addSvgLayer
     * @param {} svg
     * @return 
     */
    function addSvgLayer( svg ){
      var randNum = parseInt(Math.random()* (1111 - 111) + 111);
      var lastLayer = vm.cardData.layers[vm.cardData.layers.length - 1] || {id: 'layer' + 181 + randNum, index: 131 + randNum, styles: {'font-size': '20px'}};
      var newLayer = {id: lastLayer.id + 'svg' + 111, type: 'svg', class: 'shape_svg', index: lastLayer.index + 151, value: svg.element, styles: {}};
      newLayer.styles.top = '50px';
      newLayer.styles.left = '50px';
      newLayer.styles.width = '120px';
      newLayer.styles.height = '120px';
      vm.cardData.layers.push(newLayer);
      $timeout(function () {
        makeDraggable('div#'+newLayer.id, false);
      }, 1000);
    }

    /**
     * Description
     * @method changeSelectedSvgColor
     * @param {} color
     * @param {} classNameB
     * @return 
     */
    function changeSelectedSvgColor( color, classNameB ){
      var className = '#' + vm.activeLayer.id + ' .shape.svg.content_svg svg .color';
      angular.element( className + '.' + classNameB ).css({'fill':color });
    }

    /**
     * Description
     * @method insertImageToCard
     * @param {} image
     * @return 
     */
    function insertImageToCard( image ){
      var randNum = parseInt(Math.random()* (1111 - 111) + 111);
      var lastLayer = vm.cardData.layers[vm.cardData.layers.length - 1] || {id: 'layer' + 11 + randNum, index: 11 + randNum};
      var newLayer = {id: lastLayer.id + 111, type: 'image', class: 'image', value: image.url, index: lastLayer.index + 111, styles: {top: '50px', left: '50px', width: '200px', height: '200px', background: 'url('+image.url+')'}};
      vm.cardData.layers.push(newLayer);
      $timeout(function () {
        makeDraggable('div#'+newLayer.id);
      }, 1);
    }

    /**
     * Description
     * @method showGradientPicker
     * @return 
     */
    function showGradientPicker(){
      if(!vm.showGradientColorPicker){
        vm.gradientLayers = [{color: '#a7cfdf', loc: 0}, {color: '#23538a', loc: 100}];
        vm.showGradientColorPicker = true;
      }else{
        vm.showGradientColorPicker = false;
      }
    }

    /**
     * Description
     * @method getGradientOutput
     * @return 
     */
    function getGradientOutput(){
      // linear-gradient(to bottom, rgba(167,207,223,1) 0%,rgba(35,83,138,1) 100%)
      var gc__layer = [], gc__output;
      gc__layer.push('to bottom');
      vm.gradientLayers.forEach(function (layer) {
        gc__layer.push(layer.color + ' ' + layer.loc + '%');
      });
      gc__output = 'linear-gradient(' + gc__layer.join(',') + ')';
      vm.gradientOutput = {background: gc__output};
      vm.cardData.styles['background'] = gc__output;
    }


    /**
     * Description
     * @method increaseCardArea
     * @return 
     */
    function increaseCardArea(){
      var size = vm.cardViewPercent + 10;
      if( size > 100){
        changeCardViewArea(100);
      }else{
        changeCardViewArea(size);
      }
    }

    /**
     * Description
     * @method changeWidthHeight
     * @param {} layer
     * @param {} index
     * @param {} size
     * @return 
     */
    function changeWidthHeight(layer, index, size){
      var lw = layer.styles && layer.styles['width'] ? layer.styles['width'] : '';
      lw = parseFloat(lw.replace('px', ''));
      lw = lw ? ((lw*size)/vm.cardData.percent)+'px' : '';
      vm.cardData.layers[index]['styles']['width'] = lw;

      var lh = layer.styles && layer.styles['height'] ? layer.styles['height'] : '';
      lh = parseFloat(lh.replace('px', ''));
      lh = lh ? ((lh*size)/vm.cardData.percent)+'px' : '';
      vm.cardData.layers[index]['styles']['height'] = lh;
    }

    /**
     * Description
     * @method changeCardTransform
     * @param {} layer
     * @param {} index
     * @param {} size
     * @return 
     */
    function changeCardTransform(layer, index, size){
      var ct = layer.styles && layer.styles['top'] ? layer.styles['top'] : '';
      ct = parseFloat(ct.replace('px', ''));
      ct = ct ? ((ct*size)/vm.cardData.percent)+'px' : '';
      vm.cardData.layers[index]['styles']['top'] = ct;

      var cl = layer.styles && layer.styles['left'] ? layer.styles['left'] : '';
      cl = parseFloat(cl.replace('px', ''));
      cl = cl ? ((cl*size)/vm.cardData.percent)+'px' : '';
      vm.cardData.layers[index]['styles']['left'] = cl;
    }

    /**
     * Description
     * @method decreaseCardArea
     * @return 
     */
    function decreaseCardArea(){
      var size = vm.cardViewPercent - 10;
      if( size < 20){
        changeCardViewArea(20);
      }else{
        changeCardViewArea(size);
      }
    }

    /**
     * Description
     * @method changeFontLayer
     * @param {} font
     * @return 
     */
    function changeFontLayer(font){
      vm.cardData.layers.getBy('id', vm.activeLayer.id).styles['font-family'] =  font.name;
    }
    /**
     * Description
     * @method changeFontSizeLayer
     * @param {} size
     * @return 
     */
    function changeFontSizeLayer(size){
      vm.cardData.layers.getBy('id', vm.activeLayer.id).styles['font-size'] = size + 'px';
    }


    /**
     * Description
     * @method changeLetterSpacing
     * @param {} space
     * @return 
     */
    function changeLetterSpacing(space){
      if(space && vm.activeLayer){
        vm.cardData.layers.getBy('id', vm.activeLayer.id).styles['letter-spacing'] = space + 'px';
      }
    }

    /**
     * Description
     * @method moveToForwardLayer
     * @return 
     */
    function moveToForwardLayer(){
      var currentIndex = vm.cardData.layers.indexOf(vm.activeLayer);
      var tempLayer = [];
      if(currentIndex!==0){
        vm.cardData.layers.forEach(function(ar, ind){
          var lInd = ind - 1 === -1 ? vm.cardData.layers.length - 1 : ind-1 ;
          tempLayer[lInd] = ar;
        });
        vm.cardData.layers = tempLayer;
        vm.activeLayer = tempLayer[currentIndex-1];
      }
    }

    /**
     * Description
     * @method moveToBackwardLayer
     * @return 
     */
    function moveToBackwardLayer(){
      var currentIndex = vm.cardData.layers.indexOf(vm.activeLayer);
      var tempLayer = [];
      if(currentIndex!==(vm.cardData.layers.length-1)){
        vm.cardData.layers.forEach(function(ar, ind){
          var lInd = ind + 1 === vm.cardData.layers.length ? 0 : ind+1 ;
          tempLayer[lInd] = ar;
        });
        vm.cardData.layers = tempLayer;
        vm.activeLayer = tempLayer[currentIndex+1];
      }
    }

    /**
     * Description
     * @method copyCurrentLayer
     * @return 
     */
    function copyCurrentLayer(){
      var newLayer = angular.copy(vm.activeLayer);
      newLayer.id = 'layer' + parseInt(Math.floor(Date.now() / 1000));
      newLayer.index = parseInt(Math.floor(Date.now() / 1000));
      newLayer.styles['top'] = parseInt(newLayer.styles['top']) + 10 + 'px';
      vm.cardData.layers.push(newLayer);
      $timeout(function () {
        makeDraggable('div#'+newLayer.id);
      });
    }

    /**
     * Description
     * @method changeLineHeight
     * @param {} height
     * @return 
     */
    function changeLineHeight(height){
      if(height && vm.activeLayer){
        vm.cardData.layers.getBy('id', vm.activeLayer.id).styles['line-height'] = height + 'px';
      }
    }

    /**
     * Description
     * @method changeLayerTransparency
     * @param {} trans
     * @return 
     */
    function changeLayerTransparency(trans){
      if(trans && vm.activeLayer){
        vm.cardData.layers.getBy('id', vm.activeLayer.id).styles['opacity'] = trans;
        vm.cardData.layers.getBy('id', vm.activeLayer.id).styles['filter'] = "alpha(opacity="+trans*100+")";
      }
    }

    /**
     * Description
     * @method setBackgroundGrid
     * @param {} index
     * @return 
     */
    function setBackgroundGrid(index){
      var image = typeof index === 'object' && index.url ? index.url : '/images/cards/bg-grid/' + index + '.jpg';
      vm.cardData.styles.background = 'url(' + image + ')';
    }

    /**
     * Description
     * @method selectLayer
     * @param {} layer
     * @return 
     */
    function selectLayer(layer){
      if( layer && layer.type ){
        getCardEditoryReadyFor(layer);
      }
      vm.activeLayer = layer;
    }

    /**
     * Description
     * @method getCardEditoryReadyFor
     * @param {} layer
     * @return 
     */
    function getCardEditoryReadyFor(layer) {
      if (layer.type === 'svg'){
        vm.activeSvgColors = [];
        for( var i = 1; i < 8; i++ ) {
          var colorName = 'color-' + i, className = '#' + layer.id + ' .shape.svg.content_svg svg .color';
          if(angular.element( className ).hasClass( colorName )){
            vm.activeSvgColors.push({class: colorName, color: angular.element( className + '.' + colorName).css('fill')});
          }
        }
      }
    }

    /**
     * Description
     * @method changeCardTemplate
     * @param {} card
     * @return 
     */
    function changeCardTemplate(card){
      card.layers = card.layers.filter(function (layer) {
        return layer!== null;
      });
      vm.cardData.layers = card.layers;
      vm.cardData.styles = card.styles;
      vm.cardData.percent = card.percent;
      setupLayerToFitSize();
      $timeout(function () {
        makeDraggable();
      });
    }

    /**
     * Description
     * @method saveCurrentTemplate
     * @return 
     */
    function saveCurrentTemplate(){
      var cardData = vm.cardData;
      CardService.saveTemplate(cardData).then(function (resp) {
        // console.log(resp);
      });
    }

    /**
     * Description
     * @method changeCardViewArea
     * @param {} size
     * @return 
     */
    function changeCardViewArea(size){
      vm.cardData.layers.forEach(function (layer, index) {
        changeCardTransform(layer, index, size);
        changefontSize(layer, index, size);
        changeWidthHeight(layer, index, size);
      });
      changeCardLayoutWidth(size);
      vm.cardData.percent = size;
      vm.cardViewPercent = size;
      $timeout(function () {
        setCardAreaToMiddle();
      });
    }

    /**
     * Description
     * @method setCardAreaToMiddle
     * @return 
     */
    function setCardAreaToMiddle(){
      var cw = angular.element("#cardContent").width();
      var ch = angular.element("#cardContent").height();
      var ccw = angular.element('.card___content_wrap').width();
      var cch = angular.element('.card___content_wrap').height();
      if(cw>ccw){
        var ccdw = Math.floor((cw/2 - ccw/2));
        vm.cardData.temp.styles['left'] = ccdw + 'px';

        var ccdh = Math.floor((ch/2 - (cch+50)/2));
        vm.cardData.temp.styles['top'] = ccdh + 'px';
        // console.log(ccd);
      }else{
        vm.cardData.temp.styles['left'] = '0px';
        vm.cardData.temp.styles['top'] = '0px';
      }
    }

    /**
     * Description
     * @method changeCardLayoutWidth
     * @param {} size
     * @return 
     */
    function changeCardLayoutWidth(size){
      var cw = parseFloat(vm.cardData.styles['width'].replace('px', ''));
      vm.cardData.styles['width'] = ((cw*size)/vm.cardData.percent)+'px';

      var ch = parseFloat(vm.cardData.styles['height'].replace('px', ''));
      vm.cardData.styles['height'] = ((ch*size)/vm.cardData.percent)+'px';
    }

    /**
     * Description
     * @method changefontSize
     * @param {} layer
     * @param {} index
     * @param {} size
     * @return 
     */
    function changefontSize(layer, index, size){
      var cfs = layer['styles'] && layer['styles']['font-size'] ? layer['styles']['font-size'] : '';
      cfs = parseFloat(cfs.replace('px', ''));
      cfs = cfs ? ((cfs*size)/vm.cardData.percent)+'px' : '';
      if(cfs){
        vm.cardData.layers[index]['styles']['font-size'] = cfs;
      }
    }

    /**
     * Description
     * @method changeCardBackgroundImage
     * @param {} index
     * @return 
     */
    function changeCardBackgroundImage(index){
      vm.selectedCardBgImage = index;
      vm.changeBgImage = null;
      vm.cardData.styles.background = 'url(/images/cards/main/' + index + '.png'+')';
      vm.cardData.thumb = '/images/cards/thumb/' + index + '.jpg';
    }



    /**
     * Description
     * @method checkSelectedInput
     * @return ConditionalExpression
     */
    function checkSelectedInput(){
      var input = vm.selectedInputTxtId || '';
      var withNoDigits = input.replace(/[0-9]/g, '');
      return withNoDigits === 'textInput' || withNoDigits === 'insertedText' ? parseInt(input.replace(/\D/g,'')) : 0;
    }

    /**
     * Description
     * @method insertText
     * @param {} param
     * @return 
     */
    function insertText(param){
      var textToInsert = '';
      if(param && param === 'title'){
        textToInsert = vm.cardData.eventTitle || '';
      }else if(param && param === 'address'){
        textToInsert = vm.cardData.locationName || '' + ', ' + vm.cardData.city || '' + ', ' + vm.cardData.state || '' + ', ' + vm.cardData.zip || '';
      }else if(param && param === 'date'){
        textToInsert = moment(vm.cardData.eventDate, 'M/D/YYYY').format('dddd, D MMMM YYYY') || '';
      }else if(param && param === 'time'){
        textToInsert = moment(vm.cardData.eventStartTime).format('h:mm') + ' - ' + moment(vm.cardData.eventEndTime).format('h:mm');
      }
      if(vm.cardData.id){
        vm.insertedTxtId = vm.cardData.layers.length;
      }
      vm.insertedTxtId = vm.insertedTxtId ? ++vm.insertedTxtId : 1;
      vm.cardData.layers.push({type: 'text', id: vm.insertedTxtId, value: '', pos: {}, style: {}});
      angular.element('#cardContent').prepend(' <div class="draggable card_text" id="insertedText'+vm.insertedTxtId+'" style="position: absolute;text-align:left;">\n' +
        '            <input type="text" id="textInput'+ vm.insertedTxtId +'" class="tool txt_input" style="width: 100%;font-size:18px;text-align:left;background: transparent" value="'+textToInsert+'" placeholder="Input your text here" />\n' +
        '          </div>');
      angular.element('#insertedText'+vm.insertedTxtId).draggable({
        containment: angular.element('#cardContent'),
        /**
         * Description
         * @method drag
         * @return 
         */
        drag:function () {
          vm.cardData.layers.getBy('id', vm.insertedTxtId).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
        }
      }).resizable();
    }

    /**
     * Description
     * @method insertImage
     * @param {} file
     * @return 
     */
    function insertImage(file){
      Upload.dataUrl(file, true).then(function(urlSrc){
        if(urlSrc){
          vm.insertedTxtId = vm.insertedTxtId ? ++vm.insertedTxtId : 1;
          vm.cardData.layers.push({id: vm.insertedTxtId, type: 'image', src: urlSrc, pos: {}, style: {}});
          angular.element('#cardContent').prepend('<div id="resizableImage' + vm.insertedTxtId + '" class="resizable_image card_image" style="background-image:url(' + urlSrc + ');background-size: 100% 100% !important;background-repeat: no-repeat;background-position: center center;position: absolute;width: 200px;height: 200px;">' +
            '<div class="card_image_toolbar no-print">' +
            '<md-icon md-font-icon="icon-rotate-left" class="md-font material-icons icon-rotate-left" aria-label="icon-rotate-left"></md-icon>' +
            '<md-icon md-font-icon="icon-rotate-right" class="md-font material-icons icon-rotate-right" aria-label="icon-rotate-right"></md-icon></div> '
            + '</div>');
          angular.element("#resizableImage"+vm.insertedTxtId).draggable({
            containment: angular.element('#cardContent'),
            /**
             * Description
             * @method drag
             * @return 
             */
            drag: function(){
              vm.cardData.layers.getBy('id', vm.insertedTxtId).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
              // vm.cardData.layers.getBy('id', vm.insertedTxtId).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
            }
          }).resizable({
            aspectRatio: false
          });
        }
      });

    }

    document.onkeydown = onKeyDownFunc;
    /**
     * Description
     * @method onKeyDownFunc
     * @param {} e
     * @return 
     */
    function onKeyDownFunc(e){
      if(e.ctrlKey && e.keyCode == 'P'.charCodeAt(0)){
        e.preventDefault();
        e.stopPropagation();
        printCard();
        //your saving code
      }
    }


    /**
     * Description
     * @method removeLayer
     * @return 
     */
    function removeLayer(){
      var getIndex = vm.cardData.layers.indexOf(vm.activeLayer);
      if(getIndex>-1){
        delete vm.cardData.layers[getIndex];
      }
    }

    /**
     * Description
     * @method peopleSearch
     * @param {} query
     * @return MemberExpression
     */
    function peopleSearch(query) {
      vm.previousSearchText = query;
      var results = query ? ContactService.getAllSocharaUsers({name: query}).then(function (response) {
        var totalResponse = response.map(function (c) {
          var name = c.first_name + ' ' + c.last_name;
          return{
            name: name,
            email: c.email,
            photo_url: c.photo_url ? c.photo_url.replace('assets', '').replace(/\\/, '/') : '../images/avatars/blank.jpg',
            id: c.id || undefined,
            value: name.toLowerCase()
          };
        });

        var searchQuery = angular.lowercase(query);

        totalResponse = totalResponse.concat(vm.filteredContacts);
        return totalResponse.filter(function ( state ) {
          return state.value.indexOf(searchQuery) === 0 && vm.selectedContactIdForShare.indexOf(state.id) < 0;
        });
        // return totalResponse;
      }) : vm.filteredContacts.filter(function (cont) {
        return !cont.id || vm.selectedContactIdForShare.indexOf(cont.id) < 0;
      });

      var deferred = $q.defer();
      $timeout(function () {
        deferred.resolve(results);
      }, Math.random() * 1000, false);
      return deferred.promise;
    }

    /**
     * Description
     * @method selectedPeopleChange
     * @param {} resp
     * @return 
     */
    function selectedPeopleChange(resp) {
      if (!resp) {
        return;
      }
      vm.selectedContactIdForShare = vm.selectedContactIdForShare || [];
      var findIndex = vm.selectedContactIdForShare.indexOf(resp.id);
      if (findIndex > -1) {
        showMessage('This Contact already added', 'error');
      } else {
        if (typeof vm.cardData.users === 'object' ){
          vm.cardData.users.push(resp);
          vm.cardData.userArr.push(resp.id);
        }else {
          vm.cardData.users = [resp];
          vm.cardData.userArr = [resp.id];
        }
      }
      vm.searchText = vm.previousSearchText;
      vm.selectedItem = undefined;
      // document.querySelector('#input-11').blur();
      var peopleSearchInput = angular.element("input#searchPeople");
      if (peopleSearchInput) {
        peopleSearchInput.blur();
        peopleSearchInput.focus();
      }
    }

    /**
     * Description
     * @method shareCard
     * @return 
     */
    function shareCard(){
      closeDialog();
      if(vm.cardData.id){
        saveCard();
      }
    }

    /**
     * Description
     * @method loadJsPdf
     * @return 
     */
    function loadJsPdf(){
      var jsTag = document.createElement("script");
      jsTag.src = "/js/dependencies/card/jspdf.min.js";
      document.getElementsByTagName("head")[0].appendChild(jsTag);
    }

    /**
     * Description
     * @method downloadPdf
     * @return 
     */
    function downloadPdf(){
      loadJsPdf();
      $timeout(function () {
        changeCardViewArea(50);
        var pdf = new jsPDF();
        generateImage(function (imageDataUrl) {
          pdf.addImage(imageDataUrl, 'png', 0, 0);
          pdf.save("download.pdf");
          setupLayerToFitSize();
        });
      }, 2000);
    }

    /**
     * Description
     * @method removePeopleFromShare
     * @param {} item
     * @return 
     */
    function removePeopleFromShare( item ){
      var findIndex = vm.selectedContactIdForShare.indexOf(item.id);
      vm.selectedContactIdForShare.splice(findIndex,1);
      vm.cardData.users.splice(vm.cardData.users.indexOf(item),1);
    }

    /**
     * Description
     * @method saveCardPopup
     * @param {} ev
     * @return 
     */
    function saveCardPopup(ev) {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/cards/dialogs/save-card.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method shareOnline
     * @param {} ev
     * @return 
     */
    function shareOnline(ev) {
      vm.newFolderName = null;
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/cards/dialogs/share-online.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method changeCardFontSize
     * @param {} size
     * @return 
     */
    function changeCardFontSize(size){
      var selectedInputId = checkSelectedInput();
      if(selectedInputId){
        angular.element('#textInput'+selectedInputId).css('font-size', size);
        vm.cardData.layers.getBy('id', selectedInputId).style['font-size'] = size;
      }
    }

    /**
     * Description
     * @method viewCard
     * @param {} card
     * @return 
     */
    function viewCard(card){
      if(card){ card.temp = card.temp || {styles: {top: '0px', left: '0px'}}; vm.cardData = card; $timeout(function () {setCardAreaToMiddle();}); }
    }

    /**
     * Description
     * @method resetCardDataToDefault
     * @return 
     */
    function resetCardDataToDefault(){
      vm.cardData = vm.demoCard;
    }

    /**
     * Description
     * @method applyImageShape
     * @param {} shape
     * @return 
     */
    function applyImageShape(shape){
      if( vm.activeLayer && shape && vm.activeLayer.type === 'image'){
        vm.cardData.layers.getBy('id', vm.activeLayer.id).styles['clip-path'] = shape['clip-path'];
      }else{
        showMessage('Please select an image layer to apply the shape.');
      }
    }

    /**
     * Description
     * @method changeCardTextStyleBold
     * @param {} bold
     * @return 
     */
    function changeCardTextStyleBold(bold){
      if( vm.activeLayer){
        vm.cardData.layers.getBy('id', vm.activeLayer.id).styles['font-weight'] = bold ? 'bold' : 'normal';
      }
    }

    /**
     * Description
     * @method changeCardTextStyleItalic
     * @param {} italic
     * @return 
     */
    function changeCardTextStyleItalic(italic){
      if( vm.activeLayer){
        vm.cardData.layers.getBy('id', vm.activeLayer.id).styles['font-style'] = italic ? 'italic' : 'normal';
      }
    }

    /**
     * Description
     * @method changeLayerArea
     * @return 
     */
    function changeLayerArea(){
      if( vm.activeLayer){
        vm.cardData.layers.getBy('id', vm.activeLayer.id).type = 'list';
      }
    }


    /**
     * Description
     * @method changeSelectedTextColor
     * @param {} color
     * @return 
     */
    function changeSelectedTextColor(color){
      if( color && vm.activeLayer){
        vm.cardData.layers.getBy('id', vm.activeLayer.id).styles['color'] = color;
      }
    }

    /**
     * Description
     * @method changeCardTextAlign
     * @param {} align
     * @return 
     */
    function changeCardTextAlign(align){
      if( align && vm.activeLayer){
        vm.cardData.layers.getBy('id', vm.activeLayer.id).styles['text-align'] = align;
        vm.currentTextAlign = align;
      }
    }

    /**
     * Description
     * @method changeCapitalize
     * @return 
     */
    function changeCapitalize(){
      if( vm.activeLayer){
        vm.cardData.layers.getBy('id', vm.activeLayer.id).styles['text-transform'] = vm.activeToolbar === 'capitalize' ? 'uppercase' : 'none';
      }
    }

    /**
     * Description
     * @method closeDialog
     * @return 
     */
    function closeDialog() {
      $mdDialog.hide();
    }

    /**
     * Description
     * @method deleteCard
     * @param {} id
     * @return 
     */
    function deleteCard(id) {
      vm.deleteCardItemId = id;
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/cards/dialogs/delete.html',
        parent: angular.element($document.body),
        clickOutsideToClose: false
      });

    }



    /**
     * Description
     * @method deleteItemsConfirm
     * @return 
     */
    function deleteItemsConfirm(){
      if( vm.deleteCardItemId ){
        CardService.deleteCard(vm.deleteCardItemId).then(function (response) {
          vm.deleteCardItemId = null;
          closeDialog();
          if (response) {
            getCards();
            showMessage('The Card has been deleted.');
          }
        });
      }else{
        showMessage('The Card you want do delete is not found !');
      }
    }

    /**
     * Description
     * @method findFavoriteCardItem
     * @param {} card
     * @return ConditionalExpression
     */
    function findFavoriteCardItem(card){
      var favorites = card.favorite || [];
      return favorites.indexOf($rootScope.user.id) > -1 ? 'icon-heart' : 'icon-heart-outline';
    }


    /**
     * Description
     * @method favoriteCard
     * @param {} card
     * @param {} type
     * @return 
     */
    function favoriteCard(card, type) {
      var id = card.id, message, favorites = card.favorite || [];
      var findIndex = favorites.indexOf($rootScope.user.id);
      if( findIndex > -1 ){
        favorites.splice(findIndex, 1);
        message = 'The selected card removed from favorite.';
      }else{
        favorites.push($rootScope.user.id);
        message = 'The selected card set as your favorite card.';
      }
      CardService.favoriteCard({id: id, favorite: favorites}).then(function (resp) {
        if(resp){
          if(type === 'shared'){
            vm.shareCards.getBy('id', id).favorite = favorites;
          }else{
            vm.allCards.getBy('id', id).favorite = favorites;
          }
          showMessage(message);
        }
      });
    }

    /**
     * Description
     * @method getContacts
     * @return 
     */
    function getContacts() {
      CardService.loadContact().then(function (response) {
        vm.contacts = response;
        vm.filteredContacts = response.map(function (c) {
          var name = c.first_name + ' ' + c.last_name;
          return {
            value: name.toLowerCase(),
            name: name,
            email: c.email,
            photo_url: c.photo_url,
            id: c.id || undefined,
          };
        });
      });
    }

    vm.selectContactForShare = selectContactForShare;
    vm.deleteContactFromShare = deleteContactFromShare;

    /**
     * Description
     * @method selectContactForShare
     * @param {} contact
     * @return 
     */
    function selectContactForShare(contact) {
      vm.selectedContactIdForShare = vm.cardData.userArr;
      if (vm.selectedContactIdForShare.indexOf(contact.id) === -1) {
        vm.cardData.users.push(contact);
        vm.cardData.userArr.push(contact.id);
      }
      vm.searchedContact = null;
    }

    /**
     * Description
     * @method deleteContactFromShare
     * @param {} contact
     * @return 
     */
    function deleteContactFromShare(contact) {
      var userPosition = vm.cardData.userArr.indexOf(contact.id);
      var userDataPos = vm.cardData.users.indexOf(contact);
      if (userPosition > -1) {
        vm.cardData.userArr.splice(userPosition, 1);
        vm.cardData.users.splice(userDataPos, 1);
      }
    }



    /**
     * Description
     * @method getCards
     * @return 
     */
    function getCards() {
      CardService.getCardData().then(function (response) {
        $rootScope.loadingProgress = false;
        vm.loadingCards = false;
        if (response) {
          vm.allCards = response.all;
          vm.shareCards = response.shared;
        }
      });
    }

    /**
     * Description
     * @method getThumbImage
     * @param {} file
     * @return url
     */
    function getThumbImage(file) {
      var url = '';
      if (file && file.thumb_dir) {
        url = vm.site_url + file.thumb_dir;
      } else {
        url = vm.site_url + '/images/icons/album_cover.png';
      }
      return url;
    }

    /**
     * Description
     * @method setCardTextLayer
     * @param {} layer
     * @return 
     */
    function setCardTextLayer(layer){
      angular.element('#cardContent').prepend(' <div class="draggable card_text" id="insertedText'+layer.id+'" style="position: absolute;text-align:left;">\n' +
        '            <input type="text" id="textInput'+ layer.id +'" class="tool txt_input" style="width: 100%;font-size:18px;text-align:left;background: transparent" value="'+layer.value+'" placeholder="Input your text here" />\n' +
        '          </div>');
      var layerSelector = angular.element('#insertedText'+layer.id);
      layerSelector.css(layer.pos);
      angular.element("#textInput"+layer.id).css(layer.style);
      if(layer.style['font-family']) angular.element('head').append(angular.element('<link href="https://fonts.googleapis.com/css?family='+ layer.style['font-family'] +'" rel="stylesheet" type="text/css">'));
      layerSelector.draggable({
        containment: angular.element('#cardContent'),
        /**
         * Description
         * @method drag
         * @return 
         */
        drag:function () {
          vm.cardData.layers.getBy('id', layer.id).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
        }
      }).resizable();
    }

    /**
     * Description
     * @method setCardImageLayer
     * @param {} layer
     * @return 
     */
    function setCardImageLayer(layer){
      angular.element('#cardContent').prepend('<div id="resizableImage' + layer.id + '" class="resizable_image card_image" style="background-image:url(' + layer.src + ');background-size: 100% 100% !important;background-repeat: no-repeat;background-position: center center;position: absolute;width: 200px;height: 200px;">' +
        '<div class="card_image_toolbar no-print">' +
        '<md-icon md-font-icon="icon-rotate-left" class="md-font material-icons icon-rotate-left" aria-label="icon-rotate-left"></md-icon>' +
        '<md-icon md-font-icon="icon-rotate-right" class="md-font material-icons icon-rotate-right" aria-label="icon-rotate-right"></md-icon></div> '
        + '</div>');
      var layerSelector = angular.element('#resizableImage'+layer.id);
      layerSelector.css(layer.pos);
      layerSelector.css(layer.style);
      layerSelector.draggable({
        containment: angular.element('#cardContent'),
        /**
         * Description
         * @method drag
         * @return 
         */
        drag: function(){
          vm.cardData.layers.getBy('id', layer.id).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
          // vm.cardData.layers.getBy('id', vm.insertedTxtId).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
        }
      }).resizable({
        aspectRatio: false
      });
    }

    /**
     * Description
     * @method loadExistingCard
     * @param {} drftCardData
     * @return 
     */
    function loadExistingCard(drftCardData) {
      if (drftCardData.id) {
        $rootScope.loadingProgress = true;
        CardService.loadCard(drftCardData.id).then(function (response) {
          if (response) {
            $rootScope.loadingProgress = false;
            vm.cardData = response;
            vm.cardData.layers.forEach(function (layer) {
              if(layer.type === 'text'){
                setCardTextLayer(layer);
              }else if(layer.type === 'image'){
                setCardImageLayer(layer);
              }
            });
            if (response.draft) {
              vm.currentTabIndex = 1;
            } else {
              vm.currentTabIndex = 3;
            }
          }
        });
      }
    }


    /**
     * Description
     * @method getBackgroundImageAcceptRatio
     * @return 
     */
    function getBackgroundImageAcceptRatio() {
      var cardWidth = angular.element('#cardContent').width();
      var cardHeigth = angular.element('#cardContent').height();
      if (cardWidth && cardHeigth) {
        vm.cardBackgroundImageWidth = cardWidth;
        vm.cardBackgroundImageHeight = cardHeigth;
        vm.cardBackgroundRatio = cardWidth / cardHeigth;
      }
    }

    /**
     * Description
     * @method clearOtherBackground
     * @return 
     */
    function clearOtherBackground() {
      vm.cardData.gradient = false;
    }


    /**
     * Description
     * @method printCard
     * @return 
     */
    function printCard() {
      changeCardViewArea(60);

      var checkTextLayers = vm.cardData.layers.filter(function (layer) {
        return layer.styles && layer.styles['font-family'];
      });

      var getFontFamilies = checkTextLayers.map(function (layer) {
        return layer.styles['font-family'];
      });

      var addFontFamily='';
      getFontFamilies.forEach(function (fontFamily) {
        addFontFamily = addFontFamily + '<link href="https://fonts.googleapis.com/css?family='+ fontFamily +'" rel="stylesheet" type="text/css">';
      });

      $timeout(function () {
        var printContents = angular.element('#cardContent .card___content_area').html();
        var popupWin = window.open('', '_blank');
        popupWin.document.open();
        popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="/styles/print_style.css" />'+addFontFamily+'</head><body onload="window.print();">' + printContents + '</body></html>');
        popupWin.document.close();
      });
      setupLayerToFitSize();
    }

    /**
     * Save new card Infromation
     * @method saveCard
     * @param {} copy
     * @return 
     */
    function saveCard(copy) {
      vm.cardData.styles.width = angular.element('#cardContent .card___content_area').width() + 'px';
      vm.cardData.copy = copy;
      var message;
      generateImage(function (imageDataUrl) {
        uploadCanvas(imageDataUrl, function (uploadedImageData) {
          vm.cardData.file = uploadedImageData.id || '';
          closeDialog();
          CardService.saveCard(vm.cardData).then(function (respCard) {
            $timeout(function () {
              viewMyCards();
              if(vm.cardData.id && !vm.cardData.copy){
                message = 'The card has been updated !';
              }else{
                message = 'The card has been created !';
              }
              showMessage(message);
            });
          });
        });
      });
    }


    /**
     * Description
     * @method uploadCanvas
     * @param {} canvas
     * @param {} callback
     * @return 
     */
    function uploadCanvas(canvas, callback) {
      if (canvas) {
        Upload.upload({
          url: '/cards/saveImage',
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          data: {
            file: Upload.dataUrltoBlob(canvas, vm.cardData.eventTitle + '.png')
          }
        }).then(function (resp) {
          callback(resp.data);
        }, function (resp) {
          // console.log('Error status: ' + resp.status);
        });
      } else {
        callback('');
      }
    }


    /**
     * Description
     * @method setCurrentFont
     * @param {} font
     * @return 
     */
    function setCurrentFont(font) {
      vm.cardData.styles.font = font;
    }

    /**
     * Description
     * @method setCurrentFontSize
     * @param {} fontSize
     * @return 
     */
    function setCurrentFontSize(fontSize) {
      vm.cardData.styles.fontSize = fontSize;
    }


    /**
     * Change font style
     * @return
     * @method changeFontStyleBold
     * @return 
     */
    function changeFontStyleBold() {
      if (vm.cardData.styles.fontWeight === 'bold') {
        vm.cardData.styles.fontWeight = '';
      } else {
        vm.cardData.styles.fontWeight = 'bold';
      }
    }


    /**
     * Description
     * @method generateImage
     * @param {} callback
     * @return 
     */
    function generateImage(callback) {
      vm.activeLayer = null;
      $timeout(function () {
        html2canvas(angular.element('div.card___content_area'), {
          /**
           * Description
           * @method onrendered
           * @param {} canvas
           * @return 
           */
          onrendered: function(canvas) {
            callback(canvas.toDataURL());
          },
          useCORS:true
        });
      }, 10);
    }

    /**
     * Description
     * @return
     * @method changeFontStyleItalic
     * @return 
     */
    function changeFontStyleItalic() {
      if (vm.cardData.styles.fontStyle === 'italic') {
        vm.cardData.styles.fontStyle = '';
      } else {
        vm.cardData.styles.fontStyle = 'italic';
      }
    }

    /**
     * Description
     * @return
     * @method changeFontStyleUnderline
     * @return 
     */
    function changeFontStyleUnderline() {
      if (vm.cardData.styles.fontStyleUnderline === 'underline') {
        vm.cardData.styles.fontStyleUnderline = '';
      } else {
        vm.cardData.styles.fontStyleUnderline = 'underline';
      }
    }


    vm.gradientOptions = [
      {values: "top", field: "Lighter from the top"},
      {values: "right", field: "Lighter from the right"},
      {values: "left", field: "Lighter from the left"},
      {values: "bottom", field: "Lighter from the bottom"},
    ];


    /**
     * Change gradient Color
     * @return
     * @method changeGradientColor
     * @return 
     */
    function changeGradientColor() {
      if (vm.cardData.gradient) {
        vm.cardData.styles.background = '';
        vm.changeBgImage = null;
        angular.element('#cardContent').css({
          'background': '-webkit-linear-gradient(' + vm.cardData.gradientStyle.gradientType + ',' +
          vm.cardData.gradientStyle.colorOne + ',' + vm.cardData.gradientStyle.colorTwo + ')'
        });
        angular.element('#cardContent').css({
          'background': 'linear-gradient(' + vm.cardData.gradientStyle.gradientType + ',' +
          vm.cardData.gradientStyle.colorOne + ',' + vm.cardData.gradientStyle.colorTwo + ')'
        });
        angular.element('#cardContent').css({
          'background': '-moz-linear-gradient(' + vm.cardData.gradientStyle.gradientType + ',' +
          vm.cardData.gradientStyle.colorOne + ',' + vm.cardData.gradientStyle.colorTwo + ')'
        });
      }
    }

    vm.fontSizeSelect = ['10px', '11px', '12px', '13px', '14px', '15px', '16px', '17px', '18px', '19px', '20px', '21px', '22px', '23px', '24px', '25px', '26px'];


    var varTualRange = angular.element('#cardContent');
    // var varTualRange = angular.element('#eventTitle').parent().parent();
    angular.element(document).ready(function () {
      angular.element('#eventTitle').draggable({containment: varTualRange});
      angular.element('#eventDate').draggable({containment: varTualRange});
      angular.element('#eventTime').draggable({containment: varTualRange});
      angular.element('#evrntLocation').draggable({containment: varTualRange});
    });

    /**
     * Description
     * @method changeRadius
     * @return 
     */
    vm.changeRadius = function () {
      angular.element('#cardContent').css({'border-radius': 0 + 'px'});
      angular.element('#cardContent').css({'border-radius': vm.cardData.cornerRadius + 'px'});
    };

    /**
     * Description
     * @method changeBackgroundColor
     * @return 
     */
    vm.changeBackgroundColor = function () {
      vm.cardData.styles.background = '';
      vm.changeBgImage = null;
      // vm.cardData.styles.backgroundColor = vm.cardData.cardBackgroundColor;
      vm.cardData.gradient = false;
    };


    // angular.element("#cardFontSelect").fontselect();

    /**
     * Description
     * @method changeBackGroundImage
     * @param {} ev
     * @return 
     */
    function changeBackGroundImage(ev) {
      vm.newFolderName = null;
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/cards/dialogs/change-bg-image.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method closeDialog
     * @return 
     */
    vm.closeDialog = function () {
      $mdDialog.hide();
    };


    /**
     * Toggle selected status of the contact
     * @method toggleSelectContact
     * @param contact
     * @param event
     * @return 
     */
    function toggleSelectContact(contact, event) {
      if (event) {
        event.stopPropagation();
      }

      if (vm.selectedContacts.indexOf(contact) > -1) {
        vm.selectedContacts.splice(vm.selectedContacts.indexOf(contact), 1);
      }
      else {
        vm.selectedContacts.push(contact);
      }
    }


    /**
     * Description
     * @method showMessage
     * @param {} message
     * @return 
     */
    function showMessage(message) {
      $mdToast.show({
        template: '<md-toast class="md-toast success">' + message + '</md-toast>',
        hideDelay: 3000,
        position: 'bottom left'
      });
    }


    /**
     * Toggle sidenav
     * @method toggleSidenav
     * @param sidenavId
     * @return 
     */
    function toggleSidenav(sidenavId) {
      $mdSidenav(sidenavId).toggle();
    }

    /**
     * Description
     * @method getBy
     * @param {} id
     * @param {} value
     * @return MemberExpression
     */
    Array.prototype.getBy = function (id, value) {
      return this.filter(function (x) {
        return x[id] === value;
      })[0];
    };

  }

})();
