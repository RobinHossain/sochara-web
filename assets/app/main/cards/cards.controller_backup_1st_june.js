(function () {
  'use strict';


  angular
    .module('app.cards')
    .controller('cardsController', CardsController).directive('andyDraggable', function () {
    return {
      restrict: 'A',
      link: function (scope, elm, attrs) {
        var options = scope.$eval(attrs.andyDraggable); //allow options to be passed in
        elm.draggable(options);
      }
    };
  });

  /* @ngInject */
  function CardsController($scope, $mdSidenav, $rootScope, CardService, $mdDialog, $window, $document, moment, $mdToast, Upload, $timeout, $location, $q) {

    var vm = this;
    /////////////

    vm.filterIds = null;
    vm.listType = 'all';
    vm.listOrder = 'name';
    vm.listOrderAsc = false;
    vm.selectedContacts = [];
    vm.newGroupName = '';
    vm.labelCorRadiusClr = '#0098eb';
    vm.labelGradientFirst = '#0098eb';
    vm.labelGradientSecond = '#02c9ce';

    vm.saveCard = saveCard;
    vm.changeBackGroundImage = changeBackGroundImage;
    vm.currentDate = new Date();
    vm.setCurrentFont = setCurrentFont;
    vm.setCurrentFontSize = setCurrentFontSize;
    vm.changeFontStyleBold = changeFontStyleBold;
    vm.changeFontStyleItalic = changeFontStyleItalic;
    vm.changeFontStyleUnderline = changeFontStyleUnderline;
    vm.clearOtherBackground = clearOtherBackground;

    vm.getBackgroundImageAcceptRatio = getBackgroundImageAcceptRatio;
    vm.cardBackgroundRatio = 1;
    vm.site_url = $location.protocol() + '://' + $location.host() + ':' + $location.port();
    vm.getThumbImage = getThumbImage;
    vm.loadExistingCard = loadExistingCard;
    vm.loadExistingDummyCard = loadExistingDummyCard;
    vm.cardSharedUser = [];
    vm.initCardObjects = initCardObjects;
    vm.deleteCard = deleteCard;
    vm.deleteItemsConfirm = deleteItemsConfirm;
    vm.favoriteCard = favoriteCard;
    vm.findFavoriteCardItem = findFavoriteCardItem;
    vm.changeCardBackground = changeCardBackground;
    vm.viewMyCards = viewMyCards;
    vm.viewTemplates = viewTemplates;
    vm.showElements = showElements;
    vm.showTexFormats = showTexFormats;
    vm.showBackgroundOptions = showBackgroundOptions;


    // Methods
    vm.toggleSelectContact = toggleSelectContact;
    vm.addNewGroup = addNewGroup;
    vm.toggleSidenav = toggleSidenav;
    vm.generateImage = generateImage;
    vm.changeGradientColor = changeGradientColor;
    vm.printCard = printCard;
    vm.closeDialog = closeDialog;
    vm.insertText = insertText;
    vm.insertImage = insertImage;
    vm.insertDate = insertDate;
    vm.insertTime = insertTime;
    vm.removeLayer = removeLayer;
    vm.changeCardFontSize = changeCardFontSize;
    vm.changeCardTextStyleBold = changeCardTextStyleBold;
    vm.changeCardTextStyleItalic = changeCardTextStyleItalic;
    vm.changeCardTextStyleUnderline = changeCardTextStyleUnderline;
    vm.changeCardTextAlign = changeCardTextAlign;
    vm.changeSelectedTextColor = changeSelectedTextColor;
    vm.getDummyCards = getDummyCards;
    vm.cardArrays = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25];
    vm.cardReady = cardReady;
    vm.changeCardBackgroundImage = changeCardBackgroundImage;
    vm.increaseCardArea = increaseCardArea;
    vm.decreaseCardArea = decreaseCardArea;
    vm.changeCardViewArea = changeCardViewArea;


    vm.contacts = [];


    // vm.cardData.layerPosition.eventTitleLeft = 0;
    // vm.cardData.layerPosition.eventTitleTop = 10;
    // vm.cardData.layerPosition.eventDateLeft = 0;
    // vm.cardData.layerPosition.eventDateTop = 40;
    // vm.cardData.layerPosition.eventTimeLeft = 0;
    // vm.cardData.layerPosition.eventTimeTop = 70;
    // vm.cardData.layerPosition.evrntLocationLeft = 0;
    // vm.cardData.layerPosition.evrntLocationTop = 100;


    init();

    function init() {
      $rootScope.searchable = false;
      initCardObjects();
      vm.loadingCards = true;
      $rootScope.loadingProgress = true;
      vm.sideCurrentTab = 'cards';
      $timeout( function(){
        getCards();
        getContacts();
        // getSharedCards();
        // getDummyCards();
      }, 2000 );



    }

    function changeCardBackground(file){
      vm.cardBgImage = file;
      if(file){
        Upload.imageDimensions(file).then(function(dimensions){
          vm.cardData.styles.width = dimensions.width;
          vm.cardData.styles.height = dimensions.height;
          console.log(dimensions.width, dimensions.height);
        });
      }
      console.log(file);
    }

    function viewMyCards(){
      vm.sideCurrentTab = 'cards';
      console.log(vm.sideCurrentTab);
    }

    function viewTemplates(){
      vm.sideCurrentTab = 'templates';
    }

    function showElements(){
      vm.sideCurrentTab = 'elements';
    }

    function showTexFormats(){
      vm.sideCurrentTab = 'text';
    }

    function showBackgroundOptions(){
      vm.sideCurrentTab = 'background_options';
    }

    function initCardObjects(init) {
      vm.cardData = {layers: []};
      vm.cardData.styles = {width: '339px', height: '226px'};
      vm.cardData.percent = 20;
      vm.cardData.temp = {styles: {top: '0px', left: '0px'}};
      // vm.cardData.styles.font = 'Open Sans';
      // vm.cardData.styles.fontSize = '20px';
      // vm.cardData.styles.fontWeight = '';
      // vm.cardData.styles.backgroundColor = '';
      // vm.cardData.styles.color = 'red';
      // vm.cardData.styles.fontStyle = '';
      // vm.cardData.styles.fontStyleUnderline = '';
      // vm.cardData.styles.rotate = 0;

      // Gradient Styles
      vm.cardData.gradient = false;
      vm.cardData.gradientStyle = {};
      vm.cardData.gradientStyle.gradientType = 'right';
      vm.cardData.gradientStyle.colorOne = '#56CCF2';
      vm.cardData.gradientStyle.colorTwo = '#2F80ED';

      vm.cardData.layerPosition = {};
      vm.cardData.eventDate = new Date();
      vm.cardData.eventStartTime = new Date();
      vm.cardData.eventEndTime = new Date();
      vm.cardData.userArr = [];


      if (init) {
        vm.changeBgImage = null;
        vm.currentTabIndex = 1;
      }

      vm.selectedCardBgImage = 1;
      // vm.cardData.styles.background = vm.site_url + '/images/cards/main/1.png';
      vm.cardViewPercent = 30;

      vm.cardData.layers = [
        {id: 'layer1', type: 'fill', value: 'http://localhost:1337/images/cards/main/2.png', class: 'fill', index: 1, styles: {'width': '338px', 'height': '225px'}},
        {id: 'layer2', type: 'text', value: 'Robin Hossain', class: 'name', index: 2, styles: {'transform': 'translate(37px, 35px)', 'width': '145px', 'height': '45px', 'font-family': 'Annie Use Your Telescope', 'font-size': '90%'}},
        {id: 'layer3', type: 'text', value: 'age', class: 'age', index: 2, styles: {'transform': 'translate(175px, 62px)', 'width': '145px', 'height': '45px', 'font-family': 'Annie Use Your Telescope', 'font-size': '100%'}},
        {id: 'layer4', type: 'image', value: 'http://localhost:1337/uploads/files/59db22471864af49d2eebce1/file/NafSadh_Profile_1527671870027.jpg', class: 'image', index: 3, styles: {'transform': 'translate(78px, 100px)', 'width': '122px', 'height': '45px', 'font-family': 'Annie Use Your Telescope', 'font-size': '100%'}},
        {id: 'layer5', type: 'text', value: 'date', class: 'date', index: 4, styles: {'transform': 'translate(251px, 85px)', 'width': '145px', 'height': '45px', 'font-family': 'Annie Use Your Telescope', 'font-size': '75%'}},
        {id: 'layer6', type: 'text', value: 'time', class: 'time', index: 5, styles: {'transform': 'translate(251px, 105px)', 'width': '145px', 'height': '45px', 'font-family': 'Annie Use Your Telescope', 'font-size': '75%'}},
        {id: 'layer7', type: 'text', value: 'address line', class: 'address', index: 2, styles: {'transform': 'translate(232px, 142px)', 'width': '145px', 'height': '45px', 'font-family': 'Annie Use Your Telescope', 'font-size': '75%'}},
        {id: 'layer8', type: 'text', value: 'address line 2', class: 'address', index: 7, styles: {'transform': 'translate(231px, 164px)', 'width': '145px', 'height': '45px', 'font-family': 'Annie Use Your Telescope', 'font-size': '75%'}},
        {id: 'layer9', type: 'text', value: 'other info', class: 'info', index: 8, styles: {'transform': 'translate(231px, 190px)', 'width': '145px', 'height': '45px', 'font-family': 'Annie Use Your Telescope', 'font-size': '75%'}},
      ];
      clearLayersIfHave();
      setupLayerToFitSize();
    }

    vm.cardTemplates = [

    ];

    function clearLayersIfHave(){
      // angular.element('#cardContent .card___content_layer').delete();
    }

    function setupLayerToFitSize(){
      $timeout(function () {
        changeCardViewArea(33);
      });
    }

    function cardReady(){
      loadFontSelector();
      (function(){
        angular.element('#cardContent').on('click change mouseup', '.card_text, .card_image', function (event) {
          if(event.type === 'click' && angular.element(event.target).hasClass('icon-rotate-left')){
            vm.selectedInputTxtId = angular.element(event.target).parent().parent().attr('id');
            if(vm.selectedInputTxtId){
              vm.cardData.styles.rotate = vm.cardData.styles.rotate - 1;
              changeRotationOfId(parseInt(vm.selectedInputTxtId.replace(/\D/g,'')));
            }
          }else if(event.type === 'click' && angular.element(event.target).hasClass('icon-rotate-right')){
            vm.selectedInputTxtId = angular.element(event.target).parent().parent().attr('id');
            if(vm.selectedInputTxtId){
              vm.cardData.styles.rotate = vm.cardData.styles.rotate + 1;
              changeRotationOfId(parseInt(vm.selectedInputTxtId.replace(/\D/g,'')));
            }
          }else if(event.type === 'change' && angular.element(event.target).hasClass('txt_input')){
            vm.selectedInputTxtId = event.target.id;
            var currentInputId = parseInt(vm.selectedInputTxtId.replace(/\D/g,''));
            var currentInput = angular.element(event.target).val();
            vm.cardData.layers.getBy('id', currentInputId)['value'] = currentInput;
            angular.element(event.target).attr("value", currentInput);
          }else{
            vm.selectedInputTxtId = event.target.id;
          }
          // resetToolbarItem();
        });

        // angular.element('#cardContent').on('click', '.card_image .card_image_toolbar .icon-rotate-left', function (event) {
        //   var selectedInputId = checkSelectedInput();
        //   if(selectedInputId){
        //     vm.cardData.styles.rotate -= 5;
        //     changeRotationOfId(selectedInputId);
        //     console.log('yes');
        //   }
        // });
        //
        // angular.element('#cardContent').on('click', '.card_image .card_image_toolbar .icon-rotate-right', function (event) {
        //   var selectedInputId = checkSelectedInput();
        //   if(selectedInputId){
        //     vm.cardData.styles.rotate += 5;
        //     changeRotationOfId(selectedInputId);
        //     console.log('yes');
        //   }
        // });

        angular.element("#cardFontFamilies").change(function () {
          var font = angular.element(this).val().replace(/\+/g, ' ');
          var selectedInputId = checkSelectedInput();
          if(selectedInputId){
            angular.element('#textInput'+selectedInputId).css('font-family', font);
            vm.cardData.layers.getBy('id', selectedInputId).style['font-family'] = font;
          }
        });
      })();
    }

    function changeRotationOfId( selectedInputId ){
      vm.cardData.layers.getBy('id', selectedInputId).style['transform'] = 'rotate('+ vm.cardData.styles.rotate +'deg)';
      vm.cardData.layers.getBy('id', selectedInputId).style['-webkit-transform'] = 'rotate('+ vm.cardData.styles.rotate +'deg)';
      vm.cardData.layers.getBy('id', selectedInputId).style['-moz-transform'] = 'rotate('+ vm.cardData.styles.rotate +'deg)';
      vm.cardData.layers.getBy('id', selectedInputId).style['-ms-transform'] = 'rotate('+ vm.cardData.styles.rotate +'deg)';
      angular.element('#resizableImage'+selectedInputId).css({'-webkit-transform' : 'rotate('+ vm.cardData.styles.rotate +'deg)',
        '-moz-transform' : 'rotate('+ vm.cardData.styles.rotate +'deg)',
        '-ms-transform' : 'rotate('+ vm.cardData.styles.rotate +'deg)',
        'transform' : 'rotate('+ vm.cardData.styles.rotate +'deg)'});
    }

    function increaseCardArea(){
      var size = vm.cardViewPercent + 10;
      if( size > 100){
        changeCardViewArea(100);
      }else{
        changeCardViewArea(size);
      }
    }

    function changeWidthHeight(layer, index, size){
      var lw = layer.styles && layer.styles['width'] ? layer.styles['width'] : '';
      lw = parseFloat(lw.replace('px', ''));
      lw = lw ? ((lw*size)/vm.cardData.percent)+'px' : '';
      vm.cardData.layers[index]['styles']['width'] = lw;

      var lh = layer.styles && layer.styles['height'] ? layer.styles['height'] : '';
      lh = parseFloat(lh.replace('px', ''));
      lh = lh ? ((lh*size)/vm.cardData.percent)+'px' : '';
      vm.cardData.layers[index]['styles']['height'] = lh;
    }

    function changeCardTransform(layer, index, size){
      var tr = layer['styles'] && layer['styles']['transform'] ? layer['styles']['transform']:'';
      if(tr){
        var wh = /\(([^)]+)\)/.exec(tr)[1];
        var w = parseFloat(wh.split(',')[0].replace(' ',''));
        var h = parseFloat(wh.split(',')[1].replace(' ',''));
        var tw = w ? ((w*size)/vm.cardData.percent)+'px' : '';
        var th = h ? ((h*size)/vm.cardData.percent)+'px' : '';
        vm.cardData.layers[index]['styles']['transform'] = 'translate('+tw+','+th+')';
      }
    }

    function decreaseCardArea(){
      var size = vm.cardViewPercent - 10;
      if( size < 20){
        changeCardViewArea(20);
      }else{
        changeCardViewArea(size);
      }
    }

    function changeCardViewArea(size){
      vm.cardData.layers.forEach(function (layer, index) {
        changeCardTransform(layer, index, size);
        changefontSize(layer, index, size);
        changeWidthHeight(layer, index, size);
      });
      changeCardLayoutWidth(size);
      vm.cardData.percent = size;
      vm.cardViewPercent = size;
      $timeout(function () {
        setCardAreaToMiddle();
      });
    }

    function setCardAreaToMiddle(){
      var cw = angular.element("#cardContent").width();
      var ch = angular.element("#cardContent").height();
      var ccw = angular.element('.card___content_wrap').width();
      var cch = angular.element('.card___content_wrap').height();
      if(cw>ccw){
        var ccdw = Math.floor((cw/2 - ccw/2));
        vm.cardData.temp.styles['left'] = ccdw + 'px';

        var ccdh = Math.floor((ch/2 - (cch+50)/2));
        vm.cardData.temp.styles['top'] = ccdh + 'px';
        // console.log(ccd);
      }else{
        vm.cardData.temp.styles['left'] = '0px';
        vm.cardData.temp.styles['top'] = '0px';
      }
    }

    function changeCardLayoutWidth(size){
      var cw = parseFloat(vm.cardData.styles['width'].replace('px', ''));
      vm.cardData.styles['width'] = ((cw*size)/vm.cardData.percent)+'px';

      var ch = parseFloat(vm.cardData.styles['height'].replace('px', ''));
      vm.cardData.styles['height'] = ((ch*size)/vm.cardData.percent)+'px';
    }

    function changefontSize(layer, index, size){
      var cfs = layer['styles'] && layer['styles']['font-size'] ? layer['styles']['font-size'] : '';
      cfs = parseFloat(cfs.replace('%', ''));
      cfs = cfs ? ((cfs*size)/vm.cardData.percent)+'%' : '';
      if(cfs){
        vm.cardData.layers[index]['styles']['font-size'] = cfs;
      }
    }

    function changeCardBackgroundImage(index){
      vm.selectedCardBgImage = index;
      vm.changeBgImage = null;
      vm.cardData.styles.background = vm.site_url + '/images/cards/main/' + index + '.png';
    }

    function loadFontSelector(){
      if(!angular.element("input#cardFontFamilies ~ div.font-select").length){
        angular.element("#cardFontFamilies").fontselect({lookahead: 20});
      }
    }


    function checkSelectedInput(){
      var input = vm.selectedInputTxtId || '';
      var withNoDigits = input.replace(/[0-9]/g, '');
      return withNoDigits === 'textInput' || withNoDigits === 'insertedText' ? parseInt(input.replace(/\D/g,'')) : 0;
    }

    function insertText(param){
      var textToInsert = '';
      if(param && param === 'title'){
        textToInsert = vm.cardData.eventTitle || '';
      }else if(param && param === 'address'){
        textToInsert = vm.cardData.locationName || '' + ', ' + vm.cardData.city || '' + ', ' + vm.cardData.state || '' + ', ' + vm.cardData.zip || '';
      }else if(param && param === 'date'){
        textToInsert = moment(vm.cardData.eventDate, 'M/D/YYYY').format('dddd, D MMMM YYYY') || '';
      }else if(param && param === 'time'){
        textToInsert = moment(vm.cardData.eventStartTime).format('h:mm') + ' - ' + moment(vm.cardData.eventEndTime).format('h:mm');
      }
      if(vm.cardData.id){
        vm.insertedTxtId = vm.cardData.layers.length;
      }
      vm.insertedTxtId = vm.insertedTxtId ? ++vm.insertedTxtId : 1;
      vm.cardData.layers.push({type: 'text', id: vm.insertedTxtId, value: '', pos: {}, style: {}});
      angular.element('#cardContent').prepend(' <div class="draggable card_text" id="insertedText'+vm.insertedTxtId+'" style="position: absolute;text-align:left;">\n' +
        '            <input type="text" id="textInput'+ vm.insertedTxtId +'" class="tool txt_input" style="width: 100%;font-size:18px;text-align:left;background: transparent" value="'+textToInsert+'" placeholder="Input your text here" />\n' +
        '          </div>');
      angular.element('#insertedText'+vm.insertedTxtId).draggable({
        containment: angular.element('#cardContent'),
        drag:function () {
          vm.cardData.layers.getBy('id', vm.insertedTxtId).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
        }
      }).resizable();
    }

    function insertImage(file){
      Upload.dataUrl(file, true).then(function(urlSrc){
        if(urlSrc){
          vm.insertedTxtId = vm.insertedTxtId ? ++vm.insertedTxtId : 1;
          vm.cardData.layers.push({id: vm.insertedTxtId, type: 'image', src: urlSrc, pos: {}, style: {}});
          angular.element('#cardContent').prepend('<div id="resizableImage' + vm.insertedTxtId + '" class="resizable_image card_image" style="background-image:url(' + urlSrc + ');background-size: 100% 100% !important;background-repeat: no-repeat;background-position: center center;position: absolute;width: 200px;height: 200px;">' +
            '<div class="card_image_toolbar no-print">' +
            '<md-icon md-font-icon="icon-rotate-left" class="md-font material-icons icon-rotate-left" aria-label="icon-rotate-left"></md-icon>' +
            '<md-icon md-font-icon="icon-rotate-right" class="md-font material-icons icon-rotate-right" aria-label="icon-rotate-right"></md-icon></div> '
            + '</div>');
          angular.element("#resizableImage"+vm.insertedTxtId).draggable({
            containment: angular.element('#cardContent'),
            drag: function(){
              vm.cardData.layers.getBy('id', vm.insertedTxtId).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
              // vm.cardData.layers.getBy('id', vm.insertedTxtId).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
            }
          }).resizable({
            aspectRatio: false
          });
        }
      });

    }

    document.onkeydown = onKeyDownFunc;
    function onKeyDownFunc(e){
      if(e.ctrlKey && e.keyCode == 'P'.charCodeAt(0)){
        e.preventDefault();
        e.stopPropagation();
        printCard();
        //your saving code
      }
    }

    function insertDate(){

    }

    function insertTime(){

    }

    function removeLayer(){
      if(vm.selectedInputTxtId && vm.selectedInputTxtId.length){
        var withNoDigits = vm.selectedInputTxtId.replace(/[0-9]/g, ''),
          childElem = angular.element('#'+vm.selectedInputTxtId), parentElement = angular.element('#'+vm.selectedInputTxtId).parent();
        if(withNoDigits === 'textInput' && parentElement.length ){
          parentElement.remove();
        }else if((withNoDigits === 'insertedText' || withNoDigits === 'resizableImage') && childElem.length ){
          childElem.remove();
        }
      }
    }

    function changeCardFontSize(size){
      var selectedInputId = checkSelectedInput();
      if(selectedInputId){
        angular.element('#textInput'+selectedInputId).css('font-size', size);
        vm.cardData.layers.getBy('id', selectedInputId).style['font-size'] = size;
      }
    }

    function changeCardTextStyleBold(bold){
      var selectedInputId = checkSelectedInput();
      if(selectedInputId){
        if(bold){
          angular.element('#textInput'+selectedInputId).css('font-weight', 'bold');
          vm.cardData.layers.getBy('id', selectedInputId).style['font-weight'] = 'bold';
        }else{
          angular.element('#textInput'+selectedInputId).css('font-weight', 'normal');
          vm.cardData.layers.getBy('id', selectedInputId).style['font-weight'] = 'normal';
        }
      }
    }

    function changeCardTextStyleItalic(italic){
      var selectedInputId = checkSelectedInput();
      if(selectedInputId){
        if(italic){
          angular.element('#textInput'+selectedInputId).css('font-style', 'italic');
          vm.cardData.layers.getBy('id', selectedInputId).style['font-style'] = 'italic';
        }else{
          angular.element('#textInput'+selectedInputId).css('font-style', 'normal');
          vm.cardData.layers.getBy('id', selectedInputId).style['font-style'] = 'normal';
        }
      }
    }

    function changeCardTextStyleUnderline(underline){
      var selectedInputId = checkSelectedInput();
      if(selectedInputId){
        if(underline){
          angular.element('#textInput'+selectedInputId).css('text-decoration', 'underline');
          vm.cardData.layers.getBy('id', selectedInputId).style['text-decoration'] = 'underline';
        }else{
          angular.element('#textInput'+selectedInputId).css('text-decoration', 'none');
          vm.cardData.layers.getBy('id', selectedInputId).style['text-decoration'] = 'none';
        }
      }
    }

    function changeSelectedTextColor(color){
      var selectedInputId = checkSelectedInput();
      if(selectedInputId){
        angular.element('#textInput'+selectedInputId).css('color', color);
        vm.cardData.layers.getBy('id', selectedInputId).style['color'] = color;
      }
    }

    function changeCardTextAlign(align){
      var selectedInputId = checkSelectedInput();
      if(selectedInputId){
        angular.element('#textInput'+selectedInputId).css('text-align', align);
        vm.cardData.layers.getBy('id', selectedInputId).style['text-align'] = align;
      }
    }

    function closeDialog() {
      $mdDialog.hide();
    }

    function deleteCard(card) {
      vm.deleteCardItemId = card;
      $mdDialog.show({
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/cards/dialogs/delete.html',
        parent: angular.element($document.body),
        clickOutsideToClose: false
      });

    }



    function deleteItemsConfirm(){
      if( vm.deleteCardItemId ){
        CardService.deleteCard(vm.deleteCardItemId).then(function (response) {
          vm.deleteCardItemId = null;
          closeDialog();
          if (response) {
            getCards();
            showMessage('The Card has been deleted.');
          }
        });
      }else{
        showMessage('The Card you want do delete is not found !');
      }
    }

    function findFavoriteCardItem(card){
      var favorites = card.favorite || [];
      return favorites.indexOf($rootScope.user.id) > -1 ? 'icon-heart' : 'icon-heart-outline';
    }


    function favoriteCard(card, type) {
      var id = card.id, message, favorites = card.favorite || [];
      var findIndex = favorites.indexOf($rootScope.user.id);
      if( findIndex > -1 ){
        favorites.splice(findIndex, 1);
        message = 'The selected card removed from favorite.';
      }else{
        favorites.push($rootScope.user.id);
        message = 'The selected card set as your favorite card.';
      }
      CardService.favoriteCard({id: id, favorite: favorites}).then(function (resp) {
        if(resp){
          if(type === 'shared'){
            vm.shareCards.getBy('id', id).favorite = favorites;
          }else{
            vm.allCards.getBy('id', id).favorite = favorites;
          }
          showMessage(message);
        }
      });
    }

    function getContacts() {
      CardService.loadContact().then(function (response) {
        vm.contacts = response;
      });
    }

    vm.selectContactForShare = selectContactForShare;
    vm.deleteContactFromShare = deleteContactFromShare;

    function selectContactForShare(contact) {
      // serializeSharedUser(vm.cardSharedUser, function (returnUsers) {
      //   vm.selectedContactIdForShare = returnUsers;
      //   if (vm.selectedContactIdForShare.indexOf(contact.id) === -1) {
      //     vm.cardSharedUser.push(contact);
      //     vm.selectedContactIdForShare.push(contact.id);
      //   }
      // });

      var checkExist = vm.cardData.userArr.filter(function (usr) {
        return usr.id === contact.id;
      });
      if(!checkExist.length){
        vm.cardData.userArr.push(contact.id);
        vm.cardData.users.push(contact);
      }
      vm.searchedContact = null;
    }

    function deleteContactFromShare(contact) {
      // serializeSharedUser(vm.cardSharedUser, function (returnUsers) {
      //   vm.selectedContactIdForShare = returnUsers;
      //   var userPosition = vm.selectedContactIdForShare.indexOf(contact.id);
      //   var userPositionIndex = vm.cardSharedUser.indexOf(contact);
      //   if (userPosition > -1) {
      //     vm.selectedContactIdForShare.splice(userPosition, 1);
      //     vm.cardSharedUser.splice(userPositionIndex, 1);
      //   }
      // });
      var userPosition = vm.cardData.userArr.indexOf(contact.id);
      var userDataPos = vm.cardData.users.indexOf(contact);
      if (userPosition > -1) {
        vm.cardData.userArr.splice(userPosition, 1);
        vm.cardData.users.splice(userDataPos, 1);
      }
    }

    function serializeSharedUser(users, callback) {
      var userData = [];
      if (users && users.length) {
        users.forEach(function (user) {
          userData.push(user.id);
        });
      }
      callback(userData);
    }


    function getCards() {
      CardService.getCardData().then(function (response) {
        $rootScope.loadingProgress = false;
        vm.loadingCards = false;
        if (response) {
          vm.allCards = response.all;
          vm.shareCards = response.shared;
        }
      });
    }



    function getDummyCards(num) {
      vm.dummyCards = new Array(num);
      console.log(vm.dummyCards);
      // CardService.getDummyCards().then(function (response) {
      //   console.log(response);
      //   if (response) {
      //     vm.dummyCards = response;
      //   }
      // });
    }

    function getSharedCards() {
      CardService.getSharedCardData().then(function (response) {
        if (response) {
          vm.shareCards = response;
        }
      });
    }

    function getThumbImage(file) {
      var url = '';
      if (file && file.thumb_dir) {
        url = vm.site_url + file.thumb_dir;
      } else {
        url = vm.site_url + '/images/icons/album_cover.png';
      }
      return url;
    }

    function setCardTextLayer(layer){
      angular.element('#cardContent').prepend(' <div class="draggable card_text" id="insertedText'+layer.id+'" style="position: absolute;text-align:left;">\n' +
        '            <input type="text" id="textInput'+ layer.id +'" class="tool txt_input" style="width: 100%;font-size:18px;text-align:left;background: transparent" value="'+layer.value+'" placeholder="Input your text here" />\n' +
        '          </div>');
      var layerSelector = angular.element('#insertedText'+layer.id);
      layerSelector.css(layer.pos);
      angular.element("#textInput"+layer.id).css(layer.style);
      if(layer.style['font-family']) angular.element('head').append(angular.element('<link href="https://fonts.googleapis.com/css?family='+ layer.style['font-family'] +'" rel="stylesheet" type="text/css">'));
      layerSelector.draggable({
        containment: angular.element('#cardContent'),
        drag:function () {
          vm.cardData.layers.getBy('id', layer.id).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
        }
      }).resizable();
    }

    function setCardImageLayer(layer){
      angular.element('#cardContent').prepend('<div id="resizableImage' + layer.id + '" class="resizable_image card_image" style="background-image:url(' + layer.src + ');background-size: 100% 100% !important;background-repeat: no-repeat;background-position: center center;position: absolute;width: 200px;height: 200px;">' +
        '<div class="card_image_toolbar no-print">' +
        '<md-icon md-font-icon="icon-rotate-left" class="md-font material-icons icon-rotate-left" aria-label="icon-rotate-left"></md-icon>' +
        '<md-icon md-font-icon="icon-rotate-right" class="md-font material-icons icon-rotate-right" aria-label="icon-rotate-right"></md-icon></div> '
        + '</div>');
      var layerSelector = angular.element('#resizableImage'+layer.id);
      layerSelector.css(layer.pos);
      layerSelector.css(layer.style);
      layerSelector.draggable({
        containment: angular.element('#cardContent'),
        drag: function(){
          vm.cardData.layers.getBy('id', layer.id).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
          // vm.cardData.layers.getBy('id', vm.insertedTxtId).pos = {left: angular.element(this).css('left'), top: angular.element(this).css('top')};
        }
      }).resizable({
        aspectRatio: false
      });
    }

    function loadExistingCard(drftCardData) {
      clearLayersIfHave();
      if (drftCardData.id) {
        $rootScope.loadingProgress = true;
        CardService.loadCard(drftCardData.id).then(function (response) {
          if (response) {
            $rootScope.loadingProgress = false;
            vm.cardData = response;
            vm.cardData.layers.forEach(function (layer) {
              if(layer.type === 'text'){
                setCardTextLayer(layer);
              }else if(layer.type === 'image'){
                setCardImageLayer(layer);
              }
            });
            if (response.draft) {
              vm.currentTabIndex = 1;
            } else {
              vm.currentTabIndex = 3;
            }
          }
        });
      }
    }

    function loadExistingDummyCard(drftCardData) {
      if (drftCardData && drftCardData.id) {
        $rootScope.loadingProgress = true;
        CardService.loadDummyCard(drftCardData.id).then(function (response) {
          $rootScope.loadingProgress = false;
          if (response) {
            if (response.background) {
              vm.cardData.styles.background = vm.site_url + '/images/cards/background/' + response.id + '.png';
              vm.changeBgImage = vm.site_url + '/images/cards/background/' + response.id + '.png';
              // vm.changeBgImage = vm.site_url + response.background.file_dir;
            } else if (response.styles.backgroundImg) {
              vm.cardData.styles.background = response.styles.backgroundImg;
            } else {
              vm.cardData.styles.background = '';
            }
            if (response.gradient) {
              vm.cardData.gradient = true;
              vm.cardData.gradientStyle = response.styles.gradient;
              changeGradientColor();
            }
            vm.cardData.styles = response.styles;
            vm.cardData.styles.backgroundColor = response.styles.backgroundColor;
            vm.cardData = response;
            vm.cardData.id = null;
            vm.cardData.copy = false;
            vm.cardSharedUser = response.users || [];
            vm.cardData.eventDate = new Date(response.eventDate);
            vm.cardData.eventStartTime = new Date(response.eventStartTime);
            vm.cardData.eventEndTime = new Date(response.eventEndTime);
            if (vm.cardData.layerPosition.eventTitleLeft) {
              angular.element('#eventTitle').css('left', vm.cardData.layerPosition.eventTitleLeft);
            }
            if (vm.cardData.layerPosition.eventTitleTop) {
              angular.element('#eventTitle').css('top', vm.cardData.layerPosition.eventTitleTop);
            }
            if (vm.cardData.layerPosition.eventDateLeft) {
              angular.element('#eventDate').css('left', vm.cardData.layerPosition.eventDateLeft);
            }
            if (vm.cardData.layerPosition.eventDateTop) {
              angular.element('#eventDate').css('top', vm.cardData.layerPosition.eventDateTop);
            }
            if (vm.cardData.layerPosition.eventTimeLeft) {
              angular.element('#eventTime').css('left', vm.cardData.layerPosition.eventTimeLeft);
            }
            if (vm.cardData.layerPosition.eventTimeTop) {
              angular.element('#eventTime').css('top', vm.cardData.layerPosition.eventTimeTop);
            }
            if (vm.cardData.layerPosition.evrntLocationLeft) {
              angular.element('#evrntLocation').css('left', vm.cardData.layerPosition.evrntLocationLeft);
            }
            if (vm.cardData.layerPosition.evrntLocationTop) {
              angular.element('#evrntLocation').css('top', vm.cardData.layerPosition.evrntLocationTop);
            }
            if (response.draft) {
              vm.currentTabIndex = 1;
            } else {
              vm.currentTabIndex = 3;
            }
          }
        });
      }
    }

    function getBackgroundImageAcceptRatio() {
      var cardWidth = angular.element('#cardContent').width();
      var cardHeigth = angular.element('#cardContent').height();
      if (cardWidth && cardHeigth) {
        vm.cardBackgroundImageWidth = cardWidth;
        vm.cardBackgroundImageHeight = cardHeigth;
        vm.cardBackgroundRatio = cardWidth / cardHeigth;
      }
    }

    function clearOtherBackground() {
      vm.cardData.gradient = false;
    }

    function convertCanvasToImage(callback) {
      var cardContentArea = angular.element('#cardContent');
      html2canvas(cardContentArea, {
        onrendered: function (canvas) {
          var image = new Image();
          image.src = canvas.toDataURL("image/png");
          callback(image);
        }
      });
    }

    function printCard() {


      var checkTextLayers = vm.cardData.layers.filter(function (layer) {
        return layer.type === 'text' && layer.style && layer.style['font-family'];
      });

      var getFontFamilies = checkTextLayers.map(function (layer) {
        return layer.style['font-family'];
      });

      var addFontFamily='';
      getFontFamilies.forEach(function (fontFamily) {
        addFontFamily = addFontFamily + '<link href="https://fonts.googleapis.com/css?family='+ fontFamily +'" rel="stylesheet" type="text/css">';
      });
      var printContents = angular.element('div.card-preview').html();
      var popupWin = window.open('', '_blank');
      popupWin.document.open();
      popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="/styles/print_style.css" />'+addFontFamily+'</head><body onload="window.print();">' + printContents + '</body></html>');
      popupWin.document.close();


      // convertCanvasToImage(function (imageDataUrl) {
      //   // console.log(imageDataUrl);
      //   printDataImage(imageDataUrl);
      // });
    }

    // function printDataImage( imageDataUrl ){
    //   var myWindow=window.open('','');
    //   myWindow.document.write("<p>" + imageDataUrl + "</p>");
    //   myWindow.document.close();
    //   myWindow.focus();
    //   myWindow.print();
    //   myWindow.close();
    //   return true;
    // }

    /*
    * Save new card Infromation
    *
    */
    function saveCard(copy) {
      vm.cardData.styles.width = angular.element('#cardContent').width();
      vm.cardData.copy = copy;
      var message;
      console.log(vm.cardData);
      generateImage(function (imageDataUrl) {
        uploadCanvas(imageDataUrl, function (uploadedImageData) {
          vm.cardData.file = uploadedImageData.id || '';
          CardService.saveCard(vm.cardData).then(function (respCard) {
            $timeout(function () {
              vm.currentTabIndex = 0;
              getCards();
              if(vm.cardData.id && !vm.cardData.copy){
                message = 'The card has been updated !';
              }else{
                message = 'The card has been created !';
              }
              showMessage(message);
              console.log(respCard);
            });
          });
        });
      });

      // $rootScope.loadingProgress = true;
      // var layerPosition = {};
      // layerPosition.eventTitleLeft = parseInt(angular.element('#eventTitle').css('left'));
      // layerPosition.eventTitleTop = parseInt(angular.element('#eventTitle').css('top'));
      // layerPosition.eventDateLeft = parseInt(angular.element('#eventDate').css('left'));
      // layerPosition.eventDateTop = parseInt(angular.element('#eventDate').css('top'));
      // layerPosition.eventTimeLeft = parseInt(angular.element('#eventTime').css('left'));
      // layerPosition.eventTimeTop = parseInt(angular.element('#eventTime').css('top'));
      // layerPosition.evrntLocationLeft = parseInt(angular.element('#evrntLocation').css('left'));
      // layerPosition.evrntLocationTop = parseInt(angular.element('#evrntLocation').css('top'));
      // uploadImage(vm.changeBgImage, function (imageId) {
      //   // console.log(imageId);
      //   // if (imageId) {
      //   generateImage(function (imageDataUrl) {
      //     if (!vm.cardData.eventTitle) {
      //       showMessage('Please Input Event Title');
      //     } else {
      //       uploadCanvas(imageDataUrl, function (canvasId) {
      //         var cardDatas = vm.cardData;
      //         if (imageId) {
      //           cardDatas.background = imageId;
      //         } else {
      //           cardDatas.styles.backgroundImg = vm.changeBgImageCropped || '';
      //         }
      //         if (vm.cardData.gradient) {
      //           cardDatas.gradient = true;
      //           cardDatas.styles.gradient = vm.cardData.gradientStyle;
      //         }
      //         cardDatas.file = canvasId;
      //         cardDatas.users = vm.selectedContactIdForShare || [];
      //         // if (!copy || !!copy === 'yes') {
      //         //   cardDatas.draftId = vm.activeDraftCard || '';
      //         //   cardDatas.draft = true;
      //         // }
      //
      //         if( !copy ){
      //           cardDatas.draftId = vm.activeDraftCard || '';
      //           cardDatas.draft = true;
      //         }else if( copy && copy === 'yes' ){
      //           cardDatas.id = null;
      //           cardDatas.draft = false;
      //           cardDatas.draftId = '';
      //         }
      //
      //
      //         cardDatas.layerPosition = layerPosition;
      //         CardService.saveCard(cardDatas).then(function (response) {
      //           if (response) {
      //             $timeout(function () {
      //               vm.currentTabIndex = 0;
      //               showMessage('The card has been saved');
      //               getCards();
      //               getSharedCards();
      //             });
      //           }
      //         });
      //       });
      //       // Upload.upload({
      //       //   url: '/cards/saveCard',
      //       //   headers: {
      //       //     'Content-Type': 'multipart/form-data'
      //       //   },
      //       //   data: {
      //       //     draft: true,
      //       //     eventTitle: vm.cardData.eventTitle,
      //       //     additionalTextForEvent: vm.cardData.additionalTextForEvent || '',
      //       //     eventDate: vm.cardData.eventDate || '',
      //       //     eventStartTime: vm.cardData.eventStartTime || '',
      //       //     eventEndTime: vm.cardData.eventEndTime || '',
      //       //     locationName: vm.cardData.locationName || '',
      //       //     state: vm.cardData.state || '',
      //       //     city: vm.cardData.city || '',
      //       //     zip: vm.cardData.zip || '',
      //       //     cornerRadius: vm.cardData.cornerRadius || '',
      //       //     styles: vm.cardData.styles || '',
      //       //     background: imageId,
      //       //     draftId: vm.activeDraftCard || '',
      //       //     position: vm.eventCssPosition || '',
      //       //     users: vm.selectedContactIdForShare || [],
      //       //     file: Upload.dataUrltoBlob(imageDataUrl, vm.cardData.eventTitle + '.png')
      //       //   }
      //       // }).success(function (data) {
      //       //   $timeout(function () {
      //       //     // goToPhotoLocation();
      //       //     console.log(data);
      //       //     vm.currentTabIndex = 0;
      //       //     showMessage('The card has been saved');
      //       //     getDraftCards();
      //       //   });
      //       // }).error(function (err) {
      //       //   console.log(err);
      //       // });
      //     }
      //   });
      //
      // });
    }

    function uploadImage(image, callback) {
      var image_return = '';
      if (image && image.type) {
        Upload.upload({
          url: '/cards/saveImage',
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          data: {
            file: image
          }
        }).then(function (resp) {
          callback(resp.data);
        }, function (resp) {
          console.log('Error status: ' + resp.status);
        });
      } else {
        callback(image_return);
      }
    }

    function uploadCanvas(canvas, callback) {
      if (canvas) {
        Upload.upload({
          url: '/cards/saveImage',
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          data: {
            file: Upload.dataUrltoBlob(canvas, vm.cardData.eventTitle + '.png')
          }
        }).then(function (resp) {
          callback(resp.data);
        }, function (resp) {
          console.log('Error status: ' + resp.status);
        });
      } else {
        callback('');
      }
    }


    function setCurrentFont(font) {
      vm.cardData.styles.font = font;
    }

    function setCurrentFontSize(fontSize) {
      vm.cardData.styles.fontSize = fontSize;
    }


    /**
     * Change font style
     * @method changeFontStyleBold
     * @return
     */
    function changeFontStyleBold() {
      if (vm.cardData.styles.fontWeight === 'bold') {
        vm.cardData.styles.fontWeight = '';
      } else {
        vm.cardData.styles.fontWeight = 'bold';
      }
    }

    angular.element.fn.copyCSS = function (source) {
      var dom = angular.element(source).get(0);
      var dest = {};
      var style, prop;
      if (window.getComputedStyle) {
        var camelize = function (a, b) {
          return b.toUpperCase();
        };
        if (style = window.getComputedStyle(dom, null)) {
          var camel, val;
          if (style.length) {
            for (var i = 0, l = style.length; i < l; i++) {
              prop = style[i];
              camel = prop.replace(/\-([a-z])/, camelize);
              val = style.getPropertyValue(prop);
              dest[camel] = val;
            }
          } else {
            for (prop in style) {
              camel = prop.replace(/\-([a-z])/, camelize);
              val = style.getPropertyValue(prop) || style[prop];
              dest[camel] = val;
            }
          }
          return this.css(dest);
        }
      }
      if (style = dom.currentStyle) {
        for (prop in style) {
          dest[prop] = style[prop];
        }
        return this.css(dest);
      }
      if (style = dom.style) {
        for (prop in style) {
          if (typeof style[prop] != 'function') {
            dest[prop] = style[prop];
          }
        }
      }
      return this.css(dest);
    };

    function generateImage(callback) {
      var cardPreview = angular.element("div.card-preview");
      cardPreview.appendTo('body');
      html2canvas(angular.element('div.card-preview'), {
        onrendered: function(canvas) {
          cardPreview.appendTo('#cardEditor');
          callback(canvas.toDataURL());
        },
        useCORS:true
      });
    }

    /**
     * Description
     * @method changeFontStyleItalic
     * @return
     */
    function changeFontStyleItalic() {
      if (vm.cardData.styles.fontStyle == 'italic') {
        vm.cardData.styles.fontStyle = '';
      } else {
        vm.cardData.styles.fontStyle = 'italic';
      }
    }

    /**
     * Description
     * @method changeFontStyleUnderline
     * @return
     */
    function changeFontStyleUnderline() {
      if (vm.cardData.styles.fontStyleUnderline == 'underline') {
        vm.cardData.styles.fontStyleUnderline = '';
      } else {
        vm.cardData.styles.fontStyleUnderline = 'underline';
      }
    }


    vm.gradientOptions = [
      {values: "top", field: "Lighter from the top"},
      {values: "right", field: "Lighter from the right"},
      {values: "left", field: "Lighter from the left"},
      {values: "bottom", field: "Lighter from the bottom"},
    ];


    /**
     * Change gradient Color
     * @method changeGradientColor
     * @return
     */
    function changeGradientColor() {
      if (vm.cardData.gradient) {
        vm.cardData.styles.background = '';
        vm.changeBgImage = null;
        angular.element('#cardContent').css({
          'background': '-webkit-linear-gradient(' + vm.cardData.gradientStyle.gradientType + ',' +
          vm.cardData.gradientStyle.colorOne + ',' + vm.cardData.gradientStyle.colorTwo + ')'
        });
        angular.element('#cardContent').css({
          'background': 'linear-gradient(' + vm.cardData.gradientStyle.gradientType + ',' +
          vm.cardData.gradientStyle.colorOne + ',' + vm.cardData.gradientStyle.colorTwo + ')'
        });
        angular.element('#cardContent').css({
          'background': '-moz-linear-gradient(' + vm.cardData.gradientStyle.gradientType + ',' +
          vm.cardData.gradientStyle.colorOne + ',' + vm.cardData.gradientStyle.colorTwo + ')'
        });
      }
    }


    vm.fontFamilies = [
      {name: "Aclonica", fontFamily: "Aclonica"},
      {name: "Allan", fontFamily: "Allan"},
      {name: "Annie Use Your Telescope", fontFamily: "Annie Use Your Telescope"},
      {name: "Anonymous Pro", fontFamily: "Anonymous Pro"},
      {name: "Allerta Stencil", fontFamily: "Allerta Stencil"},
      {name: "Allerta", fontFamily: "Allerta"},
      {name: "Amaranth", fontFamily: "Amaranth"},
      {name: "Anton", fontFamily: "Anton"},
      {name: "Architects Daughter", fontFamily: "Architects Daughter"},
      {name: "Arimo", fontFamily: "Arimo"},
      {name: "Artifika", fontFamily: "Artifika"},
      {name: "Arvo", fontFamily: "Arvo"},
      {name: "Asset", fontFamily: "Asset"},
      {name: "Astloch", fontFamily: "Astloch"},
      {name: "Bangers", fontFamily: "Bangers"},
      {name: "Bentham", fontFamily: "Bentham"},
      {name: "Bevan", fontFamily: "Bevan"},
      {name: "Bigshot One", fontFamily: "Bigshot One"},
      {name: "Bowlby One", fontFamily: "Bowlby One"},
      {name: "Bowlby One SC", fontFamily: "Bowlby One SC"},
      {name: "Brawler", fontFamily: "Brawler"},
      {name: "Buda", fontFamily: "Buda"},
      {name: "Cabin", fontFamily: "Cabin"},
      {name: "Calligraffitti", fontFamily: "Calligraffitti"},
      {name: "Candal", fontFamily: "Candal"},
      {name: "Cantarell", fontFamily: "Cantarell"},
      {name: "Cardo", fontFamily: "Cardo"},
      {name: "Carter One", fontFamily: "Carter One"},
      {name: "Caudex", fontFamily: "Caudex"},
      {name: "Cedarville Cursive", fontFamily: "Cedarville Cursive"},
      {name: "Cherry Cream Soda", fontFamily: "Cherry Cream Soda"},
      {name: "Chewy", fontFamily: "Chewy"},
      {name: "Coda", fontFamily: "Coda"},
      {name: "Coming Soon", fontFamily: "Coming Soon"},
      {name: "Copse", fontFamily: "Copse"},
      {name: "Corben", fontFamily: "Corben"},
      {name: "Cousine", fontFamily: "Cousine"},
      {name: "Covered By Your Grace", fontFamily: "Covered By Your Grace"},
      {name: "Crafty Girls", fontFamily: "Crafty Girls"},
      {name: "Crimson Text", fontFamily: "Crimson Text"},
      {name: "Crushed", fontFamily: "Crushed"},
      {name: "Cuprum", fontFamily: "Cuprum"},
      {name: "Damion", fontFamily: "Damion"},
      {name: "Dancing Script", fontFamily: "Dancing Script"},
      {name: "Dawning of a New Day", fontFamily: "Dawning of a New Day"},
      {name: "Didact Gothic", fontFamily: "Didact Gothic"},
      {name: "Droid Sans", fontFamily: "Droid Sans"},
      {name: "Droid Sans Mono", fontFamily: "Droid Sans Mono"},
      {name: "Droid Sans Mono", fontFamily: "Droid Sans Mono"},
      {name: "Droid Serif", fontFamily: "Droid Serif"},
      {name: "EB Garamond", fontFamily: "EB Garamond"},
      {name: "Expletus Sans", fontFamily: "Expletus Sans"},
      {name: "Fontdiner Swanky", fontFamily: "Fontdiner Swanky"},
      {name: "Forum", fontFamily: "Forum"},
      {name: "Francois One", fontFamily: "Francois One"},
      {name: "Geo", fontFamily: "Geo"},
      {name: "Give You Glory", fontFamily: "Give You Glory"},
      {name: "Goblin One", fontFamily: "Goblin One"},
      {name: "Goudy Bookletter 1911", fontFamily: "Goudy Bookletter 1911"},
      {name: "Gravitas One", fontFamily: "Gravitas One"},
      {name: "Gruppo", fontFamily: "Gruppo"},
      {name: "Hammersmith One", fontFamily: "Hammersmith One"},
      {name: "Holtwood One SC", fontFamily: "Holtwood One SC"},
      {name: "Homemade Apple", fontFamily: "Homemade Apple"},
      {name: "Inconsolata", fontFamily: "Inconsolata"},
      {name: "Indie Flower", fontFamily: "Indie Flower"},
      {name: "IM Fell DW Pica", fontFamily: "IM Fell DW Pica"},
      {name: "IM Fell DW Pica SC", fontFamily: "IM Fell DW Pica SC"},
      {name: "IM Fell Double Pica", fontFamily: "IM Fell Double Pica"},
      {name: "IM Fell Double Pica SC", fontFamily: "IM Fell Double Pica SC"},
      {name: "IM Fell English", fontFamily: "IM Fell English"},
      {name: "IM Fell English SC", fontFamily: "IM Fell English SC"},
      {name: "IM Fell French Canon", fontFamily: "IM Fell French Canon"},
      {name: "IM Fell French Canon SC", fontFamily: "IM Fell French Canon SC"},
      {name: "IM Fell Great Primer", fontFamily: "IM Fell Great Primer"},
      {name: "IM Fell Great Primer SC", fontFamily: "IM Fell Great Primer SC"},
      {name: "Irish Grover", fontFamily: "Irish Grover"},
      {name: "Istok Web", fontFamily: "Istok Web"},
      {name: "Josefin Sans", fontFamily: "Josefin Sans"},
      {name: "Josefin Slab", fontFamily: "Josefin Slab"},
      {name: "Judson", fontFamily: "Judson"},
      {name: "Jura", fontFamily: "Jura"},
      {name: "Just Another Hand", fontFamily: "Just Another Hand"},
      {name: "Just Me Again Down Here", fontFamily: "Just Me Again Down Here"},
      {name: "Kameron", fontFamily: "Kameron"},
      {name: "Kenia", fontFamily: "Kenia"},
      {name: "Kranky", fontFamily: "Kranky"},
      {name: "Kreon", fontFamily: "Kreon"},
      {name: "Kristi", fontFamily: "Kristi"},
      {name: "La Belle Aurore", fontFamily: "La Belle Aurore"},
      {name: "Lato", fontFamily: "Lato"},
      {name: "League Script", fontFamily: "League Script"},
      {name: "Lekton", fontFamily: "Lekton"},
      {name: "Limelight", fontFamily: "Limelight"},
      {name: "Lobster", fontFamily: "Lobster"},
      {name: "Lobster Two", fontFamily: "Lobster Two"},
      {name: "Lora", fontFamily: "Lora"},
      {name: "Love Ya Like A Sister", fontFamily: "Love Ya Like A Sister"},
      {name: "Loved by the King", fontFamily: "Loved by the King"},
      {name: "Luckiest Guy", fontFamily: "Luckiest Guy"},
      {name: "Maiden Orange", fontFamily: "Maiden Orange"},
      {name: "Mako", fontFamily: "Mako"},
      {name: "Maven Pro", fontFamily: "Maven Pro"},
      {name: "Meddon", fontFamily: "Meddon"},
      {name: "MedievalSharp", fontFamily: "MedievalSharp"},
      {name: "Megrim", fontFamily: "Megrim"},
      {name: "Merriweather", fontFamily: "Merriweather"},
      {name: "Metrophobic", fontFamily: "Metrophobic"},
      {name: "Michroma", fontFamily: "Michroma"},
      {name: "Miltonian Tattoo", fontFamily: "Miltonian Tattoo"},
      {name: "Miltonian", fontFamily: "Miltonian"},
      {name: "Modern Antiqua", fontFamily: "Modern Antiqua"},
      {name: "Monofett", fontFamily: "Monofett"},
      {name: "Molengo", fontFamily: "Molengo"},
      {name: "Mountains of Christmas", fontFamily: "Mountains of Christmas"},
      {name: "Muli", fontFamily: "Muli"},
      {name: "Neucha", fontFamily: "Neucha"},
      {name: "Neuton", fontFamily: "Neuton"},
      {name: "News Cycle", fontFamily: "News Cycle"},
      {name: "Nixie One", fontFamily: "Nixie One"},
      {name: "Nova Cut", fontFamily: "Nova Cut"},
      {name: "Nova Flat", fontFamily: "Nova Flat"},
      {name: "Nova Mono", fontFamily: "Nova Mono"},
      {name: "Nova Oval", fontFamily: "Nova Oval"},
      {name: "Nova Round", fontFamily: "Nova Round"},
      {name: "Nova Script", fontFamily: "Nova Script"},
      {name: "Nova Slim", fontFamily: "Nova Slim"},
      {name: "Nova Square", fontFamily: "Nova Square"},
      {name: "Nunito", fontFamily: "Nunito"},
      {name: "Old Standard TT", fontFamily: "Old Standard TT"},
      {name: "Open Sans", fontFamily: "Open Sans"},
      {name: "Open Sans Condensed", fontFamily: "Open Sans Condensed"},
      {name: "Orbitron", fontFamily: "Orbitron"},
      {name: "Oswald", fontFamily: "Oswald"},
      {name: "Over the Rainbow", fontFamily: "Over the Rainbow"},
      {name: "Reenie Beanie", fontFamily: "Reenie Beanie"},
      {name: "Pacifico", fontFamily: "Pacifico"},
      {name: "Patrick Hand", fontFamily: "Patrick Hand"},
      {name: "Patrick Hand SC", fontFamily: "Patrick Hand SC"},
      {name: "Paytone One", fontFamily: "Paytone One"},
      {name: "Permanent Marker", fontFamily: "Permanent Marker"},
      {name: "Philosopher", fontFamily: "Philosopher"},
      {name: "Play", fontFamily: "Play"},
      {name: "Playfair Display", fontFamily: "Playfair Display"},
      {name: "Podkova", fontFamily: "Podkova"},
      {name: "PT Sans", fontFamily: "PT Sans"},
      {name: "PT Sans Narrow", fontFamily: "PT Sans Narrow"},
      {name: "PT Serif", fontFamily: "PT Serif"},
      {name: "PT Serif Caption", fontFamily: "PT Serif Caption"},
      {name: "Puritan", fontFamily: "Puritan"},
      {name: "Quattrocento", fontFamily: "Quattrocento"},
      {name: "Quattrocento Sans", fontFamily: "Quattrocento Sans"},
      {name: "Radley", fontFamily: "Radley"},
      {name: "Raleway", fontFamily: "Raleway"},
      {name: "Redressed", fontFamily: "Redressed"},
      {name: "Rock Salt", fontFamily: "Rock Salt"},
      {name: "Rokkitt", fontFamily: "Rokkitt"},
      {name: "Ruslan Display", fontFamily: "Ruslan Display"},
      {name: "Schoolbell", fontFamily: "Schoolbell"},
      {name: "Shadows Into Light", fontFamily: "Shadows Into Light"},
      {name: "Shanti", fontFamily: "Shanti"},
      {name: "Sigmar One", fontFamily: "Sigmar One"},
      {name: "Six Caps", fontFamily: "Six Caps"},
      {name: "Slackey", fontFamily: "Slackey"},
      {name: "Smythe", fontFamily: "Smythe"},
      {name: "Sniglet", fontFamily: "Sniglet"},
      {name: "Special Elite", fontFamily: "Special Elite"},
      {name: "Stardos Stencil", fontFamily: "Stardos Stencil"},
      {name: "Sue Ellen Francisco", fontFamily: "Sue Ellen Francisco"},
      {name: "Sunshiney", fontFamily: "Sunshiney"},
      {name: "Swanky and Moo Moo", fontFamily: "Swanky and Moo Moo"},
      {name: "Syncopate", fontFamily: "Syncopate"},
      {name: "Tangerine", fontFamily: "Tangerine"},
      {name: "Tenor Sans", fontFamily: "Tenor Sans"},
      {name: "The Girl Next Door", fontFamily: "The Girl Next Door"},
      {name: "Tinos", fontFamily: "Tinos"},
      {name: "Ubuntu", fontFamily: "Ubuntu"},
      {name: "Ultra", fontFamily: "Ultra"},
      {name: "Unkempt", fontFamily: "Unkempt"},
      {name: "UnifrakturCook", fontFamily: "UnifrakturCook"},
      {name: "UnifrakturMaguntia", fontFamily: "UnifrakturMaguntia"},
      {name: "Varela", fontFamily: "Varela"},
      {name: "Varela Round", fontFamily: "Varela Round"},
      {name: "Vibur", fontFamily: "Vibur"},
      {name: "Vollkorn", fontFamily: "Vollkorn"},
      {name: "VT323", fontFamily: "VT323"},
      {name: "Waiting for the Sunrise", fontFamily: "Waiting for the Sunrise"},
      {name: "Wallpoet", fontFamily: "Wallpoet"},
      {name: "Walter Turncoat", fontFamily: "Walter Turncoat"},
      {name: "Wire One", fontFamily: "Wire One"},
      {name: "Yanone Kaffeesatz", fontFamily: "Yanone Kaffeesatz"},
      {name: "Yeseva One", fontFamily: "Yeseva One"},
      {name: "Zeyada", fontFamily: "Zeyada"},
    ];

    vm.fontSizeSelect = ['10px', '11px', '12px', '13px', '14px', '15px', '16px', '17px', '18px', '19px', '20px', '21px', '22px', '23px', '24px', '25px', '26px'];


    var varTualRange = angular.element('#cardContent');
    // var varTualRange = angular.element('#eventTitle').parent().parent();
    angular.element(document).ready(function () {
      angular.element('#eventTitle').draggable({containment: varTualRange});
      angular.element('#eventDate').draggable({containment: varTualRange});
      angular.element('#eventTime').draggable({containment: varTualRange});
      angular.element('#evrntLocation').draggable({containment: varTualRange});
    });

    vm.changeRadius = function () {
      angular.element('#cardContent').css({'border-radius': 0 + 'px'});
      angular.element('#cardContent').css({'border-radius': vm.cardData.cornerRadius + 'px'});
    };

    vm.changeBackgroundColor = function () {
      vm.cardData.styles.background = '';
      vm.changeBgImage = null;
      // vm.cardData.styles.backgroundColor = vm.cardData.cardBackgroundColor;
      vm.cardData.gradient = false;
    };


    // angular.element("#cardFontSelect").fontselect();

    function changeBackGroundImage(ev) {
      vm.newFolderName = null;
      $mdDialog.show({
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/cards/dialogs/change-bg-image.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: false
      });
    }

    vm.closeDialog = function () {
      $mdDialog.hide();
    };


    /*
     * Toggle selected status of the contact
     *
     * @param contact
     * @param event
     */
    function toggleSelectContact(contact, event) {
      if (event) {
        event.stopPropagation();
      }

      if (vm.selectedContacts.indexOf(contact) > -1) {
        vm.selectedContacts.splice(vm.selectedContacts.indexOf(contact), 1);
      }
      else {
        vm.selectedContacts.push(contact);
      }
    }


    /*
     *
     */
    function addNewGroup() {
      if (vm.newGroupName === '') {
        return;
      }

      var newGroup = {
        'id': msUtils.guidGenerator(),
        'name': vm.newGroupName,
        'contactIds': []
      };

      vm.user.groups.push(newGroup);
      vm.newGroupName = '';
    }

    function showMessage(message) {
      $mdToast.show({
        template: '<md-toast class="md-toast success">' + message + '</md-toast>',
        hideDelay: 3000,
        position: 'bottom left'
      });
    }


    /*
     * Toggle sidenav
     *
     * @param sidenavId
     */
    function toggleSidenav(sidenavId) {
      $mdSidenav(sidenavId).toggle();
    }

    Array.prototype.getBy = function (id, value) {
      return this.filter(function (x) {
        return x[id] === value;
      })[0];
    };


  }

})();
