(function () {
  'use strict';

  angular
    .module('app.cards',
      [
        // 3rd Party Dependencies
        // 'xeditable',
        'ngDraggable',
        'ngFileUpload',
        'ngImgCrop',
        'mdPickers',
        'ngMaterial'
      ]
    )
    .config(config);

  /** @ngInject */
  function config($stateProvider) {

    $stateProvider.state('app.cards', {
      url: '/cards',
      views: {
        'content@app': {
          templateUrl: 'app/main/cards/cards.html',
          controller: 'cardsController as vm'
        }
      }
    });

  }

})();
