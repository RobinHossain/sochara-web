(function () {
  'use strict';

  angular
    .module('sochara')
    .factory('mainService', MainService);

  /** @ngInject */
  function MainService($http, $q) {
    return {
      userInfo: function () {
        var defer = $q.defer();
        $http.post('/main/user').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      }

    };
  }

})();
