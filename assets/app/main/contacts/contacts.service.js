(function () {
  'use strict';

  angular
    .module('sochara')
    .factory('ContactService', contactService);

  /** @ngInject */
  function contactService($http, $q) {
    return {
      batchContactDelete: function (ids) {
        var submitURL = $http.post('/contacts/batchContactDelete', {ids: ids});
        submitURL.success(function (result) {
          return result.msg;
        });
        submitURL.error(function (resp) {
          return resp.err;
        });
        return submitURL;
      },
      fetchOutlookApiData: function (data) {
        var defer = $q.defer();
        $http.post('/contacts/fetchOutlookApiData', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      fetchGoogleApiData: function ( data ) {
        var defer = $q.defer();
        $http.post('/contacts/fetchGoogleApiData', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      importSelectedGoogleContact: function ( data ) {
        var defer = $q.defer();
        $http.post('/contacts/importSelectedGoogleContact', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getFavorites: function () {
        var submitURL = $http.post('/contacts/getFavorites');
        submitURL.success(function (result) {
          return result.msg;
        });
        submitURL.error(function (resp) {
          return resp.err;
        });
        return submitURL;
      },
      updateFavorites: function (data) {
        var defer = $q.defer();
        $http.post('/contacts/updateFavorites', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      removeFavorites: function (data) {
        var defer = $q.defer();
        $http.post('/contacts/removeFavorites', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      exportContacts: function (data) {
        var defer = $q.defer();
        $http.post('/contacts/exportContacts', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      newGroup: function (name, groupContactsId) {
        var submitURL = $http.post('/contacts/newGroup', {name: name, groupContactsId: groupContactsId});
        submitURL.success(function (result) {
          return result.msg;
        });
        submitURL.error(function (resp) {
          return resp.err;
        });
        return submitURL;
      },
      getGroups: function () {
        var defer = $q.defer();
        $http.post('/contacts/getGroups').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getOutlookImportedTempData: function () {
        var defer = $q.defer();
        $http.post('/thirdpartytoken/getTempContacts').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getContacts: function () {
        var defer = $q.defer();
        $http.post('/contacts/getContacts').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getAllContacts: function () {
        var defer = $q.defer();
        $http.post('/contacts/getAllContacts').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getPendingRequestList: function () {
        var defer = $q.defer();
        $http.post('/contacts/getPendingRequestList').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getMyRequestList: function () {
        var defer = $q.defer();
        $http.post('/contacts/getMyRequestList').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getPendingRequestListCount: function () {
        var defer = $q.defer();
        $http.post('/contacts/getPendingRequestListCount').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      getAllSocharaUsers: function ( data ) {
        var defer = $q.defer();
        $http.post('/contacts/users', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      addContactRequest: function (data) {
        var defer = $q.defer();
        $http.post('/contacts/addContactRequest', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      deleteContactRequest: function (id) {
        var defer = $q.defer();
        var data = {};
        data.id = id;
        $http.post('/contacts/deleteContactRequest', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      acceptContactRequest: function (data) {
        var defer = $q.defer();
        $http.post('/contacts/acceptContactRequest', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },
      updateGroupContacts: function (id, groupId) {
        var submitURL = $http.post('/contacts/updateGroupContacts', {id: id, groupId: groupId});
        submitURL.success(function (result) {
          console.log(result);
          return result.msg;
        });
        submitURL.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return submitURL;
      },
      addNewContact: function (data) {
        var defer = $q.defer();
        $http.post('/contacts/addContact', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      updateContact: function (contactData) {
        var submitURL = $http.post('/contacts/updateContact', contactData);
        submitURL.success(function (result) {
          console.log(result);
          return result.msg;
        });
        submitURL.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return submitURL;
      },
      removeContact: function (id) {
        var submitURL = $http.post('/contacts/removeContact', {id: id});
        submitURL.success(function (result) {
          console.log(result);
          return result.msg;
        });
        submitURL.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return submitURL;
      },

      updateGroupFromList: function (id, name, groupContactsId) {
        var submitURL = $http.post('/contacts/updateGroupFromList', {
          id: id,
          name: name,
          groupContactsId: groupContactsId
        });
        submitURL.success(function (result) {
          console.log(result);
          return result.msg;
        });
        submitURL.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return submitURL;
      },
      removeGroup: function (id) {
        var submitURL = $http.post('/contacts/removeGroup', {id: id});
        submitURL.success(function (result) {
          console.log(result);
          return result.msg;
        });
        submitURL.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return submitURL;
      },
      updateGroupName: function (id, name) {
        var submitURL = $http.post('/contacts/updateGroupName', {id: id, name: name});
        submitURL.success(function (result) {
          console.log(result);
          return result.msg;
        });
        submitURL.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return submitURL;
      },
      changeIsShownGroup: function (id, trueOrFalse) {
        var submitURL = $http.post('/contacts/changeIsShownGroup', {id: id, trueOrFalse: trueOrFalse});
        submitURL.success(function (result) {
          console.log(result);
          return result;
        });
        submitURL.error(function (resp) {
          console.log(resp);
          return resp.err;
        });
        return submitURL;
      },
    };
  }

})();
