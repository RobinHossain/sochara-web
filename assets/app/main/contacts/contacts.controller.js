(function () {
  'use strict';

  angular
    .module('app.contacts')
    .controller('ContactsController', ContactsController)
    .config(['$compileProvider', function ($compileProvider) {
      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|skype|tel|aim|ymsgr):/);
    }
    ]);

  /**
   * Description
   * @ngInject 
   * @method ContactsController
   * @param {} $scope
   * @param {} Auth
   * @param {} ContactService
   * @param {} $mdSidenav
   * @param {} $q
   * @param {} MailService
   * @param {} $rootScope
   * @param {} Upload
   * @param {} $window
   * @param {} $timeout
   * @param {} $mdDialog
   * @param {} $mdToast
   * @param {} $location
   * @param {} $state
   * @param {} $document
   * @return 
   */
  function ContactsController($scope, Auth, ContactService, $mdSidenav, $q, MailService, $rootScope, Upload, $window, $timeout, $mdDialog, $mdToast, $location, $state, $document) {

    var vm = this;

    vm.currentUrl = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');
    vm.siteUrl = $location.protocol() + '://' + $location.host() + ':' + $location.port();
    // Methods
    vm.filterChange = filterChange;
    vm.openContactDialog = openContactDialog;
    vm.openAddContactDialog = openAddContactDialog;
    vm.deleteContactConfirm = deleteContactConfirm;
    vm.deleteContact = deleteContact;
    vm.deleteSelectedContacts = deleteSelectedContacts;
    vm.toggleSelectContact = toggleSelectContact;
    vm.toggleExportSelectContact = toggleExportSelectContact;
    vm.deselectContacts = deselectContacts;
    vm.selectAllContacts = selectAllContacts;
    vm.deleteContact = deleteContact;
    vm.addNewGroup = addNewGroup;
    vm.updateGroupFromList = updateGroupFromList;
    vm.openImportExportContactDialog = openImportExportContactDialog;
    vm.deleteGroup = deleteGroup;
    vm.toggleSidenav = toggleSidenav;
    // vm.toggleInArray = msUtils.toggleInArray;
    // vm.exists = msUtils.exists;
    vm.addContact = {};
    vm.loadGroupsNameforExport = loadGroupsNameforExport;
    vm.addContact.job = [];
    vm.authgoogle = authgoogle;
    vm.exportContactSubmit = exportContactSubmit;
    vm.uploadVcf = uploadVcf;
    vm.outlookContactImport = outlookContactImport;
    /**
     * Description
     * @method StrToDate
     * @param {} str
     * @return NewExpression
     */
    vm.StrToDate = function (str) {
      return new Date(str);
    };
    vm.upload = upload;
    vm.checkExists = checkExists;
    vm.loadContact = loadContact;
    vm.toggleFavoriteContact = toggleFavoriteContact;
    vm.loadGroups = loadGroups;
    vm.addContactRequest = addContactRequest;
    vm.getPendingRequestList = getPendingRequestList;
    vm.getMyRequestList = getMyRequestList;
    vm.acceptContactRequest = acceptContactRequest;
    vm.declineContactRequest = declineContactRequest;
    vm.declineContactRequestConfirm = declineContactRequestConfirm;
    vm.closeDialog = closeDialog;
    vm.contactData = {};
    vm.selectedContacts = [];
    vm.selectContactForAdd = selectContactForAdd;
    vm.groupAddedContact = [];
    vm.saveUpdatedContact = saveUpdatedContact;
    vm.isSelected = isSelected;
    vm.toggleSelectContacts = toggleSelectContacts;
    $rootScope.searchable = false;

    init();

    /**
     * Description
     * @method init
     * @return 
     */
    function init() {
      $rootScope.loadingProgress = true;
      vm.loadingContacts = true;
      vm.user = $rootScope.user;

      $timeout(function () {
        var fullUrl = $location.url();
        var module = fullUrl.substring(1).split('/', 1)[0];
        if (module === 'contacts') {
          loadExtarnalJavascript();
        }
      });

      $timeout(function () {
        vm.loadGroups();
        getPendingRequestListCount();
        loadContact();//hit the function
      }, 1000);

      if($location.search().import && $location.search().outlook){
        getOutlookImportedTempData();
      }
    }

    /**
     * Description
     * @method getOutlookImportedTempData
     * @return 
     */
    function getOutlookImportedTempData(){
      ContactService.getOutlookImportedTempData().then(function (contactData) {
        vm.importedGoogleContact = contactData;
        if(contactData && contactData.length){
          showDialogue('app/main/contacts/dialogs/import/outlook.html');
        }else{
          showMessage('No contact found to import!');
        }
      });
    }

    /**
     * Description
     * @method loadExtarnalJavascript
     * @return 
     */
    function loadExtarnalJavascript() {
      var tag;
      tag = document.createElement("script");
      tag.src = "/js/dependencies/live/wl.js";
      document.getElementsByTagName("head")[0].appendChild(tag);

      tag = document.createElement("script");
      tag.src = "https://apis.google.com/js/api.js";
      document.getElementsByTagName("head")[0].appendChild(tag);
    }

    /**
     * Load all logged in user contact.
     * @method loadContact
     * @return 
     */
    function loadContact() {
      ContactService.getAllContacts().then(function (data) {
        $rootScope.loadingProgress = false;
        vm.loadingContacts = false;
        vm.contacts = data;
        vm.contactShow = data[0];
        vm.favoriteContacts = vm.contacts.filter(function (contact) {
          return contact.favorite === true;
        });
      });
    }

    /**
     * Description
     * @method getPendingRequestListCount
     * @return 
     */
    function getPendingRequestListCount() {
      ContactService.getPendingRequestListCount().then(function (data) {
        vm.pendingRequestCount = data.count;
      });
    }

    /**
     * Description
     * @method selectContactForAdd
     * @param {} contact
     * @return 
     */
    function selectContactForAdd(contact) {
      serializeSharedUser(vm.groupAddedContact, function (returnUsers) {
        vm.selectedContactIdForShare = returnUsers;
        if (vm.selectedContactIdForShare.indexOf(contact.id) === -1) {
          console.log('inner contact');
          console.log(contact);
          vm.groupAddedContact.push(contact);
          vm.selectedContactIdForShare.push(contact.id);
        }
      });
      vm.searchedContact = null;
    }

    /**
     * Description
     * @method serializeSharedUser
     * @param {} users
     * @param {} callback
     * @return 
     */
    function serializeSharedUser(users, callback) {
      var userData = [];
      if (users && users.length) {
        users.forEach(function (user) {
          userData.push(user.id);
        });
      }
      callback(userData);
    }

    /**
     * Load all pending Request.
     * @method getPendingRequestList
     * @return 
     */
    function getPendingRequestList() {
      vm.listType = 'pending';
      vm.contacts = [];
      ContactService.getPendingRequestList().then(function (data) {
        vm.loadingContacts = false;
        vm.contacts = data;
        vm.contactShow = '';
        console.log(vm.contacts);
      });
    }

    /**
     * Description
     * @method getMyRequestList
     * @return 
     */
    function getMyRequestList() {
      vm.listType = 'request';
      vm.socharaContacts = [];
      ContactService.getMyRequestList().then(function (data) {
        vm.loadingContacts = false;
        vm.contacts = data;
        vm.contactShow = '';
        console.log(vm.contacts);
      });
    }


    /**
     * Description
     * @method acceptContactRequest
     * @param {} id
     * @return 
     */
    function acceptContactRequest(id) {
      if (id) {
        ContactService.acceptContactRequest({id: id}).then(function (response) {
          vm.getPendingRequestList();
          getPendingRequestListCount();
          $rootScope.getGlobalNotificationCount();
        });
      } else {
        showMessage('The Contact request you want accept is not found !');
      }
    }

    /**
     * Description
     * @method declineContactRequest
     * @param {} id
     * @return 
     */
    function declineContactRequest(id) {
      vm.deleteContaactRequestId = id;
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/contacts/dialogs/delete.html',
        parent: angular.element($document.body),
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method declineContactRequestConfirm
     * @return 
     */
    function declineContactRequestConfirm() {
      if (vm.deleteContaactRequestId) {
        ContactService.deleteContactRequest(vm.deleteContaactRequestId).then(function (response) {
          vm.deleteContaactRequestId = null;
          vm.closeDialog();
          vm.getPendingRequestList();
          showMessage('The Contact request has been deleted.');
        });
      } else {
        showMessage('The Contact request you want do delete is not found !');
      }
    }


    //setup before functions
    var contactSearchTypingTimer;                //timer identifier
    var contactSearchDoneTypingInterval = 1000;  //time in ms, 5 second for example
    vm.contactSearchTimoutInterval = contactSearchTimoutInterval;
    vm.contactSearchClearTimoutInterval = contactSearchClearTimoutInterval;

    /**
     * Description
     * @return
     * @method contactSearchTimoutInterval
     * @return 
     */
    function contactSearchTimoutInterval() {
      $timeout.cancel(contactSearchTypingTimer);
      contactSearchTypingTimer = $timeout(contactSearch, contactSearchDoneTypingInterval);
    }

    /**
     * Description
     * @return
     * @method contactSearch
     * @return 
     */
    function contactSearch() {
      if( vm.searchContact.length ){
        ContactService.getAllSocharaUsers({name: vm.searchContact}).then(function (response) {
          vm.socharaContacts = response;
          vm.showSocharaSearchResult = true;
        });
      }else{
        vm.socharaContacts = [];
      }
    }

    /**
     * Description
     * @return
     * @method contactSearchClearTimoutInterval
     * @return 
     */
    function contactSearchClearTimoutInterval() {
      $timeout.cancel(contactSearchTypingTimer);
    }


    /**
     * Description
     * @method addContactRequest
     * @param {} id
     * @return 
     */
    function addContactRequest(id) {
      vm.socharaContacts = vm.socharaContacts.filter(function (user) {
        return user.id !== id;
      });
      if( id ){
        ContactService.addContactRequest({id: id}).then(function (response) {
          if (response) {
            vm.contacts.push(response);
            showMessage('Contact request sent successfully.');
          }
        });
      }else{
        showMessage('No contact found with this id or something is wrong !');
      }
    }

    /**
     * Load all logged in user created groups.
     * @method loadGroups
     * @return 
     */
    function loadGroups() {
      ContactService.getGroups().then(function (resp) {
        vm.userGroups = resp;
      });
    }

    /**
     * Description
     * @method savePhoto
     * @param {} data
     * @return 
     */
    vm.savePhoto = function (data) {
      Auth.savePhoto(data).success(function () {
        showMessage('Photo has been updated successfully!');
      }).error(function () {
        showMessage('Something went wrong. Please try again!');
      });
    };

    /**
     * Description
     * @method loadFavorites
     * @return 
     */
    vm.loadFavorites = function () {
      ContactService.getFavorites().then(function (resp) {
        if (resp && typeof resp.contactIds !== 'undefined') {
          vm.starredUsers = resp.contactIds;
        }
        else {
          vm.starredUsers = [];
        }
      });

    };

    /**
     * Description
     * @method toggleFavoriteContact
     * @param {} item
     * @param {} status
     * @return 
     */
    function toggleFavoriteContact(item, status) {
      if (status === false) {
        ContactService.updateFavorites({id: item}).then(function (resp) {
          if (resp) {
            var message = 'The selected contact set as your favorite contact.';
            showMessage(message);
            vm.loadContact();
          }
        });
      } else {
        ContactService.removeFavorites({id: item}).then(function (resp) {
          if (resp) {
            var message = 'The selected contact remove from your favorite contact.';
            showMessage(message);
            vm.loadContact();
          }
        });
      }
    }

    /**
     * Check if item exists in a list
     * @method checkExists
     * @param item
     * @param list
     * @return 
     */
    function checkExists(item, list) {
      if (list.length) {
        return list.indexOf(item) > -1;
      }
    }

    vm.filterIds = null;
    vm.listType = 'all';
    vm.listOrder = 'name';
    vm.listOrderAsc = false;
    vm.selectedContacts = [];
    vm.newGroupName = '';
    /**
     * Description
     * @method closeAddContactDialog
     * @return 
     */
    vm.closeAddContactDialog = function () {
      $mdDialog.hide();
    };

    /**
     * Description
     * @method closeDialog
     * @return 
     */
    function closeDialog() {
      $mdDialog.hide();
    }

    /**
     * Description
     * @method loadGroupsNameforExport
     * @return CallExpression
     */
    function loadGroupsNameforExport() {
      return $timeout(function () {
        vm.groupsNameforExport = vm.user.groups;
      }, 650);
    }


    /**
     * Contacts item add
     * @method toggleInArrayNew
     * @param {} id
     * @param {} groupId
     * @return 
     */
    vm.toggleInArrayNew = function (id, groupId) {
      ContactService.updateGroupContacts(id, groupId).success(function () {
        vm.loadGroups();
      }).error(function () {

      });
    };

    /**
     * Description
     * @method uploadVcf
     * @param {} file
     * @return 
     */
    function uploadVcf(file) {

      if (file) {
        Upload.upload({
          url: '/contacts/uploadVcf',
          data: {file: file, 'username': $scope.username}
        }).then(function (resp) {

          // if(resp.data)
          showMessage('Saving Contacts!');
          $mdToast.hide();
          $mdDialog.hide();
          vm.importedVcfContact = resp.data;
          console.log('vm.importedVcfContact');
          console.log(vm.importedVcfContact);
          console.log('vm.importedVcfContact');
          vm.dtOptions = {
            dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
            pagingType: 'simple',
            autoWidth: false,
            responsive: true
          };

          vm.loadContact();//hit the function

          $mdDialog.show({
            /**
             * Description
             * @method controller
             * @return vm
             */
            controller: function () {
              return vm;
            },
            controllerAs: 'vm',
            templateUrl: 'app/main/contacts/dialogs/contact/contacts_imported_vcf.html',
            clickOutsideToClose: true,
            // targetEvent: ev,
          });

        }, function (resp) {
          showMessage('Something went wrong!');
        }, function (evt) {
          // var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          // //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
      }

    }

    /**
     * Description
     * @method upload
     * @param {} file
     * @return 
     */
    function upload(file) {
      // console.log(file);
      if (file) {
        Upload.upload({
          url: '/contacts/uploadCSV',
          data: {file: file, 'username': $scope.username}
        }).then(function (resp) {
          showMessage('Saving Contacts!');
          $mdToast.hide();
          $mdDialog.hide();
          vm.importedCsvContact = resp.data;
          vm.dtOptions = {
            dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
            pagingType: 'simple',
            autoWidth: false,
            responsive: true
          };

          vm.loadContact();//hit the function

          $mdDialog.show({
            /**
             * Description
             * @method controller
             * @return vm
             */
            controller: function () {
              return vm;
            },
            controllerAs: 'vm',
            templateUrl: 'app/main/contacts/dialogs/contact/contacts_imported_csv.html',
            clickOutsideToClose: true,
          });
        }, function (resp) {
          console.log(resp);
          showMessage('Something went wrong!');
        }, function (evt) {
        });
      }
    }

    /**
     * Description
     * @method getOutLookAuthUri
     * @return 
     */
    function getOutLookAuthUri(){
      MailService.OutlookAuthUrl({module: 'contact'}).then(function (response) {
        if (response) {
          vm.outlookContactAuthUrl = response;
        }
      });
    }


    /**
     * Description
     * @method outlookContactImport
     * @return 
     */
    function outlookContactImport() {



      //console.log('mad');
      // WL.init({
      //   client_id: "55a857a7-806b-4f56-9c0a-c086a8c9ab95",
      //   redirect_uri: vm.siteUrl+'/outlook/importContact',
      //   scope: ["wl.basic", "wl.contacts"],
      //   response_type: "token"
      // });
      // // WL.login({
      // //   scope: ["wl.basic"]
      // // }).then(function (response) {
      // //     WL.api({
      // //       path: "me/contacts",
      // //       method: "GET"
      // //     }).then(function (response) {
      // //       console.log(response);
      // //         // if (response.data.length) {
      // //         //   showMessage('Saving Contacts!');
      // //         //   ContactService.fetchOutlookApiData({data: JSON.stringify(response.data)}).success(function () {
      // //         //     $mdToast.hide();
      // //         //     $mdDialog.hide();
      // //         //     vm.importedOutlookContact = response.data;
      // //         //     // //console.log(resp[0]);
      // //         //     vm.dtOptions = {
      // //         //       dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
      // //         //       pagingType: 'simple',
      // //         //       autoWidth: false,
      // //         //       responsive: true
      // //         //     };
      // //         //
      // //         //     vm.loadContact();//hit the function
      // //         //
      // //         //     $mdDialog.show({
      // //         //       controller: function () {
      // //         //         return vm;
      // //         //       },
      // //         //       controllerAs: 'vm',
      // //         //       templateUrl: 'app/main/contacts/dialogs/contact/contacts_imported_outlook.html',
      // //         //       clickOutsideToClose: true,
      // //         //       // targetEvent: ev,
      // //         //     });
      // //         //   }).error(function () {
      // //         //     showMessage('Something went wrong!');
      // //         //   });
      // //         // } else {
      // //         //   showMessage('There is no contacts in your account!');
      // //         // }
      // //
      // //       },
      // //       function (responseFailed) {
      // //         ////console.log(responseFailed);
      // //         showMessage('Something went wrong!');
      // //       }
      // //     );
      // //   },
      // //   function (responseFailed) {
      // //     ////console.log("Error signing in: " + responseFailed.error_description);
      // //     showMessage('Something went wrong!');
      // //   });
    }

    //////////
    /**
     * Description
     * @description The function hitted when add contact button is pressed
     * @method addContactEventClicked
     * @return 
     */
    vm.addContactEventClicked = function () {
      delete vm.addContact.id;
      vm.addContact.first_name = '';
      vm.addContact.middle_name = '';
      vm.addContact.last_name = '';
      vm.addContact.notes = '';
      vm.myCroppedImage = '';
      vm.myImage = '';
      angular.forEach(
        angular.element(document.querySelector('#fileInput')),
        function (inputElem) {
          angular.element(inputElem).val(null);
        });
      vm.tmpAvatar = '';
      vm.addContact.date_birth = '';
      vm.addContact.date_anniversary = '';
      vm.addContact.job = [{title: '', company: ''}];
      vm.addContact.email = '';
      vm.addContact.emails = [{type: '', address: ''}];
      vm.addContact.phone = [{type: '', number: ''}];
      vm.addContact.address = [{type: '', address: {street: '', city: '', state: '', zip: '', country: ''}}];
      vm.addContact.relatives = [{type: '', number: ''}];
      vm.addContact.social = [{type: '', address: ''}];
      vm.addContact.im = [{type: '', address: ''}];
    };

    /**
     * Description
     * @method addContactItem
     * @param {} param
     * @return 
     */
    vm.addContactItem = function (param) {
      if (param === 'job') {
        vm.addContact.job.push({title: '', company: ''});
      }
      if (param === 'phone') {
        vm.addContact.phone.push({type: '', number: ''});
      }
      if (param === 'emails') {
        vm.addContact.emails.push({type: '', address: ''});
      }
      if (param === 'address') {
        vm.addContact.address.push({type: '', address: {street: '', city: '', state: '', zip: '', country: ''}});
      }

      if (param === 'relatives') {
        vm.addContact.relatives.push({type: '', number: ''});
      }

      if (param === 'social') {
        vm.addContact.social.push({type: '', address: ''});
      }
      if (param === 'im') {
        vm.addContact.im.push({type: '', address: ''});
      }
    };


    /**
     * Description
     * @method openImportExportContactDialog
     * @param {} ev
     * @return 
     */
    function openImportExportContactDialog(ev) {
      getOutLookAuthUri();
      vm.selectedContactsForExport = [];
      vm.exportType = [{title: 'Outlook', value: 'outlook', type: 'up', src:'images/contacts/outlook.png'}, {title: 'Google', value: 'google', type: 'up', src: 'images/contacts/gmail.png'}, {title: '', value: 'other', type: 'up', src: ''},
        {title: 'vCard File', value: 'card', type: 'down', src: 'images/icons/ic_import_contacts_black_24px.svg'}, {title: 'CSV File', value: 'csv', type: 'down', src: 'images/icons/ic_contact_phone_black_24px.svg'}];
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/contacts/dialogs/contact/contact-import-export-dialog.html',
        clickOutsideToClose: false,
        targetEvent: ev,
        /**
         * Description
         * @method onRemoving
         * @param {} event
         * @param {} removePromise
         * @return 
         */
        onRemoving: function (event, removePromise) {
          vm.filterChange('all');
          vm.search = '';
        }
      });
    }

    /**
     * Description
     * @description removing Job list
     * @method removeJobList
     * @param {} index
     * @return 
     */
    vm.removeJobList = function (index) {
      //console.log('mad')
      if (vm.addContact.job.length === 1) {
        vm.addContact.job[0].title = '';
        vm.addContact.job[0].company = '';
      } else {
        vm.addContact.job.splice(index, 1);
      }
    };

    /**
     * Description
     * @method removePhoneList
     * @param {} index
     * @return 
     */
    vm.removePhoneList = function (index) {
      if (vm.addContact.phone.length === 1) {
        vm.addContact.phone[0].type = '';
        vm.addContact.phone[0].number = '';
      } else {
        vm.addContact.phone.splice(index, 1);
      }
    };

    /**
     * Description
     * @description removing item from Email list
     * @method removeEmailList
     * @param index
     * @return 
     */
    vm.removeEmailList = function (index) {
      if (vm.addContact.emails.length === 1) {
        vm.addContact.emails[0].type = '';
        vm.addContact.emails[0].address = '';
      } else {
        vm.addContact.emails.splice(index, 1);
      }
    };
    /**
     * Description
     * @method removeAddressList
     * @param {} index
     * @return 
     */
    vm.removeAddressList = function (index) {
      if (vm.addContact.address.length === 1) {
        vm.addContact.address[0].type = '';
        vm.addContact.address[0].address.street = '';
        vm.addContact.address[0].address.city = '';
        vm.addContact.address[0].address.state = '';
        vm.addContact.address[0].address.zip = '';
        vm.addContact.address[0].address.country = '';
      } else {
        vm.addContact.address.splice(index, 1);
      }
    };

    /**
     * Description
     * @method removeRelativesList
     * @param {} index
     * @return 
     */
    vm.removeRelativesList = function (index) {
      if (vm.addContact.relatives.length === 1) {
        vm.addContact.relatives[0].type = '';
        vm.addContact.relatives[0].number = '';

      } else {
        vm.addContact.relatives.splice(index, 1);
      }
    };

    /**
     * Description
     * @method removeSocialList
     * @param {} index
     * @return 
     */
    vm.removeSocialList = function (index) {
      if (vm.addContact.social.length === 1) {
        vm.addContact.social[0].type = '';
        vm.addContact.social[0].address = '';

      } else {
        vm.addContact.social.splice(index, 1);
      }
    };

    /**
     * Description
     * @method removeIMList
     * @param {} index
     * @return 
     */
    vm.removeIMList = function (index) {
      if (vm.addContact.im.length === 1) {
        vm.addContact.im[0].type = '';
        vm.addContact.im[0].address = '';

      } else {
        vm.addContact.im.splice(index, 1);
      }
    };

    /*************************/


    //Saving Contact

    /**
     * Description
     * @method saveAddContact
     * @return 
     */
    vm.saveAddContact = function () {
      if (vm.addContact.first_name !== '' ||
        vm.addContact.middle_name !== '' ||
        vm.addContact.last_name !== '') {
        vm.addContact.date_birth = vm.date_birth;
        vm.addContact.date_anniversary = vm.date_anniv;
        vm.addContact.photo_url = vm.tmpAvatar;
        ContactService.addNewContact(vm.addContact).then(function () {
          $mdDialog.hide();
          showMessage('Contact has been saved successfully!');
          vm.loadContact();
        });
      }
      else {
        showMessage('Something went wrong. Please try again.');
      }
    };


    /*Update Contact*/
    vm.updateContactData = {};
    /**
     * Description
     * @method updateContact
     * @param {} ev
     * @return 
     */
    vm.updateContact = function (ev) {
      if (vm.contactShow.photo_url) {
        vm.tmpAvatar = vm.contactShow.photo_url;
        vm.tmpAvatar.replace('assets', '');
      }
      else {
        vm.tmpAvatar = '';
        vm.contactShow.photo_url = '';
      }
      angular.copy(vm.contactShow, vm.addContact);
      //console.log(vm.addContact);
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/contacts/dialogs/contact/contact-update-dialog.html',
        clickOutsideToClose: true,
        targetEvent: ev,
      });

    };

    /**
     * Saving updated contact
     * @method saveUpdatedContact
     * @return 
     */
    function saveUpdatedContact() {
      console.log('test update');
      if (vm.addContact.first_name !== '' || vm.addContact.middle_name !== '' || vm.addContact.last_name !== '') {
        console.log('test update inner');
        vm.addContact.date_birth = vm.date_birth;
        vm.addContact.date_anniversary = vm.date_anniv;
        vm.addContact.photo_url = vm.tmpAvatar;
        vm.addContact.isChangedAvatar = 'false';

        if (vm.addContact.photo_url.replace('assets', '') === vm.contactShow.photo_url.replace('assets', '')) {
          vm.addContact.isChangedAvatar = 'false';
        } else {
          vm.addContact.isChangedAvatar = 'true';
        }
        ContactService.updateContact(vm.addContact).success(function () {
          $mdDialog.hide();
          showMessage('Contact has been updated successfully!');
          vm.loadContact();
        }).error(function () {
          $mdDialog.hide();
          showMessage('Something went wrong. Please try again.');
        });
      }
      else {
        showMessage('Something went wrong. Please try again.');
      }
    }

    /**
     * Description
     * @method isSelected
     * @param {} item
     * @param {} list
     * @return 
     */
    function isSelected(item, list) {
      if (list && list.length) {
        return list.indexOf(item) > -1;
      }
    }

    /**
     * Change Contact List Filter
     * @method filterChange
     * @param type
     * @return 
     */
    function filterChange(type) {

      vm.listType = type;

      if (type === 'all') {
        vm.filterIds = null;
        vm.search = '';
        vm.contactShow = vm.contacts[0];
        // console.log(vm.contactShow);
        vm.loadContact();
      }
      else if (type === 'frequent') {
        vm.search = '';
        vm.filterIds = vm.user.frequentContacts;
      }
      else if (type === 'starred') {
        vm.search = '';
        vm.loadingContacts = true;
        var favoriteContact = [];

        favoriteContact = vm.contacts.filter(function (contact) {
          return contact.favorite === true;
        });
        vm.contacts = favoriteContact;
      }
      else if (type === 'group') {
        vm.search = '';
        vm.contactShow = '';
        for (var i = 0; i < vm.contacts.length; i++) {
          if (vm.contacts[i].id === vm.user.groups[0]) {
            vm.contactShow = vm.contacts[i].id;
          }
        }
      }
      else if (angular.isObject(type)) {
        vm.filterIds = type.contactIds;
      }

      vm.selectedContacts = [];
      vm.socharaContacts = [];

    }

    /**
     * Description
     * @method filterChangeNew
     * @param {} type
     * @param {} ids
     * @return 
     */
    vm.filterChangeNew = function (type, ids) {
      if (type === 'group') {
        vm.contactShow = '';
        for (var i = 0; i < vm.contacts.length; i++) {
          if (vm.contacts[i].id === ids.contactIds[0]) {
            vm.contactShow = vm.contacts[i];
          }
        }
      }
      else if (type === 'starred') {
        vm.contactShow = '';
        for (var x = 0; i < vm.contacts.length; i++) {
          if (vm.contacts[x].id === vm.starredUsers[0]) {
            vm.contactShow = vm.contacts[x];
          }
        }
      }
    };

    /**
     * Open new contact dialog
     * @method openAddContactDialog
     * @param ev
     * @param contact
     * @return 
     */
    function openAddContactDialog(ev, contact) {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/contacts/dialogs/contact/contact-add-dialog.html',
        clickOutsideToClose: true,
        targetEvent: ev,
      });
    }

    /**
     * Open new contact dialog
     * @method openContactDialog
     * @param ev
     * @param contact
     * @return 
     */
    function openContactDialog(ev, contact) {
      vm.contactShow = contact;
    }

    /**
     * Delete Contact Confirm Dialog
     * @method deleteContactConfirm
     * @param {} contact
     * @param {} ev
     * @return 
     */
    function deleteContactConfirm(contact, ev) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure want to delete the contact?')
        .htmlContent('<b>' + contact.first_name + ' ' + contact.last_name + '</b>' + ' will be deleted.')
        .ariaLabel('delete contact')
        .targetEvent(ev)
        .ok('OK')
        .cancel('CANCEL');

      $mdDialog.show(confirm).then(function () {
        deleteContact(contact.id);
        vm.selectedContacts = [];
      }, function () {

      });
    }

    /**
     * Delete Contact
     * @method deleteContact
     * @param {} id
     * @return 
     */
    function deleteContact(id) {
      ContactService.removeContact(id).success(function () {
        vm.loadContact();
        showMessage('Contact has been removed successfully!');
      }).error(function () {

      });
    }

    /**
     * Delete Selected Contactsec
     * @method deleteSelectedContacts
     * @param {} ev
     * @return 
     */
    function deleteSelectedContacts(ev) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure want to delete the selected contacts?')
        .htmlContent('<b>' + vm.selectedContacts.length + ' selected</b>' + ' will be deleted.')
        .ariaLabel('delete contacts')
        .targetEvent(ev)
        .ok('OK')
        .cancel('CANCEL');

      $mdDialog.show(confirm).then(function () {
        var contactIds = [];
        vm.selectedContacts.forEach(function (contact) {
          contactIds.push(contact.id);
        });

        ContactService.batchContactDelete(contactIds).success(function () {
          vm.loadContact();
          vm.loadGroups();
          vm.loadFavorites();
          vm.selectedContacts = [];
          showMessage('Contact has been removed successfully!');
        }).error(function () {
          showMessage('Something went wrong!');
        });
        vm.selectedContacts = [];

      });

    }

    /**
     * Toggle selected status of the contact
     * @method toggleSelectContact
     * @param contact
     * @param event
     * @return 
     */
    function toggleSelectContact(contact, event) {
      if (event) {
        event.stopPropagation();
      }

      if (vm.selectedContacts.indexOf(contact) > -1) {
        vm.selectedContacts.splice(vm.selectedContacts.indexOf(contact), 1);
      }
      else {
        vm.selectedContacts.push(contact);
      }
    }

    /**
     * Description
     * @method toggleExportSelectContact
     * @param {} contact
     * @param {} event
     * @return 
     */
    function toggleExportSelectContact(contact, event){
      if(event) event.stopPropagation();
      if (vm.selectedContactsForExport.indexOf(contact) > -1) {
        vm.selectedContactsForExport.splice(vm.selectedContactsForExport.indexOf(contact), 1);
      }
      else {
        vm.selectedContactsForExport.push(contact);
      }
    }

    /**
     * Deselect contactsec
     * @method deselectContacts
     * @return 
     */
    function deselectContacts() {
      vm.selectedContacts = [];
    }

    /**
     * Sselect all contactsec
     * @method selectAllContacts
     * @param {} key
     * @param {} value
     * @return 
     */
    function selectAllContacts(key, value) {
      // vm.selectedContacts = $scope.filteredContacts;
      // vm.selectedContacts = vm.contacts;

      vm.selectedContacts = [];

      for (var i = 0; i < vm.contacts.length; i++) {
        if (angular.isUndefined(key) && angular.isUndefined(value)) {
          vm.selectedContacts.push(vm.contacts[i]);
          continue;
        }

        if (angular.isDefined(key) && angular.isDefined(value) && vm.contacts[i][key] === value) {
          vm.selectedContacts.push(vm.contacts[i]);
        }
      }
    }

    /**
     * Toggle select threads
     * @method toggleSelectContacts
     * @return 
     */
    function toggleSelectContacts() {
      if (vm.selectedContacts.length > 1) {
        vm.deselectContacts();
      }
      else {
        vm.selectAllContacts();
      }
    }

    /**
     * Description
     * @method addNewGroup
     * @return 
     */
    function addNewGroup() {
      if (vm.newGroupName === '') {
        showMessage('Please give your group a name!');
        return;
      }
      var groupContactsId = [];
      //console.log(vm.asyncContacts)
      if (vm.asyncContacts.length) {
        vm.asyncContacts.forEach(function (t) {
          groupContactsId.push(t.id);
        });
      }
      ContactService.newGroup(vm.newGroupName, groupContactsId).then(function (resp) {
        if (resp) {
          vm.loadGroups();
          vm.closeDialog();
          showMessage('New group has been created successfully!');
        }
      });
      vm.newGroupName = '';
    }

    /**
     * Description
     * @method updateGroupFromList
     * @return 
     */
    function updateGroupFromList() {

      if (vm.editGroupData.name === '') {
        showMessage('Give your group a name!');
        return;
      }
      var groupContactsId = [];
      //console.log(vm.asyncContacts);
      if (vm.asyncContacts.length) {
        vm.asyncContacts.forEach(function (t) {
          groupContactsId.push(t.id);
        });
      }

      ContactService.updateGroupFromList(vm.editGroupData.id, vm.editGroupData.name, groupContactsId).success(function () {
        vm.loadGroups();
        vm.closeDialog();
        showMessage('Group data has been updated successfully!');
      }).error(function () {
        showMessage('Something went wrong. Try again!');
      });
      vm.newGroupName = '';
    }

    /**
     * Delete Group
     * @method deleteGroup
     * @param {} ev
     * @return 
     */
    function deleteGroup(ev) {
      var group = vm.listType;

      var confirm = $mdDialog.confirm()
        .title('Are you sure want to delete the group?')
        .htmlContent('<b>' + group.name + '</b>' + ' will be deleted.')
        .ariaLabel('delete group')
        .targetEvent(ev)
        .ok('OK')
        .cancel('CANCEL');

      $mdDialog.show(confirm).then(function () {
        ContactService.removeGroup(group.id).success(function () {
          vm.loadGroups();
          showMessage('Contact has been updated successfully!');
        }).error(function () {
          showMessage('Something went wrong. Try again!');
        });
        filterChange('all');
      });

    }

    /**
     * Description
     * @method editGroupName
     * @param {} group
     * @return 
     */
    vm.editGroupName = function (group) {
      if (group.name === '') {
        return;
      }
      ContactService.updateGroupName(group.id, group.name).success(function () {
        vm.loadGroups();
        showMessage('Group Name has been updated successfully!');
      }).error(function () {
        showMessage('Something went wrong. Try again!');
      });
    };

    /**
     * Toggle sidenav
     * @method toggleSidenav
     * @param sidenavId
     * @return 
     */
    function toggleSidenav(sidenavId) {
      $mdSidenav(sidenavId).toggle();
    }

    /*Cropped Image*/
    vm.myImage = '';
    vm.myCroppedImage = '';

    /**
     * Description
     * @method handleFileSelect
     * @param {} evt
     * @return 
     */
    var handleFileSelect = function (evt) {
      var file = angular.element(document.querySelector('#fileInput'))[0].files[0];
      if (file.type === 'image/jpeg' || file.type === 'image/png') {
        vm.uploadButton = true;
        var reader = new FileReader();
        /**
         * Description
         * @method onload
         * @param {} evt
         * @return 
         */
        reader.onload = function (evt) {
          $scope.$apply(function ($scope) {
            vm.myImage = evt.target.result;
          });
        };
        reader.readAsDataURL(file);
      }
      else {
        vm.myCroppedImage = '';
      }
    };
    vm.tmpAvatar = '';
    vm.showAvatarImage = handleFileSelect;
    /**
     * Description
     * @method saveAvatar
     * @return 
     */
    vm.saveAvatar = function () {
      if (vm.myCroppedImage === '') {
        showMessage('No Image found!');
      }
      else if (vm.myCroppedImage !== '') {
        // angular.copy(vm.myCroppedImage, vm.tmpAvatar);
        vm.tmpAvatar = vm.myCroppedImage;
        angular.element.modal.close();
      }
    };

    /**
     * Start Gogole Contacts Import API Ingegration
     * @method authgoogle
     * @return 
     */
    function authgoogle() {
      gapi.load('client:auth2', initGoogleClient);
    }

    /**
     * Description
     * @method initGoogleClient
     * @return 
     */
    function initGoogleClient() {
      gapi.auth2.authorize({
        client_id: '224735986645-jdri0gpds1a7esr419e6iv1u25e13t46.apps.googleusercontent.com',
        scope: 'https://www.googleapis.com/auth/contacts.readonly',
        response_type: 'id_token permission'
      }, function(response) {
        if (response.error) {
          console.log(response.error);
        }
        // The user authorized the application for the scopes requested.
        var accessToken = response.access_token;
        fetchGoogleApiData(accessToken);
      });
    }

    vm.selectedGoogleImportContact = [];
    vm.googleContactImportToggle = googleContactImportToggle;
    vm.googleContactImportSelectionExists = googleContactImportSelectionExists;
    vm.importSelectedGoogleContact = importSelectedGoogleContact;
    /**
     * Description
     * @method googleContactImportSelectionExists
     * @param {} item
     * @param {} list
     * @return BinaryExpression
     */
    function googleContactImportSelectionExists( item, list ){
      return list.indexOf(item) > -1;
    }

    /**
     * Description
     * @method googleContactImportToggle
     * @param {} item
     * @param {} list
     * @return 
     */
    function googleContactImportToggle( item, list ){
      var idx = list.indexOf(item);
      if (idx > -1) {
        list.splice(idx, 1);
      }
      else {
        list.push(item);
      }
    }

    /**
     * Description
     * @method importSelectedGoogleContact
     * @return 
     */
    function importSelectedGoogleContact(){
      closeDialog();
      ContactService.importSelectedGoogleContact({contacts: vm.selectedGoogleImportContact}).then(function (respImport) {
        if(respImport){
          showMessage('The selected contacts were imported');
          loadContact();
        }
      });
    }

    /**
     * Description
     * @method showDialogue
     * @param {} templateUrl
     * @param {} clickOutsideToClose
     * @return 
     */
    function showDialogue(templateUrl, clickOutsideToClose) {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: templateUrl,
        clickOutsideToClose: clickOutsideToClose || false
      });
    }

    /**
     * Description
     * @method fetchGoogleApiData
     * @param {} token
     * @return 
     */
    function fetchGoogleApiData(token) {
      ContactService.fetchGoogleApiData({token: JSON.stringify(token)}).then(function (respApi) {
        vm.importedGoogleContact =respApi;
        console.log(vm.importedGoogleContact);
        vm.dtOptions = {
          dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
          pagingType: 'simple',
          autoWidth: false,
          responsive: true
        };

        vm.loadContact();//hit the function
        showDialogue('app/main/contacts/dialogs/contact/contacts_imported_google.html');
      });

    }

    /**
     * End Gogole Contacts Import API Ingegration
     * @method exportContactSubmit
     * @return 
     */
    function exportContactSubmit() {
      var contacts = [];
      var type = vm.exportContacts;
      if (type === 'all') {
        contacts = vm.contacts;
      } else if (type === 'favorites') {
        contacts = vm.contacts.filter(function (contact) {
          return contact.favorite;
        });
      }
      else if (type === 'selected') {
        contacts = vm.selectedContactsForExport;
      }
      else if (type === 'group') {
        //console.log(vm.selectedGroupForExport.contactIds)
        contacts = vm.contacts.filter(function (contact) {
          return vm.selectedGroupForExport.contactIds.indexOf(contact.id) !== -1;
        });
      }

      ContactService.exportContacts({contacts: contacts, format: vm.selectedExportType}).then(function (resp) {
        if(resp){
          var downloadUrl = vm.siteUrl + resp.replace('assets', '');
          downloadFile(downloadUrl);
        }
      });
    }

    /**
     * Description
     * @method downloadFile
     * @param {} fileUrl
     * @return 
     */
    function downloadFile( fileUrl ){
      var element = document.createElement('a');
      element.setAttribute('href', fileUrl);
      element.setAttribute('download', fileUrl.substring(fileUrl.lastIndexOf('/')+1));
      element.setAttribute('target', '_blank');
      element.style.display = 'none';
      document.body.appendChild(element);
      element.click();
      document.body.removeChild(element);
    }

    /**
     * Description
     * @method openAddGroupPopup
     * @return 
     */
    vm.openAddGroupPopup = function () {
      vm.loadContactsAddGroup([]);
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/contacts/dialogs/contact/add_new_group.html',
        clickOutsideToClose: true,
        // targetEvent: ev,
      });
    };
    /**
     * Description
     * @method openManageGroupPopup
     * @return 
     */
    vm.openManageGroupPopup = function () {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/contacts/dialogs/contact/manage_groups.html',
        clickOutsideToClose: true,
        // targetEvent: ev,
      });
    };
    /**
     * Description
     * @method changeIsShownGroup
     * @param {} id
     * @param {} trueOrFalse
     * @return 
     */
    vm.changeIsShownGroup = function (id, trueOrFalse) {
      ContactService.changeIsShownGroup(id, trueOrFalse).success(function () {
        showMessage('Group Data updated successfully!');
      }).error(function () {
        showMessage('Something went wrong!');
      });
    };
    /**
     * Description
     * @method removeGroupFromList
     * @param {} ev
     * @param {} id
     * @param {} groupName
     * @return 
     */
    vm.removeGroupFromList = function (ev, id, groupName) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure want to delete the group?')
        .htmlContent('<b>' + groupName + '</b>' + ' will be deleted.')
        .ariaLabel('delete group')
        .targetEvent(ev)
        .ok('OK')
        .cancel('CANCEL');

      $mdDialog.show(confirm).then(function () {

        ContactService.removeGroup(id).success(function () {
          vm.loadGroups();
          showMessage('group has been removed successfully!');
        }).error(function () {
          showMessage('Something went wrong. Try again!');
        });

      }, function () {
        vm.openManageGroupPopup();
      });
    };
    /**
     * Description
     * @method editGroupFromList
     * @param {} group
     * @return 
     */
    vm.editGroupFromList = function (group) {
      vm.editGroupData = group;
      var groupContacts = [];
      if (group.contactIds.length) {

        var contacts = vm.contacts.filter(function (contact) {
          return group.contactIds.indexOf(contact.id) !== -1;
        });
        if (contacts.length) {
          contacts.forEach(function (t) {
            groupContacts.push({
              name: t.first_name + ' ' + t.middle_name + ' ' + t.last_name,
              email: t.email,
              image: t.photo_url ? t.photo_url.replace('assets', '').replace(/\\/, '/') : '../images/avatars/blank.jpg',
              id: t.id,
              _lowername: angular.lowercase(t.first_name + ' ' + t.middle_name + ' ' + t.last_name)
            });
          });
        }
        //console.log(groupContacts);
      }

      vm.loadContactsAddGroup(groupContacts);
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/contacts/dialogs/contact/edit_group.html',

        clickOutsideToClose: true,
        // targetEvent: ev,
      });
    };
    /**
     * Description
     * @method loadContactsAddGroup
     * @param {} groupContacts
     * @return 
     */
    vm.loadContactsAddGroup = function (groupContacts) {
      // var vm = this;
      var pendingSearch, cancelSearch = angular.noop;
      var cachedQuery, lastSearch;
      vm.allContacts = loadContacts();
      vm.contactsForGroup = [vm.allContacts[0]];
      vm.asyncContacts = groupContacts;
      // //console.log(vm.asyncContacts);
      vm.filterSelected = true;
      vm.querySearch = querySearch;
      vm.delayedQuerySearch = delayedQuerySearch;

      /**
       * Search for contacts; use a random delay to simulate a remote call
       * @method querySearch
       * @param {} criteria
       * @return ConditionalExpression
       */
      function querySearch(criteria) {
        cachedQuery = cachedQuery || criteria;
        return cachedQuery ? vm.allContacts.filter(createFilterFor(cachedQuery)) : vm.allContacts.filter(createFilterFor(''));
      }

      /**
       * Async search for contacts
       * Also debounce the queries; since the md-contact-chips does not support this
       * @method delayedQuerySearch
       * @param {} criteria
       * @return pendingSearch
       */
      function delayedQuerySearch(criteria) {
        cachedQuery = criteria;
        if (!pendingSearch || !debounceSearch()) {
          cancelSearch();
          return pendingSearch = $q(function (resolve, reject) {
            // Simulate async search... (after debouncing)
            cancelSearch = reject;
            $timeout(function () {
              resolve(vm.querySearch());
              refreshDebounce();
            }, Math.random() * 500, true)
          });
        }
        return pendingSearch;
      }

      /**
       * Description
       * @method refreshDebounce
       * @return 
       */
      function refreshDebounce() {
        lastSearch = 0;
        pendingSearch = null;
        cancelSearch = angular.noop;
      }

      /**
       * Debounce if querying faster than 300ms
       * @method debounceSearch
       * @return BinaryExpression
       */
      function debounceSearch() {
        var now = new Date().getMilliseconds();
        lastSearch = lastSearch || now;
        return ((now - lastSearch) < 300);
      }

      /**
       * Create filter function for a query string
       * @method createFilterFor
       * @param {} query
       * @return FunctionExpression
       */
      function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(contact) {
          return (contact._lowername.indexOf(lowercaseQuery) != -1);
        };
      }

      /**
       * Description
       * @method loadContacts
       * @return CallExpression
       */
      function loadContacts() {
        var contacts = vm.contacts;
        // //console.log(contacts);
        return contacts.map(function (c) {
          // var cParts = c.split(' ');
          var contact = {
            name: c.first_name + ' ' + c.last_name,
            image: c.photo_url ? c.photo_url.replace('assets', '').replace(/\\/, '/') : '../images/avatars/blank.jpg',
            id: c.id
          };
          contact._lowername = contact.name.toLowerCase();
          return contact;
        });
      }
    };
    /**
     * Description
     * @method socialContactURL
     * @param {} type
     * @return 
     */
    vm.socialContactURL = function (type) {
      if (type === 'facebook') {
        return 'https://www.facebook.com/';
      }
      if (type === 'twitter') {
        return 'https://www.twitter.com/';
      }
      if (type === 'skype') {
        return 'skype:';
      }
      if (type === 'linkedin') {
        return 'https://www.linkedin.com/in/';
      }
      if (type === 'other') {
        return 'https://www.google.com/search?q=';
      }
    };

    /**
     * Description
     * @method imURL
     * @param {} type
     * @param {} value
     * @return 
     */
    vm.imURL = function (type, value) {
      if (type === 'skype') {
        return 'skype:' + value;
      }
      if (type === 'facebook') {
        return 'https://www.facebook.com/messages/t/' + value;
      }
      if (type === 'facebook') {
        return 'https://www.facebook.com/messages/t/' + value;
      }
      if (type === 'aim') {
        return 'aim:GoIm?screenname=' + value;
      }
      if (type === 'google') {
        return 'https://hangouts.google.com/';
      }
      if (type === 'yahoo') {
        return 'ymsgr:sendim?' + value;
      }
    };

    //redirecting controller
    /**
     * Description
     * @method redirectToChat
     * @param {} id
     * @return 
     */
    vm.redirectToChat = function (id) {
      MailService.getUserInfoById(id).then(function (response) {
        if (response) {
          $state.go('app.chat.type.id', {type: 'u', typeId: id});
        } else {
          showMessage('Seems, You are trying to chat with user who is out of Sochara.');
        }
      });
    };
    /**
     * Description
     * @method redirectToEmail
     * @param {} contactInfo
     * @return 
     */
    vm.redirectToEmail = function (contactInfo) {
      var toEmail = {};
      toEmail.type = 'global';
      toEmail.to = [];
      MailService.getMailAddresses().then(function (response) {
        if (response && response.length) {
          vm.accounts = $rootScope.mailAccounts = response;
          if (contactInfo && contactInfo.email) {
            var emailInfo = {};
            emailInfo.email = contactInfo.email;
            emailInfo.name = contactInfo.first_name || 'Name';
            toEmail.to.push(emailInfo);
            showComposeDialog(vm.accounts, toEmail);
          } else {
            showMessage('Did not found any email address, are you sure that you have clicked for correct contact info.');
          }
        } else {
          showMessage('Seems, did not integrade any email Account !');
        }
      });
    };

    /**
     * Description
     * @method showMessage
     * @param {} message
     * @return 
     */
    function showMessage(message) {
      $mdToast.show({
        template: '<md-toast class="md-toast success">' + message + '</md-toast>',
        hideDelay: 3000,
        position: 'bottom left'
      });
    }

    /**
     * Description
     * @method showComposeDialog
     * @param {} accounts
     * @param {} to
     * @return 
     */
    function showComposeDialog(accounts, to) {
      $mdDialog.show({
        controller: 'ComposeDialogController',
        controllerAs: 'vm',
        locals: {
          selectedMail: to,
          emailAccounts: accounts,
          selectedAccount: accounts[0]
        },
        templateUrl: 'app/main/mail/dialogs/compose/compose-dialog.html',
        parent: angular.element($document.body),
        clickOutsideToClose: false
      });
    }
  }

})();
