(function () {
  'use strict';

  angular
    .module('app.contacts',
      [
        // 3rd Party Dependencies
        // 'xeditable'
      ]
    )
    .config(config);

  /** @ngInject */
  function config($stateProvider) {

    $stateProvider.state('app.contacts', {
      url: '/contacts',
      views: {
        'content@app': {
          templateUrl: 'app/main/contacts/contacts.html',
          controller: 'ContactsController as vm'
        }
      }
    });

  }

})();
