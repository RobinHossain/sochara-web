(function () {
  'use strict';

  angular
    .module('app.pages', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {

    // var token = localStorage.getItem('ats');
    // if ( token && token.length ) {
    //   return;
    // }

    // State
    // $stateProvider.state('app.pages', {
    //   url: '/page/google-privacy-policy',
    //   views: {
    //     'main@': {
    //       templateUrl: 'app/core/layouts/content-only.html',
    //       controller: 'MainController as vm'
    //     },
    //     'content@app.privacypolicy-facebook': {
    //       templateUrl: 'app/main/auth/privacypolicy-facebook/privacypolicy.html',
    //       controller: 'PrivacyPolicyControllerFacebook as vm'
    //     }
    //   },
    //   bodyClass: 'login'
    // });

    $stateProvider.state('app.pages', {
      url: '/page',
      bodyClass: 'login',
      views: {
        'main@': {
          templateUrl: 'app/core/layouts/content-only.html',
          controller: 'MainController as vm'
        },
        'content@app.pages': {
          templateUrl: 'app/main/pages/main.html',
          controller: 'pageController as vm'
        }
      }
    }).state('app.pages.page', {
      url: '/:pageId'
    });

  }

})();
