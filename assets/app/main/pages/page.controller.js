(function () {
  'use strict';

  angular
    .module('app.pages')
    .controller('pageController', PageController);

  /** @ngInject */
  function PageController( $state ) {
    var vm = this;

    init();

    function init(){
      var pageId = $state.params.pageId;
      vm.page = pageId;
      console.log(pageId);
    }
    // vm.page = 'google-privacy-policy';
  }

})();
