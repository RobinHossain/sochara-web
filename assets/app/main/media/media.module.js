(function () {
  'use strict';

  angular
    .module('app.media',
      [
        'app.auth.login',
        'hmTouchevents',
        'sochara.lightbox'
      ]
    )
    .config(config);

  /** @ngInject */
  function config($stateProvider) {

    // State
    $stateProvider
      .state('app.media', {
        url: '/media',
        views: {
          'content@app': {
            templateUrl: 'app/main/media/media.html',
            controller: 'MediaController as vm'
          }
        },
        bodyClass: 'media'
      })
      .state('app.media.type', {
        url: '/:type',
        bodyClass: 'media'
      })
      .state('app.media.type.album', {
        url: '/:albumId',
        bodyClass: 'media'
      })
      .state('app.media.type.album.editor', {
        url: '/:photoId',
        views: {
          'content@app': {
            templateUrl: 'app/main/media/editor.html',
            controller: 'MediaController as vm'
          }
        },
        bodyClass: 'media'
      });
    // .state('app.media.type.album.editor', {
    //   url: '/:photoId',
    //   views: {
    //     'content@app': {
    //       templateUrl: 'app/main/media/editor.html',
    //       controller: 'MediaController as vm'
    //     }
    //   },
    //   bodyClass: 'media'
    // })
    // .state('app.media.type.album.editor.id', {
    //   url: '/:album',
    //   views: {
    //     'content@app': {
    //       templateUrl: 'app/main/media/editor.html',
    //       controller: 'MediaController as vm'
    //     }
    //   },
    //   bodyClass: 'media'
    // });

  }
})();
