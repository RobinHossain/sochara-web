(function () {
  'use strict';

  angular
    .module('app.media')
    .controller('MediaController', mediaController);

  /**
   * Description
   * @ngInject 
   * @method mediaController
   * @param {} $q
   * @param {} $rootScope
   * @param {} MediaService
   * @param {} $mdDialog
   * @param {} ContactService
   * @param {} DashboardService
   * @param {} $timeout
   * @param {} ChatsService
   * @param {} $document
   * @param {} $window
   * @param {} Upload
   * @param {} $scope
   * @param {} $state
   * @param {} $mdToast
   * @param {} $location
   * @param {} socharaLightbox
   * @return 
   */
  function mediaController($q, $rootScope, MediaService, $mdDialog, ContactService, DashboardService, $timeout, ChatsService, $document, $window, Upload, $scope, $state, $mdToast, $location, socharaLightbox) {
    var vm = this;
    vm.site_url = $location.protocol() + '://' + $location.host() + ':' + $location.port();
    vm.tiles = [{tile: "one"}, {tile: "two"}];
    vm.selectedPhotos = [];
    vm.uploadMedia = uploadMedia;
    vm.currentAlbumId = "";
    vm.init = init;

    vm.closeDialog = closeDialog;
    vm.goToAlbums = goToAlbums;
    vm.getThumbFromPhoto = getThumbFromPhoto;
    vm.toggleSelect = toggleSelect;
    vm.existsPhoto = existsPhoto;
    vm.imagePreview = imagePreview;
    vm.videoPreview = videoPreview;
    vm.deleteItems = deleteItems;
    vm.deleteItemsConfirm = deleteItemsConfirm;
    vm.deletePhoto = deletePhoto;
    vm.getAllVideos = getAllVideos;
    vm.getAllPhotos = getAllPhotos;
    vm.printThisPhoto = printThisPhoto;
    vm.sharePhotoTwitter = sharePhotoTwitter;
    vm.sharePhotoLinkedin = sharePhotoLinkedin;
    vm.sharePhotoGoogle = sharePhotoGoogle;
    vm.sharePhotoFacebook = sharePhotoFacebook;
    vm.shareAlbumFacebook = shareAlbumFacebook;
    vm.shareAlbumTwitter = shareAlbumTwitter;
    vm.sharePhotoPinterest = sharePhotoPinterest;
    vm.closeVideoPopup = closeVideoPopup;
    vm.getAlbums = getAlbums;
    vm.checkAlbumOrType = checkAlbumOrType;
    vm.createNewAlbum = createNewAlbum;
    vm.newAlbumSubmission = newAlbumSubmission;
    vm.goToAlbum = goToAlbum;
    vm.downloadALbum = downloadALbum;
    vm.renameAlbum = renameAlbum;
    vm.deleteAlbum = deleteAlbum;
    vm.deleteAlbumConfirm = deleteAlbumConfirm;
    vm.shareMedia = shareMedia;
    vm.shareMediaSubmission = shareMediaSubmission;
    vm.renameAlbumConfirm = renameAlbumConfirm;
    vm.getThumbCover = getThumbCover;
    vm.sharedFileFolderList = sharedFileFolderList;
    vm.editPhoto = editPhoto;
    vm.getPhotoAlbumListByLocation = getPhotoAlbumListByLocation;
    vm.copySelectedItems = copySelectedItems;
    vm.cutSelectedItems = cutSelectedItems;
    vm.pasteSelectedItems = pasteSelectedItems;
    vm.addToStar = addToStar;
    vm.removeFromStar = removeFromStar;
    vm.renameItem = renameItem;
    vm.renameMediaConfirm = renameMediaConfirm;

    vm.albumRoute = true;
    vm.photoForDelete = '';
    vm.photos = [];
    vm.videos = [];
    vm.albums = [];
    vm.locations = [];
    vm.locationPhotos = [];
    vm.path = [{name: "Main", uri: "app.media.type", param: "photos"}];


    /**
     * Description
     * @method init
     * @return 
     */
    function init() {
      $rootScope.searchable = false;
      var itemType = $state.params.type;
      var albumId = $state.params.albumId;

      getSocialAccount();

      $timeout(function () {
        if(itemType){
          switch (itemType){
            case 'videos':
              vm.albumRoute = false;
              getVideos();
              break;
            case 'photos':
              vm.albumRoute = false;
              getPhotos();
              break;
            case 'album':
              vm.albumRoute = true;
              albumId ? goToAlbum(albumId) : getAlbums();
              break;
            case 'location':
              getPhotoAlbumListByLocation();
              break;
            case 'shared':
              sharedFileFolderList();
              break;
            default:
              $state.go('app.media.type', {type: 'album'});
              getAlbums();
          }

        }else{
          $state.go('app.media.type', {type: 'album'});
          getAlbums();
        }

      });

      $timeout(function() {
        var fullUrl = $location.url();
        var module = fullUrl.substring(1).split('/', 1)[0];
        if( module === 'media' ){
          loadExtarnalJavascript();
        }
      }, 1000);
    }



    /**
     * Description
     * @method loadExtarnalJavascript
     * @return 
     */
    function loadExtarnalJavascript(){
      $timeout(function () {
        var tag = document.createElement("script");
        tag.src = "/js/dependencies/media/react.min.js";
        document.getElementsByTagName("head")[0].appendChild(tag);

        tag = document.createElement("script");
        tag.src = "/js/dependencies/media/react-dom.min.js";
        document.getElementsByTagName("head")[0].appendChild(tag);

        tag = document.createElement("script");
        tag.src = "/js/dependencies/media/PhotoEditorSDK.js";
        document.getElementsByTagName("head")[0].appendChild(tag);

        tag = document.createElement("script");
        tag.src = "/js/dependencies/media/PhotoEditorReactUI.js";
        document.getElementsByTagName("head")[0].appendChild(tag);
      }, 1000);
    }

    /**
     * Description
     * @method addToStar
     * @return 
     */
    function addToStar() {
      var selectedPhotoIds = findIdsFromArray(vm.selectedPhotos);
      MediaService.addToStar({photos: selectedPhotoIds}).then(function ( respStared ) {
        showMessage('Items are added as your favorite.');
      });
    }

    /**
     * Description
     * @method removeFromStar
     * @return 
     */
    function removeFromStar() {
      var selectedPhotoIds = findIdsFromArray(vm.selectedPhotos);
      MediaService.removeFromStar({photos: selectedPhotoIds}).then(function ( respStared ) {
        showMessage('Items are removed from your favorite.')
      });
    }

    /**
     * Description
     * @method renameItem
     * @return 
     */
    function renameItem(){
      vm.mediaNameForRename = vm.selectedPhotos[0] ? vm.selectedPhotos[0].name : '';
      showDialogue('app/main/media/dialogs/rename.html', false);
    }

    /**
     * Description
     * @method renameMediaConfirm
     * @return 
     */
    function renameMediaConfirm(){
      var name = vm.mediaNameForRename || '';
      var selectedIds = findIdsFromArray(vm.selectedPhotos);
      var id = selectedIds.length ? selectedIds[0] : '';
      if( name && id ){
        MediaService.renameMedia({name: name, id: id}).then(function ( respRename ) {
          if( respRename && respRename.length ){
            reloadMedia();
            showMessage('File has been renamed.');
          }
        });
      }
      closeDialog();
    }


    /**
     * Description
     * @method findIdsFromArray
     * @param {} arr
     * @return CallExpression
     */
    function findIdsFromArray( arr ){
      return arr.map(function (val) {
        return val.id;
      });
    }

    /**
     * Description
     * @method editPhoto
     * @param {} photo
     * @return 
     */
    function editPhoto(photo) {
      $rootScope.loadingProgress = true;
      // loadDependancies();
      vm.photoEditAlbum = $state.params.albumId || '';
      vm.editImageUrl = vm.site_url + photo.file_dir;
      // console.log(vm.photoEditAlbum);
      if (vm.photoEditAlbum) {
        $state.go('app.media.type.album.editor', {type: 'photos', albumId: vm.photoEditAlbum, photoId: photo.id});
      } else {
        $state.go('app.media.type.album.editor', {albumId: 0, photoId: photo.id});
      }
    }

    /**
     * Description
     * @method sharedFileFolderList
     * @return 
     */
    function sharedFileFolderList() {
      vm.navMenu = 'shared';
      $state.go('app.media.type', {type: 'shared'});
      $rootScope.loadingProgress = true;
      vm.albumRoute = false;
      getSharedItemList();
    }

    /**
     * Description
     * @method getSharedItemList
     * @param {} album
     * @return 
     */
    function getSharedItemList(album) {
      MediaService.getSharedItemList(album).then(function (response) {
        $rootScope.loadingProgress = false;
        vm.photos = [];
        vm.albums = [];
        vm.locations = [];
        if (response) {
          vm.albums = response.album || [];
          vm.photos = response.photos || [];
        }
      });
    }

    /**
     * Description
     * @method getPhotoAlbumListByLocation
     * @return 
     */
    function getPhotoAlbumListByLocation() {
      vm.navMenu = 'location';
      $rootScope.loadingProgress = true;
      vm.albumRoute = false;
      $state.go('app.media.type', {type: 'location'});
      MediaService.getPhotoAlbumListByLocation().then(function (response) {
        vm.photos = [];
        vm.videos = [];
        vm.albums = [];
        if( Object.prototype.toString.call(response) === '[object Object]'){
          $rootScope.loadingProgress = false;
          vm.locations = response.locations;
          vm.locationPhotos = response.items;
        }else{
          showMessage('Can not found any data');
        }
      });
    }


    /**
     * Description
     * @method toggleSelect
     * @param {} photo
     * @return 
     */
    function toggleSelect(photo) {
      var idx = vm.selectedPhotos.indexOf(photo);
      if (idx > -1) {
        vm.selectedPhotos.splice(idx, 1);
      }
      else {
        vm.selectedPhotos.push(photo);
      }
    }

    /**
     * Description
     * @method existsPhoto
     * @param {} photo
     * @return BinaryExpression
     */
    function existsPhoto(photo) {
      return vm.selectedPhotos.indexOf(photo) > -1;
    }

    // Watch To Go As Route Parameter
    $scope.$watchCollection(function () {
      return $state.params;
    }, function () {
      var type = $state.params.type || '';
      if (type) {
        if (type === 'videos') {
          vm.albumRoute = false;
          getAllVideos();
        } else if (type === 'photos') {
          vm.albumRoute = false;
          if ($state.params.photoId) {
            $state.go('app.media.type.album.editor', {album: 0, photoId: $state.params.photoId});
          } else {
            getAllPhotos();
          }
        } else if (type === 'album') {
          vm.albumRoute = true;
          if ($state.params.photoId) {
            $state.go('app.media.type.album.editor', {album: 0, photoId: $state.params.photoId});
          } else {
            if ($state.params.albumId) {
              vm.albumRoute = false;
              $state.go('app.media.type.album', {albumId: $state.params.albumId, type: 'album'});
              vm.videos = [];
              vm.photos = [];
              getAlbumItems($state.params.albumId);
            } else {
              goToAlbums();
            }
          }
        }else if(type === 'location'){
          getPhotoAlbumListByLocation();
        }
      }
    });

    /**
     * Description
     * @method isCheckedPhoto
     * @return BinaryExpression
     */
    vm.isCheckedPhoto = function () {
      var totalItems = vm.photos.length + vm.videos.length;
      return vm.selectedPhotos.length === totalItems;
    };

    /**
     * Description
     * @method isIndeterminate
     * @return LogicalExpression
     */
    vm.isIndeterminate = function () {
      var totalItemLength = vm.photos.length + vm.videos.length;
      return (vm.selectedPhotos.length !== 0 &&
        vm.selectedPhotos.length !== totalItemLength);
    };

    /**
     * Description
     * @method toggleAllPhotos
     * @return 
     */
    vm.toggleAllPhotos = function () {
      var totalItems = vm.photos.length + vm.videos.length;
      if (vm.selectedPhotos.length === totalItems) {
        vm.selectedPhotos = [];
      } else if (vm.selectedPhotos.length === 0 || vm.selectedPhotos.length > 0) {
        var selectedItem = vm.photos.slice(0);
        vm.selectedPhotos = selectedItem.concat(vm.videos.slice(0));
      }
    };

    /**
     * Description
     * @method imagePreview
     * @param {} photo
     * @param {} photos
     * @return 
     */
    function imagePreview(photo, photos) {
      $rootScope.loadingProgress = true;
      if (photo) {
        var imageList = photos || vm.photos;
        var fileUrl = vm.site_url + photo.file_dir;
        var lightBoxOption = {};
        makeImageLinkArray(imageList, function (listData) {
          $rootScope.loadingProgress = false;
          if (listData && listData.length) {
            var fileUrlPosition = listData.indexOf(fileUrl);
            if (fileUrlPosition > -1) {
              lightBoxOption.initialIndex = fileUrlPosition;
              socharaLightbox.show(listData, lightBoxOption);
            } else {
              showMessage('Preview Not Available For This File');
            }
          } else {
            showMessage('No Image File Found');
          }
        });
      } else {
        showMessage('Invalid File');
      }
    }

    /**
     * Description
     * @method goToAlbums
     * @return 
     */
    function goToAlbums() {
      vm.selectedPhotos = [];
      vm.loadingAlbums = true;
      $rootScope.loadingProgress = true;
      vm.photos = [];
      vm.videos = [];
      vm.locations = [];
      vm.albumRoute = true;
      getAlbums();
      getPhotos();
    }

    /**
     * Description
     * @method createNewAlbum
     * @param {} ev
     * @return 
     */
    function createNewAlbum(ev) {
      vm.newAlbumName = null;
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/media/dialogs/new-album.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method newAlbumSubmission
     * @return 
     */
    function newAlbumSubmission() {
      var name = vm.newAlbumName || '';
      var albumID = $state.params.albumId || '';
      closeDialog();
      if (!name) {
        return;
      }
      $rootScope.loadingProgress = true;
      MediaService.createNewAlbum(name, albumID).then(function (response) {
        if (response) {
          vm.albums.push(response);
        }
        $rootScope.loadingProgress = false;
      });
    }

    /**
     * Description
     * @method downloadALbum
     * @param {} album
     * @return 
     */
    function downloadALbum(album) {
      MediaService.downloadAlbum({id: album.id}).then(function (response) {
        if( response && typeof response === 'string' ){
          $window.open(vm.site_url + response, '_blank');
        }else {
          showMessage('Can not make the album archive.');
        }
      });
    }

    /**
     * Description
     * @method renameAlbum
     * @param {} album
     * @return 
     */
    function renameAlbum(album) {
      vm.albumIdForRename = album.id;
      vm.previousAlbumName = album.name;
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/media/dialogs/rename-album.html',
        parent: angular.element($document.body),
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method renameAlbumConfirm
     * @return 
     */
    function renameAlbumConfirm() {
      $rootScope.loadingProgress = true;
      MediaService.renameAlbum({album: vm.albumIdForRename, name: vm.previousAlbumName}).then(function (response) {
        if (response) {
          vm.albums.getById(vm.albumIdForRename).name = response.name;
          vm.albumIdForRename = null;
          vm.previousAlbumName = null;
        }
        closeDialog();
        $rootScope.loadingProgress = false;
      });
    }


    /**
     * Description
     * @method deleteAlbum
     * @param {} album
     * @return 
     */
    function deleteAlbum(album) {
      vm.albumIdForDelete = album.id;
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/media/dialogs/delete-album.html',
        parent: angular.element($document.body),
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method deleteAlbumConfirm
     * @return 
     */
    function deleteAlbumConfirm() {
      $rootScope.loadingProgress = true;
      MediaService.deleteAlbum(vm.albumIdForDelete).then(function (response) {
        if (response) {
          // var albumIndex = vm.albums.findIndex(x => x.id == vm.albumIdForDelete);
          var albumIndex = vm.albums.findIndex(function (x) {
            return x.id === vm.albumIdForDelete;
          });

          if (albumIndex > -1) {
            vm.albums.splice(albumIndex, 1);
          }
          vm.albumIdForDelete = null;
        }
        closeDialog();
        $rootScope.loadingProgress = false;
        getAlbums();
      });
    }

    /**
     * Description
     * @method shareMedia
     * @param {} media
     * @param {} type
     * @return 
     */
    function shareMedia( media, type) {
      if(!(media && type)){
        return ;
      }
      loadContacts();
      vm.sharaData = {id: media.id, type: type};
      getPreviousSharedUsers(vm.sharaData);
      vm.customKeys = [];
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/media/dialogs/share.html',
        parent: angular.element($document.body),
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method shareMediaSubmission
     * @return 
     */
    function shareMediaSubmission() {
      if( typeof vm.sharaData !=='object' ){
        return;
      }
      $rootScope.loadingProgress = true;
      var selectedContacts = vm.selectedContacts.map(function (contact) {
        return contact.id;
      });
      var shareData = {users: selectedContacts, type: vm.sharaData.type || '', id: vm.sharaData.id || ''};
      MediaService.itemsShareWithUsers(shareData).then(function (response) {
        if (response) {
          showMessage("Media share updated !");
          $rootScope.loadingProgress = false;
          vm.sharaData = '';
        }
        closeDialog();
      });
    }

    /**
     * Description
     * @method getPreviousSharedUsers
     * @param {} data
     * @return 
     */
    function getPreviousSharedUsers(data) {
      MediaService.getPreviousSharedUsers(data).then(function (response) {
        vm.selectedContacts = response;
      });
    }


    /**
     * Description
     * @method goToAlbum
     * @param {} album
     * @return 
     */
    function goToAlbum(album) {
      $rootScope.loadingProgress = true;
      vm.albums = [];
      vm.videos = [];
      vm.photos = [];
      vm.locations = [];
      vm.albumRoute = false;
      $state.go('app.media.type.album', {type: 'album', albumId: album});
      getAlbumItems(album);
    }

    /**
     * Description
     * @method getAlbumItems
     * @param {} albumId
     * @return 
     */
    function getAlbumItems(albumId) {
      vm.videos = [];
      vm.photos = [];
      vm.locations = [];
      MediaService.getAlbumItems({album: albumId}).then(function (response) {
        $rootScope.loadingProgress = false;
        filterPhotosVideos(response);
      });
    }

    /**
     * Description
     * @method filterPhotosVideos
     * @param {} response
     * @return Literal
     */
    function filterPhotosVideos(response) {
      vm.videos = [];
      vm.photos = [];
      vm.locations = [];
      response.forEach(function (resp) {
        var fileType = resp.file_type;
        if (fileType && fileType === 'video') {
          vm.videos.push(resp);
        } else {
          vm.photos.push(resp);
        }
      });
      return true;
    }

    /**
     * Description
     * @method closeVideoPopup
     * @return 
     */
    function closeVideoPopup() {
      $mdDialog.hide();
      angular.element('#videoPlayerPopup').removeClass('is-visible');
      vm.videoPlayers.forEach(function (player) {
        player.destroy();
      });
      angular.element('#videoUrl').attr('src', '');
      angular.element('#posterUrl').attr('poster', '');
    }

    /**
     * Description
     * @method videoPreview
     * @param {} video
     * @return 
     */
    function videoPreview(video) {

      showDialogue('app/main/media/dialogs/video_player.html', false);

      $timeout(function () {
        var videoUrl = vm.site_url + video.file_dir;
        var videoPosterUrl = vm.site_url + video.poster_dir;
        vm.previewVideoTitle = video.name;
        vm.videoPlayers = plyr.setup(document.getElementById('sochara_video_player'), [{
          autoplay: false,
          hideControls: true
        }]);

        vm.videoPlayers[0].source({
          type: 'video',
          sources: [{
            src: videoUrl,
            type: 'video/mp4'
          }],
          poster: videoPosterUrl
        });
      }, 100);
    }

    /**
     * Description
     * @method getAlbums
     * @param {} albumId
     * @return 
     */
    function getAlbums(albumId) {
      vm.navMenu = 'album';
      $state.go('app.media.type.album', { type: 'album', albumId: albumId });
      vm.loadingAlbums = true;
      albumId = albumId || '';
      MediaService.getAllAlbumList({album: albumId}).then(function (response) {
        if (response) {
          $rootScope.loadingProgress = false;
          vm.albums = response;
          vm.loadingAlbums = false;
        }
      });
    }

    /**
     * Description
     * @method getThumbCover
     * @param {} album
     * @return url
     */
    function getThumbCover(album) {
      var url = '';
      if (album && album.cover) {
        url = vm.site_url + album.cover;
      } else {
        url = vm.site_url + '/images/icons/album_cover.png';
      }
      return url;
    }

    /**
     * Description
     * @method checkAlbumOrType
     * @return LogicalExpression
     */
    function checkAlbumOrType() {
      return $state.params.type && $state.params.type === 'album';
    }


    /**
     * Description
     * @method makeImageLinkArray
     * @param {} photos
     * @param {} callback
     * @return 
     */
    function makeImageLinkArray(photos, callback) {
      var fileDataUrls = [];
      photos.forEach(function (val) {
        var filExt = val.ext.toLowerCase();
        if (filExt === '.png' || filExt === '.jpeg' || filExt === '.jpg' || filExt === '.gif' || filExt === '.bmp') {
          fileDataUrls.push(vm.site_url + val.file_dir);
        }
      });
      callback(fileDataUrls);
    }

    /**
     * Description
     * @method reloadMedia
     * @return 
     */
    function reloadMedia() {
      if ($state.params.albumId) {
        getAlbumItems($state.params.albumId);
      } else {
        if( $state.params.type && $state.params.type === 'photos' ){
          getAllPhotos();
        }else if( $state.params.type && $state.params.type === 'videos' ){
          getAllVideos();
        }else if( $state.params.type && $state.params.type === 'album' ){
          goToAlbums();
        }else {
          getPhotos();
        }
      }
    }

    /**
     * Description
     * @method getAllVideos
     * @return 
     */
    function getAllVideos() {
      vm.navMenu = 'videos';
      $rootScope.loadingProgress = true;
      vm.photos = [];
      vm.albums = [];
      vm.locations = [];
      getVideos();
      $state.go('app.media.type', {type: 'videos'});
    }

    /**
     * Description
     * @method getAllPhotos
     * @return 
     */
    function getAllPhotos() {
      vm.navMenu = 'photos';
      vm.albumRoute = false;
      vm.videos = [];
      vm.albums = [];
      vm.locations = [];
      getPhotos();
      $state.go('app.media.type', {type: 'photos'});
    }

    /**
     * Description
     * @method getSocialAccount
     * @return 
     */
    function getSocialAccount(){
      DashboardService.getSocialAccount().then(function ( respAcc ) {
        respAcc = respAcc.map(function (val) {
          return val.provider;
        });
        vm.socialList = respAcc.filter(function (val) {
          return val.provider !== 'instagram';
        });
      });
    }

    /**
     * Description
     * @method getPhotos
     * @return 
     */
    function getPhotos() {
      vm.photos = [];
      vm.loadingPhotos = true;
      $rootScope.loadingProgress = true;
      MediaService.getPhotos().then(function (response) {
        $rootScope.loadingProgress = false;
        if (response) {
          vm.photos = response;
          vm.loadingPhotos = false;
        }
      });
    }

    /**
     * Description
     * @method printThisPhoto
     * @param {} directory
     * @return 
     */
    function printThisPhoto( directory ) {
      var photoActualUrl = vm.site_url + directory;
      var newWindow = $window.open(photoActualUrl);
      newWindow.print();
    }

    /**
     * Description
     * @method sharePhotoTwitter
     * @param {} photo
     * @return 
     */
    function sharePhotoTwitter (photo) {
      MediaService.sharePhotoTwitter({id: photo.id}).then(function ( respPhoto ) {
        showMessage('Your photo is posted to twitter !');
      });
    }

    /**
     * Description
     * @method shareAlbumTwitter
     * @param {} album
     * @return 
     */
    function shareAlbumTwitter (album) {
      MediaService.shareAlbumTwitter({id: album.id, name: album.name}).then(function ( respPhoto ) {
        showMessage('Your album is posted to twitter !');
      });
    }

    /**
     * Description
     * @method sharePhotoLinkedin
     * @param {} photo
     * @return 
     */
    function sharePhotoLinkedin ( photo ) {
      var photoActualUrl = vm.site_url + photo.file_dir;
      $window.open("https://www.linkedin.com/shareArticle?mini=true&url=" + photoActualUrl + "&title==" + photo.name + "&source=" + photoActualUrl, '_blank')
    }

    /**
     * Description
     * @method sharePhotoGoogle
     * @param {} photo_dir
     * @return 
     */
    function sharePhotoGoogle ( photo_dir ) {
      var photoActualUrl = vm.site_url + photo_dir;
      $window.open("https://plus.google.com/share?url=" + photoActualUrl, '_blank');
    }

    /**
     * Description
     * @method sharePhotoFacebook
     * @param {} photo
     * @return 
     */
    function sharePhotoFacebook(photo) {
      MediaService.postToFacebook({id: photo.id}).then(function (respPost) {
        showMessage('The media is posted to facebook.');
      });
    }

    /**
     * Description
     * @method shareAlbumFacebook
     * @param {} album
     * @return 
     */
    function shareAlbumFacebook(album){
      MediaService.shareAlbumFacebook({id: album.id}).then(function (respAlbum) {
        showMessage(respAlbum.message);
      });
    }


    /**
     * Description
     * @method sharePhotoPinterest
     * @param {} photo
     * @return 
     */
    function sharePhotoPinterest( photo ) {
      var photoActualUrl = vm.site_url + photo.file_dir;
      $window.open("https://pinterest.com/pin/create/button/?url=" + photoActualUrl + "&media=" + photoActualUrl + "&description=" + photo.name, '_blank')
    }

    /**
     * Description
     * @method getVideos
     * @return 
     */
    function getVideos() {
      vm.loadingVideos = true;
      vm.videos = [];
      $rootScope.loadingProgress = true;
      MediaService.getVideos().then(function (response) {
        if (response) {
          $rootScope.loadingProgress = false;
          vm.loadingVideos = false;
          vm.videos = response;
        }
      });
    }

    /**
     * Description
     * @method deleteItems
     * @param {} ev
     * @return 
     */
    function deleteItems(ev) {
      if (vm.selectedPhotos.length) {
        $mdDialog.show({
          // controller: 'UploadDialogController',
          /**
           * Description
           * @method controller
           * @return vm
           */
          controller: function () {
            return vm;
          },
          controllerAs: 'vm',
          templateUrl: 'app/main/media/dialogs/delete.html',
          parent: angular.element($document.body),
          targetEvent: ev,
          clickOutsideToClose: false
        });
      } else {
        showMessage("Select image first");
      }
    }

    /**
     * Description
     * @method deleteItemsConfirm
     * @return 
     */
    function deleteItemsConfirm() {
      if (vm.photoForDelete || vm.selectedPhotos.length) {
        var deleteData = {};
        if (vm.photoForDelete) {
          deleteData.items = vm.photoForDelete;
        } else if (vm.selectedPhotos.length) {
          deleteData.items = [];
          vm.selectedPhotos.forEach(function (val) {
            if (val.ext === '.png' || val.ext === '.jpeg' || val.ext === '.jpg' || val.ext === '.gif' || val.ext === '.bmp') {
              deleteData.items.push(val.id);
            }
          });
        }
        MediaService.deleteItems(deleteData).then(function (response) {
          if (response) {
            vm.selectedPhotos = [];
            reloadMedia();
          }
        });
      }
      vm.photoForDelete = '';
      closeDialog();
    }

    /**
     * Description
     * @method copySelectedItems
     * @return 
     */
    function copySelectedItems() {
      vm.selectedFilesForMove = [];
      vm.selectedFilesForCopy = vm.selectedPhotos;
      vm.selectedFilesFrom = $state.params.albumId || '0';
      showMessage("Items are coppied.");
    }

    /**
     * Description
     * @method cutSelectedItems
     * @return 
     */
    function cutSelectedItems() {
      vm.selectedFilesForCopy = [];
      vm.selectedFilesForMove = vm.selectedPhotos;
      vm.selectedFilesFrom = $state.params.albumId || '0';
      showMessage("Items are ready to move.");
    }

    /**
     * Description
     * @method pasteSelectedItems
     * @return 
     */
    function pasteSelectedItems() {
      var pasteData = vm.selectedFilesForCopy.length ? vm.selectedFilesForCopy : vm.selectedFilesForMove;
      var pasteTo = $state.params.albumId || null;
      var pasteFrom = vm.selectedFilesFrom;
      var pasteType = vm.selectedFilesForCopy && vm.selectedFilesForCopy.length ? 'copy' : 'cut';
      if( !pasteData.length || !pasteTo || !pasteFrom || !pasteType ){
        return 0;
      }
      MediaService.pasteSelectedItems({pasteItems: pasteData, album: pasteTo, pasteFrom: pasteFrom, pasteType: pasteType}).then(function (response) {
        if (response) {
          reloadMedia();
        }
      });
    }

    /**
     * Description
     * @method deletePhoto
     * @param {} ev
     * @param {} photo
     * @return 
     */
    function deletePhoto(ev, photo) {
      vm.photoForDelete = photo.id;
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: 'app/main/media/dialogs/delete.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: false
      });
    }

    /**
     * Description
     * @method showMessage
     * @param {} message
     * @param {} type
     * @param {} position
     * @return 
     */
    function showMessage(message, type, position) {
      type = type || 'success';
      position = position || 'bottom';
      $mdToast.show({
        template: '<md-toast class="md-toast ' + type + ' ">' + message + '</md-toast>',
        hideDelay: 3000,
        position: 'bottom left'
      });
    }

    /**
     * Description
     * @method showDialogue
     * @param {} templateUrl
     * @param {} clickOutsideToClose
     * @return 
     */
    function showDialogue(templateUrl, clickOutsideToClose) {
      $mdDialog.show({
        /**
         * Description
         * @method controller
         * @return vm
         */
        controller: function () {
          return vm;
        },
        controllerAs: 'vm',
        templateUrl: templateUrl,
        clickOutsideToClose: clickOutsideToClose || false
      });
    }

    /**
     * Description
     * @method getThumbFromPhoto
     * @param {} photo
     * @return url
     */
    function getThumbFromPhoto(photo) {
      var url = '';
      if (photo && photo.thumb_dir) {
        url = photo.thumb_dir;
      }
      return url;
    }

    /**
     * Description
     * @method closeDialog
     * @return 
     */
    function closeDialog() {
      $mdDialog.hide();
    }

    /***************************************** Autocomplete For Share FIles ****************************************/
    vm.selectedContact = null;
    vm.searchText = null;
    vm.selectedContacts = [];

    vm.querySearch = querySearch;


    /**
     * Search for vegetables.
     * @method querySearch
     * @param {} query
     * @return MemberExpression
     */
    function querySearch (query) {
      // query = query.length > 1 ? query : '';
      var results = query ? ContactService.getAllSocharaUsers({name: query}).then(function (response) {
        var totalResponse = response.map(function (c) {
          var contact = {
            name: c.first_name + ' ' + c.last_name,
            email: c.email,
            photo_url: c.photo_url ? c.photo_url.replace('assets', '').replace(/\\/, '/') : '../images/avatars/blank.jpg',
            id: c.id || undefined,
          };
          return {
            value: contact.name.toLowerCase(),
            name: contact.name,
            email: contact.email,
            image: contact.photo_url,
            isContact: false,
            id: contact.id
          };
        });
        var searchQuery = angular.lowercase(query);
        totalResponse = totalResponse.concat(vm.filteredContacts);
        return totalResponse.filter(function ( state ) {
          return state.value.indexOf(searchQuery) === 0;
        });
      }) : vm.filteredContacts;
      var deferred = $q.defer();
      $timeout(function () {
        deferred.resolve(results);
      }, Math.random() * 1000, false);
      return deferred.promise;
    }

    /**
     * Description
     * @method loadContacts
     * @return 
     */
    function loadContacts() {
      ContactService.getContacts().then(function (resp) {
        if (resp) {
          vm.filteredContacts = resp.map(function (c) {
            var contact = {
              name: c.first_name + ' ' + c.last_name,
              email: c.email,
              photo_url: c.photo_url ? c.photo_url.replace('assets', '').replace(/\\/, '/') : '../images/avatars/blank.jpg',
              id: c.sochara_user_id || undefined,
            };
            // contact._lowername = contact.name.toLowerCase();
            return {
              value: contact.name.toLowerCase(),
              name: contact.name,
              email: contact.email,
              isContact: true,
              image: contact.photo_url,
              id: contact.id
            };
          });
        }
      });
    }


    /**
     * *************************************** Autocomplete For Share FIles ***************************************
     * @method uploadMedia
     * @param {} photos
     * @param {} errFiles
     * @param {} album
     * @param {} from
     * @return 
     */
    function uploadMedia(photos, errFiles, album, from) {
      if(!($rootScope.loadingProgress && from && from === 'parent')){
        photos = photos.filter(function (photo) {
          return photo.type && photo.type.length;
        });
        $rootScope.loadingProgress = true;
        vm.uploadNotification = true;
        $scope.files = photos;
        vm.errFiles = errFiles;
        // closeDialog();
        if (photos && photos.length) {
          album = album || $state.params.albumId || '';
          angular.forEach(photos, function(file, i) {
            file.upload = Upload.upload({
              url: '/media/uploadMediaVideo',
              headers: {
                'Content-Type': 'multipart/form-data'
              },
              data: {album: album || '', filePath: file.path, file: file}
            });

            file.upload.then(function (response) {
              $timeout(function () {
                file.result = response.data;
                if ($scope.files && $scope.files.length && $scope.files.length === i + 1) {
                  vm.showUploadedNotification = true;
                  $scope.files = null;
                  $timeout( function(){
                    // $rootScope.loadingProgress = false;
                    reloadMedia();
                  }, 2000 );
                }
              });
            }, function (response) {
              if (response.status > 0) {
                vm.errorMsg = response.status + ': ' + response.data;
              }
            }, function (evt) {
              file.progress = Math.min(100, parseInt(100.0 *
                evt.loaded / evt.total));
            });
          });
        }
      }
    }

    /********************************** Photo Edit Initialize ***************************************************/

    /**
     * *********************************** Photo Edit Save As Confirm Function **************************************
     * @method createNewPhoto
     * @return
     */



    angular.element(document).ready(function () {
      var editor;

      var foundEditor = angular.element('#editor');
      if (foundEditor.length) {

        var photoId = $state.params.photoId || '';
        if (photoId) {

          var myImage = new Image();

          photoUrlFromId(photoId, function (photoUrl) {
            if (photoUrl) {
              myImage.src = photoUrl;
              myImage.addEventListener('load', function () {
                $timeout(function () {
                  run();
                }, 1500);
              });
            }
          });
        }

      }

      /**
       * Description
       * @return
       * @method run
       * @param {} preferredRenderer
       * @return 
       */
      function run(preferredRenderer) {
        'use strict';
        $rootScope.loadingProgress = false;
        var ReactUI = PhotoEditorSDK.UI.ReactUI, _PhotoEditorSDK = PhotoEditorSDK, Promise = _PhotoEditorSDK.Promise;

        editor = new ReactUI({
          container: document.querySelector('#editor'),
          logLevel: 'error',
          editor: {
            image: myImage,
            preferredRenderer: preferredRenderer || 'canvas',
            export: {
              download: false,
              type: PhotoEditorSDK.RenderType.DATAURL
            }
          },
          assets: {
            baseUrl: vm.site_url + '/vendors/SocharaPhotoEditor',
            /**
             * Description
             * @method resolver
             * @param {} path
             * @return path
             */
            resolver: function (path) {
              return path;
            }
          },
          language: 'en'
        });

        editor.on('cancel', function () {
          goToPhotoLocation();
        });

        /**
         * Description
         * @method goToPhotoLocation
         * @return 
         */
        function goToPhotoLocation() {
          var photoEditAlbumId = vm.photoEditAlbum || $state.params.albumId || '';
          if (photoEditAlbumId) {
            goToAlbum(photoEditAlbumId);
          } else if (vm.photoEditId) {
            $state.go('app.media.type', {type: 'photos'});
          }
        }

        editor.on('export', function (dataURL) {
          $rootScope.loadingProgress = true;
          var photoEditAlbumId = vm.photoEditAlbum || $state.params.albumId || '';
          Upload.upload({
            url: '/media/uploadMediaEdited',
            headers: {
              'Content-Type': 'multipart/form-data'
            },
            data: {id: vm.photoEditId, album: photoEditAlbumId, file: Upload.dataUrltoBlob(dataURL, vm.photoEditName)}
          }).success(function (data) {

            $timeout(function () {
              $rootScope.loadingProgress = false;
              goToPhotoLocation();
            }, 1000);

          }).error(function (err) {
            console.log(err);
          });
          // Upload to Server
        });

        editor.on('saveas', function (dataURL) {
          $rootScope.loadingProgress = true;
          var photoEditAlbumId = vm.photoEditAlbum || $state.params.albumId || '';
          Upload.upload({
            url: '/media/uploadMediaEdited',
            headers: {
              'Content-Type': 'multipart/form-data'
            },
            data: {
              id: vm.photoEditId,
              album: photoEditAlbumId,
              copy: 'yes',
              file: Upload.dataUrltoBlob(dataURL, vm.photoEditName)
            }
          }).success(function (data) {

            // $scope.saveNotification = true;
            // console.log(data.id);
            $timeout(function () {
              $rootScope.loadingProgress = false;
              goToPhotoLocation();
            }, 1000);

          }).error(function (err) {
            console.log(err);
          });
        });
      }


    });

    /**
     * ******************************** End # Photo Edit Initialize **************************************************
     * @method photoUrlFromId
     * @param {} id
     * @param {} callback
     * @return 
     */
    function photoUrlFromId(id, callback) {
      var photoUrl = '';
      MediaService.getPhotoInfoFromId(id).then(function (response) {
        if (response) {
          vm.photoEditId = response.id;
          vm.photoEditName = response.name;
          photoUrl = vm.site_url + response.file_dir;
          callback(photoUrl);
        } else {
          callback(photoUrl);
        }
      });
    }

    vm.ngFlowOptions = {
      // You can configure the ngFlow from here
      /*target                   : 'api/media/image',
       chunkSize                : 15 * 1024 * 1024,
       maxChunkRetries          : 1,
       simultaneousUploads      : 1,
       testChunks               : false,
       progressCallbacksInterval: 1000*/
    };
    vm.ngFlow = {
      // ng-flow will be injected into here through its directive
      flow: {}
    };


    /**
     * Array prototype
     * Get by id
     * @method getById
     * @param value
     * @return MemberExpression
     */
    Array.prototype.getById = function (value) {
      return this.filter(function (x) {
        return x.id === value;
      })[0];
    };

  }

})();
