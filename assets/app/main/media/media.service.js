(function () {
  'use strict';

  angular
    .module('sochara')
    .factory('MediaService', mediaService);

  /** @ngInject */
  function mediaService($http, $q) {
    return {
      getContacts: function () {
        var defer = $q.defer();
        $http.post('/json/users').then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getPhotos: function () {
        var defer = $q.defer();
        $http.post('/media/allPhotoList').then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      addToStar: function ( data ) {
        var defer = $q.defer();
        $http.post('/media/addToStar', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      removeFromStar: function ( data ) {
        var defer = $q.defer();
        $http.post('/media/removeFromStar', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      renameMedia: function ( data ) {
        var defer = $q.defer();
        $http.post('/media/renameMedia', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      postToFacebook: function ( data ) {
        var defer = $q.defer();
        $http.post('/dashboard/mediaPostToFacebook', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      sharePhotoTwitter: function ( data ) {
        var defer = $q.defer();
        $http.post('/dashboard/mediaPostTwitter', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      shareAlbumTwitter: function ( data ) {
        var defer = $q.defer();
        $http.post('/dashboard/albumPostTwitter', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      shareAlbumFacebook: function ( data ) {
        var defer = $q.defer();
        $http.post('/dashboard/albumPostToFacebook', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      pasteSelectedItems: function ( data ) {
        var defer = $q.defer();
        $http.post('/media/pasteSelectedItems', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getVideos: function () {
        var defer = $q.defer();
        $http.post('/media/allVideoList').then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getAlbumItems: function ( data ) {
        var defer = $q.defer();
        $http.post('/media/getAlbumItems', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      createNewAlbum: function ( name, album ) {
        var defer = $q.defer();
        var data = {};
        if ( name ) {
          data.name = name;
        }
        if ( album ) {
          data.root = album;
        }
        $http.post('/media/createNewAlbum', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      deleteItems: function ( data ) {
        var defer = $q.defer();
        $http.post('/media/deleteItems', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      renameAlbum: function ( data ) {
        var defer = $q.defer();
        $http.post('/media/renameAlbum', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      deleteAlbum: function ( id ) {
        var defer = $q.defer();
        $http.post('/media/deleteAlbum', { id: id } ).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      downloadAlbum: function ( data ) {
        var defer = $q.defer();
        $http.post('/media/downloadAlbum', data ).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getAllAlbumList: function ( data ) {
        var defer = $q.defer();
        $http.post('/media/getAllAlbumList', data).then(function SuccessCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },


      itemsShareWithUsers: function ( data ) {
        var defer = $q.defer();
        $http.post('/media/itemsShareWithUsers', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getSharedItemList: function (album) {
        var defer = $q.defer();
        var data = {};
        if (album) {
          data.album = album;
        }
        $http.post('/media/getSharedItemList', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getPreviousSharedUsers: function ( data ) {
        var defer = $q.defer();
        $http.post('/media/getPreviousSharedUsers', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getPhotoInfoFromId: function ( id ) {
        var defer = $q.defer(); var data = {}; data.id = id;
        $http.post('/media/getPhotoInfoFromId', data).then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      },

      getPhotoAlbumListByLocation: function(  ){
        var defer = $q.defer();
        $http.post('/media/getPhotoAlbumListByLocation').then(function successCallback(resp) {
          defer.resolve(resp.data);
        }, function errorCallback(err) {
          defer.reject(err);
        });
        return defer.promise;
      }
    };
  }

})();
