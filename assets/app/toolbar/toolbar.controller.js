(function () {
  'use strict';

  angular
    .module('app.toolbar')
    .controller('ToolbarController', ToolbarController);

  /** @ngInject */

  function ToolbarController($rootScope, $q,  $state, $timeout, $scope, $mdSidenav, DashboardService, $filter, $mdToast, Auth, $window, $location) {
    var vm = this;

    // Data
    $rootScope.global = {
      search: ''
    };

    $rootScope.incrementNotificationCounting = incrementNotificationCounting;
    $rootScope.decrementNotificationCounting = decrementNotificationCounting;

    //changing route to Profile page
    vm.profilePage = function () {
      $state.go('app.profile', {});
    };

    vm.mailInboxPage = function () {
      // $location.path('/mail/inbox');
      $state.go('app.mail.account.threads', {filter: 'inbox', accountId: 0});
    };

    vm.goToCalendarModule = $rootScope.goToCalendarModule = function () {
      $state.go('app.calendar', {});
    };

    vm.goToChatModule = function () {
      $state.go('app.chat', {});
    };


    vm.goToFilesModule = function () {
      $state.go('app.files', {});
    };

    vm.goToMediaModule = function () {
      $state.go('app.media.type', {});
    };

    vm.goToGlobalSetting = function () {
      $state.go('app.global-setting', {});
    };

    vm.goToPollModule = function () {
      $state.go('app.poll', {});
    };

    vm.goToCardModule = function () {
      $state.go('app.cards', {});
    };

    /**
     * @description Opening Contacts Module [Last Modified: 18/8/17: 11.56 AM by Mahdi]
     * @method openContactsModule
     * @return undefined
     */

    vm.openContactsModule = function () {

      //Changing State to Contacts
      $state.go('app.contacts', {});
    };

    vm.openLabelModule = function () {

      //Changing State to Contacts
      $state.go('app.labels', {});
    };

    vm.bodyEl = angular.element('body');
    vm.userStatusOptions = [
      {
        'title': 'Online',
        'icon': 'icon-checkbox-marked-circle',
        'class': 'online',
        'color': '#4CAF50'
      },
      {
        'title': 'Away',
        'icon': 'icon-clock',
        'class': 'away',
        'color': '#FFC107'
      },
      {
        'title': 'Do not Disturb',
        'icon': 'icon-minus-circle',
        'class': 'do-not-disturb',
        'color': '#F44336'
      },
      {
        'title': 'Offline',
        'icon': 'icon-checkbox-blank-circle-outline',
        'class': 'offline',
        'color': '#616161'
      }

    ];

    // Methods
    vm.toggleSidenav = toggleSidenav;
    vm.logout = logout;
    vm.setUserStatus = setUserStatus;
    vm.toggleHorizontalMobileMenu = toggleHorizontalMobileMenu;
    vm.search = search;
    vm.searchResultClick = searchResultClick;
    vm.gotoHome = gotoHome;
    vm.globalSearch = globalSearch;
    $rootScope.getGlobalNotificationCount = getGlobalNotificationCount;


    //////////

    init();

    /**
     * Initialize
     */
    function userInfo() {

      Auth.info().then(function (response) {
        vm.user = response;
        vm.user.mode = 'online';
        $rootScope.user = vm.user;
        vm.userStatus = $filter('filter')(vm.userStatusOptions, {class: vm.user.mode}, true)[0];
      });

    }

    function init() {
      // Select the first status as a default

      userInfo();

      getGlobalNotificationCount();
      $rootScope.totalNotificationCount = 0;

      vm.date = new Date();
      $timeout(function () {
        io.socket.post('/socket/joinSocket', function gotResponse(data, jwRes) {});
      });

    }

    function joinSocket(){
      $timeout(function () {
        DashboardService.socketTestOne().then(function (respSocket) {
          console.log(respSocket);
        });
      }, 5000);
    }

    function gotoHome() {
      $state.go('dashboard');
    }

    function globalSearch( searchValue ){
      $rootScope.globalSearchValue = searchValue;
      $rootScope.showSearchResult = !! searchValue.length;
    }

    io.socket.on('new_message', function onServerSentEvent( message ) {
      // showMessage('You have new message!');
      getGlobalNotificationCount();
    });


    function incrementNotificationCounting( module ){
      $rootScope.notificationCount[module] ++;
      refreshTotalNotificationCount();
    }

    function decrementNotificationCounting( module ){
      $rootScope.notificationCount[module] = $rootScope.notificationCount[module] ? $rootScope.notificationCount[module]-- : 0;
      refreshTotalNotificationCount();
    }


    io.socket.on('im_refresh_conversation', function onServerSentEvent(data) {
      if (data.users.indexOf(vm.user.id) > -1 && data.sender.id !== vm.user.id) {
        if( vm.chatConversationId !== data.conversation && data.sender.first_name ){
          var chatMessage = "You have a message from " + data.sender.first_name + ' ' + data.sender.last_name;
          showMessage(chatMessage);
        }
      }
    });

    io.socket.on('global', function onServerSentEvent(data) {
      if( data && data.message ){
        showMessage(data.message);
      }
    });



    function showMessage(message) {
      $mdToast.show({
        template: '<md-toast class="md-toast success">' + message + '</md-toast>',
        hideDelay: 3000,
        position: 'bottom left'
      });
    }


    /**
     * Toggle sidenav
     *
     * @param sidenavId
     */
    function toggleSidenav(sidenavId) {
      $mdSidenav(sidenavId).toggle();
    }

    /**
     * Sets User Status
     * @param status
     */
    function setUserStatus(status) {
      Auth.changeMode(status.class).then(function (resp) {
        // console.log(resp);
        vm.userStatus = status;
        vm.user.mode = status.class;
        $rootScope.user.mode = status.class;
      });
    }

    function getGlobalNotificationCount(){
      DashboardService.getNotificationCount().then(function ( respCount ) {
        $rootScope.notificationCount = respCount;
        refreshTotalNotificationCount();
      });
    }

    function refreshTotalNotificationCount(){
      $rootScope.totalNotificationCount = Object.keys($rootScope.notificationCount).filter(function (obj) {
        return $rootScope.notificationCount[obj];
      }).length;
    }

    /**
     * Logout Function
     */
    function logout() {
      // Do logout here..
      Auth.logout().then(function (response) {
        if (response) {
          localStorage.removeItem('ats');
          $state.go('app.auth_login');
          return;
        }
      });
    }


    /**
     * Toggle horizontal mobile menu
     */
    function toggleHorizontalMobileMenu() {
      vm.bodyEl.toggleClass('ms-navigation-horizontal-mobile-menu-active');
    }



    /**
     * Search action
     *
     * @param query
     * @returns {Promise}
     */
    function search(query) {

      // Fake service delay
      $timeout(function () {
        deferred.resolve(navigation);
      }, 1000);

      return deferred.promise;
    }

    /**
     * Search result click action
     *
     * @param item
     */
    function searchResultClick(item) {
      // If item has a link
      if (item.uisref) {
        // If there are state params,
        // use them...
        if (item.stateParams) {
          $state.go(item.state, item.stateParams);
        }
        else {
          $state.go(item.state);
        }
      }
    }
  }

})();
