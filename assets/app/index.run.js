
(function () {
  'use strict';

  angular
    .module('sochara')
    .run(runBlock);

  /** @ngInject */
  function runBlock($rootScope, $timeout, $state, $location, Auth) {

    // var token = localStorage.getItem('ats');
    // if( !token ){
    //   $location.path('/auth/login');
    //   return false;
    //   // $state.go('app.auth_login');
    // }


    var token = localStorage.getItem('ats');
    if (token) {
      Auth.isAuthenticated().then(function (response) {
        if (!response.user_id) {
          $location.path('/auth/login');
          return false;
        }
      });
    } else if ($location.$$path === '/setAtsToken') {
      var newToken = $location.absUrl().split('ats=')[1];
      var ats = {token: newToken};
      Auth.isAuthenticated().then(function (response) {
        if ( !response.user_id ) {
          $location.path('/auth/login');
          return false;
        }else{
          localStorage.setItem('ats', JSON.stringify(ats));
        }
      });

    } else if ($location.$$path === '/auth/register') {
      $location.path('/auth/register');
    }
    else if ($location.$$path === '/page/facebook-privacy-policy') {
      $location.path('/page/facebook-privacy-policy');
    }
    else if ($location.$$path === '/page/google-privacy-policy') {
      $location.path('/page/google-privacy-policy');
    }
    else if ($location.$$path === '/page/instagram-privacy-policy') {
      $location.path('/page/instagram-privacy-policy');
    }
    else if ($location.$$path === '/auth/forgot-password') {
      $location.path('/auth/forgot-password');
    }
    else if ($location.$$path === '/auth/reset-password') {
      $location.path('/auth/reset-password');
    }
    else {
      $location.path('/auth/login');
      return false;
    }

    // Activate loading indicator
    var stateChangeStartEvent = $rootScope.$on('$stateChangeStart', function () {
      $rootScope.loadingProgress = true;
    });

    // De-activate loading indicator
    var stateChangeSuccessEvent = $rootScope.$on('$stateChangeSuccess', function () {
      $timeout(function () {
        $rootScope.loadingProgress = false;
      });
    });

    // Store state in the root scope for easy access
    $rootScope.state = $state;

    // Cleanup
    $rootScope.$on('$destroy', function () {
      stateChangeStartEvent();
      stateChangeSuccessEvent();
    });
  }
})();
