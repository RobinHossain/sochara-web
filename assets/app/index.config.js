(function () {
  'use strict';

  angular
    .module('sochara')
    .config(config);

  /** @ngInject */
  function config($provide, $mdAriaProvider) {


    // Put your common app configurations here
    $mdAriaProvider.disableWarnings();



    // Text Angular options
    $provide.decorator('taOptions', [
      'taRegisterTool', '$delegate', function (taRegisterTool, taOptions) {
        taOptions.toolbar = [
          ['fontName', 'fontSize', 'fontColor', 'backgroundColor', 'bold', 'italics', 'underline', 'ul', 'ol', 'redo', 'undo'],
          ['justifyLeft', 'justifyCenter', 'justifyRight'],
          ['insertImage', 'insertLink']
        ];

        taOptions.classes = {
          focussed: 'focussed',
          toolbar: 'ta-toolbar',
          toolbarGroup: 'ta-group',
          toolbarButton: 'md-button',
          toolbarButtonActive: 'active',
          disabled: '',
          textEditor: 'form-control',
          htmlEditor: 'form-control'
        };

        taRegisterTool('fontName', {
          display: "<md-menu ng-style='{\"width\": \"43px\"}'><md-button ng-style='{\"width\": \"43px\", \"text-transform\": \"capitalize\"}' ng-click='$mdMenu.open()'>Fonts<md-icon md-font-icon='icon-menu-down'></md-icon></md-button>" +
          "<md-menu-content>" +
          "<md-menu-item ng-repeat='o in options'><md-button ng-style='{\"font-family\": \"{{o.css}}\"}' ng-click='action($event, o.css, o.name)'>{{o.name}}</md-button></md-menu-item>" +
          "</md-menu-content></md-menu>",
          // "<ul class='dropdown-menu'><li ng-repeat='o in options'><button class='btn btn-blue checked-dropdown' style='font-family: {{o.css}}; width: 100%' type='button' ng-click='action($event, o.css)'><i ng-if='o.active' class='fa fa-check'></i>{{o.name}}</button></li></ul></span>",
          action: function (event, font, name) {
            //Ask if event is really an event.
            if (!!event.stopPropagation) {
              //With this, you stop the event of textAngular.
              event.stopPropagation();
              //Then click in the body to close the dropdown.
              $("body").trigger("click");
            }
            return this.$editor().wrapSelection('fontName', font);
          },
          options: [
            {name: 'Sans-Serif', css: 'Arial, Helvetica, sans-serif'},
            {name: 'Serif', css: "'times new roman', serif"},
            {name: 'Wide', css: "'arial black', sans-serif"},
            {name: 'Narrow', css: "'arial narrow', sans-serif"},
            {name: 'Comic Sans MS', css: "'comic sans ms', sans-serif"},
            {name: 'Courier New', css: "'courier new', monospace"},
            {name: 'Garamond', css: 'garamond, serif'},
            {name: 'Georgia', css: 'georgia, serif'},
            {name: 'Tahoma', css: 'tahoma, sans-serif'},
            {name: 'Trebuchet MS', css: "'trebuchet ms', sans-serif"},
            {name: "Helvetica", css: "'Helvetica Neue', Helvetica, Arial, sans-serif"},
            {name: 'Verdana', css: 'verdana, sans-serif'},
            {name: 'Proxima Nova', css: 'proxima_nova_rgregular'}
          ]
        });

        taRegisterTool('fontSize', {
          // display: "<span class='bar-btn-dropdown dropdown'>" +
          // "<button class='btn btn-blue dropdown-toggle' type='button' ng-disabled='showHtml()' style='padding-top: 4px'><i class='fa fa-text-height'></i><i class='fa fa-caret-down'></i></button>" +
          // "<ul class='dropdown-menu'><li ng-repeat='o in options'><button class='btn btn-blue checked-dropdown' style='font-size: {{o.css}}; width: 100%' type='button' ng-click='action($event, o.value)'><i ng-if='o.active' class='fa fa-check'></i> {{o.name}}</button></li></ul>" +
          // "</span>",
          display: "<md-menu ng-style='{\"width\": \"55px\"}'><md-button ng-style='{\"width\": \"55px\"}' ng-click='$mdMenu.open()'><i class='icon sochara-icon font_size'></i><md-icon md-font-icon='icon-menu-down'></md-icon></md-button>" +
          "<md-menu-content>" +
          "<md-menu-item ng-repeat='o in options'><md-button ng-style='{\"font-size\": \"{{o.css}}\"}' ng-click='action($event, o.value, o.name)'>{{o.name}}</md-button></md-menu-item>" +
          "</md-menu-content></md-menu>",
          action: function (event, size, name) {
            //Ask if event is really an event.
            if (!!event.stopPropagation) {
              //With this, you stop the event of textAngular.
              event.stopPropagation();
              //Then click in the body to close the dropdown.
              $("body").trigger("click");
            }
            return this.$editor().wrapSelection('fontSize', parseInt(size));
          },
          options: [
            {name: 'xx-small', css: 'xx-small', value: 1},
            {name: 'x-small', css: 'x-small', value: 2},
            {name: 'small', css: 'small', value: 3},
            {name: 'medium', css: 'medium', value: 4},
            {name: 'large', css: 'large', value: 5},
            {name: 'x-large', css: 'x-large', value: 6},
            {name: 'xx-large', css: 'xx-large', value: 7}
          ]
        });

        taRegisterTool('backgroundColor', {
          display: "<div spectrum-colorpicker style='width: 63px' ng-model='color' on-change='!!color && action(color)' format='\"hex\"' options='options'></div>",
          action: function (color) {
            var me = this;
            if (!this.$editor().wrapSelection) {
              setTimeout(function () {
                me.action(color);
              }, 100)
            } else {
              return this.$editor().wrapSelection('backColor', color);
            }
          },
          options: {
            replacerClassName: 'icon-format-color-fill',
            showButtons: false,
            showPaletteOnly: true,
            showPalette: true,
            hideAfterPaletteSelect: true,
            palette: [
              ["#000", "#444", "#666", "#999", "#ccc", "#eee", "#f3f3f3", "#fff"],
              ["#f00", "#f90", "#ff0", "#0f0", "#0ff", "#00f", "#90f", "#f0f"],
              ["#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#cfe2f3", "#d9d2e9", "#ead1dc"],
              ["#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
              ["#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6fa8dc", "#8e7cc3", "#c27ba0"],
              ["#c00", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3d85c6", "#674ea7", "#a64d79"],
              ["#900", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#0b5394", "#351c75", "#741b47"],
              ["#600", "#783f04", "#7f6000", "#274e13", "#0c343d", "#073763", "#20124d", "#4c1130"]
            ]
          },
          color: "#fff"
        });

        taRegisterTool('fontColor', {
          display: "<spectrum-colorpicker style='width: 63px' trigger-id='{{trigger}}' ng-model='color' on-change='!!color && action(color)' format='\"hex\"' options='options'></spectrum-colorpicker>",
          action: function (color) {
            var me = this;
            if (!this.$editor().wrapSelection) {
              setTimeout(function () {
                me.action(color);
              }, 100)
            } else {
              return this.$editor().wrapSelection('foreColor', color);
            }
          },
          options: {
            replacerClassName: 'icon-format-color',
            showButtons: false,
            showPaletteOnly: true,
            showPalette: true,
            hideAfterPaletteSelect: true,
            palette: [
              ["#000", "#444", "#666", "#999", "#ccc", "#eee", "#f3f3f3", "#fff"],
              ["#f00", "#f90", "#ff0", "#0f0", "#0ff", "#00f", "#90f", "#f0f"],
              ["#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#cfe2f3", "#d9d2e9", "#ead1dc"],
              ["#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
              ["#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6fa8dc", "#8e7cc3", "#c27ba0"],
              ["#c00", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3d85c6", "#674ea7", "#a64d79"],
              ["#900", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#0b5394", "#351c75", "#741b47"],
              ["#600", "#783f04", "#7f6000", "#274e13", "#0c343d", "#073763", "#20124d", "#4c1130"]
            ]
          },
          color: "#000000"
        });

        // taRegisterTool('fontColor', {
        //   display: "<button type='button' colorpicker colorpicker-text-editor='true' colorpicker-parent='true' class='btn btn-default font-color' ng-disabled='showHtml()' ng-init-'fontColor=\"#000\"' ng-model='fontColor'><i class='fa fa-magic'></i>Font</button>",
        //   action: function(deferred) {
        //     var textAngular = this,
        //       scope = angular.element(".font-color").scope(),
        //       foreColor = this.$editor().queryCommandValue('foreColor');
        //
        //     if(foreColor) {
        //       scope.fontColor = foreColor;
        //     }
        //
        //     scope.$watch('fontColor', function(value, oldValue){
        //       if(value !== '' && value !== oldValue) {
        //         textAngular.$editor().wrapSelection('foreColor', value);
        //         deferred.resolve();
        //       }
        //     });
        //
        //     return false;
        //   }
        // });

        // taRegisterTool('fontColor', {
        //   // display:"<spectrum-colorpicker trigger-id='{{trigger}}' ng-model='color' on-change='!!color && action(color)' format='\"hex\"' options='options'></spectrum-colorpicker>",
        //   // display:"<ms-material-color-picker ng-style='{\"width\": \"50px\"}' ms-model-type='obj' trigger-id='{{trigger}}' ng-model='color' ng-change='!!color && action(color)' format='\"hex\"' options='options'>"+
        //   // "<md-button ng-style='{\"width\": \"50px\"}'>Color</md-button>"+
        //   // "</ms-material-color-picker>",
        //   // display:"<input class='' style='width:100px;' color-picker ng-model='color' on-change='!!color && action(color)'>",
        //   display:"<md-menu ng-style='{\"width\": \"55px\"}'><md-button ng-style='{\"width\": \"55px\"}' ng-click='$mdMenu.open()'><i class='icon sochara-icon font_size'></i><md-icon md-font-icon='icon-menu-down'></md-icon></md-button>" +
        //   "<md-menu-content>"+
        //   // "<span ng-repeat='o in options' class='color_palate' ng-style='{\"background-color\": \"{{o}}\", \"width\": \"20px\", \"height\": \"20px\", \"display\": \"inline-block\", \"float\": \"left\"}' ng-click='action($event, o)'></span>"+
        //   "<md-menu-item ng-repeat='o in options' <md-button class='color_palate' ng-style='{\"background-color\": \"{{o}}\", \"width\": \"20px\", \"height\": \"20px\", \"display\": \"inline-block\", \"float\": \"left\"}' ng-click='action($event, o)'></md-button></md-menu-item>"+
        //   "</md-menu-content></md-menu>",
        //   action: function ( event, color ) {
        //     // var me = this;
        //     // if (!this.$editor().wrapSelection) {
        //     //   console.log('IF');
        //     //   setTimeout(function () {
        //     //     me.action(color);
        //     //   }, 100)
        //     // } else {
        //     //   console.log(color);
        //     //   return this.$editor().wrapSelection('backColor', color);
        //     // }
        //
        //     if (!!event.stopPropagation) {
        //       //With this, you stop the event of textAngular.
        //       event.stopPropagation();
        //       //Then click in the body to close the dropdown.
        //       $("body").trigger("click");
        //     }
        //     console.log(color);
        //     return this.$editor().wrapSelection('backColor', color);
        //   },
        //   // options: {
        //   //   replacerClassName: 'fa fa-paint-brush', showButtons: false
        //   // },
        //   options: [
        //    '#000000', '#ffffff', '#333333'
        //   ],
        //   color: "#000"
        // });


        return taOptions;
      }
    ]);

    // Text Angular tools
    $provide.decorator('taTools', [
      '$delegate', function (taTools) {
        taTools.quote.iconclass = 'icon-format-quote';
        taTools.bold.iconclass = 'icon-format-bold';
        taTools.italics.iconclass = 'icon-format-italic';
        taTools.underline.iconclass = 'icon-format-underline';
        taTools.strikeThrough.iconclass = 'icon-format-strikethrough';
        taTools.ul.iconclass = 'icon-format-list-bulleted';
        taTools.ol.iconclass = 'icon-format-list-numbers';
        taTools.redo.iconclass = 'icon-redo';
        taTools.undo.iconclass = 'icon-undo';
        taTools.clear.iconclass = 'icon-close-circle-outline';
        taTools.justifyLeft.iconclass = 'icon-format-align-left';
        taTools.justifyCenter.iconclass = 'icon-format-align-center';
        taTools.justifyRight.iconclass = 'icon-format-align-right';
        taTools.justifyFull.iconclass = 'icon-format-align-justify';
        taTools.indent.iconclass = 'icon-format-indent-increase';
        taTools.outdent.iconclass = 'icon-format-indent-decrease';
        taTools.html.iconclass = 'icon-code-tags';
        taTools.insertImage.iconclass = 'icon-file-image-box';
        taTools.insertLink.iconclass = 'icon-link';
        taTools.insertVideo.iconclass = 'icon-filmstrip';

        return taTools;
      }
    ]);

  }

})();
