(function () {
  'use strict';

  angular
    .module('sochara')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    // var token = localStorage.getItem('ats');
    // if( !token ){
    //
    // }


    $urlRouterProvider.otherwise('/dashboard');



    // State definitions
    $stateProvider
      .state('app', {
        abstract: true,
        views: {
          'main@': {
            templateUrl: 'app/core/layouts/vertical-navigation.html',
            controller: 'MainController as vm'
          },
          'toolbar@app': {
            templateUrl: 'app/toolbar/layouts/vertical-navigation/toolbar.html',
            controller: 'ToolbarController as vm'
          }
        }
      });
  }

})();
