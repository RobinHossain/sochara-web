module.exports = function(grunt) {


  // path.join(conf.paths.src, '/app/core/scss/**/*.scss'),
  //   path.join(conf.paths.src, '/app/core/**/*.scss'),
  //   path.join(conf.paths.src, '/app/**/*.scss'),
  //   path.join('!' + conf.paths.src, '/app/main/components/material-docs/demo-partials/**/*.scss'),
  //   path.join('!' + conf.paths.src, '/app/core/scss/partials/**/*.scss'),
  //   path.join('!' + conf.paths.src, '/app/index.scss')


  grunt.config.set('sass', {
    dev: {
      files: [{
        expand: true,
        cwd: 'assets/app/',
        src: ['core/scss/**/*.scss', 'core/**/*.scss', '**/*.scss',
          'main/components/material-docs/demo-partials/**/*.scss', '!core/scss/partials/**/*.scss',
          '!index.scss', 'core/directives/**/*.scss'],
        dest: '.tmp/public/styles/app/',
        ext: '.css'
      }],
      options: {
        read: false,
        style: 'expanded'
      },
    }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
};
