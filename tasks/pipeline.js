/**
 * grunt/pipeline.js
 *
 * The order in which your css, javascript, and template files should be
 * compiled and linked from your views and static HTML files.
 *
 * (Note that you can take advantage of Grunt-style wildcard/glob/splat expressions
 * for matching multiple files, and ! in front of an expression to ignore files.)
 *
 * For more information see:
 *   https://github.com/balderdashy/sails-docs/blob/master/anatomy/myApp/tasks/pipeline.js.md
 */


// CSS files to inject in order
//
// (if you're using LESS with the built-in default config, you'll want
//  to change `assets/styles/importer.less` instead.)
var cssFilesToInject = [
  '/vendors/perfect-scrollbar/css/perfect-scrollbar.css',
  '/vendors/spectrum/spectrum.css',
  '/app/core/directives/sochara-lightbox/light-box.directive.css',
  '/vendors/jquery-ui/themes/base/jquery-ui.min.css',
  '/vendors/plyr/dist/plyr.css',
  '/vendors/SocharaPhotoEditor/css/PhotoEditorReactUI.min.css',
  '/styles/index.css',
  '/vendors/skypecon/dist/css/skypecon.min.css',
  '/vendors/fullcalendar/dist/fullcalendar.css',
  '/vendors/mdPickers/dist/mdPickers.min.css',
  '/vendors/ng-img-crop-full-extended/compile/minified/ng-img-crop.css',
  '/vendors/ng-emoticons/dist/ng-emoticons.min.css',
  '/vendors/jquery-modal/jquery.modal.css',
  '/vendors/textAngular/dist/textAngular.css',
  '/vendors/emoji-picker/lib/css/emoji.css',
  '/vendors/font-awesome/css/font-awesome.min.css',
  '/app/main/labels/fontselect.css',
  '/app/main/calendar/angular-tiny-calendar/tiny-calendar.min.css',
  '/styles/importer.css'
  // '/styles/print_style.css'
  // 'styles/**/*.css'
];


// Client-side javascript files to inject in order
// (uses Grunt-style wildcard/glob/splat expressions)
// var jsFilesToInject = [
//
//   // Load sails.io before everything else
//   'js/dependencies/sails.io.js',
//   'js/dependencies/angular.js',
//
//   // Dependencies like jQuery, or Angular are brought in here
//   'js/dependencies/**/*.js',
//
//   // All of the rest of your client-side js files
//   // will be injected here in no particular order.
//   'js/app.js',
//   'js/services/accessLevels.js',
//   'js/**/*.js'
// ];

var jsFilesToInject = [

  // Load sails.io before everything else
  'js/dependencies/sails.io.js',
  '/vendors/jquery/dist/jquery.js',
  '/vendors/angular/angular.js',
  '/vendors/angular-animate/angular-animate.js',
  '/vendors/angular-aria/angular-aria.js',
  '/vendors/angular-material/angular-material.js',
  '/vendors/angular-sanitize/angular-sanitize.js',
  '/vendors/angular-ui-router/release/angular-ui-router.js',
  '/vendors/mobile-detect/mobile-detect.js',
  '/vendors/moment/moment.js',
  '/vendors/moment-range/dist/moment-range.min.js',
  '/vendors/angular-moment/angular-moment.min.js',
  '/vendors/perfect-scrollbar/js/perfect-scrollbar.js',
  '/vendors/fullcalendar/dist/fullcalendar.js',
  '/vendors/angular-ui-calendar/src/calendar.js',

  // Widget
  '/vendors/jquery-ui/jquery-ui.min.js',
  '/vendors/rangy/rangy-core.js',
  '/vendors/rangy/rangy-classapplier.js',
  '/vendors/rangy/rangy-highlighter.js',
  '/vendors/rangy/rangy-selectionsaverestore.js',
  '/vendors/rangy/rangy-serializer.js',
  '/vendors/ng-file-upload/ng-file-upload.min.js',
  '/vendors/spectrum/spectrum.js',
  '/app/core/directives/sochara-lightbox/light-box.directive.js',

  '/app/core/core.module.js',
  '/app/core/directives/ms-splash-screen/ms-splash-screen.directive.js',
  '/app/core/directives/ms-search-bar/ms-search-bar.directive.js',
  '/app/core/directives/ms-scroll/ms-scroll.directive.js',
  '/app/core/directives/color-picker.directive.js',
  '/app/core/directives/ms-info-bar/ms-info-bar.directive.js',
  '/app/core/directives/ms-form-wizard/ms-form-wizard.directive.js',
  '/vendors/ng-img-crop-full-extended/compile/unminified/ng-img-crop.js',
  '/vendors/mdPickers/dist/mdPickers.min.js',
  '/app/core/directives/ng-draggable.js',
  '/vendors/hammerjs/hammer.min.js',
  '/vendors/angular-hammer/angular-hammer.js',
  '/vendors/base-64/base64.js',
  '/vendors/quoted-printable/quoted-printable.js',
  '/vendors/utf8/utf8.js',
  '/app/core/theming/sochara-theming.service.js',
  '/app/core/theming/sochara-theming.config.js',
  '/app/core/theming/sochara-themes.constant.js',
  '/app/core/theming/sochara-palettes.constant.js',
  '/app/core/theming/sochara-generator.service.js',
  '/app/core/services/ms-utils.service.js',
  '/app/core/filters/filterByIds.filter.js',
  '/app/core/filters/basic.filter.js',
  '/app/core/filters/altDate.filter.js',
  '/app/core/directives/sochara.directive.js',
  '/app/core/config/sochara-config.provider.js',
  '/app/toolbar/toolbar.module.js',
  '/app/toolbar/toolbar.controller.js',
  '/app/index.module.js',
  '/app/main/main.service.js',
  '/app/main/main.controller.js',

  '/app/main/auth/login/login.module.js',
  '/app/main/auth/login/login.controller.js',
  '/vendors/packery/dist/packery.pkgd.js',


  '/app/main/dashboard/dashboard.module.js',
  '/app/main/dashboard/dashboard.service.js',
  '/app/main/dashboard/dashboard.controller.js',

  '/vendors/ng-file-upload/ng-file-upload-shim.min.js',

  '/vendors/plyr/dist/plyr.js',
  '/vendors/textAngular/dist/textAngularSetup.js',
  '/vendors/textAngular/dist/textAngular.js',
  '/vendors/textAngular/dist/textAngular.umd.js',
  '/vendors/textAngular/dist/textAngular-sanitize.js',
  '/vendors/textAngular/dist/textAngular-rangy.min.js',


  // '/app/main/dashboardsec/dashboardsec.module.js',
  // '/app/main/dashboardsec/dashboardsec.controller.js',


  '/app/main/auth/register/register.module.js',
  '/app/main/auth/register/register.controller.js',

  '/app/main/pages/page.module.js',
  '/app/main/pages/page.controller.js',

  '/app/main/auth/forgot-password/forgot-password.module.js',
  '/app/main/auth/forgot-password/forgot-password.controller.js',

  '/app/main/auth/reset-password/reset-password.module.js',
  '/app/main/auth/reset-password/reset-password.controller.js',

  '/app/main/setting/global.module.js',
  '/app/main/setting/global.service.js',
  '/app/main/setting/global.controller.js',

  '/app/main/profile/profile.module.js',
  '/app/main/profile/popup/settings-popup.controller.js',
  '/app/main/profile/profile.service.js',
  '/app/main/profile/profile.controller.js',

  '/app/main/contacts/contacts.module.js',
  '/app/main/contacts/contacts.service.js',
  '/app/main/contacts/contacts.controller.js',


  '/app/main/labels/label.module.js',
  '/app/main/labels/label.service.js',
  '/app/main/labels/label.controller.js',
  // '/app/main/labels/jquery.fontselect.js',

  '/app/main/calendar/angular-tiny-calendar/tiny-calendar.js',
  '/app/main/calendar/calendar.module.js',
  '/app/main/calendar/calendar.service.js',
  '/app/main/calendar/calendar.controller.js',
  '/app/main/calendar/dialogs/event-detail/event-detail-dialog.controller.js',
  '/app/main/calendar/dialogs/event-form/event-form-dialog.controller.js',

  '/app/main/cards/cards.module.js',
  '/app/main/cards/card.service.js',
  '/app/main/cards/cards.controller.js',

  '/app/main/poll/poll.module.js',
  '/app/main/poll/poll.service.js',
  '/app/main/poll/poll.controller.js',

  '/app/main/mail/mail.module.js',
  '/app/main/mail/dialogs/compose/compose-dialog.controller.js',
  '/app/main/mail/mail.service.js',
  '/app/main/mail/mail.controller.js',

  '/app/main/chat/chat.module.js',
  '/app/main/chat/chat.service.js',
  '/app/main/chat/chat.controller.js',

  '/app/main/media/media.module.js',
  '/app/main/media/media.service.js',
  '/app/main/media/media.controller.js',

  '/app/main/files/files.module.js',
  '/app/main/files/files.service.js',
  '/app/main/files/files.controller.js',

  '/app/core/core.run.js',
  '/app/core/core.config.js',
  '/app/index.run.js',
  '/app/index.route.js',
  '/app/index.controller.js',
  '/app/index.constants.js',
  '/app/index.config.js',
  '/app/index.api.js',

  '/app/main/auth/services/auth.service.js',

  '/vendors/html2canvas/build/html2canvas.min.js'

  // All of the rest of your client-side js files
  // will be injected here in no particular order.
  // 'js/app.js',
  // 'js/services/accessLevels.js',
  // 'js/**/*.js'
];


// Client-side HTML templates are injected using the sources below
// The ordering of these templates shouldn't matter.
// (uses Grunt-style wildcard/glob/splat expressions)
//
// By default, Sails uses JST templates and precompiles them into
// functions for you.  If you want to use jade, handlebars, dust, etc.,
// with the linker, no problem-- you'll just want to make sure the precompiled
// templates get spit out to the same file.  Be sure and check out `tasks/README.md`
// for information on customizing and installing new tasks.
var templateFilesToInject = [
  // 'templates/**/*.html'
];


// Default path for public folder (see documentation for more information)
var tmpPath = '.tmp/public/';

// Prefix relative paths to source files so they point to the proper locations
// (i.e. where the other Grunt tasks spit them out, or in some cases, where
// they reside in the first place)
module.exports.cssFilesToInject = cssFilesToInject.map(function (cssPath) {
  // If we're ignoring the file, make sure the ! is at the beginning of the path
  if (cssPath[0] === '!') {
    return require('path').join('!.tmp/public/', cssPath.substr(1));
  }
  return require('path').join('.tmp/public/', cssPath);
});
module.exports.jsFilesToInject = jsFilesToInject.map(function (jsPath) {
  // If we're ignoring the file, make sure the ! is at the beginning of the path
  if (jsPath[0] === '!') {
    return require('path').join('!.tmp/public/', jsPath.substr(1));
  }
  return require('path').join('.tmp/public/', jsPath);
});
module.exports.templateFilesToInject = templateFilesToInject.map(function (tplPath) {
  // If we're ignoring the file, make sure the ! is at the beginning of the path
  if (tplPath[0] === '!') {
    return require('path').join('!assets/', tplPath.substr(1));
  }
  return require('path').join('assets/', tplPath);
});


