// Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license. See full license at the bottom of this file.

module.exports = function (req, res, next) {

  'use strict';
  const outlook = require('nodejs-outlook');

  let host = req.get('host');

  let credentials = {
    client: {
      id: '55a857a7-806b-4f56-9c0a-c086a8c9ab95',
      secret: 'ybNEFBRxvwyYV2MzFGvEpkX',
    },
    auth: {
      tokenHost: 'https://login.microsoftonline.com',
      authorizePath: 'common/oauth2/v2.0/authorize',
      tokenPath: 'common/oauth2/v2.0/token'
    }
  };
  let outlookOauth2 = require('simple-oauth2').create(credentials);

  // var redirectUri = 'http://localhost:8000/authorize';
  // var redirectUri = 'http://localhost:1337/admin/outlook/oauth2callback';

  let redirectUri = req.protocol + '://' + req.get('host') + '/outlook/oauth2callback';


  let scopes = ['openid',
    'offline_access',
    'https://outlook.office.com/mail.readwrite',
    'https://outlook.office.com/mail.send',
    'https://outlook.office.com/calendars.readwrite',
    'https://outlook.office.com/contacts.readwrite'
  ];

  req.getAuthUrl = function ( redirectUriParam, scopeParams ) {
    let redirectUrl = redirectUriParam || redirectUri;
    scopes = scopeParams || scopes;
    return outlookOauth2.authorizationCode.authorizeURL({
      redirect_uri: redirectUrl,
      scope: scopes.join(' ')
    });
  };


  req.getTokenFromCode = function (codeObj, callback) {
    let Token, getToken = {}, redirectUrl = codeObj && codeObj.uri ?  codeObj.uri : redirectUri;
    scopes = codeObj && codeObj.scopes ? codeObj.scopes : scopes;
    outlookOauth2.authorizationCode.getToken({
      code: codeObj.code,
      redirect_uri: redirectUrl,
      scope: scopes.join(' ')
    }, function (error, result) {
      if (error) {
        console.log('Access token error: ', error.message);
        callback(error);
      } else {
        Token = outlookOauth2.accessToken.create(result);
        getToken.access_token = Token.token.access_token
        getToken.id_token = Token.token.id_token
        getToken.refresh_token = Token.token.refresh_token
        getToken.token_type = Token.token.token_type
        getToken.scope = Token.token.scope
        getToken.expires_at = Token.token.expires_at
        // console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        // console.log(getToken);
        callback(getToken);
      }
    });
  };

  req.getEmailUpdate = function (token, email, threadID, updateData, callback) {

    // Set the API endpoint to use the v2.0 endpoint
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    // Set the anchor mailbox to the user's SMTP address
    outlook.base.setAnchorMailbox(email);

    outlook.mail.updateMessage({token: token, messageId: threadID, update: updateData},
      function (error, result) {
        if (error) {
          console.log('getMessages returned an error: ' + error);
        }
        else if (result) {
          callback({'updated': 'done'});
          // console.log('getMessages returned ' + result.value.length + ' messages.');
          // result.value.forEach(function(message) {
          //   console.log('  Subject: ' + message.Subject);
          //   var from = message.From ? message.From.EmailAddress.Name : 'NONE';
          //   console.log(message);
          // });;
        }
      });
  };

  req.getEmailsByFilter = function (token, email, folder, top, skip, filter, callback) {

    var queryParams = {
      // '$select': 'Subject,ReceivedDateTime,From,IsRead',
      '$orderby': 'ReceivedDateTime desc',
      '$top': top,
      '$skip': skip,
      '$count': 'true',
      '$filter': filter
    };

    // Set the API endpoint to use the v2.0 endpoint
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    // Set the anchor mailbox to the user's SMTP address
    outlook.base.setAnchorMailbox(email);

    outlook.mail.getMessages({token: token, folderId: folder, odataParams: queryParams},
      function (error, result) {
        if (error) {
          console.log('getMessages returned an error: ' + error);
        }
        else if (result) {
          callback(result);
        }
      });
  };

  req.getCurrentMailAttachment = function (token, email, mailId, callback) {
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    outlook.base.setAnchorMailbox(email);

    outlook.mail.getMailAttachement({token: token, mailId: mailId},
      function (error, result) {
        if (error) {
          console.log('getMessages returned an error: ' + error);
        }
        else if (result) {
          callback(result);
        }
      });
  };

  req.createNewLabel = function (token, email, parentId, name, callback) {
    // Set the API endpoint to use the v2.0 endpoint
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    // Set the anchor mailbox to the user's SMTP address
    outlook.base.setAnchorMailbox(email);

    outlook.mail.createNewLabel({token: token, parentId: parentId, name: name},
      function (error, result) {
        if (error) {
          console.log('getMessages returned an error: ' + error);
        }
        callback({'updated': 'done'});
      });
  };

  req.moveOrCopyMessageTo = function (token, email, threadID, update, action, callback) {

    // Set the API endpoint to use the v2.0 endpoint
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    // Set the anchor mailbox to the user's SMTP address
    outlook.base.setAnchorMailbox(email);

    var updateData = {};
    updateData.DestinationId = update;

    outlook.mail.moveOrCopyMessageTo({token: token, messageId: threadID, update: updateData, action: action},
      function (error, result) {
        if (error) {
          console.log('getMessages returned an error: ' + error);
        }
        else if (result) {
          callback({'updated': 'done'});
          // console.log('getMessages returned ' + result.value.length + ' messages.');
          // result.value.forEach(function(message) {
          //   console.log('  Subject: ' + message.Subject);
          //   var from = message.From ? message.From.EmailAddress.Name : 'NONE';
          //   console.log(message);
          // });;
        }
      });
  };


  req.getEmailDelete = function (token, email, threadID, callback) {

    // Set the API endpoint to use the v2.0 endpoint
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    // Set the anchor mailbox to the user's SMTP address
    outlook.base.setAnchorMailbox(email);

    outlook.mail.deleteMessage({token: token, messageId: threadID},
      function (error, result) {
        if (error) {
          console.log(error);
        }
        else if (result) {
          callback(result);
        }
      });
  };

  req.getEmailSendWithAttachment = function (token, email, message, name, content, callback) {

    // Set the API endpoint to use the v2.0 endpoint
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    // Set the anchor mailbox to the user's SMTP address
    outlook.base.setAnchorMailbox(email);

    outlook.mail.sendAttachmentNewMessage({token: token, message: message, name: name, content: content},
      function (error, result) {
        if (error) {
          console.log(error);
        }
        callback({'sent': true});
      });
  };

  req.getOutlookEmailSend = function (token, email, message, callback) {

    // Set the API endpoint to use the v2.0 endpoint
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    // Set the anchor mailbox to the user's SMTP address
    outlook.base.setAnchorMailbox(email);

    outlook.mail.sendNewMessage({token: token, message: message},
      function (error, result) {
        if (error) {
          console.log(error);
        }
        callback({'sent': true});
      });
  };


  req.emailToSaveFolder = function (token, email, message, folder, callback) {

    // Set the API endpoint to use the v2.0 endpoint
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    // Set the anchor mailbox to the user's SMTP address
    outlook.base.setAnchorMailbox(email);

    outlook.mail.saveMassageToFolder({token: token, message: message, folder: folder},
      function (error, result) {
        if (error) {
          console.log(error);
        }
        else if (result) {
          callback(result);
        }
      });
  };


  /**
   *
   * @param token
   * @param email
   * @param folder
   * @param threadID
   * @param callback
   */
  req.getEmail = function (token, email, threadID, callback) {


    // Set the API endpoint to use the v2.0 endpoint
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    // Set the anchor mailbox to the user's SMTP address
    outlook.base.setAnchorMailbox(email);

    outlook.mail.getMessage({token: token, messageId: threadID},
      function (error, result) {
        if (error) {
          console.log('getMessages returned an error: ' + error);
        }
        else if (result) {
          callback(result);

        }
      });
  };

  req.getEmails = function (token, email, folder, top, skip, callback) {

    var queryParams = {
      // '$select': 'Subject,ReceivedDateTime,From,IsRead',
      '$orderby': 'ReceivedDateTime desc',
      '$top': top,
      '$skip': skip,
      '$count': 'true'
    };

    // Set the API endpoint to use the v2.0 endpoint
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    // Set the anchor mailbox to the user's SMTP address
    outlook.base.setAnchorMailbox(email);

    outlook.mail.getMessages({token: token, folderId: folder, odataParams: queryParams},
      function (error, result) {
        if (error) {
          console.log('getMessages returned an error: ' + error);
        }
        else if (result) {
          callback(result);
        }
      });
  };

  req.getMailFolders = function (token, email, callback) {
    var queryParams = {
      // '$select': 'Subject,ReceivedDateTime,From,IsRead',
      '$orderby': 'ReceivedDateTime desc',
    };

    // Set the API endpoint to use the v2.0 endpoint
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    // Set the anchor mailbox to the user's SMTP address
    outlook.base.setAnchorMailbox(email);

    outlook.mail.getFolders({token: token},
      function (error, result) {
        if (error) {
          console.log('getMessages returned an error: ' + error);
        }
        else if (result) {
          callback(result);
          // console.log('getMessages returned ' + result.value.length + ' messages.');
          // result.value.forEach(function(message) {
          //   console.log('  Subject: ' + message.Subject);
          //   var from = message.From ? message.From.EmailAddress.Name : 'NONE';
          //   console.log(message);
          // });;
        }
      });
  };

  req.getMailSubFolders = function (token, email, folder, callback) {

    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    // Set the anchor mailbox to the user's SMTP address
    outlook.base.setAnchorMailbox(email);

    outlook.mail.getSubFolders({token: token, folder: folder},
      function (error, result) {
        if (error) {
          console.log('getMessages returned an error: ' + error);
        }
        else if (result) {
          callback(result);
        }
      });
  };


  req.getMailFolder = function (token, email, folder, callback) {
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    outlook.base.setAnchorMailbox(email);
    outlook.mail.getFolder({token: token, folder: folder},
      function (error, result) {
        if (error) {
          console.log('getMessages returned an error: ' + error);
        }
        else if (result) {
          callback(result);
        }
      });
  };

  req.getUserEmail = function (token, callback) {
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');

    // Set up oData parameters
    var queryParams = {
      '$select': 'DisplayName, EmailAddress',
    };

    outlook.base.getUser({token: token.access_token, odataParams: queryParams}, function (error, user) {
      if (error) {
        callback(error, null);
      } else {
        callback(null, user);
      }
    });
  };


  req.tokenReceived = function (response, error, token) {
    if (error) {
      console.log('Access token error: ', error.message);
    } else {
      getUserEmail(token.token.access_token, function (error, email) {
        if (error) {
          console.log('getUserEmail returned an error: ' + error);
        } else if (email) {
          var cookies = ['node-tutorial-token=' + token.token.access_token + ';Max-Age=4000',
            'node-tutorial-refresh-token=' + token.token.refresh_token + ';Max-Age=4000',
            'node-tutorial-token-expires=' + token.token.expires_at.getTime() + ';Max-Age=4000',
            'node-tutorial-email=' + email + ';Max-Age=4000'];
        }
      });
    }
  };


  req.refreshAccessToken = function (refreshToken, callback) {
    var tokenObj = outlookOauth2.accessToken.create({refresh_token: refreshToken});
    tokenObj.refresh(callback);
  };

  return next();

}


// exports.getAuthUrl = getAuthUrl;
// exports.getTokenFromCode = getTokenFromCode;
// exports.refreshAccessToken = refreshAccessToken;

