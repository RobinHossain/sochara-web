/**
 * onlineUsers.js
 *
 * @module      :: Policy
 * @description :: Simple policy to set all online and offline users to view
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function (req, res, next) {

  if (req.session && req.session.authenticated) {
    let online_user_id = req.session.User.id;

    try {
      User.update({id:online_user_id},{mode:'online'}).exec(function afterwards(err, updated){});
    } catch (e) {
      console.log("Error in onDisconnect: ", e);
    }
    next();
    // User.find({
    //   id: {'!': [online_user_id]},
    //   'online': true
    // }).exec(function foundUsers(err, users) {
    //   if (err) return next(err);
    //   res.locals.online_users = users;
    //   //console.log("online_users");
    //   //console.log(users);
    //
    //   User.find({
    //     'online': false
    //   }).exec(function foundUsers(err, users) {
    //     if (err) return next(err);
    //     res.locals.offline_users = users;
    //     //console.log("offline_users");
    //     //console.log(users);
    //     next();
    //   });
    // });
  }
}
