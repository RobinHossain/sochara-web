/*
 Help Links:
 https://github.com/emailjs/emailjs-imap-client
 https://github.com/eleith/emailjs
 */

var ImapClient = require('emailjs-imap-client')

module.exports = function(req, res, next) {
    return next();

    var CLIENT_ID = sails.config.email.google_app_client_id;
    var CLIENT_SECRET = sails.config.email.google_app_secret;
    var LoggedInUserID = req.session.User.id;
    User.findOne(LoggedInUserID, function froundCB(err, loggedInUser) {
        if (loggedInUser.access_token && loggedInUser.refresh_token && loggedInUser.email) {
            req.gmail_client = new ImapClient("imap.gmail.com", 993, {
                useSecureTransport: true,
                auth:{
                    user: loggedInUser.email,
                    xoauth2:loggedInUser.access_token
                },
                enableCompression: true
            });

            // req.gmail_client.connect().then(() => {
            //     req.gmail_client.listMailboxes().then((mailboxes) => {
            //         console.log("mailboxes");
            //         console.log(mailboxes);
            //     });
            // });

            return next();
        } else {
            return next();
        }
    });
};
