/*
Helpful Links:
https://developers.google.com/gmail/api/v1/reference/
https://developers.google.com/gmail/api/guides/push#getting_gmail_mailbox_updates
https://github.com/Flolagale/mailin
*/
module.exports = function(req, res, next) {

    return next();

    var LoggedInUserID = req.session.User.id;
    GmailAccount.findOne(LoggedInUserID, function froundCB(err, loggedInUser) {
        if (loggedInUser.access_token && loggedInUser.refresh_token && loggedInUser.email) {

            req.oauth2Client.setCredentials({
                access_token: loggedInUser.access_token,
                refresh_token: loggedInUser.refresh_token
            });

            req.gmail.users.watch({ resource: {name: 'INBOX'}, userId: 'me', auth: req.oauth2Client }, function (err, data) {
                if (err) console.log(err);

                console.log(data);

                return next();
            });

        } else {
            return next();
        }
    });
};
