/*
 Help Links:
 http://www.geektantra.com/2013/08/implement-passport-js-authentication-with-sails-js/
 https://github.com/mscdex/node-imap
 https://github.com/pipedrive/inbox
 https://github.com/Flolagale/mailin
 https://www.google.com/settings/security/lesssecureapps
 https://github.com/eleith/emailjs
 */

var inbox = require("inbox"),
  util = require("util");

module.exports = function (req, res, next) {

  var MailInfo = {
    "email": "sochara.dev@gmail.com",
    "pass": "$sochara.dev*"
  };
  /*req.gmail_client = inbox.createConnection(false, "imap.gmail.com", {
      secureConnection: true,
      auth:{
          user: MailInfo.email,
          pass: MailInfo.pass
      }
  });*/

  var CLIENT_ID = sails.config.email.google_app_client_id;
  var CLIENT_SECRET = sails.config.email.google_app_secret;
  var LoggedInUserID = req.session.User.id;
  User.findOne(LoggedInUserID, function froundCB(err, loggedInUser) {
    if (loggedInUser.access_token && loggedInUser.refresh_token && loggedInUser.email) {
      req.gmail_client = inbox.createConnection(false, "imap.gmail.com", {
        secureConnection: true,
        debug: true,
        /*auth:{
            user: MailInfo.email,
            pass: MailInfo.pass
        },*/
        auth: {
          XOAuth2: {
            user: loggedInUser.email,
            clientId: CLIENT_ID,
            clientSecret: CLIENT_SECRET,
            refreshToken: loggedInUser.refresh_token,
            accessToken: loggedInUser.access_token,
            timeout: 3600
          }
        }
      });

      req.gmail_client.connect();
      req.gmail_client.on("connect", function () {
        req.gmail_client.on("new", function (message) {
          console.log("New message:");
          console.log(message.UID + ": " + message.title + ": " + message.flags);
          //emit a socket event with this message to the client
        });
        return next();
      });
    } else {
      return next();
    }
  });
};
