/*
 Help Links:
 https://github.com/google/google-api-nodejs-client
 https://cloud.google.com/pubsub/quickstart-console
 https://developers.google.com/gmail/api/guides/sync#full_synchronization

 https://developers.google.com/gmail/api/v1/reference/users/labels/create#parameters
 https://github.com/Flolagale/mailin

 So far seems this policy is working properly with Google Plus profile information!
 */

const google = require('googleapis');

module.exports = function (req, res, next) {
  var host = req.get('host') || '';
  var CLIENT_ID = sails.config.email.google_app_client_id;
  var CLIENT_SECRET = sails.config.email.google_app_secret;
  var REDIRECT_URL;
  var REDIRECT_URL_Login;
  if (host.indexOf('localhost') !== -1) {
    REDIRECT_URL_Login = req.protocol + '://' + req.get('host') + '/auth/gmailAuthCallback';
    REDIRECT_URL = req.protocol + '://' + req.get('host') + '/gmail/oauth2callback';
  } else {
    REDIRECT_URL_Login = 'https://www.sochara.com' + '/auth/gmailAuthCallback';
    REDIRECT_URL = 'https://www.sochara.com' + '/gmail/oauth2callback';
  }





  var OAuth2Client = google.auth.OAuth2;
  req.google = google;
  req.plus = google.plus('v1');
  req.gmail = google.gmail('v1');
  req.calendar = google.calendar('v3');
  req.oauth2Client = new OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);
  req.oauth2ClientLogin = new OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL_Login);
  req.googleAuthUrl = req.oauth2Client.generateAuthUrl({
    approval_prompt: 'force', // will return a refresh token each time
    access_type: 'offline', // will return a refresh token because we are going to access the api later with this refresh token
    scope: [
      'https://www.googleapis.com/auth/plus.me', // can be a space-delimited string or an array of scopes
      'https://www.googleapis.com/auth/plus.profile.emails.read',
      'https://mail.google.com/',
      'https://www.googleapis.com/auth/gmail.modify',
      'https://www.googleapis.com/auth/gmail.insert',
      'https://www.googleapis.com/auth/gmail.send',
      'https://www.googleapis.com/auth/gmail.labels',
      'https://www.googleapis.com/auth/gmail.compose',
      'https://www.googleapis.com/auth/calendar'
    ]
  });

  // live
  //z0fl9Hm4LOCgYyOngM416ZhC
  //678407714922-qv5bnp6mjhvbsjt14bcv9740clnb09ec.apps.googleusercontent.com
  req.googleLoginUri = req.oauth2ClientLogin.generateAuthUrl({
    approval_prompt: 'force',
    access_type: 'offline',
    scope: [
      'https://www.googleapis.com/auth/plus.login', // can be a space-delimited string or an array of scopes
      'https://www.googleapis.com/auth/userinfo.email',
      'https://www.googleapis.com/auth/userinfo.profile',
    ]
  });

  return next();
};
