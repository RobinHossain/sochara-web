/**
 * Utility.js
 *
 * @module      :: Service
 * @description :: Simple service to store all Utility functions
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Services
 *
 */
module.exports = {

  /**
   * Description
   * @method title_to_slug
   * @param {} $str
   * @return $str
   */
  title_to_slug: function($str) {
    $str = $str.replace(/[^a-zA-Z0-9\s]/g,"");
    $str = $str.toLowerCase();
    $str = $str.replace(/\s/g,'-');
    return $str;
  },

  /**
   * Description
   * @method getSiteBaseURL
   * @param {} req
   * @return BinaryExpression
   */
  getSiteBaseURL: function(req) {
      return req.protocol + '://' + req.headers.host + '';
  },
    
    /**
     * Description
     * @method file_check_and_delete
     * @param {} file
     * @param {} callback
     * @return 
     */
    file_check_and_delete: function(file, callback)  {
        const fs = require('fs');
        fs.exists(file, (exists) => {
            if (exists) {
                fs.unlink(file, function(err) {
                    if (err) throw err;
                    callback();
                });
            } else {
                callback();
            }
        });
    }

}
