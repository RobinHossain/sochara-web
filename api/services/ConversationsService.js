"use srict";
var fs = require('fs');
var path = require('path');
var moment = require('moment');

module.exports = {
  /**
   * Description
   * @method getConversations
   * @param {} user_id
   * @param {} next
   * @return 
   */
  getConversations: function(user_id, next) {
    Conversations.find().populate('users', {select: ['_id','first_name', 'last_name', 'online', 'photo_url', 'seen']}).populate('unread', {where: {user: user_id}}).populate('created_by').populate('last_message_by').sort('updatedAt desc').exec(function(err, ConversationLists) {
      if(err) throw err;
      var result = [];
      ConversationLists.forEach(function(Conversation) {
        if (Conversation && Conversation.users && Conversation.users.length > 0 ) {
          for(var i=0; i < Conversation.users.length; i++) {
            if ( Conversation.users[i].id == user_id ) {
              result.push(Conversation);
            }
          }
        }
      });

      next(result);
    });
  },
  /**
   * Description
   * @method getConversationUsers
   * @param {} id
   * @param {} next
   * @return 
   */
  getConversationUsers: function(id, next) {
    if ( typeof id === "undefined" ) next();
    Conversations.findOne(id).populate('users').exec(function(err, Conversation) {

      if(err) throw err;

      var result = [];
      if (Conversation && Conversation.users && Conversation.users.length > 0 ) {
        for(var i=0; i < Conversation.users.length; i++) {
          result.push(Conversation.users[i].id);
        }
      }

      next(result);
    });
  },


  /**
   * Description
   * @return
   * @method checkConversationMute
   * @param {} id
   * @param {} user_id
   * @param {} next
   * @return 
   */
  checkConversationMute: function(id, user_id, next) {
    if ( typeof id === "undefined" ) next();
    ConversationMute.find({ conversation_id: id, user_id: user_id }).exec(function(err, resp) {
      if( resp && resp.length ){
        next('yes');
      }else{
        next('no');
      }
    });
  },
  /**
   * Description
   * @method addConversations
   * @param {} values
   * @param {} next
   * @return 
   */
  addConversations: function(values, next) {
    Conversations.create(values).exec(function(err, Conversation) {
      if(err) throw err;
      next(Conversation);
    });
  },

  /**
   * Description
   * @method checkIfValidSocketAlreadyExistsInRoom
   * @param {} check_socket_id
   * @param {} conversation_name
   * @return return_bool
   */
  checkIfValidSocketAlreadyExistsInRoom: function(check_socket_id, conversation_name) {
    var return_bool = false;
    var subscribers = sails.sockets.subscribers(conversation_name);
    for (var socket_id in subscribers) {
      if (subscribers[socket_id] == check_socket_id) {
        return_bool = true;
      }
    }
    return return_bool;
  },
  /**
   * Description
   * @method checkIfSocketIdValid
   * @param {} id
   * @return 
   */
  checkIfSocketIdValid: function(id) {
    // Cast `id` to string
    id = id+'';

    var namespaceId = '/';

    var namespace = sails.io.of(namespaceId);

    var foundSocket = namespace.connected[id+''];
    if (!foundSocket) {
      return false;
    } else {
      return true;
    }
  },
  
  /**
   * Description
   * @method deleteAttachment
   * @param {} id
   * @param {} userId
   * @return Literal
   */
  deleteAttachment: function (id, userId) {
    UserFiles.findOne(id, function foundCB(err, record) {
      if (err){ console.log(err); }

      if (record) {
        UserFiles.destroy({id: id, user_id: userId}, function destroyedCB(err) {
          if (err){ console.log(err); }

          // For Main File
          var fileName = path.basename(record.file_dir);
          var fileNameOnly = fileName.substr(0, fileName.lastIndexOf('.'))
          var fileDir = 'assets/uploads/files/' + userId + '/chat/' + fileName;
          var fileThumbDir = 'assets/uploads/files/' + userId + '/chat/thumb/' + fileNameOnly + '.jpg';
          var filePoserDir = 'assets/uploads/files/' + userId + '/chat/poster/' + fileNameOnly + '.jpeg';


          if (fs.existsSync(fileDir)) {
            fs.unlink(fileDir, (err) => {
              if (err) { throw err; }

              if (fs.existsSync(fileThumbDir)) {
                fs.unlink(fileThumbDir, (err) => {
                  if(err) { console.log(err) ; }

                  if (fs.existsSync(filePoserDir)) {
                    fs.unlink(filePoserDir, (err) => {
                      if(err) { console.log(err) ; }
                    });
                  }

                });
              }

            });
          }
          
        });
      }

    });
    
    return true;
    
  }
};
