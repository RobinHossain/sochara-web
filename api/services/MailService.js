var moment = require('moment');
var AsyncCall = require('async');

module.exports = {

  /**
   * Description
   * @method getGmailInboxCount
   * @param {} req
   * @param {} data
   * @param {} callback
   * @return 
   */
  getGmailInboxCount: function (req, data, callback) {
    EmailAccount.findOne({
      user: data.userId,
      provider: 'gmail',
      select: ['email', 'access_token', 'refresh_token']
    }).exec(function foundCB(err, record) {
      if (record) {
        req.oauth2Client.setCredentials({
          access_token: record.access_token,
          refresh_token: record.refresh_token
        });
        // req.gmail.users.getProfile({userId: record.email, auth: req.oauth2Client}, function (err, data) {
        req.gmail.users.labels.get({id: 'INBOX', userId: record.email, auth: req.oauth2Client}, function (err, labelData) {
          if( err ) console.log(err);
          callback(labelData);
        });
      }else{
        callback({});
      }
    });
  },

  /**
   * Description
   * @method getOutlookInboxCount
   * @param {} req
   * @param {} data
   * @param {} callback
   * @return 
   */
  getOutlookInboxCount: function (req, data, callback) {
    EmailAccount.findOne({
      user: data.userId,
      provider: 'outlook',
      select: ['email', 'refresh_token', 'access_token']
    }, function froundCB(err, record) {
      if (record) {
        req.getMailFolder(record.access_token, record.email, 'Inbox', function (resp) {
          callback(resp);
        });
      }else{
        callback({});
      }
    });
  },

  /**
   * Description
   * @method gmailExtract
   * @param {} mailData
   * @param {} gmailMessageData
   * @param {} currentEmail
   * @param {} emailID
   * @param {} next
   * @return 
   */
  gmailExtract: function (mailData, gmailMessageData, currentEmail, emailID, next) {
    if (mailData.threads && mailData.threads.length) {
      mailData.threads.forEach(function (val) {
        // getGMailHeaderValueByName('Subject', val.metadata[0]);
        if (val.metadata && val.metadata.length) {
          var email = {};
          var time;
          email.from = {};
          email.to = {};
          email.mail_id = val.id;
          var fromName = getGMailHeaderValueByName('From', val.metadata[0]);
          if (fromName && fromName.indexOf('<') > -1) {
            email.from.name = fromName.split(" <")[0].replace(/[^a-zA-Z ]/g, "");
            email.from.email = fromName.split(" <")[1].replace(">", "");
          } else {
            email.from.email = fromName;
          }

          email.to.email = getGMailHeaderValueByName('To', val.metadata[0]);
          email.to.name = "me";
          email.message = val.snippet;
          email.subject = getGMailHeaderValueByName('Subject', val.metadata[0]);
          var getDate = getGMailHeaderValueByName('Date', val.metadata[0]);
          if (moment([getDate]).diff(moment(), 'day') > -2) {
            time = moment(new Date(getDate)).format('HH:mm a');
          } else {
            time = moment(new Date(getDate)).format('DD MMM');
          }
          email.time = time;
          // email.date = moment(new Date(getDate)).format('ddd, MMMM Do YYYY [at] HH:mm a');
          email.date = new Date(getDate);
          email.hasAttachments = isHasAttachmentsGmail(val);
          email.read = !val.metadata[0].labelIds.includes('UNREAD');
          email.starred = val.metadata[0].labelIds.includes('STARRED');
          email.important = val.metadata[0].labelIds.includes('IMPORTANT');
          email.labels = val.metadata[0].labelIds;
          email.address = currentEmail;
          email.provider = 'gmail';
          email.email_id = emailID;
          gmailMessageData.messages.push(email);
        }
      });
    }
    next(gmailMessageData);
  },

  /**
   * Description
   * @method gmailExtractAndStore
   * @param {} mailData
   * @param {} currentEmail
   * @param {} emailID
   * @param {} userID
   * @param {} next
   * @return 
   */
  gmailExtractAndStore: function (mailData, currentEmail, emailID, userID, next) {
    if (mailData.threads && mailData.threads.length) {
      AsyncCall.forEach(mailData.threads, function (val, callback) {
        if (val.metadata && val.metadata.length) {
          var email = {};
          var time;
          email.from = {};
          email.to = {};
          email.mail_id = val.id;
          var fromName = getGMailHeaderValueByName('From', val.metadata[0]);
          if (fromName && fromName.indexOf('<') > -1) {
            email.from.name = fromName.split(" <")[0].replace(/[^a-zA-Z ]/g, "") || '';
            email.from.email = fromName.split(" <")[1] ? fromName.split(" <")[1].replace(">", "") : '';
          } else {
            email.from.email = fromName;
          }

          email.to.email = getGMailHeaderValueByName('To', val.metadata[0]);
          email.to.name = "me";
          email.message = val.snippet;
          email.subject = getGMailHeaderValueByName('Subject', val.metadata[0]);
          var getDate = getGMailHeaderValueByName('Date', val.metadata[0]);
          if (moment([getDate]).diff(moment(), 'day') > -2) {
            time = moment(new Date(getDate)).format('HH:mm a');
          } else {
            time = moment(new Date(getDate)).format('DD MMM');
          }
          email.time = time;
          // email.date = moment(new Date(getDate)).format('ddd, MMMM Do YYYY [at] HH:mm a');
          email.date = new Date(getDate);
          email.hasAttachments = isHasAttachmentsGmail(val);
          email.read = !val.metadata[0].labelIds.includes('UNREAD');
          email.starred = val.metadata[0].labelIds.includes('STARRED');
          email.important = val.metadata[0].labelIds.includes('IMPORTANT');
          email.labels = val.metadata[0].labelIds;
          email.address = currentEmail;
          email.provider = 'gmail';
          email.email_id = emailID;
          email.user = userID;


          Email.findOne({ mail_id : email.mail_id, user: userID }).exec(function(err, user) {
            if (err) console.log(err);
            if ( !user ){
              Email.create(email).exec(function (err, createdEmail) {
                if (err) console.log(err);
                if( createdEmail && mailData.newMail && mailData.newMail === 'yes'){
                  // sails.sockets.broadcast(sails.sockets.subscribers('global'), 'refresh_for_new_mail', {data: 'done'});
                  // sails.sockets.broadcast('sochara_' + createdEmail.user, 'new_email', createdEmail);
                  if( createdEmail.user ){
                    sails.sockets.broadcast('sochara_' + createdEmail.user, 'new_email', createdEmail);
                  }
                  callback();
                }else{
                  callback();
                }
              });
            }else{
              callback();
            }
          });
        }
      }, function (err){
        if (err) console.log(err);
        next('done');
      });
    }else{
      next('done');
    }

  },

  /**
   * Description
   * @method gmailStateCheckAndStore
   * @param {} mailData
   * @param {} currentEmail
   * @param {} emailID
   * @param {} userID
   * @param {} next
   * @return 
   */
  gmailStateCheckAndStore: function (mailData, currentEmail, emailID, userID, next) {
    if (mailData.threads && mailData.threads.length) {
      AsyncCall.forEach(mailData.threads, function (val, callback) {
        if (val.metadata && val.metadata.length) {
          var email = {};
          var time;
          email.from = {};
          email.to = {};
          email.mail_id = val.id;
          var fromName = getGMailHeaderValueByName('From', val.metadata[0]);
          if (fromName && fromName.indexOf('<') > -1) {
            email.from.name = fromName.split(" <")[0].replace(/[^a-zA-Z ]/g, "") || '';
            email.from.email = fromName.split(" <")[1] ? fromName.split(" <")[1].replace(">", "") : '';
          } else {
            email.from.email = fromName;
          }

          email.to.email = getGMailHeaderValueByName('To', val.metadata[0]);
          email.to.name = "me";
          email.message = val.snippet;
          email.subject = getGMailHeaderValueByName('Subject', val.metadata[0]);
          var getDate = getGMailHeaderValueByName('Date', val.metadata[0]);
          if (moment([getDate]).diff(moment(), 'day') > -2) {
            time = moment(new Date(getDate)).format('HH:mm a');
          } else {
            time = moment(new Date(getDate)).format('DD MMM');
          }
          email.time = time;
          // email.date = moment(new Date(getDate)).format('ddd, MMMM Do YYYY [at] HH:mm a');
          email.date = new Date(getDate);
          email.hasAttachments = isHasAttachmentsGmail(val);
          email.read = !val.metadata[0].labelIds.includes('UNREAD');
          email.starred = val.metadata[0].labelIds.includes('STARRED');
          email.important = val.metadata[0].labelIds.includes('IMPORTANT');
          email.labels = val.metadata[0].labelIds;
          email.address = currentEmail;
          email.provider = 'gmail';
          email.email_id = emailID;
          email.user = userID;


          Email.findOne({ mail_id : email.mail_id, user: userID, read: email.read, starred: email.starred, important: email.important, labels: email.labels }).exec(function(err, hasExist) {
            if (err) console.log(err);
            if ( !hasExist ){
              Email.update({mail_id : email.mail_id, user: userID}, email).exec(function (err, updatedMails) {
                console.log(updatedMails);
                if (err) console.log(err);
                callback();
              });
            }else{
              callback();
            }
          });
        }
      }, function (err){
        if (err) console.log(err);
        next('done');
      });
    }else{
      next('done');
    }

  },

  /**
   * Description
   * @method outlookExtractAndStore
   * @param {} mailData
   * @param {} currentEmail
   * @param {} emailID
   * @param {} userID
   * @param {} next
   * @return 
   */
  outlookExtractAndStore: function (mailData, currentEmail, emailID, userID, next) {
    if (mailData.value && mailData.value.length) {
      AsyncCall.forEach(mailData.value, function (val, callback) {
        var email = {};
        var time;
        email.from = {};
        email.to = {};
        email.mail_id = val.Id;
        if (val.From.EmailAddress.Name) {
          email.from.name = val.From.EmailAddress.Name;
        }
        if (val.From.EmailAddress.Address) {
          email.from.email = val.From.EmailAddress.Address;
        }
        email.to.email = currentEmail;
        email.to.name = "me";
        email.message = val.BodyPreview;
        email.subject = val.Subject;
        var getDate = val.SentDateTime;
        if (moment([getDate]).diff(moment(), 'day') > -2) {
          time = moment(new Date(getDate)).format('HH:mm a');
        } else {
          time = moment(new Date(getDate)).format('DD MMM');
        }
        email.time = time;
        // email.date = moment(new Date(getDate)).format('ddd, MMMM Do YYYY [at] HH:mm a');
        email.date = new Date(getDate);
        email.hasAttachments = val.HasAttachments;
        email.read = val.IsRead;
        email.starred = false;
        email.important = val.Importance === 'High';
        email.labels = getOutlookLabelIds(val.IsDraft, val.IsRead, val.Categories);
        email.address = currentEmail;
        email.provider = 'outlook';
        email.email_id = emailID;
        email.user = userID;


        Email.findOne({ mail_id : email.mail_id, user: userID }).exec(function(err, user) {
          if (err) console.log(err);
          if ( !user ){
            Email.create(email).exec(function (err, createdEmail) {
              if (err) console.log(err);
              if( createdEmail && mailData.newMail && mailData.newMail === 'yes'){
                // sails.sockets.broadcast(sails.sockets.subscribers('global'), 'refresh_for_new_mail', {data: 'done'});
                sails.sockets.broadcast('sochara_' + createdEmail.user, 'new_email', createdEmail);
                callback();
              }else{
                callback();
              }
            });
          }else{
            callback();
          }
        });
      }, function (err){
        if (err) console.log(err);
        next('done');
      });
    }else{
      next('done');
    }
  },

  /**
   * Description
   * @method outlookCheckStatusAndStore
   * @param {} mailData
   * @param {} emailAccount
   * @param {} next
   * @return 
   */
  outlookCheckStatusAndStore: function (mailData, emailAccount, next) {
    let currentEmail = emailAccount.email, emailID = emailAccount.id, userID = emailAccount.user;
    if (mailData.value && mailData.value.length) {
      AsyncCall.forEach(mailData.value, function (val, callback) {
        var email = {};
        var time;
        email.from = {};
        email.to = {};
        email.mail_id = val.Id;
        if (val.From.EmailAddress.Name) {
          email.from.name = val.From.EmailAddress.Name;
        }
        if (val.From.EmailAddress.Address) {
          email.from.email = val.From.EmailAddress.Address;
        }
        email.to.email = currentEmail;
        email.to.name = "me";
        email.message = val.BodyPreview;
        email.subject = val.Subject;
        var getDate = val.SentDateTime;
        if (moment([getDate]).diff(moment(), 'day') > -2) {
          time = moment(new Date(getDate)).format('HH:mm a');
        } else {
          time = moment(new Date(getDate)).format('DD MMM');
        }
        email.time = time;
        // email.date = moment(new Date(getDate)).format('ddd, MMMM Do YYYY [at] HH:mm a');
        email.date = new Date(getDate);
        email.hasAttachments = val.HasAttachments;
        email.read = val.IsRead;
        email.starred = false;
        email.important = val.Importance === 'High';
        email.labels = getOutlookLabelIds(val.IsDraft, val.IsRead, val.Categories);
        email.address = currentEmail;
        email.provider = 'outlook';
        email.email_id = emailID;
        email.user = userID;

        Email.findOne({ mail_id : email.mail_id, user: userID, read: email.read }).exec(function(err, hasExist) {
          if (err) console.log(err);
          if ( !hasExist ){
            Email.update({mail_id : email.mail_id, user: userID}, email).exec(function (err, updatedMails) {
              if (err) console.log(err);
              callback();
            });
          }else{
            callback();
          }
        });
      }, function (err){
        if (err) console.log(err);
        next('done');
      });
    }else{
      next('done');
    }
  },


  /**
   * Description
   * @method outlookExtract
   * @param {} mailData
   * @param {} outlookMessageData
   * @param {} currentEmail
   * @param {} emailID
   * @param {} next
   * @return 
   */
  outlookExtract: function (mailData, outlookMessageData, currentEmail, emailID, next) {
    if (mailData.value && mailData.value.length) {
      mailData.value.forEach(function (val) {
        var email = {};
        var time;

        email.from = {};
        email.to = {};
        email.mail_id = val.Id;
        if (val.From.EmailAddress.Name) {
          email.from.name = val.From.EmailAddress.Name;
        }
        if (val.From.EmailAddress.Address) {
          email.from.email = val.From.EmailAddress.Address;
        }
        email.to.email = currentEmail;
        email.to.name = "me";
        email.message = val.BodyPreview;
        email.subject = val.Subject;
        var getDate = val.SentDateTime;
        if (moment([getDate]).diff(moment(), 'day') > -2) {
          time = moment(new Date(getDate)).format('HH:mm a');
        } else {
          time = moment(new Date(getDate)).format('DD MMM');
        }
        email.time = time;
        // email.date = moment(new Date(getDate)).format('ddd, MMMM Do YYYY [at] HH:mm a');
        email.date = new Date(getDate);
        email.hasAttachments = val.HasAttachments;
        email.read = val.IsRead;
        email.starred = false;
        email.important = val.Importance === 'High';
        email.labels = getOutlookLabelIds(val.IsDraft, val.IsRead, val.Categories);
        email.address = currentEmail;
        email.provider = 'outlook';
        email.email_id = emailID;
        outlookMessageData.messages.push(email);

      });
    }
    next(outlookMessageData);
  },

  /**
   * Description
   * @method outlookMailBoxExtract
   * @param {} mailboxRespData
   * @param {} outlookMailboxData
   * @param {} next
   * @return 
   */
  outlookMailBoxExtract: function (mailboxRespData, outlookMailboxData, next) {
    if (mailboxRespData.value && mailboxRespData.value.length) {
      mailboxRespData.value.forEach(function (val) {
        var folder = {};
        folder.id = val.Id;
        folder.name = val.DisplayName;
        folder.icon = setOutlookIconByName(val.name);
        folder.unread = val.UnreadItemCount;
        folder.total = val.TotalItemCount;
        folder.type = 'system';
        outlookMailboxData.push(folder);
      });
    }
    next(outlookMailboxData);
  }


};

/**
 * Description
 * @method getGMailHeaderValueByName
 * @param {} headerField
 * @param {} emailItem
 * @return val
 */
function getGMailHeaderValueByName(headerField, emailItem) {
  var val = '';
  if (typeof emailItem == "undefined" || typeof emailItem.payload.headers == "undefined") {
    return val;
  }
  emailItem.payload.headers.forEach(function (value) {
    if (value.name == headerField) {
      val = value.value;
    }
  });
  return val;
}

/**
 * Description
 * @method setOutlookIconByName
 * @param {} folderName
 * @return ConditionalExpression
 */
function setOutlookIconByName( folderName ){
  let folderIcons = [{name: 'INBOX', icon: 'icon-inbox' }, {name: 'STARRED', icon: 'icon-star' }, {name: 'SENT', icon: 'icon-send' },
    {name: 'DRAFT', icon: 'icon-email-open' }, {name: 'SPAM', icon: 'icon-alert-octagon' }, {name: 'TRASH', icon: 'icon-delete' }];
  let folderIcon = folderIcons.filter(fIcon=>{
    return fIcon.name === folderName;
  });
  return Object.keys(folderIcon).length ? folderIcon[0].icon : 'icon-label';
}


/**
 * Description
 * @method isHasAttachmentsGmail
 * @param {} email
 * @return 
 */
function isHasAttachmentsGmail(email) {
  if (email.metadata.length) {
    var val = false;
    email.metadata.forEach(function (value) {
      if (value.payload.mimeType == "multipart/mixed") {
        val = true;
        return val;
      }
    });
    return val;
  } else {
    return false;
  }
}


/**
 * Description
 * @method getOutlookLabelIds
 * @param {} draft
 * @param {} read
 * @param {} categories
 * @return labels
 */
function getOutlookLabelIds(draft, read, categories) {
  labels = [];
  if (draft) {
    labels.push('Draft');
  }
  if (!read) {
    labels.push('Unread');
  }
  if (categories.length) {
    categories.forEach(function (val) {
      labels.push(val);
    })
  }

  return labels;

}
