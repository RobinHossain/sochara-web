module.exports = {
  /**
   * Description
   * @method getMessages
   * @param {} next
   * @return 
   */
  getMessages: function(next) {
    Message.find().populate('sender').exec(function(err, Messages) {
      if(err) throw err;
      next(Messages);
    });
  },
  /**
   * Description
   * @method findMessagesByConversationID
   * @param {} conversation_id
   * @param {} next
   * @return 
   */
  findMessagesByConversationID: function(conversation_id, next) {
    Message.find({conversation: conversation_id}).populate('sender').exec(function findCB(err, Messages) {
      if(err) throw err;
      next(Messages);
    });
  },

  /**
   * Description
   * @method findMessagesByConversationIDWithLimit
   * @param {} conversation_id
   * @param {} limit
   * @param {} skip
   * @param {} next
   * @return 
   */
  findMessagesByConversationIDWithLimit: function(conversation_id, limit, skip, next) {
    Message.find({conversation: conversation_id},{limit: limit, skip: skip}).sort({createdAt: -1}).populate('sender').exec(function findCB(err, Messages) {
      if(err) throw err;
      next(Messages);
    });
  },
};


