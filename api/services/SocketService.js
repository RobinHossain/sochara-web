/**
 * Created by ASUS on 14-Feb-17.
 */
'use strict';
module.exports = {
  /**
   * Description
   * @method CalendarEventAdd
   * @param {} data
   * @param {} req
   * @param {} next
   * @return 
   */
  CalendarEventAdd:function (data, req, next) {
    var attendeeId = data.people;
    attendeeId.forEach(function (eachItem) {
     var eventDate = data.start.date+' '+data.start.time+ ' to '+ data.end.date+ ' '+ data.end.time;
      sails.sockets.broadcast('calendarSocket', 'calendarAdded', {userId:eachItem.id, creatorId:data.creatorId, title:data.title, date: eventDate, place:data.place, message: 'hi there!'}, req);
    }); 
    next('success');
  }
};
