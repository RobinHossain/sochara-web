var actionUtil = require('sails/lib/hooks/blueprints/actionUtil'),
  _ = require('lodash'),
  util = require('util');

module.exports = {
  /**
   * Description
   * @method getUserById
   * @param {} id
   * @param {} next
   * @return 
   */
  getUserById: function(id, next) {
    User.findOne(id).exec(function(err, User) {
      if(err) throw err;
      next(User);
    });
  },
  /**
   * Description
   * @method getUserByEmail
   * @param {} email
   * @param {} next
   * @return 
   */
  getUserByEmail: function(email, next) {
    User.findOne({'email': email}).exec(function(err, User) {
      if(err) throw err;
      next(User);
    });
  },
  /**
   * Description
   * @method getUsers
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  getUsers: function(req, res, next) {
    var query = User.find()
      .where( actionUtil.parseCriteria(req) )
      .limit( actionUtil.parseLimit(req) )
      .skip( actionUtil.parseSkip(req) )
      .sort( actionUtil.parseSort(req) );
      query = actionUtil.populateEach(query, req);
    query.exec(function found(err, Users) {
      if(err) throw err;
      next(Users);
    });
  },
  /**
   * Description
   * @method getOnlineUsers
   * @param {} only
   * @param {} next
   * @return 
   */
  getOnlineUsers: function(only, next) {
    "use strict";
    User.find({
      id: only,
      online: true,
      select: ['_id', 'first_name', 'last_name', 'photo_url', 'city', 'country']
    }).exec(function(err, Users) {
      if(err) throw err;
      next(Users);
    });
  },

  /**
   * Description
   * @method getAllUsers
   * @param {} only
   * @param {} next
   * @return 
   */
  getAllUsers: function(only, next) {
    User.find({
      'id': only
    }).exec(function(err, Users) {
      if(err) throw err;
      next(Users);
    });
  },
  
  /**
   * Description
   * @method getOfflineUsers
   * @param {} only
   * @param {} next
   * @return 
   */
  getOfflineUsers: function(only, next) {
    "use strict";
    User.find({
      id: only,
      online: false,
      select: ['_id', 'first_name', 'last_name', 'photo_url', 'city', 'country']
    }).exec(function(err, Users) {
      if(err) throw err;
      next(Users);
    });
  },

  /**
   * Description
   * @method getFriends
   * @param {} user_id
   * @param {} next
   * @return 
   */
  getFriends: function(user_id, next) {
    Friend.find({
      friend1: user_id
    }).populate('friend1').populate('friend2').exec(function findCB(err, obj) {
      if(err) throw err;

      var result = [];
      obj.forEach(function(item) {
        result.push(item);
      });

      Friend.find({friend2: user_id}).populate('friend1').populate('friend2').exec(function findCB(err, obj) {
        if(err) throw err;

        obj.forEach(function(item) {
          result.push(item);
        });

        next(result);
      });
    });
  },
  
  /**
   * Description
   * @method getConversationOnlineUsers
   * @param {} except
   * @param {} next
   * @return 
   */
  getConversationOnlineUsers: function(except, next) {
    User.find({
      id: { '!' : except },
      'online':true
    }).exec(function(err, Users) {
      if(err) throw err;
      next(Users);
    });
  },
  /**
   * Description
   * @method getConversationOfflineUsers
   * @param {} except
   * @param {} next
   * @return 
   */
  getConversationOfflineUsers: function(except, next) {
    User.find({
      id: { '!' : except },
      'online':false
    }).exec(function(err, Users) {
      if(err) throw err;
      next(Users);
    });
  }
};
