const moment = require('moment');
const AsyncCall = require('async');
const Jimp = require("jimp");
var fs = require('fs');
const path = require('path');
const rimraf = require('rimraf');
const http = require('https');
const request = require('request');

module.exports = {

  /**
   * Description
   * @method ifFolderAvailable
   * @param {} folder
   * @param {} next
   * @return 
   */
  ifFolderAvailable: function ( folder, next ) {
    FilesFolder.findOne({root: folder}, function destroyedCB(err, record) {
      if(record){
        next({data: 1});
      }else{
        next({data: 0});
      }
    });
  },

  /**
   * Description
   * @method downloadProfilePhoto
   * @param {} fileUrlData
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  downloadProfilePhoto: function(fileUrlData, req, res, next){
    let dir = 'assets/uploads/files/' + fileUrlData.user;
    let folder = fileUrlData.module || 'profile';
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }

    // to save user photo
    let fileDir = dir + '/' + folder;
    if (!fs.existsSync(fileDir)) {
      fs.mkdirSync(fileDir);
    }



    let fileName = fileUrlData.url.replace(/^.*[\\\/]/, '');
    let randNum = new Date().getTime();

    let mName = fileName.substr(0, fileName.lastIndexOf('.'));
    let fName = mName.replace(/\s\s+/g, ' ');
    fName = fName.replace(/ /g, "-");
    let ext = '.jpg';
    let FinalDName = fName + '_' + randNum + ext;

    let file = fs.createWriteStream(fileDir + '/' + FinalDName);
    http.get(fileUrlData.url, function(response) {
      response.pipe(file);
    });
    next('/uploads/files/' +  fileUrlData.user + '/' + folder +'/' + FinalDName);
  },

  /**
   * Description
   * @method downloadFileFromUrl
   * @param {} fileUrlData
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  downloadFileFromUrl: function (fileUrlData, req, res, next) {
    let folder = fileUrlData.folder || null;
    let module = fileUrlData.module || 'file';
    fileUrlData.url = 'https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png';
    if (fileUrlData.url) {
      var fileName = fileUrlData.url.replace(/^.*[\\\/]/, '');

      let fileDir = 'assets/uploads/files/' + fileUrlData.user + '/'+module+'';
      if (!fs.existsSync(fileDir)) {
        fs.mkdirSync(fileDir);
      }
      var randNum = new Date().getTime();


      // var baseURL = Utility.getSiteBaseURL(req);
      var mName = fileName.substr(0, fileName.lastIndexOf('.'));
      var fName = mName.replace(/\s\s+/g, ' ');
      fName = fName.replace(/ /g, "-");
      var ext = path.extname(fileName);
      var FinalDName = fName + '_' + randNum + ext;
      var file = fs.createWriteStream(fileDir + '/' + FinalDName);


      request(fileUrlData.url).pipe(file);
      setTimeout(function () {
        const fileStats = fs.statSync(fileDir + '/' + FinalDName);
        let fileData = {};
        fileData.name = FinalDName;
        fileData.type = 'profile';
        fileData.owner = fileUrlData.user;
        fileData.file_dir = '/uploads/files/' + fileUrlData.user + '/' + module + '/' + FinalDName;
        fileData.ext = ext;
        fileData.size = fileStats.size;
        fileData.folder = folder || '0';
        UserFiles.create(fileData, function fileCreated(err, createdFile) {
          if (err) {
            console.log(err);
          }
          if (createdFile) {
            next(createdFile.file_dir);
          }
        });
      }, 1000);
    }
  },

};

