var AsyncCall = require('async');

module.exports = {
  /**
   * Description
   * @method eventExtractFromGoogle
   * @param {} eventData
   * @param {} emailData
   * @param {} next
   * @return 
   */
  eventExtractFromGoogle: function ( eventData, emailData, next ) {
    if ( eventData && eventData.items && eventData.items.length ) {
      AsyncCall.forEach(eventData.items, function (val, callback) {
        var eventObj = {};
        eventObj.title = val.summary || 'Untitled Event';
        eventObj.organizer = emailData.user.id;
        if( val.reminders ){
          eventObj.reminder = [{type: 'notification', duration: 30, format: 'minutes'}];
        }

        eventObj.start = val.start ? (val.start.dateTime || val.start.date) : undefined;
        eventObj.end = val.end ? (val.end.dateTime || val.end.date) : undefined;
        eventObj.status = val.status;
        eventObj.description = val.description || '';
        eventObj.htmlLink = val.htmlLink;
        eventObj.user = emailData.user.id;
        eventObj.stick = true;
        eventObj.email = emailData.email;
        eventObj.provider = 'gmail';
        eventObj.mail_id = emailData.id;
        eventObj.event_id = val.id;
        eventObj.color = {eventColor: 'c10', textColor: 'c2'};
        eventObj.attendees = [{id: emailData.user.id, role: 'organizer', acceptance: 'pending', name: emailData.user.first_name + ' ' + emailData.user.last_name, email: emailData.user.email, photo_url: 'images/avatars/profile.jpg'}];

        CalendarEvent.findOne({ event_id : eventObj.event_id }).exec(function(err, foundEvent) {
          if (err) console.log(err);
          if( foundEvent ){
            callback(foundEvent.event_id || '');
          }else if ( !foundEvent && (eventObj.start && eventObj.end)){
            CalendarEvent.create(eventObj).exec(function (err, eventCreated) {
              if (err) console.log(err);
              callback(eventCreated.event_id || '');
            });
          }else{
            callback();
          }
        });
      }, function (err){
        if (err) console.log(err);
        next('done');
      });
    }else{
      next('done');
    }
  },

  /**
   * Description
   * @method holidayExtractFromGoogle
   * @param {} eventData
   * @param {} countryState
   * @param {} next
   * @return 
   */
  holidayExtractFromGoogle: function ( eventData, countryState, next ) {
    eventData  = JSON.parse(eventData);
    if ( eventData && eventData.items && eventData.items.length ) {
      AsyncCall.forEach(eventData.items, function (val, callback) {
        if( !(val.start && val.start.date) || !(val.end && val.end.date) ){
          callback('');
        }else{
          var eventObj = {};
          eventObj.title = val.summary;
          eventObj.summary = eventData.summary;
          eventObj.start = new Date(val.start.date);
          eventObj.end = new Date(val.end.date);
          eventObj.status = val.status;
          eventObj.htmlLink = val.htmlLink;
          eventObj.stick = true;
          eventObj.provider = 'google';
          eventObj.type = 'holiday';
          eventObj.event_id = val.id;
          eventObj.countryState = countryState;
          CalendarHolyday.findOne({ event_id : eventObj.event_id }).exec(function(err, foundEvent) {
            if (err) console.log(err);
            if ( !foundEvent ){
              CalendarHolyday.create(eventObj).exec(function (err, eventCreated) {
                if (err) console.log(err);
                callback();
              });
            }else{
              callback();
            }
          });
        }

      }, function (err){
        if (err) console.log(err);
        next('done');
      });
    }else{
      next('done');
    }
  },

  /**
   * Description
   * @method eventExtractFromOutlook
   * @param {} eventData
   * @param {} emailData
   * @param {} next
   * @return 
   */
  eventExtractFromOutlook: function ( eventData, emailData, next ) {
    if ( eventData.value && eventData.value.length ) {
      AsyncCall.forEach(eventData.value, function (val, callback) {
        if( !(val.Start && val.Start.DateTime) || !(val.End && val.End.DateTime) ){
          callback('');
        }else {
          var eventObj = {};
          eventObj.title = val.Subject;
          eventObj.organizer = emailData.user.id;
          if (val.IsReminderOn) {
            eventObj.reminder = [{type: 'notification', duration: 30, format: 'minutes'}];
          }
          eventObj.start = val.Start.DateTime;
          eventObj.end = val.End.DateTime;
          eventObj.description = val.BodyPreview;
          eventObj.htmlLink = val.WebLink;
          eventObj.user = emailData.user.id;
          eventObj.email = emailData.email;
          eventObj.provider = 'outlook';
          eventObj.stick =true;
          eventObj.mail_id = emailData.id;
          eventObj.event_id = val.Id;
          eventObj.color = {eventColor: 'c10', textColor: 'c2'};
          eventObj.attendees = [{
            id: emailData.user.id,
            role: 'organizer',
            acceptance: 'pending',
            name: emailData.user.first_name + ' ' + emailData.user.last_name,
            email: emailData.user.email,
            photo_url: 'images/avatars/profile.jpg'
          }];

          CalendarEvent.findOne({event_id: eventObj.event_id, provider: 'outlook'}).exec(function (err, foundEvent) {
            if (err) console.log(err);
            if (!foundEvent) {
              CalendarEvent.create(eventObj).exec(function (err, eventCreated) {
                if (err) console.log(err);
                callback(eventCreated.id);
              });
            } else {
              callback(foundEvent.id);
            }
          });
        }

      }, function (err){
        if (err) console.log(err);
        next('done');
      });
    }else{
      next('done');
    }
  }
};
