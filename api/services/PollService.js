module.exports = {
  /**
   * Description
   * @return
   * @method getPolls
   * @param {} next
   * @return 
   */
  getPolls: function (next) {
    Poll.find().populate('poll_answer').exec(function (err, Records) {
      if (err) throw err;
      next(Records);
    });
  },

  /**
   * Description
   * @return
   * @method findPollByID
   * @param {} id
   * @param {} next
   * @return 
   */
  findPollByID: function (id, next) {
    Poll.findOne(id).exec(function findCB(err, record) {
      if (err) throw err;
      next(record);
    });
  },

  /**
   * Description
   * @return
   * @method findPollVotesByID
   * @param {} id
   * @param {} next
   * @return 
   */
  findPollVotesByID: function (id, next) {
    PollVote.find({'poll_id': id}).populate('poll').populate('user').exec(function findCB(err, record) {
      if (err) throw err;
      next(record);
    });
  },
  /**
   * Description
   * @return
   * @method findPollVotesOfMeByID
   * @param {} id
   * @param {} me
   * @param {} next
   * @return 
   */
  findPollVotesOfMeByID: function (id, me, next) {
    PollVote.find({
      'poll': id,
      'user': me
    }).populate('poll').populate('user').exec(function findCB(err, record) {
      if (err) throw err;
      next(record);
    });
  },
  /**
   * Description
   * @return
   * @method findPollVotesOfIPByID
   * @param {} id
   * @param {} ip
   * @param {} next
   * @return 
   */
  findPollVotesOfIPByID: function (id, ip, next) {
    PollVote.find({'poll': id, 'ip': ip}).populate('poll').populate('user').exec(function findCB(err, record) {
      if (err) throw err;
      next(record);
    });
  },

  /**
   * Description
   * @return
   * @method findPollVotesOfIPByIdPublic
   * @param {} id
   * @param {} ip
   * @param {} next
   * @return 
   */
  findPollVotesOfIPByIdPublic: function (id, ip, next) {
    PollVote.find({'poll_id': id, 'ip': ip}).populate('poll').populate('user').exec(function findCB(err, record) {
      if (err) throw err;
      next(record);
    });
  },
};


