/**
 * Created by ASUS on 14-Dec-16.
 */
module.exports = {
  /**
   * Description
   * @return
   * @method interoperability
   * @param {} framework
   * @return 
   */
  interoperability: function (framework) {
    /*'other_address_street', 'other_address_city',
        'other_address_state', 'other_address_postal_code', 'other_address_country/region',*/
    var ContactFramework = framework;
    var CommonAttributes = {
      'full_name': ['Name', 'Display Name', ''],
      'first_name': ['Given Name', 'First Name', 'First'],
      'middle_name': ['Additional Name', 'Middle Name', 'Middle'],
      'last_name': ['Family Name', 'Last Name', 'Last'],
      'email_home': ['E-mail 1 - Value', 'E-mail Address', 'Email'],
      'email_work': ['E-mail 2 - Value', 'E-mail 2 Address', 'Alternate Email 1'],
      'email_other': ['E-mail 4 - Value', 'E-mail 3 Address', 'Alternate Email 2'],
      'phone_main': ['Phone 5 - Value', 'Primary Phone', 'Home'],
      'pone_work': ['Phone 3 - Value', 'Business Phone', 'Work'],
      'phone_other': ['Phone 7 - Value', 'Other Phone', 'Other'],
      'home_address_street': ['Address 1 - Street', 'Home Street', 'Home Address'],
      'home_address_city': ['Address 1 - City', 'Home City', ' Home City'],
      'home_address_state': ['Address 1 - Region', 'Home State', 'Home State'],
      'home_address_postal_code': ['Address 1 - Postal Code', 'Home Postal Code', 'Home ZIP'],
      'home_address_country/region': ['Address 1 - Country', 'Home Country/Region', 'Home Country'],
      'work_address_street': ['Address 2 - Street', 'Business Street', 'Work Address'],
      'work_address_city': ['Address 2 - City', 'Business City', ' Work City'],
      'work_address_state': ['Address 2 - Region', 'Business State', 'Work State'],
      'work_address_postal_code': ['Address 2 - Postal Code', 'Business Postal Code', 'Work ZIP'],
      'work_address_country/region': ['Address 2 - Country', 'Business Country/Region', 'Work Country'],
      'url_home': ['Website 3 - Type', '', ''],
      'url_work': ['Website 2 - Type', '', ''],
      'url_other': ['Website 1 - Type', '', ''],
      'date_birt': ['Birthday', '', 'Birthday'],
      'date_anniversary': ['', '', 'Anniversary'],
      'social_fb': ['', '', ''],
      'social_skype': ['', '', ''],
      'social_linkedin': ['', '', ''],
      'social_twitter': ['', '', ''],
      'social_other': ['', '', ''],
      'im_skype': ['IM 1 - Value', '', 'Skype ID'],
      'im_aim': ['IM 1 - Value', '', 'AIM ID'],
      'im_yahoo': ['IM 1 - Value', '', ''],
      'im_fb': ['IM 1 - Value', '', ''],
      'im_google': ['IM 1 - Value', '', 'Google ID'],
      'im_other': ['IM 1 - Value', '', ''],
      'relative_father': ['', '', ''],
      'relative_mother': ['', '', ''],
      'relative_parent': ['', '', ''],
      'relative_brother': ['', '', ''],
      'relative_sister': ['', '', ''],
      'relative_son': ['', '', ''],
      'relative_daughter': ['', '', ''],
      'relative_wife': ['', '', ''],
      'relative_husband': ['', '', ''],
      'relative_spouse': ['', '', ''],
      'relative_partner': ['', '', ''],
      'relative_assistant': ['', '', ''],
      'relative_manager': ['', '', ''],
      'relative_other': ['', '', ''],
      'custom_other': ['', '', ''],
      'designation': ['Organization 1 - Title', '', 'Title'],
      'employer': ['Organization 1 - Name', '', 'Company']
    };

  }
};
