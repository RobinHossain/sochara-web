/**
 * Friend.js
 *
 * @module      :: Friend
 * @description :: Simple service to store all Friend model functions
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Services
 *
 */
module.exports = {

  /**
   * Description
   * @method addToFriend
   * @param {} friend1_id
   * @param {} friend2_id
   * @param {} status
   * @param {} next
   * @return 
   */
  addToFriend: function(friend1_id, friend2_id, status, next) {
    var data = {
      friend1: friend1_id,
      friend2: friend2_id,
      request_seen: 'no', //Sent to user seen this request or not on top of his / her account friend request area
      accept_seen: 'no',  //Send by user seen this request has been accepted on top of his / her account notification area
      status: status
    }
    Friend.create( data, function CreateCB(err, Friend) {
        if (err) {
          var response = {
            err: {"message": err}
          }
        } else {
          var response = {
            msg: {"message": "Friend request has been sent successfully."}
          }
        }
        next(response);
    });
  },
    
  /**
   * Description
   * @method updateFriendStatus
   * @param {} id
   * @param {} status
   * @param {} next
   * @return 
   */
  updateFriendStatus: function(id, status, next) {
    Friend.update(id, {
        status: status
    }, function(err) {
        if (err) {
          var response = {
            err: {"message": err}
          }
        } else {
          var response = {
            msg: {"message": "Friend request status updated."}
          }
        }
        next(response);
    });
  },
    
    /**
     * Description
     * @method isMyFriend
     * @param {} user_id
     * @param {} friend_id
     * @param {} next
     * @return 
     */
    isMyFriend: function(user_id, friend_id, next) {
        
        //As the normal waterline ORM is not working as expected for now this is the hack solution, needs to optimize later.
        Friend.find({friend1: user_id}).populate('friend1').populate('friend2').exec(function findCB(err, obj) {
            if(err) throw err;
          
            var result = [];
          if(obj.length){
            obj.forEach(function(item) {
              if (item && item.friend2 && item.friend2.id == friend_id ) {
                result.push(item);
              }
            });
          }
            
            
            if (result.length > 0 ) {
                
                //updating is not required as I am not viewing my friend requests
                next(result); 

                   
            } else {
                Friend.find({friend2: user_id}).populate('friend1').populate('friend2').exec(function findCB(err, obj) {
                    if(err) throw err;

                    var result = [];

                  if(obj.length){
                    obj.forEach(function(item) {
                      if (item && item.friend1 && item.friend1.id == friend_id ) {
                        result.push(item);
                      }
                    });
                  }
                    
                    
                    if ( result.length > 0 ) {
                        //updating the request as seen
                        Friend.update(result[0].id, {
                            request_seen: true
                        }, function(err) {
                            if (err) return next(err);
                            next(result); 
                        }); 
                    } else {
                        next(result); 
                    }
                    
                });    
            }
        });
        
        /*
        var criteria = { 
            "$or": [
                { "friend1": user_id, "friend2": friend_id },
                { "friend1": friend_id, "friend2": user_id }
            ]
        };
        var criteria = {"friend1": user_id};

        // Grab an instance of the mongo-driver
        Friend.native(function(err, collection) {        
            if (err) return res.serverError(err);

            // Execute any query that works with the mongo js driver
            collection.find(criteria).toArray(function (err, friendObj) {
                if(err) throw err;
                console.log(friendObj);
                next(friendObj);
            });
        });
        */
    },
    
    /**
     * Description
     * @method getFriends
     * @param {} user_id
     * @param {} next
     * @return 
     */
    getFriends: function(user_id, next) {
        Friend.find({friend1: user_id}).populate('friend1').populate('friend2').exec(function findCB(err, obj) {
            if(err) throw err;
          
            var result = [];
          if(obj.length){
            obj.forEach(function(item) {
              result.push(item);
            });
          }
            
            
            Friend.find({friend2: user_id}).populate('friend1').populate('friend2').exec(function findCB(err, obj) {
                if(err) throw err;

              if(obj.length){
                obj.forEach(function(item) {
                  result.push(item);
                });
              }
                

                next(result);  
            });
        });
    },
    
    /**
     * Description
     * @method getFriendIds
     * @param {} user_id
     * @param {} next
     * @return 
     */
    getFriendIds: function(user_id, next) {
        Friend.find({friend1: user_id}).populate('friend1').populate('friend2').exec(function findCB(err, obj) {
            if(err) throw err;
          
            var result = [];
          
          if(obj.length){
            obj.forEach(function(item) {
              if (item.status == "1") {
                if(item.friend2){
                  result.push(item.friend2.id);
                }
              }
            });
          }
            
            
            Friend.find({friend2: user_id}).populate('friend1').populate('friend2').exec(function findCB(err, obj) {
                if(err) throw err;

              if(obj.length){
                obj.forEach(function(item) {
                  if (item.status == "1") {
                    if(item.friend1){
                      result.push(item.friend1.id);
                    }
                  }
                });
              }

                next(result);  
            });
        });
    },
    
    /**
     * Description
     * @method getPendingFriendRequests
     * @param {} user_id
     * @param {} next
     * @return 
     */
    getPendingFriendRequests: function(user_id, next) {
        Friend.find({friend2: user_id}).populate('friend1').populate('friend2').exec(function findCB(err, obj) {
            if(err) throw err;
          
            var result = [];
          if(obj.length) {
            obj.forEach(function (item) {
              if (item && item.status == "0") {
                result.push(item);
              }
            });
          }
          
          next(result);
          
        });
    },
    
}
