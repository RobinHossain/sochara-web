var AsyncCall = require('async');
const google = require('googleapis');
var outlook = require('nodejs-outlook');
var moment = require('moment');

var REDIRECT_URL;

// Gmail Mail request Variables
// For Sochara Local
var CLIENT_ID = '678407714922-1auh2t76gvk479sn1m02g4omt22oo0in.apps.googleusercontent.com';
var CLIENT_SECRET = '9CdpLyvCF7HvZ2vDQ8TgK31j';
// REDIRECT_URL = 'http' + '://' + 'localhost:1337' + '/gmail/oauth2callback';

// For Sochara Live
// var CLIENT_ID = '678407714922-d8i524n104gd1iv7mmq75ga4a0fe4ihe.apps.googleusercontent.com';
// var CLIENT_SECRET = 'fTA3FpcXbMODymQuTbktfC5W';
REDIRECT_URL = 'https' + '://' + 'www.sochara.com' + '/gmail/oauth2callback';

var OAuth2Client;
var gmail;
var oauth2Client;


// Outlook Mail Request Variables
var Mailbox = 'Inbox';
var top;
var skip;


var credentials = {
  client: {
    id: '55a857a7-806b-4f56-9c0a-c086a8c9ab95',
    secret: 'ybNEFBRxvwyYV2MzFGvEpkX',
  },
  auth: {
    tokenHost: 'https://login.microsoftonline.com',
    authorizePath: 'common/oauth2/v2.0/authorize',
    tokenPath: 'common/oauth2/v2.0/token'
  }
};
var outlookOauth2 = require('simple-oauth2').create(credentials);

/**
 * Description
 * @method refreshAccessToken
 * @param {} refreshToken
 * @param {} callback
 * @return
 */
var refreshAccessToken = function (refreshToken, callback) {
  var tokenObj = outlookOauth2.accessToken.create({refresh_token: refreshToken});
  tokenObj.refresh(callback);
};

module.exports = {

  /**
   * Description
   * @method cronMails
   * @param {} next
   * @return
   */
  cronMails: function (next) {
    EmailAccount.find({
      select: ['_id', 'email', 'provider', 'user', 'access_token', 'refresh_token']
    }).exec(function foundCB(err, emailAccounts) {
      if (emailAccounts && emailAccounts.length) {
        AsyncCall.forEach(emailAccounts, function (emailAccount, callback) {

          Email.findOne({
            address: emailAccount.email,
            user: emailAccount.user,
            provider: emailAccount.provider,
            select: ['_id', 'date', 'updatedAt', 'createdAt']
          }).sort({date: -1}).exec(function foundCB(err, lastMail) {

            if (lastMail) {
              var lastMailDate;
              if (emailAccount.provider === 'gmail' && emailAccount.access_token) {
                OAuth2Client = google.auth.OAuth2;
                gmail = google.gmail('v1');
                oauth2Client = new OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);
                oauth2Client.setCredentials({
                  access_token: emailAccount.access_token,
                  refresh_token: emailAccount.refresh_token
                });
                var $params = {};
                $params.labelIds = ["INBOX"];
                $params.maxResults = 10;

                $params.userId = 'me';
                $params.auth = oauth2Client;

                if (lastMail && lastMail.date) {
                  // lastMailDate = moment(lastMail.date).format('YYYY/MM/DD');
                  // lastMailDate = new Date(lastMail.date).getTime();
                  // var myDate = new Date("2012-02-10T13:19:11+0000");
                  // var offset = myDate.getTimezoneOffset() * 60 * 1000;
                  var tmpLastMailDate = Date.parse(lastMail.date);
                  lastMailDate = tmpLastMailDate / 1000;

                  $params.q = "in:inbox after:" + lastMailDate;
                }

                gmail.users.threads.list($params, function (err, data) {
                  var threads = [];
                  // if (err) console.log(err);
                  if (data) {
                    threads = data.threads;
                    if ( threads && threads.length ) {
                      AsyncCall.forEachOf(threads, function (value, key, callback) {

                        oauth2Client = new OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);
                        oauth2Client.setCredentials({
                          access_token: emailAccount.access_token,
                          refresh_token: emailAccount.refresh_token
                        });

                        if (value.id) {
                          gmail.users.threads.get({
                            id: value.id,
                            format: 'full',
                            userId: 'me',
                            auth: oauth2Client
                          }, function (err, body) {
                            if (err) console.log(err);
                            if (body && body.messages) {
                              threads[key]['metadata'] = body.messages;
                            }
                            callback();
                          });
                        } else {
                          callback();
                        }

                      }, function (err) {
                        if (err) console.log(err);
                        data.threads = threads;
                        if (data) {
                          data.newMail = 'yes';
                          MailService.gmailExtractAndStore(data, emailAccount.email, emailAccount.id, emailAccount.user, function (mailData) {
                            next(mailData);
                          });
                        } else {
                          next('No Message Found');
                        }
                      });
                    }
                  }
                });
              } else if (emailAccount.provider === 'outlook') {
                top = 10;
                skip = 0;
                var pagination = {};
                refreshAccessToken(emailAccount.refresh_token, function (error, newToken) {
                  if (error) {
                    console.log(error);
                  }
                  if (newToken) {
                    EmailAccount.update({
                      id: emailAccount.id,
                      user: emailAccount.user,
                      provider: 'outlook'
                    }, {
                      access_token: newToken.token.access_token,
                      refresh_token: newToken.token.refresh_token,
                      id_token: newToken.token.id_token,
                      expires_at: newToken.token.expires_at
                    }).exec(function afterwards(err, updated) {
                      if (err) console.log(err);
                      // console.log('updated');
                    });
                    var filter = 'ReceivedDateTime' + ' ge ' + moment(lastMail.date).format('YYYY-MM-DD');
                    getOutlookEmails(newToken.token.access_token, emailAccount.email, Mailbox, top, skip, filter, function (resp) {
                      if (resp) {
                        resp.newMail = 'yes';
                        MailService.outlookExtractAndStore(resp, emailAccount.email, emailAccount.id, emailAccount.user, function (mailData) {
                          next(mailData);
                        });
                      } else {
                        next('No Message Found');
                      }
                    });
                  }
                });
              } else {
                callback();
              }
            }
          });
        });
      }
    });
  },

  /**
   * Description
   * @method cronNewAccountMails
   * @param {} next
   * @return
   */
  cronNewAccountMails: function (next) {
    Cron.findOne({
      module: "mail",
      status: "pending"
    }).exec(function foundCB(err, cronData) {
      if (cronData) {
        EmailAccount.findOne(cronData.data.id).exec(function foundCB(err, emailAccount) {
          if (emailAccount) {
            if (emailAccount.provider === 'gmail' && emailAccount.access_token) {
              OAuth2Client = google.auth.OAuth2;
              oauth2Client = new OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);
              oauth2Client.setCredentials({
                access_token: emailAccount.access_token,
                refresh_token: emailAccount.refresh_token
              });
              var $params = {};
              $params.labelIds = ["INBOX"];
              $params.maxResults = 40;
              var pageToken = cronData.data.nextPageToken;
              if (pageToken && pageToken !== '') {
                $params.pageToken = pageToken;
              }

              $params.userId = 'me';
              $params.auth = oauth2Client;
              gmail = google.gmail('v1');
              gmail.users.threads.list($params, function (err, data) {
                var threads = [];
                if (err) console.log(err);

                if (data) {
                  threads = data.threads;
                  AsyncCall.forEachOf(threads, function (value, key, callback) {
                    if (value.id) {
                      gmail.users.threads.get({
                        id: value.id,
                        format: 'full',
                        userId: 'me',
                        auth: oauth2Client
                      }, function (err, body) {
                        if (err) console.log(err);
                        if (body && body.messages) {
                          threads[key]['metadata'] = body.messages;
                        }

                        callback();
                      });
                    } else {
                      callback();
                    }

                  }, function (err) {
                    if (err) console.log(err);
                    data.threads = threads;
                    if (data) {
                      MailService.gmailExtractAndStore(data, emailAccount.email, emailAccount.id, emailAccount.user, function (mailData) {
                        if (mailData) {
                          var gdata = cronData.data;
                          gdata.maxResults = cronData.data.maxResults - 40;
                          gdata.nextPageToken = data.nextPageToken;
                          if (gdata.maxResults <= 0) {
                            Cron.destroy({id: cronData.id}).exec(function (err) {
                              if (err) {
                                console.log(err);
                              }
                            });
                          } else {
                            Cron.update({id: cronData.id}, {data: gdata}).exec(function foundCB(err, updatedCData) {
                              // console.log(updatedCData[0].id);
                            });
                          }
                          next(mailData);
                        }
                      });
                    } else {
                      next('No Message Found');
                    }
                  });
                }
              });
            } else if (emailAccount.provider === 'outlook') {
              var pagination = {};
              refreshAccessToken(emailAccount.refresh_token, function (error, newToken) {
                if (error) {
                  console.log(error);
                }
                if (newToken) {
                  EmailAccount.update({
                    id: emailAccount.id,
                    user: emailAccount.user,
                    provider: 'outlook'
                  }, {
                    access_token: newToken.token.access_token,
                    refresh_token: newToken.token.refresh_token,
                    id_token: newToken.token.id_token,
                    expires_at: newToken.token.expires_at
                  }).exec(function afterwards(err, updated) {
                    if (err) console.log(err);
                    // console.log('updated');
                  });

                  top = 40;
                  skip = cronData.data.skip;
                  getOutlookEmails(newToken.token.access_token, emailAccount.email, Mailbox, top, skip, null, function (resp) {
                    if (resp) {
                      MailService.outlookExtractAndStore(resp, emailAccount.email, emailAccount.id, emailAccount.user, function (mailData) {
                        if (mailData) {
                          var gdata = cronData.data;
                          gdata.maxResults = cronData.data.maxResults - 40;
                          gdata.skip = cronData.data.skip + 40;
                          if (gdata.maxResults <= 0) {
                            Cron.destroy({id: cronData.id}).exec(function (err) {
                              if (err) {
                                console.log(err);
                              }
                            });
                          } else {
                            Cron.update({id: cronData.id}, {data: gdata}).exec(function foundCB(err, updatedCData) {

                            });
                          }

                          next(mailData);
                        }
                      });
                    } else {
                      next('No Message Found');
                    }
                  });
                }
              });
            }
          }
        });
      }

    });
  }
};

/**
 * Description
 * @method getOutlookEmails
 * @param {} token
 * @param {} email
 * @param {} folder
 * @param {} top
 * @param {} skip
 * @param {} filter
 * @param {} callback
 * @return
 */
function getOutlookEmails(token, email, folder, top, skip, filter, callback) {

  var queryParams = {
    // '$select': 'Subject,ReceivedDateTime,From,IsRead',
    '$orderby': 'ReceivedDateTime desc',
    '$top': top,
    '$skip': skip,
    '$count': 'true'
  };

  if (filter) {
    queryParams.$filter = filter;
  }

  // Set the API endpoint to use the v2.0 endpoint
  outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
  // Set the anchor mailbox to the user's SMTP address
  outlook.base.setAnchorMailbox(email);

  outlook.mail.getMessages({token: token, folderId: folder, odataParams: queryParams},
    function (error, result) {
      if (error) {
        console.log('getMessages returned an error: ' + error);
      }
      else if (result) {
        callback(result);
      }
    });
}
