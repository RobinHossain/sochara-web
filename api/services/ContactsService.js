/**
 * Contacts Service
 * @description this service processes all the contacts data iteration task for all modules need contact data
 */
'use strict';
/*jshint esversion: 6 */
const async = require('async');

module.exports = {
  /**
   * @function
   * @return
   * @description returns user data including contact data of users id
   * @method getUserinfo
   * @param {array} items -- Users id
   * @param {} cb
   * @return 
   */
  getUserinfo: function (items, cb) {
    const peopleIds = items.people;
    const curUserId = items.curUserId;
    const async = require('async'); //async library
    const usersData = [];
    const curUsersData = [];
    peopleIds.forEach(function (singleItem) {
      if(singleItem.id !== curUserId) {
        usersData.push(function (callback) {
          User.findOne({id: singleItem.id}).exec(function (err, data) { //finding each user
            if (err) {
              return;
            }
            if (data) {
              const userData = {};
              userData.first_name = data.first_name ;
              userData.last_name = data.last_name;
              userData.email = data.email;
              userData.photo_url = data.photo_url;
              userData.response = singleItem.response;
              userData.role = singleItem.role;
              callback(null, userData);
            }
            if (!data) {
              Contacts.findOne({
                id: singleItem.id
              }).exec(function (err, contactRecord) {
                if (err) {
                  return;
                }
                if (contactRecord) {
                  var pushDataContact = {};
                  pushDataContact.first_name = contactRecord.first_name ;
                  pushDataContact.last_name = contactRecord.last_name;
                  pushDataContact.email = contactRecord.email;
                  pushDataContact.photo_url = contactRecord.photo_url;
                  pushDataContact.response = singleItem.response;
                  pushDataContact.role = singleItem.role;
                  callback(null, pushDataContact);
                }
              });
            }
          });
        });
      }
      if(singleItem.id === curUserId) {
        curUsersData.push(function (callback) {
          User.findOne({id: singleItem.id}).exec(function (err, data) { //finding each user
            if (err) {
              return;
            }
            if (data) {
              const userData = {};
              userData.first_name = data.first_name ;
              userData.last_name = data.last_name;
              userData.email = data.email;
              userData.photo_url = data.photo_url;
              userData.response = singleItem.response;
                userData.role = singleItem.role;
              callback(null, userData);
            }
          });
        });
      }
    });
    async.parallel(usersData, function (err, peopledata) { //fetching all people excluding current user
      const returnData ={};
      if (err) {
        console.log(err);
      }
      if (peopledata) {
        returnData.peopledata = peopledata;
      }
      async.parallel(curUsersData, function (err, curuserdata) { //fetching current user data
        if (err) {
          console.log(err);
        }
        if (curuserdata) {
          returnData.curUserData = curuserdata;
          cb(returnData);
        }
      });
    });
  },
  /**
   * Description
   * @method extractGoogleContacts
   * @param {} items
   * @param {} next
   * @return 
   */
  extractGoogleContacts: function (items, next ) {
    let contactData = [];
    if( items.feed.entry && items.feed.entry.length ){
      // console.log(items.feed.entry[0].title);
      items.feed.entry.forEach(function (item, index) {
        let contactObj = {};
        let fullName = item.title['$t'] || '';
        contactObj.first_name = fullName.split(' ').slice(0, -1).join(' ');
        contactObj.last_name = fullName.split(' ').slice(-1).join(' ');
        contactObj.email = Array.isArray(item['gd$email']) ? item['gd$email'][0].address : '';
        contactObj.phone = Array.isArray(item['gd$phoneNumber']) ? item['gd$phoneNumber'][0]['$t'] : '';
        if( contactObj.first_name || contactObj.last_name ){
          contactData.push(contactObj);
        }
      });
      next(contactData);
    }else{
      next([]);
    }

  },
  /**
   * Description
   * @method contactExtractFromOutlook
   * @param {} contactData
   * @param {} user
   * @param {} next
   * @return 
   */
  contactExtractFromOutlook: function(contactData, user, next){
    if ( contactData.value && contactData.value.length ) {
      let contactObj, contactArr = [];
      async.forEach(contactData.value, function (contact, callback) {
        contactObj = {first_name: contact.GivenName, last_name: contact.Surname || '', phone: contact.MobilePhone1, module: 'contact', user: user};
        if(contact.EmailAddresses && Object.prototype.toString.call(contact.EmailAddresses) === '[object Array]'){
          contactObj.email = contact.EmailAddresses[0] ? contact.EmailAddresses[0].Address : '';
        }
        if(contactObj.email){
          Temp.findOrCreate({user: user, email: contactObj.email}, contactObj).exec(function (err, createdContact) {
            if(err) console.log(err);
            contactArr.push(createdContact);
            callback(null, createdContact);
          });
        }else{
          callback(null, '');
        }
      }, function (err){
        if (err) console.log(err);
        next(contactArr);
      });
    }else{
      next([]);
    }
  },
  /**
   * Description
   * @method addContactRequest
   * @param {} data
   * @param {} callback
   * @return 
   */
  addContactRequest: function (data, callback) {
    User.findOne({id: data.userId}).exec(function (err, respUser) {
      Contacts.create({
        user_id: data.loggedInUserId,
        sochara_user_id: respUser.id,
        status: 'pending',
        is_sochara_user: true,
        request_sent_by: data.loggedInUserId,
        first_name: respUser.first_name,
        last_name: respUser.last_name,
        email: respUser.email
      }).exec(function (err, respContact) {
        sails.sockets.broadcast('sochara_' + data.userId, 'global', {message: 'You have a new contact request, to accept go to Contact module!'});
        callback({message: 'done'});
      });
    });
  },
};
