module.exports = {

  /**
   * Description
   * @method getUnreadEmails
   * @param {} fieldName
   * @param {} fieldValue
   * @param {} user_id
   * @param {} page
   * @param {} perPage
   * @param {} type
   * @param {} next
   * @return 
   */
  getUnreadEmails: function (fieldName, fieldValue, user_id, page, perPage, type, next) {
    if (type == 'label') {
      MailToLabel.find({"label_id": fieldValue, "user_id": user_id}).exec(function findCB(err, MailLists) {
        if (err) throw err;
        var MailIDs = [];
        MailLists.forEach(function (c, i) {
          MailIDs.push(c.mail_id);
        });
        MailToFolder.find({
          "mail_id": MailIDs,
          "user_id": user_id,
          "flag": 'unread'
        }).sort('createdAt desc').skip(perPage * page).limit(perPage).populate('parent').populate('sender').populate('user').populate('mail').exec(function findCB(err, eMailLists) {
          if (err) throw err;
          next(eMailLists);
        });
      });
    } else if (fieldName == 'starred') {
      MailToFolder.find({
        'starred': fieldValue,
        "user_id": user_id,
        "flag": 'unread'
      }).sort('createdAt desc').skip(perPage * page).limit(perPage).populate('parent').populate('sender').populate('user').populate('mail').exec(function findCB(err, MailLists) {
        if (err) throw err;
        next(MailLists);
      });
    } else if (fieldName == 'important') {
      MailToFolder.find({
        'important': fieldValue,
        "user_id": user_id,
        "flag": 'unread'
      }).sort('createdAt desc').skip(perPage * page).limit(perPage).populate('parent').populate('sender').populate('user').populate('mail').exec(function findCB(err, MailLists) {
        if (err) throw err;
        next(MailLists);
      });
    } else {
      MailToFolder.find({
        'folder': fieldValue,
        "user_id": user_id,
        "flag": 'unread'
      }).sort('createdAt desc').skip(perPage * page).limit(perPage).populate('sender').populate('parent').populate('user').populate('mail').exec(function findCB(err, MailLists) {
        if (err) throw err;
        next(MailLists);
      });
    }
  },

  /**
   * Description
   * @method getEMails
   * @param {} fieldName
   * @param {} fieldValue
   * @param {} user_id
   * @param {} page
   * @param {} perPage
   * @param {} type
   * @param {} next
   * @return 
   */
  getEMails: function (fieldName, fieldValue, user_id, page, perPage, type, next) {
    
    if (type == 'label') {
      MailToFolder.find({"label_id": fieldValue, "user_id": user_id}).exec(function findCB(err, lMailLists) {
        if (err) throw err;
        var MailIDs = [];
        lMailLists.forEach(function (c, i) {
          MailIDs.push(c.mail_id);
        });
        MailToFolder.find({
          "mail_id": MailIDs,
          "user_id": user_id,
          "parent_id": ""
        }).sort('createdAt desc').skip(perPage * page).limit(perPage).populate('sender').populate('user').populate('mail').exec(function findCB(err, MailLists) {
          if (err) throw err;
          //next(MailLists);
          var AsyncCall = require('async');
          var obj = MailLists;
          AsyncCall.forEachOf(obj, function (value, key, callback) {
            MailToFolder.find({
              "user_id": user_id,
              "parent_id": value.id
            }).sort('createdAt asc').populate('sender').populate('user').populate('mail').exec(function findCB(err, SubMailLists) {
              MailLists[key] = value.toObject();
              MailLists[key].subEmails = SubMailLists;
              MailAttachment.find({"mail_id": value.mail_id}).sort('createdAt asc').exec(function findCB(err, Attachments) {
                MailLists[key] = value.toObject();
                MailLists[key].attachments = Attachments;
                callback();
              });
            });
          }, function (err) {
            if (err) console.error(err.message);
            next(MailLists);
          });
        });
      });
    } else if (fieldName == 'starred') {
      MailToFolder.find({
        'starred': 'yes',
        "user_id": user_id,
        "parent_id": ""
      }).sort('createdAt desc').skip(perPage * page).limit(perPage).populate('sender').populate('user').populate('mail').exec(function findCB(err, MailLists) {
        if (err) throw err;
        //next(MailLists);
        var AsyncCall = require('async');
        var obj = MailLists;
        AsyncCall.forEachOf(obj, function (value, key, callback) {
          MailToFolder.find({
            "user_id": user_id,
            "parent_id": value.id
          }).sort('createdAt asc').populate('sender').populate('user').populate('mail').exec(function findCB(err, SubMailLists) {
            MailLists[key] = value.toObject();
            MailLists[key].subEmails = SubMailLists;
            MailAttachment.find({"mail_id": value.mail_id}).sort('createdAt asc').exec(function findCB(err, Attachments) {
              MailLists[key] = value.toObject();
              MailLists[key].attachments = Attachments;
              callback();
            });
          });
        }, function (err) {
          if (err) console.error(err.message);
          next(MailLists);
        });
      });
    } else if (fieldName == 'important') {
      MailToFolder.find({
        'important': 'yes',
        "user_id": user_id,
        "parent_id": ""
      }).sort('createdAt desc').skip(perPage * page).limit(perPage).populate('sender').populate('user').populate('mail').exec(function findCB(err, MailLists) {
        if (err) throw err;
        //next(MailLists);
        var AsyncCall = require('async');
        var obj = MailLists;
        AsyncCall.forEachOf(obj, function (value, key, callback) {
          MailToFolder.find({
            "user_id": user_id,
            "parent_id": value.id
          }).sort('createdAt asc').populate('sender').populate('user').populate('mail').exec(function findCB(err, SubMailLists) {
            MailLists[key] = value.toObject();
            MailLists[key].subEmails = SubMailLists;
            MailAttachment.find({"mail_id": value.mail_id}).sort('createdAt asc').exec(function findCB(err, Attachments) {
              MailLists[key] = value.toObject();
              MailLists[key].attachments = Attachments;
              callback();
            });
          });
        }, function (err) {
          if (err) console.error(err.message);
          next(MailLists);
        });
      });
    } else {
      MailToFolder.find({
        'folder': fieldValue,
        "user_id": user_id,
        "parent_id": ""
      }).sort('createdAt desc').skip(perPage * page).limit(perPage).populate('sender').populate('user').populate('mail').exec(function findCB(err, MailLists) {
        if (err) throw err;
        //next(MailLists);
        var AsyncCall = require('async');
        var obj = MailLists;
        AsyncCall.forEachOf(obj, function (value, key, callback) {
          MailToFolder.find({
            "user_id": user_id,
            "parent_id": value.id
          }).sort('createdAt asc').populate('sender').populate('user').populate('mail').exec(function findCB(err, SubMailLists) {
            MailLists[key] = value.toObject();
            MailLists[key].subEmails = SubMailLists;
            MailAttachment.find({"mail_id": value.mail_id}).sort('createdAt asc').exec(function findCB(err, Attachments) {
              MailLists[key] = value.toObject();
              MailLists[key].attachments = Attachments;
              callback();
            });
          });
        }, function (err) {
          if (err) console.error(err.message);
          next(MailLists);
        });
      });
    }
  },
  /**
   * Description
   * @method getEMails___
   * @param {} fieldName
   * @param {} fieldValue
   * @param {} user_id
   * @param {} page
   * @param {} perPage
   * @param {} type
   * @param {} next
   * @return 
   */
  getEMails___: function (fieldName, fieldValue, user_id, page, perPage, type, next) {
    if (type == 'label') {
      MailToLabel.find({"label_id": fieldValue, "user_id": user_id}).exec(function findCB(err, MailLists) {
        if (err) throw err;
        var MailIDs = [];
        MailLists.forEach(function (c, i) {
          MailIDs.push(c.mail_id);
        });
        MailToFolder.find({
          "mail_id": MailIDs,
          "user_id": user_id,
          "parent_id": ""
        }).sort('createdAt desc').skip(perPage * page).limit(perPage).populate('sender').populate('user').populate('mail').exec(function findCB(err, eMailLists) {
          if (err) throw err;
          next(eMailLists);
        });
      });
    } else if (fieldName == 'starred') {
      MailToFolder.find({
        'starred': fieldValue,
        "user_id": user_id,
        "parent_id": ""
      }).sort('createdAt desc').skip(perPage * page).limit(perPage).populate('sender').populate('user').populate('mail').exec(function findCB(err, MailLists) {
        if (err) throw err;
        next(MailLists);
      });
    } else if (fieldName == 'important') {
      MailToFolder.find({
        'important': fieldValue,
        "user_id": user_id,
        "parent_id": ""
      }).sort('createdAt desc').skip(perPage * page).limit(perPage).populate('sender').populate('user').populate('mail').exec(function findCB(err, MailLists) {
        if (err) throw err;
        next(MailLists);
      });
    } else {
      MailToFolder.find({
        'folder': fieldValue,
        "user_id": user_id,
        "parent_id": ""
      }).sort('createdAt desc').skip(perPage * page).limit(perPage).populate('sender').populate('user').populate('mail').exec(function findCB(err, MailLists) {
        if (err) throw err;
        next(MailLists);
      });
    }
  },


  /**
   * Description
   * @method getDrafts
   * @param {} id
   * @param {} page
   * @param {} perPage
   * @param {} next
   * @return 
   */
  getDrafts: function (id, page, perPage, next) {
    Draft.find({"user_id": id}).sort('createdAt desc').skip(perPage * page).limit(perPage).populate('user').exec(function findCB(err, DraftLists) {
      if (err) throw err;
      next(DraftLists);
    });
  },

  /**
   * Description
   * @method getUserLabels
   * @param {} user_id
   * @param {} next
   * @return 
   */
  getUserLabels: function (user_id, next) {
    MailLabel.find({"user_id": user_id}).sort('name asc').populate('user').exec(function findCB(err, Labels) {
      if (err) throw err;
      next(Labels);
    });
  },

  /**
   * getMailBody: function(id, next) {
   * MailBody.findOne({id: id}).exec(function findCB(err, eMailBody) {
   * if(err) throw err;
   * next(eMailBody);
   * });
   * },
   * @method getMailDetails
   * @param {} folder
   * @param {} mail_id
   * @param {} user_id
   * @param {} next
   * @return 
   */
  getMailDetails: function (folder, mail_id, user_id, next) {
    MailToFolder
      .findOne({"folder": folder, "mail_id": mail_id, "user_id": user_id})
      .populateAll()
      .then(function (mail) {
        var attachments = MailAttachment.find({
          "mail_id": mail.mail_id
        })
          .then(function (attachments) {
            return attachments;
          });

        var from_mails = MailToFolder.find({
          "mail_id": mail.mail_id,
          "msg_type": 'from'
        })
          .populate('user')
          .then(function (from_mails) {
            return from_mails;
          });

        var to_mails = MailToFolder.find({
          "mail_id": mail.mail_id,
          "msg_type": 'to'
        })
          .populate('user')
          .then(function (to_mails) {
            return to_mails;
          });

        var cc_mails = MailToFolder.find({
          "mail_id": mail.mail_id,
          "msg_type": 'cc'
        })
          .populate('user')
          .then(function (cc_mails) {
            return cc_mails;
          });

        var bcc_mails = MailToFolder.find({
          "mail_id": mail.mail_id,
          "msg_type": 'bcc'
        })
          .populate('user')
          .then(function (bcc_mails) {
            return bcc_mails;
          });

        var sub_mails = MailToFolder.find({
          "user_id": user_id,
          "parent_id": mail.id
        })
          .sort('createdAt asc')
          //.populate('parent')
          .populate('sender')
          //.populate('user')
          .populate('mail')
          .then(function (SubMailLists) {
            var AsyncCall = require('async');
            var obj = SubMailLists;


            //console.log("*********************************8SubMailLists8*********************************");
            //console.log(SubMailLists);
            //console.log(SubMailLists);


            AsyncCall.forEachOf(obj, function (value, key, callback) {
              //console.log("*********************************from*********************************");
              //console.log(SubMailLists[key].createdAt);
              var sub_from_mails = MailToFolder.find({
                "mail_id": SubMailLists[key].mail_id,
                "msg_type": 'from'
              })
                .populate('user')
                .exec(function (from_mails) {
                  SubMailLists[key].from_mails = from_mails;
                  return from_mails;
                });
              //SubMailLists[key].from_mails___ = sub_from_mails;
              callback();
            }, function (err) {
              if (err) console.error(err.message);

              //We can skip this bellow 4 asyncCall as its overwriting on MailController.js before sending response.
              var obj = SubMailLists;
              AsyncCall.forEachOf(obj, function (value, key, callback) {
                //console.log("*********************************to*********************************");
                //console.log(SubMailLists[key].createdAt);
                var sub_to_mails = MailToFolder.find({
                  "mail_id": SubMailLists[key].mail_id,
                  "msg_type": 'to'
                })
                  .populate('user')
                  .exec(function findCB(err, to_mails) {
                    SubMailLists[key].to_mails = to_mails;
                    return to_mails;
                  });
                //SubMailLists[key].to_mails___ = sub_to_mails;
                callback();
              }, function (err) {
                if (err) console.error(err.message);

                var obj = SubMailLists;
                AsyncCall.forEachOf(obj, function (value, key, callback) {
                  //console.log("*********************************cc*********************************");
                  //console.log(SubMailLists[key].createdAt);
                  var sub_cc_mails = MailToFolder.find({
                    "mail_id": SubMailLists[key].mail_id,
                    "msg_type": 'cc'
                  })
                    .populate('user')
                    .exec(function findCB(err, cc_mails) {
                      SubMailLists[key].cc_mails = cc_mails;
                      return cc_mails;
                    });
                  //SubMailLists[key].cc_mails___ = sub_cc_mails;
                  callback();
                }, function (err) {
                  if (err) console.error(err.message);

                  var obj = SubMailLists;
                  AsyncCall.forEachOf(obj, function (value, key, callback) {
                    //console.log("*********************************bcc*********************************");
                    //console.log(SubMailLists[key].createdAt);
                    var sub_bcc_mails = MailToFolder.find({
                      "mail_id": SubMailLists[key].mail_id,
                      "msg_type": 'bcc'
                    })
                      .populate('user')
                      .exec(function (bcc_mails) {
                        SubMailLists[key].bcc_mails = bcc_mails;
                        return bcc_mails;
                      });
                    ///SubMailLists[key].bcc_mails___ = sub_bcc_mails;
                    callback();
                  }, function (err) {
                    if (err) console.error(err.message);
                  });
                });
              });
            });

            return SubMailLists;
          });

        return [mail, attachments, from_mails, to_mails, cc_mails, bcc_mails, sub_mails];
      })
      .spread(function (mail, attachments, from_mails, to_mails, cc_mails, bcc_mails, sub_mails) {
        mail = mail.toObject()
        mail.attachments = attachments;
        mail.from_mails = from_mails;
        mail.to_mails = to_mails;
        mail.cc_mails = cc_mails;
        mail.bcc_mails = bcc_mails;
        mail.sub_mails = sub_mails;

        console.log("*******************************************Final Email Details***********************************");
        console.log(mail);
        next(mail);
      })
      .catch(function (err) {
        if (err) console.error(err.message);
      });
  },

  /**
   * Description
   * @method setEmailFolder
   * @param {} id
   * @param {} folder
   * @param {} next
   * @return 
   */
  setEmailFolder: function (id, folder, next) {
    MailToFolder.findOne({"id": id}, function foundUser(err, eMail) {
      if (err) return next(err);
      eMail["folder"] = folder;
      eMail.save(function (err, mail) {
        if (err) return next(err);
        next(mail);
      });
    });
  },

  /**
   * Description
   * @method destroyDraft
   * @param {} id
   * @param {} next
   * @return 
   */
  destroyDraft: function (id, next) {
    Draft.destroy({id: id}).exec(function (err) {
      DraftAttachment.destroy({draft_id: id}).exec(function (err) {
        next();
      });
    });
  },
  /**
   * Description
   * @method calendarEventMail
   * @param {} data
   * @param {} next
   * @return 
   */
  calendarEventMail: function (data, next) {
    var siteUrl =data.siteUrl;
    var async = require('async');
    var calendarEvent = data.title;
    var calendarId = data.id;
    var calendarPlace = data.place;
    var calendarDescription = data.description;
    var calendarDate = data.start.date+' '+ data.start.time +' to '+ data.end.date+' '+ data.end.time;
    var attendeeId = data.people;
    var userDataPushed = [];
    attendeeId.forEach(function (people) {
      userDataPushed.push(function (callback) {
        var id = people.id;
          User.findOne({id: id}).exec(function (err, record) {
            if (record) {
              var pushData = {};
              pushData.name = record.first_name + ' ' + record.last_name;
              pushData.email = record.email;
              pushData.userId = people.id;
              pushData.response = people.response;
              callback(null, pushData);
            }
            if (!record) {
              Contacts.findOne({
                id: id
              }).exec(function (err, contactRecord) {
                if (err) {
                  console.log(err);
                }
                if (contactRecord) {
                  var pushDataContact = {};
                  pushDataContact.name = contactRecord.first_name + ' ' + contactRecord.last_name;
                  pushDataContact.email = contactRecord.email;
                  pushDataContact.userId = people.id;
                  pushDataContact.response = people.response;
                  callback(null, pushDataContact);
                }
              });
            }
          });
        }
      )
    });
    async.parallel(userDataPushed, function (err, result) {
      /* this code will run after all calls finished the job or
       when any of the calls passes an error */
      if (err) {
        next('error');
      }
      if (result) {
        var attendeeEmail = result;
        var finalResult = [];
        attendeeEmail.forEach(function (data) {
          finalResult.push(function (callback) {
            /**
             * Description
             * @method validateEmail
             * @param {} email
             * @return CallExpression
             */
            function validateEmail(email) {
              var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              return re.test(email);
            }
            if(validateEmail(data.email)) {
              sails.hooks.email.send(
                "testEmail",
                {
                  recipientName: data.name,
                  recipientId: data.userId,
                  eventId: calendarId,
                  senderName: "Sochara Application",
                  eventTitle: calendarEvent,
                  eventPlace:calendarPlace,
                  eventDescripton:calendarDescription,
                  eventDate:calendarDate,
                  eventPeople:result,
                  eventReturn: siteUrl+'/public/calendar/remoteEventResponse'
                },
                {
                  to: data.email,
                  subject: "Hi there"
                },
                function (err) {
                  callback(null, 'mail sent successfully!')
                }
              );
            }
            else{callback(null, 'not valid email');}
          });
        });
        async.parallel(finalResult, function (err, result) {
          if(err){
            next('error');
          }
          if(result){
            next('Success');
          }
        });
      }
    });
  }
};
