var actionUtil = require('sails/lib/hooks/blueprints/actionUtil'), _ = require('lodash'), util = require('util');

module.exports = {
  /**
   * Description
   * @return
   * @method getRecordById
   * @param {} model
   * @param {} id
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  getRecordById: function(model, id, req, res, next) {
    var Model = req._sails.models[model];
    var pk = actionUtil.requirePk(req) || id;
    var query = Model.findOne(pk);
    query = actionUtil.populateEach(query, req);
    query.exec(function foundCB(err, matchingRecord) {
      if (err) return res.serverError(err);
      next(matchingRecord);
    });
  },
  /**
   * Description
   * @return
   * @method getSearchResults
   * @param {} model
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  getSearchResults: function(model, req, res, next) {
    var Model = req._sails.models[model];
    if ( !Model ) throw new Error(util.format('Invalid route option, "model".\nI don\'t know about any models named: `%s`',model));
    var query = Model.find();
      if ( true ) {
        query.where( actionUtil.parseCriteria(req) );
      }
      query.limit( actionUtil.parseLimit(req) );
      query.skip( actionUtil.parseSkip(req) );
      query.sort( actionUtil.parseSort(req) );
      query = actionUtil.populateEach(query, req);
    query.exec(function foundCB(err, matchingRecords) {
      if(err) throw err;
      next(matchingRecords);
    });
  },
  /**
   * Description
   * @return
   * @method getRecords
   * @param {} model
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  getRecords: function(model, req, res, next) {
    var Model = req._sails.models[model];
    if ( !Model ) throw new Error(util.format('Invalid route option, "model".\nI don\'t know about any models named: `%s`',model));
    var query = Model.find()
      .where( actionUtil.parseCriteria(req) )
      .limit( actionUtil.parseLimit(req) )
      .skip( actionUtil.parseSkip(req) )
      .sort( actionUtil.parseSort(req) );
      query = actionUtil.populateEach(query, req);
    query.exec(function foundCB(err, matchingRecords) {
      if(err) throw err;
      next(matchingRecords);
    });
  },
  /**
   * Description
   * @return
   * @method getRecordsWhere
   * @param {} model
   * @param {} where
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  getRecordsWhere: function(model, where, req, res, next) {
    var Model = req._sails.models[model];
    if ( !Model ) throw new Error(util.format('Invalid route option, "model".\nI don\'t know about any models named: `%s`',model));
    var query = Model.find()
      .where( where )
      .limit( actionUtil.parseLimit(req) )
      .skip( actionUtil.parseSkip(req) )
      .sort( actionUtil.parseSort(req) );
      query = actionUtil.populateEach(query, req);
    query.exec(function foundCB(err, matchingRecords) {
      if(err) throw err;
      next(matchingRecords);
    });
  }
};
