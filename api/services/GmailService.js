/**
 Help Links:
 https://github.com/google/google-api-nodejs-client
 http://stackoverflow.com/questions/24460422/how-to-send-a-message-successfully-using-the-new-gmail-rest-api
 http://stackoverflow.com/questions/30762369/gmail-api-playground-send-method-converted-mime-raw-header-not-populating-emai
 */
var NodeCache = require("node-cache");
var memoryCache = new NodeCache({stdTTL: 60 * 60 * 1000, checkperiod: 120});
const base64url = require('base64url');

module.exports = {

  /**
   * Description
   * @return
   * @method getMailThreads
   * @param {} gmailId
   * @param {} LoggedInUserID
   * @param {} labelID
   * @param {} maxResults
   * @param {} pageToken
   * @param {} q
   * @param {} withOutCache
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  getMailThreads: function (gmailId, LoggedInUserID, labelID, maxResults, pageToken, q, withOutCache, req, res, next) {

    EmailAccount.findOne({id: gmailId, user: LoggedInUserID, provider: 'gmail'}, function froundCB(err, gRecord) {

      var cacheManagerKey = 'mail_threads_' + gmailId + '_' + LoggedInUserID + '_' + labelID + '_' + pageToken;

      memoryCache.get(cacheManagerKey, function (err, result) {
        if (err) {
          console.log(err);
        }

        if (result && withOutCache === 'yes') {
          // console.log("Displaying threads from Cache key: " + cacheManagerKey);
          return next(result);
        }

        // console.log("Displaying threads from Gmail API Call");
        req.oauth2Client.setCredentials({
          access_token: gRecord.access_token,
          refresh_token: gRecord.refresh_token
        });
        var $params = {};
        $params.labelIds = [labelID];
        $params.maxResults = maxResults;
        if (pageToken && pageToken !== '') {
          $params.pageToken = pageToken;
        }
        if (q && q !== '') {
          $params.q = q;
        }
        $params.userId = 'me';
        $params.auth = req.oauth2Client;

        req.gmail.users.threads.list($params, function (err, data) {
          var threads = [];
          if (err) console.log(err);
          var AsyncCall = require('async');
          threads = data.threads;
          AsyncCall.forEachOf(threads, function (value, key, callback) {
            req.gmail.users.threads.get({
              id: value.id,
              format: 'full',
              userId: 'me',
              auth: req.oauth2Client
            }, function (err, body) {
              if (err) console.log(err);
              threads[key]['metadata'] = body.messages;
              callback();
            });
          }, function (err) {
            if (err) console.log(err);
            data.threads = threads;


            memoryCache.set(cacheManagerKey, data, function (err, success) {
              if (!err && success) {
                // console.log("Created threads Cache key: " + cacheManagerKey);
              } else {
                console.log("Somthing went wrong, can not create cache key: " + cacheManagerKey);
                console.log("Error from cache manager: " + err);
              }

              // console.log("Thread Messages:");
              // console.log(data);
              next(data);
            });
          });
        });
      });


    });
  },


  /**
   * Description
   * @method getMailThreadsCompact
   * @param {} gmailId
   * @param {} LoggedInUserID
   * @param {} labelID
   * @param {} maxResults
   * @param {} pageToken
   * @param {} q
   * @param {} withOutCache
   * @param {} access_token
   * @param {} refresh_token
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  getMailThreadsCompact: function (gmailId, LoggedInUserID, labelID, maxResults, pageToken, q, withOutCache, access_token, refresh_token, req, res, next) {
    // console.log("Displaying threads from Gmail API Call");
    req.oauth2Client.setCredentials({
      access_token: access_token,
      refresh_token: refresh_token
    });
    var $params = {};
    $params.labelIds = [labelID];
    $params.maxResults = maxResults;
    if (pageToken && pageToken !== '') {
      $params.pageToken = pageToken;
    }
    if (q && q !== '') {
      $params.q = q;
    }
    $params.userId = 'me';
    $params.auth = req.oauth2Client;

    req.gmail.users.threads.list($params, function (err, data) {
      var threads = [];
      if (err) console.log(err);
      var AsyncCall = require('async');
      if (data) {
        threads = data.threads;
        AsyncCall.forEachOf(threads, function (value, key, callback) {
          if (value.id) {
            req.gmail.users.threads.get({
              id: value.id,
              format: 'full',
              userId: 'me',
              auth: req.oauth2Client
            }, function (err, body) {
              if (err) console.log(err);
              if (body && body.messages) {
                threads[key]['metadata'] = body.messages;
              }

              callback();
            });
          } else {
            callback();
          }

        }, function (err) {
          if (err) console.log(err);
          data.threads = threads;
          next(data);
        });
      }
    });
  },

  /**
   * Description
   * @return
   * @method getMailMessages
   * @param {} LoggedInUserID
   * @param {} labelID
   * @param {} maxResults
   * @param {} pageToken
   * @param {} q
   * @param {} withOutCache
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  getMailMessages: function (LoggedInUserID, labelID, maxResults, pageToken, q, withOutCache, req, res, next) {
    User.findOne(LoggedInUserID, function froundCB(err, loggedInUser) {


      var cacheManagerKey = 'mail_messages_' + gmailId + '_' + LoggedInUserID + '_' + labelID + '_' + pageToken;

      memoryCache.get(cacheManagerKey, function (err, result) {
        if (err) {
          console.log(err);
        }

        if (result && withOutCache == 'yes') {
          console.log("Displaying messages from Cache key: " + cacheManagerKey);
          return next(result);
        }

        console.log("Displaying messages from Gmail API Call");
        req.oauth2Client.setCredentials({
          access_token: loggedInUser.access_token,
          refresh_token: loggedInUser.refresh_token
        });
        var $params = {};
        $params.labelIds = [labelID];
        $params.maxResults = maxResults;
        if (pageToken && pageToken != '') {
          $params.pageToken = pageToken;
        }
        if (q && q != '') {
          $params.q = q;
        }
        $params.userId = 'me';
        $params.auth = req.oauth2Client;
        req.gmail.users.messages.list($params, function (err, data) {
          if (err) console.log(err);
          var AsyncCall = require('async');
          var messages = data.messages;
          var obj = data.messages;
          AsyncCall.forEachOf(obj, function (value, key, callback) {
            req.gmail.users.messages.get({
              id: value.id,
              format: 'metadata',
              userId: 'me',
              auth: req.oauth2Client
            }, function (err, body) {
              if (err) console.log(err);
              messages[key]['metadata'] = body;
              callback();
            });
          }, function (err) {
            if (err) console.log(err);
            data.messages = messages;


            memoryCache.set(cacheManagerKey, data, function (err, success) {
              if (!err && success) {
                console.log("Created messages Cache key: " + cacheManagerKey);
              } else {
                console.log("Somthing went wrong, can not create cache key: " + cacheManagerKey);
                console.log("Error from cache manager: " + err);
              }

              next(data);
            });
          });
        });
      });


    });
  },

  /**
   * Description
   * @return
   * @method getListsOfMailboxes
   * @param {} LoggedInUserID
   * @param {} gmailId
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  getListsOfMailboxes: function (LoggedInUserID, gmailId, req, res, next) {
    EmailAccount.findOne({id: gmailId, user: LoggedInUserID, provider: 'gmail'}, function froundCB(err, record) {

      var cacheManagerKey = 'mail_boxes_' + gmailId + '_' + LoggedInUserID;

      memoryCache.get(cacheManagerKey, function (err, result) {
        if (err) {
          console.log(err);
        }

        if (result) {
          console.log("Displaying mailboxes from Cache key: " + cacheManagerKey);
          return next(result);
        }

        console.log("Displaying mailboxes from Gmail API Call");

        req.oauth2Client.setCredentials({
          access_token: record.access_token,
          refresh_token: record.refresh_token
        });
        req.gmail.users.labels.list({userId: 'me', auth: req.oauth2Client}, function (err, data) {
          if (err) console.log(err);

          memoryCache.set(cacheManagerKey, data, function (err, success) {
            if (!err && success) {
              console.log("Created mailboxes Cache key: " + cacheManagerKey);
            } else {
              console.log("Somthing went wrong, can not create cache key: " + cacheManagerKey);
              console.log("Error from cache manager: " + err);
            }

            next(data);
          });

        });
      });
    });
  },

  /**
   * Description
   * @return
   * @method createNewLabel
   * @param {} gmailId
   * @param {} LoggedInUserID
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  createNewLabel: function (gmailId, LoggedInUserID, req, res, next) {
    EmailAccount.findOne({id: gmailId, user: LoggedInUserID, provider: 'gmail'}, function froundCB(err, record) {
      req.oauth2Client.setCredentials({
        access_token: record.access_token,
        refresh_token: record.refresh_token
      });
      req.gmail.users.labels.create({
        resource: {
          name: req.param('label'),
          messageListVisibility: 'show',
          labelListVisibility: 'labelShow'
        }, userId: 'me', auth: req.oauth2Client
      }, function (err, data) {
        if (err) console.log(err);

        var cacheManagerKey = 'mail_boxes_' + gmailId + '_' + LoggedInUserID;
        req.gmail.users.labels.list({userId: 'me', auth: req.oauth2Client}, function (err, data) {
          if (err) console.log(err);
          console.log("GmailService createNewLabel: ");
          // console.log(data);
          memoryCache.del(cacheManagerKey);
          next(data);
        });
      });
    });

  },

  /**
   * Description
   * @return
   * @method getMailThreadDetails
   * @param {} gmailID
   * @param {} LoggedInUserID
   * @param {} threadID
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  getMailThreadDetails: function (gmailID, LoggedInUserID, threadID, req, res, next) {

    EmailAccount.findOne({
      id: gmailID,
      user: LoggedInUserID,
      provider: 'gmail'
    }, function froundCB(err, loggedInUser) {
      if( loggedInUser ){
        req.oauth2Client.setCredentials({
          access_token: loggedInUser.access_token,
          refresh_token: loggedInUser.refresh_token
        });
        req.gmail.users.threads.get({
          id: threadID,
          format: 'full',
          userId: 'me',
          auth: req.oauth2Client
        }, function (err, data) {
          // req.gmail.users.threads.get({ id: value.id, format: 'full', userId: 'me', auth: req.oauth2Client }, function (err, body) {
          if (err) console.log(err);

          // console.log(data);
          // Mail Server - Way 1
          // fetching individual message raw data
          var obj = data.messages;
          // req.gmail.users.messages.get({
          //   id: obj[data.messages.length - 1].id,
          //   format: 'raw',
          //   userId: 'me',
          //   auth: req.oauth2Client
          // }, function (err, messageRawData) {
          //   if (err) console.log(err);
          //   //console.log("GmailService getMailDetails: ");
          //   //console.log(data);
          //   data.messages[data.messages.length - 1].raw = messageRawData.raw;
          //   next(data);
          // });

          next(data);

          // From Mail Service - Way 2
          // GmailService.getMailDetails(gmailID, LoggedInUserID, obj[data.messages.length - 1].id, req, res, function (messageRawData) {
          //   // console.log("messageRawData");
          //   // console.log(messageRawData);
          //   data.messages[data.messages.length - 1].raw = messageRawData.raw;
          //   next(data);
          // });

        });
      }else{
        res.send("Gmail ID not Found");
      }
    });

  },

  /**
   * Description
   * @return
   * @method getMailDetails
   * @param {} gmailID
   * @param {} LoggedInUserID
   * @param {} mailID
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  getMailDetails: function (gmailID, LoggedInUserID, mailID, req, res, next) {

    EmailAccount.findOne({
      id: gmailID,
      user: LoggedInUserID,
      provider: 'gmail'
    }, function froundCB(err, loggedInUser) {
      req.oauth2Client.setCredentials({
        access_token: loggedInUser.access_token,
        refresh_token: loggedInUser.refresh_token
      });
      req.gmail.users.messages.get({
        id: mailID,
        format: 'raw',
        userId: 'me',
        auth: req.oauth2Client
      }, function (err, data) {
        if (err) console.log(err);
        //console.log("GmailService getMailDetails: ");
        //console.log(data);
        next(data);
      });
    });

  },

  /**
   * Description
   * @return
   * @method getMailAttachment
   * @param {} gmailID
   * @param {} LoggedInUserID
   * @param {} mailID
   * @param {} attachmentID
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  getMailAttachment: function (gmailID, LoggedInUserID, mailID, attachmentID, req, res, next) {

    EmailAccount.findOne({
      id: gmailID,
      user: LoggedInUserID,
      provider: 'gmail'
    }, function froundCB(err, loggedInUser) {
      req.oauth2Client.setCredentials({
        access_token: loggedInUser.access_token,
        refresh_token: loggedInUser.refresh_token
      });
      req.gmail.users.messages.attachments.get({
        id: attachmentID,
        messageId: mailID,
        userId: 'me',
        auth: req.oauth2Client
      }, function (err, data) {
        if (err) console.log(err);
        //console.log("GmailService getMailDetails: ");
        //console.log(data);
        next(data);
      });
    });

  },

  /**
   * Description
   * @return
   * @method modifyThread
   * @param {} gmailID
   * @param {} LoggedInUserID
   * @param {} threadIDS
   * @param {} requestBody
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  modifyThread: function (gmailID, LoggedInUserID, threadIDS, requestBody, req, res, next) {

    EmailAccount.findOne({id: gmailID, user: LoggedInUserID, provider: 'gmail'}, function froundCB(err, gRecord) {
      if (gRecord) {
        req.oauth2Client.setCredentials({
          access_token: gRecord.access_token,
          refresh_token: gRecord.refresh_token
        });

        var threadIDArr = threadIDS || [];
        var AsyncCall = require('async');
        var obj = threadIDArr;
        var dataRet = {};
        AsyncCall.forEachOf(obj, function (value, key, callback) {
          req.gmail.users.threads.modify({
            id: value,
            userId: 'me',
            resource: requestBody,
            auth: req.oauth2Client
          }, function (err, data) {
            if (err) console.log(err);
            // console.log("GmailService  modifyThread:");
            // console.log(data);
            dataRet = data;
            callback();
          });
        }, function (err) {
          if (err) console.error(err.message);
          next(dataRet);
        });
      } else {
        next({});
      }
    });

  },

  /**
   * Description
   * @method formatGmailData
   * @param {} data
   * @param {} callback
   * @return 
   */
  formatGmailData: function ( data, callback ) {
    let currentThread = {messages: [], read: false};
    if(data && data.messages.length){
      data.messages.forEach(function (value) {
        let currentThreadMessage = extractGMailThreadData(value);
        currentThread.read = !isReadMail(value);
        currentThread.messages.push(currentThreadMessage.body || null);
        if(currentThreadMessage.attachments){
          currentThread.attachments = currentThreadMessage.attachments;
        }
      });
    }
    callback(currentThread);
  },

  /**
   * Description
   * @return
   * @method deleteThreadParmanent
   * @param {} gmailID
   * @param {} LoggedInUserID
   * @param {} threadIDS
   * @param {} requestBody
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  deleteThreadParmanent: function (gmailID, LoggedInUserID, threadIDS, requestBody, req, res, next) {

    EmailAccount.findOne({id: gmailID, user: LoggedInUserID, provider: 'gmail'}, function froundCB(err, gRecord) {
      req.oauth2Client.setCredentials({
        access_token: gRecord.access_token,
        refresh_token: gRecord.refresh_token
      });

      var threadIDArr = threadIDS || [];
      var AsyncCall = require('async');
      var dataRet = {};
      AsyncCall.forEachOf(threadIDArr, function (value, key, callback) {
        req.gmail.users.threads.delete({
          id: value,
          userId: 'me',
          resource: requestBody,
          auth: req.oauth2Client
        }, function (err, data) {
          if (err) console.log(err);
          // console.log("GmailService  modifyThread:");
          // console.log(data);
          dataRet = data;
          callback();
        });
      }, function (err) {
        if (err) console.error(err.message);
        next(dataRet);
      });
    });

  },


  /**
   * Description
   * @return
   * @method getGmailThreadCount
   * @param {} gmailID
   * @param {} LoggedInUserID
   * @param {} boxIDS
   * @param {} requestBody
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  getGmailThreadCount: function (gmailID, LoggedInUserID, boxIDS, requestBody, req, res, next) {
    EmailAccount.findOne({
      id: gmailID,
      user: LoggedInUserID,
      provider: 'gmail'
    }, function froundCB(err, loggedInUser) {
      req.oauth2Client.setCredentials({
        access_token: loggedInUser.access_token,
        refresh_token: loggedInUser.refresh_token
      });

      /// Modified
      var AsyncCall = require('async');
      var obj = boxIDS;
      var resData = [];
      AsyncCall.forEachOf(obj, function (value, key, callback) {
        //req.gmail.users.labels.get({ id: boxID, userId: 'me', resource: requestBody, auth: req.oauth2Client }, function (err, data) {
        req.gmail.users.labels.get({id: value, userId: 'me', auth: req.oauth2Client}, function (err, data) {
          if (err) console.log(err);
          var dataRet = {};
          if (data) {
            dataRet.id = data.id;
            dataRet.total = data.threadsTotal;
            dataRet.unread = data.threadsUnread;
          }
          resData.push(dataRet);
          callback();
        });
      }, function (err) {
        if (err) console.error(err.message);
        next(resData);
      });
      /// Modified
    });
  },

  /**
   * Description
   * @return
   * @method modifyThreadOldModify
   * @param {} LoggedInUserID
   * @param {} threadID
   * @param {} requestBody
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  modifyThreadOldModify: function (LoggedInUserID, threadID, requestBody, req, res, next) {

    User.findOne(LoggedInUserID, function froundCB(err, loggedInUser) {
      req.oauth2Client.setCredentials({
        access_token: loggedInUser.access_token,
        refresh_token: loggedInUser.refresh_token
      });
      req.gmail.users.threads.modify({
        id: threadID,
        userId: 'me',
        resource: requestBody,
        auth: req.oauth2Client
      }, function (err, data) {
        if (err) console.log(err);
        next(data);
      });
    });

  },

  /**
   * Description
   * @return
   * @method sendEmail
   * @param {} access_token
   * @param {} refresh_token
   * @param {} threadID
   * @param {} requestBody
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  sendEmail: function (access_token, refresh_token, threadID, requestBody, req, res, next) {

    req.oauth2Client.setCredentials({
      access_token: access_token,
      refresh_token: refresh_token
    });
    req.gmail.users.messages.send({userId: 'me', resource: requestBody, auth: req.oauth2Client}, function (err, data) {
      if (err) console.log(err);
      // console.log("GmailService  sendEmail:");
      // console.log(data);
      next(data);
    });

  },




  //EmailJS Imap Client Backup
  /**
   * Description
   * @return
   * @method emailJS_Imap_getListsOfMailboxes
   * @param {} LoggedInUserID
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  emailJS_Imap_getListsOfMailboxes: function (LoggedInUserID, req, res, next) {

    // req.gmail_client.connect().then(() => {
    //     req.gmail_client.listMailboxes().then((mailboxes) => {
    //         next(mailboxes);
    //     });
    // });

  },

  /**
   * Description
   * @return
   * @method emailJS_Imap_createNewLabel
   * @param {} LoggedInUserID
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  emailJS_Imap_createNewLabel: function (LoggedInUserID, req, res, next) {

    // req.gmail_client.connect().then(() => {
    //     req.gmail_client.createMailbox(req.param('label')).then(() => {
    //         req.gmail_client.listMailboxes().then((mailboxes) => {
    //             next(mailboxes);
    //         });
    //     });
    // });

  }

};

/**
 * Description
 * @method extractGMailThreadData
 * @param {} data
 * @return returnData
 */
function extractGMailThreadData( data ){

  let attachments = {images: [], zips: [], pdfs: [], docs: []};
  let returnData = {};


  let result = '';
  // In e.g. a plain text message, the payload is the only part.
  let parts = [data.payload];
  let part;
  let contentUrls = {};
  while (parts.length) {
    part = parts.shift();
    if (part.parts) {
      parts = parts.concat(part.parts);
    }

    if (part.mimeType === 'text/plain') {
      result = deCodeData(part.body.data);
      result = removeStyle(result);
    } else if (part.mimeType === 'text/html') {
      result = deCodeData(part.body.data);
      result = removeStyle(result);
    }else if (part.mimeType === "image/jpeg" || part.mimeType === "image/png" || part.mimeType === "image/gif") {
      // let contentVal = checkIfInlineImage(part);
      // if(contentVal){
      //   contentUrls.contentVal = "https://gm1.ggpht.com/is4abhJ88U2fGRfwYs-0fUAW82B2TdH70KYTIHiscfZs580-Dg9ai8VO4ciGmGge700O6H68tE90RVo1qErgRLhVx6JEReX7Ags976h_fs6Cs9KcE8uPMG0WeP4nGGCgKovdoR-Melnf8aaY00mUdhS3nMiAcvu0vRWP9NV6HPG4j9eY893IvbtcLMUkJMv88F4pj9hIdbNWCA8GZqvb4fZkFGVmU5yj_Yhgv59ys17fUqHPZVx-g4LnzdhFhtg2VxqVMDGgshFJxmliMn9VFjMQGwwNZNCSRrLQh_pmRhT94hWxMjv3VDf_AY-2AysZCQyVLvWTgMpywq5yb-aRg6qvROAafJeQNThNn-X6n-WEmGbYVu3rmA4Q2dUs7llNI8XtLcQdnTjHBR8ZNCxZMnteFdOIqkKb_lZYWYsryh7cuaEVChwQrafOtItzuIfH-Jvxb2JCxm5RurbosQ2WmA26tfD9ThremlcyNj8_1Al5IpEGXW03Ed8r5-J7QJZNBoclabjSL5HW0M_Ss0ahGTdcPJqLYlnFvzr4FqGOurkbzGMRWuywk3ahYkLr-vW8N-Y4lYgekbVzaBt8K1SHbhDJ13Qq3BF5SPC5bWzC37cVDaDMyKWsZybcstPkLXClgChkqexBmQvaARVOTzc0C8bFMTeYu5EetVyORXyKmFPzF1TprkoduMOTpRPW7Q=w64-h64-l75-ft";
      // }else{
      //   attachments.images.push({id: part.body.attachmentId, size: part.body.size || '', name: part.filename});
      // }
      attachments.images.push({id: part.body.attachmentId, size: part.body.size || '', name: part.filename});
    } else if (part.mimeType === "application/zip" || part.mimeType === "application/x-zip-compressed") {
      attachments.zips.push({id: part.body.attachmentId, size: part.body.size || '', name: part.filename});
    } else if (part.mimeType === "application/pdf") {
      attachments.pdfs.push({id: part.body.attachmentId, size: part.body.size || '', name: part.filename});
    } else if (part.mimeType === "application/msword" || part.mimeType === "plication/vnd.openxmlformats-officedocument.wordprocessingml.document" || part.mimeType === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
      attachments.docs.push({id: part.body.attachmentId, size: part.body.size || '', name: part.filename});
    }
  }

  returnData.body = result;
  if( attachments.images.length || attachments.zips.length || attachments.pdfs.length || attachments.docs.length){
    returnData.attachments = attachments;
  }
  return returnData;
}

/**
 * Description
 * @method deCodeData
 * @param {} body
 * @return CallExpression
 */
function deCodeData( body ){
  return base64url.decode(body);
  // return (new Buffer(body.replace(/-/g, '+').replace(/_/g, '/'), "base64")).toString();
}

/**
 * Description
 * @method checkIfInlineImage
 * @param {} part
 * @return LogicalExpression
 */
function checkIfInlineImage( part ){
  let indexedHeaders = part.headers.reduce(function(acc, header) {
    acc[header.name.toLowerCase()] = header.value;
    return acc;
  }, {});
  let contentId = indexedHeaders['content-id'] || '';
  let xAttachmentId = indexedHeaders['x-attachment-id'] || '';
  return contentId || xAttachmentId;
}


/**
 * Description
 * @method removeStyle
 * @param {} str
 * @return CallExpression
 */
function removeStyle(str) {
  let basic = str.replace(/\r?\n|\r/g, "").replace(/<style(.*?)<\/style>/gm, "");
  return basic.replace(/<!--[\s\S]*?-->/g, "");
}


/**
 * Description
 * @method isReadMail
 * @param {} val
 * @return BinaryExpression
 */
function isReadMail( val ) {
  return val.labelIds.indexOf("UNREAD") > -1;
}
