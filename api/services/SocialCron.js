var facebookConfigure = {id: '1589011794492434', secret: '4bd36c109d167cb1b161e76a1a68f81d'};
var FB = require('fb');
var async = require('async');
var Twitter = require('twitter');
var request = require('request');
var path = require('path');
var moment = require('moment');
var instagramApi = require('instagram-node').instagram();

module.exports = {
  /**
   * Description
   * @method cronSocialFeed
   * @param {} callback
   * @return 
   */
  cronSocialFeed: function (callback) {
    SocialAuth.find().exec(function (err, respSocialAccounts) {
      async.forEach(respSocialAccounts, function (respSocialAccount, accountCallback) {
        if (respSocialAccount.provider === 'facebook') {
          SocialData.findOne({
            user: respSocialAccount.user,
            provider: respSocialAccount.provider,
            select: ['_id', 'date_sort', 'createdAt']
          }).sort({date_sort: -1}).exec(function foundCB(err, lastData) {
            // let minutesAgo = moment().subtract(20, 'minutes').unix();
            if( lastData ){
              let lastTime = lastData.date_sort;
              FB.setAccessToken(respSocialAccount.accessToken);
              FB.api('me/posts' + '?fields=' + 'attachments{description,media,target,title,url,type,description_tags,subattachments},source,message,story,type,link,full_picture,description,icon,name,place,from,created_time,application' + '&since=' + lastTime + '&limit=' + 100, function (response) {
                if (!response || response.error) {
                  console.log(!response ? 'error occurred' : response.error);
                }
                if( response ){
                  DashboardService.facebookPostExtractFromCron(response, respSocialAccount, function (returnFromFacebook) {
                    if (returnFromFacebook) {
                      return accountCallback();
                    }
                  });
                }
              });
            }else{
              return accountCallback();
            }
          });

        } else if (respSocialAccount.provider === 'twitter') {
          let client = new Twitter({
            consumer_key: '2ourwWtAYQBvpUAn150SXe9oY',
            consumer_secret: 'ypZfC5xdrEyqVTycFvMrV80F7asJnUio5Bswp60PyOzQF4cfPg',
            access_token_key: respSocialAccount.accessToken,
            access_token_secret: respSocialAccount.accessTokenSecret
          });
          client.get('statuses/user_timeline', { exclude_replies: true, count: 5 }, function (error, tweets) {
            // console.log(tweets[0].entities.urls[0]);
            DashboardService.twitterPostExtract(tweets, respSocialAccount, function (returnFromTwitter) {
              if (returnFromTwitter) {
                return accountCallback();
              }
            });
          });
        } else if (respSocialAccount.provider === 'instagram') {
          let minutesAgo = moment().subtract(20, 'minutes').unix();
          instagramApi.use({access_token: respSocialAccount.accessToken});
          instagramApi.user_media_recent(respSocialAccount.providerId, { min_timestamp: minutesAgo, count: 50 }, function (err, medias, pagination, remaining, limit) {
            DashboardService.instagramPostExtract(medias, respSocialAccount, function (returnFromInstagram) {
              if (returnFromInstagram) {
                return accountCallback();
              }
            });
          });
        }
      }, function (err) {
        if (err) {
          console.log(err);
        } else {
          return callback();
        }
      });
    });
  },

  /**
   * Description
   * @method refreshToken
   * @param {} callback
   * @return 
   */
  refreshToken: function (callback) {
    SocialAuth.find({provider: 'facebook'}).exec(function (err, respSocialAccounts) {
      if (respSocialAccounts && respSocialAccounts.length) {
        async.forEach(respSocialAccounts, function (respSocialAccount, accountCallback) {
          if(respSocialAccount && respSocialAccount.expire_date && moment().isAfter(moment(respSocialAccount.expire_date).subtract(3,'days'))){
            FB.api('oauth/access_token', {
              grant_type: 'fb_exchange_token',
              client_id: facebookConfigure.id,
              client_secret: facebookConfigure.secret,
              fb_exchange_token: respSocialAccount.accessToken
            }, function (resp) {
              if (!resp || resp.error) {
                console.log(!resp ? 'error occurred' : resp.error);
                return accountCallback();
              }
              let socialData = {accessToken: resp.access_token, expires_in: resp.expires_in};
              socialData.expire_date = moment().add(socialData.expires_in, 'seconds').format('YYYY-MM-DD HH:mm:ss');
              SocialAuth.update({id: respSocialAccount.id}, socialData).exec(function (err, respUser) {
                if (respUser && respUser[0]) {
                  return accountCallback();
                }
              });
            });
          }else{
            return accountCallback();
          }

        }, function (err) {
          if (err) {
            console.log(err);
          } else {
            return callback();
          }
        });
      } else {
        return callback();
      }
    });


  }
};

