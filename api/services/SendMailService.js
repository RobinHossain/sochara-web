var api_key = 'key-e1d1201962871b1417f00c1be487466e';
var pub_api_key = 'pubkey-68856bd5339dd9181a3013a434c68376';
var domain = 'sochara.com';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain, publicApiKey: pub_api_key});


module.exports = {

  /**
   * Description
   * @method send
   * @param {} sendData
   * @param {} callback
   * @return 
   */
  send: function (sendData, callback) {

    sendData.from = sendData.from || 'Sochara <info@sochara.com>';
    if( !sendData.to ){
      callback();
    }

    eMailTemplate.getFormatted(sendData.html, function ( respHtml ) {
      sendData.html = respHtml;
      sendData.subject = sendData.subject || '';
      if( sendData.to ){
        sendData.to = sendData.to.join(',');
      }
      if( sendData.cc ){
        sendData.cc = sendData.cc.join(',');
      }
      if( sendData.bcc ){
        sendData.bcc = sendData.bcc.join(',');
      }
      sendMailByMailgun(sendData, function ( sendResp ) {
        callback(sendResp);
      });
    });
  }

};

/**
 * Description
 * @method sendMailByMailgun
 * @param {} sendData
 * @param {} next
 * @return 
 */
function sendMailByMailgun(sendData, next) {
  mailgun.messages().send(sendData, function (error, body) {
    if( error ) console.log(error);
    next(body);
  });
}
