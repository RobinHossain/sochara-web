const async = require('async');
module.exports = {
  /**
   * Description
   * @return
   * @method getSharedAlbums
   * @param {} userId
   * @param {} next
   * @return 
   */
  getSharedAlbums: function (userId, next) {
    SharedMedia.find({
      album: {$ne: null},
      users: userId
    }).populate('album', {select: ['_id', 'name', 'cover', 'updatedAt']}).exec(function (err, resp) {
      let records = resp.map((media)=>{
        return media.album;
      });
      let totalRecords = [];
      async.forEach(records, function (item, callback) {
        UserFiles.count({album: item.id}).exec(function countCB(error, found) {
          let totalItem = found, coverPhoto = item.cover;
          UserFiles.count({album: item.id, file_type: 'video'}).exec(function countCB(error, foundv) {
            item.videos = foundv;
            item.photos = totalItem - foundv;
            UserFiles.findOne({
              album: item.id,
              select: ['_id', 'thumb_dir']
            }).sort({createdAt: -1}).exec(function (err, record) {
              if (typeof(coverPhoto) === 'undefined' && record) {
                item.cover = record.thumb_dir;
              }
              totalRecords.push(item);
              callback();
            });
          });
        });
      }, function (err) {
        if (err) console.log(err);
        next(totalRecords);
      });
    });

  },
};


