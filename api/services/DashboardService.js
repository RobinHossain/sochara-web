var AsyncCall = require('async');
var FB = require('fb');
var moment = require('moment');

module.exports = {
  /**
   * Description
   * @method facebookPostExtract
   * @param {} facebookData
   * @param {} userData
   * @param {} backToController
   * @return
   */
  facebookPostExtract: function (facebookData, userData, backToController) {
    if (facebookData && facebookData.data && facebookData.data.length) {
      AsyncCall.eachOf(facebookData.data, function (feedItem, key, callback) {

        try {
          let dataObj = {};
          dataObj.post = feedItem.message || '';
          dataObj.source = feedItem.source || '';
          dataObj.story = feedItem.story || '';
          dataObj.post_id = feedItem.id || '';
          dataObj.type = feedItem.type || '';
          dataObj.media = feedItem.full_picture || '';
          dataObj.link = feedItem.link || '';
          dataObj.icon = feedItem.icon || '';
          dataObj.created = feedItem.created_time || new Date();
          dataObj.date_sort = feedItem.created_time ? moment(new Date(feedItem.created_time)).unix() : moment(new Date()).unix();
          dataObj.from = feedItem.from || null;
          dataObj.provider = 'facebook';
          dataObj.user = userData.user;
          if(feedItem.application){
            dataObj.application = feedItem.application.id || undefined;
          }
          if(feedItem.attachments && feedItem.attachments.data.length){
            dataObj.attachment_details = {type: feedItem.attachments.data[0].type};
            dataObj.attachment_details.title = feedItem.attachments.data[0].title || '';
            dataObj.attachment_details.url = feedItem.attachments.data[0].url || '';
            dataObj.attachment_details.description = feedItem.attachments.data[0].description || '';
            dataObj.attachment_details.media = feedItem.attachments.data[0].media ? feedItem.attachments.data[0].media.image.src : '';
            if( dataObj.attachment_details.type ){
              if( dataObj.attachment_details.type === 'album' || dataObj.attachment_details.type === 'new_album' ){
                // dataObj.attachments = [];
                dataObj.attachments = feedItem.attachments.data[0].subattachments.data.map(val=>{
                  return {type: val.type || '', url: val.url || '', media: val.media ? val.media.image.src : ''};
                });
                // feedItem.attachments.data[0].subattachments.data.forEach(function (val, key) {
                //   dataObj.attachments.push({type: val.type || '', url: val.url || '', media: val.media ? val.media.image.src : ''});
                // });
              }else if(dataObj.attachment_details.type === 'share'){
                dataObj.attachment_details.host_name = getHostName(dataObj.link) || '';
              }
            }
          }
          let checkData = {post_id: dataObj.post_id, user: userData.user};
          if( dataObj.story ){
            checkData.story = dataObj.story;
          }

          SocialData.findOne(checkData).exec(function (err, foundSocial) {
            if (err) console.log(err);
            if (!foundSocial) {
              SocialData.create(dataObj).exec(function (err, socialCreated) {
                if (err) console.log(err);
              });
            }
          });
        }catch (e){
          console.log(e);
          return callback();
        } finally {

        }
        return callback();

      }, function (err) {
        if (err) {
          console.log(err);
        } else {
          return backToController(200);
        }
      });
    } else {
      backToController(200);
    }
  },
  /**
   * Description
   * @method facebookPostExtractFromCron
   * @param {} facebookData
   * @param {} userData
   * @param {} backToController
   * @return
   */
  facebookPostExtractFromCron: function (facebookData, userData, backToController) {
    if (facebookData && facebookData.data && facebookData.data.length) {
      AsyncCall.eachOf(facebookData.data, function (feedItem, key, callback) {

        try {
          let dataObj = {};
          dataObj.post = feedItem.message || '';
          dataObj.source = feedItem.source || '';
          dataObj.story = feedItem.story || '';
          dataObj.post_id = feedItem.id || '';
          dataObj.type = feedItem.type || '';
          dataObj.media = feedItem.full_picture || '';
          dataObj.link = feedItem.link || '';
          dataObj.icon = feedItem.icon || '';
          dataObj.created = feedItem.created_time || new Date();
          dataObj.date_sort = feedItem.created_time ? moment(new Date(feedItem.created_time)).unix() : moment(new Date()).unix();
          dataObj.from = feedItem.from || null;
          dataObj.provider = 'facebook';
          dataObj.user = userData.user;
          if(feedItem.application){
            dataObj.application = feedItem.application.id || undefined;
          }
          if(feedItem.attachments && feedItem.attachments.data.length){
            dataObj.attachment_details = {type: feedItem.attachments.data[0].type};
            dataObj.attachment_details.title = feedItem.attachments.data[0].title || '';
            dataObj.attachment_details.url = feedItem.attachments.data[0].url || '';
            dataObj.attachment_details.description = feedItem.attachments.data[0].description || '';
            dataObj.attachment_details.media = feedItem.attachments.data[0].media ? feedItem.attachments.data[0].media.image.src : '';
            if( dataObj.attachment_details.type ){
              if( dataObj.attachment_details.type === 'album' || dataObj.attachment_details.type === 'new_album' ){
                dataObj.attachments = feedItem.attachments.data[0].subattachments.data.map(val=>{
                  return {type: val.type || '', url: val.url || '', media: val.media ? val.media.image.src : ''};
                });
              }else if(dataObj.attachment_details.type === 'share'){
                dataObj.attachment_details.host_name = getHostName(dataObj.link) || '';
              }
            }
          }
          let checkData = {post_id: dataObj.post_id, user: userData.user};
          if( dataObj.story ){
            checkData.story = dataObj.story;
          }

          SocialData.findOne(checkData).exec(function (err, foundSocial) {
            if (err) console.log(err);
            if (!foundSocial) {

              console.log('------------------------------------------ Seperator----------------------------------------------');
              console.log(feedItem);

              console.log('------------------------------------------ Seperator----------------------------------------------');
              console.log(dataObj);

              SocialData.create(dataObj).exec(function (err, socialCreated) {
                if (err) console.log(err);
                sails.sockets.broadcast('sochara_' + userData.user, 'refresh_social', socialCreated);
              });
            }
          });
        } catch (e){
          console.log(e);
          return callback();
        }finally {

        }
        return callback();

      }, function (err) {
        if (err) {
          console.log(err);
        } else {
          backToController();
        }
      });
    } else {
      backToController();
    }
  },
  /**
   * Description
   * @method twitterPostExtract
   * @param {} twitterData
   * @param {} userData
   * @param {} backToController
   * @return
   */
  twitterPostExtract: function (twitterData, userData, backToController) {
    if (twitterData && twitterData.length) {
      AsyncCall.forEachOf(twitterData, function (tweetItem, key, callback) {
        let dataObj = {};
        dataObj.from = {
          name: tweetItem.user.name,
          id: tweetItem.user.id,
          profile_photo: tweetItem.user.profile_image_url_https
        };
        dataObj.post = tweetItem.text || '';
        dataObj.post_id = tweetItem.id_str || '';
        dataObj.favorite = tweetItem.favorited;
        dataObj.media = tweetItem.entities.media ? tweetItem.entities.media[0].media_url_https : '';
        dataObj.place = tweetItem.place ? tweetItem.name : '';
        dataObj.favorite_count = tweetItem.favorite_count || 0;
        dataObj.source = tweetItem.source || '';
        dataObj.created = tweetItem.created_at || new Date();
        dataObj.date_sort = tweetItem.created_at ? moment(new Date(tweetItem.created_at)).unix() : moment(new Date()).unix();
        dataObj.user_name = userData.screenName || '';
        dataObj.provider = 'twitter';
        dataObj.user = userData.user;
        SocialData.findOne({post_id: dataObj.post_id, user: userData.user}).exec(function (err, foundSocial) {
          if (err) console.log(err);
          if (!foundSocial) {
            SocialData.create(dataObj).exec(function (err, socialCreated) {
              if (err) console.log(err);
              sails.sockets.broadcast('sochara_' + userData.user, 'refresh_social', socialCreated);
              return callback();
            });
          } else {
            return callback();
          }
        });
      }, function (err) {
        if (err) {
          console.log(err);
        } else {
          backToController();
        }
      });
    }
  },
  /**
   * Description
   * @method instagramPostExtract
   * @param {} instagramData
   * @param {} userData
   * @param {} backToController
   * @return
   */
  instagramPostExtract: function (instagramData, userData, backToController) {
    if (instagramData && instagramData.length) {
      AsyncCall.forEachOf(instagramData, function (instaItem, key, callback) {
        let dataObj = {};
        dataObj.from = {
          name: instaItem.user.full_name,
          id: instaItem.user.id,
          profile_photo: instaItem.user.profile_picture
        };
        dataObj.post = instaItem.caption ? instaItem.caption.text : '';
        dataObj.post_id = instaItem.id || '';
        dataObj.media = instaItem.images.standard_resolution.url || '';
        dataObj.place = instaItem.location ? instaItem.location.name : '';
        dataObj.like_count = instaItem.likes.count || 0;
        dataObj.tags = instaItem.tags || [];
        dataObj.comment_count = instaItem.comments.count || 0;
        dataObj.type = instaItem.type || '';
        dataObj.source = instaItem.source || '';
        dataObj.link = instaItem.link || '';
        // dataObj.created = instaItem.created_time ? new Date(parseInt(instaItem.created_time)) : new Date();
        dataObj.created = moment.unix(instaItem.created_time).format('YYYY-MM-DD HH:mm:ss');
        dataObj.date_sort = instaItem.created_time ? moment.unix(instaItem.created_time).unix() : moment(new Date()).unix();
        dataObj.provider = 'instagram';
        dataObj.user = userData.user;
        SocialData.findOne({post_id: dataObj.post_id, user: userData.user}).exec(function (err, foundSocial) {
          if (err) console.log(err);
          if (!foundSocial) {
            SocialData.create(dataObj).exec(function (err, socialCreated) {
              if (err) {
                console.log(err);
              } else {
                sails.sockets.broadcast('sochara_' + socialCreated.user, 'refresh_social', socialCreated);
                return callback();
              }
            });
          } else {
            return callback();
          }
        });
      }, function (err) {
        if (err) {
          console.log(err);
        } else {
          backToController();
        }
      });
    }
  },
  /**
   * Description
   * @method facebookPostExtractSinglePost
   * @param {} facebookData
   * @param {} userId
   * @param {} backToController
   * @return
   */
  facebookPostExtractSinglePost: function (facebookData, userId, backToController) {
    SocialAuth.findOne({user: userId, provider: 'facebook'}).exec(function (err, respAuthData) {
      let dataObj = {};
      FB.setAccessToken(respAuthData.accessToken);
      FB.api(facebookData.post_id + '?fields=message,story,type,link,picture,full_picture,description,icon,name,place,shares,from,created_time', function (response) {
        if (!response || response.error) {
          backToController('');
        }
        dataObj.post = response.message || '';
        dataObj.source = response.source || '';
        dataObj.story = response.story || '';
        dataObj.post_id = response.id || '';
        dataObj.type = response.type || '';
        dataObj.media = response.full_picture || '';
        dataObj.link = response.link || '';
        dataObj.icon = response.icon || '';
        dataObj.created = response.created_time || new Date();
        dataObj.date_sort = response.created_time ? moment(new Date(response.created_time)).unix() : moment(new Date()).unix();
        dataObj.from = response.from || null;
        dataObj.provider = 'facebook';
        dataObj.user = respAuthData.user;
        if(response.application){
          dataObj.application = response.application.id || undefined;
        }
        // SocialData.findOne({post_id: dataObj.post_id, user: respAuthData.user}).exec(function (err, foundSocial) {
        //   if (err) console.log(err);
        //   if (!foundSocial) {
        //     SocialData.create(dataObj).exec(function (err, socialCreated) {
        //       if (err) console.log(err);
        //       backToController(socialCreated);
        //     });
        //   } else {
        //     backToController(foundSocial);
        //   }
        // });
        backToController(dataObj);

      });
    });
  },
  /**
   * Description
   * @method twitterPostExtractSingle
   * @param {} tweetItem
   * @param {} userId
   * @param {} backToController
   * @return
   */
  twitterPostExtractSingle: function (tweetItem, userId, backToController) {
    SocialAuth.findOne({user: userId, provider: 'twitter'}).exec(function (err, respAuthData) {
      let dataObj = {};
      dataObj.from = {
        name: tweetItem.user.name,
        id: tweetItem.user.id,
        profile_photo: tweetItem.user.profile_image_url_https
      };
      dataObj.post = tweetItem.text || '';
      dataObj.post_id = tweetItem.id_str || '';
      dataObj.media = tweetItem.entities.media ? tweetItem.entities.media[0].media_url_https : '';
      dataObj.place = tweetItem.place ? tweetItem.name : '';
      dataObj.favorite_count = tweetItem.favorite_count || 0;
      dataObj.source = tweetItem.source || '';
      dataObj.created = tweetItem.created_at || new Date();
      dataObj.date_sort = tweetItem.created_at ? moment(new Date(tweetItem.created_at)).unix() : moment(new Date()).unix();
      dataObj.user_name = respAuthData.screenName || '';
      dataObj.provider = 'twitter';
      dataObj.user = respAuthData.user;
      dataObj.favorite = tweetItem.favorited;
      backToController(dataObj);
      // For Single Post
      // SocialData.findOne({post_id: dataObj.post_id, user: respAuthData.user}).exec(function (err, foundSocial) {
      //   if (err) console.log(err);
      //   if (!foundSocial) {
      //     SocialData.create(dataObj).exec(function (err, socialCreated) {
      //       if (err) console.log(err);
      //       backToController(socialCreated);
      //     });
      //   } else {
      //     backToController(foundSocial);
      //   }
      // });
    });
  }
};


/**
 * Description
 * @method getHostName
 * @param {} url
 * @return
 */
function getHostName(url) {
  var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
  if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
    return match[2];
  }
  else {
    return null;
  }
}
