/**
 * Created by ASUS on 17-May-17.
 */
module.exports = {
  /**
   * Description
   * @description Fetching Facebook Page data. Page with its posts containing images, message, type
   * @method fetchOther
   * @param pageType
   * @param accessToken
   * @param userId
   * @param response
   * @param next
   * @return 
   */
  fetchOther: function (pageType, accessToken, userId, response, next) {
    var fb = require('fb');
    var async = require('async');
    fb.setAccessToken(accessToken);
    if (response.data) {
      if (response.data.length > 0) {
        var returningArray = [];
        response.data.forEach(function (item) {
          returningArray.push(function (callback) {
            fb.api(item.id + '/posts?fields=message,type,full_picture,from,created_time', function (secondaryResponse) {
              if (secondaryResponse.error) {
                callback(null, secondaryResponse.error);
              }
              if (secondaryResponse.data) {
                secondaryResponse.data.forEach(function (obj) {
                  obj.fbApiType = pageType;
                });
                callback(null, secondaryResponse.data);
              }
            });
          });
        });
        async.parallel(returningArray, function (err, result) {
          if (err) {
            next([]);
          }
          if (result) {
            next(result);
          }
        });
      }
      else {
        next([]);
      }
    }
    else {
      next([]);
    }
  },
  /**
   * Description
   * @method fetchFeed
   * @param {} accessToken
   * @param {} userId
   * @param {} response
   * @param {} next
   * @return 
   */
  fetchFeed: function (accessToken, userId, response, next) {
    var fb = require('fb');
    var async = require('async');
    fb.setAccessToken(accessToken);
    if (response.data) {
      if (response.data.length > 0) {
        response.data.splice(0,3);
        var feedDataWithDetails = [];
        response.data.forEach(function (eachItem) {
          feedDataWithDetails.push(function (callabck) {
            fb.api(eachItem.id + '?fields=message,type,link,full_picture,from,created_time', function (response) {
              if (!response || response.error) {
                callabck(null, []);
              }
              response.fbApiType = 'feed';
              callabck(null, response);
            });
          });
        });
        async.parallel(feedDataWithDetails, function (err, result) {
          if (err) {
            next([]);
          }
          if (result) {
            next(result);
          }
        });
      }
    }
  }
};
