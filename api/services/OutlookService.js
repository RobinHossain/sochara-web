/**
 Help Links:
 https://github.com/google/google-api-nodejs-client
 http://stackoverflow.com/questions/24460422/how-to-send-a-message-successfully-using-the-new-gmail-rest-api
 http://stackoverflow.com/questions/30762369/gmail-api-playground-send-method-converted-mime-raw-header-not-populating-emai
 */
const outlook = require('nodejs-outlook');
const moment = require('moment');

let credentials = {
  client: {
    id: '55a857a7-806b-4f56-9c0a-c086a8c9ab95',
    secret: 'ybNEFBRxvwyYV2MzFGvEpkX',
  },
  auth: {
    tokenHost: 'https://login.microsoftonline.com',
    authorizePath: 'common/oauth2/v2.0/authorize',
    tokenPath: 'common/oauth2/v2.0/token'
  }
};
const outlookOauth2 = require('simple-oauth2').create(credentials);

module.exports = {


  /**
   * Description
   * @return
   * @method sendEmail
   * @param access_token
   * @param refresh_token
   * @param messageData
   * @param req
   * @param res
   * @param next
   * @return 
   */
  sendEmail: function (access_token, refresh_token, messageData, req, res, next) {
    var user = req.session.User.id;
    if (access_token && refresh_token) {
      req.getOutlookEmailSend(access_token, refresh_token, messageData, function (resp) {
        res.send(resp);
      });
    }
  },

  /**
   * Description
   * @return
   * @method getMailSubFolders
   * @param {} req
   * @param {} next
   * @return 
   */
  getMailSubFolders: function (req, next) {
    'use strict';
    let user = req.session.User.id;
    let account = req.param('account');
    let folder = req.param('folder');

    EmailAccount.findOne({id: account.id, user: user, provider: 'outlook'}, function froundCB(err, record) {
      if (record && record.access_token && record.refresh_token) {
        req.getMailSubFolders(record.access_token, record.email, folder, function (resp) {
          next(resp);
        });
      }
    });
  },

  /**
   * Description
   * @method getMessages
   * @param {} emailAccount
   * @param {} mailBox
   * @param {} top
   * @param {} skip
   * @param {} filter
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  getMessages: function (emailAccount, mailBox, top, skip, filter, req, res, next) {
    refreshAccessToken(emailAccount.refresh_token, function (error, newToken) {
      if (error) {
        console.log(error);
      }
      if (newToken) {
        EmailAccount.update({
          id: emailAccount.id,
          user: emailAccount.user,
          provider: 'outlook'
        },{
          access_token: newToken.token.access_token,
          refresh_token: newToken.token.refresh_token,
          id_token: newToken.token.id_token,
          expires_at: newToken.token.expires_at
        }).exec(function afterwards(err, updated) {
          if (err) console.log(err);
          // console.log('updated');
        });

        getOutlookEmails(newToken.token.access_token, emailAccount.email, mailBox, top, skip, filter, function (resp) {
          if (resp) {
            next(resp);
          } else {
            next('No Message Found');
          }
        });
      }
    });
  }
};


/**
 * Description
 * @method getOutlookEmails
 * @param {} token
 * @param {} email
 * @param {} folder
 * @param {} top
 * @param {} skip
 * @param {} filter
 * @param {} callback
 * @return 
 */
function getOutlookEmails(token, email, folder, top, skip, filter, callback) {

  var queryParams = {
    // '$select': 'Subject,ReceivedDateTime,From,IsRead',
    '$orderby': 'ReceivedDateTime desc',
    '$top': top,
    '$skip': skip,
    '$count': 'true'
  };

  if (filter) {
    queryParams.$filter = filter;
  }

  // Set the API endpoint to use the v2.0 endpoint
  outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
  // Set the anchor mailbox to the user's SMTP address
  outlook.base.setAnchorMailbox(email);

  outlook.mail.getMessages({token: token, folderId: folder, odataParams: queryParams},
    function (error, result) {
      if (error) {
        console.log('getMessages returned an error: ' + error);
      }
      else if (result) {
        callback(result);
      }
    });
}

/**
 * Description
 * @method refreshAccessToken
 * @param {} refreshToken
 * @param {} callback
 * @return 
 */
function refreshAccessToken(refreshToken, callback) {
  var tokenObj = outlookOauth2.accessToken.create({refresh_token: refreshToken});
  tokenObj.refresh(callback);
}
