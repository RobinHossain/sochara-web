/**
 * DashboardController
 *
 * @description :: Server-side logic for managing Dashboard content
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
const fs = require("fs");
var FB = require('fb');
var async = require('async');
var Twitter = require('twitter');
var MetaInspector = require('node-metainspector');
var path = require('path');
var moment = require('moment');

var facebookConfigure = {id: '1589011794492434', secret: '4bd36c109d167cb1b161e76a1a68f81d'};
var instagramApi = require('instagram-node').instagram();

instagramApi.use({
  client_id: '0ab9e25c89494bc4ae8b8743e34b0ca7',
  client_secret: '7f99e08f3b8f46adb3e4146eb5e026db '
});


module.exports = {
  /**
   * Description
   * @description Fetching all posts from Facebook, Twitter, Instagram of a user
   * @link Facebook : developer.facebook.com
   * @link Twitter : apps.twitter.com
   * @method fetchAll
   * @param req
   * @param res
   * @return
   */
  fetchAll: function (req, res) {

    let loggedInUserId = req.session.User.id;

    SocialAuth.find({user: loggedInUserId}).exec(function (err, respSocialAccounts) {
      let socialData = [];
      if (respSocialAccounts && respSocialAccounts.length) {
        async.eachOf(respSocialAccounts, function (respSocialAccount, key, accountCallback) {
          if (respSocialAccount.provider === 'facebook') {
            SocialData.find({
              user: loggedInUserId,
              provider: 'facebook'
            }).sort({createdAt: -1}).limit(50).exec(function (err, resp) {
              socialData.push(...resp);
              return accountCallback();
            });
          } else if (respSocialAccount.provider === 'twitter') {
            SocialData.find({
              user: loggedInUserId,
              provider: 'twitter'
            }).sort({createdAt: -1}).limit(50).exec(function (err, resp) {
              socialData.push(...resp);
              return accountCallback();
            });

          } else if (respSocialAccount.provider === 'instagram') {
            SocialData.find({
              user: loggedInUserId,
              provider: 'instagram'
            }).sort({createdAt: -1}).limit(50).exec(function (err, resp) {
              socialData.push(...resp);
              return accountCallback();
            });
          }
        }, function (err) {
          if (err) {
            console.log(err);
          } else {
            res.send(socialData);
          }
        });
      } else {
        res.send({});
      }
    });
  },

  /**
   * Description
   * @method joinSocket
   * @param {} req
   * @param {} res
   * @return
   */
  joinSocket: function (req, res) {
    let user = req.session.User ? req.session.User.id : '';
    if (!req.isSocket && !user) {
      return res.badRequest();
    }
    let roomName = 'sochara_' + user;
    try {
      sails.sockets.join(req, roomName, function (err) {
        if (err) console.log(err);
      });
    } catch (e) {
      console.log(e);
    }
    res.send(200);
  },

  /**
   * Description
   * @method socketTestOne
   * @param {} req
   * @param {} res
   * @return CallExpression
   */
  socketTestOne: function (req, res) {
    let user = req.session.User.id;
    sails.sockets.broadcast('sochara_' + user, 'new_email', {data: 'hello'});
    return res.json({
      anyData: 'we want to send back'
    });
  },

  /**
   * Description
   * @method getNotificationCount
   * @param {} req
   * @param {} res
   * @return
   */
  getNotificationCount: function (req, res) {
    let userId = req.session.User.id;
    let count = {mail: 0, chat: 0, calendar: 0, contacts: 0};
    let data;
    async.parallel([
        function (callback) {
          Contacts.count({
            is_sochara_user: true,
            sochara_user_id: userId,
            status: 'pending'
          }).exec(function (err, requestCount) {
            if (err) console.log(err);
            count.contacts = requestCount;
            callback();
          });
        },
        function (callback) {
          Chat.find({users: userId, select: ['unread']}).exec(function (err, conversations) {
            if (err) console.log(err);
            conversations.forEach(function (conversation) {
              if (conversation.unread[userId]) count.chat++;
            });
            callback();
          });
        },
        function (callback) {
          data = {userId: userId};
          MailService.getGmailInboxCount(req, data, function (respInboxData) {
            if (respInboxData && typeof respInboxData === 'object' && Object.keys(respInboxData).length) {
              count.mail = count.mail + respInboxData.threadsUnread;
            }
            callback();
          });
        },
        function (callback) {
          data = {userId: userId};
          MailService.getOutlookInboxCount(req, data, function (respInboxData) {
            if (Object.keys(respInboxData).length) {
              count.mail = count.mail + respInboxData.UnreadItemCount;
            }
            callback();
          });
        }
      ],
      function (err, results) {
        if (err) console.log(err);
        res.send(count);
      });
  },

  /**
   * Description
   * @method getCalendarEvents
   * @param {} req
   * @param {} res
   * @return
   */
  getCalendarEvents: function (req, res) {
    let user = req.session.User.id;
    let begin = moment(moment().format("YYYY-MM-DD")).toISOString();
    CalendarEvent.find({
      attendees: {$elemMatch: {id: user}},
      start: {'>=': begin},
      select: ['_id', 'start', 'end', 'title', 'color', 'organizer', 'type', 'attendees', 'allDay']
    }).limit(10).exec(function (err, respEvents) {
      if (err) console.log(err);
      let events = [];
      respEvents.forEach(function (respEvent) {
        let event = respEvent;
        event.stick = true;
        event.attendee = respEvent.attendees.filter(attendee => attendee.id === user)[0];
        events.push(event);
      });
      res.send(events);
    });
  },

  /**
   * Description
   * @description Sending posts to Social sites registered by user
   * @method getSocialAccount
   * @param req
   * @param res
   * @return
   */
  getSocialAccount: function (req, res) {
    let user = req.session.User.id;
    SocialAuth.find({user: user}).exec(function (err, respAcc) {
      if (err) console.log(err);
      res.send(respAcc);
    });
  },
  /**
   * Description
   * @method submitSocharaOwnPost
   * @param {} req
   * @param {} res
   * @return
   */
  submitSocharaOwnPost: function (req, res) {
    var data = {};
    data.user = req.session.User.id;
    data.post = req.param('post');
    data.photos = req.param('photos');
    data.og = req.param('og');

    SocharaOwnPosts.create(data).exec(function (err, savedData) {
      if (err) {
        res.send({err: 'failed'});
      }
      if (savedData) {
        res.send({msg: {id: savedData.id}});
      }
    });
  },

  /**
   * Description
   * @method uploadMedia
   * @param {} req
   * @param {} res
   * @return
   */
  uploadMedia: function (req, res) {
    var LoggedInUserid = req.session.User.id;
    var random_date = new Date();
    var randNum = random_date.getTime();

    req.file('file').upload({
      dirname: path.resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserid + '/social',
      /**
       * Description
       * @return
       * @method saveAs
       * @param {} __newFileStream
       * @param {} cb
       * @return
       */
      saveAs: function (__newFileStream, cb) {
        var fname = __newFileStream.filename;
        var mName = fname.substr(0, fname.lastIndexOf('.'));
        var fName = mName.replace(/\s\s+/g, ' ');
        fName = fName.replace(/ /g, "-");
        var ext = path.extname(__newFileStream.filename);
        cb(null, fName + "_" + randNum + ext);
      },
      // don't allow the total upload size to exceed ~10MB
      maxBytes: 900000000
    }, function whenDone(err, uploadedFiles) {


      if (err) {
        return res.negotiate(err);
      }

      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0) {
        return res.badRequest('No file was uploaded');
      } else {
        var fileType = uploadedFiles[0].type;
        var fileExtName = fileType.substr(0, fileType.lastIndexOf('/'));

        if (fileExtName === 'image' || fileExtName === 'video') {
          var fileNameWithExt = path.basename(uploadedFiles[0].fd);
          var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));

          var baseURL = Utility.getSiteBaseURL(req);
          var file_dir = '/uploads/files/' + LoggedInUserid + '/social/' + path.basename(uploadedFiles[0].fd);
        }

        var files = {};
        files.name = uploadedFiles[0].filename;
        files.type = 'social';
        if (fileExtName === 'video') {
          files.file_type = 'video';
        }
        files.ext = path.extname(uploadedFiles[0].filename);
        files.owner = LoggedInUserid;
        files.file_dir = file_dir;
        files.fileFD = uploadedFiles[0].fd;
        files.size = uploadedFiles[0].size;
        UserFiles.create(files, function fileCreated(err, file) {
          if (err) {
            console.log(err);
            res.send(404);
          } else {
            setTimeout(function () {
              return res.json(file);
            });
          }
        });
      }
    });
  },

  /**
   * Description
   * @method makeFavorite
   * @param {} req
   * @param {} res
   * @return
   */
  makeFavorite: function (req, res) {
    let user = req.session.User.id;
    let id = req.param('id');
    SocialData.findOne({id: id, user: user}).exec(function (err, respData) {
      if (err) {
        console.log(err);
      }
      SocialAuth.findOne({user: user, provider: respData.provider}).exec(function (err, authData) {
        if (authData && authData.provider === 'twitter') {
          let client = new Twitter({
            consumer_key: '2ourwWtAYQBvpUAn150SXe9oY',
            consumer_secret: 'ypZfC5xdrEyqVTycFvMrV80F7asJnUio5Bswp60PyOzQF4cfPg',
            access_token_key: authData.accessToken,
            access_token_secret: authData.accessTokenSecret
          });
          let postUrl = respData.favorite ? "favorites/destroy" : "favorites/create";
          client.post(postUrl, {id: respData.post_id}, function (error, tweet, response) {
            if (error) console.log(error);
            if (response.statusCode === 200) {
              SocialData.update({id: id}, {favorite: !respData.favorite}).exec(function (err) {
                if (err) console.log(err);
                res.send({success: true});
              });
            }
          });

        } else {
          res.send(200);
        }
      });
    });

  },

  /**
   * Description
   * @method removePost
   * @param {} req
   * @param {} res
   * @return
   */
  removePost: function (req, res) {
    let user = req.session.User.id;
    let id = req.param('id');
    SocialData.findOne({id: id, user: user}).exec(function (err, respData) {
      if (err) {
        console.log(err);
      }
      SocialAuth.findOne({user: user, provider: respData.provider}).exec(function (err, authData) {
        if (authData && authData.provider === 'facebook') {
          FB.setAccessToken(authData.accessToken);
          FB.api("/" + respData.post_id, 'delete', function (response) {
            if (response) {
              SocialData.destroy({id: id}).exec(function (err) {
                if (err) console.log(err);
                res.send(response);
              });
            }
          });
        } else if (authData && authData.provider === 'twitter') {
          let client = new Twitter({
            consumer_key: '2ourwWtAYQBvpUAn150SXe9oY',
            consumer_secret: 'ypZfC5xdrEyqVTycFvMrV80F7asJnUio5Bswp60PyOzQF4cfPg',
            access_token_key: authData.accessToken,
            access_token_secret: authData.accessTokenSecret
          });
          let postUrl = "statuses/destroy/" + respData.post_id;
          client.post(postUrl, function (error, tweet, response) {
            if (error) console.log(error);
            SocialData.destroy({id: id}).exec(function (err) {
              if (err) console.log(err);
              res.send({success: true});
            });
          });

        } else if (authData && authData.provider === 'instagram') {
          SocialData.destroy({id: id}).exec(function (err) {
            if (err) console.log(err);
            res.send({success: true});
          });
        } else {
          res.send(200);
        }
      });
    });
  },

  /**
   * Description
   * @method editPost
   * @param {} req
   * @param {} res
   * @return
   */
  editPost: function (req, res) {
    let user = req.session.User.id;
    let id = req.param('id');
    let message = req.param('message');
    SocialData.findOne({id: id, user: user}).exec(function (err, respData) {
      if (err) {
        console.log(err);
      }
      if (respData && respData.provider === 'facebook') {
        SocialAuth.findOne({user: user, provider: 'facebook'}).exec(function (err, authData) {
          if (authData) {
            FB.setAccessToken(authData.accessToken);
            FB.api("/" + respData.post_id, 'post', {message: message}, function (response) {
              if (response && !response.error) {
                SocialData.update({id: id}, {post: message}).exec(function (err, updatedPost) {
                  if (err) console.log(err);
                  res.send(response);
                });
              } else {
                res.send(response);
              }
            });
          } else {
            res.send(200);
          }
        });
      }
    });
  },

  /**
   * Description
   * @method submitSocialPost
   * @param {} req
   * @param {} res
   * @return
   */
  submitSocialPost: function (req, res) {
    let data = {};
    data.user = req.session.User.id;
    data.post = req.param('post');
    data.og = req.param('og') || '';
    let media = req.param('media') && req.param('media').length ? req.param('media') : [];
    let apiTypes = req.param('apiTypes');
    getUploadedFiles(media, function (uploadedFiles) {
      let respData = {media: media, socialBack: []};
      postToIndividualProvider(data, uploadedFiles, apiTypes, function (postedData) {
        res.send(respData);
      });
    });
  },

  /**
   * Description
   * @method mediaPostToFacebook
   * @param {} req
   * @param {} res
   * @return
   */
  mediaPostToFacebook: function (req, res) {
    let photoId = req.param('id');
    let user = req.session.User.id;
    SocialAuth.findOne({user: user, provider: 'facebook'}).exec(function (err, authData) {
      getUploadedFiles(photoId, function (respMedia) {
        let uploadedFile = respMedia[0];
        getFacebookAlbumId({user: user}, function (albumId) {
          try {
            FB.setAccessToken(authData.accessToken);
            FB.api('/' + albumId + '/photos', 'post', {
              source: fs.createReadStream(uploadedFile),
              caption: ""
            }, function (res) {
              if (!res || res.error) {
                console.log(!res ? 'error occurred' : res.error);
              }
            });
          } catch (e) {
            console.log(e);
          }
          res.send(200);
        });
      });
    });
  },

  /**
   * Description
   * @method albumPostTwitter
   * @param {} req
   * @param {} res
   * @return
   */
  albumPostTwitter: function (req, res) {
    let user = req.session.User.id;
    let album = {id: req.param('id'), name: req.param('name')};
    let media;
    UserFiles.find({
      type: 'media',
      album: album.id,
      fileThumb: {$exists: true, $ne: null},
      select: ['fileFD']
    }).exec(function (err, respIds) {
      media = respIds.map(media => media.fileFD);
      if (media.length > 4) {
        media = media.slice(0, 4);
      }
      twiterMediaUpload({user: user, media: media}, function (respMediaUpload) {
        twitterTweet({user: user, media: respMediaUpload, post: album.name}, function (respTweet) {
          res.send(respTweet);
        });
      });
    });
  },

  /**
   * Description
   * @method mediaPostTwitter
   * @param {} req
   * @param {} res
   * @return
   */
  mediaPostTwitter: function (req, res) {
    let user = req.session.User.id;
    let photoId = req.param('id');
    let media;
    UserFiles.find({
      id: photoId,
      type: 'media',
      fileThumb: {$exists: true, $ne: null},
      select: ['fileFD']
    }).exec(function (err, respIds) {
      media = respIds.map(media => media.fileFD);
      twiterMediaUpload({user: user, media: media}, function (respMediaUpload) {
        twitterTweet({user: user, media: respMediaUpload}, function (respTweet) {
          res.send(respTweet);
        });
      });
    });
  },

  /**
   * Description
   * @method albumPostToFacebook
   * @param {} req
   * @param {} res
   * @return
   */
  albumPostToFacebook: function (req, res) {
    let albumId = req.param('id');
    let user = req.session.User.id;
    MediaAlbum.findOne({id: albumId}).exec(function (err, respMedia) {
      if (err) console.log(err);
      if (respMedia) {
        SocialAuth.findOne({user: user, provider: 'facebook'}).exec(function (err, authData) {
          if (err) console.log(err);
          if (authData) {
            UserFiles.find({
              type: 'media',
              album: respMedia.id,
              fileThumb: {$exists: true, $ne: null},
              select: ['fileFD']
            }).exec(function (err, respIds) {
              if (err) console.log(err);
              let uploadedFiles = respIds.map(media => media.fileFD);
              createFacebookAlbum({name: respMedia.name, accessToken: authData.accessToken}, function (albumId) {
                async.eachSeries(uploadedFiles, function (uploadedFile, callbackFiles) {
                  try {
                    FB.setAccessToken(authData.accessToken);
                    FB.api('/' + albumId + '/photos', 'post', {source: fs.createReadStream(uploadedFile)}, function (res) {
                      if (!res || res.error) {
                        console.log(!res ? 'error occurred' : res.error);
                      }
                    });
                  } catch (e) {
                    console.log(e);
                  }
                  callbackFiles();
                }, function (err) {
                  if (err) console.log(err);
                  res.send({message: "Your media album is posted to Facebook;"});
                });
              });
            });
          } else {
            res.send({message: "You didn't intigrate any facebook account."});
          }
        });
      } else {
        res.send({message: 'The album not found.'});
      }
    });
  },

  /**
   * Description
   * @description Removing Posts from Sites
   * @method rmSocharaPost
   * @param req
   * @param res
   * @return
   */
  rmSocharaPost: function (req, res) {
    var id = req.param('id');

    SocharaOwnPosts.destroy({id: id}).exec(function (err) {
      if (err) {
        return res.serverError(err);
      }
      res.send({msg: 'success'});
    });
  },

  /**
   * User pushes site url and this module retrieves available meta data , i.e.- Site Name,
   * URL, Image, Description, Author, etc. from a vaild URL.
   * @description OG (open graph) works for fetching site meta data.
   * @link www.npm.com/node-metainspector
   * @link www.npm.com/open-graph
   * @method og
   * @param req
   * @param res
   * @return
   */
  og: function (req, res) {
    var url = req.param('url');

    var client = new MetaInspector(url, {timeout: 5000});

    //this On event handler runs after completing .fetch() function runs properly
    client.on("fetch", function () {
      var data = {};
      data.url = client.url;
      data.title = client.title;
      data.ogTitle = client.ogTitle;
      data.description = client.description;
      data.ogDescription = client.ogDescription;
      data.images = client.image;
      res.send({msg: data});
    });

    client.on("error", function (err) {
      return res.serverError(err);

    });

    client.fetch();
  },

  /**
   * Description
   * @description Updating existed posts from Different sites
   * @method updateSocharaOwnPost
   * @param req
   * @param res
   * @return
   */
  updateSocharaOwnPost: function (req, res) {
    var postId = req.param('id');
    SocharaOwnPosts.update({id: postId}, req.param('data')).exec(function (err, updatedData) {
      if (err) {
        return res.serverError(err);
      }
      res.send({msg: 'success'});
    });
  },

  /**
   * facebook, instagram of a user
   * @description Checking Availability of Oauth Scope for post to Social sites i.e. Twitter,
   * @method checkPostingOauth
   * @param req
   * @param res
   * @return
   */
  checkPostingOauth: function (req, res) {
    var loggedInUserId = req.session.User.id;
  },

  /**
   * Description
   * @method deletePostedMedia
   * @param {} req
   * @param {} res
   * @return
   */
  deletePostedMedia: function (req, res) {
    let user = req.session.User.id;
    UserFiles.find({id: req.param('files')}).exec(function (err, getFiles) {
      if (getFiles && getFiles.length) {
        async.forEachOf(getFiles, function (getFile, index, callback) {
          fs.unlink(getFile.fileFD, function (e) {
            if (e) {
              callback(e);
            } else {
              UserFiles.destroy({id: getFile.id}, function destroyedCB(err, record) {
                callback(record);
              });
              // callback(getFile.fileFD);
            }
          });

        }, function (err) {
          if (err) {
            console.log(err);
            res.send(200);
          } else {
            res.send(200);
          }
        });
      }
    });
  },
  /**
   * Description
   * @method fetchFbVideoDetails
   * @param {} req
   * @param {} res
   * @return
   */
  fetchFbVideoDetails: function (req, res) {
    let videoId = req.param('id');
    let loggedInUserId = req.session.User.id;
    SocialAuth.findOne({user: loggedInUserId, provider: 'facebook'}).exec(function (err, authData) {
      if (authData) {
        FB.setAccessToken(authData.accessToken);
        FB.api("/" + videoId + "?fields=source", function (response) {
          if (response && !response.error) {
            res.send(response);
          }
        });
      } else {
        res.send(200);
      }
    });
    /* make the API call */
  }
};

/**
 * Description
 * @method getUploadedFiles
 * @param {} media
 * @param {} callback
 * @return
 */
function getUploadedFiles(media, callback) {
  UserFiles.find({id: media}).exec(function (err, respData) {
    if (respData && respData.length) {
      let mediaData = [];
      respData.forEach(function (respVal) {
        mediaData.push(respVal.fileFD);
      });
      callback(mediaData);
    } else {
      callback([]);
    }
  });
}

/**
 * Description
 * @method postToIndividualProvider
 * @param {} data
 * @param {} uploadedFiles
 * @param {} apiTypes
 * @param {} postCallback
 * @return
 */
function postToIndividualProvider(data, uploadedFiles, apiTypes, postCallback) {
  async.parallel([
      function (callback) {
        try {
          if (apiTypes.includes('facebook')) {
            postToFacebookWithAttachment(data, uploadedFiles, function (uploadedData) {

            })
          }
        } catch (err) {
          console.log(err);
        }
        callback(null, 1);
      },
      function (callback) {
        try {
          if (apiTypes.includes('twitter')) {
            postToTwitterWithAttachment(data, uploadedFiles, function (uploadedData) {

            });
          }
        } catch (err) {
          console.log(err);
        }
        callback(null, 1);
      }
    ],
    function (err, results) {
      postCallback(results);
    });
}

/**
 * Description
 * @method postToFacebookWithAttachment
 * @param {} data
 * @param {} uploadedFiles
 * @param {} facebookCallback
 * @return
 */
function postToFacebookWithAttachment(data, uploadedFiles, facebookCallback) {
  getFacebookAlbumId(data, function (albumId) {
    SocialAuth.findOne({user: data.user, provider: 'facebook'}).exec(function (err, authData) {
      if (uploadedFiles && uploadedFiles.length) {
        albumId = authData.album_id;
        if (albumId) {
          async.eachSeries(uploadedFiles, function (uploadedFile, callbackFiles) {
            try {
              FB.setAccessToken(authData.accessToken);
              FB.api('/' + albumId + '/photos', 'post', {
                source: fs.createReadStream(uploadedFile),
                caption: data.post
              }, function (res) {
                if (!res || res.error) {
                  console.log(!res ? 'error occurred' : res.error);
                }
              });
            } catch (e) {
              console.log(e);
            }
            callbackFiles();
          }, function (err) {
            if (err) console.log(err);
            facebookCallback({status: 1});
          });
        } else {
          facebookCallback({status: 1});
        }

      } else {
        try {
          postToFacebook(data, authData, function (respFromFacebook) {

          });
        } catch (e) {
          console.log(e);
        }
        facebookCallback({status: 1});
      }
    });
  });

}

/**
 * Description
 * @method postToTwitterWithAttachment
 * @param {} data
 * @param {} uploadedFiles
 * @param {} twitterCallback
 * @return
 */
function postToTwitterWithAttachment(data, uploadedFiles, twitterCallback) {
  if (uploadedFiles && uploadedFiles.length) {
    let uploadedMediaIds = [];
    async.eachSeries(uploadedFiles, function (uploadedFile, callbackFiles) {
      try {
        postToTwitter(data, uploadedFile, function (respFromTwitter) {

        });
      } catch (e) {
        console.log(e);
      }
      callbackFiles();
    }, function (err) {
      if (err) console.log(err);
      twitterCallback();
    });
  } else {
    try {
      postToTwitter(data, '', function (respFromTwitter) {
      });
    } catch (e) {
      console.log(e);
    }
    twitterCallback();
  }
}

/**
 * Description
 * @method postToFacebook
 * @param {} data
 * @param {} authData
 * @param {} facebookCallback
 * @return
 */
function postToFacebook(data, authData, facebookCallback) {
  if (authData) {
    refreshAccessToken({accessToken: authData.accessToken, id: authData.id}, function (credentials) {

      data.accessToken = credentials.token;
      FB.setAccessToken(data.accessToken);
      let postData = {};
      if (data.attached_media && data.attached_media.length) {
        postData.attached_media = data.attached_media;
      }

      postData.message = data.post;
      let findUrl = urlify(data.post);
      let url = findUrl ? findUrl[0] : false;
      if (url) {
        postData.link = url;
      }

      FB.api('me/feed', 'post', postData, function (res) {
        if (!res || res.error) {
          console.log(!res ? 'error occurred' : res.error);
        }
        facebookCallback(res);
      });
    });
  } else {
    facebookCallback({});
  }
}

/**
 * Description
 * @method twiterMediaUpload
 * @param {} reqData
 * @param {} twitterCallback
 * @return
 */
function twiterMediaUpload(reqData, twitterCallback) {
  SocialAuth.findOne({user: reqData.user, provider: 'twitter'}).exec(function (err, authData) {
    var twitterClient = new Twitter({
      consumer_key: '2ourwWtAYQBvpUAn150SXe9oY',
      consumer_secret: 'ypZfC5xdrEyqVTycFvMrV80F7asJnUio5Bswp60PyOzQF4cfPg',
      access_token_key: authData.accessToken,
      access_token_secret: authData.accessTokenSecret
    });
    let uploadedMedia = [];
    async.eachSeries(reqData.media, function (uploadedFile, callbackFiles) {
      twitterClient.post('media/upload', {media: require('fs').readFileSync(uploadedFile)}, function (error, media) {
        if (error) console.log(error);
        uploadedMedia.push(media.media_id_string);
        callbackFiles();
      });
    }, function (err, results) {
      if (err) console.log(err);
      twitterCallback(uploadedMedia);
    });
  });
}

/**
 * Description
 * @method twitterTweet
 * @param {} reqData
 * @param {} tweetCallback
 * @return
 */
function twitterTweet(reqData, tweetCallback) {
  SocialAuth.findOne({user: reqData.user, provider: 'twitter'}).exec(function (err, authData) {
    if (err) console.log(err);
    if (authData) {
      var twitterClient = new Twitter({
        consumer_key: '2ourwWtAYQBvpUAn150SXe9oY',
        consumer_secret: 'ypZfC5xdrEyqVTycFvMrV80F7asJnUio5Bswp60PyOzQF4cfPg',
        access_token_key: authData.accessToken,
        access_token_secret: authData.accessTokenSecret
      });

      twitterClient.post('statuses/update', {
        status: reqData.post || '',
        media_ids: reqData.media.join(',')
      }, function (error, tweet, response) {
        if (error) console.log(error);
        tweetCallback(tweet);
      });
    }
  });
}

/**
 * Description
 * @method postToTwitter
 * @param {} data
 * @param {} media
 * @param {} twitterCallback
 * @return
 */
function postToTwitter(data, media, twitterCallback) {
  SocialAuth.findOne({user: data.user, provider: 'twitter'}).exec(function (err, authData) {
    if (err) {
      twitterCallback(err);
    }
    if (authData) {
      var client = new Twitter({
        consumer_key: '2ourwWtAYQBvpUAn150SXe9oY',
        consumer_secret: 'ypZfC5xdrEyqVTycFvMrV80F7asJnUio5Bswp60PyOzQF4cfPg',
        access_token_key: authData.accessToken,
        access_token_secret: authData.accessTokenSecret
      });

      var status = {
        status: data.post
      };

      if (media && media.length > 0) {
        // Load your image
        var mediaData = require('fs').readFileSync(media);

        // Make post request on media endpoint. Pass file data as media parameter.
        client.post('media/upload', {media: mediaData}, function (error, media, response) {

          if (!error) {
            // If successful, a media object will be returned.

            // Lets tweet it
            var status = {
              status: data.post,
              media_ids: media.media_id_string // Pass the media id string
            };
            client.post('statuses/update', status, function (error, tweet, response) {
              if (error) {
                twitterCallback({err: error});
              } else {
                twitterCallback(tweet);
              }
            });

          }
        });
      } else {
        client.post('statuses/update', status, function (error, tweet, response) {
          if (error) {
            twitterCallback({err: error});
          } else {
            twitterCallback(tweet);
          }
        });
      }
    } else {
      twitterCallback('');
    }
  });
}

/**
 * Description
 * @method getFacebookAlbumId
 * @param {} userData
 * @param {} albumCallback
 * @return
 */
function getFacebookAlbumId(userData, albumCallback) {
  SocialAuth.findOne({user: userData.user, provider: 'facebook'}).exec(function (err, authData) {
    if (authData && authData.album_id) {
      albumCallback(authData.album_id);
    } else {
      let getAlbumInfo;
      async.parallel([
          function (callback) {
            FB.setAccessToken(authData.accessToken);
            FB.api('me/albums', 'get', function (res) {
              if (!res || res.error) {
                console.log(!res ? 'error occurred' : res.error);
              }
              getAlbumInfo = res && res.data ? res.data.filter(album => album.name === 'Timeline Photos')[0] : {};
              if (getAlbumInfo) {
                SocialAuth.update({id: authData.id}, {album_id: getAlbumInfo.id || ''}).exec(function (err, respUpdate) {
                  callback(null, respUpdate[0].album_id);
                });
              }
            });
          }
        ],
        function (err, results) {
          albumCallback(results[0]);
        });
    }
  });
}

/**
 * Description
 * @method refreshAccessToken
 * @param {} credential
 * @param {} callback
 * @return
 */
function refreshAccessToken(credential, callback) {
  FB.api('oauth/access_token', {
    grant_type: 'fb_exchange_token',
    client_id: facebookConfigure.id,
    client_secret: facebookConfigure.secret,
    fb_exchange_token: credential.accessToken
  }, function (resp) {
    if (!resp || resp.error) {
      console.log(!resp ? 'error occurred' : resp.error);
      callback('');
      return;
    }
    SocialAuth.update({id: credential.id}, {accessToken: resp.access_token}).exec(function (err, respUser) {
      if (respUser && respUser[0]) {
        callback({token: respUser[0].accessToken});
      }
    });
  });
}

/**
 * Description
 * @method urlify
 * @param {} text
 * @return CallExpression
 */
function urlify(text) {
  return text.match(/(https?:\/\/[^\s]+)/g);
}

/**
 * Description
 * @method createFacebookAlbum
 * @param {} mediaData
 * @param {} next
 * @return
 */
function createFacebookAlbum(mediaData, next) {
  FB.setAccessToken(mediaData.accessToken);
  FB.api('me/albums', 'POST', {name: mediaData.name}, function (res) {
    if (!res || res.error) {
      console.log(!res ? 'error occurred' : res.error);
    } else {
      next(res.id);
    }
  });
}
