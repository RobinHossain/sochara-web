var moment = require('moment');
var Jimp = require("jimp");
var fs = require('fs');
var path = require('path');

module.exports = {

  /**
   * Save poll information
   * @return
   * @method save
   * @param req
   * @param res
   * @return 
   */
  save: function (req, res) {
    var userId = req.session.User.id;
    var pollData = req.allParams();
    pollData.user = userId;
    var id = pollData.id || '';
    var expireDate = moment(pollData.expireDate).format('YYYY-MM-DD');
    var eventDate = moment(pollData.eventDate).format('YYYY-MM-DD');
    var expireTime = moment(pollData.expireTime).format('h:mm A');
    var eventTime = moment(pollData.eventTime).format('h:mm A');
    pollData.expire_on = new Date(expireDate + ' ' + expireTime);
    pollData.event_date = new Date(eventDate + ' ' + eventTime);
    delete pollData.id;
    pollData.tvote = 0;

    // ReGenerate Poll Data
    var allAns = req.param('answers'), answers = [];
    var totalAnswer = parseInt(allAns.length);

    for (var inc = 0; inc < (totalAnswer); inc += 1) {
      var aitem = {};
      if (allAns[inc]['txt'] && allAns[inc]['txt'].length > 1) {
        aitem.id = inc + 1;
        if (allAns[inc]['txt'] && allAns[inc]['txt'].length > 1) {
          aitem.txt = allAns[inc]['txt'];
        }
        if (allAns[inc]['img'] && allAns[inc]['img'].length > 1) {
          aitem.img = allAns[inc]['img'];
        }
        if (allAns[inc]['type'] && allAns[inc]['type'].length > 1) {
          aitem.type = allAns[inc]['type'];
        }
        aitem.vote = 0;
        answers.push(aitem);
      }
    }

    pollData.answers = answers;

    if (pollData.draft && id) {
      Poll.update({id: id}, pollData).exec(function afterwards(err, updated) {
        if (err) {
          console.log(err);
        }
        if (updated) {
          res.send(updated[0]);
        }
      });
    } else {
      if (id && !pollData.draft) {
        Poll.destroy({id: id}, function destroyedCB(err, record) {
          Poll.create(pollData, function pollCreated(err, createdPoll) {
            if (err) {
              console.log(err);
            }
            if (createdPoll) {
              res.send(createdPoll);
            }
          });
        });
      } else {
        Poll.create(pollData, function pollCreated(err, createdPoll) {
          if (err) {
            console.log(err);
          }
          if (createdPoll) {
            res.send(createdPoll);
          }
        });
      }

    }
  },
  /**
   * Description
   * @method saveCustomDesign
   * @param {} req
   * @param {} res
   * @return 
   */
  saveCustomDesign: function (req, res) {
    var userId = req.session.User.id;
    var pollData = req.allParams();
    if(pollData.id) delete pollData.id;
    pollData.user = userId;
    PollCustomDesign.create(pollData, function pollCreated(err, createdDesign) {
      if (err) {
        console.log(err);
      }
      if (createdDesign) {
        res.send(createdDesign);
      }
    });
  },

  /**
   * Description
   * @method saveImage
   * @param {} req
   * @param {} res
   * @return 
   */
  saveImage: function (req, res) {
    var LoggedInUserid = req.session.User.id;
    var saveToServerName;
    var randNum = new Date().getTime();
    var thumbDir;
    var posterDir;
    req.file('file').upload({
      dirname: path.resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserid + '/poll',

      /**
       * Description
       * @method saveAs
       * @param {} __newFileStream
       * @param {} cb
       * @return 
       */
      saveAs: function (__newFileStream, cb) {
        var fname = __newFileStream.filename;
        var mName = fname.substr(0, fname.lastIndexOf('.'));
        var fName = mName.replace(/\s\s+/g, ' ');
        fName = fName.replace(/ /g, "-");
        var ext = path.extname(__newFileStream.filename);
        saveToServerName = fName + "_" + randNum + ext;
        cb(null, saveToServerName);
      },

      maxBytes: 10000000
    }, function whenDone(err, uploadedFiles) {

      if (err) {
        return res.negotiate(err);
      }

      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0) {
        return res.badRequest('No file was uploaded');
      } else {
        // var baseURL = Utility.getSiteBaseURL(req);

        let fileType = uploadedFiles[0].type;
        let fileExtName = fileType.substr(0, fileType.lastIndexOf('/'));

        let genThumUrl;
        let genPosterUrl;
        if (fileExtName === 'image' || fileExtName === 'video') {

          var fileNameWithExt = path.basename(uploadedFiles[0].fd);
          var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));

          if (fileExtName === 'image') {
            thumbDir = 'assets/uploads/files/' + LoggedInUserid + '/poll/thumb';
            if (!fs.existsSync(thumbDir)) {
              fs.mkdirSync(thumbDir);
            }

            Jimp.read(uploadedFiles[0].fd, function (err, image) {
              if (err) throw err;

              var extraLen;
              var img_w = parseInt(image.bitmap.width);
              var img_h = parseInt(image.bitmap.height);


              if (img_w > img_h) {
                extraLen = (img_w - img_h) / 2;
                image.crop(Math.floor(extraLen), 0, img_h, img_h);
              } else {
                extraLen = (img_h - img_w) / 2;
                image.crop(0, Math.floor(extraLen), img_w, img_w);
              }

              image.resize(220, Jimp.AUTO)
                .quality(60)
                .write('assets/uploads/files/' + LoggedInUserid + '/poll/thumb/' + uFileName + '.jpg'); // save
            }).catch(function (err) {
              console.log(err);
            });


            genThumUrl = path.dirname(uploadedFiles[0].fd) + '/thumb/' + uFileName + '.jpg';

          } else if (fileExtName === 'video') {

            thumbDir = 'assets/uploads/files/' + LoggedInUserid + '/poll/thumb';
            posterDir = 'assets/uploads/files/' + LoggedInUserid + '/poll/poster';
            if (!fs.existsSync(thumbDir)) {
              fs.mkdirSync(thumbDir);
            }
            if (!fs.existsSync(posterDir)) {
              fs.mkdirSync(posterDir);
            }


            const Thumbler = require('thumbler');

            Thumbler({
              type: 'video',
              input: uploadedFiles[0].fd,
              output: 'assets/uploads/files/' + LoggedInUserid + '/poll/poster/' + uFileName + '.jpeg',
              time: '00:00:03'
            }, function (err, path) {
              if (err) return err;
              // return path;
              // console.log('done');
            });

            var tempPosterDir = 'assets/uploads/files/' + LoggedInUserid + '/poll/poster/' + uFileName + '.jpeg';

            setTimeout(function () {

              Jimp.read(tempPosterDir, function (err, image) {
                if (err) {
                  console.log(err);
                }

                var fileNameWithExt = path.basename(uploadedFiles[0].fd);
                var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));

                var extraLen;
                var img_w = parseInt(image.bitmap.width);
                var img_h = parseInt(image.bitmap.height);

                if (img_w > img_h) {
                  extraLen = (img_w - img_h) / 2;
                  image.crop(Math.floor(extraLen), 0, img_h, img_h);
                } else {
                  extraLen = (img_h - img_w) / 2;
                  image.crop(0, Math.floor(extraLen), img_w, img_w);
                }

                image.resize(220, Jimp.AUTO)
                  .quality(80)
                  .write('assets/uploads/files/' + LoggedInUserid + '/poll/thumb/' + uFileName + '.jpg'); // save
              }).catch(function (err) {
                console.log(err);
              });

            }, 3000);

            genThumUrl = path.dirname(uploadedFiles[0].fd) + '/thumb/' + uFileName + '.jpg';
            genPosterUrl = path.dirname(uploadedFiles[0].fd) + '/poster/' + uFileName + '.jpeg';
          }
        }

        var thumb_dir = '/uploads/files/' + LoggedInUserid + '/poll/thumb/' + uFileName + '.jpg';
        var file_dir = '/uploads/files/' + LoggedInUserid + '/poll/' + path.basename(uploadedFiles[0].fd);

        var files = {};
        files.name = uploadedFiles[0].filename;
        files.ext = path.extname(uploadedFiles[0].filename);
        files.type = 'poll';
        files.owner = LoggedInUserid;
        files.file_dir = file_dir;
        files.fileFD = uploadedFiles[0].fd;
        files.fileThumb = genThumUrl;
        files.thumb_dir = thumb_dir;
        files.size = uploadedFiles[0].size;

        if (genThumUrl) {
          files.fileThumb = genThumUrl;
        }
        if (genPosterUrl) {
          files.filePoster = genPosterUrl;
        }
        UserFiles.create(files, function fileCreated(err, backFile) {
          if (backFile) {
            res.send(backFile);
          }
        });

      }
    });
  },

  /**
   * Description
   * @method getPolls
   * @param {} req
   * @param {} res
   * @return 
   */
  getPolls: function (req, res) {
    var userId = req.session.User.id;
    Poll.find({
      $or: [{user: userId}, { shared: userId}]
    }).populate('file', {select: ['_id', 'name', 'file_dir', 'thumb_dir', 'file_type', 'ext']}).populate('user', {select: ['_id', 'first_name', 'last_name', 'photo_url']}).populate('users', {select: ['_id']}).exec(function foundCB(err, records) {
      res.send(records);
    });
  },
  /**
   * Description
   * @method getCustomDesigns
   * @param {} req
   * @param {} res
   * @return 
   */
  getCustomDesigns: function (req, res) {
    var userId = req.session.User.id;
    PollCustomDesign.find({
      user: userId
    }).populate('file', {select: ['_id', 'name', 'file_dir', 'thumb_dir', 'file_type', 'ext']}).populate('user', {select: ['_id', 'first_name', 'last_name', 'photo_url']}).exec(function foundCB(err, records) {
      res.send(records);
    });
  },
  /**
   * Description
   * @method getDraft
   * @param {} req
   * @param {} res
   * @return 
   */
  getDraft: function (req, res) {
    Poll.findOne({
      id: req.param('id')
    }).populate('file', {select: ['_id', 'name', 'file_dir', 'thumb_dir', 'file_type', 'ext']}).populate('background', {select: ['_id', 'name', 'file_dir', 'thumb_dir', 'file_type', 'ext']}).populate('users', {select: ['_id', 'first_name', 'last_name', 'photo_url']}).exec(function foundCB(err, records) {
      res.send(records);
    });
  },
  /**
   * Description
   * @method loadCustomDesign
   * @param {} req
   * @param {} res
   * @return 
   */
  loadCustomDesign: function (req, res) {
    PollCustomDesign.findOne({
      id: req.param('id')
    }).populate('file', {select: ['_id', 'name', 'file_dir', 'thumb_dir', 'file_type', 'ext']}).populate('background', {select: ['_id', 'name', 'file_dir', 'thumb_dir', 'file_type', 'ext']}).exec(function foundCB(err, records) {
      res.send(records);
    });
  },
  /**
   * Description
   * @method loadPublicPollPreview
   * @param {} req
   * @param {} res
   * @return 
   */
  loadPublicPollPreview: function (req, res) {
    Poll.findOne({
      id: req.param('id')
    }).populate('file', {select: ['_id', 'name', 'file_dir', 'thumb_dir', 'file_type', 'ext']}).populate('background', {select: ['_id', 'name', 'file_dir', 'thumb_dir', 'file_type', 'ext']}).populate('users', {select: ['_id', 'first_name', 'last_name', 'photo_url']}).exec(function foundCB(err, records) {
      res.send(records);
    });
  },
  /**
   * Description
   * @method pollCount
   * @param {} req
   * @param {} res
   * @return 
   */
  pollCount: function (req, res) {
    var userId = req.session.User.id;
    var pollCount = {};
    Poll.count({ $or: [{user: userId}, { shared: userId}]}).exec(function countCB(error, found) {
      pollCount.all = found;
      if (found) {
        Poll.count({user: userId, draft: true}).exec(function CountPoll(err, foundDraft) {
          if (err) {
            console.log(err);
          }
          pollCount.drafts = foundDraft;
          res.send(pollCount);
        });
      } else {
        res.send(pollCount);
      }
    });
  },

  /**
   * Description
   * @method deletePoll
   * @param {} req
   * @param {} res
   * @return 
   */
  deletePoll: function (req, res) {
    var userId = req.session.User.id;
    Poll.destroy({id: req.param('id'), user: userId}, function destroyedCB(err, record) {
      if (record) {
        res.send('deleted');
      } else {
        res.send('ok');
      }
    });
  },
  /**
   * Description
   * @method deleteDesginItemsConfirm
   * @param {} req
   * @param {} res
   * @return 
   */
  deleteDesginItemsConfirm: function (req, res) {
    var userId = req.session.User.id;
    PollCustomDesign.destroy({id: req.param('id'), user: userId}, function destroyedCB(err, record) {
      if (record) {
        res.send('deleted');
      } else {
        res.send('ok');
      }
    });
  },
  /**
   * Description
   * @method saveVote
   * @param {} req
   * @param {} res
   * @return 
   */
  saveVote: function (req, res) {
    Poll.findOne(req.param('id'), function foundCB(err, record) {
      var answers = req.param('answers');
      var userId = req.session.User.id;

      if (answers) {
        var AsyncCall = require('async');
        AsyncCall.forEach(answers, function (item, callback) {
          var voteItem = {
            'poll': req.param('id'),
            'answer_id': item.answer_id,
            'ip': req.ip,
            'user_agent': req.headers['user-agent']
          };
          voteItem.user = userId;

          PollVote.create(voteItem, function CreatCB(err) {
            //Updating the vote count value for the answer itself in PollAsnwer model

            pollData = record;

            var allAns = record.answers, answers = [];
            for (var inc = 0; inc < (allAns.length); inc += 1) {
              var aitem = {};
              aitem.id = inc + 1;
              aitem.txt = allAns[inc].txt;
              if (allAns[inc].id == item.answer_id) {
                aitem.vote = allAns[inc].vote + 1;
              } else {
                aitem.vote = allAns[inc].vote;
              }

              answers.push(aitem);
            }
            pollData.tvote = record.tvote + 1;
            pollData.answers = answers;

            Poll.update(record.id, pollData, function UpdatedCB(err) {
              console.log('vote updated');
              callback();
            });

          });
        }, function (err) {
          PollService.findPollByID(req.param('id'), function (record) {
            res.send(record);
          });
        });
      } else {
        PollService.findPollByID(req.param('id'), function (record) {
          res.send(record);
        });
      }
    });
  },
  /**
   * Description
   * @method checkIfAlreadyVoted
   * @param {} req
   * @param {} res
   * @return 
   */
  checkIfAlreadyVoted: function (req, res) {
    var pollId = req.param('id');
    var userId = req.session.User.id;
    PollService.findPollByID(pollId, function (pollRecord) {
      if (pollRecord.ip_restriction == true) {
        PollService.findPollVotesOfIPByID(pollId, req.ip, function dataRecord(record) {
          if (record && record.length > 0) {
            res.send({'voted': true});
          } else {
            res.send({'voted': false});
          }
        });
      } else if (pollRecord.ip_restriction == false) {
        PollService.findPollVotesOfMeByID(pollId, userId, function dataRecord(record) {
          if (record && record.length > 0) {
            res.send({'voted': true});
          } else {
            res.send({'voted': false});
          }
        });
      } else if (pollRecord.allow_vote_again == 'never') {
        res.send({'voted': false});
      } else {
        res.send({'voted': true});
      }
    });

  },
  /**
   * Description
   * @return
   * @method getPollById
   * @param {} req
   * @param {} res
   * @return 
   */
  getPollById: function (req, res) {
    PollService.findPollByID(req.param('id'), function (record) {
      res.send(record);
    });
  },

};
