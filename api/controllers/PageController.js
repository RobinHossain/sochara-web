module.exports = {
  /**
   * Description
   * @method facebookPrivacyPolicy
   * @param {} req
   * @param {} res
   * @return 
   */
  facebookPrivacyPolicy: function (req, res) {
    res.view('pages/facebook/privacyPolicy', {layout: 'page'});
  }
};
