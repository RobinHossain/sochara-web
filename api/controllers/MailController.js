const AsyncCall = require('async');
const moment = require('moment');
const google = require('googleapis');
const outlook = require('nodejs-outlook');



// Gmail Mail request Variables
// For Sochara Local
// var CLIENT_ID = '678407714922-utfq35jpavvltnnrm41fg0u80fo0p41u.apps.googleusercontent.com';
// var CLIENT_SECRET = 'sSwAiWLv9IcAs0Jv1_l72fa_';
// var REDIRECT_URL = 'http' + '://' + 'localhost' + '/gmail/oauth2callback';

// For Sochara Live
var CLIENT_ID = '678407714922-1auh2t76gvk479sn1m02g4omt22oo0in.apps.googleusercontent.com';
var CLIENT_SECRET = '9CdpLyvCF7HvZ2vDQ8TgK31j';
var REDIRECT_URL = 'https' + '://' + 'www.sochara.com' + '/gmail/oauth2callback';

var OAuth2Client;
var gmail;
var oauth2Client;

// Outlook Mail Request Variables
var Mailbox = 'Inbox';
var top;
var credentials = {
  client: {
    id: '55a857a7-806b-4f56-9c0a-c086a8c9ab95',
    secret: 'ybNEFBRxvwyYV2MzFGvEpkX',
  },
  auth: {
    tokenHost: 'https://login.microsoftonline.com',
    authorizePath: 'common/oauth2/v2.0/authorize',
    tokenPath: 'common/oauth2/v2.0/token'
  }
};
var outlookOauth2 = require('simple-oauth2').create(credentials);

/**
 * Description
 * @method refreshAccessToken
 * @param {} refreshToken
 * @param {} callback
 * @return
 */
var refreshAccessToken = function (refreshToken, callback) {
  var tokenObj = outlookOauth2.accessToken.create({refresh_token: refreshToken});
  tokenObj.refresh(callback);
};

module.exports = {

  /**
   * Description
   * @method mailSocket
   * @param {} req
   * @param {} res
   * @return
   */
  mailSocket: function (req, res) {
    if (!req.isSocket) {
      console.log('bad');
      return res.badRequest();
    }
    // sails.sockets.join(req, 'sochara_' + user);
  },

  /**
   * Description
   * @method getMailAddresses
   * @param {} req
   * @param {} res
   * @return
   */
  getMailAddresses: function (req, res) {
    var where = {};
    where.user = req.session.User.id;
    if (req.param('provider')) {
      where.provider = req.param('provider');
    }
    where.select = ['_id', 'email', 'name', 'provider'];
    var mailQuery = EmailAccount.find(where);
    mailQuery.exec(function foundCB(err, records) {
      if (records && records.length) {
        res.send(records);
      } else {
        res.send({message: "No email account found."});
      }
    });
  },

  /**
   * Description
   * @method getMailLabels
   * @param {} req
   * @param {} res
   * @return
   */
  getMailLabels: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var providers = ['gmail', 'outlook', 'sochara'];
    var labels = [];

    AsyncCall.forEach(providers, function (provider, callback) {
      if (provider === 'gmail') {
        EmailAccount.find({user: req.session.User.id, select: ['_id']}).exec(function foundCB(err, records) {
          AsyncCall.forEach(records, function (record) {
            GmailService.getListsOfMailboxes(LoggedInUserID, record.id, req, res, function (data) {
              labels.push(data.labels);
              callback();
            });
          }, function (err) {
            if (err) console.log(err);
            else {

            }
          });

        });
      } else {
        callback();
      }
    });
  },

  /**
   * Description
   * @method checkEmailStates
   * @param {} req
   * @param {} res
   * @return
   */
  checkEmailStates: function(req, res){
    let user = req.session.User.id;
    EmailAccount.find({user: user, select: ['_id', 'email', 'name', 'access_token', 'refresh_token', 'provider', 'user']}).exec(function (err, respEmails) {
      AsyncCall.forEach(respEmails, function (record, callback) {
        if( record.provider === 'gmail'){
          GmailService.getMailThreadsCompact(record.id, user, 'INBOX', 30, '', '', 'yes', record.access_token, record.refresh_token, req, res, function (data) {
            if (data) {
              MailService.gmailStateCheckAndStore(data, record.email, record.id, record.user, function (mailData) {
                callback();
              });
            } else {
              callback();
            }
          });
        }else if( record.provider === 'outlook'){
          OutlookService.getMessages(record, 'Inbox', 30, 0, '', req, res, function (data) {
            MailService.outlookCheckStatusAndStore(data, record, function (mailData) {
              callback();
            });
          });
        }else{
          callback();
        }
      }, function (err) {
        if (err) console.log(err);
        else {
          res.send(200);
        }
      });
    });
  },

  /**
   * Description
   * @method loadSubFolders
   * @param {} req
   * @param {} res
   * @return
   */
  loadSubFolders: function(req, res){
    let account = req.param('account');
    let folders = [];
    if(account.provider === 'outlook'){
      OutlookService.getMailSubFolders(req, function (data) {
        MailService.outlookMailBoxExtract(data, folders, function (formattedFolders) {
          res.send(formattedFolders);
        });
      });
    }
  },

  /**
   * Description
   * @method getAllGmailMessages
   * @param {} req
   * @param {} res
   * @return
   */
  getAllGmailMessages: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var gmailAccounts = [];
    var gmailMessages = [];

    EmailAccount.find({
      user: LoggedInUserID,
      select: ['_id', 'email', 'name', 'access_token', 'refresh_token']
    }).exec(function foundCB(err, records) {
      gmailAccounts.push(records);
      AsyncCall.forEach(records, function (record, callback) {
        var labelID = req.param('folder') || 'INBOX';
        var maxResults = req.param('maxResults') || 10;
        var pageToken = req.param('nextPageToken') || '';
        var withOutCache = req.param('withOutCache') || 'yes';
        GmailService.getMailThreadsCompact(record.id, LoggedInUserID, labelID, maxResults, pageToken, '', withOutCache, record.access_token, record.refresh_token, req, res, function (data) {
          if (data) {
            MailService.gmailExtract(data, gmailMessages, record.email, record.id, function (mailData) {
              gmailMessages = mailData;
              callback();
            });
          } else {
            callback();
          }
        });
      }, function (err) {
        if (err) console.log(err);
        else {
          // console.log(gmailMessages);
          res.send(gmailMessages);
        }
      });
    });
  },

  /**
   * Description
   * @method getOutlookMessages
   * @param {} req
   * @param {} res
   * @return
   */
  getOutlookMessages: function(req, res){
    let user = req.session.User.id;
    let accountId = req.param('id');
    let folder = req.param('folder') || 'Inbox';
    let max = req.param('max') || 30;
    let skip = req.param('skip') || 0;
    EmailAccount.findOne({id: accountId, user: user, select: ['_id', 'email', 'name', 'provider', 'access_token', 'refresh_token', 'user']}).exec(function (err, emailAccount) {
      if(err) console.log(err);
      if(emailAccount){
        let outlookMessageData = {messages: [], skip: skip + 30};
        OutlookService.getMessages(emailAccount, folder, max, skip, '', req, res, function (data) {
          MailService.outlookExtract(data, outlookMessageData, emailAccount.email, emailAccount.id, function (mailData) {
            res.send(mailData);
          });
        });
      }else{
        res.send(200);
      }
    });
  },

  /**
   * Description
   * @method countAllUnreadMail
   * @param {} req
   * @param {} res
   * @return
   */
  countAllUnreadMail: function (req, res) {
    let LoggedInUserID = req.session.User.id;
    let mailData = {gmail: 0, outlook: 0, all: 0};
    EmailAccount.find({user: LoggedInUserID, select: ['_id', 'email', 'name', 'provider', 'access_token', 'refresh_token']}).exec(function foundCB(err, emailAccounts) {
      AsyncCall.each(emailAccounts, function (emailAccount, callback) {
        if(emailAccount.provider === 'gmail'){
          req.oauth2Client.setCredentials({
            access_token: emailAccount.access_token,
            refresh_token: emailAccount.refresh_token
          });
          req.gmail.users.labels.get({id: 'INBOX', userId: emailAccount.email, auth: req.oauth2Client}, function (err, respLabel) {
            if(err)console.log(err);
            if(respLabel && respLabel.threadsUnread){
              mailData.gmail = mailData.gmail + respLabel.threadsUnread;
              mailData.all = mailData.all + respLabel.threadsTotal;
            }
            callback();
          });
        }else if(emailAccount.provider === 'outlook'){
          console.log(emailAccount.refresh_token);
          req.refreshAccessToken(emailAccount.refresh_token, function (error, newToken) {
            if (error) console.log(error);
            if(newToken && newToken.token){
              req.getMailFolder(newToken.token.access_token, emailAccount.email, Mailbox, function (respFolder) {
                if(respFolder && respFolder.UnreadItemCount){
                  mailData.outlook = mailData.outlook + respFolder.UnreadItemCount;
                  mailData.all = mailData.all + respFolder.TotalItemCount;
                }
                callback();
              });
            }else{
              callback();
            }
          });
        }
      }, function (err) {
        if (err){
          console.log(err);
        }else{
          res.send(mailData);
        }
      });
    });
  },

  /**
   * Description
   * @method loadAccountMailCounting
   * @param {} req
   * @param {} res
   * @return
   */
  loadAccountMailCounting: function (req, res) {
    let LoggedInUserID = req.session.User.id;
    let where = {}, mailData = {all: 0};
    if(req.param('id')){
      where.id = req.param('id');
    }
    if(req.param('provider')){
      where.provider = req.param('provider');
    }
    EmailAccount.find(where).exec(function foundCB(err, emailAccounts) {
      if( emailAccounts && emailAccounts.length ){
        AsyncCall.each(emailAccounts, function (emailAccount, callback) {
          if(emailAccount.provider === 'gmail'){
            req.oauth2Client.setCredentials({
              access_token: emailAccount.access_token,
              refresh_token: emailAccount.refresh_token
            });
            req.gmail.users.labels.get({id: 'INBOX', userId: emailAccount.email, auth: req.oauth2Client}, function (err, respLabel) {
              if(err)console.log(err);
              if(respLabel && respLabel.threadsUnread){
                mailData.all = mailData.all + respLabel.threadsTotal;
              }
              callback();
            });
          }else if(emailAccount.provider === 'outlook'){
            req.refreshAccessToken(emailAccount.refresh_token, function (error, newToken) {
              if (error) console.log(error);
              req.getMailFolder(newToken.token.access_token, emailAccount.email, Mailbox, function (respFolder) {
                if(respFolder && respFolder.UnreadItemCount){
                  mailData.all = mailData.all + respFolder.TotalItemCount;
                }
                callback();
              });
            });
          }
        }, function (err) {
          if (err){
            console.log(err);
          }else{
            res.send(mailData);
          }
        });
      }else{
        res.send(mailData);
      }
    });
  },

  /**
   * Description
   * @method modifyThreadFromSochara
   * @param {} req
   * @param {} res
   * @return
   */
  modifyThreadFromSochara: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var mail_id = req.param('mail_id');
    var where = {};
    var updateData = {};
    var deleteConfirm = false;
    where.user = LoggedInUserID;
    if (mail_id) {
      where.mail_id = mail_id;
    } else {
      res.send('no Mail ID');
    }


    if (req.param('add')) {
      if (req.param('add') === 'unread' || req.param('add') === 'UNREAD' || req.param('add') === 'Unread') {
        updateData.read = false;
      } else if (req.param('add') === 'important' || req.param('add') === 'IMPORTANT' || req.param('add') === 'Important') {
        updateData.important = true;
      } else if (req.param('add') === 'starred' || req.param('add') === 'STARRED' || req.param('add') === 'Starred') {
        updateData.starred = true;
      } else if (req.param('add') === 'spam' || req.param('add') === 'SPAM' || req.param('add') === 'Spam') {
        deleteConfirm = true;
      } else if (req.param('add') === 'trash' || req.param('add') === 'TRASH' || req.param('add') === 'Trash') {
        deleteConfirm = true;
      }
    }

    if (req.param('remove')) {
      if (req.param('remove') === 'unread' || req.param('remove') === 'UNREAD' || req.param('remove') === 'Unread') {
        updateData.read = true;
      } else if (req.param('remove') === 'important' || req.param('remove') === 'IMPORTANT' || req.param('remove') === 'Important') {
        updateData.important = false;
      } else if (req.param('remove') === 'starred' || req.param('remove') === 'STARRED' || req.param('remove') === 'Starred') {
        updateData.starred = false;
      } else if (req.param('remove') === 'spam' || req.param('remove') === 'SPAM' || req.param('remove') === 'Spam') {
        deleteConfirm = false;
      } else if (req.param('remove') === 'inbox' || req.param('remove') === 'INBOX' || req.param('remove') === 'Inbox') {
        deleteConfirm = true;
      }
    }



    if (deleteConfirm === true) {
      Email.destroy({mail_id: mail_id, user: LoggedInUserID}).exec(function (err) {
        res.send('deleted');
      });
    } else {
      var mailQuery = Email.update(where, updateData);
      mailQuery.exec(function foundCB(err, records) {
        if (records && records.length ) {
          res.send(records[0].id);
        }else{
          res.send(200);
        }
      });
    }
  },


  /**
   * Description
   * @method getNextMessages
   * @param {} req
   * @param {} res
   * @return
   */
  getNextMessages: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var messageData = {};
    messageData.pagination = [];
    messageData.messages = [];
    var skip = req.param('skip') || '';
    var limit = req.param('limit') || 30;
    var where = {};
    where.user = LoggedInUserID;

    var pagination = {};
    pagination.skip = skip + 30;
    messageData.pagination.push(pagination);

    if (req.param('provider')) {
      where.provider = req.param('provider');
    }

    if (req.param('email_id')) {
      where.email_id = req.param('email_id');
    }

    var mailQuery = Email.find(where);
    mailQuery.limit(limit);
    if (skip) {
      mailQuery.skip(skip);
    }
    mailQuery.sort({date: -1});

    mailQuery.exec(function foundCB(err, mailRecords) {
      var mailLengthCount = mailRecords.length;
      if (mailLengthCount < 30) {
        EmailAccount.find({
          user: LoggedInUserID,
          select: ['_id', 'email', 'name', 'provider', 'refresh_token', 'user']
        }).exec(function foundCB(err, records) {
          if (records && records.length) {
            var maxMailFetchLimitation = Math.ceil(30 / records.length);
            AsyncCall.forEach(records, function (emailAccount, callback) {
              Email.findOne({
                address: emailAccount.email,
                user: emailAccount.user,
                provider: emailAccount.provider
              }).sort('date ASC').exec(function foundCB(err, firstMail) {
                if (firstMail && firstMail.provider === 'gmail') {
                  OAuth2Client = google.auth.OAuth2;
                  oauth2Client = new OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);
                  oauth2Client.setCredentials({
                    access_token: emailAccount.access_token,
                    refresh_token: emailAccount.refresh_token
                  });
                  var $params = {};
                  $params.labelIds = ["INBOX"];
                  $params.maxResults = maxMailFetchLimitation;
                  $params.userId = 'me';
                  $params.auth = oauth2Client;

                  if (firstMail && firstMail.date) {
                    var firstMailDate = moment(firstMail.date).format('YYYY/MM/DD');
                    $params.q = 'in:inbox before:' + firstMailDate;
                  }
                  gmail = google.gmail('v1');
                  gmail.users.threads.list($params, function (err, data) {
                    var threads = [];
                    if (err) console.log(err);
                    if (data) {
                      threads = data.threads;
                      AsyncCall.forEachOf(threads, function (value, key, callback) {
                        if (value.id) {
                          gmail.users.threads.get({
                            id: value.id,
                            format: 'full',
                            userId: 'me',
                            auth: oauth2Client
                          }, function (err, body) {
                            if (err) console.log(err);
                            if (body && body.messages) {
                              threads[key]['metadata'] = body.messages;
                            }
                            callback();
                          });
                        } else {
                          callback();
                        }

                      }, function (err) {
                        if (err) console.log(err);
                        data.threads = threads;
                        if (data) {
                          MailService.gmailExtractAndStore(data, emailAccount.email, emailAccount.id, emailAccount.user, function (mailData) {
                            callback();
                          });
                        } else {
                          callback();
                        }
                      });
                    }
                  });
                } else if (firstMail && firstMail.provider === 'outlook') {

                  // Outlook Direct Fetch Data

                  top = maxMailFetchLimitation;
                  var pagination = {};
                  refreshAccessToken(emailAccount.refresh_token, function (error, newToken) {
                    if (error) {
                      console.log(error);
                    }
                    if (newToken) {
                      EmailAccount.update({
                        id: emailAccount.id,
                        user: emailAccount.user,
                        provider: 'outlook'
                      }, {
                        access_token: newToken.token.access_token,
                        refresh_token: newToken.token.refresh_token,
                        id_token: newToken.token.id_token,
                        expires_at: newToken.token.expires_at
                      }).exec(function afterwards(err, updated) {
                        if (err) console.log(err);
                        // console.log('updated');
                      });
                      var filter = 'ReceivedDateTime' + ' le ' + moment(firstMail.date).format('YYYY-MM-DD');
                      getOutlookEmails(newToken.token.access_token, emailAccount.email, Mailbox, top, skip, filter, function (resp) {
                        if (resp) {
                          MailService.outlookExtractAndStore(resp, emailAccount.email, emailAccount.id, emailAccount.user, function (mailData) {
                            callback();
                          });
                        } else {
                          callback();
                        }
                      });
                    }
                  });
                } else {
                  callback();
                }
              });
            }, function (err) {
              if (err) console.log(err);

              var updateQuery = Email.find(where);
              updateQuery.limit(limit);
              if (skip) {
                updateQuery.skip(skip);
              }
              updateQuery.sort({date: -1});

              updateQuery.exec(function foundCB(err, records) {
                if (records && records.length) {
                  messageData.messages = records;
                  res.send(messageData);
                }
              });
            });
          } else {
            res.send({message: "No email account found."});
          }
        });
      } else {
        messageData.messages = mailRecords;
        res.send(messageData);
      }
    });

  },

  /**
   * Description
   * @method findSearchedMail
   * @param {} req
   * @param {} res
   * @return
   */
  findSearchedMail: function (req, res) {
    let userId = req.session.User.id;
    let skip = req.param('skip') || '';
    let limit = req.param('limit') || 30;
    let text = req.param('text') || '';
    let where = {user: userId, $or:[{subject : { 'like': '%'+text+'%' }}, {message : { 'like': '%'+text+'%' }}, {address : { 'like': '%'+text+'%' }}, {from: {$elemMatch: {name: { 'like': '%'+text+'%' }}}}]};
    let mailQuery = Email.find(where);
    mailQuery.limit = limit;
    if( skip ){
      mailQuery.skip = skip;
    }
    mailQuery.exec(function (err, respMails) {
      if( err ){
        console.log(err);
        return res.serverError(err);
      }else{
        res.send(respMails);
      }
    });
  },


  /**
   * Description
   * @method getMessages
   * @param {} req
   * @param {} res
   * @return
   */
  getMessages: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var messageData = {};
    messageData.pagination = [];
    messageData.messages = [];
    var skip = req.param('skip') || '';
    var limit = req.param('limit') || 30;
    var where = {};
    where.user = LoggedInUserID;

    var pagination = {};
    pagination.skip = skip + 30;

    if (req.param('provider')) {
      where.provider = req.param('provider');
    }

    if (req.param('email_id')) {
      where.email_id = req.param('email_id');
    }

    let mailQuery = Email.find(where);
    mailQuery.limit(limit);
    if (skip) {
      // mailQuery.skip(skip);
    }
    mailQuery.sort({date: -1});

    mailQuery.exec(function foundCB(err, records) {
      if (records && records.length) {
        messageData.messages = records;
        res.send(messageData);
      } else {
        res.send(messageData);
      }
    });
  },

  /**
   * Description
   * @method getGmailMessages
   * @param {} req
   * @param {} res
   * @return
   */
  getGmailMessages: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var gmailMessageData = {};
    gmailMessageData.pagination = [];
    gmailMessageData.messages = [];
    var labelID = req.param('folder') || 'INBOX';
    var maxResults = req.param('maxResults') || 10;
    var pageToken = req.param('nextPageToken') || '';
    var withOutCache = req.param('withOutCache') || 'no';

    // console.log('Max Result');

    EmailAccount.findOne({
      user: LoggedInUserID,
      id: req.param('id'),
      provider: 'gmail',
      select: ['_id', 'email', 'name', 'access_token', 'refresh_token']
    }).exec(function foundCB(err, record) {
      if (record) {
        var pagination = {};
        GmailService.getMailThreadsCompact(record.id, LoggedInUserID, labelID, maxResults, pageToken, '', withOutCache, record.access_token, record.refresh_token, req, res, function (data) {
          if (data) {
            pagination.provider = 'gmail';
            pagination.id = record.id;
            pagination.next = data.nextPageToken;
            gmailMessageData.pagination.push(pagination);
            MailService.gmailExtract(data, gmailMessageData, record.email, record.id, function (mailData) {
              res.send(mailData);
            });
          } else {
            res.send({message: 'No Message Found'});
          }
        });
      } else {
        res.send({message: 'No Record Found'});
      }
    });
  },

  /**
   * Description
   * @method GMailThreadCount
   * @param {} req
   * @param {} res
   * @return
   */
  GMailThreadCount: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    EmailAccount.findOne({
      user: LoggedInUserID,
      id: req.param('id'),
      select: ['email', 'access_token', 'refresh_token']
    }).exec(function foundCB(err, record) {
      if (record) {
        req.oauth2Client.setCredentials({
          access_token: record.access_token,
          refresh_token: record.refresh_token
        });
        // req.gmail.users.getProfile({userId: record.email, auth: req.oauth2Client}, function (err, data) {
        req.gmail.users.labels.get({id: 'INBOX', userId: record.email, auth: req.oauth2Client}, function (err, data) {
          res.send(data);
        });
      }
    });
  },

  /**
   * Description
   * @method getMail
   * @param {} req
   * @param {} res
   * @return
   */
  getMail: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    Email.findOne({
      user: LoggedInUserID,
      mail_id: req.param('id')
    }).exec(function foundCB(err, record) {
      if (record) {
        res.send(record);
      } else {
        res.send({message: "now Email found"});
      }
    });
  },

  /**
   * Description
   * @method GMailThreadFolderCount
   * @param {} req
   * @param {} res
   * @return
   */
  GMailThreadFolderCount: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    EmailAccount.findOne({
      user: LoggedInUserID,
      id: req.param('id'),
      select: ['email', 'access_token', 'refresh_token']
    }).exec(function foundCB(err, record) {
      if (record) {
        req.oauth2Client.setCredentials({
          access_token: record.access_token,
          refresh_token: record.refresh_token
        });
        // req.gmail.users.getProfile({userId: record.email, auth: req.oauth2Client}, function (err, data) {
        req.gmail.users.labels.get({
          id: req.param('folder'),
          userId: record.email,
          auth: req.oauth2Client
        }, function (err, data) {
          res.send(data);
        });
      }
    });
  },
  /**
   * Description
   * @method RemoveCurrentAccount
   * @param {} req
   * @param {} res
   * @return
   */
  RemoveCurrentAccount: function (req, res) {
    var LoggedInUserID = req.session.User.id;

    EmailAccount.findOne({
      user: LoggedInUserID,
      email: req.param('email'),
      select: ['id', 'email']
    }).exec(function foundCB(err, record) {
      if (record) {

        Email.destroy({email_id: record.id, user: LoggedInUserID}).exec(function (err) {
          if (err) console.log(err);
          Cron.destroy({
            module: 'mail',
            user: LoggedInUserID,
            'data.id': {contains: record.id}
          }).exec(function (err) {
            if (err) {
              return console.log(err);
            }
            EmailAccount.destroy({id: record.id, user: LoggedInUserID}).exec(function (err) {
              if (err) console.log(err);
              CalendarEvent.destroy({mail_id: record.id, user: LoggedInUserID}).exec(function (err) {
                if (err) console.log(err);
                res.send(200);
              });
            });
          });
        });

      } else {
        res.send({message: "Can not find the email address"});
      }

    });
  },

  /**
   * Description
   * @method sendEmail
   * @param {} req
   * @param {} res
   * @return
   */
  sendEmail: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var threadID = req.param('threadID') || '';
    var from = req.param('from') || '';
    var to = req.param('to') || '';
    var cc = req.param('cc') || '';
    var bcc = req.param('bcc') || '';
    var subject = req.param('subject');
    var attachments = req.param('attachments');
    var body = req.param('body') || '';
    var id = req.param('id');


    // to = "robinbdseo1@gmail.com,robinbdseo3@gmail.com";

    EmailAccount.findOne({
      user: LoggedInUserID,
      id: id,
      select: ['id', 'email', 'provider', 'access_token', 'refresh_token']
    }).exec(function foundCB(err, record) {
      if (record && record.provider === 'gmail') {

        var $base64RawEmail;

        var testPngData = "iVBORw0KGgoAAAANSUhEUgAAAJMAAAA5CAYAAAAyYhmdAAAJ70lEQVR4Xu2bf2xT1xXHP3ZsJ04CoeNXQDQTXSTYFKYUJDq1QUCKWtZ1YYIwUaamCEK2AqOE0jSUFBoWSpoxQjMgbYCqMJVFaobWdNqytqRU0AmoYEigFaQwtAhBKIxfsfPDduzpPcfPL46f/Sw/5sq6/gvl3XfuPed+fO73nGtMs4uW+xAfEQEDImASMBkQRWFCjoCASYBgWAQETIaFUhgSMAkGDIuAgMmwUApDAibBgGEREDAZFkphSMAkGDAsAgImw0IpDAmYBAOGRUDAZFgohSEBk2DAsAgImAwLpTAkYBIMGBYBAZNhoRSGBEyCAcMiIGAyLJTCkIBJMGBYBARMhoVSGBIwCQYMi4BBMJlYbLXxmM3C2BQzmAbX5x3grtvD+X4X+3X8t4WpJgs/S7WRYzWTYR404vPhGvBwuc/Fnwe8XNTj+uTHKVsyh8K8HLIzrcobLlcPt69cpL21maYT/x1u6aev8JfSqWRynbYFVdRGnGs5hz56ghx6OLf/16z7WGvwaNbX11H0iPTczb8O/opVRyIYVtYQzVE3jju36Dj1KbWNX9AVZXj2i1tonp8jj3JdauGpir9FmyDm53HDNMZkoTLTzlhzpLl93O3t4XcuL51hh5l5Od3OD60BIz5c3kH6TGZsCpwevnb2UuvV3riiDeWUzZpAZmCIy41L/rcVmy3wRzddZ1tYV/3Z0E14EDDl/ZLmbTPJDkz972MsKf+D9uarYVLWHuqv2hdwnP8jpVUhvgx5ZQqbmyooHB/4YyetK6vZ+U3MvER8IU6YzLyemUFuCjDg5kxPP597fZwfnHKayczcVDszUiVIfNx0OtjgCV2PmcqMDL5vkYYMcK2nj8Mer2JDGi1lrBcy0piYYgKfl2tOJxsHQu2MprhmE2umZckPHFdP835jCy0XghkoO28eZS8uoHBSupwlOo/upqThQtDQA4Apf+Nb7PrRGLh6na5JE8jmFu2bXmWratohnihriJLxJs9m84ZixZeIGS9vNc3bppPNdTpvTCBnPHQdr2PJjkuG0hQfTClpHJSOEZ+bk/f7aNRY2tK0TJ5ONYGnnw+cLj5RjStNy2SW9Mzr5nh3H/sjuKeM9Xk4c7+XBtXY7JLXOLToe9gkSI4foGLHVxrf/odZX7+Jokek4y9kYw2H6XHq3l/BzIeg82g9HXnlcna4ffYAC6v/Ed5TvTDJby9i74fP8AObPzs9W/VZWJvzt+ykcnoWXP2SiitTqJs1Bu6cp3bZLtoMxCkumAqs6axMTwkLyZA1mmzsHZlKhtdFe3c/BwMPzansG2HDhpeObie/0Ty+lBeCmbC/h5f7BrglP1Kl8RtnqSjbw+lIQZKPnkfJvHOLc61VVAU0jNEwLXyNT16QAL9O28oq2kreYpe0ka7LNC1+k8Ph1hgTTKPZ3FTnP76ufsmc1e+Fsfhj9n5YLAPXebSCkvbiwWNXh36LEbS4YELJTMMzhZ51LE7N4Nk0M3j62Od0c0LHS1MtdjZmWORsePz+YCYrWMeRV6bxHQYD1hBGXOuwjcEwLa17h7IpVgjopHHPs3/fHHKBjrYKShsjFQHRhD3w6Gqa35COLynbvcPC6q+Ge6kAHdBJqoIgmn7TEzPVmPhgQq13vNztd3HW7eaUFx1Vl4nKjExZKzl7naxyRU1Lg8u28nZWGqPU2WxtDceenCAfWyc2vUqVlh6JFhwjYVKD07qC0gP+ydfUH6BYquxunGZd2bucC12Tnsw0OY+iWbMpfno6OVKl4bpOW3UVtcP8VoPzKXPKm/2zrdjCsSKpsoui36LFK+R5nDBJ1lLYmmnnu5I4Vj5SOe/lltvNZbeHE15fGLhS2D4ynYkmuOnsDiPMtTyxsD3LzkTgmqNbFuKFW3ayWdIEdNKyoJrdMQZBGa67LFdPED6D5G/QONKUTHGP02+vp6I9ZLGxrsFxnfZD9Wz9e5gsp1SSoUda8OiLqN9ijKMBMPlnLDBbmCv3iFKCpXxgMVKV1ttHo1vdGhgOhb61B98LZLTimt+zZlo6OC6y+xe/pUWfoeGjYt1I2UI4mIIabrgwDorysP2eCK0Bmy3QM3Nz+9J5Tpz8gtYjF+jQ8FcBOkxcFFEeSb/FGEfDYFLPm4OJx1IsTLVZh8Dl6u+loc8zWPbHD1MgMynlt2GZKc6mZeE6jrwkabjw2UfZSML0eyIdc5NnU7PxOQrGW8F1j9OHtlHxsZY+DEIbNvsoa4yg374NMA1dg4mltjTm2i3Y8PEfh4PNco/IQM2k0gDfBs2kCG8dmzGs3xNNM42bx67658gPaKU3q6j9Z5iJlONUxyK09JuOV9VD4shMFraPSGOi2cvX93uojXJdstY+ghm2oWLbsGpu3HIO7ZOuNvRWc6Op3LONwlH36Dj1EasaBns+hgjwoB5Bs4MN2KzIDfnQfk80mKR31EehRiskCLQbl/8KIMwn0EnX0G//P5jMvD4ig1wz3O1x8JI7Mk2laSOYlRpSuSl9JnXGiuSBqnqMp8+kVVIbAFPwDszfW6rVuLIIjgsRx3pgQur2v+HXiUjd7HqW7FCVcqpKUu4tabVKVOOMuK+LIzPBT2wZ/NxuBq+HM45eGjR5srBjpJ2xpmAFFkBG3QE/2a3dRZfGR+qAB7+t0TrgeVTuWcP8SZLu6KRldTW7AxseN0yq5mnUHo4qg6nH6oIJGLeIvXv83e9QPxThHU6ThXxXgxks/vu6uGAC6aizM1G+evNys6+fz10eTskdDJCFuNVGQZqNUdKYMNcpDOlV+e/mDnqG/jpAuptbbE8j1yq1H3xcczg07uaC39Zwd3O5hc+zpuQJ8h+SqqIHcDenutTVbEqqNjO4kap+j16YgPy1NdQ9OUE+Ll2X/kpJxZ/oUt8GRAUaUGmreO/r4oQJ9P1qQLoIdtHuUF2lDPmGmFhrT2eGLd5fDTzM0i3lLJue5dcj0iegWwIaRf5jDx1t71LaGNLlizMzqas0Xf0uVUWlVFwxwAR51DSVUyD/GmCwReEMVJLQoWqWaouHYNUX731d3DAFFvlUio25qRbGWFR9Jp8Pp8dDZ38/hwd8Gj8/CbqZY7KwdNjvmbw4PQN09uv/PVNu4SKWzZ9J/uQsMpXeDLgc9+i6coGWA+/ReiVMeOOCaR67PvBXWfr1xxRqmir8MAT6PTHBBKiA5MZZWr+ZSpGkpWLoHwVbK/Hd1xkGU4zCXwxPwggImJJwUxPlkoApUZFPwnkFTEm4qYlyScCUqMgn4bwCpiTc1ES5JGBKVOSTcF4BUxJuaqJcEjAlKvJJOK+AKQk3NVEuCZgSFfkknFfAlISbmiiXBEyJinwSzitgSsJNTZRLAqZERT4J5xUwJeGmJsolAVOiIp+E8wqYknBTE+WSgClRkU/Cef8HIx94g4vciHAAAAAASUVORK5CYII=";

        if (attachments && attachments.length) {

          $base64RawEmail = [
            'Content-Type: multipart/mixed; boundary="foo_bar_baz"\r\n',
            'MIME-Version: 1.0\r\n',
            'From: ' + from + '\r\n',
            'To: ' + to + '\r\n',
            'Subject: ' + subject + '\r\n',

            '\r\n' + '--foo_bar_baz\r\n',
            'Content-Type: text/html; charset="UTF-8"\r\n',
            'Content-Transfer-Encoding: base64\r\n\r\n',

            body + '\r\n\r\n'];

          attachments.forEach(function (aval) {
            $base64RawEmail.push('--foo_bar_baz\r\n');
            $base64RawEmail.push('Content-Type: ' + aval.type + '\r\n');
            $base64RawEmail.push('MIME-Version: 1.0\r\n');
            $base64RawEmail.push('Content-Transfer-Encoding: base64\r\n');
            $base64RawEmail.push('Content-Disposition: attachment; filename=' + aval.name + '\r\n\r\n');
            $base64RawEmail.push(aval.url, '\r\n\r\n');
          });

          $base64RawEmail.push('--foo_bar_baz--');



          if (cc !== '') {
            $base64RawEmail.splice(5, 0, 'Cc: ' + cc + '\r\n');
          }
          if (bcc !== '') {
            $base64RawEmail.splice(5, 0, 'Bcc: ' + bcc + '\r\n');
          }

          $base64RawEmail = $base64RawEmail.join('');

        } else {

          $base64RawEmail = "From: " + from + "\nTo: " + to;
          if (cc !== '') {
            $base64RawEmail = $base64RawEmail + "\nCc: " + cc;
          }
          if (bcc !== '') {
            $base64RawEmail = $base64RawEmail + "\nBcc: " + bcc;
          }
          $base64RawEmail = $base64RawEmail + "\nSubject: " + subject + "\nContent-Type: text/html; charset=UTF-8\nContent-Transfer-Encoding: base64\n\n" + body;
        }

        $base64RawEmail = new Buffer($base64RawEmail).toString('base64');
        $base64RawEmail = $base64RawEmail.replace(/\//g, '_').replace(/\+/g, '-');

        var requestBody = {
          "raw": $base64RawEmail
        }

        GmailService.sendEmail(record.access_token, record.refresh_token, threadID, requestBody, req, res, function (data) {
          res.json(data);
        });

      } else if (record && record.provider === 'outlook') {


        var MailData = {};
        MailData.Subject = subject;
        MailData.Body = {};
        MailData.Body.ContentType = "HTML";
        MailData.Body.Content = body;

        MailData.ToRecipients = [];
        MailData.CcRecipients = [];
        MailData.BccRecipients = [];
        MailData.Importance = 'Normal';

        if (attachments && attachments.length) {
          MailData.HasAttachments = true;
          MailData.Attachments = [];
          attachments.forEach(function (val) {
            var attachment = {};
            attachment.Name = val.name;
            attachment.ContentBytes = val.url;
            attachment['@odata.type'] = "#Microsoft.OutlookServices.FileAttachment";
            MailData.Attachments.push(attachment);
          });
        } else {
          MailData.HasAttachments = false;
        }


        if (to && to.length) {
          if (to.indexOf(',') > -1 || to.indexOf(' ') > -1) {
            to = to.split(/[ ,]+/);
            to.forEach(function (val) {
              var toEmails = {};
              toEmails.EmailAddress = {};
              toEmails.EmailAddress.Address = val;
              if (validateEmail(val)) {
                MailData.ToRecipients.push(toEmails);
              }
            });
          } else {
            var toEmails = {};
            toEmails.EmailAddress = {};
            toEmails.EmailAddress.Address = to;
            if (validateEmail(to)) {
              MailData.ToRecipients.push(toEmails);
            }
          }
        }

        if (cc && cc.length) {
          if (cc.indexOf(',') > -1 || cc.indexOf(' ') > -1) {
            cc = cc.split(/[ ,]+/);
            cc.forEach(function (val) {
              var ccEmails = {};
              ccEmails.EmailAddress = {};
              ccEmails.EmailAddress.Address = val;
              if (validateEmail(val)) {
                MailData.CcRecipients.push(ccEmails);
              }
            });
          } else {
            var ccEmails = {};
            ccEmails.EmailAddress = {};
            ccEmails.EmailAddress.Address = cc;
            if (validateEmail(cc)) {
              MailData.CcRecipients.push(ccEmails);
            }
          }
        }

        if (bcc && bcc.length) {
          if (bcc.indexOf(',') > -1 || bcc.indexOf(' ') > -1) {
            bcc = bcc.split(/[ ,]+/);
            bcc.forEach(function (val) {
              var bccEmails = {};
              bccEmails.EmailAddress = {};
              bccEmails.EmailAddress.Address = val;
              if (validateEmail(val)) {
                MailData.BccRecipients.push(bccEmails);
              }
            });
          } else {
            var bccEmails = {};
            bccEmails.EmailAddress = {};
            bccEmails.EmailAddress.Address = bcc;
            if (validateEmail(bcc)) {
              MailData.BccRecipients.push(bccEmails);
            }
          }
        }

        OutlookService.sendEmail(record.access_token, record.refresh_token, MailData, req, res, function (data) {
          res.json(data);
        });


      } else {
        res.send({message: "Can not find the email address"});
      }
    });
  }
};


/**
 * Description
 * @method validateEmail
 * @param {} email
 * @return CallExpression
 */
function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

/**
 * Description
 * @method getOutlookEmails
 * @param {} token
 * @param {} email
 * @param {} folder
 * @param {} top
 * @param {} skip
 * @param {} filter
 * @param {} callback
 * @return
 */
function getOutlookEmails(token, email, folder, top, skip, filter, callback) {
  var queryParams = {
    // '$select': 'Subject,ReceivedDateTime,From,IsRead',
    '$orderby': 'ReceivedDateTime desc',
    '$top': top,
    '$skip': skip,
    '$count': 'true'
  };
  if (filter) {
    queryParams.$filter = filter;
  }
  // Set the API endpoint to use the v2.0 endpoint
  outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
  // Set the anchor mailbox to the user's SMTP address
  outlook.base.setAnchorMailbox(email);
  outlook.mail.getMessages({token: token, folderId: folder, odataParams: queryParams},
    function (error, result) {
      if (error) {
        console.log('getMessages returned an error: ' + error);
      }
      else if (result) {
        callback(result);
      }
    });
}
