/*register/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
const fs = require('fs');


module.exports = {
  /**
   * Description
   * @return
   * @return
   * @return
   * @method authenticate
   * @param {} req
   * @param {} res
   * @return
   */
  authenticate: function (req, res) {
    var email = req.param('email');
    var password = req.param('password');

    if (!email || !password) {
      return res.json(401, {err: 'username and password required'});
    }

    User.findOneByEmail(email, function (err, user) {
      if (!user) {
        return res.json(401, {err: 'invalid username or password'});
      }

      User.validPassword(password, user, function (err, valid) {
        if (err) {
          return res.json(403, {err: 'forbidden'});
        }

        if (!valid) {
          return res.json(401, {err: 'invalid username or password'});
        } else {

          var userData = {};
          userData.id = user.id;
          userData.email = user.email;
          userData.first_name = user.first_name;
          userData.last_name = user.last_name;
          userData.username = user.username;
          userData.mode = 'online';
          userData.status = user.status;
          if (user.photo_url) {
            userData.photo_url = user.photo_url;
          } else {
            userData.photo_url = null;
          }

          // console.log(req.session());
          req.session.authenticated = true;
          req.session.User = userData;
          sails.sockets.blast('user_mode', userData);
          let token = sailsTokenAuth.issueToken({sid: user.id});
          res.json({token: token});
        }
      });
    });
  },

  /**
   * Description
   * @return
   * @return
   * @return
   * @method lastUrl
   * @param {} req
   * @param {} res
   * @return
   */
  lastUrl: function (req, res) {
    let userId = req.session.User.id;
    let urlData = {module: req.param('module') || '', url: req.param('url') || '', title: req.param('title') || '', user: userId || undefined};
    Url.findOne({user: userId}).exec(function (err, foundUrl) {
      if( err ) console.log(err)
      if( foundUrl ){
        Url.update({user: userId}, urlData).exec(function (err, updatedUrl) {
          if( err ) console.log(err);
          res.send(200);
        });
      }else{
        Url.create(urlData).exec(function (err, createdUrl) {
          res.send(200);
        });
      }
    });
  },

  /**
   * Description
   * @return
   * @return
   * @return
   * @method authenticateFacebook
   * @param {} req
   * @param {} res
   * @return
   */
  authenticateFacebook: function(req, res){
    var userReqData = req.allParams();
    User.findOne({'email': userReqData.email}).exec(function (err, userRData) {
      if (userRData) {
        req.session.authenticated = true;
        req.session.User = userRData;
        var token = sailsTokenAuth.issueToken({sid: userRData.id});
        req.session.token = token;

        res.json({token: token});
      } else {
        var userData = {};
        var randomstring = Math.random().toString(36).slice(-8);
        userData.email = userReqData.email;
        userData.first_name = userReqData.first_name;
        userData.last_name = userReqData.last_name;
        userData.full_name = userReqData.name;
        userData.emails = [{type: 'Home', address: userReqData.email}];
        userData.photo_url = userReqData.picture.data.url;
        userData.access_token = userReqData.token;
        userData.password = userReqData.first_name + randomstring;

        UserGroup.findOne({'name': 'Free'}).exec(function (err, data) {
          if (data) {

            //finding current set package for this user
            Package.findOne({basicPackage: 'true'}).exec(function (err, packageData) {

              userData.user_group = data.id;
              userData.package = packageData.id;

              //hashing the user's password using bycript function from User model
              User.hashPassword(userData.password, function (data) {

                userData.encryptedPassword = data.hash;

                User.create(userData).exec(function (err, data) {
                  let fileUrlData = {user: data.id, url: data.photo_url, module: 'profile'};
                  FilesService.downloadProfilePhoto(fileUrlData, req, res, function (profileResp) {
                    setTimeout(function () {
                      User.update({id: data.id}, {photo_url: profileResp}).exec(function (err, updatedUser) {
                        if(err) console.log(err);

                        req.session.authenticated = true;
                        req.session.User = updatedUser[0];
                        var token = sailsTokenAuth.issueToken({sid: data.id});
                        req.session.token = token;
                        // Change status to online
                        Contacts.update({email:data.email},{sochara_user_id: data.id}).exec(function afterwards(err, updated){
                          res.json({token: token});
                        });
                      });
                    }, 1000);
                  });

                });
              });
            });
          }
          if (!data) {
            return res.json(401, {err: 'Something went wrong!'});
          }
        });
      }
    });
  },

  /**
   * Description
   * @return
   * @return
   * @return
   * @method logincheck
   * @param {} req
   * @param {} res
   * @return
   */
  logincheck: function (req, res) {
    if ( req.session.authenticated === true ) {
      res.json({user_id: req.session.User.id});
    } else {
      res.json({user_id: 0});
    }
  },

  /**
   * Description
   * @return
   * @return
   * @return
   * @method googleLoginUri
   * @param {} req
   * @param {} res
   * @return
   */
  googleLoginUri: function (req, res) {
    res.send(req.googleLoginUri);
  },

  /**
   * Description
   * @return
   * @return
   * @method gmailAuthCallback
   * @param {} req
   * @param {} res
   * @return
   */
  gmailAuthCallback: function (req, res) {
    var code = req.param('code');
    if (code) {
      req.oauth2ClientLogin.getToken(code, function (err, pTokens) {
        if (err) {
          console.log(err);
          res.json({
            'err': err
          });
        } else {
          req.oauth2ClientLogin.setCredentials(pTokens);
          req.plus.people.get({userId: 'me', auth: req.oauth2ClientLogin}, function (err, profile) {
            User.findOne({'email': profile.emails[0].value}).exec(function (err, userRData) {
              if (userRData) {
                req.session.authenticated = true;
                req.session.User = userRData;
                var token = sailsTokenAuth.issueToken({sid: userRData.id});
                req.session.token = token;
                res.redirect('/setAtsToken?ats=' + token);
              } else {
                var userData = {};
                var randomstring = Math.random().toString(36).slice(-8);
                userData.email = profile.emails[0].value;
                userData.first_name = profile.name.givenName;
                userData.last_name = profile.name.familyName;
                userData.full_name = profile.displayName;
                userData.emails = profile.emails;
                userData.photo_url = profile.image.url;
                userData.access_token = pTokens.access_token;
                userData.refresh_token = pTokens.refresh_token;
                userData.password = profile.name.givenName + randomstring;

                UserGroup.findOne({'name': 'Free'}).exec(function (err, data) {
                  if (data) {

                    Package.findOne({basicPackage: 'true'}).exec(function (err, packageData) {

                      userData.user_group = data.id;
                      userData.package = packageData.id;

                      User.hashPassword(userData.password, function (data) {

                        userData.encryptedPassword = data.hash;

                        User.create(userData).exec(function (err, data) {

                          let fileUrlData = {user: data.id, url: data.photo_url, module: 'profile'};
                          FilesService.downloadProfilePhoto(fileUrlData, req, res, function (profileResp) {
                            setTimeout(function () {
                              User.update({id: data.id}, {photo_url: profileResp}).exec(function (err, updatedUser) {
                                if(err) console.log(err);
                                req.session.authenticated = true;
                                req.session.User = updatedUser[0];
                                let token = sailsTokenAuth.issueToken({sid: data.id});
                                req.session.token = token;
                                // Change status to online
                                Contacts.update({email:data.email},{sochara_user_id: data.id}).exec(function afterwards(err, updated){
                                  res.redirect('/setAtsToken?ats=' + token);
                                });
                              });
                            }, 1000);
                          });
                        });
                      });
                    });
                  }
                  if (!data) {
                    return res.json(401, {err: 'Something went wrong!'});
                  }
                });

              }
            });
          });
        }
      });
    }
  },



  /**
   * Description
   * @return
   * @method logout
   * @param {} req
   * @param {} res
   * @return
   */
  logout: function (req, res) {
    try {
      if( req.session && req.session.User ){
        let userId = req.session.User.id;
        User.update({id:userId},{mode:'offline'}).exec(function afterwards(err, updated){
          sails.sockets.blast('user_mode', updated[0]);
        });
      }
    } catch (e) {
      console.log("Error in logout: ", e);
    }
    req.session.destroy();
    res.json({'logout': 'done'});
  },

  /**
   * @log: last modified : 05 01 2017 12:28 AM by Mahdi
   * @note: no tasks pending
   * @return
   * @return
   * @description registering a user through some validations
   * @method register
   * @param req
   * @param res
   * @return
   */
  register: function (req, res) {
    //TODO: Do some validation on the input
    //Checking if the password and confirm password are same
    if (req.body.password !== req.body.confirmPassword) {
      return res.json(401, {err: 'Password doesn\'t match'});
    }

    //setting User Model's required fields
    var userData = {};
    userData.email = req.body.email;
    userData.first_name = req.body.first_name;
    userData.last_name = req.body.last_name;
    userData.password = req.body.password;

    UserGroup.findOne({'name': 'Free'}).exec(function (err, data) {
      if (err) {
        console.log(err);
      }
      if (data) {

        Package.findOne({basicPackage: 'true'}).exec(function (err, packageData) {
          if (err) {
            console.log(err);
            // return res.json(401, {err: 'Something went wrong!'});
          }
          userData.user_group = data.id;
          userData.package = packageData.id;

          User.findOne({email: userData.email}).exec(function (err, data) {
            if (data) {
              // return res.json(401, {err: 'Email Already Exist.'});
            } else {
              return true;
            }
          });

          //hashing the user's password using bycript function from User model
          User.hashPassword(userData.password, function (data) {
            if (data.err) {
              console.log(err);
              // return res.json(401, {err: 'Something went wrong!'});
            }
            userData.encryptedPassword = data.hash;


            User.create(userData).exec(function (err, data) {
              if (err) {
                console.log(err);
                // return res.json(401, {err: "Invalid data!"});
              }
              if (data) {
                var userId = data.id;
                var dir = 'assets/uploads/files/' + userId;
                if (!fs.existsSync(dir)) {
                  fs.mkdirSync(dir);
                }
                Contacts.update({email:data.email},{sochara_user_id: data.id}).exec(function afterwards(err, updated){
                  res.ok('success');
                });
                //checking if the path exists. If not exists then the path is being created

              }
            });
          });
        });
      }
      if (!data) {
        return res.json(401, {err: 'Something went wrong!'});
      }
    });
  }
};

