

module.exports = {
  /**
   * Description
   * @method joinSocket
   * @param {} req
   * @param {} res
   * @return
   */
  joinSocket: function (req, res) {
    let user = req.session.User ? req.session.User.id : '';
    if (!req.isSocket && !user) {
      return res.badRequest();
    }
    let roomName = 'sochara_' + user;
    try {
      sails.sockets.join(req, roomName, function(err) {
        if (err) console.log(err);
      });
    } catch(e) {
      console.log(e);
    }
    res.send(200);
  },
};
