/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

const Jimp = require("jimp");
const path = require('path');

module.exports = {
  /**
   * Description
   * @method create
   * @param {} req
   * @param {} res
   * @return 
   */
  create: function (req, res) {
    var data = req.param('data');
    data.user_id = req.session.User.id;
    Label.create(data).exec(function (err, cd) {
      if (err) {
        return res.serverError(err);
      }
      res.send({msg: 'success'});
    });
  },

  /**
   * Description
   * @method saveCurrentDesign
   * @param {} req
   * @param {} res
   * @return 
   */
  saveCurrentDesign: function (req, res) {
    let user = req.session.User.id;
    let labelData = req.allParams();
    delete labelData.color;
    var labelId = labelData.id || '';
    delete labelData.id;
    labelData.userInfo = {name: 'Name Here', address: 'City-1000, State, Country'};
    labelData.user = user;
    labelData.name = labelData.name || 'Untitled Design';
    if (labelId) {
      Label.update({id: labelId}, labelData).exec(function (err, updatedLabel) {
        if (err) console.log(err);
        res.send(updatedLabel[0]);
      });
    } else {
      Label.create(labelData).exec(function (err, createdLabel) {
        if (err) console.log(err);
        res.send(createdLabel);
      });
    }
  },
  /**
   * Description
   * @method read
   * @param {} req
   * @param {} res
   * @return 
   */
  read: function (req, res) {
    var userId = req.session.User.id;
    Label.find({user_id: userId}).exec(function (err, fd) {
      if (err) {
        return res.serverError(err);
      }
      res.send({msg: fd});
    });
  },
  /**
   * Description
   * @method update
   * @param {} req
   * @param {} res
   * @return 
   */
  update: function (req, res) {
    var data = req.param('data');
    Label.update({id: data.id}, data).exec(function (err, ud) {
      if (err) {
        return res.serverError(err);
      }
      res.send({msg: 'success'});
    });
  },
  /**
   * Description
   * @method delete
   * @param {} req
   * @param {} res
   * @return 
   */
  delete: function (req, res) {
    var labelId = req.param('id');
    Label.destroy({id: labelId}).exec(function (err) {
      if (err) console.log(err);
      res.send(200);
    });
  },
  /**
   * Description
   * @method toPdf
   * @param {} req
   * @param {} res
   * @return 
   */
  toPdf: function (req, res) {
    var pageCss = "html, body {\n" +
      "margin:0px;padding:0px;" +
      "      width: 210mm;\n" +
      "      height: 297mm; font-family: Roboto, 'Helvetica Neue', sans-serif\n" +
      "    }" +
      " .page .draggable input{\n" +
      "    margin-top:7px;\n" +
      "  }\n" +
      "  .page #resizableImage{\n" +
      "    margin-top:18px;\n" +
      "  } .labelUserName, .labelUserAddress{display: block}" +
      "th{text-align:left;}" +
      " .page {\n" +
      "      margin: 0;\n" +
      "      border: initial;\n" +
      "      border-radius: initial;\n" +
      "      width: initial;\n" +
      "      min-height: initial;\n" +
      "      box-shadow: initial;\n" +
      "      background: initial;\n" +
      "      page-break-after: always;\n" +
      "    }" +
      "  .page .tpl-1 {\n" +
      "    margin: 0.03in;\n" +
      "    margin-top: .2in;\n" +
      "    border: 1px solid gainsboro;\n" +
      "    background-size: 100% 100% !important;\n" +
      "    background-repeat: no-repeat !important;\n" +
      "    background-position: center center !important;\n" +
      "  }\n" +
      "\n" +
      "  .page .tpl-2 {\n" +
      "margin-right:.12in;" +
      "    margin-left: .12in;\n" +
      "    margin-top: .040in;\n" +
      "    border: 1px solid gainsboro;\n" +
      "    background-size: 100% 100% !important;\n" +
      "    background-repeat: no-repeat !important;\n" +
      "    background-position: center center !important;\n" +
      "  }\n" +
      "\n" +
      "  .page .tpl-3 {\n" +
      "    margin-left: 0.045in;\n" +
      "    margin-top: .085in;\n" +
      "    border: 1px solid gainsboro;\n" +
      "    background-size: 100% 100% !important;\n" +
      "    background-repeat: no-repeat !important;\n" +
      "    background-position: center center !important;\n" +
      "  }\n" +
      "\n" +
      "  .page .tpl-4 {\n" +
      "    margin-left: .035in;\n" +
      "    margin-top: 30px;\n" +
      "    border: 1px solid gainsboro;\n" +
      "    background-size: 100% 100% !important;\n" +
      "    background-repeat: no-repeat !important;\n" +
      "    background-position: center center !important;\n" +
      "  }\n" +
      "\n" + "#insertedText{border:none;}" + ".draggable{width:max-content;border:none;background:transparent;}.draggable input{border:none;background:none;}" +
      ".resizable_image {\n" +
      "    height: 80px;\n" +
      "    width: 80px;\n" +
      "    background-size: 100% 100% !important;\n" +
      "    background-repeat: no-repeat !important;\n" +
      "    background-position: center center !important; margin-top: 12px;\n" +
      "  }" +
      "  .page .tpl-5 {\n" +
      "    margin: 30px;\n" +
      "    border: 1px solid gainsboro;\n" +
      "    background-size: 100% 100% !important;\n" +
      "    background-repeat: no-repeat !important;\n" +
      "    background-position: center center !important;\n" +
      "  }" + ".tpl-1 {\n" +
      "    width: 4in !important;\n" +
      "    min-width: 4in !important;\n" +
      "    height: 2in !important;\n" +
      "    min-height: 2in !important;\n" +
      "  }\n" +
      "\n" +
      "  .tpl-2 {\n" +
      "    width: 1.75in !important;\n" +
      "    min-width: 1.75in !important;\n" +
      "    height: 0.667in !important;\n" +
      "    min-height: 0.667in !important;\n" +
      "  }\n" +
      "\n" +
      "  .ta-bind {\n" +
      "    background-size: 100% 100% !important;\n" +
      "    background-repeat: no-repeat !important;\n" +
      "    background-position: center center !important; font-size; 20px\n" +
      "  }\n" +
      "\n" +
      "  .tpl-3 {\n" +
      "    width: 2.625in !important;\n" +
      "    min-width: 2.625in !important;\n" +
      "    height: 1in !important;\n" +
      "    min-height: 1in !important;\n" +
      "  }\n" +
      "\n" +
      "  .tpl-4 {\n" +
      "    width: 4in !important;\n" +
      "    min-width: 4in !important;\n" +
      "    height: 3.333in !important;\n" +
      "    min-height: 3.333in !important;\n" +
      "  }\n" +
      "\n" +
      "  .tpl-5 {\n" +
      "    width: 6in !important;\n" +
      "    min-width: 6in !important;\n" +
      "    height: 4in !important;\n" +
      "    min-height: 4in !important;\n" +
      "  }";

    var addressType = req.param('addressType');
    var layout = req.param('layout');
    var html = JSON.parse(req.param('html'));
    // var labelRadius= req.param(data.labelRadius);
    // var labelBGColor= req.param(data.labelBGColor);
    // var labelBGImage= req.param(data.labelBGImage);
    var editorCSS = JSON.parse(req.param('editorCSS'));
    var editorClass = JSON.parse(req.param('editorClass'));
    var amountOfPrintPage = req.param('amountOfReturnAddress');
    var singlePageRow = layout.printRow;
    var singlePageColumn = layout.printCol;
    var backgroundImageData = req.param('backgroundImageData');
    var pageContent = '';
    var singlePageContentStart, singlePageContentEnd, allRows;
    if (addressType === 'shipping') {
      for (var i = 0; i < html.length; i++) {
        singlePageContentStart = "<div class='page'>";
        singlePageContentEnd = "</div>";
        allRows = '';
        for (var j = 0; j < html[i].length; j++) {
          allRows = allRows + '<tr>'
          var allCol = '';
          for (var k = 0; k < html[i][j].length; k++) {
            console.log(html[i][j])
            allCol = allCol + '<th><div class="' + editorClass + '" style="' + editorCSS + 'background-image:url(' + backgroundImageData + ')">' + html[i][j][k] + '</div></th>';
          }
          allRows = allRows + allCol + '</tr>';
        }
        pageContent = pageContent + singlePageContentStart + '<table>' + allRows + '</table>' + singlePageContentEnd;
      }
    }
    else {
      for (var i = 0; i < amountOfPrintPage; i++) {
        singlePageContentStart = "<div class='page'>";
        singlePageContentEnd = "</div>";
        allRows = '';
        for (var j = 0; j < singlePageRow; j++) {
          allRows = allRows + '<tr>'
          var allCol = '';
          for (var k = 0; k < singlePageColumn; k++) {
            allCol = allCol + '<th><div class="' + editorClass + '" style="' + editorCSS + 'background-image:url(' + backgroundImageData + ')">' + html + '</div></th>';
          }
          allRows = allRows + allCol + '</tr>';
        }
        pageContent = pageContent + singlePageContentStart + '<table>' + allRows + '</table>' + singlePageContentEnd;
      }
    }
    var fs = require('fs');

    fs.writeFile("assets/how.html", "<html><head><style>" + pageCss + "</style></head><body>" + pageContent + "</body></html>", function (err) {
      if (err) {
        return console.log(err);
      }

      console.log("The file was saved!");
    });
    var conversion = require("phantom-html-to-pdf")({
      phantomPath: require("phantomjs-prebuilt").path
    });
    conversion({
      html: "<html><head><style>" + pageCss + "</style></head><body>" + pageContent + "</body></html>",
      paperSize: {
        format: 'A4', orientation: 'portrait', margin: '0px', width: '8.27in',
        height: '11.69in', headerHeight: '0px', footerHeight: '0px'
      }
    }, function (err, pdf) {
      if (err) {
        return res.serverError('failed');
      }
      var output = fs.createWriteStream('assets/uploads/files/' + req.session.User.id + '/LabelPDF.pdf', {flags: 'w'});
      console.log(pdf.logs);
      console.log(pdf.numberOfPages);
      // since pdf.stream is a node.js stream you can use it
      // to save the pdf to a file (like in this example) or to
      // respond an http request.
      pdf.stream.pipe(output);
      res.send('uploads/files/' + req.session.User.id + '/LabelPDF.pdf');
    });
  },

  /**
   * Description
   * @method saveLabelPreview
   * @param {} req
   * @param {} res
   * @return 
   */
  saveLabelPreview: function (req, res) {
    var user = req.session.User.id;
    var saveToServerName;
    var randNum = new Date().getTime();
    req.file('file').upload({
      dirname: path.resolve(sails.config.appPath) + '/assets/uploads/files/' + user + '/label',

      /**
       * Description
       * @method saveAs
       * @param {} __newFileStream
       * @param {} cb
       * @return 
       */
      saveAs: function (__newFileStream, cb) {
        var fname = __newFileStream.filename;
        var mName = fname.substr(0, fname.lastIndexOf('.'));
        var fName = mName.replace(/\s\s+/g, ' ');
        fName = fName.replace(/ /g, "-");
        var ext = path.extname(__newFileStream.filename);
        saveToServerName = fName + "_" + randNum + ext;
        cb(null, saveToServerName);
      },

      maxBytes: 10000000
    }, function whenDone(err, uploadedFiles) {

      if (err) {
        return res.negotiate(err);
      }

      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0) {
        return res.badRequest('No file was uploaded');
      } else {
        // var baseURL = Utility.getSiteBaseURL(req);


        Jimp.read(uploadedFiles[0].fd, function (err, image) {
          if (err) throw err;
          var extraLen;
          var img_w = parseInt(image.bitmap.width);
          var img_h = parseInt(image.bitmap.height);


          if (img_w > img_h) {
            extraLen = (img_w - img_h) / 2;
            image.crop(Math.floor(extraLen), 0, img_h, img_h);
          } else {
            extraLen = (img_h - img_w) / 2;
            image.crop(0, Math.floor(extraLen), img_w, img_w);
          }

          var fileNameWithExt = path.basename(uploadedFiles[0].fd);
          var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));

          image.resize(250, Jimp.AUTO)
            .quality(60)
            .write('assets/uploads/files/' + user + '/label/thumb/' + uFileName + '.jpg'); // save
        }).catch(function (err) {
          console.log(err);
        });

        var fileNameWithExt = path.basename(uploadedFiles[0].fd);
        var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));
        var genThumUrl = path.dirname(uploadedFiles[0].fd) + '/thumb/' + uFileName + '.jpg';
        var thumb_dir = '/uploads/files/' + user + '/label/thumb/' + uFileName + '.jpg';
        var file_dir = '/uploads/files/' + user + '/label/' + path.basename(uploadedFiles[0].fd);

        var files = {};
        files.name = uploadedFiles[0].filename;
        files.ext = path.extname(uploadedFiles[0].filename);
        files.type = 'label';
        files.owner = user;
        files.file_dir = file_dir;
        files.fileFD = uploadedFiles[0].fd;
        files.fileThumb = genThumUrl;
        files.thumb_dir = thumb_dir;
        files.size = uploadedFiles[0].size;
        let labelId = req.param('label');
        Label.update({id: labelId}, {preview: file_dir}).exec(function (err, respUpdate) {
          if (err) console.log(err);
          res.send(file_dir);
        });
      }
    });
  },

  /**
   * Description
   * @method loadExistingLabel
   * @param {} req
   * @param {} res
   * @return 
   */
  loadExistingLabel: function (req, res) {
    let user = req.session.User.id;
    Label.find({user: user}).exec(function (err, respLabel) {
      if (err) console.log(err);
      res.send(respLabel);
    });
  },

  /**
   * Description
   * @method loadSavedLabel
   * @param {} req
   * @param {} res
   * @return 
   */
  loadSavedLabel: function (req, res) {
    let id = req.param('id');
    Label.findOne({id: id}).exec(function (err, respLabel) {
      if (err) console.log(err);
      res.send(respLabel);
    });
  }
};

