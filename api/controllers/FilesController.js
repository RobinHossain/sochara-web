var Jimp = require("jimp");
var fs = require('fs');
var path = require('path');
var moment = require('moment');
var AsyncCall = require('async');
var archiver = require('archiver');
var rimraf = require('rimraf');
const request = require('request');

module.exports = {

  /**
   * Get Folder Files
   * @return
   * @method getFolderFiles
   * @param req
   * @param res
   * @return 
   */
  getFolderFiles: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var folderId = req.param('id');
    UserFiles.find({
      type: 'file',
      folder: folderId,
      select: ['_id', 'name', 'file_dir', 'ext', 'updatedAt']
    }).exec(function foundCB(err, records) {
      // res.send(records)

      var totalRecords = [];
      if (records) {
        AsyncCall.forEach(records, function (item, callback) {
          SharedFiles.findOne({file_id: item.id, owner: LoggedInUserID}).exec(function countCB(error, found) {

            if (found && found.users.length) {
              item.shared = found.users.length;
            }

            totalRecords.push(item);
            callback();

          });

        }, function (err) {
          if (err) {
            console.log(err);
          } else {
            res.send(totalRecords);
          }
        });

      }

    });
  },

  /**
   * Description
   * @method createNewFolder
   * @param {} req
   * @param {} res
   * @return 
   */
  createNewFolder: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var folder = req.param('name') || '';
    var root = req.param('root') || 0;
    if (folder) {
      var data = {};
      data.name = folder;
      data.root = root;
      data.owner = LoggedInUserID;
      data.type = 'folder';
      FilesFolder.create(data).exec(function (err, record) {
        if (err) {
          return res.serverError(err);
        }
        res.send(record);
      });
    }
  },

  /**
   * Get Previous Shared User for a file
   * @return
   * @method getPreviousShareWithUserFile
   * @param req
   * @param res
   * @return 
   */
  getPreviousShareWithUserFile: function (req, res) {
    var user_id = req.session.User.id;
    SharedFiles.findOne({user_id: user_id, file_id: req.param('id')}).exec(function (err, resp) {
      if (err) {
        return res.serverError(err);
      }
      var allUsers = [];
      if (resp) {

        var AsyncCall = require('async');
        AsyncCall.each(resp.users, function (user, cb) {
          User.findOne(user)
            .then(function (responce) {
              var users = {};
              users.id = responce.id;
              users.name = responce.first_name + ' ' + responce.last_name;
              allUsers.push(users);
              cb();
            })
            .fail(function (error) {
              cb(error);
            });
        }, function (error) {
          if (error) {
            console.log(error);
          }
          return res.json(allUsers);
        });
      } else {
        res.json(allUsers);
      }
    });
  },

  /**
   * Description
   * @method getSharedItemList
   * @param {} req
   * @param {} res
   * @return 
   */
  getSharedItemList: function (req, res) {
    var loggedInUserId = req.session.User.id;
    let items = {};
    AsyncCall.parallel([
        function(callback) {
          SharedFiles.find({
            folder: {$ne: null},
            users: loggedInUserId
          }).populate('folder', {select: ['_id', 'name', 'updatedAt']}).exec(function (err, resp) {
            items.folders = resp.map((file)=>{
              return file.folder;
            });
            callback();
          });
        },
        function(callback) {
          SharedFiles.find({
            file: {$ne: null},
            users: loggedInUserId
          }).populate('file', {select: ['_id', 'name', 'file_dir', 'ext', 'fileThumb', 'type', 'size', 'owner', 'updatedAt']}).exec(function (err, fileResp) {
            items.files = fileResp.map((file)=>{
              return file.file;
            });
            callback();
          });
        }
      ],
      function(err, results) {
        res.send(items);
      });
  },

  /**
   * Description
   * @method itemsShareWithUsers
   * @param {} req
   * @param {} res
   * @return 
   */
  itemsShareWithUsers: function (req, res) {
    let LoggedInUserID = req.session.User.id;
    let shareData = req.allParams();
    let items = shareData.items;
    let where = {}, sharedData;

    AsyncCall.each(items, function(item, callback) {
      if( item.type ==='folder'){
        where.folder = item.id;
      }else if(item.type === 'file'){
        where.file = item.id;
      }
      SharedFiles.findOne(where).exec(function (err, resp) {
        if (err) console.log(err);
        if (resp) {
          SharedFiles.update(where, {users: shareData.users}, function updatedCB(err) {
            if (err) console.log(err);
            callback();
          });
        } else {
          sharedData = {owner: LoggedInUserID};
          if( item.type ==='folder'){
            sharedData.folder = item.id;
          }else if(item.type === 'file'){
            sharedData.file = item.id;
          }
          sharedData.users = shareData.users;
          SharedFiles.create(sharedData).exec(function (err, created) {
            if (err) { console.log(err); }
            callback();
          });
        }
      });
    }, function(err) {
      if( err ) {
        console.log(err);
      } else {
        res.send(200);
      }
    });
  },

  /**
   * Description
   * @method getPreviousSharedUsers
   * @param {} req
   * @param {} res
   * @return 
   */
  getPreviousSharedUsers: function (req, res) {
    let fileData = req.allParams();
    let userId = req.session.User.id;
    let where = {};
    if( fileData.type === 'folder'){
      where.folder = fileData.id;
    }else if( fileData.type === 'file'){
      where.file = fileData.id;
    }
    SharedFiles.findOne(where).exec(function (err, resp) {
      console.log(resp);
      if (err) { console.log(err); }
      if (resp && resp.users ) {
        User.find({
          id: resp.users,
          select: ['_id', 'first_name', 'last_name', 'email', 'updatedAt', 'photo_url']
        }).exec(function (err, respContact) {
          if (err) {
            return res.serverError(err);
          }
          let contactData = respContact.map(contact=>{
            return {value: contact.first_name.toLowerCase(), name: contact.first_name + ' ' + contact.last_name,
              email: respContact.email, image: respContact.photo_url || 'images/avatars/blank.jpg', id: contact.id};
          });
          res.send(contactData);
        });
      } else {
        res.send([]);
      }
    });
  },

  /**
   * Get Shared Files
   * @return
   * @method getShareWithUser
   * @param req
   * @param res
   * @return 
   */
  getShareWithUser: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    if (req.param('type') === 'file') {
      SharedFiles.findOne({file_id: req.param('id')}).exec(function (err, resp) {
        if (err) {
          return res.serverError(err);
        }
        if (resp) {
          var prevRes = resp;
          if (prevRes.users) {

            var getUserIndex = prevRes.users.indexOf(req.param('uid'));
            if (getUserIndex == -1) {
              prevRes.users.push(req.param('uid'));
            }

          } else {
            prevRes.users = [req.param('uid')];
          }

          SharedFiles.update({file_id: req.param('id')}, {users: prevRes.users}, function updatedCB(err) {
            if (err) {
            }
            res.send('updated');
          })
        } else {
          var files = {};
          files.user_id = LoggedInUserID;
          files.user = LoggedInUserID;
          files.file_id = req.param('id');
          files.file = req.param('id');
          files.users = [req.param('uid')];
          SharedFiles.create(files, function fileCreated(err) {
            if (err) {
              console.log(err)
            }
            res.send('created');
          })
        }
      });
    } else if (req.param('type') === 'folder') {
      SharedFiles.findOne({folder_id: req.param('id')}).exec(function (err, resp) {
        if (err) {
          return res.serverError(err);
        }
        if (resp) {
          var prevRes = resp;
          if (prevRes.users) {

            var getUserIndex = prevRes.users.indexOf(req.param('uid'));
            if (getUserIndex == -1) {
              prevRes.users.push(req.param('uid'));
            }

          } else {
            prevRes.users = [req.param('uid')];
          }

          SharedFiles.update({folder_id: req.param('id')}, {users: prevRes.users}, function updatedCB(err) {
            if (err) {
            }
            res.send('updated');
          })
        } else {
          var files = {};
          files.user_id = LoggedInUserID;
          files.user = LoggedInUserID;
          files.folder_id = req.param('id');
          files.folder = req.param('id');
          files.users = [req.param('uid')];
          SharedFiles.create(files, function fileCreated(err) {
            if (err) {
              console.log(err)
            }
            res.send('created');
          })
        }
      });
    } else if (req.param('type') === 'media') {
      SharedMedia.findOne({media_id: req.param('id')}).exec(function (err, resp) {
        if (err) {
          return res.serverError(err);
        }
        if (resp) {
          var prevRes = resp;
          if (prevRes.users) {

            var getUserIndex = prevRes.users.indexOf(req.param('uid'));
            if (getUserIndex == -1) {
              prevRes.users.push(req.param('uid'));
            }

          } else {
            prevRes.users = [req.param('uid')];
          }

          SharedMedia.update({media_id: req.param('id')}, {users: prevRes.users}, function updatedCB(err) {
            if (err) {
            }
            res.send('updated');
          })
        } else {
          var files = {};
          files.user_id = LoggedInUserID;
          files.user = LoggedInUserID;
          files.media_id = req.param('id');
          files.file = req.param('id');
          files.users = [req.param('uid')];
          SharedMedia.create(files, function fileCreated(err) {
            if (err) {
              console.log(err);
            }
            res.send('created');
          });
        }
      });
    } else if (req.param('type') === 'album') {
      SharedMedia.findOne({album_id: req.param('id')}).exec(function (err, resp) {
        if (err) {
          return res.serverError(err);
        }
        if (resp) {
          var prevRes = resp;
          if (prevRes.users) {

            var getUserIndex = prevRes.users.indexOf(req.param('uid'));
            if (getUserIndex == -1) {
              prevRes.users.push(req.param('uid'));
            }

          } else {
            prevRes.users = [req.param('uid')];
          }

          SharedMedia.update({album_id: req.param('id')}, {users: prevRes.users}, function updatedCB(err) {
            if (err) {
            }
            res.send('updated');
          })
        } else {
          var files = {};
          files.user_id = LoggedInUserID;
          files.user = LoggedInUserID;
          files.album_id = req.param('id');
          files.album = req.param('id');
          files.users = [req.param('uid')];
          SharedMedia.create(files, function fileCreated(err) {
            if (err) {
              console.log(err);
            }
            res.send('created');
          });
        }
      });
    }

  },

  /**
   * Description
   * @method getContacts
   * @param {} req
   * @param {} res
   * @return 
   */
  getContacts: function (req, res) {
    Contacts.find({
      user_id: req.session.User.id,
      select: ['_id', 'first_name', 'last_name', 'email', 'updatedAt']
    }).exec(function (err, resContactData) {
      if (err) {
        return res.serverError(err);
      }
      res.send(resContactData);
    });
  },

  /**
   * Get All Folder list
   * @return
   * @method getAllFolderList
   * @param req
   * @param res
   * @return 
   */
  getAllFolderList: function (req, res) {

    var LoggedInUserID = req.session.User.id;
    var where = {};
    let paramData = req.allParams();

    where.owner = LoggedInUserID;
    if (req.param('folder')) {
      where.root = req.param('folder');
    } else {
      where.root = '0';
    }
    if (paramData.starred) {
      where.starred = true;
    }
    where.select = ['_id', 'name', 'root', 'owner', 'type', 'updatedAt', 'createdAt'];
    var folderQuery = FilesFolder.find(where);

    if (paramData.recent) {
      folderQuery.sort({updatedAt: -1});
      folderQuery.limit(15);
    } else {
      folderQuery.sort({createdAt: -1});
    }
    folderQuery.populate('owner', {select: ['_id', 'first_name', 'last_name']});
    folderQuery.exec(function foundCB(err, records) {
      if (err) return next(err);
      // res.json(records);

      var totalRecords = [];
      if (records) {
        AsyncCall.forEach(records, function (item, callback) {
          UserFiles.count({folder: item.id}).exec(function countCB(error, found) {

            if (found) {
              item.files = found;
            }

            FilesFolder.count({root: item.id}).exec(function countCB(error, foundFolder) {
              if (foundFolder) {
                item.folders = foundFolder;
              }

              UserFiles.findOne({
                folder: item.id,
                select: ['updatedAt']
              }).sort({updatedAt: -1}).exec(function (err, record) {

                if (record) {
                  item.updatedAt = moment(record.updatedAt).format('MM/DD/YYYY');
                }

                SharedFiles.findOne({folder_id: item.id, owner: LoggedInUserID}).exec(function countCB(error, found) {

                  if (found && found.users.length) {
                    item.shared = found.users.length;
                  }
                  item.type = 'folder';
                  totalRecords.push(item);
                  callback();

                });

              });

            });

          });

        }, function (err) {
          if (err) {
            console.log(err);
          } else {
            res.send(totalRecords);
          }
        });

      }

    });
  },

  /**
   * Get Sub Folder List
   * @return
   * @method getSubFolderList
   * @param req
   * @param res
   * @return 
   */
  getSubFolderList: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    FilesFolder.find({
      root: req.param('id'),
      owner: LoggedInUserID,
      select: ['_id', 'name']
    }).exec(function foundCB(err, records) {
      if (err) return next(err);
      // res.json(records);

      var totalRecords = [];
      if (records) {
        AsyncCall.forEach(records, function (item, callback) {
          UserFiles.count({folder: item.id}).exec(function countCB(error, found) {

            if (found) {
              item.files = found;
            }

            FilesFolder.count({root: item.id}).exec(function countCB(error, foundFolder) {
              if (foundFolder) {
                item.folders = foundFolder;
              }

              UserFiles.findOne({
                folder: item.id,
                select: ['updatedAt']
              }).sort({updatedAt: -1}).exec(function (err, record) {

                if (record) {
                  item.last = moment(record.updatedAt).format('MM/DD/YYYY');
                }

                SharedFiles.findOne({folder_id: item.id, owner: LoggedInUserID}).exec(function countCB(error, found) {

                  if (found && found.users.length) {
                    item.shared = found.users.length;
                  }

                  totalRecords.push(item);
                  callback();

                });

              });

            });

          });

        }, function (err) {
          if (err) {
            console.log(err);
          } else {
            res.send(totalRecords);
          }
        });

      }

    });
  },

  /**
   * Get All file list for an album
   * @return
   * @method getAlbumFileList
   * @param req
   * @param res
   * @return 
   */
  getAlbumFileList: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    UserFiles.find({
      type: 'media',
      album: req.param('id'),
      select: ['_id', 'name', 'file_dir', 'ext', 'updatedAt']
    }).exec(function foundCB(err, records) {

      var totalRecords = [];
      if (records) {
        AsyncCall.forEach(records, function (item, callback) {
          SharedMedia.findOne({media_id: item.id, owner: LoggedInUserID}).exec(function countCB(error, found) {

            if (found && found.users.length) {
              item.shared = found.users.length;
            }

            totalRecords.push(item);
            callback();

          });

        }, function (err) {
          if (err) {
            console.log(err);
          } else {
            res.send(totalRecords);
          }
        });

      }
    });
  },

  /**
   * Description
   * @method deletFiles
   * @param {} req
   * @param {} res
   * @return 
   */
  deletFiles: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var files = req.param('files');
    if (files && files.length) {

      UserFiles.find({id: files}).exec(function (err, records) {
        AsyncCall.forEach(records, function (item, callback) {
          // For Main File
          if( item.id ){
            var fileName = path.basename(item.file_dir);
            var fileDir = 'assets/uploads/files/' + LoggedInUserID + '/file/' + fileName;

            rimraf(fileDir, function () {
              UserFiles.destroy({id: item.id}).exec(function (err) {
                callback();
              });
            });
          }else{
            callback();
          }
        }, function (err) {
          if (err) {
            console.log(err);
          } else {
            res.send('Files are deleted');
          }
        });
      });
    }
  },

  /**
   * Description
   * @method deleteEntireFolder
   * @param {} req
   * @param {} res
   * @return 
   */
  deleteEntireFolder: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var folders = req.param('folders');
    if (folders && folders.length) {
      FilesFolder.find({id: folders}).exec(function (err, records) {
        if (records && records.length) {
          AsyncCall.forEach(records, function (record, callback) {
            if (record) {
              FilesFolder.destroy({id: record.id, owner: LoggedInUserID}, function destroyedCB(err) {
                if (err) return next(err);
                sails.log('Folder Deleted');
                var folderAvailable = record.id;
                AsyncCall.whilst(
                  function () {
                    return folderAvailable && folderAvailable.length;
                  },
                  function (callback) {
                    FilesFolder.findOne({root: folderAvailable}, function destroyedCB(err, frecord) {
                      if (frecord) {
                        folderAvailable = frecord.id;
                        FilesFolder.destroy({id: frecord.id}, function destroyedCB(err) {
                          callback();
                        });
                      } else {
                        folderAvailable = 0;
                        callback();
                      }
                    });
                  },
                  function (err, n) {
                    UserFiles.find({
                      type: 'file',
                      folder: record.id,
                      owner: LoggedInUserID,
                      select: ['_id', 'file_dir']
                    }).exec(function foundCB(err, records) {

                      if (records) {
                        AsyncCall.forEach(records, function (item, callback) {
                          // For Main File
                          var fileName = path.basename(item.file_dir);
                          var fileDir = 'assets/uploads/files/' + LoggedInUserID + '/file/' + fileName;

                          rimraf(fileDir, function () {
                            sails.log('files of folder deleted');
                          });


                        }, function (err) {
                          if (err) {
                            console.log(err);
                          } else {

                          }
                        });

                        UserFiles.destroy({
                          folder: record.id,
                          owner: LoggedInUserID
                        }, function destroyedCB(err) {
                          if (err) {
                            console.log(err);
                          }
                          SharedFiles.findOne({folder_id: record.id}, function foundCB(err, record) {
                            if (record) {
                              SharedFiles.destroy({folder_id: record.id}, function destroyedCB(err) {
                                if (err) console.log(err);
                                res.json('deleted');
                              });
                            } else {
                              res.json('deleted');
                            }
                          });

                        });
                      }
                    });
                  }
                );
              });
            }


          }, function (err) {
            if (err) {
              console.log(err);
            } else {

            }
          });
        }
      });
    } else {
      res.send('no Folder Found');
    }
  },

  /**
   * Description
   * @method getFiles
   * @param {} req
   * @param {} res
   * @return 
   */
  getFiles: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var thumbDir;
    var fileIds = req.param('files');
    var archive = archiver('zip', {
      // store: true // Sets the compression method to STORE.
    });

    // res.writeHead(200, {
    //   'Content-Type': 'application/zip',
    //   'Content-disposition': 'attachment; filename=files.zip'
    // });

    thumbDir = 'assets/uploads/files/' + LoggedInUserID + '/file/temp';
    if (!fs.existsSync(thumbDir)) {
      fs.mkdirSync(thumbDir);
    }
    var randNum = new Date().getTime();

    var FinalDirectory = 'files_' + randNum + '.zip';
    // var baseURL = Utility.getSiteBaseURL(req);
    var output = fs.createWriteStream(thumbDir + '/' + FinalDirectory);


    // good practice to catch this error explicitly
    archive.on('error', function (err) {
      console.log(err);
    });

    // pipe archive data to the file
    // archive.pipe(res);
    archive.pipe(output);

    UserFiles.find({id: fileIds}).exec(function foundCB(err, records) {
      if (records) {
        AsyncCall.forEach(records, function (item, callback) {
          // For Main File
          var fileName = path.basename(item.file_dir);
          var fileNameOri = item.name;
          var fileDir = require('path').resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserID + '/file/' + fileName;

          // var file1 = __dirname + '/file1.txt';
          archive.append(fs.createReadStream(fileDir), {name: fileNameOri});
          callback();

        }, function (err) {
          if (err) {
            console.log(err);
          }
          archive.finalize();
          // res.send(FinalDirectory);
        });

        setTimeout(function () {
          res.send(sails.config.appUrl + '/uploads/files/' + LoggedInUserID + '/file/temp/' + FinalDirectory);
        }, 1000);


      }

    });
  },


  /**
   * Description
   * @method getFolderFilesArchive
   * @param {} req
   * @param {} res
   * @return 
   */
  getFolderFilesArchive: function (req, res) {
    var LoggedInUserID = req.session.User.id;

    FilesFolder.findOne({id: req.param('id'), select: ['name']}).exec(function (err, record) {
      if (record) {
        var folderName = record.name;
        var fName = folderName.replace(/\s\s+/g, ' ');
        folderName = fName.replace(/ /g, "-");
        var archive = archiver('zip', {
          // store: true // Sets the compression method to STORE.
        });

        res.writeHead(200, {
          'Content-Type': 'application/zip',
          'Content-disposition': 'attachment; filename=' + folderName + '.zip'
        });

        // good practice to catch this error explicitly
        archive.on('error', function (err) {
          console.log(err);
        });

        // pipe archive data to the file
        archive.pipe(res);

        UserFiles.find({
          type: 'file',
          owner: LoggedInUserID,
          folder: req.param('id'),
          select: ['_id', 'name', 'file_dir']
        }).exec(function foundCB(err, records) {

          if (records) {
            AsyncCall.forEach(records, function (item, callback) {


              // For Main File
              var fileName = path.basename(item.file_dir);
              var fileNameOri = item.name;
              var fileDir = require('path').resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserID + '/file/' + fileName;

              // For Thumb
              var fileNameOnly = fileName.substr(0, fileName.lastIndexOf('.'));

              // var file1 = __dirname + '/file1.txt';
              archive.append(fs.createReadStream(fileDir), {name: fileNameOri});


            }, function (err) {
              if (err) {
                console.log(err);
              } else {

              }
            });

            // Task Here
            archive.finalize();

          }

        });
      }
    });
  },

  /**
   * Description
   * @method renameSelectedItem
   * @param {} req
   * @param {} res
   * @return 
   */
  renameSelectedItem: function (req, res) {
    var id = req.param('id');
    var type = req.param('type');
    if (type === 'folder') {
      FilesFolder.update({id: id}, {name: req.param('name') || 0}).exec(function afterwards(err, updated) {
        if (err) {
          console.log(err);
        }
        res.send(200);
      });
    } else if (type === 'file') {
      UserFiles.update({id: id}, {name: req.param('name') || 0}).exec(function afterwards(err, updated) {
        if (err) {
          console.log(err);
        }
        res.send(200);
      });
    }
  },

  /**
   * Description
   * @method AddToStar
   * @param {} req
   * @param {} res
   * @return 
   */
  AddToStar: function (req, res) {

    AsyncCall.eachSeries(req.param('items'), function (item, callback) {
      if (item.type === 'folder') {
        FilesFolder.findOne({id: item.id}).exec(function (err, record) {
          if (record) {
            FilesFolder.update({id: item.id}, {starred: !record.starred}).exec(function afterwards(err, updated) {
              if (err) console.log(err);
              callback();
            });
          }
        });

      } else if (item.type === 'file') {
        UserFiles.findOne({id: item.id}).exec(function (err, record) {
          if (record) {
            UserFiles.update({id: item.id}, {starred: !record.starred}).exec(function afterwards(err, updated) {
              if (err) console.log(err);
              callback();
            });
          }
        });
      }
    }, function (err) {
      if (err) {
        console.log(err);
      } else {
        res.send(200);
      }
    });
  },

  /**
   * Description
   * @method downloadFileFromUrl
   * @param {} req
   * @param {} res
   * @return 
   */
  downloadFileFromUrl: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var fileUrl = req.param('fileUrl');
    var folder = req.param('folder') || null;
    if (fileUrl) {
      var fileName = fileUrl.replace(/^.*[\\\/]/, '');
      var thumbDir = 'assets/uploads/files/' + LoggedInUserID + '/file';
      if (!fs.existsSync(thumbDir)) {
        fs.mkdirSync(thumbDir);
      }
      var randNum = new Date().getTime();


      // var baseURL = Utility.getSiteBaseURL(req);
      var mName = fileName.substr(0, fileName.lastIndexOf('.'));
      var fName = mName.replace(/\s\s+/g, ' ');
      fName = fName.replace(/ /g, "-");
      var ext = path.extname(fileName);
      var FinalDName = fName + '_' + randNum + ext;
      var file = fs.createWriteStream(thumbDir + '/' + FinalDName);


      request(fileUrl).pipe(file);
      setTimeout(function () {
        const fileStats = fs.statSync(thumbDir + '/' + FinalDName);
        var fileData = {};
        fileData.name = FinalDName;
        fileData.type = 'file';
        fileData.owner = LoggedInUserID;
        fileData.file_dir = '/uploads/files/' + LoggedInUserID + '/file/' + FinalDName;
        fileData.ext = ext;
        fileData.size = fileStats.size;
        fileData.folder = folder || '0';
        UserFiles.create(fileData, function fileCreated(err, createdFile) {
          if (err) {
            console.log(err);
          }
          if (createdFile) {
            res.send(200);
          }
        });
      }, 1000);
    }
  },

  /**
   * Description
   * @method pasteSelectedItems
   * @param {} req
   * @param {} res
   * @return 
   */
  pasteSelectedItems: function (req, res) {
    AsyncCall.eachSeries(req.param('pasteItems'), function (item, callback) {
      if (item.type === 'folder') {
        FilesFolder.update({id: item.id}, {root: req.param('folder') || 0}).exec(function afterwards(err, updated) {
          if (err) {
            console.log(err);
          }
          console.log(updated[0]);
          callback();
        });
      } else if (item.type === 'file') {

        // test
        UserFiles.findOne({id: item.id}).exec(function (err, record) {
          if (record) {
            var fileData = {};
            fileData.folder = record.folder || [];

            if (record.folder) {
              if (req.param('pasteType') === 'cut' && req.param('pasteFrom')) {
                var getFromIndex = record.folder.indexOf(req.param('pasteFrom'));
                if (getFromIndex > -1) {
                  fileData.folder.splice(getFromIndex, 1);
                }
              }
              if (req.param('folder')) {
                if (fileData.folder.indexOf(req.param('folder') === -1)) {
                  fileData.folder.push(req.param('folder'));
                }
              } else {
                fileData.folder.push('0');
              }
            } else if (req.param('folder')) {
              fileData.folder = [req.param('folder')];
            } else {
              fileData.folder = ['0'];
            }

            UserFiles.update(item.id, fileData, function updatedCB(err, updated) {
              if (err) {
                console.log(err);
              }
              console.log(updated[0]);
              callback();
            });
          } else {
            callback();
          }

        });
        // test
      } else {
        callback();
      }

    }, function (err) {
      if (err) {
        console.log(err);
      } else {
        res.send(200);
      }
    });
  },


  /**
   * Get All File List
   * @return
   * @method getAllFileList
   * @param req
   * @param res
   * @return 
   */
  getAllFileList: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    let paramData = req.allParams();

    var where = {};
    where.type = 'file';
    where.owner = LoggedInUserID;
    if (req.param('folder')) {
      where.folder = req.param('folder');
    } else {
      where.folder = '0';
    }
    if (paramData.starred) {
      where.starred = true;
    }
    where.select = ['_id', 'name', 'file_dir', 'ext', 'fileThumb', 'type', 'size', 'owner', 'updatedAt'];
    var mailQuery = UserFiles.find(where);
    if (paramData.recent) {
      mailQuery.sort({updatedAt: -1});
      mailQuery.limit(15);
    } else {
      mailQuery.sort({createdAt: -1});
    }


    mailQuery.populate('owner', {select: ['_id', 'first_name', 'last_name']});
    mailQuery.exec(function foundCB(err, records) {
      // res.send(records);

      var totalRecords = [];
      if (records) {

        AsyncCall.forEach(records, function (item, callback) {
          SharedFiles.findOne({file: item.id, owner: LoggedInUserID}).exec(function countCB(error, found) {

            if (found && found.users.length) {
              item.shared = found.users.length;
            }

            totalRecords.push(item);
            callback();

          });

        }, function (err) {
          if (err) {
            console.log(err);
          } else {
            res.send(totalRecords);
          }
        });

      }

    });
  },

  /**
   * Description
   * @method getSharedFileList
   * @param {} req
   * @param {} res
   * @return 
   */
  getSharedFileList: function (req, res) {
    var LoggedInUserID = req.session.User.id;

    var where = {};
    where.type = 'file';
    where.owner = LoggedInUserID;
    if (req.param('folder')) {
      where.folder = req.param('folder');
    } else {
      where.folder = '0';
    }

    where.starred = true;
    where.select = ['_id', 'name', 'file_dir', 'ext', 'updatedAt', 'fileThumb', 'type', 'size'];
    var mailQuery = UserFiles.find(where);
    mailQuery.sort({createdAt: -1});

    mailQuery.exec(function foundCB(err, records) {
      // res.send(records);

      var totalRecords = [];
      if (records) {

        AsyncCall.forEach(records, function (item, callback) {
          SharedFiles.findOne({file_id: item.id, owner: LoggedInUserID}).exec(function countCB(error, found) {

            if (found && found.users.length) {
              item.shared = found.users.length;
            }
            totalRecords.push(item);
            callback();

          });

        }, function (err) {
          if (err) {
            console.log(err);
          } else {
            res.send(totalRecords);
          }
        });

      }

    });
  },

  /**
   * For uploading file
   * @return
   * @method uploadFilesToFolder
   * @param {} req
   * @param {} res
   * @return 
   */
  uploadFilesToFolder: function (req, res) {
    var LoggedInUserid = req.session.User.id;
    var random_date = new Date();
    var randNum = random_date.getTime();
    var thumbDir;
    var posterDir;

    // console.log(req.allParams());

    req.file('file').upload({
      dirname: require('path').resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserid + '/file',
      /**
       * Description
       * @return
       * @method saveAs
       * @param {} __newFileStream
       * @param {} cb
       * @return 
       */
      saveAs: function (__newFileStream, cb) {
        var fname = __newFileStream.filename;
        var mName = fname.substr(0, fname.lastIndexOf('.'));
        var fName = mName.replace(/\s\s+/g, ' ');
        fName = fName.replace(/ /g, "-");
        var ext = path.extname(__newFileStream.filename);
        cb(null, fName + "_" + randNum + ext);
      },
      // don't allow the total upload size to exceed ~10MB
      maxBytes: 900000000
    }, function whenDone(err, uploadedFiles) {


      if (err) {
        return res.negotiate(err);
      }

      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0) {
        return res.badRequest('No file was uploaded');
      } else {
        var fileType = uploadedFiles[0].type;
        var fileExtName = fileType.substr(0, fileType.lastIndexOf('/'));

        var baseURL = Utility.getSiteBaseURL(req);
        var fileUrl = require('util').format('%s/users/files/%s', baseURL, LoggedInUserid);
        var file_dir = '/uploads/files/' + LoggedInUserid + '/file/' + path.basename(uploadedFiles[0].fd);

        var genThumUrl;
        var genPosterUrl;

        if (fileExtName === 'image' || fileExtName === 'video') {

          var fileNameWithExt = path.basename(uploadedFiles[0].fd);
          var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));

          if (fileExtName === 'image') {
            thumbDir = 'assets/uploads/files/' + LoggedInUserid + '/file/thumb';
            if (!fs.existsSync(thumbDir)) {
              fs.mkdirSync(thumbDir);
            }

            Jimp.read(uploadedFiles[0].fd, function (err, image) {
              if (err) throw err;

              var extraLen;
              var img_w = parseInt(image.bitmap.width);
              var img_h = parseInt(image.bitmap.height);


              if (img_w > img_h) {
                extraLen = (img_w - img_h) / 2;
                image.crop(Math.floor(extraLen), 0, img_h, img_h);
              } else {
                extraLen = (img_h - img_w) / 2;
                image.crop(0, Math.floor(extraLen), img_w, img_w);
              }

              image.resize(220, Jimp.AUTO)
                .quality(60)
                .write('assets/uploads/files/' + LoggedInUserid + '/file/thumb/' + uFileName + '.jpg'); // save
            }).catch(function (err) {
              console.log(err);
            });


            genThumUrl = path.dirname(uploadedFiles[0].fd) + '/thumb/' + uFileName + '.jpg';

          } else if (fileExtName === 'video') {

            thumbDir = 'assets/uploads/files/' + LoggedInUserid + '/file/thumb';
            posterDir = 'assets/uploads/files/' + LoggedInUserid + '/file/poster';
            if (!fs.existsSync(thumbDir)) {
              fs.mkdirSync(thumbDir);
            }
            if (!fs.existsSync(posterDir)) {
              fs.mkdirSync(posterDir);
            }

            var fileDirectoryOnly = path.dirname(uploadedFiles[0].fd);

            const Thumbler = require('thumbler');

            Thumbler({
              type: 'video',
              input: uploadedFiles[0].fd,
              output: 'assets/uploads/files/' + LoggedInUserid + '/file/poster/' + uFileName + '.jpeg',
              time: '00:00:03'
            }, function (err, path) {
              if (err) return err;
              // return path;
              // console.log('done');
            });

            var tempPosterDir = 'assets/uploads/files/' + LoggedInUserid + '/file/poster/' + uFileName + '.jpeg';

            setTimeout(function () {

              Jimp.read(tempPosterDir, function (err, image) {
                if (err) {
                  console.log(err);
                }

                var fileNameWithExt = path.basename(uploadedFiles[0].fd);
                var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));

                var extraLen;
                var img_w = parseInt(image.bitmap.width);
                var img_h = parseInt(image.bitmap.height);

                if (img_w > img_h) {
                  extraLen = (img_w - img_h) / 2;
                  image.crop(Math.floor(extraLen), 0, img_h, img_h);
                } else {
                  extraLen = (img_h - img_w) / 2;
                  image.crop(0, Math.floor(extraLen), img_w, img_w);
                }

                image.resize(220, Jimp.AUTO)
                  .quality(60)
                  .write('assets/uploads/files/' + LoggedInUserid + '/file/thumb/' + uFileName + '.jpg'); // save
              }).catch(function (err) {
                console.log(err);
              });

            }, 3000);

            genThumUrl = path.dirname(uploadedFiles[0].fd) + '/thumb/' + uFileName + '.jpg';
            genPosterUrl = path.dirname(uploadedFiles[0].fd) + '/poster/' + uFileName + '.jpeg';
          }
        }


        var files = {};
        files.name = uploadedFiles[0].filename;
        files.type = 'file';
        files.owner = LoggedInUserid;
        files.file_dir = file_dir;
        files.fileFD = uploadedFiles[0].fd;
        files.ext = path.extname(uploadedFiles[0].filename);
        files.size = uploadedFiles[0].size;

        if (genThumUrl) {
          files.fileThumb = genThumUrl;
        }
        if (genPosterUrl) {
          files.filePoster = genPosterUrl;
        }


        UserFiles.create(files, function fileCreated(err, createdFile) {
          if (createdFile) {
            // For Folder Subfolder
            let fileMUrl = require('util').format('%s/users/files/%s', baseURL, createdFile.id);
            let pathStrArr = [];
            if (req.param('filePath')) {
              let slAno = req.param('filePath').indexOf('/');
              if (slAno > -1) {
                let pathStr = req.param('filePath').replace(/\/[^\/]+$/, '');
                let pathArr = pathStr.split('/');
                let pathStrArrInc = 0;
                AsyncCall.eachSeries(pathArr, function (filePath, callback) {
                  let filePathFData = {};
                  filePathFData.name = filePath;
                  filePathFData.owner = LoggedInUserid;
                  filePathFData.type = 'folder';

                  FilesFolder.findOrCreate(filePathFData, filePathFData).exec(function createFindCB(err, record){
                    if( err ) console.log(err);
                    let pathStrArrIncTemp = pathStrArrInc - 1;
                    let pathStrVar;
                    if (pathStrArrIncTemp < 0) {
                      pathStrVar = 0;
                    } else {
                      pathStrVar = pathStrArr[pathStrArrIncTemp];
                    }

                    FilesFolder.update({id: record.id}, {root: pathStrVar}).exec(function afterwards(err, updated) {
                      console.log('');
                    });

                    if (err) {
                      return res.serverError(err);
                    }
                    if (record) {
                      pathStrArr.push(record.id);
                      pathStrArrInc = pathStrArrInc + 1;
                      console.log('count two ' + pathStrArrInc);
                      callback();
                    } else {
                      callback();
                    }
                  });

                }, function (err) {
                  if (err) {
                    console.log(err);
                  } else {
                    if (req.param('folder') && req.param('folder').length) {
                      FilesFolder.update({id: pathStrArr[pathStrArr.length - 1]}, {root: req.param('folder')}).exec(function afterwards(err, updated) {
                        UserFiles.update({id: createdFile.id}, {folder: req.param('folder')}).exec(function afterwards(err, updated) {
                          // For Folder Subfolder
                          return res.send(fileMUrl);
                        });
                      });
                    } else {
                      UserFiles.update({id: createdFile.id}, {folder: pathStrArr[pathStrArr.length - 1]}).exec(function afterwards(err, updated) {
                        // For Folder Subfolder
                        return res.send(fileMUrl);
                      });
                    }
                  }
                });
              }
            } else {
              let saveToFolder = req.param('folder') || '0';
              UserFiles.update({id: createdFile.id}, {folder: saveToFolder}).exec(function afterwards(err, updated) {
                // For Folder Subfolder
                console.log("not used path");
                return res.send(fileMUrl);
              });
            }
          }

        });

      }

    });
  },

};

