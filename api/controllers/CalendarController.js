var request = require('request');
var NodeGeocoder = require('node-geocoder');
var AsyncCall = require('async');
var moment = require('moment');

var outlook = require('nodejs-outlook');

module.exports = {
  /**
   * Description
   * @method createNewCalendarEvent
   * @param {} req
   * @param {} res
   * @return
   */
  createNewCalendarEvent: function (req, res) {
    let userId = req.session.User.id;
    let eventData = req.allParams();
    eventData.user = userId;
    eventData.organizer = req.session.User.id;
    eventData.provider = 'sochara';
    eventData.availability = {[userId]: eventData.showMeAs};
    eventData.stick = true;

    CalendarEvent.create(eventData).exec(function (err, createdEvent) {
      if (createdEvent) {
        CalendarEvent.findOne({
          id: createdEvent.id,
          select: ['_id', 'start', 'end', 'title', 'parent', 'color', 'organizer', 'type', 'attendees', 'allDay', 'stick', 'repeat', 'isRepeated']
        }).exec(function (err, respEvent) {
          if (err) console.log(err);
          let event = respEvent;
          event.attendee = respEvent.attendees.filter(attendee => attendee.id === userId)[0];
          if (respEvent.isRepeated) {
            event.origin = {start: event.start, end: event.end};
            let eventTmp = matchDates({start: respEvent.start, end: respEvent.end, repeat: respEvent.repeat});
            Object.assign(event, eventTmp);
            /*if(respEvent.repeat.type === 'weekly' && respEvent.repeat.days){
              event.start = moment(event.start).format('HH:mm');
              event.end = moment(event.end).format('HH:mm');
              event.dow = respEvent.repeat.days;
            }else if(respEvent.repeat.type === 'daily'){
              event.start = moment(event.start).format('HH:mm');
              event.end = moment(event.end).format('HH:mm');
              event.dow = [0, 1, 2, 3, 4, 5, 6];
            }*/
          }
          async.eachSeries(createdEvent.attendees, function iteratee(attendee, callbackEventMail) {
            let mailgunData = {};
            if (!attendee.isContact && userId !== attendee.id) {
              ContactsService.addContactRequest({loggedInUserId: userId, userId: attendee.id}, function (respContact) {

              });
            }
            if (attendee.role === 'organizer') {
              mailgunData = {to: [attendee.email], subject: 'Event Created for - ' + createdEvent.title};
              mailgunData.html = `Hi ${attendee.name}, the event has been created with ${createdEvent.title}. Would you please review the created event.`;
              try {
                SendMailService.send(mailgunData, function (respSend) {

                });
              } catch (e) {
                console.log(e);
              }
              callbackEventMail();
            } else {
              mailgunData = {to: [attendee.email], subject: `You are invited as a ${attendee.role}`};
              if (attendee.id) {
                mailgunData.html = `Hi ${attendee.name}, You are invited as a ${attendee.role} from an event name ${createdEvent.title}. To see and join the event please login to your Sochara account <a href="https://www.sochara.com/calendar">here</a>.`;
              } else {
                mailgunData.html = `Hi ${attendee.name}, You are invited as a ${attendee.role} for an event name ${createdEvent.title} from Sochara. Create a new account in Sochara and see the event details. <br><a href="https://www.sochara.com/auth/register">Register Now</a>`;
              }
              try {
                SendMailService.send(mailgunData, function (respSend) {

                });
              } catch (e) {
                console.log(e);
              } finally {
                if (attendee.id) sails.sockets.broadcast('sochara_' + attendee.id, 'event_notification', {data: `You have assigned as ${attendee.role} in a new event.`});
              }
              callbackEventMail();
            }
          }, function done() {
            res.send(event);
          });
        });
        // event.attendee = respEvent.attendees.filter(attendee => attendee.id === userId)[0];
      } else {
        res.send({});
      }
    });
  },

  /**
   * Description
   * @method getCalendarInitData
   * @param {} req
   * @param {} res
   * @return
   */
  getCalendarInitData: function (req, res) {
    let initData = {};
    initData.events = [];
    let userId = req.session.User.id;
    initData.user = req.session.User;


    AsyncCall.parallel([
        function (callback) {
          CalendarSetting.findOne({user: req.session.User.id}).exec(function (err, record) {
            if (record) {
              initData.settings = record;
            }
            callback();
          });
        },
        function (callback) {
          Contacts.find({
            user_id: userId,
            select: ['_id', 'user_id', 'status', 'first_name', 'last_name', 'favorite', 'is_sochara_user', 'sochara_user_id']
          }).populate('sochara_user_id', {select: ['id', 'email', 'photo_url', 'mode']}).exec(function (err, respContacts) {
            if (err) console.log(err);
            initData.contacts = respContacts;
            initData.filteredContacts = respContacts.map(c => {
              let contact = {
                name: c.first_name + ' ' + c.last_name,
                email: c.email,
                photo_url: c.is_sochara_user && c.sochara_user_id ? c.sochara_user_id.photo_url : 'images/avatars/profile.jpg',
                id: c.is_sochara_user ? c.sochara_user_id : undefined,
                acceptance: 'pending'
              };
              return {
                value: contact.name.toLowerCase(),
                display: contact.name,
                attendeeData: contact
              };
            });
            callback();
          });
        },
        function (callback) {
          let key = 'AIzaSyATOdXYksI4va9Xc9S7aN7S3o_CVDo1W58';
          let country = 'usa__en';
          let url = 'https://www.googleapis.com/calendar/v3/calendars/' + country + '@holiday.calendar.google.com/events?key=' + key;
          let startDate = new Date(new Date().getFullYear(), new Date().getMonth(), 0);
          let endDate = new Date(new Date().getFullYear(), 11, 31);
          CalendarHolyday.find({
            countryState: country,
            start: {$gte: startDate, $lt: endDate},
            select: ['_id', 'start', 'end', 'title', 'color', 'type', 'stick'],
          }).exec(function (err, holydayResp) {
            if (holydayResp && holydayResp.length) {
              Array.prototype.push.apply(initData.events, holydayResp);
              callback();
            } else {
              request(url, function (error, response) {
                if (error) {
                  console.log(error);
                } else if (response) {
                  CalendarService.holidayExtractFromGoogle(response.body, country, function (returnFromExtract) {
                    if (returnFromExtract) {
                      CalendarHolyday.find({countryState: country}).exec(function (err, holydayResp) {
                        Array.prototype.push.apply(initData.events, holydayResp);
                      });
                    }
                    callback();
                  });
                }
              });
            }
          });
        },
        function (callback) {
          Contacts.find({user_id: userId}).exec(function (err, contactsData) {
            let contactDatas = [];
            contactsData.forEach(function (contact) {
              if (contact.date_birth) {
                let contactObj = {
                  id: contact.id,
                  title: 'Birthday of ' + contact.first_name + ' ' + contact.last_name,
                  start: new Date(contact.date_birth),
                  end: new Date(contact.date_birth),
                  color: {eventColor: 'birthdayColor', textColor: 'c2'},
                  type: 'birthday',
                  stick: true
                };
                contactDatas.push(contactObj);
              }
            });
            Array.prototype.push.apply(initData.events, contactDatas);
            callback();
          });
        },
        function (callback) {
          CalendarEvent.find({
            attendees: {$elemMatch: {id: userId}},
            select: ['_id', 'start', 'end', 'title', 'color', 'parent', 'organizer', 'type', 'attendees', 'allDay', 'stick', 'repeat', 'isRepeated', 'email', 'originalStartTime']
          }).exec(function (err, respEvents) {
            if (err) console.log(err);
            let events = [];
            let childEvents = respEvents.filter(event => {
              return event.parent;
            });
            let childEventDates = childEvents.map(event => event.originalStartTime);
            respEvents.forEach(function (respEvent) {
              let event = respEvent;
              event.attendee = respEvent.attendees.filter(attendee => attendee.id === userId)[0];
              if (respEvent.isRepeated) {
                event.origin = {start: event.start, end: event.end};
                let eventTmp = matchDates({
                  start: respEvent.start,
                  end: respEvent.end,
                  repeat: respEvent.repeat,
                  childEventDates: childEventDates
                });
                Object.assign(event, eventTmp);

              }
              events.push(event);
            });
            Array.prototype.push.apply(initData.events, events);
            callback();
          });
        }
      ],
      function (err, results) {
        res.send(initData);
      });
  },

  /**
   * Description
   * @method updateSignupEvent
   * @param {} req
   * @param {} res
   * @return
   */
  updateSignupEvent: function (req, res) {
    let userId = req.session.User.id;
    let getSignup = req.param('signup');
    let comment = req.param('comment');
    let reminder = req.param('reminder');
    let eventId = req.param('id');
    let updateData = {};
    let newSignupItems = [];
    if (getSignup && getSignup.length) {
      getSignup.forEach(function (signup) {
        let newSignup = signup;
        if (signup.bring) {
          newSignup.available = (signup.available || 0) + signup.bring;
          if (signup.contributors && Array.isArray(signup.contributors)) {
            let findIndex = getIndexIfExist(signup.contributors, userId);
            if (typeof findIndex === 'boolean') {
              signup.contributors.push({user: userId, bring: signup.bring});
            } else {
              signup.contributors[findIndex].bring = signup.bring
            }
          } else {
            newSignup.contributors = [{user: userId, bring: signup.bring}];
          }
        }
        delete newSignup.bring;
        newSignupItems.push(newSignup);
      });
    }

    CalendarEvent.findOne({id: eventId}).exec(function (err, respEvent) {
      if (comment) {
        if (respEvent.comments && respEvent.comments.length) {
          updateData.comments = respEvent.comments;
          updateData.comments.push({user: userId, comment: comment});
        } else {
          updateData.comments = [{user: userId, comment: comment}];
        }
        let userName = req.session.User.first_name + ' ' + req.session.User.last_name;
        sendMailToAllAttendee({
          subject: "A new comment from a Sochara event",
          id: eventId,
          html: `A new comments from ${userName}. Event name: ${respEvent.title}, please have a look on that event.`,
          userId: userId
        });
      }
      if (reminder && reminder.length) {
        updateData.reminder = reminder;
      }
      if (newSignupItems.length) {
        updateData.signup = newSignupItems;
      }
      CalendarEvent.update({id: eventId}, updateData).exec(function afterwards(err, updated) {
        if (err) console.log(err);
        res.send(200);
      });
    });


  },

  /**
   * Description
   * @method deleteEvent
   * @param {} req
   * @param {} res
   * @return
   */
  deleteEvent: function (req, res) {
    let userId = req.session.User.id;
    let id = req.param('id');
    CalendarEvent.destroy({$or: [{parent: id}, {id: id}]}).exec(function (err, deletedEvents) {
      if (err) {
        console.log(err);
      }
      else {
        res.send(deletedEvents);
      }
    });
  },

  /**
   * Description
   * @method getSettings
   * @param {} req
   * @param {} res
   * @return
   */
  getSettings: function (req, res) {
    CalendarSetting.findOne({user: req.session.User.id}).exec(function (err, record) {
      if (record) {
        res.send(record);
      } else {
        res.send(200);
      }
    });
  },

  /**
   * Description
   * @method getEvent
   * @param {} req
   * @param {} res
   * @return
   */
  getEvent: function (req, res) {
    let type = req.param('type');
    let id = req.param('id');
    let userId = req.session.User.id;

    if (type === 'holiday') {
      CalendarHolyday.findOne(id).exec(function (err, respEvent) {
        if (err) console.log(err);
        res.send(respEvent);
      });
    } else if (type === 'birthday') {
      Contacts.findOne(id).exec(function (err, contact) {
        if (err) console.log(err);
        if (contact.date_birth) {
          var contactObj = {};
          contactObj.id = contact.id;
          contactObj.title = 'Birthday of ' + contact.first_name + ' ' + contact.last_name;
          contactObj.allDay = true;
          contactObj.email = contact.email;
          contactObj.userId = contact.id;
          contactObj.name = contact.first_name + ' ' + contact.last_name;
          contactObj.photo_url = contact.photo_url;
          contactObj.start = new Date(contact.date_birth);
          contactObj.end = new Date(contact.date_birth);
          contactObj.allDay = true;
          contactObj.color = {eventColor: 'birthdayColor', textColor: 'c2'};
          contactObj.type = 'birthday';
          contactObj.stick = true;
          res.send(contactObj);
        }
      });
    } else if (type === 'event') {
      CalendarEvent.findOne(id).exec(function (err, respEvent) {
        if (err) console.log(err);
        let event = respEvent;
        event.attendee = respEvent.attendees.filter(attendee => attendee.id === userId)[0];
        if (respEvent.isRepeated) {
          event.origin = {start: event.start, end: event.end};
          let eventTmp = matchDates({start: respEvent.start, end: respEvent.end, repeat: respEvent.repeat});
          Object.assign(event, eventTmp);
          /*if(respEvent.repeat.type === 'weekly' && respEvent.repeat.days){
            event.start = moment(event.start).format('HH:mm');
            event.end = moment(event.end).format('HH:mm');
            event.dow = respEvent.repeat.days;
          }else if(respEvent.repeat.type === 'daily'){
            event.start = moment(event.start).format('HH:mm');
            event.end = moment(event.end).format('HH:mm');
            event.dow = [0, 1, 2, 3, 4, 5, 6];
          }*/
        }
        res.send(event);
      });
    }
  },

  /**
   * Description
   * @method getApiEvents
   * @param {} req
   * @param {} res
   * @return
   */
  getApiEvents: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    CalendarSetting.findOne({user: LoggedInUserID}).exec(function (err, record) {
      if (record && record.showEventsFrom) {
        // res.send(record);
        EmailAccount.find({
          user: LoggedInUserID,
          id: record.showEventsFrom,
          select: ['_id', 'email', 'user', 'name', 'access_token', 'refresh_token', 'provider']
        }).populate('user', {select: ['_id', 'first_name', 'last_name', 'photo_url', 'email']}).exec(function foundCB(err, records) {
          var insertedEvents = [];
          AsyncCall.forEachOf(records, function (item, key, asynCallback) {
            if (item.provider === 'gmail') {
              req.oauth2Client.setCredentials({
                access_token: item.access_token,
                refresh_token: item.refresh_token
              });
              req.calendar.events.list({
                calendarId: item.email,
                auth: req.oauth2Client,
                timeMin: moment().subtract(6, 'months').format()
              }, function (err, data) {
                CalendarService.eventExtractFromGoogle(data, item, function (returnFromExtract) {
                  insertedEvents.push(returnFromExtract);
                });
              });
              asynCallback();
            } else if (item.provider === 'outlook') {
              var Mailbox = 'Inbox';
              var top = 100;
              var skip = 0;

              var credentials = {
                client: {
                  id: '55a857a7-806b-4f56-9c0a-c086a8c9ab95',
                  secret: 'ybNEFBRxvwyYV2MzFGvEpkX',
                },
                auth: {
                  tokenHost: 'https://login.microsoftonline.com',
                  authorizePath: 'common/oauth2/v2.0/authorize',
                  tokenPath: 'common/oauth2/v2.0/token'
                }
              };

              var outlookOauth2 = require('simple-oauth2').create(credentials);

              /**
               * Description
               * @method refreshAccessToken
               * @param {} refreshToken
               * @param {} callback
               * @return
               */
              var refreshAccessToken = function (refreshToken, callback) {
                var tokenObj = outlookOauth2.accessToken.create({refresh_token: refreshToken});
                tokenObj.refresh(callback);
              };


              /**
               * Description
               * @method getCalendarEvent
               * @param {} token
               * @param {} email
               * @param {} top
               * @param {} callback
               * @return
               */
              var getCalendarEvent = function (token, email, top, callback) {

                var queryParams = {
                  // '$select': 'Subject,ReceivedDateTime,From,IsRead',
                  // '$orderby': 'ReceivedDateTime desc',
                  '$top': top,
                  '$count': 'true'
                };

                // Set the API endpoint to use the v2.0 endpoint
                outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
                // Set the anchor mailbox to the user's SMTP address
                outlook.base.setAnchorMailbox(email);

                outlook.calendar.getEvents({token: token, odataParams: queryParams},
                  function (error, result) {
                    if (error) {
                      console.log(error);
                    }
                    else if (result) {
                      CalendarService.eventExtractFromOutlook(result, item, function (returnFromExtract) {
                        if (returnFromExtract) {
                          callback('done');
                        }
                      });
                      // callback(result);
                    }
                  });
              };

              refreshAccessToken(item.refresh_token, function (error, newToken) {
                if (error) {
                  console.log(error);
                }
                if (newToken) {
                  EmailAccount.update({
                    id: item.id,
                    user: item.user,
                    provider: 'outlook'
                  }, {
                    access_token: newToken.token.access_token,
                    refresh_token: newToken.token.refresh_token,
                    id_token: newToken.token.id_token,
                    expires_at: newToken.token.expires_at
                  }).exec(function afterwards(err, updated) {
                    if (err) console.log(err);
                  });
                  getCalendarEvent(newToken.token.access_token, item.email, top, function (resp) {
                    if (resp) {
                      asynCallback();
                    } else {
                      // res.redirect('/mail/inbox/' + item.id);
                    }
                  });
                }
              });
            } else {
              asynCallback();
            }
          }, function (err) {
            if (err) console.log(err);
            // console.log(insertedEvents);
            res.send(insertedEvents);
          });

        });

      } else {
        res.send(200);
      }
    });
  },

  /**
   * Description
   * @method getCalendarEventCount
   * @param {} req
   * @param {} res
   * @return
   */
  getCalendarEventCount: function (req, res) {
    let userId = req.session.User.id;
    // let startDate = moment().startOf('day');
    let startDate = new Date();
    startDate.setHours(0, 0, 0, 0);
    // let endDate = moment().endOf('day');
    let endDate = new Date();
    endDate.setHours(23, 59, 59, 999);
    CalendarEvent.count({
      attendees: {$elemMatch: {id: userId}},
      // $or: [{start: {">": new Date(startDate), "<": new Date(endDate)}}, {
      //   end: {
      //     ">": new Date(startDate),
      //     "<": new Date(endDate)
      //   }
      // }]
      $or: [{start: {$gte: startDate, $lt: endDate}}, {end: {$gte: startDate, $lt: endDate}}]
    }).exec(function (err, respCount) {
      if (err) console.log(err);
      res.json({calendarCount: respCount});
    });
  },

  /**
   * Description
   * @method getBirthdayContacts
   * @param {} req
   * @param {} res
   * @return
   */
  getBirthdayContacts: function (req, res) {
    let userId = req.session.User.id;
    Contacts.find({user_id: userId}).exec(function (err, contactsData) {
      let contactDatas = [];
      contactsData.forEach(function (contact) {
        if (contact.date_birth) {
          var contactObj = {};
          contactObj.id = contact.id;
          contactObj.title = 'Birthday of ' + contact.first_name + ' ' + contact.last_name;
          contactObj.start = new Date(contact.date_birth);
          contactObj.end = new Date(contact.date_birth);
          contactObj.color = {eventColor: 'birthdayColor', textColor: 'c2'};
          contactObj.type = 'birthday';
          contactObj.stick = true;
          contactDatas.push(contactObj);
        }
      });
      res.send(contactDatas);
    });
  },

  /**
   * Description
   * @method sendMail
   * @param {} req
   * @param {} res
   * @return
   */
  sendMail: function (req, res) {
    // let userId = req.session.User.id;
    let mailgunData = {
      to: ['muinjs@gmail.com'],
      cc: ['sochara.dev@gmail.com', 'robinbdseo1@gmail.com', 'faisal.aan048@gmail.com', 'aanhsn@gmail.com'],
      subject: 'Sochara Test mail',
      html: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

Why do we use it?
It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).`
    };
    SendMailService.send(mailgunData, function (respSend) {
      res.json(respSend);
    });

  },

  /**
   * Description
   * @method setSettings
   * @param {} req
   * @param {} res
   * @return
   */
  setSettings: function (req, res) {
    let userId = req.session.User.id;
    let updateData = req.allParams();
    CalendarSetting.findOne({user: userId}).exec(function (err, record) {
      if (err) console.log(err);
      if (record) {
        CalendarSetting.update(record.id, updateData).exec(function (err, updatedSetting) {
          if (err) console.log(err);
          res.send(updatedSetting[0]);
        });
      } else {
        updateData.user = userId;
        CalendarSetting.create(updateData).exec(function (err, createdSetting) {
          if (err) console.log(err);
          res.send(createdSetting);
        });
      }
    });
  },


  /**
   * Description
   * @method getHolidays
   * @param {} req
   * @param {} res
   * @return
   */
  getHolidays: function (req, res) {
    let userId = req.session.User.id;
    let key = 'AIzaSyATOdXYksI4va9Xc9S7aN7S3o_CVDo1W58';
    let country = 'usa__en';
    let url = 'https://www.googleapis.com/calendar/v3/calendars/' + country + '@holiday.calendar.google.com/events?key=' + key;

    User.findOne({id: userId}).exec(function (err, respUser) {
      if (err) console.log(err);
      if (respUser) {
        CalendarHolyday.find({
          countryState: country,
          select: ['_id', 'start', 'end', 'title', 'color', 'type', 'stick']
        }).exec(function (err, holydayResp) {
          if (holydayResp && holydayResp.length) {
            res.send(holydayResp);
          } else {
            request(url, function (error, response) {
              if (error) {
                console.log(error);
              } else if (response) {
                CalendarService.holidayExtractFromGoogle(response.body, country, function (returnFromExtract) {
                  if (returnFromExtract) {
                    CalendarHolyday.find({countryState: country}).exec(function (err, holydayResp) {
                      res.send(holydayResp);
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  },

  /**
   * Description
   * @method readCalendarEvents
   * @param {} req
   * @param {} res
   * @return
   */
  readCalendarEvents: function (req, res) {
    var userId = req.session.User.id;
    CalendarEvent.find({
      attendees: {$elemMatch: {id: userId}},
      select: ['_id', 'start', 'end', 'title', 'color', 'organizer', 'type', 'attendees', 'allDay', 'stick', 'repeat']
    }).exec(function (err, respEvents) {
      if (err) console.log(err);
      let events = [];
      respEvents.forEach(function (respEvent) {
        let event = respEvent;
        event.attendee = respEvent.attendees.filter(attendee => attendee.id === userId)[0];
        events.push(event);
      })
      res.send(events);
    });
  },

  /**
   * Description
   * @method updateEvent
   * @param {} req
   * @param {} res
   * @return
   */
  updateEvent: function (req, res) {
    let eventData = req.allParams();
    let id = eventData.id;
    delete eventData.id;
    let userId = req.session.User.id;

    CalendarEvent.update({id: id}, eventData).exec(function (err, updatedEvent) {
      if (err) {
        console.log(err);
        res.send(err);
      } else {
        CalendarEvent.findOne({
          id: updatedEvent[0].id,
          select: ['_id', 'start', 'end', 'title', 'color', 'organizer', 'type', 'attendees', 'allDay', 'stick', 'repeat', 'isRepeated', 'parent']
        }).exec(function (err, respEvent) {
          sendMailToAllAttendee({
            subject: "The event has been updated",
            id: respEvent.id,
            html: `The event has been updated. Event name: ${respEvent.title}, please have a look on that event.`,
            userId: userId
          });
          let event = respEvent;
          event.attendee = respEvent.attendees.filter(attendee => attendee.id === userId)[0];
          if (respEvent.isRepeated) {
            event.origin = {start: event.start, end: event.end};
            let eventTmp = matchDates({start: respEvent.start, end: respEvent.end, repeat: respEvent.repeat});
            Object.assign(event, eventTmp);
          }
          res.send(event);
        });
      }
    });
  },

  /**
   * Description
   * @method updateRecurringEvent
   * @param {} req
   * @param {} res
   * @return
   */
  updateRecurringEvent: function (req, res) {
    let eventData = req.allParams();
    let id = eventData.id;
    delete eventData.id;
    let userId = req.session.User.id;

    if (eventData.updateFor === 'all') {
      delete eventData.start;
      delete eventData.end;
      CalendarEvent.update({id: id}, eventData).exec(function (err, updatedEvent) {
        if (err) {
          console.log(err);
          res.send(err);
        } else {
          CalendarEvent.findOne({
            id: updatedEvent[0].id,
            select: ['_id', 'start', 'end', 'title', 'color', 'organizer', 'type', 'attendees', 'allDay', 'stick', 'repeat', 'isRepeated', 'parent']
          }).exec(function (err, respEvent) {
            CalendarEvent.destroy({parent: respEvent.id}).exec(function (err, deletedEvents) {
              if (err) {
                console.log(err);
              }
              sendMailToAllAttendee({
                subject: "The event has been updated",
                id: respEvent.id,
                html: `The event has been updated. Event name: ${respEvent.title}, please have a look on that event.`,
                userId: userId
              });
              let event = respEvent;
              event.attendee = respEvent.attendees.filter(attendee => attendee.id === userId)[0];
              if (respEvent.isRepeated) {
                event.origin = {start: event.start, end: event.end};
                let eventTmp = matchDates({start: respEvent.start, end: respEvent.end, repeat: respEvent.repeat});
                Object.assign(event, eventTmp);
              }
              res.send(event);
            });
          });
        }
      });
    } else if (eventData.updateFor === 'this') {
      eventData.isRepeated = false;
      delete eventData.repeat;
      eventData.parent = id;
      eventData.originalStartTime = eventData.start;
      CalendarEvent.create(eventData).exec(function (err, createdEvent) {
        if (err) {
          console.log(err);
          res.send(err);
        } else {
          CalendarEvent.find({
            $or: [{parent: id}, {id: id}],
            select: ['_id', 'start', 'end', 'title', 'parent', 'color', 'organizer', 'type', 'attendees', 'allDay', 'stick', 'repeat', 'isRepeated', 'originalStartTime']
          }).exec(function (err, respEvents) {
            sendMailToAllAttendee({
              subject: "The event has been updated",
              id: respEvents[0].id,
              html: `The event has been updated. Event name: ${respEvents[0].title}, please have a look on that event.`,
              userId: userId
            });
            // new
            let events = [];
            let childEvents = respEvents.filter(event => {
              return event.parent;
            });
            let childEventDates = childEvents.map(event => event.originalStartTime);
            respEvents.forEach(function (respEvent) {
              let event = respEvent;
              event.attendee = respEvent.attendees.filter(attendee => attendee.id === userId)[0];
              if (respEvent.isRepeated) {
                event.origin = {start: event.start, end: event.end};
                let eventTmp = matchDates({
                  start: respEvent.start,
                  end: respEvent.end,
                  repeat: respEvent.repeat,
                  childEventDates: childEventDates
                });
                Object.assign(event, eventTmp);
              }
              events.push(event);
            });
            // new

            res.send(events);

            // let event = respEvent;
            // event.attendee = respEvent.attendees.filter(attendee => attendee.id === userId)[0];
            // if( respEvent.isRepeated ){
            //   event.origin = {start: event.start, end: event.end};
            //   let eventTmp = matchDates({start: respEvent.start, end: respEvent.end, repeat: respEvent.repeat});
            //   Object.assign(event, eventTmp);
            // }
            // res.send(event);
          });
        }
      });
    }
  },

  /**
   * Description
   * @method updateEventOptions
   * @param {} req
   * @param {} res
   * @return
   */
  updateEventOptions: function (req, res) {
    let eventData = req.allParams();
    let id = eventData.id;
    delete eventData.id;
    let userId = req.session.User.id;

    CalendarEvent.update({id: id}, eventData).exec(function (err, updatedEvent) {
      if (err) {
        console.log(err);
        res.send(err);
      } else {
        CalendarEvent.findOne({
          id: updatedEvent[0].id,
          select: ['_id', 'start', 'end', 'title', 'color', 'organizer', 'type', 'attendees', 'allDay', 'stick']
        }).exec(function (err, foundEvent) {
          let event = foundEvent;
          event.attendee = foundEvent.attendees.filter(attendee => attendee.id === userId)[0];
          res.send(event);
        });
      }
    });
  },



  /**
   * Description
   * @method responseOrganizerChange
   * @param {} req
   * @param {} res
   * @return
   */
  responseOrganizerChange: function (req, res) {
    CalendarEvent.findOne({id: req.param('id'), select: ['_id', 'availability']}).exec(function (err, calendarData) {
      if (calendarData) {
        let updateMsg;
        try {
          let updateData = calendarData.availability || {};
          updateData[req.session.User.id] = req.param('res');
          CalendarEvent.update({id: calendarData.id}, {availability: updateData}).exec(function (err, uD) {
            if (err) console.log(err);
          });
        } catch (e) {
          console.log(e);
        }
        updateMsg = 'done';
        res.send({msg: updateMsg});
      }
    });
  },
  /**
   * Description
   * @method responseContributorChange
   * @param {} req
   * @param {} res
   * @return
   */
  responseContributorChange: function (req, res) {
    CalEventAttendees.findOne({
      eventId: req.param('eventId'),
      "attendees.id": req.param('userId')
    }).exec(function (err, fd) {
      if (err) {
        return res.serverError(err);
      }

      if (fd) {
        fd.attendees.forEach(function (item) {
          if (item.id === req.param('userId')) {
            item.acceptance = req.param('res');
          }
        });
        CalEventAttendees.update({id: fd.id}, fd).exec(function (err, uD) {
          if (err) {
            return res.serverError(err);
          }
          res.send({msg: 'success'});
        });
      }
    });
  },
  /**
   * Description
   * @method saveContribution
   * @param {} req
   * @param {} res
   * @return
   */
  saveContribution: function (req, res) {
    SignupVolunteer.findOne({
      eventId: req.param('eventId'),
      userId: req.param('userId'),
      eventScope: req.param('eventScope'),
      eventScopeDate: req.param('eventScopeDate')
    }).exec(function (err, fd) {
      if (err) {
        return res.serverError(err);
      }
      if (fd) {
        SignupVolunteer.update({id: fd.id}, {
          contribution: req.param('contribution')
        }).exec(function (err, ud) {
          if (err) {
            return res.serverError(err);
          }
          res.send({msg: 'success'});
        });
      }
      if (!fd) {
        SignupVolunteer.create({
          eventId: req.param('eventId'),
          userId: req.param('userId'),
          eventScope: req.param('eventScope'),
          eventScopeDate: req.param('eventScopeDate'),
          contribution: req.param('contribution')
        }).exec(function (err, cd) {
          if (err) {
            return res.serverError(err);
          }
          res.send({msg: 'success'});
        });
      }
    });
  },
  /**
   * Description
   * @method saveAttendeeCmntColor
   * @param {} req
   * @param {} res
   * @return
   */
  saveAttendeeCmntColor: function (req, res) {
    CalEventAttendees.findOne({eventId: req.param('eventId')}).exec(function (err, fD) {
      if (err) {
        return res.serverError(err);
      }
      if (fD) {
        fD.attendees.forEach(function (t, index) {
          if (t.id === req.param('userId')) {
            t.color = req.param('res').color;
            t.comment = req.param('res').comment;
          }
        });
        CalEventAttendees.update({id: fD.id}, fD).exec(function (err, uD) {
          if (err) {
            return res.serverError(err);
          }
          res.send({msg: 'success'});
        });
      }
    });
  },
  /**
   * Description
   * @method swapRequest
   * @param {} req
   * @param {} res
   * @return
   */
  swapRequest: function (req, res) {
    var data = req.body;
    EventSwap.create(data).exec(function (err, cd) {
      if (err) {
        return res.serverError(err);
      }
      res.send({msg: 'success'});
    });
  },
  /**
   * Description
   * @method getSwapRequests
   * @param {} req
   * @param {} res
   * @return
   */
  getSwapRequests: function (req, res) {
    EventSwap.find({
      "swapRequestTo.userId": req.session.User.id,
      acceptRequestFlag: false
    }).exec(function (err, request) {
      if (err) {
        return res.serverError(err);
      }
      EventSwap.find({
        "swapRequestFrom.userId": req.session.User.id,
        acceptRequestFlag: false
      }).exec(function (err, requested) {
        if (err) {
          return res.serverError(err);
        }
        res.send({msg: {request: request, requested: requested}});
      });
    });
  },
  /**
   * Description
   * @method acceptSwapRequest
   * @param {} req
   * @param {} res
   * @return
   */
  acceptSwapRequest: function (req, res) {
    if (!req.param('id')) {
      return res.serverError(err);
    }
    EventSwap.update({id: req.param('id')}, {acceptRequestFlag: true}).exec(function (err, ud) {
      if (err) {
        return res.serverError(err);
      }
      if (ud.length) {
        SignupVolunteer.update({
          eventId: ud[0].eventId,
          eventScopeDate: ud[0].eventScopeDate,
          userId: ud[0].swapRequestTo.userId
        }, {eventScopeDate: ud.swapRequestToDate}).exec(function (err, fud) {
          if (err) {
            return res.serverError(err);
          }
          SignupVolunteer.update({
            eventId: ud[0].eventId,
            eventScopeDate: ud[0].swapRequestToDate,
            userId: ud[0].swapRequestFrom.userId
          }, {eventScopeDate: ud.eventScopeDate}).exec(function (err, sud) {
            if (err) {
              return res.serverError(err);
            }
            EventSwap.destroy({id: ud.id}).exec(function () {
              if (err) {
                return res.serverError(err);
              }
              res.send({msg: 'success'});
            });
          });
        });
      }
    });
  },

  /**
   * Description
   * @method setWeatherLocation
   * @param {} req
   * @param {} res
   * @return
   */
  setWeatherLocation: function (req, res) {
    var locationData = {};
    locationData.name = req.param('name');
    locationData.geo = req.param('geo');
    locationData.user = req.session.User.id;
    WeatherLocation.findOne({user: req.session.User.id}).exec(function (err, findResp) {
      if (err) console.log(err);
      if (findResp) {
        WeatherLocation.update({id: findResp.id}, locationData).exec(function (err, updateResp) {
          res.send(updateResp[0]);
        });
      } else {
        WeatherLocation.create(locationData).exec(function (err, createdResp) {
          if (err) console.log(err);
          res.send(createdResp);
        });
      }
    });
  },

  /**
   * Description
   * @method getWeatherLocation
   * @param {} req
   * @param {} res
   * @return
   */
  getWeatherLocation: function (req, res) {
    WeatherLocation.findOne({user: req.session.User.id}).exec(function (err, resp) {
      if (err) console.log(err);
      if (resp) {
        res.send(resp);
      } else {
        res.send(200);
      }
    });
  },

  /**
   * Description
   * @method getLocationByLatLon
   * @param {} req
   * @param {} res
   * @return
   */
  getLocationByLatLon: function (req, res) {
    var lat = req.param('lat');
    var lon = req.param('lon');
    getLocationByGeoIp(lat, lon, function (resp) {
      if (resp) {
        res.send(resp);
      } else {
        res.send(200);
      }
    });

  },

  /**
   * Description
   * @method getWeatherInfo
   * @param {} req
   * @param {} res
   * @return
   */
  getWeatherInfo: function (req, res) {
    var lon = req.param('lon');
    var lat = req.param('lat');
    if (lon && lat) {
      getWeatherMap(lon, lat, function (resp) {
        if (resp) {
          res.send(resp);
        } else {
          res.send(200);
        }
      });
    }
  },
  /**
   * Description
   * @method removeEvent
   * @param {} req
   * @param {} res
   * @return
   */
  removeEvent: function (req, res) {
    CalendarEvent.destroy({id: req.param('id')}).exec(function (err) {
      if (err) {
        return res.serverError(err);
      }
      res.send({msg: 'success'});
    });
  },
  /**
   * Description
   * @method getVolunteerInThisOccurrence
   * @param {} req
   * @param {} res
   * @return
   */
  getVolunteerInThisOccurrence: function (req, res) {
    SignupVolunteer.find({eventId: req.param('id'), eventScopeDate: req.param('date')}).exec(function (err, fd) {
      if (err) {
        return res.serverError(err);
      }
      res.send({msg: fd});
    });
  },
  /**
   * Description
   * @method cancelSwapRequest
   * @param {} req
   * @param {} res
   * @return
   */
  cancelSwapRequest: function (req, res) {
    EventSwap.destroy({id: req.param('id')}).exec(function (err) {
      if (err) {
        return res.serverErr(err);
      }
      res.send({msg: 'success'});
    });
  }
};

/**
 * Description
 * @method getWeatherMap
 * @param {} lon
 * @param {} lat
 * @param {} callback
 * @return
 */
function getWeatherMap(lon, lat, callback) {
  var url = 'https://api.openweathermap.org/data/2.5/forecast/daily?lon=' + lon + '&lat=' + lat + '&cnt=10&mode=json&units=metric&appid=0702621e6f9a5bc65b5be33ddd6c5e66';
  request(url, function (error, response) {
    var weatherData = '';
    if (error) {
      console.log(error);
    } else if (response) {
      weatherData = response.body;
    }
    callback(weatherData);
  });
}

/**
 * Description
 * @method getLocationByGeoIp
 * @param {} lat
 * @param {} lon
 * @param {} callback
 * @return
 */
function getLocationByGeoIp(lat, lon, callback) {
  var options = {
    provider: 'google',
    apiKey: 'AIzaSyATOdXYksI4va9Xc9S7aN7S3o_CVDo1W58'
  };

  var geocoder = NodeGeocoder(options);

  geocoder.reverse({lat: lat, lon: lon}, function (err, res) {
    if (res && res[0]) {
      callback(res[0]);
    } else {
      callback('');
    }
  });
}


/**
 * Description
 * @method getIndexIfExist
 * @param {} arr
 * @param {} usr
 * @return retIndex
 */
function getIndexIfExist(arr, usr) {
  let retIndex = false;
  arr.forEach(function (val, key) {
    if (val.user === usr) {
      retIndex = key;
    }
  });
  return retIndex;
}

/**
 * Description
 * @method sendMailToAllAttendee
 * @param {} mailData
 * @return
 */
function sendMailToAllAttendee(mailData) {
  let mailgunData = {subject: mailData.subject, html: mailData.html, to: []};
  CalendarEvent.findOne({id: mailData.id}).exec(function afterwards(err, respEvent) {
    respEvent.attendees.forEach(function (val, key) {
      if (val.role === 'organizer') {
        mailgunData.to.push(val.email);
      } else {
        mailgunData.to.push(val.email);
      }
    });
    try {
      SendMailService.send(mailgunData, function (respSend) {

      });
    } catch (e) {
      console.log(e);
    } finally {
      respEvent.attendees.forEach(function (attendee) {
        if (attendee.id && attendee.id !== mailData.userId) {
          sails.sockets.broadcast('sochara_' + attendee.id, 'event_notification', {data: `The event has been updated, name ${respEvent.title}.`});
        }
      });
    }
  });
}

/**
 * Description
 * @method matchDates
 * @param {} dateParams
 * @return event
 */
function matchDates(dateParams) {
  let currentDate = moment(new Date(dateParams.start));
  let endDate, weekDays;
  let event = {ranges: []};
  let occurrence = {current: 0, total: dateParams.repeat.end.after ? dateParams.repeat.end.afterValue : undefined};
  if (dateParams.repeat.end.never) {
    endDate = moment(new Date()).add(3, 'years');
  } else if (dateParams.repeat.end.on) {
    endDate = moment(new Date(dateParams.repeat.end.onDate)).add(3, 'minutes');
  } else if (dateParams.repeat.end.after) {
    endDate = moment(new Date()).add(3, 'years');
  }

  endDate = moment(new Date(endDate.format('YYYY-MM-DD'))).add(3, 'minutes');
  if (dateParams.repeat.type === 'daily') {
    while (moment(currentDate).isBefore(endDate) && occurrence.current !== occurrence.total) {
      if (!isInSameTime(currentDate, dateParams.childEventDates || [])) {
        event.ranges.push({start: startOfDay(currentDate), end: endOfDay(currentDate)});
      }
      currentDate = moment(currentDate).add(dateParams.repeat.interval, 'days');
      occurrence.current += 1;
    }
  } else if (dateParams.repeat.type === 'weekly') {
    while (moment(currentDate).isBefore(endDate) && !(occurrence.current >= occurrence.total)) {
      weekDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'].filter(function (day, index) {
        return dateParams.repeat.days.includes(index);
      });
      let nexDay = moment(currentDate).add(0, 'day');
      while (moment(nexDay).isBefore(moment(currentDate).add(1, 'weeks')) && moment(nexDay).isBefore(endDate) && !(occurrence.current >= occurrence.total)) {
        if (weekDays.includes(nexDay.format('ddd'))) {
          event.ranges.push({start: startOfDay(nexDay), end: endOfDay(nexDay)});
          occurrence.current += 1;
        }
        nexDay = moment(nexDay).add(1, 'day');
      }
      currentDate = moment(currentDate).add(dateParams.repeat.interval, 'weeks');
    }
  } else if (dateParams.repeat.type === 'monthly') {
    while (moment(currentDate).isBefore(endDate) && occurrence.current !== occurrence.total) {
      event.ranges.push({start: startOfDay(currentDate), end: endOfDay(currentDate)});
      currentDate = moment(currentDate).add(dateParams.repeat.interval, 'months');
      occurrence.current += 1;
    }
  } else if (dateParams.repeat.type === 'yearly') {
    while (moment(currentDate).isBefore(endDate) && occurrence.current !== occurrence.total) {
      event.ranges.push({start: startOfDay(currentDate), end: endOfDay(currentDate)});
      currentDate = moment(currentDate).add(dateParams.repeat.interval, 'years');
      occurrence.current += 1;
    }
  }
  event.start = moment(dateParams.start).format('HH:mm');
  if (moment(dateParams.start).isBefore(dateParams.end)) {
    event.end = moment(dateParams.end).format('HH:mm');
  } else {
    event.end = moment(dateParams.start).add('30', 'minutes').format('HH:mm');
  }
  return event;
}

/**
 * Description
 * @method startOfDay
 * @param {} date
 * @return CallExpression
 */
function startOfDay(date) {
  return moment(date).startOf('day');
}

/**
 * Description
 * @method endOfDay
 * @param {} date
 * @return CallExpression
 */
function endOfDay(date) {
  return moment(date).endOf('day');
}

/**
 * Description
 * @method isInSameTime
 * @param {} date
 * @param {} dateArr
 * @return MemberExpression
 */
function isInSameTime(date, dateArr) {
  return dateArr.filter(function (fd) {
    return moment(fd).startOf('day').isSame(moment(date).startOf('day'));
  }).length;
}
