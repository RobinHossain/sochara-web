var Jimp = require("jimp");
var fs = require('fs');
var path = require('path');

module.exports = {

  /**
   * Save card information
   * @method saveCard
   * @param req
   * @param res
   * @return
   */
  saveCard: function (req, res) {
    let user = req.session.User.id;
    let cardData = req.allParams();
    cardData.user = user;
    cardData.userArr = cardData.userArr || [user];
    cardData.users = cardData.userArr;
    if (!cardData.eventTitle) cardData.eventTitle = 'Untitled Event';
    if (!cardData.title) cardData.title = 'Untitled Card';
    if (cardData.id && !cardData.copy) {
      Card.update({id: cardData.id}, cardData).exec(function afterwards(err, updated) {
        if (err) console.log(err);
        if (updated) {
          res.send(updated[0]);
        }
      });
    } else {
      if (cardData.id) delete cardData.id;
      Card.create(cardData, function cardCreated(err, createdCard) {
        if (err) console.log(err);
        if (createdCard) {
          res.send(createdCard);
        }
      });
    }
  },

  getTemplates: function (req, res) {
    CardTemplate.find().exec(function (err, respTemplates) {
      if (err) console.log(err);
      res.send(respTemplates);
    });
  },

  saveImageBackground: function (req, res) {
    let LoggedInUserid = req.session.User.id, saveToServerName, randNum = new Date().getTime(), thumbDir;
    req.file('file').upload({
      dirname: path.resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserid + '/card',
      saveAs: function (__newFileStream, cb) {
        let fname = __newFileStream.filename;
        let mName = fname.substr(0, fname.lastIndexOf('.'));
        let fName = mName.replace(/\s\s+/g, ' ').replace(/ /g, "-");
        let ext = path.extname(__newFileStream.filename);
        saveToServerName = fName + "_" + randNum + ext;
        cb(null, saveToServerName);
      },
      maxBytes: 20000000
    }, function whenDone(err, uploadedFiles) {

      if (err) {
        return res.negotiate(err);
      }

      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0) {
        return res.badRequest('No file was uploaded');
      } else {

        let fileType = uploadedFiles[0].type;
        let fileExtName = fileType.substr(0, fileType.lastIndexOf('/'));

        let genThumUrl;
        if (fileExtName === 'image') {
          var fileNameWithExt = path.basename(uploadedFiles[0].fd);
          var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));
          thumbDir = 'assets/uploads/files/' + LoggedInUserid + '/card/thumb';
          if (!fs.existsSync(thumbDir)) {
            fs.mkdirSync(thumbDir);
          }

          Jimp.read(uploadedFiles[0].fd, function (err, image) {
            if (err) throw err;

            var extraLen;
            var img_w = parseInt(image.bitmap.width);
            var img_h = parseInt(image.bitmap.height);


            if (img_w > img_h) {
              extraLen = (img_w - img_h) / 2;
              image.crop(Math.floor(extraLen), 0, img_h, img_h);
            } else {
              extraLen = (img_h - img_w) / 2;
              image.crop(0, Math.floor(extraLen), img_w, img_w);
            }

            image.resize(220, Jimp.AUTO)
              .quality(60)
              .write('assets/uploads/files/' + LoggedInUserid + '/card/thumb/' + uFileName + '.jpg'); // save
          }).catch(function (err) {
            console.log(err);
          });
          genThumUrl = path.dirname(uploadedFiles[0].fd) + '/thumb/' + uFileName + '.jpg';
        }

        let thumb_dir = '/uploads/files/' + LoggedInUserid + '/card/thumb/' + uFileName + '.jpg';
        let file_dir = '/uploads/files/' + LoggedInUserid + '/card/' + path.basename(uploadedFiles[0].fd);

        let files = {name: uploadedFiles[0].filename, ext: path.extname(uploadedFiles[0].filename), type: 'card', owner: LoggedInUserid,
          file_dir: file_dir, fileFD: uploadedFiles[0].fd, fileThumb: genThumUrl, thumb_dir: thumb_dir, size: uploadedFiles[0].size};

        UserFiles.create(files, function fileCreated(err, backFile) {
          if (backFile) {
            res.send(backFile);
            // CardBackground.create({user: backFile.owner, title: backFile.name, size: backFile.size, url: backFile.file_dir, file: backFile.id}).exec(function (error, respBg) {
            //   if(error)console.log(error);
            //   res.send(respBg);
            // });
          }
        });
      }
    });
  },

  getCardElements: function(req, res){
    CardElement.find().exec(function (err, cardElements) {
      if(err) console.log(err);
      res.send(cardElements);
    });
  },

  saveShapeSvg: function(req, res){
    let shapeData = req.allParams();
    shapeData.type = 'line';
    CardElement.create(shapeData).exec(function (err, createdSvg) {
      if(err) console.log(err);
      res.send(createdSvg);
    });
  },

  saveFileFor: function(req, res){
    let user = req.session.User.id;
    let fileData = req.allParams();
    if(fileData.type === 'image'){
      CardImage.create({user: user, title: fileData.name, size: fileData.size, url: fileData.file_dir, file: fileData.id}).exec(function (error, respImage) {
        if(error)console.log(error);
        res.send(respImage);
      });
    }else{
      CardBackground.create({user: user, title: fileData.name, size: fileData.size, url: fileData.file_dir, file: fileData.id}).exec(function (error, respBg) {
        if(error)console.log(error);
        res.send(respBg);
      });
    }

  },

  getCardBackground: function(req, res){
    let user = req.session.User.id;
    CardBackground.find({user: user, select: ['_id', 'title', 'size', 'url', 'file', 'updatedAt']}).populate('file', {select: ['_id', 'name', 'file_dir', 'thumb_dir']}).exec(function (err, respBackground) {
      if(err)console.log(err);
      res.send(respBackground);
    });
  },

  getCardImages: function(req, res){
    let user = req.session.User.id;
    CardImage.find({user: user, select: ['_id', 'title', 'size', 'url', 'file', 'updatedAt']}).populate('file', {select: ['_id', 'name', 'file_dir', 'thumb_dir']}).exec(function (err, respImage) {
      if(err)console.log(err);
      res.send(respImage);
    });
  },

  saveTemplate: function (req, res) {
    let cardData = req.allParams();
    if (!cardData.id) {
      CardTemplate.create(cardData, function cardCreated(err, createdCard) {
        if (err) console.log(err);
        if (createdCard) {
          res.send(createdCard);
        }
      });
    } else {
      let cardId = cardData.id;
      delete cardData.id;
      CardTemplate.update({id: cardId}, cardData).exec(function (err, updatedTemplates) {
        if (err) console.log(err);
        res.send(updatedTemplates[0]);
      });
    }
  },


  saveImage: function (req, res) {
    var LoggedInUserid = req.session.User.id;
    var saveToServerName;
    var randNum = new Date().getTime();
    req.file('file').upload({
      dirname: path.resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserid + '/cards',

      saveAs: function (__newFileStream, cb) {
        var fname = __newFileStream.filename;
        var mName = fname.substr(0, fname.lastIndexOf('.'));
        var fName = mName.replace(/\s\s+/g, ' ');
        fName = fName.replace(/ /g, "-");
        var ext = path.extname(__newFileStream.filename);
        saveToServerName = fName + "_" + randNum + ext;
        cb(null, saveToServerName);
      },

      maxBytes: 10000000
    }, function whenDone(err, uploadedFiles) {

      if (err) {
        return res.negotiate(err);
      }

      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0) {
        return res.badRequest('No file was uploaded');
      } else {
        // var baseURL = Utility.getSiteBaseURL(req);


        Jimp.read(uploadedFiles[0].fd, function (err, image) {
          if (err) throw err;
          var extraLen;
          var img_w = parseInt(image.bitmap.width);
          var img_h = parseInt(image.bitmap.height);


          if (img_w > img_h) {
            extraLen = (img_w - img_h) / 2;
            image.crop(Math.floor(extraLen), 0, img_h, img_h);
          } else {
            extraLen = (img_h - img_w) / 2;
            image.crop(0, Math.floor(extraLen), img_w, img_w);
          }

          var fileNameWithExt = path.basename(uploadedFiles[0].fd);
          var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));

          image.resize(250, Jimp.AUTO)
            .quality(80)
            .write('assets/uploads/files/' + LoggedInUserid + '/cards/thumb/' + uFileName + '.jpg'); // save
        }).catch(function (err) {
          console.log(err);
        });

        var fileNameWithExt = path.basename(uploadedFiles[0].fd);
        var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));
        var genThumUrl = path.dirname(uploadedFiles[0].fd) + '/thumb/' + uFileName + '.jpg';
        var thumb_dir = '/uploads/files/' + LoggedInUserid + '/cards/thumb/' + uFileName + '.jpg';
        var file_dir = '/uploads/files/' + LoggedInUserid + '/cards/' + path.basename(uploadedFiles[0].fd);

        var files = {};
        files.name = uploadedFiles[0].filename;
        files.ext = path.extname(uploadedFiles[0].filename);
        files.type = 'cards';
        files.owner = LoggedInUserid;
        files.file_dir = file_dir;
        files.fileFD = uploadedFiles[0].fd;
        files.fileThumb = genThumUrl;
        files.thumb_dir = thumb_dir;
        files.size = uploadedFiles[0].size;
        UserFiles.create(files, function fileCreated(err, backFile) {
          if (backFile) {
            res.send(backFile);
          }
        });

      }
    });
  },
  getCardData: function (req, res) {
    let userId = req.session.User.id, cardData = {};
    Card.find({$or: [{user: userId}, {userArr: userId}]}).populate('file', {select: ['_id', 'name', 'file_dir', 'thumb_dir', 'file_type', 'ext']}).populate('user', {select: ['_id', 'first_name', 'last_name', 'photo_url']}).populate('users', {select: ['_id', 'first_name', 'last_name', 'photo_url']}).exec(function foundCB(err, records) {
      cardData.all = records.filter(function (record) {
        return record.user.id === userId;
      });
      cardData.shared = records.filter(function (record) {
        return record.user.id !== userId;
      });
      res.send(cardData);
    });
  },

  getDummyCards: function (req, res) {
    DummyCard.find().exec(function foundCB(err, records) {
      res.send(records);
    });
  },

  deleteCard: function (req, res) {
    var userId = req.session.User.id;
    Card.destroy({id: req.param('id'), user: userId}, function destroyedCB(err, record) {
      if (record) {
        res.send('deleted');
      } else {
        res.send('ok');
      }
    });
  },

  favoriteCard: function (req, res) {
    Card.update({id: req.param('id')}, {favorite: req.param('favorite')}).exec(function afterwards(err, updated) {
      if (updated) {
        res.send(200);
      }
    });
  },

  getCard: function (req, res) {
    Card.findOne({id: req.param('id')}).populate('file', {select: ['_id', 'name', 'file_dir', 'thumb_dir', 'file_type', 'ext']}).populate('background', {select: ['_id', 'name', 'file_dir', 'thumb_dir', 'file_type', 'ext']}).populate('users', {select: ['_id', 'first_name', 'last_name', 'photo_url']}).exec(function (err, record) {
      res.send(record);
    });
  },

  getDummyCard: function (req, res) {
    DummyCard.findOne({id: req.param('id')}).populate('file', {select: ['_id', 'name', 'file_dir', 'thumb_dir', 'file_type', 'ext']}).populate('background', {select: ['_id', 'name', 'file_dir', 'thumb_dir', 'file_type', 'ext']}).exec(function (err, record) {
      res.send(record);
    });
  },

  getSharedCardData: function (req, res) {
    let userId = req.session.User.id;
    Card.find({
      userArr: userId
    }).populate('file', {select: ['_id', 'name', 'file_dir', 'thumb_dir', 'file_type', 'ext']}).populate('user', {select: ['_id', 'first_name', 'last_name', 'photo_url']}).exec(function foundCB(err, records) {
      res.send(records);
    });
  }
};
