/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

const async = require('async');
const https = require('https');
const csv = require('csv-parser');
const fs = require('fs');
const SkipperDisk = require('skipper-disk');

const fileAdapter = SkipperDisk(/* optional opts */);
const base64Img = require('base64-img');
const base64ToImage = require('base64-to-image');
const vcardJson = require('vcard-json');
const json2csv = require('json2csv');
const rimraf = require('rimraf');

module.exports = {

  /**
   * Description
   * @method info
   * @param {} req
   * @param {} res
   * @return
   */
  info: function (req, res) {
    var userData = req.session.User;
    res.json(userData);
  },

  /**
   * Description
   * @method avatar
   * @param {} req
   * @param {} res
   * @return
   */
  avatar: function (req, res) {

    req.validate({
      id: 'string'
    });

    User.findOne(req.param('id')).exec(function (err, user) {
      if (err) return res.negotiate(err);
      if (!user) return res.notFound();

      // User has no avatar image uploaded.
      // (should have never have hit this endpoint and used the default image)
      if (!user.avatarFd) {
        return res.notFound();
      }



      // Stream the file down
      fileAdapter.read(user.avatarFd)
        .on('error', function (err) {
          return res.serverError(err);
        })
        .pipe(res);
    });
  },
  //updating user profile
  /**
   * Description
   * @method addContact
   * @param {} req
   * @param {} res
   * @return
   */
  addContact: function (req, res) {
    let userId = req.session.User.id;
    let contactData = req.allParams();
    contactData.user_id = userId;
    Contacts.findOne({email: contactData.email}).exec(function (err, foundContact) {
      if( foundContact ){
        res.send({message: 'found_already'});
      }else{
        Contacts.create(contactData).exec(function (err, createdContact) {
          if(err) console.log(err);
          res.send(createdContact);
        });
      }
    });
  },
  /**
   * Description
   * @method updateGroupContacts
   * @param {} req
   * @param {} res
   * @return
   */
  updateGroupContacts: function (req, res) {
    var id = req.param('id');
    var groupId = req.param('groupId');

    ContactGroups.findOne({id: groupId}).exec(function (err, fd) {
      if (err) {
        return res.serverError(err);
      }
      if (fd) {
        fd.contactIds.push(id);
        ContactGroups.update({id: groupId}, fd).exec(function (err, ud) {
          if (err) {
            return res.serverError(err);
          }
          if (ud) {
            res.json({msg: ud});
          }
        });
      }
    });
  },
  /**
   * Description
   * @method updateFavorites
   * @param {} req
   * @param {} res
   * @return
   */
  updateFavorites: function (req, res) {
    Contacts.update({id: req.param('id')}, {favorite: true}).exec(function afterwards(err, updated) {
      if (updated) {
        res.send(200);
      }
    });
  },
  /**
   * Description
   * @method removeFavorites
   * @param {} req
   * @param {} res
   * @return
   */
  removeFavorites: function (req, res) {
    Contacts.update({id: req.param('id')}, {favorite: false}).exec(function afterwards(err, updated) {
      if (updated) {
        res.send(200);
      }
    });
  },
  /**
   * Description
   * @method newGroup
   * @param {} req
   * @param {} res
   * @return
   */
  newGroup: function (req, res) {
    var loggedInUserId = req.session.User.id;
    var data = {};
    data.name = req.param('name');
    data.user_id = loggedInUserId;
    data.contactIds = req.param('groupContactsId');
    ContactGroups.create(data, function groupCreated(err, createdGroup) {
      if (err) {
        console.log(err);
      }
      if (createdGroup) {
        res.send(200);
      }
    });
  },
  /**
   * Description
   * @method updateGroupFromList
   * @param {} req
   * @param {} res
   * @return
   */
  updateGroupFromList: function (req, res) {
    // var loggedInUserId = req.session.User.id;
    var data = {};
    data.name = req.param('name');
    var id = req.param('id');
    // data.user_id = loggedInUserId;
    data.contactIds = req.param('groupContactsId');
    ContactGroups.update({id: id}, data).exec(function (err, ud) {
      if (err) {
        //sending back error. need some steps in front end to analyse error property
        return res.serverError(err);
      }
      if (ud) {
        res.json({msg: 'success'});
      }
    });
  },
  /**
   * Description
   * @method updateContact
   * @param {} req
   * @param {} res
   * @return
   */
  updateContact: function (req, res) {
    var data = req.body;
    if (data.isChangedAvatar === 'false') {
      Contacts.update({id: data.id}, data).exec(function (err, cd) {
        if (err) {
          //sending back error. need some steps in front end to analyse error property
          return res.serverError(err);
        }
        if (cd) {
          res.json({msg: 'success'});
        }
      });
    }
    else if (data.isChangedAvatar === 'true') {
      var dataURI = data.photo_url;

      base64Img.img(dataURI, 'assets/uploads/files/' + Date.now() + '/', 'avatar', function (err, fp) {
        if (err) {
          Contacts.update({id: data.id}, data).exec(function (err, cd) {
            if (err) {
              //sending back error. need some steps in front end to analyse error property
              return res.serverError(err);
            }
            if (cd) {
              res.json({msg: 'success'});
            }
          });
        }
        if (fp) {
          data.photo_url = fp;
          Contacts.update({id: data.id}, data).exec(function (err, cd) {
            if (err) {
              //sending back error. need some steps in front end to analyse error property
              return res.serverError(err);
            }
            if (cd) {
              res.json({msg: 'success'});
            }
          });
        }
      });
    }
  },
  /**
   * Description
   * @method updateGroupName
   * @param {} req
   * @param {} res
   * @return
   */
  updateGroupName: function (req, res) {
    var data = req.body;
    ContactGroups.update({id: data.id}, {name: data.name}).exec(function (err, cd) {
      if (err) {
        //sending back error. need some steps in front end to analyse error property
        return res.serverError(err);
      }
      if (cd) {
        res.json({msg: 'success'});
      }
    });
  },
  /**
   * Description
   * @method removeContact
   * @param {} req
   * @param {} res
   * @return
   */
  removeContact: function (req, res) {
    var data = req.body;
    Contacts.destroy({id: data.id}).exec(function (err) {
      if (err) {
        //sending back error. need some steps in front end to analyse error property
        return res.serverError(err);
      }

      res.json({msg: 'success'});

    });
  },
  /**
   * Description
   * @method batchContactDelete
   * @param {} req
   * @param {} res
   * @return
   */
  batchContactDelete: function (req, res) {
    var data = req.body;
    Contacts.destroy({id: data.ids}, function destroyedCB(err, record) {
      if (record) {
        res.send('deleted success');
      } else {
        res.send('ok');
      }
    });
  },
  /**
   * Description
   * @method removeGroup
   * @param {} req
   * @param {} res
   * @return
   */
  removeGroup: function (req, res) {
    var data = req.body;
    ContactGroups.destroy({id: data.id}).exec(function (err) {
      if (err) {
        //sending back error. need some steps in front end to analyse error property
        return res.serverError(err);
      }

      res.json({msg: 'success'});

    });
  },
  //updating user profile
  /**
   * Description
   * @method updateUserAccount
   * @param {} req
   * @param {} res
   * @return
   */
  updateUserAccount: function (req, res) {
    var loggedInUserId = req.session.User.id;
    var userSettingData = req.body;
    if (userSettingData.password) {
      User.hashPassword(userSettingData.password, function (data) {
        if (data.err) {
          return res.json(401, {err: 'Something went wrong!'});
        }
        userSettingData.encryptedPassword = data.hash;
        User.update({id: loggedInUserId}, userSettingData).exec(function (err, updatedData) {
          if (err) {
            return res.json(401, {err: "Invalid data!"});
          }
          if (updatedData) {
            res.send({msg: 'success'});
          }
        });
      });
    }
  },
  /**
   * Description
   * @method getProfileData
   * @param {} req
   * @param {} res
   * @return
   */
  getProfileData: function (req, res) {
    var loggedInUserId = req.session.User.id;
    User.findOne({id: loggedInUserId}).exec(function (err, data) {
      if (err) {
        //sending back error. need some steps in front end to analyse error property
        return res.json(401, {err: err});
      }
      if (data) {
        res.send(data);
      }
    });
  },
  /**
   * Description
   * @method updateAvatar
   * @param {} req
   * @param {} res
   * @return
   */
  updateAvatar: function (req, res) {
    var url = req.param('url');
    User.update({id: req.session.User.id}, {photo_url: url}).exec(function (err, updatedImage) {
      if (err) {
        return res.json(401, {err: "Invalid Image!"});
      }
      if (updatedImage) {
        req.session.User.photo_url = url;
        res.send({msg: 'success'});
      }
    });
  },
  /**
   * Description
   * @method updateBanner
   * @param {} req
   * @param {} res
   * @return
   */
  updateBanner: function (req, res) {
    var loggedInUserId = req.session.User.id;
    var url = req.param('url');
    if (url) {

      var base64Str = url;
      var path = 'assets/uploads/files/' + loggedInUserId + '/';
      var optionalObj = {'fileName': 'profileBanner', 'type': 'png'};
      base64ToImage(base64Str, path, optionalObj);
      User.update({id: req.session.User.id}, {bannerImage: '/uploads/files/' + loggedInUserId + '/profileBanner.png'}).exec(function (err, updatedImage) {
        if (err) {
          return res.json(401, {err: "Invalid Image!"});
        }
        if (updatedImage) {
          req.session.User.bannerImage = '/uploads/files/' + loggedInUserId + '/profileBanner.png';
          res.send({msg: 'success'});
        }
      });
    }
    else {
      return res.serverError(err);

    }
  },
  /**
   * Description
   * @method updatePrivacyAttribute
   * @param {} req
   * @param {} res
   * @return
   */
  updatePrivacyAttribute: function (req, res) {
    var attribute = req.param('attribute');
    var privacy = req.param('privacy');
    User.findOne({id: req.session.User.id}).exec(function (err, foundData) {
      if (err) {
        return res.serverError(err);
      }
      var privacyAttributeNow = foundData.privacyAttribute;
      var friendsIndex = privacyAttributeNow.friends.indexOf(attribute);
      var onlyMeIndex = privacyAttributeNow.onlyMe.indexOf(attribute);
      if (friendsIndex !== -1) {
        privacyAttributeNow.friends.splice(friendsIndex, 1);
      }
      if (onlyMeIndex !== -1) {
        privacyAttributeNow.onlyMe.splice(onlyMeIndex, 1);
      }
      if (privacy === 'friends') {
        privacyAttributeNow.friends.push(attribute);
      }
      if (privacy === 'onlyMe') {
        privacyAttributeNow.onlyMe.push(attribute);
      }
      User.update({id: req.session.User.id}, {privacyAttribute: privacyAttributeNow}).exec(function (err, updatedImage) {
        if (err) {
          return res.serverError(err);
        }
        if (updatedImage) {
          res.send({msg: 'success'});
        }
      });
    });

  },
  /**
   * Description
   * @method getContactsForCalendar
   * @param {} req
   * @param {} res
   * @return
   */
  getContactsForCalendar: function (req, res) {
    User.find({id: {'!': req.session.User.id}}).exec(function (err, data) {
      if (err) {
        return res.serverError(err);
      }
      res.send({msg: data});
    });
  },

  /**
   * Description
   * @method getSocharaContacts
   * @param {} req
   * @param {} res
   * @return
   */
  getSocharaContacts: function (req, res) {
    let userId = req.session.User.id;
    Contacts.find({is_sochara_user: true,
      status: "approve",
      user_id: userId,
      select: ['_id', 'first_name', 'last_name']}).populate('sochara_user_id', {select: ['_id', 'email', 'photo_url', 'mode', 'status', 'address']}).exec(function (err, respContacts) {
      let contactData = respContacts.map(contact=>{ return {id: contact.id, first_name: contact.first_name, last_name: contact.last_name, user_id: contact.sochara_user_id.id, email: contact.sochara_user_id.email, photo_url: contact.sochara_user_id.photo_url, mode: contact.sochara_user_id.mode, status: contact.sochara_user_id.status};});
      res.send(contactData);
    });
  },

  /**
   * Description
   * @method users
   * @param {} req
   * @param {} res
   * @return
   */
  users: function (req, res) {
    let searchName = req.param('name');
    let userId = req.session.User.id;
    Contacts.find({
      is_sochara_user: true,
      user_id: userId,
      select: ['_id', 'sochara_user_id']
    }).exec(function (err, respContacts) {
      if (respContacts) {
        let existingContacts = respContacts.map(contact => contact.sochara_user_id);
        existingContacts.push(userId);
        User.find({
          id: {'!': existingContacts},
          // first_name: /^searchName/i,
          // first_name: {$regex: '^Hasan', $options: 'i'},
          $or:[{first_name:new RegExp('^'+searchName,'i')}, {last_name:new RegExp('^'+searchName,'i')}],
          // $or: [ {first_name:  {$regex: '^'+searchName, $options: 'i'}}, {last_name:  {$regex: '^'+searchName, $options: 'i'}} ],
          select: ['_id', 'first_name', 'last_name', 'mode', 'photo_url', 'email', 'address']
        }).sort({
          first_name: 'asc'
        }).limit(10).exec(function (err, allcontacts) {
          res.send(allcontacts);
        });
      }
    });
  },

  /**
   * Description
   * @method getContactById
   * @param {} req
   * @param {} res
   * @return
   */
  getContactById: function (req, res) {
    let userId = req.session.User.id;
    Contacts.findOne({user_id: userId, sochara_user_id: req.param('id')}).populate('sochara_user_id', {select: ['email', 'photo_url', 'mode']}).exec(function (err, contactInfo) {
      if (contactInfo) {
        res.send(contactInfo, contactInfo.sochara_user_id);
      } else {
        User.findOne({id: req.param('id')}).exec(function (err, respUser) {
          if( err )console.log(err);
          res.send(respUser);
        });
      }
    });
  },

  /**
   * Description
   * @method getPendingRequestListCount
   * @param {} req
   * @param {} res
   * @return
   */
  getPendingRequestListCount: function(req, res){
    let userId = req.session.User.id;
    Contacts.count({is_sochara_user: true, sochara_user_id: userId, status: 'pending'}).exec(function (err, requestCount) {
      if(err) console.log(err);
      res.send({count: requestCount});
    });
  },

  /**
   * Description
   * @method getAllContacts
   * @param {} req
   * @param {} res
   * @return
   */
  getAllContacts: function (req, res) {
    let userId = req.session.User.id;
    Contacts.find({
      user_id: userId,
      select: ['_id', 'user_id', 'status', 'request_sent_by', 'first_name', 'last_name', 'favorite', 'notes']
    }).populate('sochara_user_id', {select: ['email', 'photo_url', 'mode', 'address']}).exec(function (err, respContacts) {
      if (err) console.log(err);
      let contacts = respContacts.map(contact=>{
        let socharaUser = contact.sochara_user_id ? {email: contact.sochara_user_id['email'], photo_url: contact.sochara_user_id['photo_url'], mode: contact.sochara_user_id['mode'], address: contact.sochara_user_id['address']} : {};
        return Object.assign(contact, socharaUser);
      });
      res.send(contacts);
    });
  },

  /**
   * Description
   * @method getContacts
   * @param {} req
   * @param {} res
   * @return
   */
  getContacts: function (req, res) {
    let userId = req.session.User.id;
    Contacts.find({
      is_sochara_user: true,
      user_id: userId,
      select: ['_id', 'user_id', 'status', 'request_sent_by', 'first_name', 'last_name', 'favorite', 'notes']
    }).populate('sochara_user_id', {select: ['_id', 'email', 'photo_url', 'mode', 'address']}).exec(function (err, respContacts) {
      if (err) console.log(err);
      let contacts = respContacts.map(contact=>{
        return {id: contact.id, user_id: contact.user_id, sochara_user_id: contact.sochara_user_id && contact.sochara_user_id.id ? contact.sochara_user_id.id : undefined,
        first_name: contact.first_name, last_name: contact.last_name, favorite: contact.favorite,
        notes: contact.notes || '', email: contact.sochara_user_id && contact.sochara_user_id.email ? contact.sochara_user_id.email : undefined, photo_url: contact.sochara_user_id && contact.sochara_user_id.photo_url ? contact.sochara_user_id.photo_url : 'images/avatars/profile.jpg',
        mode: contact.sochara_user_id && contact.sochara_user_id.mode? contact.sochara_user_id.mode : '', address: contact.sochara_user_id && contact.sochara_user_id.address ? contact.sochara_user_id.address : ''};
      });
      res.send(contacts);
    });
  },
  /**
   * Description
   * @method getPendingRequestList
   * @param {} req
   * @param {} res
   * @return
   */
  getPendingRequestList: function (req, res) {
    var userId = req.session.User.id;
    Contacts.find({
      sochara_user_id: userId,
      status: 'pending'
    }).populate('user_id', {select: ['email', 'photo_url', 'mode', 'address', 'first_name', 'last_name']}).exec(function foundRecord(err, records) {
      let contacts = records.map(contact=>{
        let contactData = {email: contact.user_id.email, photo_url: contact.user_id.photo_url || '', mode: contact.user_id.mode || '', first_name: contact.user_id.first_name, last_name: contact.user_id.last_name, address: contact.user_id.address || []};
        return Object.assign(contact, contactData);
      });
      res.send(contacts);
    });
  },
  /**
   * Description
   * @method getMyRequestList
   * @param {} req
   * @param {} res
   * @return
   */
  getMyRequestList: function (req, res) {
    var userId = req.session.User.id;
    Contacts.find({
      request_sent_by: userId,
      status: 'pending'
    }).populate('sochara_user_id', {select: ['email', 'photo_url', 'mode', 'address', 'first_name', 'last_name']}).exec(function foundRecord(err, records) {
      // res.send(records);
      let contacts = records.map(contact=>{
        let contactData = {email: contact.sochara_user_id.email, photo_url: contact.sochara_user_id.photo_url || '', mode: contact.sochara_user_id.mode || '', first_name: contact.sochara_user_id.first_name, last_name: contact.sochara_user_id.last_name, address: contact.sochara_user_id.address || []};
        return Object.assign(contact, contactData);
      });
      res.send(contacts);
    });
  },
  /**
   * Description
   * @method getGroups
   * @param {} req
   * @param {} res
   * @return
   */
  getGroups: function (req, res) {
    ContactGroups.find({user_id: req.session.User.id}).exec(function (err, records) {
      if (err) {
        return res.serverError(err);
      }
      res.send(records);
    });
  },

  /**
   * Description
   * @method importSelectedGoogleContact
   * @param {} req
   * @param {} res
   * @return
   */
  importSelectedGoogleContact: function (req, res) {
    let loggedInUserId = req.session.User.id;
    let contactsToImport = req.param('contacts');
    if( contactsToImport && contactsToImport.length ){
      async.each(contactsToImport, function(contact, callback) {
        let contactData = {first_name: contact.first_name, last_name: contact.last_name, email: contact.email, user_id: loggedInUserId};
        if( contact.phone ){
          contactData.phone = [{type: 'work', phone: contact.phone}];
        }
        try {
          Contacts.findOne({email: contact.email}).exec(function (err, respContact) {
            if(err) console.log(err);
            if( !respContact ){
              User.findOne({email: contact.email}).exec(function (err, respUser) {
                if(err) console.log(err);
                if( respUser ){
                  Contacts.create({
                    user_id: loggedInUserId,
                    sochara_user_id: respUser.id,
                    status: 'pending',
                    is_sochara_user: true,
                    request_sent_by: loggedInUserId,
                    first_name: respUser.first_name,
                    last_name: respUser.last_name,
                    email: respUser.email
                  }).exec(function (err, respContact) {
                    if (err) console.log(err);
                  });
                }else{
                  Contacts.create(contactData).exec(function (err, createdContact) {
                    if(err) console.log(err);
                  });
                }
              });
            }
          });
        }catch (err){
          console.log(err);
        }
        callback();
      }, function(err) {
        if( err ) {
          console.log(err);
        } else {
          Temp.destroy({user: loggedInUserId}, function destroyedCB(err, record) {
            if(err)console.log(err);
            res.send(200);
          });
        }
      });
    }else{
      res.send(200);
    }
  },

  /**
   * Description
   * @method getFavorites
   * @param {} req
   * @param {} res
   * @return
   */
  getFavorites: function (req, res) {
    Contactfavorites.findOne({user_id: req.session.User.id}).exec(function (err, data) {
      if (err) console.log(err);
      if (data) {
        res.send({msg: data});
      }
      if (!data) {
        res.send({msg: []});
      }
    });
  },
  /**
   * Description
   * @method convertImg
   * @param {} req
   * @param {} res
   * @return
   */
  convertImg: function (req, res) {
    var dataURI = req.param('data');
    base64Img.img(dataURI, '', 'k.jp', function (err, fp) {
      if (err) {
        console.log(err);
      }
      console.log(fp);
    });
  },
  /**
   * Description
   * @method fetchGoogleApiData
   * @param {} req
   * @param {} res
   * @return
   */
  fetchGoogleApiData: function (req, res) {
    let token = JSON.parse(req.param('token'));
    let url = 'https://www.google.com/m8/feeds/contacts/default/full?alt=json&access_token=' + token;
    // var url = 'http://localhost/test/json.html';
    let data = '';

    // New Fresh Functions
    https.get(url, function (respUrl) {
      if (respUrl.statusCode >= 200 && respUrl.statusCode < 400) {
        respUrl.on('data', function (data_) {
          data += data_.toString();
        });
        respUrl.on('end', function () {
          //parsing data from URL
          // data = JSON.stringify(data);
          let contacts = JSON.parse(data);

          // Call to service to get Extracted data
          ContactsService.extractGoogleContacts(contacts, function ( respContacts ) {
            res.send(respContacts);
          });
        });
      }
    });
    // New Fresh Functions

  },
  /**
   * Description
   * @method fetchOutlookApiData
   * @param {} req
   * @param {} res
   * @return
   */
  fetchOutlookApiData: function (req, res) {
    var data = JSON.parse(req.param('data'));
    var filteredContacts = [];
    data.forEach(function (item) {
      if (item.name !== 'undefined' && item.name !== "" && item.name !== null) {
        var array = {};
        array['user_id'] = req.session.User.id;
        array['status'] = 'approve';
        var total_namePortion = item.name.split(' ');
        if (total_namePortion.length == 1) {
          array['first_name'] = total_namePortion[0];
          array['last_name'] = '';
        }
        if (total_namePortion.length == 2) {
          array['last_name'] = total_namePortion[1];
          array['first_name'] = total_namePortion[0];
        }
        if (total_namePortion.length == 3) {
          array['last_name'] = total_namePortion[2];
          array['middle_name'] = total_namePortion[1];
          array['first_name'] = total_namePortion[0];
        }
        if (total_namePortion.length == 4) {
          array['last_name'] = total_namePortion[3];
          array['middle_name'] = total_namePortion[2];
          array['first_name'] = total_namePortion[0] + ' ' + total_namePortion[1];
        }
        if (total_namePortion.length > 4) {
          array['last_name'] = total_namePortion[2];
          array['middle_name'] = total_namePortion[1];
          array['first_name'] = total_namePortion[0];
        }
        if (item.emails != null || item.emails != undefined) {
          var emails = [];
          Object.keys(item.emails).forEach(function (item) {
            if (item === 'personal') {
              emails.push({type: 'home', address: item.personal});
            }
            if (item === 'business') {
              emails.push({type: 'office', address: item.business});
            }
            if (item === 'other') {
              emails.push({type: 'other', address: item.other});

            }
          });
          if (emails.length) {
            array['email'] = emails[0].address;
            array['emails'] = emails;
          }
        }
        filteredContacts.push(array);
      }
    });
    // var finalResponse = []; //sending all data to view with async
    var returningItem = [];
    //irretating found result
    filteredContacts.forEach(function (eachItem) {
      returningItem.push(function (callbackreturningdata) {
        Contacts.findOne({
          email: eachItem.email
        }).exec(function foundCB(err, record) {
          console.log('record');
          console.log(record);
          console.log('record');
          if (!record) {
            Contacts.create(eachItem).exec(function (err, row) {
              if (err) {
                // return res.serverError(err);
                callbackreturningdata(null, []);
              }
              if (row) {
                row.dbUpdateStatus = 'Success';
                callbackreturningdata(null, row);
              }
              // callbackreturningdata(null, eachItem);
            });
          } else {
            eachItem.dbUpdateStatus = 'Already Added';
            callbackreturningdata(null, eachItem);
          }
        });
      });

    });
    //finalazing async callback and returning result
    async.parallel(returningItem, function (err, rD) {
      if (err)
        return res.serverError(err);

      // res.send(result);
      res.send(rD);
    });
  }
  ,
  /**
   * Description
   * @method uploadCSV
   * @param {} req
   * @param {} res
   * @return
   */
  uploadCSV: function (req, res) {
    var random_date = new Date();
    var randNum = random_date.getTime();
    var randomText = randNum + '.csv';
    var getFile = req.file('file');
    if (getFile) {
      if (getFile._files.length) {
        getFile.upload({
            dirname: require('path').resolve(sails.config.appPath) + '/assets/uploads/files/' + req.session.User.id + '/contacts',
            saveAs: randomText,
            // don't allow the total upload size to exceed ~10MB
            maxBytes: 10000000
          }, function whenDone(err, uploadedFiles) {
            if (uploadedFiles.length === 0) {
              res.send('failed');
            } else {
              var returnContacts = [];


              fs.createReadStream('assets/uploads/files/' + req.session.User.id + '/contacts/' + randomText)
                .pipe(csv())
                .on('data', function (data) {
                  returnContacts.push(data);
                }).on('error', function () {
                // console.log('error');
                return res.serverError(err);
              }).on('finish', function () {
                // console.log('finish');
                var filteredContacts = [];

                returnContacts.forEach(function (item) {
                  var dataArray = {};
                  if (Object.keys(item)[1] === "Middle Name") {
                    if (item[Object.keys(item)[0]] || item[Object.keys(item)[1]] || item[Object.keys(item)[2]]) {
                      dataArray['user_id'] = req.session.User.id;
                      dataArray['first_name'] = item[Object.keys(item)[0]];
                      dataArray['middle_name'] = item["Middle Name"];
                      dataArray['last_name'] = item["Last Name"];
                      var emails = [];
                      if (item["E-mail Address"]) {
                        console.log(item["E-mail Address"]);
                        emails.push({type: 'home', address: item["E-mail Address"]});
                      }
                      if (item["E-mail 2 Address"]) {
                        emails.push({type: 'office', address: item["E-mail 2 Address"]});
                      }
                      if (item["E-mail 3 Address"]) {
                        emails.push({type: 'other', address: item["E-mail 3 Address"]});
                      }
                      if (emails.length) {
                        dataArray['email'] = emails[0].address;
                        dataArray['emails'] = emails;
                      }

                      var phone = [];
                      if (item["Business Phone"]) {
                        phone.push({type: 'office', number: item["Business Phone"]});
                      }
                      if (item["Home Phone"]) {
                        phone.push({type: 'home', number: item["Home Phone"]});
                      }
                      if (item["Other Phone"]) {
                        phone.push({type: 'other', number: item["Other Phone"]});
                      }
                      if (phone.length) {
                        dataArray['phone'] = phone;
                      }
                    }
                  }
                  else if (Object.keys(item)[1] === "Given Name") {
                    if (item[Object.keys(item)[0]]) {
                      dataArray['user_id'] = req.session.User.id;
                      dataArray['first_name'] = item["Given Name"];
                      dataArray['middle_name'] = item["Additional Name"];
                      dataArray['last_name'] = item["Family Name"];
                      var emails = [];
                      if (item["E-mail 1 - Value"]) {
                        emails.push({type: 'home', address: item["E-mail 1 - Value"]});
                      }
                      if (item["E-mail 2 - Value"]) {
                        emails.push({type: 'office', address: item["E-mail 2 - Value"]});
                      }
                      if (item["E-mail 1 - Value"]) {
                        emails.push({type: 'other', address: item["E-mail 3 - Value"]});
                      }
                      if (emails.length) {
                        dataArray['email'] = emails[0].address;
                        dataArray['emails'] = emails;
                      }

                      var phone = [];
                      if (item["Phone 2 - Value"]) {
                        phone.push({type: 'office', number: item["Phone 2 - Value"]});
                      }
                      if (item["Phone 1 - Value"]) {
                        phone.push({type: 'home', number: item["Phone 1 - Value"]});
                      }
                      if (item["Phone 3 - Value"]) {
                        phone.push({type: 'other', number: item["Phone 3 - Value"]});
                      }
                      if (phone.length) {
                        dataArray['phone'] = phone;
                      }
                    }
                  }

                  /**
                   * Description
                   * @method isEmptyObject
                   * @param {} obj
                   * @return UnaryExpression
                   */
                  function isEmptyObject(obj) {
                    return !Object.keys(obj).length;
                  }

                  if (!isEmptyObject(dataArray)) {
                    filteredContacts.push(dataArray);
                  }

                });

                if (filteredContacts && filteredContacts.length) {
                  var returningItem = [];
                  //irretating found result
                  filteredContacts.forEach(function (eachItem) {
                    returningItem.push(function (callbackreturningdata) {
                      Contacts.findOne({
                        email: eachItem.email
                      }).exec(function foundCB(err, record) {
                        if (!record) {
                          Contacts.create(eachItem).exec(function (err, row) {
                            if (err) {
                              // return res.serverError(err);
                              callbackreturningdata(null, []);
                            }
                            if (row) {
                              row.dbUpdateStatus = 'Success';
                              callbackreturningdata(null, row);
                            }
                            // callbackreturningdata(null, eachItem);
                          });
                        } else {
                          eachItem.dbUpdateStatus = 'Already Added'
                          callbackreturningdata(null, eachItem);
                        }
                      });
                    });

                  });
                  //finalazing async callback and returning result
                  async.parallel(returningItem, function (err, rD) {
                    if (err) {
                      return res.serverError(err);
                    }

                    // res.send(result);
                    res.send(rD);
                  });
                } else {
                  return res.serverError(err);
                }
              });
            }
          }
        );
      }
      else {
        res.send('failed');
      }
    }
  }
  ,
  /**
   * Description
   * @method uploadVcf
   * @param {} req
   * @param {} res
   * @return
   */
  uploadVcf: function (req, res) {
    req.file('file').upload({
      dirname: require('path').resolve(sails.config.appPath) + '/assets/uploads/files/' + req.session.User.id + '/contacts',
      saveAs: 'contacts.vcf',
      // don't allow the total upload size to exceed ~10MB
      maxBytes: 10000000
    }, function whenDone(err, uploadedFile) {
      if (err) {
        return res.serverError(err);
      }
      vcardJson.parseVcardFile('assets/uploads/files/' + req.session.User.id + '/contacts/contacts.vcf', function (err, data) {
        if (err) console.log('oops:' + err);
        else {
          if (data.length) {
            var filteredContacts = [];
            data.forEach(function (item) {
              console.log(item);
              if (item.fullname) {
                var array = {};
                array['user_id'] = req.session.User.id;
                var total_namePortion = item.fullname.split(' ');
                if (total_namePortion.length == 1) {
                  array['first_name'] = total_namePortion[0];
                  array['last_name'] = '';
                }
                if (total_namePortion.length == 2) {
                  array['last_name'] = total_namePortion[1];
                  array['first_name'] = total_namePortion[0];
                }
                if (total_namePortion.length == 3) {
                  array['last_name'] = total_namePortion[2];
                  array['middle_name'] = total_namePortion[1];
                  array['first_name'] = total_namePortion[0];
                }
                if (total_namePortion.length == 4) {
                  array['last_name'] = total_namePortion[3];
                  array['middle_name'] = total_namePortion[2];
                  array['first_name'] = total_namePortion[0] + ' ' + total_namePortion[1];
                }
                if (total_namePortion.length > 4) {
                  array['last_name'] = total_namePortion[2];
                  array['middle_name'] = total_namePortion[1];
                  array['first_name'] = total_namePortion[0];
                }
                if (item.email != null || item.email != undefined) {
                  var emails = [];
                  var counter = 0;
                  item.email.forEach(function (item) {
                    if (counter === 0) {
                      emails.push({type: 'home', address: item.value});
                    }
                    if (counter === 1) {
                      emails.push({type: 'office', address: item.value});
                    }
                    if (counter === 3) {
                      emails.push({type: 'other', address: item.value});
                    }
                    counter++;
                  });
                  if (emails.length) {
                    array['email'] = emails[0].address;
                    array['emails'] = emails;
                  }
                }

                if (item.phone != null || item.phone != undefined) {
                  var phone = [];
                  var counter = 0;
                  item.phone.forEach(function (item) {
                    if (counter === 0) {
                      phone.push({type: 'home', number: item.value});
                    }
                    if (counter === 1) {
                      phone.push({type: 'office', number: item.value});
                    }
                    if (counter === 3) {
                      phone.push({type: 'other', number: item.value});
                    }
                    counter++;
                  });
                  if (phone.length) {
                    array['phone'] = phone;
                  }
                }
                if (item.addr != null || item.addr != undefined) {
                  var address = [];
                  var counter = 0;
                  item.addr.forEach(function (item) {
                    if (counter === 0) {
                      address.push({
                        type: 'home',
                        address: {
                          street: item.street,
                          city: item.city,
                          state: item.state,
                          zip: item.zip,
                          country: item.country
                        }
                      });
                    }
                    if (counter === 1) {
                      address.push({
                        type: 'office',
                        address: {
                          street: item.street,
                          city: item.city,
                          state: item.item.state,
                          zip: item.zip,
                          country: item.country
                        }
                      });
                    }
                    if (counter === 3) {
                      address.push({
                        type: 'other',
                        address: {
                          street: item.street,
                          city: item.city,
                          state: item.state,
                          zip: item.zip,
                          country: item.country
                        }
                      });
                    }
                    counter++;
                  });
                  if (phone.length) {
                    array['phone'] = phone;
                  }
                }
                if (item.bday != null || item.bday != undefined) {
                  array['date_birth'] = item.bday;
                }
                if (item.note != null || item.note != undefined) {
                  array['notes'] = item.note;
                }
                filteredContacts.push(array);
              }

            });
            if (filteredContacts.length) {
              var returningItem = [];
              //irretating found result
              filteredContacts.forEach(function (eachItem) {
                returningItem.push(function (callbackreturningdata) {
                  Contacts.findOne({
                    email: eachItem.email
                  }).exec(function foundCB(err, record) {
                    console.log('record');
                    console.log(record);
                    console.log('record');
                    if (!record) {
                      Contacts.create(eachItem).exec(function (err, row) {
                        if (err) {
                          // return res.serverError(err);
                          callbackreturningdata(null, []);
                        }
                        if (row) {
                          row.dbUpdateStatus = 'Success';
                          callbackreturningdata(null, row);
                        }
                        // callbackreturningdata(null, eachItem);
                      });
                    } else {
                      eachItem.dbUpdateStatus = 'Already added';
                      callbackreturningdata(null, eachItem);
                    }
                  });
                });
              });
              //finalazing async callback and returning result
              async.parallel(returningItem, function (err, rD) {
                if (err)
                  return res.serverError(err);

                // res.send(result);
                res.send(rD);
              });
            } else {
              return res.serverError(err);
            }
          }
          else {
            return res.serverError('failed');
          }

        }
      });
    });
  },
  /**
   * Description
   * @method exportContacts
   * @param {} req
   * @param {} res
   * @return
   */
  exportContacts: function (req, res) {
    let user = req.session.User.id;
    let contacts = req.param('contacts');
    let format = req.param('format');
    // console.log(format);
    let contactDir = 'assets/uploads/files/' + user + '/contacts';
    if (!fs.existsSync(contactDir)) {
      fs.mkdirSync(contactDir);
    }
    if(format === 'google' || format === 'csv'){
      exportForGoogle({contacts: contacts, user: user}, function (exportedFile) {
        setTimeout(function () {
          res.send(exportedFile);
        }, 100);
      });
    }else if(format === 'outlook'){
      exportForOutlook({contacts: contacts, user: user}, function (exportedFile) {
        setTimeout(function () {
          res.send(exportedFile);
        }, 100);
      });
    }else if(format === 'card' || format === 'other'){
      exportForVcard({contacts: contacts, user: user}, function (exportedFile) {
        setTimeout(function () {
          res.send(exportedFile);
        }, 100);
      });
    }
  },
  /**
   * Description
   * @method changeIsShownGroup
   * @param {} req
   * @param {} res
   * @return
   */
  changeIsShownGroup: function (req, res) {
    var id = req.param('id');
    var trueOrFalse = req.param('trueOrFalse');
    ContactGroups.update({id: id}, {isShown: trueOrFalse}).exec(function (err, uD) {
      if (err) {
        return res.serverError('failed');
      }
      res.send({msg: 'success'});
    });
  },
  /**
   * Description
   * @method addContactRequest
   * @param {} req
   * @param {} res
   * @return
   */
  addContactRequest: function (req, res) {
    let loggedInUserId = req.session.User.id;
    let userId = req.param('id');
    User.findOne({id: userId}).exec(function (err, respUser) {
      Contacts.create({
        user_id: loggedInUserId,
        sochara_user_id: respUser.id,
        status: 'pending',
        is_sochara_user: true,
        request_sent_by: loggedInUserId,
        first_name: respUser.first_name,
        last_name: respUser.last_name,
        email: respUser.email
      }).exec(function (err, respContact) {
        if (err) console.log(err);
        Contacts.findOne({
          id: respContact.id,
          select: ['_id', 'user_id', 'status', 'request_sent_by', 'first_name', 'last_name', 'email', 'favorite', 'notes']
        }).populate('sochara_user_id', {select: ['email', 'photo_url', 'mode']}).exec(function (err, createdContact) {
          if (err) console.log(err);
          sails.sockets.broadcast('sochara_' + userId, 'global', {message: 'You have a new contact request, to accept go to Contact module!'});
          let contactData = Object.assign(createdContact, createdContact.sochara_user_id);
          res.send(contactData);
        });
      });
    });
  },
  /**
   * Description
   * @method deleteContactRequest
   * @param {} req
   * @param {} res
   * @return
   */
  deleteContactRequest: function (req, res) {
    var userId = req.session.User.id;
    Contacts.destroy({id: req.param('id')}, function destroyedCB(err, record) {
      if (record) {
        res.send('deleted');
      } else {
        res.send('ok');
      }
    });
  },
  /**
   * Description
   * @method acceptContactRequest
   * @param {} req
   * @param {} res
   * @return
   */
  acceptContactRequest: function (req, res) {
    var contactData = req.allParams();
    var id = contactData.id || '';
    contactData.status = 'approve';
    Contacts.update({id: id}, {status: 'approve'}).exec(function afterwards(err, updated) {
      if (err) console.log(err);
      if (updated) {
        User.findOne({
          id: updated[0].user_id,
          select: ['_id', 'first_name', 'last_name', 'email', 'photo_url', 'mode']
        }).exec(function (err, respUser) {
          if (respUser) {
            Contacts.create({
              user_id: updated[0].sochara_user_id,
              status: 'approve',
              is_sochara_user: true,
              request_sent_by: respUser.id,
              first_name: respUser.first_name,
              last_name: respUser.last_name,
              email: respUser.email,
              sochara_user_id: respUser.id
            }).exec(function (err, respContact) {
              if (err) console.log(err);
              res.send(respContact);
            });
          }
        });
      }
    });
  },

};


/**
 * Description
 * @method exportForGoogle
 * @param {} data
 * @param {} next
 * @return
 */
function exportForGoogle( data, next ){
  let googleFormat =  data.contacts.map(contact=>{
    return {'Name': contact.first_name + ' ' + contact.last_name, 'Family Name': contact.first_name + ' ' + contact.last_name,
      'Birthday': contact.date_birth || '', 'E-mail 1 - Type': 'Home', 'E-mail 1 - Value': contact.email, 'Gender': contact.gender || '',
      'Location': typeof contact.address === 'object' && Object.keys(contact.address).length ? contact.address[0].address.street : contact.address || '',
      'Phone 1 - Type': 'Mobile',
      'Phone 1 - Value' : typeof contact.phone === 'object' && Object.keys(contact.phone).length ? contact.phone[0].number : contact.phone || '',
      'Group Membership': '* My Contacts', 'Notes': contact.notes || ''
    };
  });
  let fields = ['Name', 'Family Name', 'Birthday', 'E-mail 1 - Type', 'E-mail 1 - Value', 'Gender', 'Location', 'Phone 1 - Type', 'Phone 1 - Value', 'Group Membership', 'Notes'];
  let formattedCsv = json2csv({data: googleFormat, fields: fields});
  let fileName = 'assets/uploads/files/' + data.user + '/contacts/' + 'google.csv';
  createOrReplaceFile(fileName, formattedCsv, function (createdFileName) {
    next(createdFileName);
  });
}

/**
 * Description
 * @method exportForOutlook
 * @param {} data
 * @param {} next
 * @return
 */
function exportForOutlook( data, next ){
  let outlookFormat = data.contacts.map(contact=>{
    return {'First Name': contact.first_name, 'Last Name': contact.last_name, 'Birthday': contact.date_birth || '',
      'Location': typeof contact.address === 'object' && Object.keys(contact.address).length ? contact.address[0].address.street : contact.address || '',
      'E-mail Address': contact.email,
      'Mobile Phone' : typeof contact.phone === 'object' && Object.keys(contact.phone).length ? contact.phone[0].number : contact.phone || '',
      'Notes': contact.notes || ''
    };
  });
  let fields = ['First Name', 'Last Name', 'Birthday', 'Location', 'E-mail Address', 'Mobile Phone', 'Notes'];
  let formattedCsv = json2csv({data: outlookFormat, fields: fields});
  let fileName = 'assets/uploads/files/' + data.user + '/contacts/' + 'contacts.csv';
  createOrReplaceFile(fileName, formattedCsv, function (createdFileName) {
    next(createdFileName);
  });
}

/**
 * Description
 * @method exportForVcard
 * @param {} data
 * @param {} next
 * @return
 */
function exportForVcard( data, next ){
  let vcfData = '';
  let vCard;
  data.contacts.forEach(contact=>{
    vCard = require('vcards-js');
    vCard = vCard();
    vCard.firstName = contact.first_name|| '';
    vCard.lastName = contact.last_name|| '';
    vCard.birthday = contact.date_birth || '';
    vCard.email = contact.email || '';
    vCard.cellPhone = typeof contact.phone === 'object' && Object.keys(contact.phone).length ? contact.phone[0].number : contact.phone || '';
    vCard.homeAddress.label = 'Home Address';
    vCard.homeAddress.street = typeof contact.address === 'object' && Object.keys(contact.address).length ? contact.address[0].address.street : contact.address || '';
    vCard.version = '3.0';
    vcfData = vcfData + vCard.getFormattedString();
  });

  let fileName = 'assets/uploads/files/' + data.user + '/contacts/' + 'contacts.vcf';
  createOrReplaceFile(fileName, vcfData, function (createdFileName) {
    next(createdFileName);
  });
}

/**
 * Description
 * @method createOrReplaceFile
 * @param {} fileName
 * @param {} fileData
 * @param {} next
 * @return
 */
function createOrReplaceFile( fileName, fileData, next ){
  try {
    fs.stat(fileName, function(err, stat) {
      if(err == null) {
        rimraf(fileName, function () {
          fs.writeFile(fileName, fileData, function (err) {
            if (err) console.log(err);
          });
        });
      } else if(err.code === 'ENOENT') {
        fs.writeFile(fileName, fileData, function (err) {
          if (err) console.log(err);
        });
      } else {
        console.log('Some other error: ', err.code);
      }
    });
  } catch (err) {
    console.error(err);
  }
  next(fileName);
}
