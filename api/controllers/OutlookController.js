/**
 * Admin/OutlookController
 *
 * @description :: Server-side logic for managing admin/outlook
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers

 Help Links:
 https://github.com/mscdex/node-imap
 https://github.com/pipedrive/inbox
 https://github.com/google/google-api-nodejs-client
 https://lite.afterlogic.com/
 */
let outlook = require('nodejs-outlook');
let moment = require('moment');

module.exports = {

  /**
   * Description
   * @return
   * @method authUrl
   * @param {} req
   * @param {} res
   * @return 
   */
  authUrl: function (req, res) {
    let module = req.param('module') || '';
    if(module && module === 'contact'){
      let redirectUri = req.protocol + '://' + req.get('host') + '/outlook/importContact';
      let scopes = ['https://outlook.office.com/contacts.read'];
      res.send(req.getAuthUrl(redirectUri, scopes));
    }else{
      res.send(req.getAuthUrl());
    }
  },

  /**
   * Description
   * @return
   * @method oauth2callback
   * @param {} req
   * @param {} res
   * @return 
   */
  oauth2callback: function (req, res) {
    'use strict';
    var code = req.param('code');
    var LoggedInUserID = req.session.User.id;

    req.getTokenFromCode({code: code}, getUserEmails);

    /**
     * Description
     * @return
     * @method getUserEmails
     * @param {} getToken
     * @return 
     */
    function getUserEmails(getToken) {
      req.getUserEmail(getToken, function (error, userData) {
        if (error) {

        } else if (userData) {
          // console.log(userData);
          EmailAccount.findOne({
            user: LoggedInUserID,
            email: userData.EmailAddress,
            provider: 'outlook'
          }, function foundCB(err, record) {
            if (record) {
              redirectUrlTo(LoggedInUserID, function (url) {
                res.redirect(url);
              });
            } else {
              EmailAccount.create({
                email: userData.EmailAddress,
                name: userData.DisplayName,
                provider: 'outlook',
                access_token: getToken.access_token,
                id_token: getToken.id_token,
                refresh_token: getToken.refresh_token,
                token_type: getToken.token_type,
                scope: getToken.scope,
                expires_at: getToken.expires_at,
                user: LoggedInUserID
              }, function fileCreated(err, oRecord) {

                if (err) console.log(err);
                // Retrive From Outlook Mail

                var Mailbox = 'Inbox';
                var top = 20;
                var skip = 0;


                var credentials = {
                  client: {
                    id: '55a857a7-806b-4f56-9c0a-c086a8c9ab95',
                    secret: 'ybNEFBRxvwyYV2MzFGvEpkX',
                  },
                  auth: {
                    tokenHost: 'https://login.microsoftonline.com',
                    authorizePath: 'common/oauth2/v2.0/authorize',
                    tokenPath: 'common/oauth2/v2.0/token'
                  }
                };
                var outlookOauth2 = require('simple-oauth2').create(credentials);

                /**
                 * Description
                 * @method refreshAccessToken
                 * @param {} refreshToken
                 * @param {} callback
                 * @return 
                 */
                var refreshAccessToken = function (refreshToken, callback) {
                  var tokenObj = outlookOauth2.accessToken.create({refresh_token: refreshToken});
                  tokenObj.refresh(callback);
                };

                /**
                 * Description
                 * @method getEmails
                 * @param {} token
                 * @param {} email
                 * @param {} folder
                 * @param {} top
                 * @param {} skip
                 * @param {} callback
                 * @return 
                 */
                var getEmails = function (token, email, folder, top, skip, callback) {

                  var queryParams = {
                    // '$select': 'Subject,ReceivedDateTime,From,IsRead',
                    '$orderby': 'ReceivedDateTime desc',
                    '$top': top,
                    '$skip': skip,
                    '$count': 'true'
                  };

                  // Set the API endpoint to use the v2.0 endpoint
                  outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
                  // Set the anchor mailbox to the user's SMTP address
                  outlook.base.setAnchorMailbox(email);

                  outlook.mail.getMessages({token: token, folderId: folder, odataParams: queryParams},
                    function (error, result) {
                      if (error) {
                        console.log('getMessages returned an error: ' + error);
                      }
                      else if (result) {
                        callback(result);
                      }
                    });
                };

                refreshAccessToken(oRecord.refresh_token, function (error, newToken) {
                  if (error) {
                    console.log(error);
                  }
                  if (newToken) {
                    EmailAccount.update({
                      id: oRecord.id,
                      user: oRecord.user,
                      provider: 'outlook'
                    }, {
                      access_token: newToken.token.access_token,
                      refresh_token: newToken.token.refresh_token,
                      id_token: newToken.token.id_token,
                      expires_at: newToken.token.expires_at
                    }).exec(function afterwards(err, updated) {
                      if (err) console.log(err);
                      console.log('updated');
                    });
                    getEmails(newToken.token.access_token, oRecord.email, Mailbox, top, skip, function (resp) {
                      if (resp) {
                        MailService.outlookExtractAndStore(resp, oRecord.email, oRecord.id, oRecord.user, function (mailData) {
                          if (mailData) {
                            var gdata = {};
                            gdata.id = oRecord.id;
                            gdata.skip = 40;
                            gdata.maxResults = 180;
                            Cron.create({
                              name: "Outlook Module Cron",
                              type: "outlook",
                              module: "mail",
                              user: LoggedInUserID,
                              data: gdata,
                              status: "pending"
                            }, function fileCreated(err, ocRecord) {
                              if (ocRecord) {
                                EmailAccount.findOne({
                                  id: oRecord.id,
                                  select: ['_id', 'email', 'user', 'name', 'access_token', 'refresh_token', 'provider']
                                }).populate('user', {select: ['_id', 'first_name', 'last_name', 'photo_url', 'email']}).exec(function foundCB(err, gAccountRecord) {

                                  getOutlookCalendarEvents(newToken.token.access_token, gAccountRecord, '100', function (resp) {
                                    if( resp ){
                                      redirectUrlTo(LoggedInUserID, function (url) {
                                        res.redirect(url);
                                      });
                                      // res.redirect('/mail/inbox/' + ocRecord.id);
                                    }else{
                                      redirectUrlTo(LoggedInUserID, function (url) {
                                        res.redirect(url);
                                      });
                                      // res.redirect('/mail/inbox/' + ocRecord.id);
                                    }
                                  });

                                });
                              }else{
                                redirectUrlTo(LoggedInUserID, function (url) {
                                  res.redirect(url);
                                });
                                // res.redirect('/mail/inbox/' + ocRecord.id);
                              }
                            });
                          }
                        });
                      } else {
                        console.log('No Message Found');
                        redirectUrlTo(LoggedInUserID, function (url) {
                          res.redirect(url);
                        });
                        // res.redirect('/mail/inbox/' + oRecord.id);
                      }
                    });
                  }
                });

                // Retrive From Outlook Mail


              });
            }
          });
        }
      });
    }
  },

  /**
   * Description
   * @return
   * @method loginOutlook
   * @param {} req
   * @param {} res
   * @return 
   */
  loginOutlook: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    EmailAccount.findOne({
      id: req.param('id'),
      user: LoggedInUserID,
      provider: 'outlook'
    }, function froundCB(err, record) {
      if (record && record.access_token && record.refresh_token) {

        req.session.Outlook = record;

        req.refreshAccessToken(record.refresh_token, function (error, newToken) {
          if (error) {
            callback(error, null);
          } else if (newToken) {


            EmailAccount.update({id: req.param('id'), user: LoggedInUserID, provider: 'outlook'},
              {
                access_token: newToken.token.access_token,
                refresh_token: newToken.token.refresh_token,
                expires_at: newToken.token.expires_at
              })
              .exec(function afterwards(err, updated) {
                if (err) console.log(err);

                res.view('admin/outlook/index', {
                  'outlookId': req.param('id'),
                  'layout': 'admin/layout-outlook',
                  'allParams': req.allParams(),
                  'profile': record,
                  'gmail_profile': record,
                  'folder': 'inbox'
                });

              });


          }
        });

      }
    });
  },

  /**
   * Description
   * @return
   * @method index
   * @param {} req
   * @param {} res
   * @return 
   */
  index: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    EmailAccount.findOne({
      user: LoggedInUserID, provider: 'outlook'
    }).sort({updatedAt: -1}).exec(function (err, record) {
      if (record) {
        res.redirect('/admin/outlook/account/' + record.id);
      } else {
        res.view("admin/outlook/permission", {
          'url': req.googleAuthUrl,
          'layout': 'admin/layout-outlook-permission'
        });
      }
    });
  },


  /**
   * Description
   * @return
   * @method sendEmail
   * @param {} req
   * @param {} res
   * @return 
   */
  sendEmail: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    EmailAccount.findOne({
      id: req.param('outlookId'),
      user: LoggedInUserID,
      provider: 'outlook'
    }, function froundCB(err, record) {
      if (record && record.access_token && record.refresh_token) {

        req.getEmailSend(record.access_token, record.email, req.param('Message'), function (resp) {
          res.send(resp);
        });

      }
    });
  },

  /**
   * Description
   * @return
   * @method createNewLabel
   * @param {} req
   * @param {} res
   * @return 
   */
  createNewLabel: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    EmailAccount.findOne({
      id: req.param('outlookId'),
      user: LoggedInUserID,
      provider: 'outlook'
    }, function froundCB(err, record) {
      if (record && record.access_token && record.refresh_token) {


        // Outlook Mail Query
        req.createNewLabel(record.access_token, record.email, req.param('parentId'), req.param('name'), function (resp) {
          res.send('done');
        });


      }
    });
  },

  /**
   * Description
   * @return
   * @method getMailAttachment
   * @param {} req
   * @param {} res
   * @return 
   */
  getMailAttachment: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var threadID = req.param('threadID');
    EmailAccount.findOne({
      id: req.param('id'),
      user: LoggedInUserID,
      provider: 'outlook'
    }, function froundCB(err, record) {
      if (record && record.access_token && record.refresh_token) {
        req.getCurrentMailAttachment(record.access_token, record.email, threadID, function (resp) {
          res.send(resp);
        });

      }
    });
  },

  /**
   * Description
   * @return
   * @method getMailThreads
   * @param {} req
   * @param {} res
   * @return 
   */
  getMailThreads: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var id = req.param('id');
    var Mailbox = req.param('Mailbox') || 'Inbox';
    var top = req.param('top') || 10;
    var skip = req.param('skip') || 0;

    var outlookMessageData = {};
    outlookMessageData.pagination = [];
    outlookMessageData.messages = [];

    EmailAccount.findOne({id: id, user: LoggedInUserID, provider: 'outlook'}, function froundCB(err, record) {
      if (record && record.access_token && record.refresh_token) {
        req.session.Outlook = record;
        var pagination = {};

        // Set New Refresh Token
        req.refreshAccessToken(record.refresh_token, function (error, newToken) {
          if (error) {
            console.log(error);
          }
          if (newToken) {
            EmailAccount.update({
              id: id,
              user: LoggedInUserID,
              provider: 'outlook'
            }, {
              access_token: newToken.token.access_token,
              refresh_token: newToken.token.refresh_token,
              id_token: newToken.token.id_token,
              expires_at: newToken.token.expires_at
            }).exec(function afterwards(err, updated) {
              if (err) console.log(err);
              // console.log('updated');
            });
            req.getEmails(newToken.token.access_token, record.email, Mailbox, top, skip, function (resp) {
              // res.send(resp);
              if (resp) {
                pagination.provider = 'outlook';
                pagination.id = record.id;
                pagination.skip = skip + top;
                outlookMessageData.pagination.push(pagination);
                MailService.outlookExtract(resp, outlookMessageData, record.email, record.id, function (mailData) {
                  res.send(mailData);
                });
              } else {
                res.send({message: 'No Message Found'});
              }
            });
          }
        });
        // Ne New
      }
    });
  },

  /**
   * Description
   * @return
   * @method getMailFolders
   * @param {} req
   * @param {} res
   * @return 
   */
  getMailFolders: function (req, res) {
    'use strict';
    var LoggedInUserID = req.session.User.id;
    var id = req.param('id');
    var outlookMailboxData = [];

    EmailAccount.findOne({id: id, user: LoggedInUserID, provider: 'outlook'}, function froundCB(err, record) {
      if (record && record.access_token && record.refresh_token) {
        // Outlook Mail Query
        req.getMailFolders(record.access_token, record.email, function (resp) {
          res.send(resp);
        });
      }
    });
  },

  /**
   * Get Mail/Thread Details
   * @return
   * @method getMailThreadDetails
   * @param req
   * @param res
   * @return 
   */
  getMailThreadDetails: function (req, res) {
    'use strict';
    var id = req.param('id');
    var LoggedInUserID = req.session.User.id;
    var threadID = req.param('threadID');

    EmailAccount.findOne({id: id, user: LoggedInUserID, provider: 'outlook'}, function froundCB(err, record) {
      if (record && record.access_token && record.refresh_token) {
        // Outlook Mail Query

        req.getEmail(record.access_token, record.email, threadID, function (resp) {
          res.send(resp);
        });

      }
    });
  },

  /**
   * Description
   * @return
   * @method modifyThread
   * @param {} req
   * @param {} res
   * @return 
   */
  modifyThread: function (req, res) {
    'use strict';
    var AsyncCall = require('async');
    var LoggedInUserID = req.session.User.id;
    var mailBoxes = req.param('idStr');
    var updateData = req.param('updateData');

    if (req.param('idStr')) {
      mailBoxes = req.param('idStr').split(',');
    }
    else {
      mailBoxes = [];
    }
    EmailAccount.findOne({
      id: req.param('id'),
      user: LoggedInUserID,
      provider: 'outlook'
    }, function froundCB(err, record) {
      if (record && record.access_token && record.refresh_token) {
        var DeletedMailID = [];
        if (mailBoxes) {
          AsyncCall.forEach(mailBoxes, function (mailId, next) {
            req.getEmailUpdate(record.access_token, record.email, mailId, updateData, function (resp) {
              if (resp) {
                DeletedMailID.push(mailId);
                next();
              }
            });
          }, function (err) {
            if (err) {
              console.log(err);
            } else {
              res.send(DeletedMailID);
            }
          });
        } else {
          res.send('can not');
        }
      }
    });

  },


  /**
   * Description
   * @return
   * @method getMailMoveToFolder
   * @param {} req
   * @param {} res
   * @return 
   */
  getMailMoveToFolder: function (req, res) {
    'use strict';

    var LoggedInUserID = req.session.User.id;
    var id = req.param('id');
    var threadID = req.param('threadID');
    var add = req.param('add');
    var remove = req.param('remove');

    console.log(id);
    console.log(threadID);
    console.log(add);
    console.log(remove);

    EmailAccount.findOne({
      id: req.param('id'),
      user: LoggedInUserID,
      provider: 'outlook'
    }, function froundCB(err, record) {
      if (record && record.access_token && record.refresh_token) {
        if (add && add === 'No' && remove) {
          if (remove === 'Inbox') {
            req.moveOrCopyMessageTo(record.access_token, record.email, threadID, 'Archive', 'move', function (resp) {
              res.send({success: 'Moved'});
            });
          } else if (remove === 'JunkEmail') {
            req.moveOrCopyMessageTo(record.access_token, record.email, threadID, 'Inbox', 'move', function (resp) {
              res.send({success: 'Moved'});
            });
          } else if (remove === 'Unread') {
            req.getEmailUpdate(record.access_token, record.email, threadID, {"IsRead": true}, function (resp) {
              res.send({success: 'Done'});
            });
          } else if (add === 'Important') {
            req.getEmailUpdate(record.access_token, record.email, threadID, {"Importance": "Normal"}, function (resp) {
              res.send({success: 'Done'});
            });
          } else if (add === 'Starred') {
            res.send({success: 'Outlook don\'t have star option.'});
          }

        } else if (add && !remove) {
          if (add === 'JunkEmail') {
            req.moveOrCopyMessageTo(record.access_token, record.email, threadID, 'JunkEmail', 'move', function (resp) {
              res.send({success: 'Moved'});
            });
          } else if (add === 'DeletedItems') {
            req.moveOrCopyMessageTo(record.access_token, record.email, threadID, 'DeletedItems', 'move', function (resp) {
              res.send({success: 'Moved'});
            });
          } else if (add === 'Inbox') {
            req.moveOrCopyMessageTo(record.access_token, record.email, threadID, 'Inbox', 'move', function (resp) {
              res.send({success: 'Moved'});
            });
          } else if (add === 'SentItems') {
            req.moveOrCopyMessageTo(record.access_token, record.email, threadID, 'SentItems', 'move', function (resp) {
              res.send({success: 'Moved'});
            });
          } else if (add === 'Drafts') {
            req.moveOrCopyMessageTo(record.access_token, record.email, threadID, 'Drafts', 'move', function (resp) {
              res.send({success: 'Moved'});
            });
          } else if (add === 'Unread') {
            req.getEmailUpdate(record.access_token, record.email, threadID, {"IsRead": false}, function (resp) {
              res.send({success: 'Done'});
            });
          } else if (add === 'Important') {
            req.getEmailUpdate(record.access_token, record.email, threadID, {"Importance": "High"}, function (resp) {
              res.send({success: 'Done'});
            });
          } else if (add === 'Starred') {
            res.send({success: 'Outlook don\'t have star option.'});
          }
        } else if (add && add !== 'No' && remove) {
          if (add === 'JunkEmail') {
            req.moveOrCopyMessageTo(record.access_token, record.email, threadID, 'JunkEmail', 'move', function (resp) {
              res.send({success: 'Moved'});
            });
          } else if (add === 'DeletedItems') {
            req.moveOrCopyMessageTo(record.access_token, record.email, threadID, 'DeletedItems', 'move', function (resp) {
              res.send({success: 'Moved'});
            });
          }
        } else {
          res.send({error: 'No Action'})
        }
      } else {
        res.send({error: 'No Message Found'});
      }
    });

  },

  /**
   * Description
   * @return
   * @method getMailThreadDelete
   * @param {} req
   * @param {} res
   * @return 
   */
  getMailThreadDelete: function (req, res) {
    'use strict';
    var LoggedInUserID = req.session.User.id;
    var mailBoxes;

    if (req.param('mailBoxId')) {
      mailBoxes = req.param('mailBoxId').split(',');
    }
    else {
      mailBoxes = [];
    }

    // console.log(mailBoxes);

    mailBoxes.forEach(function (mailId, index) {

      EmailAccount.findOne({
        id: req.param('id'),
        user: LoggedInUserID,
        provider: 'outlook'
      }, function froundCB(err, record) {
        if (record && record.access_token && record.refresh_token) {


          var DeletedMailID = 0;
          if (mailBoxes) {
            var AsyncCall = require('async');
            AsyncCall.forEach(mailBoxes, function (mailId, callback) {

              // Outlook Mail Query
              req.getEmailDelete(record.access_token, record.email, mailId, function (resp) {

                DeletedMailID = DeletedMailID + 1;
                console.log('deleted response');
                console.log(resp);
                callback();

              });

            }, function (err) {
              if (err) {
                console.log(err);
              }
              res.send(DeletedMailID);

            });
          }
        }
      });
    });


  },

  /**
   * Description
   * @return
   * @method remove
   * @param {} req
   * @param {} res
   * @return 
   */
  remove: function (req, res) {
    res.view("admin/outlook/remove", {
      'url': req.googleAuthUrl,
      'layout': 'admin/layout-outlook-permission'
    });
  },

  /**
   * Description
   * @return
   * @method removeEmailAccount
   * @param {} req
   * @param {} res
   * @return 
   */
  removeEmailAccount: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    EmailAccount.findOne({
      id: req.param('id'),
      user: LoggedInUserID,
      provider: 'outlook'
    }).exec(function (err, record) {
      if (record) {
        EmailAccount.destroy({id: req.param('id'), user: LoggedInUserID, provider: 'outlook'}).exec(function (err) {
          if (err) console.log(err);
          res.send('Removed');
        })
      } else {
        res.send('Can Not remove');
      }
    });
  },

  /**
   * Description
   * @method getOutlookAddresses
   * @param {} req
   * @param {} res
   * @return 
   */
  getOutlookAddresses: function (req, res) {
    EmailAccount.find({
      user: req.session.User.id,
      provider: 'outlook',
      select: ['_id', 'email', 'name', 'provider']
    }).exec(function foundCB(err, records) {
      res.send(records);
    });
  },

  /**
   * Description
   * @method getUnreadMailCount
   * @param {} req
   * @param {} res
   * @return 
   */
  getUnreadMailCount: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var id = req.param('id');
    var Mailbox = req.param('Mailbox') || 'Inbox';
    var top = 1;
    var skip = 0;
    var filter = req.param('filter') || "isread eq false";

    EmailAccount.findOne({
      id: id,
      user: LoggedInUserID,
      provider: 'outlook',
      select: ['email', 'refresh_token', 'access_token']
    }, function froundCB(err, record) {
      if (record) {
        req.refreshAccessToken(record.refresh_token, function (error, newToken) {
          if (error) {
            console.log(error);
          }
          if (newToken) {
            EmailAccount.update({
              id: id,
              user: LoggedInUserID,
              provider: 'outlook'
            }, {
              access_token: newToken.token.access_token,
              refresh_token: newToken.token.refresh_token,
              expires_at: newToken.token.expires_at
            }).exec(function afterwards(err, updated) {
              if (err) console.log(err);
              // console.log('updated');
            });
            req.getMailFolder(newToken.token.access_token, record.email, Mailbox, function (resp) {
              if (resp) {
                res.send(resp);
              } else {
                res.send({message: 'No Message Found'});
              }
            });
          }
        });
      } else {
        res.send({message: 'No Email Account Found'});
      }
    });
  },

};

/**
 * Description
 * @method getOutlookCalendarEvents
 * @param {} token
 * @param {} emailData
 * @param {} top
 * @param {} callback
 * @return 
 */
function getOutlookCalendarEvents(token, emailData, top, callback){
  var queryParams = {
    // '$select': 'Subject,ReceivedDateTime,From,IsRead',
    // '$orderby': 'ReceivedDateTime desc',
    'startdatetime': moment().subtract(6, 'months').format('YYYY-MM-DD'),
    'enddatetime': moment().add(6, 'months').format('YYYY-MM-DD'),
    '$top': top
  };

  // Set the API endpoint to use the v2.0 endpoint
  outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
  // Set the anchor mailbox to the user's SMTP address
  outlook.base.setAnchorMailbox(emailData.email);

  outlook.calendar.getEvents({token: token, odataParams: queryParams},
    function (error, result) {
      if (error) {
        console.log('getEvents returned an error: ');
        console.log(error);
      }
      else if (result) {
        CalendarService.eventExtractFromOutlook(result, emailData, function (returnFromExtract) {
          if( returnFromExtract ){
            callback('done');
          }
        });
        // callback(result);
      }
    });
}


/**
 * Description
 * @method redirectUrlTo
 * @param {} userId
 * @param {} callback
 * @return 
 */
function redirectUrlTo( userId, callback ){
  Url.findOne({user: userId}).exec(function (err, respUrl) {
    if(err)console.log(err);
    if( respUrl && respUrl.module ){
      let callbackUrl;
      if( respUrl.module === 'calendar'){
        callbackUrl = '/calendar';
      }else {
        callbackUrl = '/mail/0/inbox';
      }
      callback(callbackUrl);
    }else{
      callback('/mail/0/inbox');
    }
  });
}
