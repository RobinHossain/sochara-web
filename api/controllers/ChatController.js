/*register**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var request = require('request');
const AsyncCall = require('async');

const giphyApiKey = 'dc6zaTOxFJmzC';
const moment = require('moment');

module.exports = {

  /**
   * Description
   * @method conversations
   * @param {} req
   * @param {} res
   * @return
   */
  conversations: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    Chat.find({users: LoggedInUserID}).populate('recipients', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url', 'email']}).exec(function (err, conversations) {
      if (err) {
        return next(err);
      }
      res.json(conversations);
    });
  },

  /**
   * Description
   * @method createNewGroup
   * @param {} req
   * @param {} res
   * @return
   */
  createNewGroup: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    let groupConversationData = {name: req.param('name') || undefined};
    groupConversationData.created_by = LoggedInUserID;
    groupConversationData.last_message_by = LoggedInUserID;
    Contacts.find({id: req.param('contacts'), select: ['sochara_user_id']}).exec(function (err, respContacts) {
      if( err ) console.log(err);
      let users = respContacts.map(user=>user.sochara_user_id);
      users.push(LoggedInUserID);
      groupConversationData.users = users;
      groupConversationData.recipients = users;
      groupConversationData.unread = {};
      groupConversationData.group = true;
      Chat.create(groupConversationData).exec(function (err, createdConv) {
        if (err) console.log(err);
        if( createdConv ){
          Chat.findOne({id: createdConv.id}).populate('created_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('last_message_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('recipients', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url', 'email']}).exec(function (err, conversationRec) {
            try {
              let involveUsers = conversationRec.users.filter(user=>user!==LoggedInUserID);
              involveUsers.forEach(function (usr) {
                sails.sockets.broadcast('sochara_' + usr, 'new_conversation', {data: conversationRec});
              });
            } catch (e){
              console.log(e);
            }
            let chat = conversationRec;
            chat.messages = [];
            res.send(chat);
          });
        }else{
          res.send(createdConv);
        }
      });
    });
  },

  /**
   * Description
   * @method getChatInitData
   * @param {} req
   * @param {} res
   * @return
   */
  getChatInitData: function (req, res) {
    let userId = req.session.User.id;
    let initData = {};
    AsyncCall.parallel([
        function(callback) {
          Chat.find({users: userId, last_message: {$exists: true}}).populate('recipients', {select: ['_id', 'first_name', 'last_name', 'mode', 'photo_url', 'email']}).exec(function (err, conversations) {
            if (err) {
              return next(err);
            }
            initData.conversations = conversations;
            callback();
          });
        },
        function(callback) {
          Contacts.find({is_sochara_user: true,
            status: "approve",
            user_id: userId,
            select: ['_id', 'first_name', 'last_name']}).populate('sochara_user_id', {select: ['_id', 'email', 'photo_url', 'mode', 'status']}).exec(function (err, respContacts) {
            initData.contacts = respContacts.map(contact=>{ return {id: contact.id, first_name: contact.first_name, last_name: contact.last_name, user_id: contact.sochara_user_id.id, email: contact.sochara_user_id.email, photo_url: contact.sochara_user_id.photo_url, mode: contact.sochara_user_id.mode, status: contact.sochara_user_id.status};});
            callback();
          });
        }
      ],
      function(err, results) {
        res.send(initData);
      });
  },

  /**
   * Description
   * @method conversationById
   * @param {} req
   * @param {} res
   * @return
   */
  conversationById: function (req, res) {
    let LoggedInUserID = req.session.User.id;
      Chat.findOne({id: req.param('id')}).populate('created_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('last_message_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('recipients', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url', 'email']}).exec(function (err, conversation) {
      if (err) {
        console.log(err);
      }
      if (conversation) {
        var chat = conversation;
        ChatMessage.find({conversation: chat.id}).populate('sender', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).exec(function (err, response) {
          if (err) {
            console.log(err);
          }
          chat.messages = response;
          res.json(chat);
        });
      } else {
        res.json('');
      }
    });
  },

  /**
   * Description
   * @method sendMail
   * @param {} req
   * @param {} res
   * @return
   */
  sendMail: function (req, res) {
    let userId = req.session.User.id;
    Contacts.findOne({id: req.param('id')}).exec(function (err, foundContact) {
      let mailgunData;
      if( foundContact ){
        mailgunData = {to: [foundContact.email], subject: 'Invitation from Sochara'};
        mailgunData.html = `Hi ${foundContact.first_name} ${foundContact.last_name}, you have a message request from Sochara. Register <a href="https://www.sochara.com/register">here</a>, to start message`;
        try {
          SendMailService.send(mailgunData, function (respSend) {

          });
        } catch (e) {
          console.log(e);
        }
        res.send(200);
      }else{
        res.send({message: 'contact not found'});
      }
    });
  },

  /**
   * Description
   * @method createGroupChat
   * @param {} req
   * @param {} res
   * @return
   */
  createGroupChat: function (req, res) {
    let conversation = req.param('conversation');
    let contacts = req.param('contacts');
    let LoggedInUserID = req.session.User.id;

    Chat.findOne(conversation).exec(function (err, conversationRecord) {
      if (err) {
        console.log(err);
      }

      let groupConversationData = {};
      groupConversationData.created_by = LoggedInUserID;
      groupConversationData.last_message_by = LoggedInUserID;
      let existingUsers = conversationRecord.users;
      contacts.forEach(function (contact) {
        existingUsers.push(contact.id);
      });
      groupConversationData.users = existingUsers;
      groupConversationData.recipients = existingUsers;
      groupConversationData.unread = {};
      groupConversationData.group = true;
      Chat.create(groupConversationData).exec(function (err, createdConv) {
        if (err) console.log(err);
        if( createdConv ){
          Chat.findOne({id: createdConv.id}).populate('created_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('last_message_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('recipients', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url', 'email']}).exec(function (err, conversationRec) {
            try {
              let involveUsers = conversationRec.users.filter(user=>user!==LoggedInUserID);
              involveUsers.forEach(function (usr) {
                sails.sockets.broadcast('sochara_' + usr, 'new_conversation', {data: conversationRec});
              });
            } catch (e){
              console.log(e);
            }
            let chat = conversationRec;
            chat.messages = [];
            res.send(chat);
          });
        }else{
          res.send(createdConv);
        }
      });
    });
  },

  /**
   * Description
   * @method makeUnreadEmpty
   * @param {} req
   * @param {} res
   * @return
   */
  makeUnreadEmpty: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    Chat.findOne({id: req.param('id')}).exec(function (err, conversation) {
      if (err) {
        console.log(err);
      }
      if( conversation ){
        var unread;
        if (conversation && conversation.unread) {
          unread = conversation.unread;
        } else {
          unread = {};
        }
        if (unread && Object.keys(unread).length > 0) {
          unread[LoggedInUserID] = 0;
        }

        Chat.update(conversation.id, { unread: unread}).exec(function afterwards(err, updated) {
          res.send(200);
        });
      }
    });
  },

  /**
   * Description
   * @method unreadCountTotal
   * @param {} req
   * @param {} res
   * @return
   */
  unreadCountTotal: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    Chat.find({$or: [{"created_by": LoggedInUserID}, {"last_message_by": LoggedInUserID}]}, {select: ['unread']}).exec(function (err, conversations) {
      if (err) {
        return next(err);
      }

      var totalCount = 0;

      conversations.forEach(function (conversation) {
        if (  conversation.unread && typeof conversation.unread[LoggedInUserID] !== 'undefined') {
          totalCount += parseInt(conversation.unread[LoggedInUserID]);
        }
      });
      // console.log(totalCount);
      res.json(totalCount);
    });
  },

  /**
   * Description
   * @method conversationByContactId
   * @param {} req
   * @param {} res
   * @return
   */
  conversationByContactId: function (req, res) {
    var LoggedInUserID = req.session.User.id;

    Chat.findOne({
      users: {$all: [LoggedInUserID, req.param('contact')]},
      group: false
    }).populate('created_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('last_message_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('recipients', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url', 'email']}).exec(function (err, fdrecord) {
      if (fdrecord) {
        var chat = fdrecord;
        ChatMessage.find({conversation: chat.id}).populate('sender', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).exec(function (err, response) {
          if (err) {
            console.log(err);
          }
          chat.messages = response;
          res.send(chat);
        });
      } else {

        let messageType = req.param('type');
        let messageBody;
        User.findOne({id: req.param('contact')}).exec(function (err, respContact) {
          if( err ) console.log(err);
          if( respContact ){
            // if( messageType && messageType === 'invite'){
            //   messageBody = 'Hi '+respContact.first_name + ' ' + respContact.last_name +', I\'d like to add you as a contact.';
            //   try {
            //     sendInvitationMessage({to: {email: respContact.email, name: respContact.first_name + ' ' + respContact.last_name}, from: {name: req.session.User.first_name + ' ' + req.session.User.last_name, email: req.session.User.email},}, function (resp) {
            //
            //     });
            //   }catch (e){
            //     console.log(e);
            //   }
            //   ContactsService.addContactRequest({loggedInUserId:LoggedInUserID, userId: req.param('contact')}, function (respContact) {
            //
            //   })
            // }

            let unreadObj = {};
            unreadObj[LoggedInUserID] = 0;
            unreadObj[req.param('contact')] = 0;
            let chatData = {created_by: LoggedInUserID, last_message_by: LoggedInUserID, last_message_time: moment().format(),recipients: [LoggedInUserID, req.param('contact')],users: [LoggedInUserID, req.param('contact')],unread: unreadObj,};
            if(messageType && messageType === 'invite'){
              chatData.pending = true;
            }
            Chat.create(chatData).exec(function (err, createdConv) {
              if( err ) console.log(err);
              // let messageData = {sender: LoggedInUserID, message_type: req.param('message_type') || 'chat', body: messageBody || '', conversation: createdConv.id, users: createdConv.users};
              Chat.findOne(createdConv.id).populate('created_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('last_message_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('recipients', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url', 'email']}).exec(function (err, conversationRec) {
                conversationRec.messages = [];
                res.send(conversationRec);
              });
            });
          }else{
            res.send(200);
          }
        });

      }
    });
  },

  /**
   * Description
   * @method sendContactRequest
   * @param {} req
   * @param {} res
   * @return
   */
  sendContactRequest: function (req, res) {
    let loggedInUserDetails = req.session.User;
    let chatId = req.param('id') || '';
    let messageBody = req.param('message') || ''; // 'Hi '+respContact.first_name + ' ' + respContact.last_name +', I\'d like to add you as a contact.';
    Chat.findOne({id: chatId}).populate('recipients', {select: ['_id', 'first_name', 'last_name', 'photo_url', 'email']}).exec(function (err, chatData) {
      if(chatData){
        let resUser = chatData.recipients.filter(function (user) {
          return user.id !== loggedInUserDetails.id;
        })[0];
        try {
          sendInvitationMessage({to: {email: resUser.email, name: resUser.first_name + ' ' + resUser.last_name}, from: {name: loggedInUserDetails.first_name + ' ' + loggedInUserDetails.last_name, email: loggedInUserDetails.email}}, function (resp) {

          });
        }catch (e){
          console.log(e);
        }
        ContactsService.addContactRequest({loggedInUserId:loggedInUserDetails.id, userId: resUser.id}, function (respContact) {

        });
        let messageData = {sender: loggedInUserDetails, message_type: 'chat', body: messageBody || '', conversation: chatData.id, users: chatData.users};
        ChatMessage.create(messageData).exec(function (err, createdMessage) {
          if( err ) console.log(err);
          Chat.update({id: chatId}, {last_message:messageBody}).exec(function (err, updatedChat) {
            if(err)console.log(err);
            Chat.findOne(chatData.id).populate('created_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('last_message_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('recipients', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url', 'email']}).exec(function (err, conversationRec) {
              conversationRec.messages = [createdMessage];
              res.send(conversationRec);
            });
          });
        });
      }else{
        res.send(200);
      }
    });
  },

  /**
   * Description
   * @method acceptRequest
   * @param {} req
   * @param {} res
   * @return
   */
  acceptRequest: function (req, res) {
    let loggedInUserId = req.session.User.id;
    let userId;
    let chatId = req.param('id');
    Chat.findOne({id: chatId}).populate('created_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).exec(function (err, chatData) {
      if( chatData ){
        userId = chatData.created_by.id;
        Contacts.update({is_sochara_user: true, sochara_user_id: loggedInUserId, status: 'pending', user_id: userId}, {status: 'approve'}).exec(function (err, updatedContacts) {
          if(err)console.log(err);
          if (updatedContacts && updatedContacts.length) {
            User.findOne({
              id: updatedContacts[0].user_id,
              select: ['_id', 'first_name', 'last_name', 'email', 'photo_url', 'mode']
            }).exec(function (err, respUser) {
              if(err)console.log(err);
              if (respUser) {
                Contacts.create({
                  user_id: updatedContacts[0].sochara_user_id,
                  status: 'approve',
                  is_sochara_user: true,
                  request_sent_by: respUser.id,
                  first_name: respUser.first_name,
                  last_name: respUser.last_name,
                  email: respUser.email,
                  sochara_user_id: respUser.id
                }).exec(function (err, respContact) {
                  if (err) console.log(err);
                  Chat.update({id: chatId}, {pending: false}).exec(function (err, respChat) {
                    if(err)console.log(err);
                    res.send(respContact);
                  });
                });
              }
            });
          }else{
            res.send(200);
          }
        });
      }else{
        res.send(200);
      }
    });
  },

  /**
   * Description
   * @method addMessage
   * @param {} req
   * @param {} res
   * @return
   */
  addMessage: function (req, res) {
    var LoggedInUserID = req.session.User.id;

    var conversationId = req.param('conversation');
    var recipientId = req.param('recipients');


    if (conversationId) {

      Chat.findOne(conversationId, function foundUsr(err, conversationData) {
        conversationData.unread = incUnreadMessageCountUsers(conversationData.users, conversationData.unread, LoggedInUserID);

        var data = {};
        data.sender = LoggedInUserID;
        data.message_type = req.param('message_type');
        data.body = req.param('body');
        data.conversation = conversationId;
        data.users = conversationData.users;
        ChatMessage.create(data).exec(function (err, record) {
          var last_message;
          if (record.message_type === 'sticker') {
            last_message = "Sent a Sticker";
          } else if (record.message_type === 'giphy') {
            last_message = "Sent a Giphy";
          } else if (record.message_type === 'attachment') {
            last_message = "Sent an Attachment";
          } else {
            last_message = record.body.replace('sc-2x', '');
          }
          ChatMessage.findOne({id: record.id}).populate('sender', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).exec(function (err, chatMessageResp) {
            try {
              if( conversationData.group ){
                let involveUsers = conversationData.users.filter(user=>user!==LoggedInUserID);
                involveUsers.forEach(function (usr) {
                  sails.sockets.broadcast('sochara_' + usr, 'new_message', {data: chatMessageResp});
                });
              }else{
                let singleUser = conversationData.users.filter(user=>user!==LoggedInUserID)[0];
                sails.sockets.broadcast('sochara_' + singleUser, 'new_message', {data: chatMessageResp});
              }
            } catch (e){
              console.log(e);
            }

            conversationData.last_message = last_message;
            conversationData.last_message_time = record.createdAt;
            conversationData.save(function (err, returnData) {
              res.send(record);
            });
          });
        });

      });
    } else {
      Chat.findOne({users: {$all: [LoggedInUserID, req.param('recipients')]}}).exec(function (err, fdrecord) {
        var data = {};
        data.sender = LoggedInUserID;
        data.message_type = req.param('message_type');
        data.body = req.param('body');
        if (fdrecord && fdrecord.users.length && fdrecord.users.indexOf(req.param('recipients')) > -1 && fdrecord.users.length === 2) {
          data.conversation = fdrecord.id;
          data.users = fdrecord.users;
          ChatMessage.create(data).exec(function (err, record) {
            var last_message;
            if (record.message_type === 'sticker') {
              last_message = "Sent a Sticker";
            } else if (record.message_type === 'giphy') {
              last_message = "Sent a Giphy";
            } else if (record.message_type === 'attachment') {
              last_message = "Sent a Attachment";
            } else {
              last_message = record.body.replace('sc-2x', '');
            }

            // var recipients = record_conversation.recipients;
            // recipients.push(req.param('recipients'));
            Chat.findOne(record.conversation, function foundUsr(err, conversationData) {

              try {
                if( conversationData.group ){
                  let involveUsers = conversationData.users.filter(user=>user!==LoggedInUserID);
                  involveUsers.forEach(function (usr) {
                    sails.sockets.broadcast('sochara_' + usr, 'new_message', {data: record});
                  });
                }else{
                  let singleUser = conversationData.users.filter(user=>user!==LoggedInUserID)[0];
                  sails.sockets.broadcast('sochara_' + singleUser, 'new_message', {data: record});
                }
              } catch (e){
                console.log(e);
              }

              // New Conversation
              sails.sockets.broadcast('sochara_' + usr, 'new_message', {data: chatMessageResp});

              // conversationData.unread = conversationData.unread + 1;
              var unreadMsg = incUnreadMessageCountUsers(conversationData.users, conversationData.unread, LoggedInUserID);
              Chat.update(conversationData.id, {
                last_message: last_message,
                last_message_time: record.createdAt,
                unread: unreadMsg
              }).exec(function afterwards(err, updated) {
                // ChatMessage.findOne({id: record.id}).populate('sender', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).exec(function (err, chatMessageResp) {
                //
                // });
                Chat.findOne(updated[0].id).populate('created_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('last_message_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('recipients', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url', 'email']}).exec(function (err, conversationRec) {
                  res.send(conversationRec);
                });
              });

            });
          });
        } else {
          Chat.create({
            created_by: LoggedInUserID,
            last_message_by: LoggedInUserID,
            recipients: [LoggedInUserID, recipientId],
            users: [LoggedInUserID, recipientId]
          }).exec(function (err, record_conversation) {
            if (record_conversation) {
              data.conversation = record_conversation.id;
              data.users = record_conversation.users;
              ChatMessage.create(data).exec(function (err, record) {
                var last_message;
                if (record.message_type === 'sticker') {
                  last_message = "Sent a Sticker";
                } else if (record.message_type === 'giphy') {
                  last_message = "Sent a Giphy";
                } else if (record.message_type === 'attachment') {
                  last_message = "Sent a Attachment";
                } else {
                  last_message = record.body.replace('sc-2x', '');
                }

                try {
                  if( record_conversation.group ){
                    let involveUsers = record_conversation.users.filter(user=>user!==LoggedInUserID);
                    involveUsers.forEach(function (usr) {
                      sails.sockets.broadcast('sochara_' + usr, 'new_conversation', {data: record_conversation});
                    });
                  }else{
                    let singleUser = record_conversation.users.filter(user=>user!==LoggedInUserID)[0];
                    sails.sockets.broadcast('sochara_' + singleUser, 'new_conversation', {data: record_conversation});
                  }
                } catch (e){
                  console.log(e);
                }

                Chat.findOne(record.conversation, function foundUsr(err, conversationData) {


                  try {
                    if( conversationData.group ){
                      let involveUsers = conversationData.users.filter(user=>user!==LoggedInUserID);
                      involveUsers.forEach(function (usr) {
                        sails.sockets.broadcast('sochara_' + usr, 'new_message', {data: record_conversation});
                      });
                    }else{
                      let singleUser = conversationData.users.filter(user=>user!==LoggedInUserID)[0];
                      sails.sockets.broadcast('sochara_' + singleUser, 'new_message', {data: record_conversation});
                    }
                  } catch (e){
                    console.log(e);
                  }

                  var unreadMsg = incUnreadMessageCountUsers(conversationData.users, conversationData.unread, LoggedInUserID);
                  Chat.update(record.conversation, {
                    last_message: last_message,
                    last_message_time: record.createdAt,
                    unread: unreadMsg
                  }).exec(function afterwards(err, updated) {

                    // ChatMessage.findOne({id: record.id}).populate('sender', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).exec(function (err, chatMessageResp) {
                    //
                    // });
                    Chat.findOne(updated[0].id).populate('created_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('last_message_by', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url']}).populate('recipients', {select: ['_id', 'first_name', 'last_name', 'mode', 'status', 'photo_url', 'email']}).exec(function (err, conversationRec) {
                      res.send(conversationRec);
                    });
                  });
                });
              });
            }
          });
        }
      });

    }

  },

  /**
   * Description
   * @method getGiphySticker
   * @param {} req
   * @param {} res
   * @return
   */
  getGiphySticker: function (req, res) {
    var limit = req.param('limit') || '';
    var offset = req.param('offset') || '';
    var url = 'https://api.giphy.com/v1/stickers/trending' + '?api_key=' + giphyApiKey + '&limit=' + limit + '&offset=' + offset;

    request(url, function (error, response) {
      if (error) {
        console.log(error);
      } else if (response) {
        res.send(response.body);
      }
    });
  },

  /**
   * Description
   * @method changeUserStatus
   * @param {} req
   * @param {} res
   * @return
   */
  changeUserStatus: function (req, res) {
    let LoggedInUserID = req.session.User.id;
    let status = req.param('status') || '';
    User.update({id: LoggedInUserID}, {status: status}).exec(function (err, respUpdate) {
      if(err) console.log(err);
      res.send(respUpdate[0].status);
    })
  },

  /**
   * Description
   * @method leaveGroup
   * @param {} req
   * @param {} res
   * @return
   */
  leaveGroup: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    Chat.findOne(req.param('conversation')).exec(function (err, conversation) {
      if (conversation) {
        var users = conversation.users;
        var userIndex = users.indexOf(LoggedInUserID);
        if (userIndex > -1) {
          users.splice(userIndex, 1);
          Chat.update(req.param('conversation'), {recipients: users, users: users}).exec(function (err, conversation) {
            res.send(conversation[0]);
          });
        }
      } else {
        res.send('not found any existing data');
      }
    });
  },

  /**
   * Description
   * @method muteConversation
   * @param {} req
   * @param {} res
   * @return
   */
  muteConversation: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    Chat.findOne(req.param('id')).exec(function (err, conversation) {
      if (conversation) {
        var mute = conversation.mute || [];
        var mswitch = req.param('mswitch');
        if (mswitch === 'on') {
          mute.push(LoggedInUserID);
        } else if (mswitch === 'off') {
          var mLocation = mute.indexOf(LoggedInUserID);
          if (mLocation > -1) {
            mute.splice(mLocation);
          }
        }

        Chat.update(req.param('conversation'), {mute: mute}).exec(function (err, conversation) {
          res.send(conversation[0]);
        });
      } else {
        res.send('not found any existing data');
      }
    });
  },

  /**
   * Description
   * @method getGiphyStickerByTag
   * @param {} req
   * @param {} res
   * @return
   */
  getGiphyStickerByTag: function (req, res) {
    var limit = req.param('limit') || '';
    var offset = req.param('offset') || '';
    var tag = req.param('q') || [];
    var url = 'https://api.giphy.com/v1/stickers/search' + '?api_key=' + giphyApiKey + '&limit=' + limit + '&offset=' + offset + '&q=' + tag;

    request(url, function (error, response) {
      if (error) {
        console.log(error);
      } else if (response) {
        res.send(response.body);
      }
    });
  },

  /**
   * Description
   * @method deleteConversation
   * @param {} req
   * @param {} res
   * @return
   */
  deleteConversation: function (req, res) {
    var chatId = req.param('id');
    Chat.destroy(chatId).exec(function (err) {
      if( err ){
        console.log(err);
      }
      ChatMessage.destroy({ conversation: chatId} ).exec(function (err) {
        if( err ){
          console.log(err);
        }
        res.send(200);
      });
    });
  }

};

/**
 * Description
 * @method incUnreadMessageCountUsers
 * @param {} users
 * @param {} unread
 * @param {} loggedIn
 * @return unread
 */
function incUnreadMessageCountUsers(users, unread, loggedIn) {
  'use strict';
  if (!unread) {
    unread = {};
  }
  users.forEach(function (user) {
    if (user !== loggedIn) {
      if (unread[user]) {
        unread[user] = parseInt(unread[user]) + 1;
      } else {
        unread[user] = 1;
      }
    } else {
      unread[user] = 0;
    }
  });
  // console.log(unread);
  return unread;
}

/**
 * Description
 * @method sendInvitationMessage
 * @param {} mailData
 * @param {} callback
 * @return
 */
function sendInvitationMessage( mailData, callback ){
  let mailgunData = {
    to: [mailData.to.email],
    from: mailData.from.name + ' <'+ mailData.from.email +'>',
    subject: 'Chat invitation from Sochara',
    html: `Hi ${mailData.to.name}, It's ${mailData.from.name}, I'd like to add you as a contact. To chat with me register Sochara from from <a href='https://www.sochara.com/auth/register'>here</a>.`
  };

  try {
    SendMailService.send(mailgunData, function (respSend) {
      console.log(respSend);
    });
  }catch (e){
    console.log(e);
  }
  callback(null, 'something');
}
