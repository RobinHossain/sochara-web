/**
 * Created by ASUS on 08-May-17.
 */
var twitterAPI = require('node-twitter-api');
var instagramApi = require('instagram-node').instagram();
var FB = require('fb');
var Twitter = require('twitter');
let moment = require('moment');

instagramApi.use({
  client_id: '0ab9e25c89494bc4ae8b8743e34b0ca7',
  client_secret: '7f99e08f3b8f46adb3e4146eb5e026db'
});


module.exports = {

  /**
   * Description
   * @method socialAccounts
   * @param {} req
   * @param {} resp
   * @return 
   */
  socialAccounts: function ( req, resp ) {
    let userId = req.session.User.id;
    SocialAuth.find({user: userId}).exec(function ( respSocialAccounts ) {
      if( respSocialAccounts && respSocialAccounts.length ){
        res.send(respSocialAccounts);
      }
    });
  },

  /**
   * Description
   * @method facebook
   * @param {} req
   * @param {} res
   * @return 
   */
  facebook: function (req, res) {
    // var facebookNpm = require('fb');
    let request = require('request');
    let loggedInUserId = req.session.User.id;
    let savingData = {};
    savingData.user = loggedInUserId;
    savingData.providerId = req.body.userID;
    savingData.accountName = req.body.name;
    savingData.accessArea = req.body.accessArea;




    // Last updated - by Robin
    // savingData.accessToken = req.param('accessToken');

    let tmpAccessToken = req.body.accessToken;
    // request.get('https://graph.facebook.com/' + req.body.userID + '/picture?type=small');
    request.get('https://graph.facebook.com/oauth/access_token?client_id=1589011794492434&client_secret=4bd36c109d167cb1b161e76a1a68f81d&grant_type=fb_exchange_token&%20fb_exchange_token=' + tmpAccessToken, function (err, response, body) {
    // request.get('https://graph.facebook.com/oauth/access_token?client_id=294059841030709&client_secret=05415bc122a6e9fe8558e65a927db05e&grant_type=fb_exchange_token&%20fb_exchange_token=' + tmpAccessToken, function (err, response, body) {
      if (err)console.log(err);
      if (body) {
        body = JSON.parse(body);
        // Commented - updated by Robin
        // savingData.accessToken = body.access_token;
        savingData.provider = 'facebook';
        savingData.action = 'authorized';
        savingData.accessToken = body.access_token;
        savingData.expires_in = body.expires_in;
        savingData.expire_date = moment().add(savingData.expires_in, 'seconds').format('YYYY-MM-DD HH:mm:ss');
        SocialAuth.findOne({user: loggedInUserId, provider: 'facebook'}).exec(function (err, data) {
          if (err) {
            return res.serverError(err);

          }
          if (!data) {
            SocialAuth.create(savingData).exec(function (err, createdData) {
              if (err) {
                return res.serverError(err);

              }
              if (createdData) {
                let accessFields = Object.keys(createdData.accessArea).filter(val => createdData.accessArea[val] && val !== 'feedPostPermission');
                accessFields = accessFields.length ? accessFields.join(',') : '';
                FB.setAccessToken(createdData.accessToken);
                FB.api('me/posts' + '?fields=' + 'attachments{description,media,target,title,url,type,description_tags,subattachments},source,message,story,type,link,full_picture,description,icon,name,place,from,created_time,application' + '&limit=' + 100, function (response) {
                  if (!response || response.error) {
                    console.log(!response ? 'error occurred' : response.error);
                  }

                  if( response ){
                    DashboardService.facebookPostExtract(response, createdData, function (returnFromFacebook) {
                      if (returnFromFacebook) {
                        res.send (200);
                      }
                    });
                  }
                });
              }
            });
          }else {
            SocialAuth.update({id: data.id}, savingData).exec(function (err, updatedData) {
              if (err) {
                return res.serverError(err);

              }
              if (updatedData) {
                res.send({msg: 'success'});
              }
            });
          }
        });

      }
    });

  },

  /**
   * Description
   * @method oAuthInstagramApi
   * @param {} req
   * @param {} res
   * @return 
   */
  oAuthInstagramApi: function (req, res) {
    // let redirect_uri;
    // if( req.headers.host === 'www.sochara.com' || req.headers.host === 'sochara.com' ){
    //   redirect_uri = 'https://' + req.headers.host + '/oAuthInstagramApiToken';
    // }else{
    //   redirect_uri = 'http://' + req.headers.host + '/oAuthInstagramApiToken';
    // }
    // instagramApi.authorize_user(req.query.code, redirect_uri, function(err, result) {
    //   if (err) {
    //     console.log(err.body);
    //     res.send("Didn't work");
    //   } else {
    //     console.log('Yay! Access token is ' + result.access_token);
    //     res.send('You made it!!');
    //   }
    // });
  },

  /**
   * Description
   * @method oAuthInstagramApiToken
   * @param {} req
   * @param {} res
   * @return 
   */
  oAuthInstagramApiToken: function (req, res) {
    console.log('Options');
    console.log(req.query.code);
  },

  /**
   * Description
   * @method oAuthForInstagram
   * @param {} req
   * @param {} res
   * @return 
   */
  oAuthForInstagram: function (req, res) {
    let redirect_uri;
    if( req.headers.host === 'www.sochara.com' || req.headers.host === 'sochara.com' ){
      redirect_uri = 'https://' + req.headers.host + '/oAuthInstagramApi';
    }else{
      redirect_uri = 'http://' + req.headers.host + '/oAuthInstagramApi';
    }

    res.redirect(instagramApi.get_authorization_url(redirect_uri, { scope: ['likes'], state: 'a state' }));

  },

  /**
   * Description
   * @method oAuthTwitter
   * @param {} req
   * @param {} res
   * @return 
   */
  oAuthTwitter: function (req, res) {

    let url;
    if( req.headers.host === 'www.sochara.com' || req.headers.host === 'sochara.com' ){
      url = 'https://' + req.headers.host + '/global-setting';
    }else{
      url = 'http://' + req.headers.host + '/global-setting';
    }

    let twitter = new twitterAPI({
      consumerKey: '2ourwWtAYQBvpUAn150SXe9oY',
      consumerSecret: 'ypZfC5xdrEyqVTycFvMrV80F7asJnUio5Bswp60PyOzQF4cfPg',
      callback: url
    });
    twitter.getRequestToken(function (error, requestToken, requestTokenSecret, results) {
      if (error) {
        return res.serverError(error);
      } else {
        req.session.twitterRequestTokenSecret = requestTokenSecret;
        res.send({msg: requestToken});
        //store token and tokenSecret somewhere, you'll need them later; redirect user
      }
    });
  },
  /**
   * Description
   * @method oAuthInstagram
   * @param {} req
   * @param {} res
   * @return 
   */
  oAuthInstagram: function (req, res) {
    var accessToken = req.param('accessToken');
    var request = require('request');
    request.get('https://api.instagram.com/v1/users/self?access_token=' + accessToken, function (err, response, body) {
      if (err) {
        return res.serverError(err);
      }
      if (body) {
        var bodyData = JSON.parse(body);
        if (bodyData.meta.code === 200) {
          var savedData = {};
          savedData.user = req.session.User.id;
          savedData.accessToken = accessToken;
          savedData.profilePicture = bodyData.data.profile_picture;
          savedData.userName = bodyData.data.full_name;
          savedData.provider = 'instagram';
          savedData.providerId = bodyData.data.id;
          SocialAuth.findOne({user: req.session.User.id, provider: 'instagram'}).exec(function (err, foundData) {
            if (err) {
              return res.serverError(err);
            }
            if (foundData) {
              SocialAuth.update({id: foundData.id}, savedData).exec(function (err, insertedData) {
                if (err) {
                  return res.serverError(err);
                }
                if (insertedData) {
                  res.send({msg: 'successful'});
                }
              });
            }
            if (!foundData) {
              SocialAuth.create(savedData).exec(function (err, createdData) {
                if (err) {
                  return res.serverError(err);
                }
                if (createdData) {
                  instagramApi.use({access_token: createdData.accessToken});
                  instagramApi.user_media_recent(createdData.providerId, {count: 50}, function (err, medias, pagination, remaining, limit) {
                    DashboardService.instagramPostExtract(medias, createdData, function (returnFromInstagram) {
                      var returnedData = {};
                      returnedData.name = createdData.userName;
                      returnedData.profilePicture = createdData.profilePicture;
                      res.send({msg: returnedData});
                    });
                  });
                }
              });
            }
          });
        }
        else {
          return res.serverError('failed');
        }
      }
    });
  },
  /**
   * Description
   * @method newTwitterOuth
   * @param {} req
   * @param {} res
   * @return 
   */
  newTwitterOuth: function (req, res) {
    var loggedInUserId = req.session.User.id;
    var requestToken = req.param('oauthToken');
    var oauth_verifier = req.param('oauthTokenVerifier');
    var requestTokenSecret = req.session.twitterRequestTokenSecret;
    var twitter = new twitterAPI({
      consumerKey: '2ourwWtAYQBvpUAn150SXe9oY',
      consumerSecret: 'ypZfC5xdrEyqVTycFvMrV80F7asJnUio5Bswp60PyOzQF4cfPg',
      callback: 'https://www.sochara.com/profile'
    });
    twitter.getAccessToken(requestToken, requestTokenSecret, oauth_verifier, function (error, accessToken, accessTokenSecret, results) {
      if (error) {
        return res.serverError(error);
      }
      else {
        var TwitterCore = require('twitter');
        var client = new TwitterCore({
          //consumer_key: 'rtf2UmoSnKmFcDC25tzO2syUu',
          //consumer_secret: 'NtQpIJA5oLHvdm30cGo3nsygWPz8Hkc3AVOpkFEZ5XxRptwn2g',
          consumer_key: '2ourwWtAYQBvpUAn150SXe9oY',
          consumer_secret: 'ypZfC5xdrEyqVTycFvMrV80F7asJnUio5Bswp60PyOzQF4cfPg',
          access_token_key: accessToken,
          access_token_secret: accessTokenSecret
        });

        var params = {skip_status: true};
        client.get('account/verify_credentials', params, function (error, userCredentials, response) {
          if (error) {
            console.log(error);
          }
          if (!error) {
            SocialAuth.findOne({user: loggedInUserId, screenName: userCredentials.name, provider: 'twitter'}).exec(function (err, foundData) {
              if (err) {
                return res.serverError(err);
              }
              if (foundData) {
                SocialAuth.update({id: foundData.id}, {
                  accessToken: accessToken,
                  accessTokenSecret: accessTokenSecret,
                  screenName: userCredentials.screen_name
                }).exec(function (err, updatedData) {
                  if (err) {
                    return res.serverError(err);
                  }
                  res.send({msg: {screen_name: updatedData.screenName}});
                });
              }
              if (!foundData) {
                SocialAuth.create({
                  user: loggedInUserId,
                  accessToken: accessToken,
                  accessTokenSecret: accessTokenSecret,
                  screenName: userCredentials.screen_name,
                  userName: userCredentials.name,
                  provider: 'twitter'
                }).exec(function (err, createdData) {
                  if (err) {
                    return res.serverError(err);
                  }
                  let client = new Twitter({
                    consumer_key: '2ourwWtAYQBvpUAn150SXe9oY',
                    consumer_secret: 'ypZfC5xdrEyqVTycFvMrV80F7asJnUio5Bswp60PyOzQF4cfPg',
                    access_token_key: createdData.accessToken,
                    access_token_secret: createdData.accessTokenSecret
                  });
                  let params = {exclude_replies: true};
                  client.get('statuses/user_timeline', params, function (error, tweets) {
                    DashboardService.twitterPostExtract(tweets, createdData, function (returnFromTwitter) {
                      res.send({msg: {screen_name: createdData.screenName}});
                    });
                  });

                });
              }
            });
          }
        });
      }
    });
  },
  /**
   * Description
   * @method deAuthorizeTwitterOauth
   * @param {} req
   * @param {} res
   * @return 
   */
  deAuthorizeTwitterOauth: function (req, res) {
    var loggedInUserId = req.session.User.id;
    SocialAuth.destroy({user: loggedInUserId, provider: 'twitter'}).exec(function (err) {
      if (err) {
        return res.serverError(err);
      }
      SocialData.destroy({user: loggedInUserId, provider: 'twitter'}).exec(function (err) {
        if( err ) console.log(err);
        res.send({msg: 'success'});
      });
    });
  },
  /**
   * Description
   * @method socialAccountAuthCheck
   * @param {} req
   * @param {} res
   * @return 
   */
  socialAccountAuthCheck: function (req, res) {
    var loggedInUserId = req.session.User.id;

    SocialAuth.find({user: loggedInUserId}).exec(function (err, respSocialAccounts) {
      if (err) {
        return res.serverError(err);
      }
      res.send(respSocialAccounts);
    });
  },
  /**
   * Description
   * @method rmFbPermission
   * @param {} req
   * @param {} res
   * @return 
   */
  rmFbPermission: function (req, res) {
    var loggedInUserId = req.session.User.id;
    SocialAuth.findOne({'$and': [{user: loggedInUserId}, {provider: 'facebook'}]}).exec(function (err, data) {
      if (err) {
        return res.serverError(err);

      }
      if (data) {
        var scope = req.param('permissionScope');
        var access_token = data.accessToken;
        var userId = data.providerId;
        var request = require('request');
        request.delete("https://graph.facebook.com/" + userId + '/permissions/' + scope + '?access_token=' + access_token, function (err, response, body) {

          if (err) {
            res.send({err: 'failed'});
          }
          if (scope === 'publish_actions') {
            delete data.accessArea.feedPostPermission;
          }
          if (scope === 'user_posts') {
            delete data.accessArea.feed;
          }
          if (scope === 'user_actions.music') {
            delete data.accessArea.music;
          }
          if (scope === 'user_actions.video') {
            delete data.accessArea.movies;
          }
          if (scope === 'user_actions.books') {
            delete data.accessArea.books;
          }
          SocialAuth.update({id: data.id}, {accessArea: data.accessArea}).exec(function (err, updatedData) {
            if (err) {
              return res.serverError(err);
            }
            res.send({msg: 'success'});
          });

        });

      }
    });
  },
  /**
   * Description
   * @method removeAuthorization
   * @param {} req
   * @param {} res
   * @return 
   */
  removeAuthorization: function (req, res) {
    var loggedInUserId = req.session.User.id;
    SocialAuth.findOne({user: loggedInUserId, provider: 'facebook'}).exec(function (err, data) {
      if (err) {
        return res.serverError(err);
      }
      if (data) {
        var access_token = data.accessToken;
        var userId = data.providerId;
        var request = require('request');
        request.delete("https://graph.facebook.com/" + userId + '/permissions?access_token=' + access_token, function (err, response, body) {

          if (err) {
            return res.serverError(err);

          }

          SocialAuth.destroy({id: data.id}).exec(function (err) {
            if (err) {
              return res.serverError(err);
            }
            SocialData.destroy({user: loggedInUserId, provider: 'facebook'}).exec(function (err) {
              if( err ){
                console.log(err);
              }
              res.send(200);
            });
          });

        });

      }
    });
  },
  /**
   * Description
   * @method setFbPermission
   * @param {} req
   * @param {} res
   * @return 
   */
  setFbPermission: function (req, res) {
    var loggedInUserId = req.session.User.id;
    var scope = req.param('permissionScope');
    SocialAuth.findOne({'$and': [{user: loggedInUserId}, {provider: 'facebook'}]}).exec(function (err, data) {

      if (err) {
        console.log(err);
        return res.serverError(err);
      }

      if (data) {
        if (scope === 'publish_actions') {
          data.accessArea.feedPostPermission = true;
        }
        if (scope === 'user_posts') {
          data.accessArea.feed = true;
        }
        if (scope === 'user_actions.music') {
          data.accessArea.music = true;
        }
        if (scope === 'user_actions.video') {
          data.accessArea.movies = true;
        }
        if (scope === 'user_actions.books') {
          data.accessArea.books = true;
        }

        SocialAuth.update({id: data.id}, {accessArea: data.accessArea}).exec(function (err, updatedData) {
          if (err) {
            return res.serverError(err);
          }
          res.send({msg: 'success'});
        });
      }

    });
  },
  /**
   * Description
   * @method InstagramOauthCheck
   * @param {} req
   * @param {} res
   * @return 
   */
  InstagramOauthCheck: function (req, res) {
    SocialAuth.findOne({user: req.session.User.id, provider: 'instagram'}).exec(function (err, data) {
      if (err) {
        return res.serverError(err);
      }
      if (data) {
        var returnedData = {};
        returnedData.name = data.userName;
        returnedData.profilePicture = data.profilePicture;
        res.send({msg: returnedData});
      }
      if (!data) {
        return res.send('');
      }
    });
  },

  /**
   * Description
   * @method rmInstagramAuth
   * @param {} req
   * @param {} res
   * @return 
   */
  rmInstagramAuth: function (req, res) {
    SocialAuth.destroy({user: req.session.User.id, provider: 'instagram'}).exec(function (err) {
      if (err) {
        return res.serverError(err);
      }
      res.send({msg: 'successful'});
    });
  }
};


/**
 * Description
 * @method getActualRequestLink
 * @param {} host
 * @param {} secure
 * @return ConditionalExpression
 */
function getActualRequestLink( host, secure ){
  return secure ? 'https://'+ host : 'http' + host;
}

