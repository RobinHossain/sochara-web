/**
 * Gmail Controller
 *
 * @description :: Server-side logic for managing gmail
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

// Include Require Module
var inbox = require("inbox");
var URLSafeBase64 = require('urlsafe-base64');
var AsyncCall = require('async');
const google = require('googleapis');


const moment = require('moment');

module.exports = {

  /**
   * Description
   * @method run
   * @return
   */
  run: function () {
    EmailAccount.find({
      provider: 'gmail',
      select: ['_id', 'email', 'name', 'provider']
    }).exec(function foundCB(err, records) {
      console.log(records);
    });
  },

  /**
   * Description
   * @method getGMailAddresses
   * @param {} req
   * @param {} res
   * @return
   */
  getGMailAddresses: function (req, res) {
    EmailAccount.find({
      user: req.session.User.id, provider: 'gmail',
      select: ['_id', 'email', 'name', 'provider']
    }).exec(function foundCB(err, records) {
      res.send(records);
    });
  },

  /**
   * Description
   * @method getListsOfMailboxes
   * @param {} req
   * @param {} res
   * @return
   */
  getListsOfMailboxes: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    GmailService.getListsOfMailboxes(LoggedInUserID, req.param('id'), req, res, function (data) {
      res.send(data.labels);
    });
  },

  /**
   * Description
   * @return
   * @method oauth2callback
   * @param {} req
   * @param {} res
   * @return
   */
  oauth2callback: function (req, res) {
    var code = req.param('code');
    let LoggedInUserID = req.session.User.id;
    if (code) {

      req.oauth2Client.getToken(code, function (err, tokens) {

        if (err) {
          console.log(err);
          res.json({
            'err': err
          });
        } else {
          req.oauth2Client.setCredentials(tokens);

          //for storing email address to DB reading profile
          req.plus.people.get({userId: 'me', auth: req.oauth2Client}, function (err, profile) {
            if (err) {
              res.json({
                'err': err
              });
            } else {
              //Saving access token and refresh token to DB

              EmailAccount.findOne({
                user: LoggedInUserID,
                email: profile.emails[0].value, provider: 'gmail'
              }, function foundCB(err, record) {
                if (record) {
                  redirectUrlTo(LoggedInUserID, function (url) {
                    res.redirect(url);
                  });
                  // res.redirect('/mail/0/inbox');
                } else {
                  EmailAccount.create({
                    email: profile.emails[0].value,
                    name: profile.displayName,
                    provider: 'gmail',
                    access_token: tokens.access_token,
                    token_type: tokens.token_type,
                    id_token: tokens.id_token,
                    refresh_token: tokens.refresh_token,
                    user: LoggedInUserID
                  }, function fileCreated(err, gcRecord) {
                    if (err) console.log(err);


                    // For Sochara Live
                    var CLIENT_ID = '678407714922-1auh2t76gvk479sn1m02g4omt22oo0in.apps.googleusercontent.com';
                    var CLIENT_SECRET = '9CdpLyvCF7HvZ2vDQ8TgK31j';
                    var REDIRECT_URL = 'https' + '://' + 'www.sochara.com' + '/gmail/oauth2callback';

                    var OAuth2Client = google.auth.OAuth2;

                    var gmail = google.gmail('v1');
                    var calender = google.calendar('v3');
                    var oauth2Client = new OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);

                    // Retrive Functionalities
                    oauth2Client.setCredentials({
                      access_token: gcRecord.access_token,
                      refresh_token: gcRecord.refresh_token
                    });
                    var $params = {};
                    $params.labelIds = ["INBOX"];
                    $params.maxResults = 20;
                    var pageToken = "";
                    if (pageToken && pageToken !== '') {
                      $params.pageToken = pageToken;
                    }

                    $params.userId = 'me';
                    $params.auth = oauth2Client;

                    gmail.users.threads.list($params, function (err, data) {
                      var threads = [];
                      if (err) console.log(err);
                      var AsyncCall = require('async');
                      if (data) {
                        threads = data.threads;
                        AsyncCall.forEachOf(threads, function (value, key, callback) {
                          if (value.id) {
                            gmail.users.threads.get({
                              id: value.id,
                              format: 'full',
                              userId: 'me',
                              auth: oauth2Client
                            }, function (err, body) {
                              if (err) console.log(err);
                              if (body && body.messages) {
                                threads[key]['metadata'] = body.messages;
                              }

                              callback();
                            });
                          } else {
                            callback();
                          }

                        }, function (err) {
                          if (err) console.log(err);
                          data.threads = threads;
                          if (data) {

                            MailService.gmailExtractAndStore(data, gcRecord.email, gcRecord.id, gcRecord.user, function (mailData) {
                              if (mailData) {
                                var gdata = {};
                                gdata.id = gcRecord.id;
                                gdata.nextPageToken = data.nextPageToken;
                                gdata.maxResults = 180;
                                Cron.create({
                                  name: "GMail Module Cron",
                                  type: "gmail",
                                  module: "mail",
                                  user: LoggedInUserID,
                                  data: gdata,
                                  status: "pending"
                                }, function fileCreated(err, cronRecerd) {
                                  if (cronRecerd) {
                                    EmailAccount.findOne({
                                      id: gcRecord.id,
                                      select: ['_id', 'email', 'user', 'name', 'access_token', 'refresh_token', 'provider']
                                    }).populate('user', {select: ['_id', 'first_name', 'last_name', 'photo_url', 'email']}).exec(function foundCB(err, gAccountRecord) {
                                      calender.events.list({calendarId: gcRecord.email, auth: oauth2Client, timeMin: moment().subtract(6, 'months').format()}, function (err, data) {
                                        CalendarService.eventExtractFromGoogle(data, gAccountRecord, function (returnFromExtract) {
                                          // res.send(200);
                                          if( returnFromExtract ){
                                            redirectUrlTo(LoggedInUserID, function (url) {
                                              res.redirect(url);
                                            });
                                          }else{
                                            redirectUrlTo(LoggedInUserID, function (url) {
                                              res.redirect(url);
                                            });
                                          }
                                        });
                                      });
                                    });
                                  }else{
                                    // res.send(200);
                                    redirectUrlTo(LoggedInUserID, function (url) {
                                      res.redirect(url);
                                    });
                                  }
                                });
                              }
                            });
                          } else {
                            console.log('No Message Found');
                            redirectUrlTo(LoggedInUserID, function (url) {
                              res.redirect(url);
                            });
                          }
                        });
                      }
                    });

                  });
                }
              });
            }
          });
        }
      });
    } else {
      redirectUrlTo(LoggedInUserID, function (url) {
        res.redirect(url);
      });
    }
  },

  /**
   * Description
   * @return
   * @method loginGmail
   * @param {} req
   * @param {} res
   * @return
   */
  loginGmail: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    EmailAccount.findOne({
      id: req.param('id'),
      user: LoggedInUserID,
      provider: 'gmail'
    }, function froundCB(err, record) {


      if (record && record.access_token && record.refresh_token) {


        //Already refresh token available in DB
        req.oauth2Client.setCredentials({
          access_token: '',
          refresh_token: ''
        });

        req.oauth2Client.setCredentials({
          access_token: record.access_token,
          refresh_token: record.refresh_token
        });


        req.oauth2Client.refreshAccessToken(function (err, tokens) {

          if (err) console.log(err);

          req.oauth2Client.setCredentials({
            access_token: tokens.access_token,
            refresh_token: tokens.refresh_token
          });


          EmailAccount.update({id: req.param('id'), user: LoggedInUserID, provider: 'gmail'}, {
            access_token: tokens.access_token,
            token_type: tokens.token_type,
            id_token: tokens.id_token,
            refresh_token: tokens.refresh_token
          }, function (err) {
            if (err) console.log(err);


            req.plus.people.get({userId: 'me', auth: req.oauth2Client}, function (err, profile) {
              if (err) {
                res.json({
                  'err': err
                });
              } else {
                req.gmail.users.getProfile({userId: 'me', auth: req.oauth2Client}, function (err, data) {
                  if (err) {
                    res.json({
                      'err': err
                    });
                  } else {

                    req.session.Gmail = record;

                    res.view('admin/gmail/index', {
                      'gmailId': req.param('id'),
                      'layout': 'admin/layout-gmail',
                      'allParams': req.allParams(),
                      'profile': profile,
                      'gmail_profile': data,
                      'folder': 'inbox'
                    });

                  }
                });
              }
            });

          });

        });


      } else {
        res.view("admin/gmail/permission", {
          'url': req.googleAuthUrl,
          'layout': 'admin/layout'
        });
      }
    });
  },

  /**
   * Description
   * @return
   * @method getMailThreads
   * @param {} req
   * @param {} res
   * @return
   */
  getMailThreads: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var labelID = req.param('folder') || 'INBOX';
    var maxResults = req.param('maxResults') || 15;
    var pageToken = req.param('nextPageToken') || '';
    var withOutCache = req.param('withOutCache') || 'yes';

    GmailService.getMailThreads(req.param('id'), LoggedInUserID, labelID, maxResults, pageToken, '', withOutCache, req, res, function (data) {
      res.send(data);
    });
  },


  /**
   * Description
   * @return
   * @method getMailDetails
   * @param {} req
   * @param {} res
   * @return
   */
  getMailDetails: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var mailID = req.param('mailID');

    GmailService.getMailDetails(req.param('id'), LoggedInUserID, mailID, req, res, function (data) {
      // var base64 = require('js-base64').Base64;
      // // js-base64 is working fine for me.
      //
      // var bodyData = message.payload.body.data;
      // // Simplified code: you'd need to check for multipart.
      //
      // base64.decode(bodyData.replace(/-/g, '+').replace(/_/g, '/'));
      res.json(data);
    });
  },

  /**
   * Description
   * @return
   * @method getMailThreadDetails
   * @param {} req
   * @param {} res
   * @return
   */
  getMailThreadDetails: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var threadID = req.param('threadID');

    GmailService.getMailThreadDetails(req.param('id'), LoggedInUserID, threadID, req, res, function (data) {
      GmailService.formatGmailData(data, function (formatedData) {
        res.send(formatedData);
      });
    });
  },

  /**
   * Description
   * @return
   * @method getMailAttachment
   * @param {} req
   * @param {} res
   * @return
   */
  getMailAttachment: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var threadID = req.param('threadID');

    GmailService.getMailAttachment(req.param('id'), LoggedInUserID, threadID, req.param('attachmentID'), req, res, function (data) {
      res.send(data);
    });
  },

  /**
   * Description
   * @return
   * @method modifyThread
   * @param {} req
   * @param {} res
   * @return
   */
  modifyThread: function (req, res) {
    var threadIDS = [];
    var LoggedInUserID = req.session.User.id;


    if (typeof req.param('threadID') === 'string') {
      threadIDS.push(req.param('threadID'));
    } else {
      threadIDS = req.param('threadID');
    }

    var addLabelIds;
    var removeLabelIds;
    //var addLabelIds = req.param('addLabelIds').split(',') || [];
    if (req.param('addLabelIds')) {
      if (req.param('addLabelIds') === 'no' || req.param('addLabelIds') === 'NO') {
        addLabelIds = [];
      } else {
        addLabelIds = req.param('addLabelIds').split(',');
      }
    } else {
      addLabelIds = [];
    }


    if (req.param('removeLabelIds')) {
      removeLabelIds = req.param('removeLabelIds').split(',');
    } else {
      removeLabelIds = [];
    }

    var requestBody = {
      "addLabelIds": addLabelIds,
      "removeLabelIds": removeLabelIds
    };


    GmailService.modifyThread(req.param('id'), LoggedInUserID, threadIDS, requestBody, req, res, function (data) {
      res.json(data);
    });
  },

  /**
   * Description
   * @return
   * @method deleteParmanentThreadGMail
   * @param {} req
   * @param {} res
   * @return
   */
  deleteParmanentThreadGMail: function (req, res) {
    var threads = req.param('threads');
    var LoggedInUserID = req.session.User.id;
    var requestBody = {};


    AsyncCall.eachSeries(threads, function (thread, callback) {
      var threadIDS = []; threadIDS.push(thread.mail_id);
      GmailService.deleteThreadParmanent(thread.id, LoggedInUserID, threadIDS, requestBody, req, res, function (deleteResp) {
        callback();
      });
    }, function (err) {
      if (err) {
        console.log(err);
      } else {
        res.send(200);
      }
    });

  },

  /**
   * Description
   * @method getThreadCountGMail
   * @param {} req
   * @param {} res
   * @return
   */
  getThreadCountGMail: function (req, res) {
    var labelIDs = [];
    var LoggedInUserID = req.session.User.id;
    if (typeof req.param('mailbox') === 'string' && typeof req.param('mailbox') !== 'object') {
      labelIDs.push(req.param('mailbox'));
    } else {
      labelIDs = req.param('mailbox');
    }
    var requestBody = {};
    GmailService.getGmailThreadCount(req.param('id'), LoggedInUserID, req.param('mailbox'), requestBody, req, res, function (data) {
      res.send(data);
    });
  },


  /**
   * Description
   * @method getAuthUrl
   * @param {} req
   * @param {} res
   * @return
   */
  getAuthUrl: function (req, res) {
    res.send(req.googleAuthUrl);
  },

};

/**
 * Description
 * @method redirectUrlTo
 * @param {} userId
 * @param {} callback
 * @return
 */
function redirectUrlTo( userId, callback ){
  Url.findOne({user: userId}).exec(function (err, respUrl) {
    if(err)console.log(err);
    if( respUrl && respUrl.module ){
      let callbackUrl;
      if( respUrl.module === 'calendar'){
        callbackUrl = '/calendar';
      }else {
        callbackUrl = '/mail/0/inbox';
      }
      callback(callbackUrl);
    }else{
      callback('/mail/0/inbox');
    }
  });
}
