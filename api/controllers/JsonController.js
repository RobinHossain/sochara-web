

module.exports =  {

  /**
   * Description
   * @method users
   * @param {} req
   * @param {} res
   * @return
   */
  users: function (req, res) {
    User.find({
      select: ['_id','first_name', 'last_name', 'mode', 'photo_url', 'email']
    }).sort({
      first_name: 'asc'
    }).limit(100).exec(function (err, allcontacts) {
      res.send( allcontacts );
    });
  },

  /**
   * Description
   * @method userById
   * @param {} req
   * @param {} res
   * @return
   */
  userById: function (req, res) {
    User.findOne({
      id: req.param('id')
    }).exec(function (err, details) {
      res.send( details );
    });
  },

  /**
   * Description
   * @method contacts
   * @param {} req
   * @param {} res
   * @return
   */
  contacts: function (req, res) {
    var user_id = req.session.User.id;

    Contacts.find({
      creator: user_id, select: ['_id','first_name', 'last_name', 'mode', 'status', 'photo_url', 'email']
    }).sort({
      last_name: 'desc'
    }).exec(function (err, allcontacts) {
      if (err) {
        return res.serverError(err);
      }

      Friend.find({
        friend1: user_id, select: ['_id','first_name', 'last_name', 'mode', 'status', 'photo_url', 'email']
      }).populate('friend2', {select: ['_id','first_name', 'last_name', 'mode', 'status', 'photo_url', 'email']}).exec(function findCB(err, obj) {
        if (err) throw err;

        var result = [];
        obj.forEach(function (item) {
          if (item.friend1 == user_id) {
            result.push(item.friend2);
          } else {
            result.push(item.friend1);
          }
        });

        Friend.find({
          friend2: user_id, select: ['_id','first_name', 'last_name', 'mode', 'status', 'photo_url', 'email']
        }).populate('friend1', {select: ['_id','first_name', 'last_name', 'mode', 'status', 'photo_url', 'email']}).exec(function findCB(err, obj) {
          if (err) throw err;

          obj.forEach(function (item) {
            if (item.friend1 == user_id) {
              result.push(item.friend2);
            } else {
              result.push(item.friend1);
            }
          });
          var allcontactsFinal = allcontacts.concat(result);
          var filteredAllContacts = [];
          if( allcontactsFinal.length ){
            allcontactsFinal.forEach(function (filteredConcatcitem) {
              if( filteredConcatcitem ){
                if ( filteredConcatcitem.first_name ) {
                  filteredAllContacts.push(filteredConcatcitem);
                }
              }

            });
          }

          res.send(filteredAllContacts);

        });
      });

    });
  },

  /**
   * Description
   * @method user
   * @param {} req
   * @param {} res
   * @return
   */
  user: function (req, res) {
    let userId = req.session.User.id;
    User.findOne( userId ).exec(function countCB(error, response) {
      if( response ){
        res.send(response);
      }
    });
  },

  /**
   * Description
   * @method test
   * @param {} req
   * @param {} res
   * @return
   */
  test: function (req, res) {
    console.log('result');
    res.send('result');
  }

};
