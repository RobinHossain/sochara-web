/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  /**
   * Description
   * @method info
   * @param {} req
   * @param {} res
   * @return 
   */
  info: function (req, res) {
    var id = req.session.User.id;
    User.findOne(id).exec(function (err, fd) {
      if(err){
        return res.serverError(err);
      }
      res.send(fd);
    });
  },

  /**
   * Description
   * @method getUserById
   * @param {} req
   * @param {} res
   * @return 
   */
  getUserById: function (req, res) {
    User.findOne( req.param('id') ).exec(function (err, UserResp) {
      if( UserResp ){
        res.send( UserResp );
      }else{
        res.send('');
      }

    });
  },

  /**
   * Description
   * @method avatar
   * @param {} req
   * @param {} res
   * @return 
   */
  avatar: function (req, res) {

    req.validate({
      id: 'string'
    });

    User.findOne(req.param('id')).exec(function (err, user) {
      if (err) return res.negotiate(err);
      if (!user) return res.notFound();

      // User has no avatar image uploaded.
      // (should have never have hit this endpoint and used the default image)
      if (!user.avatarFd) {
        return res.notFound();
      }

      var SkipperDisk = require('skipper-disk');
      var fileAdapter = SkipperDisk(/* optional opts */);

      // Stream the file down
      fileAdapter.read(user.avatarFd)
        .on('error', function (err) {
          return res.serverError(err);
        })
        .pipe(res);
    });
  },
  //updating user profile
  /**
   * Description
   * @method updateUser
   * @param {} req
   * @param {} res
   * @return 
   */
  updateUser: function (req, res) {
    var loggedInUserId = req.session.User.id;
    var data = req.body;
    User.update({id: loggedInUserId}, data).exec(function (err, updatedData) {
      if (err) {
        //sending back error. need some steps in front end to analyse error property
        return res.json(401, {err: err});
      }
      if (updatedData) {
        res.json({msg: 'success'});
      }
    });
  },
  //updating user profile
  /**
   * Description
   * @method updateUserAccount
   * @param {} req
   * @param {} res
   * @return 
   */
  updateUserAccount: function (req, res) {
    var loggedInUserId = req.session.User.id;
    var userSettingData = req.body;
    if (userSettingData.password) {
      User.hashPassword(userSettingData.password, function (data) {
        if (data.err) {
          return res.json(401, {err: 'Something went wrong!'});
        }
        userSettingData.encryptedPassword = data.hash;
        User.update({id: loggedInUserId}, userSettingData).exec(function (err, updatedData) {
          if (err) {
            return res.json(401, {err: "Invalid data!"});
          }
          if (updatedData) {
            res.send({msg: 'success'});
          }
        });
      });
    }
  },
  /**
   * Description
   * @method getProfileData
   * @param {} req
   * @param {} res
   * @return 
   */
  getProfileData: function (req, res) {
    var loggedInUserId = req.session.User.id;
    User.findOne({id: loggedInUserId}).exec(function (err, data) {
      if (err) {
        //sending back error. need some steps in front end to analyse error property
        return res.json(401, {err: err});
      }
      if (data) {
        res.send(data);
      }
    });
  },
  /**
   * Description
   * @method updateAvatar
   * @param {} req
   * @param {} res
   * @return 
   */
  updateAvatar: function (req, res) {
    var url = req.param('url');
    User.update({id: req.session.User.id}, {photo_url: url}).exec(function (err, updatedImage) {
      if (err) {
        return res.json(401, {err: "Invalid Image!"});
      }
      if (updatedImage) {
        req.session.User.photo_url = url;
        res.send({msg: 'success'});
      }
    });
  },
  /**
   * Description
   * @method updateBanner
   * @param {} req
   * @param {} res
   * @return 
   */
  updateBanner: function (req, res) {
    var loggedInUserId = req.session.User.id;
    var url = req.param('url');
    if (url) {
      var base64ToImage = require('base64-to-image');
      var base64Str = url;
      var path = 'assets/uploads/files/' + loggedInUserId + '/';
      var optionalObj = {'fileName': 'profileBanner', 'type': 'png'};
      base64ToImage(base64Str, path, optionalObj);
      User.update({id: req.session.User.id}, {bannerImage: '/uploads/files/' + loggedInUserId + '/profileBanner.png'}).exec(function (err, updatedImage) {
        if (err) {
          return res.json(401, {err: "Invalid Image!"});
        }
        if (updatedImage) {
          req.session.User.bannerImage = '/uploads/files/' + loggedInUserId + '/profileBanner.png';
          res.send({msg: 'success'});
        }
      });
    }
    else {
      return res.serverError(err);

    }
  },
  /**
   * Description
   * @method updatePrivacyAttribute
   * @param {} req
   * @param {} res
   * @return 
   */
  updatePrivacyAttribute: function (req, res) {
    var attribute = req.param('attribute');
    var privacy = req.param('privacy');
    User.findOne({id: req.session.User.id}).exec(function (err, foundData) {
      if (err) {
        return res.serverError(err);
      }
      var privacyAttributeNow = foundData.privacyAttribute;
      var friendsIndex = privacyAttributeNow.friends.indexOf(attribute);
      var onlyMeIndex = privacyAttributeNow.onlyMe.indexOf(attribute);
      if (friendsIndex !== -1) {
        privacyAttributeNow.friends.splice(friendsIndex, 1);
      }
      if (onlyMeIndex !== -1) {
        privacyAttributeNow.onlyMe.splice(onlyMeIndex, 1);
      }
      if (privacy === 'friends') {
        privacyAttributeNow.friends.push(attribute);
      }
      if (privacy === 'onlyMe') {
        privacyAttributeNow.onlyMe.push(attribute);
      }
      User.update({id: req.session.User.id}, {privacyAttribute: privacyAttributeNow}).exec(function (err, updatedImage) {
        if (err) {
          return res.serverError(err);
        }
        if (updatedImage) {
          res.send('success');
        }
      });
    });

  },
  /**
   * Description
   * @method allUserList
   * @param {} req
   * @param {} res
   * @return 
   */
  allUserList: function (req, res) {
    User.find().exec(function (err, data) {
      if (err) {
        return res.serverError(err);
      }
      res.send(data);
    });
  },

  /**
   * Description
   * @method changeMode
   * @param {} req
   * @param {} res
   * @return 
   */
  changeMode:function (req, res) {
    let userId = req.session.User.id;
    User.update({id:userId},{mode:req.param('status')}).exec(function (err, uD) {
      if (err) {
        console.log(err);
      }
      sails.sockets.blast('user_mode', uD[0]);
      res.send(200);
    });
  }


};

