
var Jimp = require("jimp");
var path = require('path');


module.exports = {

  /**
   * Save poll information
   * @return
   * @method saveProfilePhoto
   * @param req
   * @param res
   * @return 
   */
  saveProfilePhoto: function (req, res) {
    var LoggedInUserid = req.session.User.id;
    var saveToServerName;
    var randNum = new Date().getTime();
    req.file('file').upload({
      dirname: path.resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserid + '/profile',

      /**
       * Description
       * @method saveAs
       * @param {} __newFileStream
       * @param {} cb
       * @return 
       */
      saveAs: function (__newFileStream, cb) {
        var fname = __newFileStream.filename;
        var mName = fname.substr(0, fname.lastIndexOf('.'));
        var fName = mName.replace(/\s\s+/g, ' ');
        fName = fName.replace(/ /g, "-");
        var ext = path.extname(__newFileStream.filename);
        saveToServerName = fName + "_" + randNum + ext;
        cb(null, saveToServerName);
      },

      maxBytes: 10000000
    }, function whenDone(err, uploadedFiles) {

      if (err) {
        return res.negotiate(err);
      }

      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0) {
        return res.badRequest('No file was uploaded');
      } else {
        // var baseURL = Utility.getSiteBaseURL(req);


        Jimp.read(uploadedFiles[0].fd, function (err, image) {
          if (err) throw err;
          var extraLen;
          var img_w = parseInt(image.bitmap.width);
          var img_h = parseInt(image.bitmap.height);


          if (img_w > img_h) {
            extraLen = (img_w - img_h) / 2;
            image.crop(Math.floor(extraLen), 0, img_h, img_h);
          } else {
            extraLen = (img_h - img_w) / 2;
            image.crop(0, Math.floor(extraLen), img_w, img_w);
          }

          var fileNameWithExt = path.basename(uploadedFiles[0].fd);
          var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));

          image.resize(250, Jimp.AUTO)
            .quality(60)
            .write('assets/uploads/files/' + LoggedInUserid + '/profile/thumb/' + uFileName + '.jpg'); // save
        }).catch(function (err) {
          console.log(err);
        });

        var fileNameWithExt = path.basename(uploadedFiles[0].fd);
        var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));
        var genThumUrl = path.dirname(uploadedFiles[0].fd) + '/thumb/' + uFileName + '.jpg';
        var thumb_dir = '/uploads/files/' + LoggedInUserid + '/profile/thumb/' + uFileName + '.jpg';
        var file_dir = '/uploads/files/' + LoggedInUserid + '/profile/' + path.basename(uploadedFiles[0].fd);

        var files = {};
        files.name = uploadedFiles[0].filename;
        files.ext = path.extname(uploadedFiles[0].filename);
        files.type = 'profile';
        files.owner = LoggedInUserid;
        files.file_dir = file_dir;
        files.fileFD = uploadedFiles[0].fd;
        files.fileThumb = genThumUrl;
        files.thumb_dir = thumb_dir;
        files.size = uploadedFiles[0].size;
        UserFiles.create(files, function fileCreated(err, backFile) {
          if (backFile) {
            User.update({id: LoggedInUserid}, {photo_url: file_dir}).exec(function afterwards(err, updated) {
              res.send(file_dir);
            });
          }
        });

      }
    });
  },
  /**
   * Description
   * @method saveRelative
   * @param {} req
   * @param {} res
   * @return 
   */
  saveRelative: function (req, res) {
    var userId = req.session.User.id;
    var relativeData = req.allParams();
    relativeData.user = userId;
    Relative.create(relativeData, function relativeCreated(err, createdRelative) {
      if (err) {
        console.log(err);
      }
      if (createdRelative) {
        res.send(createdRelative);
      }
    });
  },
  /**
   * Description
   * @method getAllRelativeInfo
   * @param {} req
   * @param {} res
   * @return 
   */
  getAllRelativeInfo: function (req, res) {
    var userId = req.session.User.id;
    Relative.find({
      user: userId
    }).populate('relative', {select: ['_id', 'first_name', 'last_name', 'email', 'photo_url']}).exec(function foundCB(err, records) {
      res.send(records);
      console.log(records);
    });
  },
  /**
   * Description
   * @method getRelativeInfo
   * @param {} req
   * @param {} res
   * @return 
   */
  getRelativeInfo: function (req, res) {
    var userId = req.session.User.id;
    Relative.findOne({
      user: userId,
      id: req.param('id')
    }).populate('relative', {select: ['_id', 'first_name', 'last_name', 'email', 'photo_url']}).exec(function foundCB(err, record) {
      if( record ){
        res.send(record);
      }
    });
  },
  /**
   * Description
   * @method updateRelative
   * @param {} req
   * @param {} res
   * @return 
   */
  updateRelative: function (req, res) {
    var userId = req.session.User.id;
    var relativeData = req.allParams();
    Relative.update({id: relativeData.id }, relativeData).exec(function foundCB(err, record) {
      if( record ){
        res.send(record);
      }
    });
  },
  /**
   * Description
   * @method deleteRelative
   * @param {} req
   * @param {} res
   * @return 
   */
  deleteRelative: function (req, res) {
    var userId = req.session.User.id;
    Relative.destroy({id: req.param('id'), user: userId}, function destroyedCB(err, record) {
      if (record) {
        res.send('deleted');
      } else {
        res.send('ok');
      }
    });
  }


};
