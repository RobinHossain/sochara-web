let outlook = require('nodejs-outlook');


module.exports = {

  /**
   * Description
   * @method importContact
   * @param {} req
   * @param {} res
   * @return 
   */
  importContact: function(req, res){
    var LoggedInUserID = req.session.User.id;
    let code = req.param('code');
    let queryParams = { '$top': 200 };
    let uri = req.protocol + '://' + req.get('host') + '/outlook/importContact';
    let scopes = ['https://outlook.office.com/contacts.read'];

    req.getTokenFromCode({code: code, uri: uri, scopes: scopes}, tokenCallbackFunc);

    /**
     * Description
     * @method tokenCallbackFunc
     * @param {} getToken
     * @return 
     */
    function tokenCallbackFunc(getToken) {
      outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
      outlook.contacts.getContacts({token: getToken.access_token, odataParams: queryParams},
        function (error, result) {
          if (error) {
            console.log('getContacts returned an error: ');
            console.log(error);
          }
          else if (result) {
            ContactsService.contactExtractFromOutlook(result, LoggedInUserID, function (returnFromExtract) {
              if( returnFromExtract ){
                res.redirect('/contacts?import=true&outlook=true');
              }
            });
          }
        });
    }
  },

  /**
   * Description
   * @method getTempContacts
   * @param {} req
   * @param {} res
   * @return 
   */
  getTempContacts: function(req, res){
    let LoggedInUserID = req.session.User.id;
    Temp.find({user: LoggedInUserID}).exec(function (err, contacts) {
      if(err)console.log(err);
      res.send(contacts);
    });
  }
};
