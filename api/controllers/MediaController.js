/**
 * Admin/LabelController
 *
 * @description :: Server-side logic for managing admin/labels
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


/**
 * Dependencies
 *
 */

var Jimp = require("jimp");
var fs = require('fs');
var path = require('path');
var Thumbler = require('thumbler');
var rimraf = require('rimraf');
var AsyncCall = require('async');
var archiver = require('archiver');
var ExifImage = require('exif');
var NodeGeocoder = require('node-geocoder');


module.exports = {

  /**
   * @return
   * @method index
   * @param {} req
   * @param {} res
   * @return 
   */
  index: function (req, res) {
    var title = "Media";
    res.view({
      data: title, 'layout': 'admin/layout-media'
    })
  },

  /**
   * @return
   * @method getMediaPhoto
   * @param {} req
   * @param {} res
   * @return 
   */
  getMediaPhoto: function (req, res) {

    UserFiles.findOne(req.param('id')).exec(function (err, fileRes) {
      if (err) return res.negotiate(err);
      if (!fileRes) return res.notFound();

      // User has no avatar image uploaded.
      // (should have never have hit this endpoint and used the default image)
      if (!fileRes.fileFD) {
        return res.notFound();
      }

      var SkipperDisk = require('skipper-disk');
      var fileAdapter = SkipperDisk(/* optional opts */);

      // Stream the file down
      fileAdapter.read(fileRes.fileFD)
        .on('error', function (err) {
          return res.serverError(err);
        })
        .pipe(res);
    });
  },

  /**
   * Description
   * @method addToStar
   * @param {} req
   * @param {} res
   * @return 
   */
  addToStar: function (req, res) {
    let media = req.param('media');
    UserFiles.update({id: media, type: 'media'}, {starred: true}).exec(function afterwards(err, updated) {
      res.send(200);
    });
  },


  /**
   * Description
   * @method renameMedia
   * @param {} req
   * @param {} res
   * @return 
   */
  renameMedia: function (req, res) {
    let media = req.param('media');
    UserFiles.update({id: req.param('id')}, {id: req.param('id'), name: req.param('name')}).exec(function afterwards(err, updated) {
      res.send(updated);
    });
  },

  /**
   * Description
   * @method removeFromStar
   * @param {} req
   * @param {} res
   * @return 
   */
  removeFromStar: function (req, res) {
    let media = req.param('media');
    UserFiles.update({id: media, type: 'media'}, {starred: false}).exec(function afterwards(err, updated) {
      res.send(200);
    });
  },

  /**
   * Description
   * @method pasteSelectedItems
   * @param {} req
   * @param {} res
   * @return 
   */
  pasteSelectedItems: function (req, res) {
    AsyncCall.eachSeries(req.param('pasteItems'), function (item, callback) {
      if (item.type === 'album') {
        MediaAlbum.update({id: item.id}, {root: req.param('album') || 0}).exec(function afterwards(err, updated) {
          if (err) {
            console.log(err);
          }
          callback();
        });
      } else if (item.type === 'media') {
        // test
        UserFiles.findOne({id: item.id}).exec(function (err, record) {
          if (record) {
            var fileData = {};
            fileData.album = record.album || [];

            if (record.album) {
              if (req.param('pasteType') === 'cut' && req.param('pasteFrom')) {
                var getFromIndex = fileData.album.indexOf(req.param('pasteFrom'));
                if (getFromIndex > -1) {
                  fileData.album.splice(getFromIndex, 1);
                }
              }
              if (req.param('album')) {
                if (fileData.album.indexOf(req.param('album') === -1)) {
                  fileData.album.push(req.param('album'));
                }
              } else {
                fileData.album.push('0');
              }
            } else if (req.param('album')) {
              fileData.album = [req.param('album')];
            } else {
              fileData.album = ['0'];
            }

            UserFiles.update(item.id, fileData, function updatedCB(err, updated) {
              if (err) {
                console.log(err);
              }
              callback();
            });
          } else {
            callback();
          }

        });
        // test
      } else {
        callback();
      }

    }, function (err) {
      if (err) {
        console.log(err);
      } else {
        res.send(200);
      }
    });
  },

  /**
   * ********************** Get Video file **********************
   * @return
   * @method getMediaVideo
   * @param {} req
   * @param {} res
   * @return 
   */
  getMediaVideo: function (req, res) {

    UserFiles.findOne(req.param('id')).exec(function (err, fileRes) {
      if (err) return res.negotiate(err);
      if (!fileRes) return res.notFound();

      if (!fileRes.fileFD) {
        return res.notFound();
      }

      var SkipperDisk = require('skipper-disk');
      var fileAdapter = SkipperDisk(/* optional opts */);

      // Stream the file down
      fileAdapter.read(fileRes.fileFD)
        .on('error', function (err) {
          return res.serverError(err);
        })
        .pipe(res);
    });
  },

  /**
   * ************************** Get Photo Thumbnail ***************************
   * @return
   * @method getMediaPhotoThumb
   * @param {} req
   * @param {} res
   * @return 
   */
  getMediaPhotoThumb: function (req, res) {

    UserFiles.findOne(req.param('id')).exec(function (err, fileRes) {
      if (err) return res.negotiate(err);
      if (!fileRes) return res.notFound();

      if (!fileRes.fileThumb) {
        return res.notFound();
      }

      var SkipperDisk = require('skipper-disk');
      var fileAdapter = SkipperDisk(/* optional opts */);

      // Stream the file down
      fileAdapter.read(fileRes.fileThumb)
        .on('error', function (err) {
          return res.serverError(err);
        })
        .pipe(res);
    });
  },

  /**
   * ************************** Get Video Thumbnail ***************************
   * @return
   * @method getMediaVideoThumb
   * @param {} req
   * @param {} res
   * @return 
   */
  getMediaVideoThumb: function (req, res) {

    UserFiles.findOne(req.param('id')).exec(function (err, fileRes) {
      if (err) return res.negotiate(err);
      if (!fileRes) return res.notFound();

      if (!fileRes.fileThumb) {
        return res.notFound();
      }

      var SkipperDisk = require('skipper-disk');
      var fileAdapter = SkipperDisk(/* optional opts */);

      // Stream the file down
      fileAdapter.read(fileRes.fileThumb)
        .on('error', function (err) {
          return res.serverError(err);
        })
        .pipe(res);
    });
  },

  /**
   * ************************** Get Poster Photo for Video ***************************
   * @return
   * @method getMediaPosterThumb
   * @param {} req
   * @param {} res
   * @return 
   */
  getMediaPosterThumb: function (req, res) {

    UserFiles.findOne(req.param('id')).exec(function (err, fileRes) {
      if (err) return res.negotiate(err);
      if (!fileRes) return res.notFound();

      if (!fileRes.filePoster) {
        return res.notFound();
      }

      var SkipperDisk = require('skipper-disk');
      var fileAdapter = SkipperDisk(/* optional opts */);

      // Stream the file down
      fileAdapter.read(fileRes.filePoster)
        .on('error', function (err) {
          return res.serverError(err);
        })
        .pipe(res);
    });
  },

  /**
   * ************************** Get Shared Meida ***************************
   * @return
   * @method people
   * @param {} req
   * @param {} res
   * @return 
   */
  people: function (req, res) {
    var title = "Media Shared With People";
    res.view({
      data: title, 'layout': 'admin/layout-media'
    })
  },

  /**
   * ************************** Get Photo Album Archive ***************************
   * @return
   * @method getAlbumPhotoArchive
   * @param {} req
   * @param {} res
   * @return 
   */
  getAlbumPhotoArchive: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var archiver = require('archiver');

    MediaAlbum.findOne({id: req.param('id'), select: ['name']}).exec(function (err, record) {
      if (record) {
        var albumName = record.name;
        var fName = albumName.replace(/\s\s+/g, ' ');
        albumName = fName.replace(/ /g, "-");
        var archive = archiver('zip', {
          // store: true // Sets the compression method to STORE.
        });

        res.writeHead(200, {
          'Content-Type': 'application/zip',
          'Content-disposition': 'attachment; filename=' + albumName + '.zip'
        });


        // good practice to catch this error explicitly
        archive.on('error', function (err) {
          console.log(err);
        });

        // pipe archive data to the file
        archive.pipe(res);

        UserFiles.find({
          type: 'media',
          user_id: LoggedInUserID,
          album: req.param('id'),
          select: ['_id', 'name', 'file_dir']
        }).exec(function foundCB(err, records) {

          if (records) {
            AsyncCall.forEach(records, function (item, callback) {


              // For Main File
              var fileName = path.basename(item.file_dir);
              var fileNameOri = item.name;
              var fileDir = path.resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserID + '/media/' + fileName;

              // For Thumb

              // var file1 = __dirname + '/file1.txt';
              archive.append(fs.createReadStream(fileDir), {name: fileNameOri});

            }, function (err) {
              if (err) {
                console.log(err);
              } else {

              }
            });
            // Task Here
            archive.finalize();
          }
        });
      }
    });


  },

  /**
   * ************************** New Album Creation ***************************
   * @return
   * @method createNewAlbum
   * @param {} req
   * @param {} res
   * @return 
   */
  createNewAlbum: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var album = req.param('name') || '';
    var root = req.param('root') || 0;
    if (album) {
      var data = {};
      data.name = album;
      data.root = root;
      data.owner = LoggedInUserID;
      MediaAlbum.create(data).exec(function (err, record) {
        if (err) {
          return res.serverError(err);
        }
        res.send(record);
      });
    }
  },

  // getAllAlbumList: function (req, res) {
  //
  //   var LoggedInUserID = req.session.User.id;
  //   var where = {};
  //   where.owner = LoggedInUserID;
  //   if (req.param('album')) {
  //     where.root = req.param('album');
  //   } else {
  //     where.root = '0';
  //   }
  //   if (req.param('starred') && req.param('starred') === 'yes') {
  //     where.starred = true;
  //   }
  //   where.select = ['_id', 'name', 'root', 'owner', 'updatedAt', 'createdAt'];
  //   var albumQuery = MediaAlbum.find(where);
  //
  //   albumQuery.populate('owner', {select: ['_id', 'first_name', 'last_name']});
  //   albumQuery.exec(function foundCB(err, records) {
  //     if (err) return next(err);
  //     // res.json(records);
  //
  //     var totalRecords = [];
  //     if (records) {
  //       AsyncCall.forEach(records, function (item, callback) {
  //         UserFiles.count({album: item.id}).exec(function countCB(error, found) {
  //
  //           UserFiles.count({album: item.id, file_type: 'video'}).exec(function countCB(error, foundv) {
  //             item.videos = foundv;
  //             item.photos = totalItem - foundv;
  //           });
  //
  //           if (found) {
  //             item.photos = found;
  //           }
  //
  //           MediaAlbum.count({root: item.id}).exec(function countCB(error, foundAlbum) {
  //             if (foundAlbum) {
  //               item.albums = foundAlbum;
  //             }
  //
  //             UserFiles.findOne({
  //               album: item.id,
  //               select: ['updatedAt']
  //             }).sort({updatedAt: -1}).exec(function (err, record) {
  //
  //               if (record) {
  //                 item.updatedAt = moment(record.updatedAt).format('MM/DD/YYYY');
  //               }
  //
  //               SharedFiles.findOne({album: item.id, owner: LoggedInUserID}).exec(function countCB(error, found) {
  //
  //                 if (found && found.users.length) {
  //                   item.shared = found.users.length;
  //                 }
  //                 item.type = 'album';
  //                 totalRecords.push(item);
  //                 callback();
  //
  //               });
  //
  //             });
  //
  //           });
  //
  //         });
  //
  //       }, function (err) {
  //         if (err) {
  //           console.log(err);
  //         } else {
  //           res.send(totalRecords);
  //         }
  //       });
  //
  //     }
  //
  //   });
  // },

  /**
   * ************************** Remove from album ***************************
   * @return
   * @method removeFromAlbum
   * @param {} req
   * @param {} res
   * @return 
   */
  removeFromAlbum: function (req, res) {
    UserFiles.findOne({id: req.param('photo')}).exec(function (err, record) {
      if (record) {
        let updateData = record;

        var getAlbumIndex = updateData.album.indexOf(req.param('album'));
        if (getAlbumIndex > -1) {
          updateData.album.splice(getAlbumIndex, 1);
        }

        UserFiles.update(req.param('photo'), updateData, function updatedCB(err) {
          if (err) {
          }
          MediaAlbum.update(req.param('album'), {cover: null}, function updatedCB(err) {
            if (err) return next(err);
            return res.json('removed');
          });
        });
      } else {
        return res.json('can not remove');
      }

    });
  },

  /**
   * ************************** Set album cover photo ***************************
   * @return
   * @method setAlbumCover
   * @param {} req
   * @param {} res
   * @return 
   */
  setAlbumCover: function (req, res) {

    MediaAlbum.findOne({id: req.param('album')}).exec(function (err, record) {
      if (record) {
        let updateData = record;
        updateData.cover = req.param('photo');

        MediaAlbum.update(req.param('album'), updateData, function updatedCB(err) {
          if (err) return next(err);
          res.json('added');
        });
      } else {
        return res.json('can not add');
      }
    });
  },

  /**
   * ************************** Move photo to an album ***************************
   * @return
   * @method movePhotoToAlbum
   * @param {} req
   * @param {} res
   * @return 
   */
  movePhotoToAlbum: function (req, res) {
    UserFiles.findOne({id: req.param('photo')}).exec(function (err, record) {
      if (record) {
        let updateData = record;

        var getAlbumIndex = updateData.album.indexOf(req.param('current'));
        if (getAlbumIndex > -1) {
          updateData.album.splice(getAlbumIndex, 1);
        }

        updateData.album.push(req.param('album'));

        UserFiles.update(req.param('photo'), updateData, function updatedCB(err) {
          if (err) {
          }
          return res.json('moved');
        });
      } else {
        return res.json('can not move');
      }

    });
  },

  /**
   * ************************** Copy photo one album to another album ***************************
   * @return
   * @method copyPhotoToAlbum
   * @param {} req
   * @param {} res
   * @return 
   */
  copyPhotoToAlbum: function (req, res) {
    UserFiles.findOne({id: req.param('photo')}).exec(function (err, record) {
      if (record) {
        let updateData = record;
        if (updateData.album) {
          updateData.album.push(req.param('album'));
        } else {
          updateData.album = [req.param('album')];
        }
        // updateData.album = req.param('album');
        UserFiles.update(req.param('photo'), updateData, function updatedCB(err) {
          if (err) {
          }
          return res.json('moved');
        });
      } else {
        return res.json('can not move');
      }

    });
  },

  /**
   * ************************** Get all photos ***************************
   * @return
   * @method allphotos
   * @param {} req
   * @param {} res
   * @return 
   */
  allphotos: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var title = "ALl Videos";
    res.view({data: title, 'layout': 'admin/layout-media'})
  },

  /**
   * ************************** Get all Videos ***************************
   * @return
   * @method allvideos
   * @param {} req
   * @param {} res
   * @return 
   */
  allvideos: function (req, res) {
    var title = "ALl Videos";
    res.view({data: title, 'layout': 'admin/layout-media'});
  },

  /**
   * ************************** Get shared user name ***************************
   * @return
   * @method getShareWithUser
   * @param {} req
   * @param {} res
   * @return 
   */
  getShareWithUser: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    if (req.param('type') == 'file') {
      SharedMedia.findOne({media_id: req.param('id')}).exec(function (err, resp) {
        if (err) {
          return res.serverError(err);
        }
        if (resp) {
          var prevRes = resp;
          if (prevRes.users) {

            var getUserIndex = prevRes.users.indexOf(req.param('uid'));
            if (getUserIndex == -1) {
              prevRes.users.push(req.param('uid'));
            }

          } else {
            prevRes.users = [req.param('uid')];
          }

          SharedMedia.update({media_id: req.param('id')}, {users: prevRes.users}, function updatedCB(err) {
            if (err) {
            }
            res.send('updated');
          });
        } else {
          var medias = {};
          medias.user_id = LoggedInUserID;
          medias.user = LoggedInUserID;
          medias.media_id = req.param('id');
          medias.media = req.param('id');
          medias.users = [req.param('uid')];
          SharedMedia.create(medias, function fileCreated(err) {
            if (err) {
              console.log(err)
            }
            res.send('created');
          });
        }
      });
    } else if (req.param('type') == 'album') {
      SharedMedia.findOne({album_id: req.param('id')}).exec(function (err, resp) {
        if (err) {
          return res.serverError(err);
        }
        if (resp) {
          var prevRes = resp;
          if (prevRes.users) {

            var getUserIndex = prevRes.users.indexOf(req.param('uid'));
            if (getUserIndex == -1) {
              prevRes.users.push(req.param('uid'));
            }

          } else {
            prevRes.users = [req.param('uid')];
          }

          SharedMedia.update({album_id: req.param('id')}, {users: prevRes.users}, function updatedCB(err) {
            if (err) {
            }
            res.send('updated');
          });
        } else {
          var medias = {};
          medias.user_id = LoggedInUserID;
          medias.user = LoggedInUserID;
          medias.album_id = req.param('id');
          medias.album = req.param('id');
          medias.users = [req.param('uid')];
          SharedMedia.create(medias, function fileCreated(err) {
            if (err) {
              console.log(err)
            }
            res.send('created');
          })
        }
      });
    }

  },

  /**
   * ************************** Remove an user from share ***************************
   * @return
   * @method removeUserFromShare
   * @param {} req
   * @param {} res
   * @return 
   */
  removeUserFromShare: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    if (req.param('type') == 'file') {
      SharedMedia.findOne({media_id: req.param('id')}).exec(function (err, resp) {
        if (err) {
          return res.serverError(err);
        }
        if (resp) {
          var prevRes = resp;
          if (prevRes.users) {

            var getUserIndex = prevRes.users.indexOf(req.param('uid'));
            if (getUserIndex > -1) {
              prevRes.users.splice(getUserIndex, 1);
            }

          }

          SharedMedia.update({media_id: req.param('id')}, {users: prevRes.users}, function updatedCB(err) {
            if (err) {
            }
            return res.json('updated');
          })
        }
      });
    } else if (req.param('type') == 'album') {
      SharedMedia.findOne({album_id: req.param('id')}).exec(function (err, resp) {
        if (err) {
          return res.serverError(err);
        }
        if (resp) {
          var prevRes = resp;

          if (prevRes.users) {

            var getUserIndex = prevRes.users.indexOf(req.param('uid'));
            if (getUserIndex > -1) {
              prevRes.users.splice(getUserIndex, 1);
            }

          }

          SharedMedia.update({album_id: req.param('id')}, {users: prevRes.users}, function updatedCB(err) {
            if (err) {
            }
            return res.json('updated');
          })
        }
      });
    }

  },

  /**
   * Description
   * @return
   * @method getPreviousShareWithUserPhoto
   * @param {} req
   * @param {} res
   * @return 
   */
  getPreviousShareWithUserPhoto: function (req, res) {
    var user_id = req.session.User.id;
    SharedMedia.findOne({user_id: user_id, media_id: req.param('id')}).exec(function (err, resp) {
      if (err) {
        return res.serverError(err);
      }
      var allUsers = [];
      if (resp) {

        AsyncCall.each(resp.users, function (user, cb) {
          User.findOne(user)
            .then(function (responce) {
              var users = {};
              users.id = responce.id;
              users.name = responce.first_name + ' ' + responce.last_name;
              allUsers.push(users);
              cb();
            })
            .fail(function (error) {
              cb(error);
            })
        }, function (error) {
          if (error) {
          }
          ;
          return res.send(allUsers);
        });
      } else {
        res.send(allUsers);
      }
    });
  },

  /**
   * Description
   * @return
   * @method getPreviousShareWithUserAlbum
   * @param {} req
   * @param {} res
   * @return 
   */
  getPreviousShareWithUserAlbum: function (req, res) {
    var user_id = req.session.User.id;
    SharedMedia.findOne({user_id: user_id, album_id: req.param('id')}).exec(function (err, resp) {
      if (err) {
        return res.serverError(err);
      }
      var allUsers = [];
      if (resp) {

        AsyncCall.each(resp.users, function (user, cb) {
          User.findOne(user).exec(function (err, responce) {
            var users = {};
            users.id = responce.id;
            users.name = responce.first_name + ' ' + responce.last_name;
            allUsers.push(users);
            cb();
          });
         }, function (error) {
          if (error) {
         }
         return res.send(allUsers);
       });
      } else {
        res.send(allUsers);
      }
    });
  },


  /**
   * Description
   * @return
   * @method reloadPreviousSharedContactAlbum
   * @param {} req
   * @param {} res
   * @return 
   */
  reloadPreviousSharedContactAlbum: function (req, res) {
    var user_id = req.session.User.id;
    SharedMedia.findOne({user_id: user_id, album_id: req.param('id')}).exec(function (err, resp) {
      if (err) {
        return res.serverError(err);
      }
      var allUsers = [];
      if (resp) {

        AsyncCall.each(resp.users, function (user, cb) {
          User.findOne(user).exec(function (err, responce) {
              var users = {};
              users.id = responce.id;
              users.name = responce.first_name + ' ' + responce.last_name;
              allUsers.push(users);
              cb();
            });
        }, function (error) {
          if (error) {
            console.log(error);
          }
          return res.send(allUsers);
        });
      } else {
        res.send(allUsers);
      }
    });
  },


  /**
   * Description
   * @return
   * @method getMycontacts
   * @param {} req
   * @param {} res
   * @return 
   */
  getMycontacts: function (req, res) {
    var user_id = req.session.User.id;
    Contacts.find({
      creator: user_id
    }).sort({
      last_name: 'desc'
    }).exec(function (err, allcontacts) {
      if (err) {
        return res.serverError(err);
      }

      Friend.find({
        friend1: user_id
      }).populate('friend2', {select: ['_id', 'first_name', 'last_name']}).exec(function findCB(err, obj) {
        if (err) throw err;

        var result = [];
        var finalRes = [];
        obj.forEach(function (item) {
          if (item.friend1 == user_id) {
            result.push(item.friend2);
          } else {
            result.push(item.friend1);
          }
        });

        Friend.find({
          friend2: user_id
        }).populate('friend1', {select: ['_id', 'first_name', 'last_name']}).exec(function findCB(err, obj) {
          if (err) throw err;

          obj.forEach(function (item) {
            if (item.friend1 == user_id) {
              result.push(item.friend2);
            } else {
              result.push(item.friend1);
            }
          });


          var type;
          var existingUserForAlbum = false;
          if (req.param('type') == 'file') {
            SharedMedia.findOne({
              user_id: user_id,
              media_id: req.param('id')
            }, {select: ['_id', 'user_id']}).exec(function (err, resp) {
              if (err) {
                return res.serverError(err);
              }
              if (resp) {
                existingUserForAlbum = true;
              }

            });
          } else if (req.param('type') == 'album') {
            SharedMedia.findOne({
              user_id: user_id,
              album_id: req.param('id')
            }, {select: ['_id', 'user_id']}).exec(function (err, resp) {
              if (err) {
                return res.serverError(err);
              }
              if (resp) {
                existingUserForAlbum = true;
              }
            });
          }

          result.forEach(function (res) {
            if (res && res.length) {
              resObj = {};
              if (existingUserForAlbum == false) {
                resObj.id = res.id;
                resObj.name = res.first_name + ' ' + res.last_name;
                finalRes.push(resObj);
              }
            }
          });

          res.send(allcontacts.concat(finalRes));

        });
      });
    });
  },

  /**
   * Description
   * @return
   * @method getRecentUploadedFiles
   * @param {} req
   * @param {} res
   * @return 
   */
  getRecentUploadedFiles: function (req, res) {
    var LoggedInUserID = req.session.User.id;

    // Set Srart and End Days
    var start = new Date();
    start.setHours(0, 0, 0, 0);

    var end = new Date();
    end.setHours(23, 59, 59, 999);

    UserFiles.find({
      type: 'media',
      user_id: LoggedInUserID,
      "createdAt": {$gte: start, $lt: end},
      select: ['_id', 'name', 'file_type', 'file_dir', 'updatedAt']
    }).sort({createdAt: -1}).exec(function foundCB(err, records) {
      res.send(records);
    });
  },

  /**
   * Description
   * @return
   * @method getVideoUrl
   * @param {} req
   * @param {} res
   * @return 
   */
  getVideoUrl: function (req, res) {
    UserFiles.findOne({
      id: req.param('id'),
      file_type: 'video',
      select: ['file_dir']
    }).exec(function (err, record) {
      res.send(record);
    });
  },

  /**
   * Description
   * @return
   * @method getALbumName
   * @param {} req
   * @param {} res
   * @return 
   */
  getALbumName: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var albumId = req.param('id');
    MediaAlbum.findOne({id: albumId, select: ['name']}).exec(function (err, record) {
      res.send(record);
    })
  },

  /**
   * Description
   * @return
   * @method albumphotos
   * @param {} req
   * @param {} res
   * @return 
   */
  albumphotos: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var album = req.param('id');
    res.view({
      album: album, 'layout': 'admin/layout-media'
    });
  },


  /**
   * Get All File List from Album
   * @return
   * @method getAlbumItems
   * @param req
   * @param res
   * @return 
   */
  getAlbumItems: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var where = {};
    where.type = 'media';
    where.owner = LoggedInUserID;
    if (req.param('album')) {
      where.album = req.param('album');
    } else {
      where.album = '0';
    }
    if (req.param('started') && req.param('started') === 'yes') {
      where.starred = true;
    }
    where.fileThumb = {$exists: true, $ne: null};
    where.select = ['_id', 'name', 'file_dir', 'thumb_dir', 'poster_dir', 'file_type', 'ext', 'updatedAt'];
    var mediaItemQuery = UserFiles.find(where);
    if (req.param('recent') && req.param('recent') === 'recent') {
      mediaItemQuery.sort({updatedAt: -1});
      mediaItemQuery.limit(15);
    } else {
      mediaItemQuery.sort({createdAt: -1});
    }
    mediaItemQuery.populate('owner', {select: ['_id', 'first_name', 'last_name']});
    mediaItemQuery.exec(function foundCB(err, records) {
      var totalRecords = [];
      if (records) {
        AsyncCall.forEach(records, function (item, callback) {
          SharedFiles.findOne({file: item.id, owner: LoggedInUserID}).exec(function countCB(error, found) {

            if (found && found.users.length) {
              item.shared = found.users.length;
            }
            totalRecords.push(item);
            callback();
          });
        }, function (err) {
          if (err) {
            console.log(err);
          } else {
            res.send(totalRecords);
          }
        });
      }
    });
  },

  /**
   * Description
   * @return
   * @method allPhotoList
   * @param {} req
   * @param {} res
   * @return 
   */
  allPhotoList: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    UserFiles.find({
      file_type: {'!': 'video'},
      type: 'media',
      album: '0',
      owner: LoggedInUserID,
      fileFD: {$exists: true, $ne: null},
      fileThumb: {$exists: true, $ne: null},
      select: ['_id', 'name', 'file_dir', 'updatedAt', 'ext', 'thumb_dir']
    }).sort({createdAt: -1}).exec(function foundCB(err, records) {
      res.send(records);
    });
  },

  /**
   * Description
   * @return
   * @method allVideoList
   * @param {} req
   * @param {} res
   * @return 
   */
  allVideoList: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    UserFiles.find({
      file_type: 'video',
      type: 'media',
      album: '0',
      owner: LoggedInUserID,
      fileFD: {$exists: true, $ne: null},
      fileThumb: {$exists: true, $ne: null},
      select: ['_id', 'name', 'file_dir', 'thumb_dir', 'poster_dir', 'ext', 'updatedAt']
    }).sort({createdAt: -1}).exec(function foundCB(err, records) {
      res.send(records);
    });
  },

  /**
   * Description
   * @method renameAlbum
   * @param {} req
   * @param {} res
   * @return 
   */
  renameAlbum: function (req, res) {
    var album = req.param('album');
    var name = req.param('name');

    FilesFolder.update({id: album}, {name: name || ''}).exec(function afterwards(err, updated) {
      if (err) {
        console.log(err);
      }
      res.send(200);
    });
  },
  /**
   * Description
   * @return
   * @method allPhotoListShared
   * @param {} req
   * @param {} res
   * @return 
   */
  allPhotoListShared: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    SharedMedia.find({
      user_id: LoggedInUserID,
      select: ['_id', 'media_id', 'users']
    }).exec(function foundCB(err, records) {
      var shareRecords = [];
      if (records) {
        AsyncCall.each(records, function (record, cb) {
          if (record && record.media_id && record.users.length) {
            UserFiles.findOne({
              user_id: LoggedInUserID,
              id: record.media_id,
              select: ['_id', 'name', 'file_type', 'file_dir', 'updatedAt']
            })
              .then(function (urecord) {
                shareRecords.push(urecord);
                cb();
              })
              .fail(function (error) {
                console.log(error);
              })
          } else {
            cb();
          }
        }, function (error) {
          if (error) {
            console.log(error);
          }
          return res.send(shareRecords);
        });
      } else {
        return res.send(shareRecords);
      }

    });
  },


  /**
   * Description
   * @return
   * @method allPhotoListShareOthers
   * @param {} req
   * @param {} res
   * @return 
   */
  allPhotoListShareOthers: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    SharedMedia.find({
      users: LoggedInUserID,
      select: ['_id', 'media_id', 'users']
    }).exec(function foundCB(err, records) {
      var shareRecords = [];
      if (records) {
        AsyncCall.each(records, function (record, cb) {
          if (record && record.media_id) {
            UserFiles.findOne({id: record.media_id, select: ['_id', 'name', 'file_type', 'file_dir', 'updatedAt']}).exec(function (err, urecord) {
                shareRecords.push(urecord);
                cb();
              });
          } else {
            cb();
          }
        }, function (error) {
          if (error) {
            console.log(error);
          }
          return res.send(shareRecords);
        });
      } else {
        return res.send(shareRecords);
      }
    });
  },

  /**
   * Description
   * @return
   * @method allAlbumListShared
   * @param {} req
   * @param {} res
   * @return 
   */
  allAlbumListShared: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    SharedMedia.find({
      user_id: LoggedInUserID,
      select: ['_id', 'album_id', 'users']
    }).exec(function foundCB(err, records) {
      var shareRecords = [];
      if (records) {
        AsyncCall.each(records, function (record, cb) {
          if (record.album_id && record.users.length) {
            MediaAlbum.findOne({user_id: LoggedInUserID, id: record.album_id})
              .exec(function (err, urecord) {
                shareRecords.push(urecord);
                cb();
              });
          } else {
            cb();
          }
        }, function (error) {
          if (error) {
            console.log(error);
          }
          if (shareRecords && shareRecords.length) {
            var totalRecords = [];
            AsyncCall.forEach(shareRecords, function (item, callback) {
              if (item && item.length) {
                UserFiles.count({album: item.id}).exec(function countCB(error, found) {
                  var totalItem = found, coverPhoto = item.cover;
                  // console.log(coverPhoto);

                  UserFiles.count({album: item.id, file_type: 'video'}).exec(function countCB(error, foundv) {
                    item.videos = foundv;
                    item.photos = totalItem - foundv;


                    UserFiles.findOne({
                      album: item.id,
                      select: ['_id']
                    }).sort({createdAt: -1}).exec(function (err, record) {
                      if (typeof(coverPhoto) == 'undefined' && record) {
                        item.cover = record.id;
                      }
                      totalRecords.push(item);
                      callback();
                    });

                  })

                });
              } else {
                callback();
              }


            }, function (err) {
              if (err) {
                console.log(err);
              } else {
                res.send(totalRecords);
              }
            });
          }


        });
      } else {
        return res.send(shareRecords);
      }

    })
  },

  /**
   * Description
   * @return
   * @method allAlbumListShareOthers
   * @param {} req
   * @param {} res
   * @return 
   */
  allAlbumListShareOthers: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    SharedMedia.find({
      users: LoggedInUserID,
      select: ['_id', 'album_id', 'users']
    }).exec(function foundCB(err, records) {
      var shareRecords = [];
      if (records) {
        AsyncCall.each(records, function (record, cb) {
          if (record.album_id && record.users.length) {
            MediaAlbum.findOne({id: record.album_id}).exec(function (urecord) {
                shareRecords.push(urecord);
                cb();
              });
          } else {
            cb();
          }
        }, function (error) {
          if (error) {
            console.log(error);
          }
          if (shareRecords) {
            var totalRecords = [];
            AsyncCall.forEach(shareRecords, function (item, callback) {
              UserFiles.count({album: item.id}).exec(function countCB(error, found) {
                var totalItem = found, coverPhoto = item.cover;
                // console.log(coverPhoto);

                UserFiles.count({album: item.id, file_type: 'video'}).exec(function countCB(error, foundv) {
                  item.videos = foundv;
                  item.photos = totalItem - foundv;


                  UserFiles.findOne({
                    album: item.id,
                    select: ['_id']
                  }).sort({createdAt: -1}).exec(function (err, record) {
                    if (typeof(coverPhoto) == 'undefined' && record) {
                      item.cover = record.id;
                    }
                    totalRecords.push(item);
                    callback();
                  });

                })

              });

            }, function (err) {
              if (err) {
                console.log(err);
              } else {
                res.send(totalRecords);
              }
            });
          }


        });
      } else {
        return res.send(shareRecords);
      }

    });
  },

  /**
   * Description
   * @return
   * @method allAlbumPhotoList
   * @param {} req
   * @param {} res
   * @return 
   */
  allAlbumPhotoList: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var albumId = req.param('id');
    UserFiles.find({
      type: 'media',
      album: albumId,
      fileFD: {$exists: true, $ne: null},
      fileThumb: {$exists: true, $ne: null},
      select: ['_id', 'name', 'file_type', 'file_dir', 'updatedAt']
    }).exec(function foundCB(err, records) {
      res.send(records);
    });
  },

  /**
   * Description
   * @method getPhotoInfoFromId
   * @param {} req
   * @param {} res
   * @return 
   */
  getPhotoInfoFromId: function (req, res) {
    var ID = req.param('id');
    if (ID) {
      UserFiles.findOne(ID).exec(function (err, record) {
        if (record) {
          res.send(record);
        } else {
          res.send('no file found');
        }
      });
    } else {
      res.send(200);
    }
  },

  /**
   * Description
   * @return
   * @method getAllAlbumList
   * @param {} req
   * @param {} res
   * @return 
   */
  getAllAlbumList: function (req, res) {
    let LoggedInUserID = req.session.User.id;
    MediaAlbum.find({
      owner: LoggedInUserID,
      select: ['_id', 'name', 'cover', 'updatedAt']
    }).sort({createdAt: -1}).exec(function foundCB(err, records) {
      if (err) return next(err);

      let totalRecords = [];
      if (records) {
        AsyncCall.forEach(records, function (item, callback) {
          UserFiles.count({album: item.id}).exec(function countCB(error, found) {
            var totalItem = found, coverPhoto = item.cover;
            // console.log(coverPhoto);

            UserFiles.count({album: item.id, file_type: 'video'}).exec(function countCB(error, foundv) {
              item.videos = foundv;
              item.photos = totalItem - foundv;

              UserFiles.findOne({
                album: item.id,
                select: ['_id', 'thumb_dir']
              }).sort({createdAt: -1}).exec(function (err, record) {
                if (typeof(coverPhoto) == 'undefined' && record) {
                  item.cover = record.thumb_dir;
                }
                totalRecords.push(item);
                callback();
              });
            });
          });
        }, function (err) {
          if (err) {
            console.log(err);
          } else {
            res.send(totalRecords);
          }
        });
      }
    });
  },

  /**
   * Description
   * @method getSharedItemList
   * @param {} req
   * @param {} res
   * @return 
   */
  getSharedItemList: function (req, res) {
    var loggedInUserId = req.session.User.id;
    let items = {};
    AsyncCall.parallel([
        function(callback) {
          MediaService.getSharedAlbums(loggedInUserId, function (albumData) {
            items.album = albumData;
            callback();
          });
        },
        function(callback) {
          SharedMedia.find({
            media: {$ne: null},
            users: loggedInUserId
          }).populate('media', {select: ['name', 'file_dir', 'ext', 'fileThumb', 'thumb_dir', 'type', 'size', 'owner', 'updatedAt']}).exec(function (err, fileResp) {
            if (fileResp) {
              items.photos = fileResp.map(mediaObj=>{
                return Object.assign(mediaObj, mediaObj.media);
              });
              // items.photos = fileResp;
              callback();
            }
          });
        }
      ],
      function(err, results) {
        res.json(items);
      });


    // SharedMedia.find({
    //   album: {$ne: null},
    //   users: loggedInUserId
    // }).populate('album', {select: ['_id', 'name', 'root', 'owner', 'type', 'updatedAt', 'createdAt']}).exec(function (err, resp) {
    //   if (resp) {
    //     items.album = resp;
    //     SharedMedia.find({
    //       media: {$ne: null},
    //       users: loggedInUserId
    //     }).populate('media', {select: ['_id', 'name', 'file_dir', 'ext', 'fileThumb', 'type', 'size', 'owner', 'updatedAt']}).exec(function (err, fileResp) {
    //       if (fileResp) {
    //         items.photos = fileResp;
    //         res.send(items);
    //       }
    //     });
    //   }
    // });

    // Contacts.findOne({
    //   user_id: loggedInUserId,
    //   select: ['_id', 'first_name', 'last_name', 'email', 'updatedAt']
    // }).exec(function (err, contactData) {
    //   if (contactData) {
    //     var items = {};
    //     SharedMedia.find({
    //       album: {$ne: null},
    //       users: contactData.id
    //     }).populate('album', {select: ['_id', 'name', 'root', 'owner', 'type', 'updatedAt', 'createdAt']}).exec(function (err, resp) {
    //       if (resp) {
    //         items.album = resp;
    //         SharedMedia.find({
    //           media: {$ne: null},
    //           users: contactData.id
    //         }).populate('media', {select: ['_id', 'name', 'file_dir', 'ext', 'fileThumb', 'type', 'size', 'owner', 'updatedAt']}).exec(function (err, fileResp) {
    //           if (fileResp) {
    //             items.photos = fileResp;
    //             res.send(items);
    //           }
    //         });
    //       }
    //     });
    //   } else {
    //     res.send([]);
    //   }
    // });
  },

  /**
   * Description
   * @return
   * @method getOtherAlbums
   * @param {} req
   * @param {} res
   * @return 
   */
  getOtherAlbums: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    MediaAlbum.find({
      id: {'!': req.param('id')},
      user_id: LoggedInUserID,
      select: ['_id', 'name']
    }).exec(function foundCB(err, records) {
      if (err) return next(err);
      res.json(records);
    });
  },

  /**
   * Description
   * @return
   * @method getAllAlbums
   * @param {} req
   * @param {} res
   * @return 
   */
  getAllAlbums: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    MediaAlbum.find({user_id: LoggedInUserID, select: ['_id', 'name']}).exec(function foundCB(err, records) {
      if (err) return next(err);
      res.json(records);
    });
  },

  /**
   * Description
   * @return
   * @method deleteAlbum
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  deleteAlbum: function (req, res, next) {
    var LoggedInUserID = req.session.User.id;
    MediaAlbum.findOne(req.param('id'), function foundCB(err, record) {
      if (err) console.log(err);
      if (record) {
        MediaAlbum.destroy({id: req.param('id'), owner: LoggedInUserID}, function destroyedCB(err) {
          if (err) return next(err);
          UserFiles.find({
            type: 'media',
            album: req.param('id'),
            owner: LoggedInUserID,
            select: ['_id', 'file_dir']
          }).exec(function foundCB(err, records) {

            if (records) {
              AsyncCall.forEach(records, function (item, callback) {


                // For Main File
                var fileName = path.basename(item.file_dir);
                var fileDir = 'assets/uploads/files/' + LoggedInUserID + '/media/' + fileName;

                // For Thumb
                var fileNameOnly = fileName.substr(0, fileName.lastIndexOf('.'));
                var fileDirThumb = 'assets/uploads/files/' + LoggedInUserID + '/media/thumb/' + fileNameOnly + '.jpg';
                var fileDirPoster = 'assets/uploads/files/' + LoggedInUserID + '/media/poster/' + fileNameOnly + '.jpeg';

                fs.unlink(fileDir, function (e) {
                  if (e) {
                    throw e;
                  } else {
                    fs.unlink(fileDirThumb, function (e) {
                      if (e) {
                        throw e;
                      }
                      if (item.file_type && item.file_type === 'video' && fileDirPoster) {
                        fs.unlink(fileDirPoster, function (e) {
                          if (e) console.log(e);
                        });
                      }
                    });
                  }
                });

              }, function (err) {
                if (err) {
                  console.log(err);
                } else {

                }
              });

              UserFiles.destroy({album: req.param('id'), user_id: LoggedInUserID}, function destroyedCB(err) {
                if (err) {
                  console.log(err);
                }
                SharedMedia.findOne({album: req.param('id')}, function foundCB(err, record) {
                  if (record) {
                    SharedMedia.destroy({album: req.param('id')}, function destroyedCB(err) {
                      if (err) console.log(err);
                      res.json('deleted');
                    });
                  } else {
                    res.json('deleted');
                  }
                });
              });
            }
          });
        });
      }
    });
  },

  /**
   * Description
   * @method getFolderFilesArchive
   * @param {} req
   * @param {} res
   * @return 
   */
  getFolderFilesArchive: function (req, res) {
    var LoggedInUserID = req.session.User.id;


  },

  /**
   * Description
   * @method downloadAlbum
   * @param {} req
   * @param {} res
   * @return 
   */
  downloadAlbum: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var tempDir;
    let albumId = req.param('id');
    var archive = archiver('zip', {
      // store: true // Sets the compression method to STORE.
    });

    // res.writeHead(200, {
    //   'Content-Type': 'application/zip',
    //   'Content-disposition': 'attachment; filename=files.zip'
    // });

    tempDir = 'assets/uploads/files/' + LoggedInUserID + '/media/temp';
    if (!fs.existsSync(tempDir)) {
      fs.mkdirSync(tempDir);
    }
    var randNum = new Date().getTime();

    var FinalDirectory = 'files_' + randNum + '.zip';
    // var baseURL = Utility.getSiteBaseURL(req);
    var output = fs.createWriteStream(tempDir + '/' + FinalDirectory);


    // good practice to catch this error explicitly
    archive.on('error', function (err) {
      console.log(err);
    });

    // pipe archive data to the file
    // archive.pipe(res);
    archive.pipe(output);

    MediaAlbum.findOne(albumId, function foundCB(err, albumResp) {
      if (albumResp) {
        UserFiles.find({type: 'media', album: albumId}).exec(function foundCB(err, records) {
          if (records) {
            AsyncCall.forEach(records, function (item, callback) {
              // For Main File
              var fileName = path.basename(item.file_dir);
              var fileNameOri = item.name;
              var fileDir = path.resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserID + '/media/' + fileName;

              // var file1 = __dirname + '/file1.txt';
              archive.append(fs.createReadStream(fileDir), {name: fileNameOri});
              callback();

            }, function (err) {
              if (err) {
                console.log(err);
              }
              archive.finalize();
              // res.send(FinalDirectory);
            });

            setTimeout(function () {
              res.send('/uploads/files/' + LoggedInUserID + '/media/temp/' + FinalDirectory);
            }, 1000);
          } else {
            res.send(200);
          }

        });
      } else {
        res.send(200);
      }
    });


  },

  /**
   * Description
   * @method itemsShareWithUsers
   * @param {} req
   * @param {} res
   * @return 
   */
  itemsShareWithUsers: function (req, res) {
    let LoggedInUserID = req.session.User.id;
    let shareData = req.allParams();

    let where = {};
    if( shareData.type ==='album'){
      where.album = shareData.id;
    }else if(shareData.type === 'media'){
      where.media = shareData.id;
    }

    SharedMedia.findOne(where).exec(function (err, resp) {
      if (err) console.log(err);
      if (resp) {
        SharedMedia.update(where, {users: shareData.users}, function updatedCB(err) {
          if (err) console.log(err);
          res.send(200);
        });
      } else {
        let sharedData = {owner: LoggedInUserID};
        if( shareData.type ==='album'){
          sharedData.album = shareData.id;
        }else if(shareData.type === 'media'){
          sharedData.media = shareData.id;
        }
        sharedData.users = shareData.users;
        SharedMedia.create(sharedData, function fileCreated(err) {
          if (err) {
            console.log(err);
          }
          res.send(200);
        });
      }
    });
  },

  /**
   * Description
   * @method getPreviousSharedUsers
   * @param {} req
   * @param {} res
   * @return 
   */
  getPreviousSharedUsers: function (req, res) {
    let mediaData = req.allParams();
    let where = {};
    if( mediaData.type === 'album'){
      where.album = mediaData.id;
    }else if( mediaData.type === 'media'){
      where.media = mediaData.id;
    }
    SharedMedia.findOne(where).exec(function (err, resp) {
      if (err) { console.log(err); }
      if (resp && resp.users ) {
        User.find({
          id: resp.users,
          select: ['_id', 'first_name', 'last_name', 'email', 'updatedAt', 'photo_url']
        }).exec(function (err, respContact) {
          if (err)console.log(err);
          let contactData = respContact.map(contact=>{
            return {value: contact.first_name.toLowerCase(), name: contact.first_name + ' ' + contact.last_name,
              email: respContact.email, image: respContact.photo_url || 'images/avatars/blank.jpg', id: contact.id};
          });
          res.send(contactData);
        });
      } else {
        res.send([]);
      }
    });
  },

  /**
   * Description
   * @return
   * @method deleteAlbumPhoto
   * @param {} req
   * @param {} res
   * @return 
   */
  deleteAlbumPhoto: function (req, res) {
    var LoggedInUserid = req.session.User.id;
    UserFiles.findOne(req.param('id'), function foundCB(err, record) {
      if (err) return next(err);

      if (record) {
        UserFiles.destroy({id: req.param('id'), user_id: LoggedInUserid}, function destroyedCB(err) {
          if (err) return next(err);

          // For Main File
          var fileName = path.basename(record.file_dir);
          var fileDir = 'assets/uploads/files/' + LoggedInUserid + '/media/' + fileName;

          // For Thumb
          var fileNameOnly = fileName.substr(0, fileName.lastIndexOf('.'));
          var fileDirThumb = 'assets/uploads/files/' + LoggedInUserid + '/media/thumb/' + fileNameOnly + '.jpg';


          if (fs.existsSync(fileDir)) {
            fs.unlink(fileDir, function (e) {
              if (e) {
                throw e;
              } else {
                fs.unlink(fileDirThumb, function (e) {
                  if (e) {
                    throw e;
                  }
                  SharedMedia.findOne({media_id: req.param('id')}, function foundCB(err, record) {
                    if (record) {
                      SharedMedia.destroy({media_id: req.param('id')}, function destroyedCB(err) {
                        if (err) console.log(err);
                        res.json('deleted');
                      })
                    } else {
                      res.json('deleted');
                    }
                  })
                });
              }
            });
          } else {
            res.json('deleted');
          }


        });
      }

    });
  },

  /**
   * Description
   * @return
   * @method deleteVideo
   * @param {} req
   * @param {} res
   * @return 
   */
  deleteVideo: function (req, res) {
    var LoggedInUserid = req.session.User.id;
    UserFiles.findOne(req.param('id'), function foundCB(err, record) {
      if (err) return next(err);

      if (record) {
        UserFiles.destroy({id: req.param('id'), user_id: LoggedInUserid}, function destroyedCB(err) {
          if (err) return next(err);

          // For Main File
          var fileName = path.basename(record.file_dir);
          var fileDir = 'assets/uploads/files/' + LoggedInUserid + '/media/' + fileName;

          // For Thumb
          var fileNameOnly = fileName.substr(0, fileName.lastIndexOf('.'));
          var fileDirThumb = 'assets/uploads/files/' + LoggedInUserid + '/media/thumb/' + fileNameOnly + '.jpg';

          // For Poster
          var fileDirPoster = 'assets/uploads/files/' + LoggedInUserid + '/media/poster/' + fileNameOnly + '.jpeg';

          if (fs.existsSync(fileDir)) {
            fs.unlink(fileDir, function (err) {
              if (err) throw err;

              if (fs.existsSync(fileDirThumb)) {
                fs.unlink(fileDirThumb, function (err) {
                  if (err) throw err;
                  // res.json('deleted');
                });

                if (fs.existsSync(fileDirPoster)) {
                  fs.unlink(fileDirPoster, function (err) {
                    if (err) throw err;
                    res.json('deleted');
                  });
                }
              }

            })
            ;
          } else {
            res.json('deleted');
          }


        });
      }

    });
  },

  /**
   * Description
   * @return
   * @method renameAlbum
   * @param {} req
   * @param {} res
   * @return 
   */
  renameAlbum: function (req, res) {
    MediaAlbum.findOne(req.param('album'), function foundCB(err, record) {
      if (err) console.log(err);
      if (record) {
        MediaAlbum.update(req.param('album'), {name: req.param('name')}, function updatedCB(err, updated) {
          if (err) console.log(err);
          res.json(updated[0]);
        });
      }
    });
  },

  /**
   * Description
   * @return
   * @method editphoto
   * @param {} req
   * @param {} res
   * @return 
   */
  editphoto: function (req, res) {
    var LoggedInUserid = req.session.User.id;
    UserFiles.findOne({
      id: req.param('id'),
      type: 'media',
      user_id: LoggedInUserid,
      select: ['_id', 'name', 'file_dir', 'updatedAt', 'album']
    }).exec(function (err, fileRes) {
      if (err) return res.negotiate(err);
      res.view({data: fileRes, 'layout': 'admin/layout-photo-editor'});
    });
  },


  /**
   * Description
   * @return
   * @method photoeditor
   * @param {} req
   * @param {} res
   * @return 
   */
  photoeditor: function (req, res) {
    var LoggedInUserid = req.session.User.id;
    UserFiles.findOne({
      id: req.param('id'),
      type: 'media',
      user: LoggedInUserid,
      select: ['_id', 'name', 'album', 'file_dir', 'fileFD', 'updatedAt', 'album']
    }).exec(function (err, fileRes) {
      if (err) return res.negotiate(err);
      if (fileRes && req.param('album')) {
        fileRes.album = req.param('album');
        res.view({data: fileRes, 'layout': 'layout-editor'});
      } else {
        res.send(200);
      }
    });
  },


  /**
   * Description
   * @return
   * @method photoeditoralbum
   * @param {} req
   * @param {} res
   * @return 
   */
  photoeditoralbum: function (req, res) {
    var LoggedInUserid = req.session.User.id;
    UserFiles.findOne({
      id: req.param('id'),
      type: 'media',
      user_id: LoggedInUserid,
      select: ['_id', 'name', 'album', 'file_dir', 'fileFD', 'updatedAt', 'album']
    }).exec(function (err, fileRes) {
      if (err) return res.negotiate(err);
      res.view({data: fileRes, album: req.param('album'), 'layout': 'admin/layout-photo-editor'});
    });
  },

  /**
   * Description
   * @return
   * @method uploadMedia
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  uploadMedia: function (req, res, next) {
    var LoggedInUserid = req.session.User.id;
    var random_date = new Date();
    var randNum = random_date.getTime();
    var thumbDir;

    req.file('file').upload({
      dirname: path.resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserid + '/media',
      /**
       * Description
       * @return
       * @method saveAs
       * @param {} __newFileStream
       * @param {} cb
       * @return 
       */
      saveAs: function (__newFileStream, cb) {
        var fname = __newFileStream.filename;
        var mName = fname.substr(0, fname.lastIndexOf('.'));
        var fName = mName.replace(/\s\s+/g, ' ');
        fName = fName.replace(/ /g, "-");
        var ext = path.extname(__newFileStream.filename);
        cb(null, fName + "_" + randNum + ext);
      },
      // don't allow the total upload size to exceed ~10MB
      maxBytes: 10000000
    }, function whenDone(err, uploadedFiles) {


      if (err) {
        return res.negotiate(err);
      }

      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0) {
        return res.badRequest('No file was uploaded');
      } else {
        var baseURL = Utility.getSiteBaseURL(req);
        var file_dir = '/uploads/files/' + LoggedInUserid + '/media/' + path.basename(uploadedFiles[0].fd);

        thumbDir = 'assets/uploads/files/' + LoggedInUserid + '/media/thumb';
        if (!fs.existsSync(thumbDir)) {
          fs.mkdirSync(thumbDir);
        }

        Jimp.read(uploadedFiles[0].fd, function (err, image) {
          if (err) throw err;

          var extraLen;
          var img_w = parseInt(image.bitmap.width);
          var img_h = parseInt(image.bitmap.height);

          var fileNameWithExt = path.basename(uploadedFiles[0].fd);
          var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));

          if (img_w > img_h) {
            extraLen = (img_w - img_h) / 2;
            image.crop(Math.floor(extraLen), 0, img_h, img_h);
          } else {
            extraLen = (img_h - img_w) / 2;
            image.crop(0, Math.floor(extraLen), img_w, img_w);
          }

          image.resize(220, Jimp.AUTO)
            .quality(60)
            .write('assets/uploads/files/' + LoggedInUserid + '/media/thumb/' + uFileName + '.jpg'); // save
        }).catch(function (err) {
          console.log(err);
        });

        var fileNameWithExt = path.basename(uploadedFiles[0].fd);
        var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));

        var genThumUrl = path.dirname(uploadedFiles[0].fd) + '/thumb/' + uFileName + '.jpg';

        var files = {};
        files.name = uploadedFiles[0].filename;
        files.type = 'media';
        files.owner = LoggedInUserid;
        files.file_dir = file_dir;
        files.fileFD = uploadedFiles[0].fd;
        files.fileThumb = genThumUrl;
        files.size = uploadedFiles[0].size;


        UserFiles.create(files, function fileCreated(err, file) {
          var fileMUrl = require('util').format('%s/users/files/%s', baseURL, file.id);
          if (err) {
            req.session.flash = {
              err: err
            }
            return res.redirect(res.locals.admin_url_prefix + '/media/upload');
          } else {
            return res.json(fileMUrl);
          }
        });
      }

    });
  },

  /**
   * Description
   * @method deleteItems
   * @param {} req
   * @param {} res
   * @return 
   */
  deleteItems: function (req, res) {
    var LoggedInUserID = req.session.User.id;
    var files = req.param('items');
    if (files && files.length) {

      UserFiles.find({id: files}).exec(function (err, records) {
        AsyncCall.forEach(records, function (item, callback) {
          // For Main File
          if (item.id) {
            var fileName = path.basename(item.file_dir);
            var fileDir = 'assets/uploads/files/' + LoggedInUserID + '/media/' + fileName;

            rimraf(fileDir, function () {
              if (item.thumb_dir) {
                var thumbDir = 'assets/uploads/files/' + LoggedInUserID + '/media/thumb/' + fileName;
                rimraf(thumbDir, function () {
                  if (item.poster_dir) {
                    var posterDir = 'assets/uploads/files/' + LoggedInUserID + '/media/poster/' + fileName;
                    rimraf(posterDir, function () {
                    });
                  }
                });
              }
              UserFiles.destroy({id: item.id}).exec(function (err) {
                callback();
              });
            });
          } else {
            callback();
          }
        }, function (err) {
          if (err) {
            console.log(err);
          } else {
            res.send('Files are deleted');
          }
        });
      });
    }
  },

  /**
   * Description
   * @return
   * @method uploadMediaVideo
   * @param {} req
   * @param {} res
   * @return 
   */
  uploadMediaVideo: function (req, res) {
    var LoggedInUserid = req.session.User.id;
    var random_date = new Date();
    var randNum = random_date.getTime();
    var thumbDir;
    var posterDir;

    req.file('file').upload({
      dirname: path.resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserid + '/media',
      /**
       * Description
       * @return
       * @method saveAs
       * @param {} __newFileStream
       * @param {} cb
       * @return 
       */
      saveAs: function (__newFileStream, cb) {
        var fname = __newFileStream.filename;
        var mName = fname.substr(0, fname.lastIndexOf('.'));
        var fName = mName.replace(/\s\s+/g, ' ');
        fName = fName.replace(/ /g, "-");
        var ext = path.extname(__newFileStream.filename);
        cb(null, fName + "_" + randNum + ext);
      },
      // don't allow the total upload size to exceed ~10MB
      maxBytes: 900000000
    }, function whenDone(err, uploadedFiles) {


      if (err) {
        return res.negotiate(err);
      }

      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0) {
        return res.badRequest('No file was uploaded');
      } else {
        var fileType = uploadedFiles[0].type;
        var fileExtName = fileType.substr(0, fileType.lastIndexOf('/'));

        if (fileExtName === 'image' || fileExtName === 'video') {
          var fileNameWithExt = path.basename(uploadedFiles[0].fd);
          var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));

          var baseURL = Utility.getSiteBaseURL(req);
          var file_dir = '/uploads/files/' + LoggedInUserid + '/media/' + path.basename(uploadedFiles[0].fd);
          var genPosterUrl;
          var genThumUrl;
          var tempPosterDir;
          var thumb_dir;
          var poster_dir;

          if (fileExtName === 'image') {

            thumbDir = 'assets/uploads/files/' + LoggedInUserid + '/media/thumb';
            if (!fs.existsSync(thumbDir)) {
              fs.mkdirSync(thumbDir);
            }


            Jimp.read(uploadedFiles[0].fd, function (err, image) {
              if (err) throw err;

              var extraLen;
              var img_w = parseInt(image.bitmap.width);
              var img_h = parseInt(image.bitmap.height);


              if (img_w > img_h) {
                extraLen = (img_w - img_h) / 2;
                image.crop(Math.floor(extraLen), 0, img_h, img_h);
              } else {
                extraLen = (img_h - img_w) / 2;
                image.crop(0, Math.floor(extraLen), img_w, img_w);
              }

              image.resize(300, Jimp.AUTO)
                .quality(60)
                .write('assets/uploads/files/' + LoggedInUserid + '/media/thumb/' + uFileName + '.jpg'); // save
            }).catch(function (err) {
              console.log(err);
            });


            genThumUrl = path.dirname(uploadedFiles[0].fd) + '/thumb/' + uFileName + '.jpg';
            thumb_dir = '/uploads/files/' + LoggedInUserid + '/media/thumb/' + uFileName + '.jpg';

          } else if (fileExtName === 'video') {

            thumbDir = 'assets/uploads/files/' + LoggedInUserid + '/media/thumb';
            posterDir = 'assets/uploads/files/' + LoggedInUserid + '/media/poster';
            if (!fs.existsSync(thumbDir)) {
              fs.mkdirSync(thumbDir);
            }
            if (!fs.existsSync(posterDir)) {
              fs.mkdirSync(posterDir);
            }

            Thumbler({
              type: 'video',
              input: uploadedFiles[0].fd,
              output: 'assets/uploads/files/' + LoggedInUserid + '/media/poster/' + uFileName + '.jpeg',
              time: '00:00:03'
            }, function (err, path) {
              if (err) return err;
              // return path;
              // console.log('done');
            });

            tempPosterDir = 'assets/uploads/files/' + LoggedInUserid + '/media/poster/' + uFileName + '.jpeg';

            setTimeout(function () {

              Jimp.read(tempPosterDir, function (err, image) {
                if (err) {
                  console.log(err);
                }

                var fileNameWithExt = path.basename(uploadedFiles[0].fd);
                var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));

                var extraLen;
                var img_w = parseInt(image.bitmap.width);
                var img_h = parseInt(image.bitmap.height);

                if (img_w > img_h) {
                  extraLen = (img_w - img_h) / 2;
                  image.crop(Math.floor(extraLen), 0, img_h, img_h);
                } else {
                  extraLen = (img_h - img_w) / 2;
                  image.crop(0, Math.floor(extraLen), img_w, img_w);
                }

                image.resize(300, Jimp.AUTO)
                  .quality(60)
                  .write('assets/uploads/files/' + LoggedInUserid + '/media/thumb/' + uFileName + '.jpg'); // save
              }).catch(function (err) {
                console.log(err);
              });

            }, 3000);

            genThumUrl = path.dirname(uploadedFiles[0].fd) + '/thumb/' + uFileName + '.jpg';
            genPosterUrl = path.dirname(uploadedFiles[0].fd) + '/poster/' + uFileName + '.jpeg';
            thumb_dir = '/uploads/files/' + LoggedInUserid + '/media/thumb/' + uFileName + '.jpg';
            poster_dir = '/uploads/files/' + LoggedInUserid + '/media/poster/' + uFileName + '.jpeg';
          }

        }

        var files = {};
        files.name = uploadedFiles[0].filename;
        files.type = 'media';
        if (fileExtName === 'video') {
          files.file_type = 'video';
        }
        files.ext = path.extname(uploadedFiles[0].filename);
        files.owner = LoggedInUserid;
        files.file_dir = file_dir;
        files.fileFD = uploadedFiles[0].fd;
        files.size = uploadedFiles[0].size;

        // if (req.param('album') && req.param('album').length > 3) {
        //   files.album = [req.param('album')];
        // }
        files.album = req.param('album') || '0';

        if (genThumUrl) {
          files.fileThumb = genThumUrl;
          files.thumb_dir = thumb_dir;
        }
        if (genPosterUrl) {
          files.filePoster = genPosterUrl;
          files.poster_dir = poster_dir;
        }

        getLocationIfFound( uploadedFiles[0].fd, function ( geo ) {
          var metaData = {};
          if( geo && geo.city ){
            metaData.city = geo.city;
            metaData.country = geo.country;
            metaData.countryCode = geo.countryCode;
            metaData.zipcode = geo.zipcode;
            metaData.streetName = geo.streetName;
            metaData.formattedAddress = geo.formattedAddress;
            metaData.latitude = geo.latitude;
            metaData.longitude = geo.longitude;
            files.metaData = metaData;
          }

          UserFiles.create(files, function fileCreated(err, file) {
            var fileMUrl = require('util').format('%s/users/files/%s', baseURL, file.id);
            if (err) {
              console.log(err);
              res.send(404);
            } else {
              setTimeout(function () {
                return res.json(fileMUrl);
              }, 1000);
            }
          });
        });



      }
    });
  },

  /**
   * Description
   * @return
   * @method uploadMediaEdited
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  uploadMediaEdited: function (req, res, next) {
    var LoggedInUserid = req.session.User.id;

    UserFiles.findOne(req.param('id')).exec(function (err, fileRes) {
      if (fileRes) {
        var saveToServerName;
        var randNum = new Date().getTime();
        req.file('file').upload({
          dirname: path.resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserid + '/media',
          /**
           * Description
           * @return
           * @method saveAs
           * @param {} __newFileStream
           * @param {} cb
           * @return 
           */
          saveAs: function (__newFileStream, cb) {
            if (req.param('copy') && req.param('copy') === 'yes') {
              var fname = __newFileStream.filename;
              var mName = fname.substr(0, fname.lastIndexOf('.'));
              var fName = mName.replace(/\s\s+/g, ' ');
              fName = fName.replace(/ /g, "-");
              var ext = path.extname(__newFileStream.filename);
              saveToServerName = fName + "_" + randNum + ext;
            } else {
              saveToServerName = path.basename(fileRes.file_dir);
            }
            cb(null, saveToServerName);
          },

          maxBytes: 10000000
        }, function whenDone(err, uploadedFiles) {


          if (err) {
            return res.negotiate(err);
          }

          // If no files were uploaded, respond with an error.
          if (uploadedFiles.length === 0) {
            return res.badRequest('No file was uploaded');
          } else {
            // var baseURL = Utility.getSiteBaseURL(req);

            Jimp.read(uploadedFiles[0].fd, function (err, image) {
              if (err) throw err;
              var extraLen;
              var img_w = parseInt(image.bitmap.width);
              var img_h = parseInt(image.bitmap.height);


              if (img_w > img_h) {
                extraLen = (img_w - img_h) / 2;
                image.crop(Math.floor(extraLen), 0, img_h, img_h);
              } else {
                extraLen = (img_h - img_w) / 2;
                image.crop(0, Math.floor(extraLen), img_w, img_w);
              }

              var fileNameWithExt = path.basename(uploadedFiles[0].fd);
              var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));

              image.resize(220, Jimp.AUTO)
                .quality(60)
                .write('assets/uploads/files/' + LoggedInUserid + '/media/thumb/' + uFileName + '.jpg'); // save
            }).catch(function (err) {
              console.log(err);
            });

            var fileNameWithExt = path.basename(uploadedFiles[0].fd);
            var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));
            var genThumUrl = path.dirname(uploadedFiles[0].fd) + '/thumb/' + uFileName + '.jpg';
            var thumb_dir = '/uploads/files/' + LoggedInUserid + '/media/thumb/' + uFileName + '.jpg';
            var file_dir = '/uploads/files/' + LoggedInUserid + '/media/' + path.basename(uploadedFiles[0].fd);


            var files = {};
            files.name = uploadedFiles[0].filename;
            files.ext = path.extname(uploadedFiles[0].filename);
            files.type = 'media';
            files.owner = LoggedInUserid;
            files.file_dir = file_dir;
            files.fileFD = uploadedFiles[0].fd;
            files.fileThumb = genThumUrl;
            files.thumb_dir = thumb_dir;
            files.size = uploadedFiles[0].size;
            files.album = req.param('album') || '0';


            if (req.param('copy') && req.param('copy') === 'yes') {
              UserFiles.create(files, function fileCreated(err, file) {
                if (file) {
                  setTimeout(function () {
                    return res.send(file);
                  }, 1000);
                }
              });
            } else {
              UserFiles.update(req.param('id'), files, function updatedCB(err, updatedResp) {
                res.send(updatedResp[0]);
              });
            }

          }
        });
      }
    });
  },

  /**
   * Description
   * @method getPhotoAlbumListByLocation
   * @param {} req
   * @param {} res
   * @return 
   */
  getPhotoAlbumListByLocation: function(req, res){
    var LoggedInUserID = req.session.User.id;
    UserFiles.find({
      file_type: {'!': 'video'},
      type: 'media',
      owner: LoggedInUserID,
      fileFD: {$exists: true, $ne: null},
      fileThumb: {$exists: true, $ne: null},
      metaData: {$exists: true, $ne: null},
      select: ['_id', 'name', 'file_dir', 'ext', 'thumb_dir', 'metaData', 'updatedAt']
    }).sort({createdAt: -1}).exec(function foundCB(err, records) {
      var filterData = {};
      filterData.locations = [];
      filterData.items = {};
      records.forEach(function (val) {
        if (val.metaData && val.metaData.city) {
          if (filterData.items[val.metaData.city]) {
            filterData.items[val.metaData.city].push(val);
          } else {
            var lData = {};
            lData.city = val.metaData.city;
            lData.country = val.metaData.country;
            filterData.locations.push(lData);
            filterData.items[val.metaData.city] = [];
            filterData.items[val.metaData.city].push(val);
          }
        }
      });
      res.send(filterData);
    });
  },

  /**
   * Description
   * @return
   * @method uploadMediaEditedSaveAs
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  uploadMediaEditedSaveAs: function (req, res, next) {
    var LoggedInUserid = req.session.User.id;

    UserFiles.findOne(req.param('id')).exec(function (err, fileRes) {

      if (fileRes) {
        var fineName = path.basename(fileRes.file_dir);
        req.file('file').upload({
          dirname: path.resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserid + '/media',
          /**
           * Description
           * @return
           * @method saveAs
           * @param {} __newFileStream
           * @param {} cb
           * @return 
           */
          saveAs: function (__newFileStream, cb) {
            var mName = fineName.substr(0, fineName.lastIndexOf('.'));
            var fName = mName + '_new';
            var ext = path.extname(fineName);
            cb(null, fName + ext);
          },
          // don't allow the total upload size to exceed ~10MB
          maxBytes: 10000000
        }, function whenDone(err, uploadedFiles) {


          if (err) {
            return res.negotiate(err);
          }

          // If no files were uploaded, respond with an error.
          if (uploadedFiles.length === 0) {
            return res.badRequest('No file was uploaded');
          } else {
            var baseURL = Utility.getSiteBaseURL(req);
            var file_dir = '/uploads/files/' + LoggedInUserid + '/media/' + path.basename(uploadedFiles[0].fd);
            Jimp.read(uploadedFiles[0].fd, function (err, image) {
              if (err) throw err;
              var extraLen;
              var img_w = parseInt(image.bitmap.width);
              var img_h = parseInt(image.bitmap.height);

              var fileNameWithExt = path.basename(uploadedFiles[0].fd);

              if (img_w > img_h) {
                extraLen = (img_w - img_h) / 2;
                image.crop(Math.floor(extraLen), 0, img_h, img_h);
              } else {
                extraLen = (img_h - img_w) / 2;
                image.crop(0, Math.floor(extraLen), img_w, img_w);
              }

              // Renaming Image
              var mName = fineName.substr(0, fineName.lastIndexOf('.'));
              var fName = mName + '_new';

              image.resize(220, Jimp.AUTO)
                .quality(60)
                .write('assets/uploads/files/' + LoggedInUserid + '/media/thumb/' + fName + '.jpg'); // save
            }).catch(function (err) {
              console.log(err);
            });

            var fileNameWithExt = path.basename(uploadedFiles[0].fd);
            var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));
            var genThumUrl = path.dirname(uploadedFiles[0].fd) + '/thumb/' + uFileName + '.jpg';


            var files = {};
            files.name = uploadedFiles[0].filename;
            files.type = 'media';
            files.user_id = LoggedInUserid;
            files.user = LoggedInUserid;
            files.file_dir = file_dir;
            files.fileFD = uploadedFiles[0].fd;
            files.fileThumb = genThumUrl;
            files.size = uploadedFiles[0].size;

            if (req.param('album')) {
              files.album = req.param('album');
            }

            UserFiles.create(files, function fileCreated(err, createdFile) {
              fileMUrl = require('util').format('%s/users/files/%s', baseURL, createdFile.id);
              if (err) {
                req.session.flash = {
                  err: err
                }
                return res.redirect(res.locals.admin_url_prefix + '/media/album');
              } else {
                if (fileMUrl) {
                  var albumId;
                  if (req.param('album')) {
                    albumId = req.param('album');
                  } else {
                    albumId = '';
                  }
                  return res.json({id: createdFile.id, album: albumId});
                }
              }
            })
          }
        });
      }
    })


    var random_date = new Date();
    var randNum = random_date.getTime();


  },

  /**
   * Description
   * @return
   * @method uploadMediaAlbum
   * @param {} req
   * @param {} res
   * @param {} next
   * @return 
   */
  uploadMediaAlbum: function (req, res, next) {
    var LoggedInUserid = req.session.User.id;
    var random_date = new Date();
    var randNum = random_date.getTime();
    var thumbDir;

    req.file('file').upload({
      dirname: path.resolve(sails.config.appPath) + '/assets/uploads/files/' + LoggedInUserid + '/media',
      /**
       * Description
       * @return
       * @method saveAs
       * @param {} __newFileStream
       * @param {} cb
       * @return 
       */
      saveAs: function (__newFileStream, cb) {
        var fname = __newFileStream.filename;
        var mName = fname.substr(0, fname.lastIndexOf('.'));
        var fName = mName.replace(/\s\s+/g, ' ');
        fName = fName.replace(/ /g, "-");
        var ext = path.extname(__newFileStream.filename);
        cb(null, fName + "_" + randNum + ext);
      },
      // don't allow the total upload size to exceed ~10MB
      maxBytes: 10000000
    }, function whenDone(err, uploadedFiles) {


      if (err) {
        return res.negotiate(err);
      }

      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0) {
        return res.badRequest('No file was uploaded');
      } else {
        var baseURL = Utility.getSiteBaseURL(req);
        var file_dir = '/uploads/files/' + LoggedInUserid + '/media/' + path.basename(uploadedFiles[0].fd);

        thumbDir = 'assets/uploads/files/' + LoggedInUserid + '/media/thumb';
        if (!fs.existsSync(thumbDir)) {
          fs.mkdirSync(thumbDir);
        }

        Jimp.read(uploadedFiles[0].fd, function (err, image) {
          if (err) throw err;
          var extraLen;
          var img_w = parseInt(image.bitmap.width);
          var img_h = parseInt(image.bitmap.height);

          var fileNameWithExt = path.basename(uploadedFiles[0].fd);
          var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));

          if (img_w > img_h) {
            extraLen = (img_w - img_h) / 2;
            image.crop(Math.floor(extraLen), 0, img_h, img_h);
          } else {
            extraLen = (img_h - img_w) / 2;
            image.crop(0, Math.floor(extraLen), img_w, img_w);
          }

          image.resize(220, Jimp.AUTO)
            .quality(60)
            .write('assets/uploads/files/' + LoggedInUserid + '/media/thumb/' + uFileName + '.jpg'); // save
        }).catch(function (err) {
          console.log(err);
        });


        var fileNameWithExt = path.basename(uploadedFiles[0].fd);
        var uFileName = fileNameWithExt.substr(0, fileNameWithExt.lastIndexOf('.'));
        var genThumUrl = path.dirname(uploadedFiles[0].fd) + '/thumb/' + uFileName + '.jpg';


        var files = {};
        files.name = uploadedFiles[0].filename;
        files.type = 'media';
        files.user_id = LoggedInUserid;
        files.user = LoggedInUserid;
        files.file_dir = file_dir;
        files.fileFD = uploadedFiles[0].fd;
        files.fileThumb = genThumUrl;
        files.size = uploadedFiles[0].size;


        if (req.param('id') && req.param('id').length > 3) {
          files.album = [req.param('id')];
        }

        UserFiles.create(files, function fileCreated(err, file) {
          fileMUrl = require('util').format('%s/users/files/%s', baseURL, file.id);
          if (err) {
            req.session.flash = {
              err: err
            }
            return res.redirect(res.locals.admin_url_prefix + '/media/album');
          } else {
            return res.json(fileMUrl);
          }
        })

      }


    });
  },

  /**
   * Description
   * @return
   * @method upload
   * @param {} req
   * @param {} res
   * @return 
   */
  upload: function (req, res) {
    var title = "Upload photo or video";
    res.view({
      data: title, 'layout': 'admin/layout-media'
    })
  }


};

/**
 * Description
 * @method getLocationIfFound
 * @param {} image
 * @param {} callback
 * @return 
 */
function getLocationIfFound( image, callback ){
  ExifImage.ExifImage({ image: image }, function (err, exifData){
    if( exifData && exifData.gps && exifData.gps.GPSLatitude ){

      var lat = exifData.gps.GPSLatitude;
      var lon = exifData.gps.GPSLongitude;
      var latRef = exifData.gps.GPSLatitudeRef || "N";
      var lonRef = exifData.gps.GPSLongitudeRef || "W";

      lat = (lat[0] + lat[1]/60 + lat[2]/3600) * (latRef == "N" ? 1 : -1);
      lon = (lon[0] + lon[1]/60 + lon[2]/3600) * (lonRef == "W" ? -1 : 1);



      var options = {
        provider: 'google',

        // Optional depending on the providers
        httpAdapter: 'https', // Default
        apiKey: 'AIzaSyATOdXYksI4va9Xc9S7aN7S3o_CVDo1W58', // for Mapquest, OpenCage, Google Premier
        formatter: null         // 'gpx', 'string', ...
      };

      var geocoder = NodeGeocoder(options);

      geocoder.reverse({lat:lat, lon:lon}, function(err, res) {
        callback(res[0]);
      });
    }else{
      callback('');
    }
  });
}

