/**
* Conversations.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    created_by: {
      model: "User",
      required: true
    },
    title: {
      type: "string",
      required: true
    },
    last_message: {
      type: "string"
    },
    last_message_time: {
      type: "datetime"
    },
    last_message_by: {
      model: "User"
    },
    users: {
      collection: "User",
      via: "conversations"
    },
    unread: {
      collection: "ConversationMessageSeen",
      via: "conversation"
    },
    
    online: {
      type: 'boolean',
      defaultsTo: true
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
    }
  }

};

