/**
 * PollAnswer.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {
    name: {
      type: "string",
      required: true
    },
    poll_id: {
      type: "string",
      required: true
    },
    poll: {
      model: "Poll",
      required: true
    },
    vote: {
      type: "integer",
      defaultsTo: 0
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }
};

