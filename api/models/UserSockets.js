/**
* UserSockets.js
*
* @description :: TODO: When a user signed in his/her socket id is going to be save here pairing with his session user id.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    user_id: {
      type: "string",
      required: true
    },
    socket_id: {
      type: "string",
      required: true
    },
    online: {
      type: 'boolean',
      defaultsTo: true
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
    }
  }

};

