/**
* MailAttachment.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    name: {
      type: "string",
      required: true
    },
    originalName: {
      type: "string"
    },
    size: {
      type: "string"
    },
    url: {
      type: "string"
    },
    type: {
      type: "string"
    },
    mail: {
      model: "MailBody",
      required: true
    },
    mail_id: {
      type: "string",
      required: true
    },
    user: {
      model: "User",
      required: true
    },
    user_id: { 
      type: "string",
      required: true
    },
  
    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
    }
  }
};

