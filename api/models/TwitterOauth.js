module.exports = {

  schema: true,

  attributes: {
    user_id: {
      type: 'string'
    },
    accessToken: {
      type: 'string'
    },
    accessTokenSecret: {
      type: 'string'
    },
    screenName: {
      type: 'string'
    },
    userName: {
      type: 'string'
    }
  }
};
