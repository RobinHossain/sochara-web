/**
* MailLabel.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    user: {
      model: "User",
      required: false
    },
    user_id: { 
      type: "string",
      required: true
    },
    name: {
      type: "string",
      required: true
    },
    slug: {
      type: "string",
      required: true
    },
    domain: {
      type: "string",
      required: true,
      defaultsTo: "sochara"
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
    }
  }
};
