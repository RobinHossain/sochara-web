/**
 * Chat.js
 *
 * @description :: Conversation: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {
    first_name: {
      type: "string"
    },
    last_name: {
      type: "string"
    },
    email: {
      type: "string"
    },
    module: {
      type: "string"
    },
    phone: {
      type: "string"
    },
    user: {
      model: "User"
    },


    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }

};

