/**
* MailBody.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    sender: {
      model: "User",
      required: true
    },
    sender_id: { 
      type: "string",
      required: true
    },
    subject: {
      type: "string"
    },
    
    has_attachment: {
      type: "string"
    },
    
    body: {
      type: "text"
    },
    domain: {
      type: "string",
      enum: ['sochara', 'gmail'],
      defaultsTo: "sochara"
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
    }
  }
};

