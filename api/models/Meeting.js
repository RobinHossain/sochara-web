/**
* Meeting.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


module.exports = {

  schema: true,

  attributes: {
    user: {
      model: "User"
    },
    user_id: {
      type: "string"
    },
    color: {
      model: "Color"
    },
    color_id: {
      type: "string"
    },
    title: {
      type: "string",
      required: true
    },
    slug: {
      type: "string"
    },
    content: {
      type: "string"
    },
    start: {
      type: "datetime"
    },
    start_year: {
      type: "integer"
    },
    start_month: {
      type: "integer"
    },
    start_day: {
      type: "integer"
    },
    start_hour: {
      type: "integer"
    },
    start_min: {
      type: "integer"
    },
    start_sec: {
      type: "integer"
    },
    end: {
      type: "datetime"
    },
    end_year: {
      type: "integer"
    },
    end_month: {
      type: "integer"
    },
    end_day: {
      type: "integer"
    },
    end_hour: {
      type: "integer"
    },
    end_min: {
      type: "integer"
    },
    end_sec: {
      type: "integer"
    },
    repeat: {
      type: "string",
      defaultsTo: "no"
    },
    location: {
      type: "string"
    },
    contact: {
      type: "string"
    },
    topics: {
      type: "string"
    },
    website: {
      type: "string"
    },
    invitation_email: {
      type: "integer",
      defaultsTo: 1
    },
    participants: {
      collection: "User",
      via: "meetings"
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
    }
  },

  // Lifecycle Callbacks
  /**
   * Description
   * @method beforeCreate
   * @param {} values
   * @param {} cb
   * @return
   */
  beforeCreate: function (values, cb) {
      values.slug = Utility.title_to_slug(values.title);
      values.color = values.color_id;
      values.start_year = momentJS(values.start).format("YYYY");
      values.start_month = momentJS(values.start).format("MM");
      values.start_day = momentJS(values.start).format("DD");
      values.start_hour = momentJS(values.start).format("HH");
      values.start_min = momentJS(values.start).format("mm");
      values.start_sec = momentJS(values.start).format("ss");

      values.end_year = momentJS(values.end).format("YYYY");
      values.end_month = momentJS(values.end).format("MM");
      values.end_day = momentJS(values.end).format("DD");
      values.end_hour = momentJS(values.end).format("HH");
      values.end_min = momentJS(values.end).format("mm");
      values.end_sec = momentJS(values.end).format("ss");
      cb();
  },
  /**
   * Description
   * @method beforeUpdate
   * @param {} values
   * @param {} cb
   * @return
   */
  beforeUpdate: function (values, cb) {
      values.slug = Utility.title_to_slug(values.title);
      values.color = values.color_id;
      values.start_year = momentJS(values.start).format("YYYY");
      values.start_month = momentJS(values.start).format("MM");
      values.start_day = momentJS(values.start).format("DD");
      values.start_hour = momentJS(values.start).format("HH");
      values.start_min = momentJS(values.start).format("mm");
      values.start_sec = momentJS(values.start).format("ss");

      values.end_year = momentJS(values.end).format("YYYY");
      values.end_month = momentJS(values.end).format("MM");
      values.end_day = momentJS(values.end).format("DD");
      values.end_hour = momentJS(values.end).format("HH");
      values.end_min = momentJS(values.end).format("mm");
      values.end_sec = momentJS(values.end).format("ss");
      cb();
  }
};
