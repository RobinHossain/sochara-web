/**
 * @name Email Model
 * @description - Define you model attributes and validation here for Mail Data.
 */

module.exports = {

  schema: true,

  attributes: {

    from: {
      type: "json"
    },

    mail_id: {
      type: "string"
    },

    to: {
      type: "json"
    },

    message: {
      type: "string"
    },

    subject: {
      type: "string"
    },

    time: {
      type: "string"
    },

    date: {
      type: "datetime"
    },

    hasAttachments: {
      type: "boolean"
    },

    read: {
      type: "boolean"
    },

    starred: {
      type: "boolean"
    },

    important: {
      type: "boolean"
    },

    labels: {
      type: "array"
    },

    address: {
      type: "string"
    },

    provider: {
      type: "string"
    },

    email_id: {
      type: "string"
    },

    user: {
      model: "User"
    },

    /**
     * @example toJSON - To make Json.
     * @returns {Object} - Return Object.
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }

  },

  autoPK:false

};
