/**
* UserRole.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    name: {
      type: "string",
      required: true
    },
    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
    }
  },

  /**
   * Description
   * @method beforeCreate
   * @param {} values
   * @param {} next
   * @return
   */
  beforeCreate: function(values, next) {
      if (!values.name ) {
        return next({err: ["Please enter a valid user group name."]});
      }
      next();
  }
};

