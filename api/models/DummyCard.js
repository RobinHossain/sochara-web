/**
 * DummyCard.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {
    eventTitle: {
      type: "string"
    },
    additionalTextForEvent: {
      type: "string"
    },
    eventDate: {
      type: "date"
    },
    eventStartTime: {
      type: "datetime"
    },
    eventEndTime: {
      type: "datetime"
    },
    locationName: {
      type: "string"
    },
    city: {
      type: "string"
    },
    background: {
      model: "UserFiles"
    },
    state: {
      type: "string"
    },
    zip: {
      type: "string"
    },
    cornerRadius: {
      type: "string"
    },
    styles: {
      type: "json"
    },
    layerPosition: {
      type: "json"
    },
    file: {
      model: "UserFiles"
    },
    gradient: {
      type: "boolean"
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  },

};

