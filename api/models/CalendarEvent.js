/**
 * CalendarEvent.js
 *
 * @description :: this is Calendar modules Event collection. It is the main collection and it interacts with other models
 * using relationships, e.g.- Users, Signups
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {
    title: {
      type: 'string',
      required: true
    },
    place: {
      type: 'string'
    },
    description: {
      type: 'string'
    },
    organizer: {
      model: 'User'
    },
    status: {
      type: 'string'
    },
    email: {
      type: 'string'
    },
    mail_id: {
      type: 'string'
    },
    event_id: {
      type: 'string'
    },
    user: {
      model: 'User'
    },
    provider: {
      type: 'string'
    },
    htmlLink: {
      type: 'string'
    },
    isRepeated: {
      type: 'boolean',
      defaultsTo:false
    },
    repeat:{
      type:'json'
    },
    isSignupRepeatWithEvent:{
      type:'boolean'
    },
    isSignupRepeat:{
      type:'boolean'
    },
    signupRepeat:{
      type:'json'
    },
    originalStartTime:{
      type: 'datetime'
    },
    isSignupChecked:{
      type:'boolean',
      defaultsTo:true
    },
    isReminder:{
      type:'boolean',
      defaultsTo:false
    },
    isSignupReminder:{
      type:'boolean',
      defaultsTo:false
    },
    parent: {
      model: 'CalendarEvent'
    },
    stick:{
      type:'boolean',
      defaultsTo:true
    },
    reminder: {
      type: 'json'
    },
    signupReminder: {
      type: 'json',
      defaultsTo:[{type:'',duration:'',yes:''}]
    },
    start:{
      type:'datetime'
    },
    type:{
      type:'string',
      defaultsTo:'event'
    },
    end:{
      type:'datetime'
    },
    timeZone: {
      type:'string'
    },
    allDay:{
      type:'boolean'
    },
    isNAndP:{
      type:'boolean'
    },
    color: {
      type: 'json',
      defaultsTo: {eventColor: 'c6', textColor: 'c6'}
    },
    notifcationStatus: {
      type: 'string',
      defaultsTo: '0'
    },
    showMeAs:{
      type:'string',
      defaultsTo:'available'
    },
    availability:{
      type:'json'
    },
    comments:{
      type:'json',
      defaultsTo: []
    },
    creator:{
      model: 'User'
    },
    attendees:{
      type: 'json',
      defaultsTo: [{id: '', role: '', acceptance: '', name: '', email: '', photo_url: '', color: '', comment: ''}]
    },
    signup:{
      type:'json'
    },
    isNotify:{
      type:'boolean',
      defaultsTo:false
    },
    nAndP:{
      type:'json',
      defaultsTo:{newEventNotifyEdit:true,newEventNotifySwap:true,newEventPermissionCoEdit:true,newEventPermissionUserSwap:true,
        newEventPermissionLimitSignup:true,newEventPermissionRequiredInfo:true}
    },
    signupVolunteer:{
      collection:'signupVolunteer',
      via:'eventId'
    },
    eventSwapRequest:{
      collection:'eventSwap',
      via:'eventId'
    },
   /* beforeDestroy: function(criteria, cb) {
      // Destroy any user associated to a deleted pet
      Pet.find(criteria).populate('owners').exec(function (err, pets){
        if (err) return cb(err);
        pets.forEach(function(recordToDestroy) {
          User.destroy({id: _.pluck(recordToDestroy.owners, 'id')}).exec(function(err) {
            console.log('The users associated to the pet ' + recordToDestroy.name + ' have been deleted');
          });
        });
        cb();
      })
    },*/

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  },

};

