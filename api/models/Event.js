module.exports = {

  schema: true,

  attributes: {
    title: {
      type: "string",
      required: true
    },
    place: {
      type: "string",
      required: true
    },
    start: {
      type: "json",
      required: true
    },
    end: {
      type: "json",
      required: true
    },
    allday: {
      type: "string"
    },
    repeat:{
      type:'json'
    },
    reminder:{
      type:'integer'
    },
    private: {
      type: "string"
    },
    showas: {
      type: "string"
    },
    eventcolor: {
      type: "string"
    },
    eventtextcolor: {
      type: 'string'
    },
    description: {
      type: "string"
    },
    people: {
      type: "json"
    },
    creator: {
      type: "string",
      required: true
    },
    eventNotification:{
      type:'string'
    }
  }
};
