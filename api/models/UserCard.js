/**
 * UserCard.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  schema: true,

  attributes: {
    user: {
      model: "User",
      required: true
    },
    user_id: { 
      type: "string",
      required: true
    },
    category: {
      model: "CardCategory",
      required: true
    },
    category_id: { 
      type: "string",
      required: true
    },
    layout: {   //card
      model: "Card",
      required: true
    },
    layout_id: { //card_id
      type: "string",
      required: true
    },
    width: { 
      type: "string",
      required: true
    },
    height: { 
      type: "string",
      required: true
    },
    title: {
      type: "string"
    },
    imgDataUrl: {
      type: "text"
    },
    canvasDataHtml: {
      type: "text"
    },
    cardType: {
      type: "string"
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
    }
  },
    
  /**
   * Description
   * @method beforeValidate
   * @param {} values
   * @param {} next
   * @return 
   */
  beforeValidate: function(values, next) {
    if ( values.category_id && values.category_id != '' ) {
      values.category = values.category_id;
    }

    next();
  }
};

