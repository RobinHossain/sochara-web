/**
 * SubGroups.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    schema: true,

    attributes: {
        
        user_id: {
            type: "string",
            required: true
        },

        subgroup_name: {
            type: "string",
            required: true
        },
        parent_id:{
            type:"string",
            required:true
        },
        contacts:{
            type:"array",
            required:false
        },


        /**
         * Description
         * @method toJSON
         * @return obj
         */
        toJSON: function () {
            var obj = this.toObject();
            delete obj._csrf;
            return obj;
        }
    }
};

