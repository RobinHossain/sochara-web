/**
 * ChatMessage.js
 *
 * @description :: ChatMessage: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,
  attributes: {
    sender: {
      model: "User",
      required: true
    },
    conversation: {
      model: "Conversations",
      required: true
    },
    message_type: {
      type: "string",
      defaultsTo: "chat"
    },
    body: {
      type: "string"
    },
    attachment: {
      type: "json"
    },
    users: {
      type: "array"
    },


    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }


  }
};

