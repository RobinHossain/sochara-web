/**
* Friend.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {

    friend1: {
      model: "User",
      required: true
    },
    friend2: {
      model: "User",
      required: true
    },
    status: {
      type: "int",
      defaultsTo: "0"
    },
    request_seen: {
      type: "boolean",
      defaultsTo: false
    },
    accept_seen: {
      type: "boolean",
      defaultsTo: false
    }
  }
};

