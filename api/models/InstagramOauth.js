module.exports = {

  schema: true,

  attributes: {
    user_id: {
      type: 'string'
    },
    accessToken: {
      type: 'string'
    },
    profilePicture: {
      type: 'string'
    },
    userName: {
      type: 'string'
    }
  }
};
