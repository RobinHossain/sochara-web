module.exports = {

  schema: true,

  attributes: {
    packageName:{
      type:'string',
      unique:true,
      required:true
    },
    diskSpace:{
      type:'string',
      // unique:true,
      required:true
    },
    adds:{
      type:'string',
      // unique:true,
      required:true
    },
    basicPackage:{
      type:'string',
      defaultsTo:'false'
    }
  }
};
