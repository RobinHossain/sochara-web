/**
 * DraftPoll.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    schema: true,

    attributes: {

        question: {
            type: "string",
            required: true
        },
        
        answer: {
            type: "array",
            required: true
        },

        user: {
            model: "User"
        },

        user_id: {
            type: "string"
        },

        shared_poll: {
            type: "array"
        },
        
        polltype: {
            type: "string",
            required: false
        },

        styles: {
            type: "json",
            required: false
        },
        
        slug: {
            type: "string",
            required: false
        },

        ip_restriction: {
            type: "string",
            required: false
        },

        show_vote_result:{
            type: "string",
            required: false
        },
        
        expire_on: {
            type: "datetime"
        },
        
        

        /**
         * Description
         * @method toJSON
         * @return obj
         */
        toJSON: function () {
            var obj = this.toObject();
            delete obj._csrf;
            return obj;
        }
    }
};

