/**
 * SharedPoll.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    schema: true,

    attributes: {

        poll: {
            model: "Poll",
            required: true
        },

        poll_id: {
            type: "string",
            required: true
        },

        user: {
            model: "User",
            required: true
        },

        user_id: {
            type: "string",
            required: true
        },

        access_type: {
            type: "string"
        },
        
        
        

        /**
         * Description
         * @method toJSON
         * @return obj
         */
        toJSON: function () {
            var obj = this.toObject();
            delete obj._csrf;
            return obj;
        }
    }
};

