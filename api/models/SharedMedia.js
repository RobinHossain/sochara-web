/**
 * SharedPoll.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {

    owner: {
      model: "User",
      required: true
    },

    access_type: {
      type: "string"
    },

    media: {
      model: "UserFiles",
    },

    users: {
      type: "array"
    },

    album: {
      model: "MediaAlbum",
    },


    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }
};

