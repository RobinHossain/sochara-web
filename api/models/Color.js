/**
* Color.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    user: {
      model: "User",
      required: true
    },
    user_id: { 
      type: "string",
      required: true
    },
    title: {
      type: "string",
      required: true
    },
    slug: {
      type: "string"
    },
    code: {
      type: "string"
    },
    
    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
    }
  },
    
  // Lifecycle Callbacks
  /**
   * Description
   * @method beforeCreate
   * @param {} values
   * @param {} cb
   * @return 
   */
  beforeCreate: function (values, cb) {
      values.slug = Utility.title_to_slug(values.title);
      cb();
  },
  /**
   * Description
   * @method beforeUpdate
   * @param {} values
   * @param {} cb
   * @return 
   */
  beforeUpdate: function (values, cb) {
      values.slug = Utility.title_to_slug(values.title);
      cb();
  }
};

