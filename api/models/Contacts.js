/**
 * ContactGroup.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
  schema: true,

  attributes: {
    user_id: {
      model: 'User'
    },
    contacts_table_id: {
      type: "string"
    },
    photo_url: {
      type: "string"
    },
    first_name: {
      type: "string"
    },
    middle_name: {
      type: "string"
    },
    last_name: {
      type: "string"
    },
    address: {
      type: "json"
    },
    date_birth: {
      type: "string"
    },
    date_anniversary: {
      type: "string"
    },
    social: {
      type: "json"
    },
    url: {
      type: "json"
    },
    im: {
      type: "json"
    },
    relatives: {
      type: "json"
    },
    job: {
      type: "json"
    },
    phone: {
      type: "json"
    },
    email: {
      type: 'string',
      unique: true
    },
    favorite: {
      type: "boolean",
      defaultsTo: false
    },
    emails: {
      type: "json"
    },
    notes: {
      type: "string",
      defaultsTo: '',
    },
    status: {
      type: "string",
      defaultsTo: 'approve',
    },
    sochara_user_id: {
      model: 'User'
    },
    from_first_name: {
      type: "string"
    },
    from_last_name: {
      type: "string"
    },
    from_email: {
      type: "string"
    },
    is_sochara_user: {
      type: "boolean",
      defaultsTo: false
    },
    request_sent_by: {
      type: "string"
    },
  }
};

