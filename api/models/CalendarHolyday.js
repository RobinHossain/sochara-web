/**
 * CalendarEvent.js
 *
 * @description :: this is Calendar modules Event collection. It is the main collection and it interacts with other models
 * using relationships, e.g.- Users, Signups
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {
    title: {
      type: 'string',
      required: true
    },
    countryState: {
      type: 'string'
    },
    summary: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    event_id: {
      type: 'string'
    },
    provider: {
      type: 'string'
    },
    htmlLink: {
      type: 'string'
    },
    start:{
      type:'datetime'
    },
    type:{
      type:'string',
      defaultsTo:'event'
    },
    stick:{
      type:'boolean',
      defaultsTo:true
    },
    end:{
      type:'datetime'
    },
    allDay:{
      type:'boolean'
    },
    color: {
      type: 'json',
      defaultsTo: {eventColor: 'c6', textColor: 'c6'}
    },
   /* beforeDestroy: function(criteria, cb) {
      // Destroy any user associated to a deleted pet
      Pet.find(criteria).populate('owners').exec(function (err, pets){
        if (err) return cb(err);
        pets.forEach(function(recordToDestroy) {
          User.destroy({id: _.pluck(recordToDestroy.owners, 'id')}).exec(function(err) {
            console.log('The users associated to the pet ' + recordToDestroy.name + ' have been deleted');
          });
        });
        cb();
      })
    },*/

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  },

};

