/**
 * CalendarEvent.js
 *
 * @description :: this is Calendar modules Event collection. It is the main collection and it interacts with other models
 * using relationships, e.g.- Users, Signups
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {
    eventId: {
      model:'CalendarEvent'
    },
    eventScope:{
      type:'string',
      defaultsTo:'daySpecific'
    },
    eventScopeDate:{
      type:'string',
      defaultsTo:''
    },
    contribution: {
      type: 'json',
      defaultsTo: [{job: '', bringing: ''}]
    },
    userId: {
      type: 'string',
      defaultsTo: ''
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  },

};

