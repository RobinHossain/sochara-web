/**
* ConversationMessageSeen.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {

    user: {
      model: "User",
      required: true
    },
    conversation: {
      model: "Conversations",
      via: "unread"
    },
    message: {
      model: "Message",
      required: true
    },
    status: {
      type: "string",
      defaultsTo: "no"
    }
  }
};

