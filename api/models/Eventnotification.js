/**
 * Created by ASUS on 15-Feb-17.
 */
module.exports = {

  schema: true,

  attributes: {
    socketId:{
      type:'string'
    },
    userId:{
      type: 'string'
    },
    functionCalled:{
      type:'boolean'
    },
    userInteracted:{
      type:'boolean'
    }
  }
};
