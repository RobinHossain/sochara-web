/**
 * Chat.js
 *
 * @description :: Conversation: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {
    created_by: {
      model: "User",
      required: true
    },
    last_message: {
      type: "string"
    },
    last_message_time: {
      type: "datetime"
    },
    last_message_by: {
      model: "User"
    },
    with: {
      model: "User"
    },
    recipients: {
      collection: "User",
      via: "conversations"
    },
    users: {
      type: "array"
    },
    name: {
      type: "string",
    },
    group: {
      type: "boolean",
      defaultsTo: false
    },
    unread: {
      type: "json"
    },
    mute: {
      type: "array"
    },
    pending: {
      type: "boolean"
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }

};

