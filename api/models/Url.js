/**
* Color.js
*
* @description :: URL: The Model for use callback url entry
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    user: {
      model: "User"
    },
    title: {
      type: "string"
    },
    module: {
      type: "string"
    },
    type: {
      type: "string"
    },
    url: {
      type: "string"
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
    }
  },

  // Lifecycle Callbacks
  /**
   * Description
   * @method beforeCreate
   * @param {} values
   * @param {} cb
   * @return
   */
  beforeCreate: function (values, cb) {
      values.slug = Utility.title_to_slug(values.title);
      cb();
  },
  /**
   * Description
   * @method beforeUpdate
   * @param {} values
   * @param {} cb
   * @return
   */
  beforeUpdate: function (values, cb) {
      values.slug = Utility.title_to_slug(values.title);
      cb();
  }
};

