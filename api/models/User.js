/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */
var bcrypt = require('bcrypt');

module.exports = {

  schema: true,

  attributes: {
    user_group: {
      model: "UserGroup",
      required: true
    },
    username: {
      type: "string",
      unique: true
    },
    password: {
      type: "string",
    },
    encryptedPassword: {
      type: 'string',
      required: true
    },
    photo_url: {
      type: "string",
      defaultsTo:''
    },
    full_name: {
      type: "string",
      defaultsTo:''

    },
    first_name: {
      type: "string",
      required: true,

    },
    middle_name: {
      type: "string",
      defaultsTo:''

    },
    last_name: {
      type: "string",
      defaultsTo:''

    },
    email: {
      type: "string",
      required: true,
      unique: true
    },
    emails: {
      type: "json",
      defaultsTo: [{type: 'Home', address: '', visibility: ''}, {type: 'Office', address: '', visibility: ''}, {type: 'Other', address: '', visibility: ''}]
    },
    phone: {
      type: "json",
      defaultsTo: [{type: 'Home', number: '', visibility: ''}, {type: 'Office', number: '', visibility: ''}, {type: 'Other', number: '', visibility: ''}]
    },
    address: {
      type: "json",
      defaultsTo: [{type: 'Home', address: {street: '', city: '', state: '', zip: '', country: ''}, visibility: ''}, {
        type: 'Office',
        address: {street: '', city: '', state: '', zip: '', country: ''}, visibility: ''
      }],
    },
    url_home: {
      type: "string"
    },
    date_birth: {
      type: "string",
      defaultsTo:''

    },
    date_anniversary: {
      type: "string",
      defaultsTo:''

    },
    social: {
      type: "json",
      defaultsTo: [{type: '', address: '', visibility: ''}]
    },
    im: {
      type: "json",
      defaultsTo: [{type: '', address: '', visibility: ''}]
    },
    // relatives: {
    //   type: "json",
    //   defaultsTo: [{type: 'father', number: ''},
    //     {type: 'mother', number: ''},
    //     {type: 'parents', number: ''},
    //     {type: 'brother', number: ''},
    //     {type: 'sister', number: ''},
    //     {type: 'son', number: ''},
    //     {type: 'daughter', number: ''},
    //     {type: 'husband', number: ''},
    //     {type: 'wife', number: ''},
    //     {type: 'spouse', number: ''},
    //     {type: 'partner', number: ''},
    //     {type: 'pa', number: ''},
    //     {type: 'manager', number: ''},
    //   ]
    // },
    custom_other: {
      type: "string",
    },
    job: {
      type: "json",
      defaultsTo: [{employee: '', company: '', visibility: ''}]
    },
    notes: {
      type: "string",
    },
    profGenSetAddItemAmount: {
      type: "json",
      defaultsTo: [{label: '', value: ''}]
    },
    profcontactSetAddItemAmount: {
      type: "json",
      defaultsTo: [{label: '', value: ''}]
    },
    profWorkSetAddItemAmount: {
      type: "json",
      defaultsTo: [{label: '', value: ''}]
    },
    profrelativeSetAddItemAmount: {
      type: "json",
      defaultsTo: [{label: '', value: ''}]
    },
    relativeFromContact: {
      type: "json",
      defaultsTo: {}
    },

    bannerImage: {
      type: "string",
    },
    privacyAttribute: {
      type: "json",
      defaultsTo: {friends: [], onlyMe: []},
    },
    status: {
      type: 'string'
    },
    mode: {
      type: 'string',
      defaultsTo:'offline'
    },
    conversations: {
      collection: "Chat",
      via: "recipients"
    },
    // calendarAttendee: {
    //   collection: "CalendarEvent",
    //   via: "attendees"
    // },
    cards: {
      collection: "Card",
      via: "users"
    },
    polls: {
      collection: "Poll",
      via: "users"
    },
    meetings: {
      collection: "Meeting",
      via: "participants"
    },
    current_active_socket_id: {
      type: "string"
    },
    gravatarUrl: {
      type: "string"
    },
    avatarFd: {
      type: "string"
    },
    access_token: {
      type: "text"
    },
    refresh_token: {
      type: "text"
    },
    id_token: {
      type: "text"
    },
    token_type: {
      type: "string"
    },
    package: {
      type: 'string',
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj.encryptedPassword;
      delete obj.password;
      delete obj.confirmation;
      // delete obj._csrf;
      return obj;
    }
  },

  beforeCreate: function (values, next) {
    /*bcrypt.genSalt(10, function(err, salt) {
      if (err) return next(err);

      bcrypt.hash(values.password, salt, function(err, hash) {
        if (err) return next(err);

        values.encryptedPassword = hash;
      });
    });*/
    //
    // if (!values.user_group || values.user_group == 'Select User Group') {
    //   return next({err: ["Please select a user group for this user account."]});
    // }
    // if (!values.password || values.password != values.confirmation) {
    //   return next({err: ["Password doesn't match password confirmation."]});
    // }

    next();

  },

  validPassword: function (password, user, cb) {
    bcrypt.compare(password, user.encryptedPassword, function (err, match) {
      if (err) cb(err);

      if (match) {
        cb(null, true);
      } else {
        cb(err);
      }
    });
  },
  hashPassword: function (password, cb) {

    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        cb({err: err});
      }
      bcrypt.hash(password, salt, function (err, hash) {
        if (err) {
          cb({err: err});
        }
        cb({hash: hash})
      });
    });
  }
};

