/**
 * CardElements.js
 *
 * @description :: Card Elements - Where Describe card contents
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {
    user: {
      model: "User"
    },
    title: {
      type: "string"
    },
    element: {
      type: "string"
    },
    size: {
      type: "string"
    },
    type: {
      type: "string"
    },
    fileType: {
      type: "string"
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }
};

