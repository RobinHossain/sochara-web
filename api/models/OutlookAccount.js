/**
 * OutlookAccount.js
 *
 * @description :: All user's gmail auth info will store via this model.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    schema: true,

    attributes: {

      email: {
        type: "string",
        required: true
      },

      name: {
        type: "string"
      },

      provider: {
        type: "string",
        required: true
      },

      access_token: {
        type: "text"
      },

      id_token: {
        type: "text"
      },


      refresh_token: {
        type: "text"
      },

      token_type: {
        type: "string"
      },

      scope: {
        type: "string"
      },

      expires_at: {
        type: "string"
      },

      user_id: {
        type: "string",
        required: true
      },

      user: {
        model: "User"
      },

      /**
       * Description
       * @method toJSON
       * @return obj
       */
      toJSON: function () {
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
      }

    }

};
