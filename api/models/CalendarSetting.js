/**
 * CalendarSetting.js
 *
 * @description :: CalendarSetting: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {
    showEvents: {
      type: "boolean",
      defaultsTo: true
    },
    showHolidays: {
      type: "boolean",
      defaultsTo: true
    },
    showApiEvents: {
      type: "boolean",
      defaultsTo: false
    },
    showBirthdays: {
      type: "boolean",
      defaultsTo: true
    },
    showEventsFrom: {
      type: "array"
    },
    user: {
      model: "User",
      required: true
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }

};

