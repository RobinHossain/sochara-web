/**
 * CardCategory.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {
    title: {
      type: "string",
      required: true
    },
    slug: {
      type: "string",
      required: true,
      unique: true
    },
    content: {
      type: "string"
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  },

  /**
   * Description
   * @method beforeValidate
   * @param {} values
   * @param {} next
   * @return
   */
  beforeValidate: function (values, next) {
    if (!values.title) {
      return next({err: ["Please enter a valid settings name for your card category."]});
    }

    if (!values.slug) {
      values.slug = Utility.title_to_slug(values.title);
    }


    if (!values.slug) {
      return next({err: ["Please enter a valid slug for category " + values.title + "."]});
    }
    next();
  }
};

