/**
* MessageMedia.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    name: {
      type: "string",
      required: true
    },
    originalName: {
      type: "string"
    },
    size: {
      type: "string"
    },
    url: {
      type: "string"
    },
    type: {
      type: "string"
    },
    message: {
      model: "Message",
      required: true
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
    }
  }
};

