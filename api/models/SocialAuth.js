/**
 * Created by ASUS on 09-May-17.
 */
module.exports = {

  schema: true,

  // attributes: {
  //   userId: {
  //     type: 'string'
  //   },
  //   socialAccount:{
  //     type:'string'
  //   },
  //   accountName:{
  //     type:'string'
  //   },
  //   socialUserId:{
  //     type:'string'
  //   },
  //   accessArea:{
  //     type:'json'
  //   },
  //   accessToken:{
  //     type:'string'
  //   },
  //   action:{
  //     type:'string'
  //   }
  // },

  attributes: {
    user: {
      model: 'User'
    },
    user_id: {
      model: 'User'
    },
    userId: {
      model: 'User'
    },
    provider:{
      type:'string'
    },
    album_id:{
      type:'string'
    },
    accountName:{
      type:'string'
    },
    providerId:{
      type:'string'
    },
    profilePicture: {
      type: 'string'
    },
    accessArea:{
      type:'json'
    },
    accessToken:{
      type:'string'
    },
    expires_in:{
      type:'string'
    },
    expire_date:{
      type:'datetime'
    },
    screenName: {
      type: 'string'
    },
    accessTokenSecret: {
      type: 'string'
    },
    userName: {
      type: 'string'
    },
    action:{
      type:'string'
    }
  }
};
