/**
 * Poll.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {
    user: {
      model: "User",
      required: true
    },
    styles: {
      type: "json"
    },
    slug: {
      type: "string"
    },
    gradient: {
      type: "boolean"
    },
    background: {
      model: "UserFiles"
    },
    file: {
      model: "UserFiles"
    },


    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }
};

