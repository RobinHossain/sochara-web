/**
 * Poll.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {
    question: {
      type: "string",
      required: true
    },
    answers: {
      type: "array",
      required: true
    },
    shared: {
      type: "array"
    },
    user: {
      model: "User",
      required: true
    },
    users: {
      collection: "User",
      via: "polls"
    },
    polltype: {
      type: "string"
    },
    styles: {
      type: "json"
    },
    slug: {
      type: "string"
    },
    poll_lebel: {
      type: "string"
    },
    ip_restriction: {
      type: "boolean"
    },
    gradient: {
      type: "boolean"
    },
    allow_vote_again: {
      type: "string"
    },
    show_vote_result: {
      type: "boolean"
    },
    show_vote_result_when_closed: {
      type: "boolean"
    },
    expire_on: {
      type: "datetime"
    },
    event_date: {
      type: "datetime"
    },
    tvote: {
      type: "integer"
    },
    draft: {
      type: "boolean"
    },
    background: {
      model: "UserFiles"
    },
    file: {
      model: "UserFiles"
    },


    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }
};

