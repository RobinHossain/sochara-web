/**
 * PollVote.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {

    answer_id: {
      type: "string",
      required: true
    },
    poll: {
      model: "Poll",
      required: true
    },
    user: {
      model: "User",
      required: false
    },
    ip: {
      type: "string"
    },
    user_agent: {
      type: "string"
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }
};

