/**
 * Poll.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {

    name: {
      type: "string"
    },

    file_dir: {
      type: "string"
    },

    fileFD: {
      type: "string"
    },

    fileThumb: {
      type: "string"
    },

    thumb_dir: {
      type: "string"
    },

    poster_dir: {
      type: "string"
    },

    metaData: {
      type: "json"
    },

    filePoster: {
      type: "string"
    },

    owner: {
      model: "User"
    },

    users: {
      type: "array"
    },

    album: {
      type: "array"
    },

    folder: {
      type: "array"
    },

    ext: {
      type: "string"
    },

    type: {
      type: "string"
    },

    starred: {
      type: "boolean"
    },

    file_type: {
      type: "string"
    },

    size: {
      type: "string"
    },

    type_id: {
      type: "string"
    },


    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }
};

