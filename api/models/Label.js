/**
 * Label.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {

    name: {
      type: "string",
      required: true
    },
    userInfo: {
      type: "json"
    },
    layers :{
      type: "json"
    },
    styles: {
      type: 'json'
    },
    addressType: {
      type: "string",
      defaultsTo: "return"
    },
    shippingAddress: {
      type: 'json'
    },
    user:{
      model: "User"
    },
    user_id: {
      type: "string"
    },
    html: {
      type: "string",
      // required: false
    },
    preview: {
      type: "string",
      // required: false
    },
    labelBGColor: {
      type: "string",
    },
    labelBGImage: {
      type: "string"
    },
    editorCSS: {
      type: "string"
    },
    labelRadius: {
      type: "string",
    },
    isInsertImage:{
      type:"boolean"
    },
    isInsertextText:{
      type:"boolean"
    },
    labelPreview:{
      type:"string"
    },
   /* insertedImageCSS: {
      type: 'string'
    },
    insertedTextCSS: {
      type: 'string'
    },*/
    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }
};

