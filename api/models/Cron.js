/**
 * @name Cron System Model - Where store all cron schedule based
 * @description - Defination of Cron Model
 */

module.exports = {

  schema: true,

  attributes: {

    name: {
      type: "string"
    },

    type: {
      type: "string"
    },

    module: {
      type: "string"
    },

    data: {
      type: "json"
    },

    status: {
      type: "json"
    },

    user: {
      type: "string"
    },

    /**
     * @example toJSON - To make Json.
     * @returns {Object} - Return Object.
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }

  }

};
