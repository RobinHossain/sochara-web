/**
 * Created by ASUS on 03-Feb-17.
 */
module.exports = {

  schema: true,

  attributes: {
    user: {
      required: true,
      model: 'User'
    },
    name: {
      type: 'string'
    },
    geo: {
      type: 'json',
      defaultsTo:{lat:'',lon:''}
    }
  }
};
