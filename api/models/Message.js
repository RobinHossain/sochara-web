/**
* Message.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    sender: {
      model: "User",
      required: true
    },
    message_type: {
      type: "string",
      defaultsTo: "chat"
    },
    type_content: {
      type: "string"
    },
    conversation: {
      model: "Conversations",
      required: true
    },
    subject: {
      type: "string",
      required: false
    },
    body: {
      type: "string"
    },
    attachment: {
      type: "json"
    },


    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
   

  }
};

