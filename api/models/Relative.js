/**
 * Chat.js
 *
 * @description :: Conversation: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {
    user: {
      model: "User",
      required: true
    },
    type: {
      type: "string"
    },
    relative: {
      model: "User",
      required: true
    },
    visibility: {
      type: 'string'
    },

    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }

};

