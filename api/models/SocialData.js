/**
 * Created by ASUS on 09-May-17.
 */
module.exports = {

  schema: true,

  attributes: {
    user: {
      model: 'User'
    },
    post: {
      type:'string'
    },
    story: {
      type:'string'
    },
    post_id: {
      type:'string'
    },
    favorite: {
      type:'boolean'
    },
    application: {
      type:'string'
    },
    type:{
      type:'string'
    },
    picture:{
      type:'string'
    },
    full_picture:{
      type:'string'
    },
    link: {
      type: 'string'
    },
    icon:{
      type: 'string'
    },
    date_sort: {
      type: 'string'
    },
    media: {
      type: 'string'
    },
    place: {
      type: 'string'
    },
    tags: {
      type: 'array'
    },
    user_name: {
      type: 'string'
    },
    provider: {
      type: 'string'
    },
    source: {
      type: 'string'
    },
    favorite_count: {
      type: 'integer'
    },
    comment_count: {
      type: 'integer'
    },
    like_count: {
      type: 'integer'
    },
    created: {
      type: 'datetime'
    },
    from:{
      type:'json'
    },
    attachment_details:{
      type:'json'
    },
    attachments:{
      type:'array'
    },

    toJSON: function () {
      var obj = this.toObject();
      delete obj.encryptedPassword;
      delete obj.password;
      delete obj.confirmation;
      // delete obj._csrf;
      return obj;
    }
  }
};
