/**
* MailToFolder.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    parent: {
      model: "MailToFolder",
      required: false
    },
    parent_id: {
      type: "string",
      required: false
    },
    sender: {
      model: "User",
      required: true
    },
    sender_id: { 
      type: "string",
      required: true
    },
    user: {
      model: "User",
      required: true
    },
    user_id: { 
      type: "string",
      required: true
    },
    mail: {
      model: "MailBody",
      required: true
    },
    mail_id: {
      type: "string",
      required: true
    },
    label: {
      model: "MailLabel"
    },
    label_id: {
      type: "string"
    },
    folder: {
      type: "string",
      enum: ["inbox", "sent", "draft", "archive", "trash", "spam", "custom"],
      required: true
    },
    msg_type: {
      type: "string",
      enum: ["from", "to", "cc", "bcc"],
      defaultsTo: "to"
    },
    flag: {
      type: "string",
      enum: ["read", "unread"],
      defaultsTo: "unread"
    },
    important: {
      type: "string",
      enum: ["yes", "no"],
      defaultsTo: "no"
    },
    starred: {
      type: "string",
      enum: ["yes", "no"],
      defaultsTo: "no"
    },
  
    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
    }
  }
};

