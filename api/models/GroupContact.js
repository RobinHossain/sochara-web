/**
 * Group Contact.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    schema: true,

    attributes: {

        user: {
            model: "User",
            required: true
        },

        user_id: {
            type: "string",
            required: true
        },
        
        group:{
            model: "ContactGroup",
            required: true
        },
        
        group_id: {
            type: "string",
            required: true
        },

        contact: {
            type: "array",
            required: true
        },


        /**
         * Description
         * @method toJSON
         * @return obj
         */
        toJSON: function () {
            var obj = this.toObject();
            delete obj._csrf;
            return obj;
        }
    }
};

