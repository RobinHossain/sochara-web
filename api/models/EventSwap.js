/**
 * CalendarEvent.js
 *
 * @description :: this is Calendar modules Event collection. It is the main collection and it interacts with other models
 * using relationships, e.g.- Users, Signups
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true,

  attributes: {
    eventId: {
      model: 'CalendarEvent'
    },
    eventScopeDate: {
      type: 'string'
    },
    swapRequestFrom: {
      type: 'json',
      defaultsTo: {name: '', userId: '', photo_url: '', email: ''}
    },
    swapRequestToDate: {
      type: 'string'
    },
    swapRequestTo: {
      type: 'json',
      defaultsTo: {name: '', userId: '', photo_url: '', email: ''}
    },
    dateOfRevereseRequest: {
      type: 'string',
      defaultsTo: ''
    },
    reverseRequestResponse: {
      type: 'string',
      defaultsTo: 'pending'
    },
    reverseRequestFlag: {
      type: 'boolean',
      defaultsTo: false
    },
    acceptRequestFlag:{
      type:'boolean',
      defaultsTo:false
    },
    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  },

};

