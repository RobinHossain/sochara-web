/**
 * Created by ASUS on 02-Mar-17.
 */
module.exports = {

  schema: true,

  attributes: {
    item:{
      type:'json',
      required:'true'
    },
    repeatDays:{
      type:'json'
    },
    repeatPattern:{
      type:'json'
    },
    repeatStart:{
      type:'json'
    },
    repeatEnd:{
      type:'json'
    },
    numberOfOccurance:{
      type:'string'
    },
    notifications:{
      type:'json'
    },
    reminder:{
      type:'json'
    },
    permission:{
      type:'json'
    },
    eventId:{
      type:'string'
    }
  }
  };
