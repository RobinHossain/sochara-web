/**
* Draft.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    user: {
      model: "User",
      required: true
    },
    user_id: { 
      type: "string",
      required: true
    },
    parent: {
      model: "MailToFolder",
      required: false
    },
    parent_id: {
      type: "string",
      required: false
    },
    ReplyOrForwardType: { 
      type: "string",
      required: false
    },
    to: { 
      type: "string",
      required: false
    },
    cc: {
      type: "string",
      required: false
    },
    bcc: {
      type: "string",
      required: false
    },
    from: {
      type: "string",
      required: false
    },
    subject: {
      type: "string",
      required: false
    },
    body: {
      type: "string",
      required: false
    },
  
    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
    }
  }
};

