/**
 * Tag.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  schema: true,

  attributes: {
    name: {
      type: "string",
      required: true
    },
    slug: {
      type: "string",
      required: true
    },
    type: {
      type: "string"
    },
    /**
     * Description
     * @method toJSON
     * @return obj
     */
    toJSON: function(){
        var obj = this.toObject();
        delete obj._csrf;
        return obj;
    }
  },

  /**
   * Description
   * @method beforeValidate
   * @param {} values
   * @param {} next
   * @return
   */
  beforeValidate: function(values, next) {
    if ( !values.name ) {
      return next({err: ["Please enter a valid tag name."]});
    }

    if ( !values.slug ) {
      values.slug = Utility.title_to_slug(values.name);
    };

    if ( !values.type ) {
      values.type = 'card';
    };

    next();
  }
};

