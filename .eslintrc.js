module.exports = {
  "env": {
    "browser": 1, "node": 1, "es6": 1
  },
  "globals": {
    "angular": 1, "node": 1
  },
  "extends": [
    "airbnb",
    "eslint:recommended"
  ],
  "plugins": [
    "jsdoc"
  ],
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "rules": {
    "no-console": 0,
    "indent": [
      "error",
      2
    ],
    "semi": [
      "error",
      "always"
    ],
    "jsdoc/check-param-names": 2,
    "jsdoc/check-tag-names": 2,
    "jsdoc/check-types": 2,
    "jsdoc/newline-after-description": 2,
    "jsdoc/require-description-complete-sentence": 2,
    "jsdoc/require-example": 2,
    "jsdoc/require-hyphen-before-param-description": 2,
    "jsdoc/require-param": 2,
    "jsdoc/require-param-description": 2,
    "jsdoc/require-param-type": 2,
    "jsdoc/require-returns-description": 2,
    "jsdoc/require-returns-type": 2,
    "linebreak-style": 0
  }
};
