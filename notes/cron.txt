# First of All decide what you want to do with setting Cron/Schedule.

=> If you want to process just normal task then create a task file with "crons" directory.
=> If you want to process Data or any complexity work then create you method on "api/services/CronService.js" file.
=> Link up your created task on "config/crontab.js" or Call your method from "config/crontab.js".
=> Set Schedule with setting time:

*    *    *    *    *    *
┬    ┬    ┬    ┬    ┬    ┬
│    │    │    │    │    |
│    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
│    │    │    │    └───── month (1 - 12)
│    │    │    └────────── day of month (1 - 31)
│    │    └─────────────── hour (0 - 23)
│    └──────────────────── minute (0 - 59)
└───────────────────────── second (0 - 59, OPTIONAL)

So cron setting example:
// jsonArray.push({interval: '*/10 * * * * *', method: 'mail'});

Lookup "config/crontab.js" file for more details.
